<!--
 * @(#)SCAppointmentPageOrder.page
 * 
 * Copyright 2010 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author hs <hschroeder@gms-online.de>
 * @version $Revision$, $Date$
-->
    <!--c:SCAppointmentProposal oid="{!id}" showsummary="true" showappointments="true"/-->

<apex:page id="mypage" title="{!$Label.SC_btn_GetAppointments}" name="GetAppointmentsEx" 
    StandardController="SCOrder__c" Extensions="SCAppointmentExtension" >
    
    <apex:stylesheet value="{!URLFOR($Resource.SCRes,'css/cp-global.css')}" />
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/jquery-1.7.1.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'scripts/global.js')}" />

    <script type="text/javascript"> 
    
        // START 1 OF 3 OF COLOR-APPOINTMENT FUNCTIONS
        // Need this to colorize the appointment proposals rows (gmssu)
        // The RGB colors in black/white area are so: rgb(X,X,X) 
        // where X is from 0 to 255 (255,255,255 -> white, 0,0,0 -> black)
        var countColors = 255; 
        // Already colored rows
        var alreadyColored = new Array();
        
        // Calculated appointment-delta array
        var appDeltas = new Array();
        // Appointment-id-delta array
        var appIdDelta = new Array();
        
        // Function to clear colored data
        function clearColoredData()
        {
            countColors = 255;
            alreadyColored.length = 0;
            appDeltas.length = 0;
            appIdDelta.length = 0;
        }
        
        // Help function for sorting the numeric arrays (a - b > asc , b - a > desc)
        function sortNumber(a, b)
        {
            // ASC
            return a - b;
        }
        
        // Help function for removing duplicate elements from the array
        function removeDuplicateElement(arrayToClear)
        {
            var newDeltaArray = new Array();
            label:for(var i = 0; i < arrayToClear.length; i++ )
            {  
                for(var j = 0; j < newDeltaArray.length; j++ )
                {
                    if(newDeltaArray[j] == arrayToClear[i]) 
                    continue label;
                }
                newDeltaArray[newDeltaArray.length] = arrayToClear[i];
            }
            return newDeltaArray;
        }
        
        // Colorize the appointments depending on date (earliest date = white, later date = black)
        // This script colorizes the appointment rows depending on date (same date - same color) and delta (0 - white, 1 - light gray and so on).
        function colorizeAppointments()
        {
            
            // Only run the script if these arrays are not empty
            if(appDeltas.length != 0 && appIdDelta.length != 0)
            {
                // Sorting the deltas array (asc)
                appDeltas.sort(sortNumber);
                appDeltas = removeDuplicateElement(appDeltas);
                
                // Running through the deltas array
                for(var i = 0; i < appDeltas.length; i++)
                {
                    // Running through the app-id-deltas array
                    for(var x = 0; x < appIdDelta.length; x++)
                    {
                        // Exploding the string
                        var tempAppItem = appIdDelta[x].split(',');
                        
                        // Getting the app Id and delta
                        var tmpAppId = tempAppItem[0];
                        var tmpAppDelta = tempAppItem[1];
                        
                        // Colorize the appointments row, if appDelta = Delta
                        if(tmpAppDelta == appDeltas[i] && jQuery.inArray(tmpAppId, alreadyColored) == -1)
                        {
                            document.getElementById(tmpAppId).parentNode.style.backgroundColor = "rgb(" + countColors + "," + countColors + "," + countColors + ")";
                            alreadyColored.push(tmpAppId);
                        }
                    } 
                    
                    // RGB-Counter. Make the color darker but not too dark.
                    if(countColors >= 195)
                    {
                        countColors -= 12;
                    }
                    if(countColors < 165)
                    {
                        countColors -= 6;
                    } 
                }
            }
            
            // Clear the colored data
            clearColoredData();
        }
        
        // Days between two dates
        function daysBetween(date1, date2)
        {
            // The number of milliseconds in one day
            var ONE_DAY = 1000 * 60 * 60 * 24;
        
            // Convert both dates to milliseconds
            var date1_ms = date1.getTime();
            var date2_ms = date2.getTime();
        
            // Calculate the difference in milliseconds
            var difference_ms = Math.abs(date1_ms - date2_ms);
            
            // Convert back to days and return
            return Math.round(difference_ms/ONE_DAY);
        }
        // END 1 OF COLOR-APPOINTMENT FUNCTIONS
    
        function showAppointmentAnalysisDialog() 
        {
            var dlgattribs = "menubar=0,location=0,status=0,resizable=1,scrollbars=1,width=" + screen.width + ",height=" + (screen.height - 1);
            dlg = window.open("/apex/SCAppointmentAnalysis", "Dispatching", dlgattribs);
            dlg.moveTo(0, 0);
        }
        function disableInputs()
        {
            jQuery(':input').attr('disabled',true);
        }

        /** 
        * Is called when a customer property is changed. If all questions are answered,
        * the methode onContinueServiceMessage is called
        */
        function onChgCustomerProperty()
        {
            var allAnswered = 1;
            jQuery('select[class=customerProperty]').each( function(index, element)
            {
                allAnswered &= (jQuery(element).val() != '');
            });
            
            if (1 == allAnswered)
            {
                onContinueServiceMessage();
            }
        }
    </script>

<apex:form id="dlgForm">

    <!-- Order summary - the time window fields are displayed if invalid -->
    <apex:pageBlock title="{!$ObjectType.SCOrder__c.label}" mode="edit" id="dlgOrder">
        <apex:pageBlockButtons location="top">
            <apex:commandButton value="{!$Label.SC_btn_Cancel}" onClick="window.location.href = '/{!order2.id}'; return false;"/>
            <!-- almost hidden input field that gets the input focus - prevent the auto drop down in the desired start field -->        
            <apex:inputText value="{!CompShowSummary}" style="width:0px;height:0px"/>
        </apex:pageBlockButtons>
        <apex:pageBlockSection >
            <apex:pageBlock >
                <apex:pageBlockSection columns="1" id="dlgOverview">
                    <apex:outputField value="{!order2.Name}"/>
                    <apex:outputField value="{!order2.Status__c}"/>
                    <apex:outputField value="{!order2.Type__c}"/>
                    <apex:outputField value="{!order2.DepartmentCurrent__r.name}"/>
                    <apex:outputField value="{!order2.CustomerPriority__c}"/>

                    <apex:inputField value="{!order2.CustomerTimewindow__c}" />
                    <apex:inputField value="{!order2.CustomerPrefStart__c}"/>
                    <apex:inputField value="{!order2.CustomerPrefEnd__c}" />
                    <apex:inputField value="{!order2.Duration__c}"/>
                    <apex:inputField value="{!order2.Standby__c}"/>
                    <!-- apex:pageBlockSectionItem -->
                        <!-- apex:outputLabel value="{!$ObjectType.SCOrder__c.fields.AdditionalEmployee__c.label}" rendered="{!hasPending}"/ -->
                        <!-- apex:panelGroup layout="none" -->
                            <!-- apex:inputField id="addEmpl" value="{!order2.AdditionalEmployee__c}"  rendered="{!hasPending}" / -->
                            <!-- apex:panelGroup layout="none" rendered="{!NOT(canHaveAdditionalEmployee)}" -->
                                <!-- script>
                                    jQuery('[id$=addEmpl]').attr('disabled', true);
                                </script -->
                            <!-- /apex:panelGroup -->              
                        <!-- /apex:panelGroup -->              
                    <!-- /apex:pageBlockSectionItem -->                    
                </apex:pageBlockSection>
            </apex:pageBlock>

            <apex:pageBlock >
                <apex:pageBlock title="{!$ObjectType.SCInstalledBase__c.label}">
                    <apex:pageBlockSection columns="1">
                    <apex:outputfield value="{!orderitem.InstalledBase__r.ProductNameCalc__c}"/>
                    <apex:outputfield value="{!orderitem.InstalledBase__r.ProductModel__c}" />
                    <apex:outputfield value="{!orderitem.InstalledBase__r.SerialNo__c}" />
                    <apex:outputfield value="{!orderitem.InstalledBase__r.IdExt__c}" />
                    </apex:pageBlockSection>
                </apex:pageBlock>
                
                <c:SCInfoBlock title="{!$ObjectType.SCInstalledBaseLocation__c.label}" text="{!LocationAddress}"/>
            </apex:pageBlock>
        </apex:pageBlockSection>
    
    
<!--    
    </apex:pageBlock>
    <apex:pageBlock title="{!$ObjectType.SCOrder__c.label}" mode="edit" id="dlgOrder2" >
        <apex:pageBlockButtons >
            <apex:outputLink value="/{!order2.id}" styleClass="btn cp-link-button" >{!$Label.SC_btn_Cancel}</apex:outputLink>
            <apex:inputField value="{!order2.IdOld__c}" style="width:0;height:0"/>
        </apex:pageBlockButtons>

        <apex:inputField value="{!order2.Country__c}" style="display:none"/>    

        <apex:pageBlockSection id="dlgOverview" rendered="{!canDispatch}">
            <apex:outputField value="{!order2.Name}"/>
                <apex:inputField value="{!order2.CustomerTimewindow__c}" />
            <apex:outputField value="{!order2.Status__c}"/>
                <apex:inputField value="{!order2.CustomerPrefStart__c}"/>
            <apex:outputField value="{!order2.Type__c}"/>
                <apex:inputField value="{!order2.CustomerPrefEnd__c}" />
            <apex:outputField value="{!order2.DepartmentCurrent__r.name}"/>
                <apex:outputField value="{!order2.CustomerPriority__c}"/>
            <apex:outputField value="{!order2.Duration__c}"/>
                <apex:inputField value="{!order2.Standby__c}"/>

            <apex:pageBlock title="{!$ObjectType.SCInstalledBase__c.label}">
                <apex:pageBlockSection columns="1">
                <apex:outputfield value="{!orderitem.InstalledBase__r.ProductGroup__c}"/>
                <apex:outputfield value="{!orderitem.InstalledBase__r.ProductPower__c}"/>
                <apex:outputfield value="{!orderitem.InstalledBase__r.ProductEnergy__c}"/>
                </apex:pageBlockSection>
            </apex:pageBlock>
            
            <c:SCInfoBlock title="{!$ObjectType.SCInstalledBaseLocation__c.label}" text="{!LocationAddress}"/>
        </apex:pageBlockSection>
-->

        <apex:pageBlockSection columns="1" rendered="{!IsCustomerLocked}">
            <apex:pageMessage severity="warning" strength="1" escape="false" title="{!$Label.SC_msg_AccLockTitle}"> 
                {!$Label.SC_msg_AccLockInfo}: 
                <apex:panelGroup layout="none" rendered="{!NOT(ISNULL(roleRE.Account__r.LockType__c))}">
                    <b>{!$ObjectType.Account.fields.LockType__c.label}:</b> 
                    [<apex:outputField value="{!roleRE.Account__r.LockType__c}"/>] 
                </apex:panelGroup>
                <apex:panelGroup layout="none" rendered="{!NOT(ISNULL(roleRE.Account__r.RiskClass__c))}">
                    <b>{!$ObjectType.Account.fields.RiskClass__c.label}:</b> 
                    [<apex:outputField value="{!roleRE.Account__r.RiskClass__c}"/>]
                </apex:panelGroup>
            </apex:pageMessage>
        </apex:pageBlockSection>

        <!-- Order Type / Status error -->
        <apex:pageBlockSection columns="1" rendered="{!!canDispatch}" id="dlgWarning">
            <apex:pageMessage severity="warning" strength="2" title="{!$Label.SC_msg_SchedulingWarning}" escape="false" rendered="{!NOT(canDispatch)}"> 
            {!$Label.SC_msg_SchedulingImpossible}
            <B>
            <apex:outputPanel rendered="{!OR(orditem.order__r.Status__c == '5506', 
                                             orditem.order__r.Status__c == '5507',
                                             orditem.order__r.Status__c == '5508')}">
                <br/>- der Auftragsstatus ist bereits [<apex:outputField value="{!orditem.order__r.Status__c}"/>]                
            </apex:outputPanel>

            <apex:outputPanel rendered="{!orditem.order__r.InvoicingReleased__c}">
                <br/>- der Auftrag wurde bereits zur Faktura freigegeben - siehe Kennzeichen [Rechnung freigeben]. 
            </apex:outputPanel>

            <apex:outputPanel rendered="{!orditem.order__r.cce_ExternalOperationCount__c > 0}">
                <br/>- der Auftrag wurde bereits extern beauftragt und kann daher nicht auf einen Techniker disponiert werden.
            </apex:outputPanel>
            
            <apex:outputPanel rendered="{!orditem.cce_canDispatch__c == '0'}">
                <br/>- der Auftrag gehört zu einer anderen Organisationseinheit (werksfremd) 
            </apex:outputPanel>
            <BR/>
            </B>
            </apex:pageMessage>                    
            <apex:commandButton value="{!$Label.SC_btn_Continue}" onClick="window.location.href = '/{!order2.id}'; return false;"/>
        </apex:pageBlockSection>



        <!-- Validation: show already existing appointments -->
        <apex:pageBlockSection columns="1"  rendered="{!!canHaveMoreAppointments && canDispatch}" id="dlgPendingAppointments">
            <apex:pageMessage severity="info" strength="1" escape="false" title="{!$Label.SC_msg_SchedulingAppointmentsExist}"> 
            </apex:pageMessage>
            <apex:pageblockTable value="{!pendingAppointments}" var="a">
                <apex:column value="{!a.Start__c}"/>
                <apex:column value="{!a.End__c}"/>
                <apex:column value="{!a.Resource__c}"/>
                <apex:column value="{!a.Employee__c}"/>
                <apex:column value="{!a.Mobile__c}"/>
                <apex:column value="{!a.Phone__c}"/>
                <apex:column value="{!a.AssignmentStatus__c}"/>
                <apex:column value="{!a.CustomerTimewindow__c}"/>
                <apex:column value="{!a.Fixed__c}"/>
                <apex:column value="{!a.FixedResource__c}"/>
            </apex:pageblockTable>
        </apex:pageBlockSection>
        
        <!-- Validation: show the geocoding component if not yet geocoded-->
        <apex:pageBlockSection columns="1" rendered="{!!IsGeocoded && canDispatch && NOT(IsCustomerLocked) && canHaveMoreAppointments}" id="dlgGeocoding">
            <apex:pageMessage severity="warning" strength="1" title="{!$Label.SC_msg_SchedulingGeocodeAddress}" 
                              detail="{!$Label.SC_msg_SchedulingGeocodeAddress2}" escape="false"/> 
            <c:SCAddressValidation id="addressList" 
                                       key="addressList" 
                                       paramAddr="{!locaddr}" 
                                       paramShowEdit="true" 
                                       paramShowMap="true" 
                                       pageController="{!This}" 
                                       pageControllerRerender="dlgForm,suggestions" />
        </apex:pageBlockSection>

<!-- PMS 31145/Review20110808/CR: Mehrfachdisposition deaktivieren (Change Request) HasPending  -->

        <apex:actionFunction name="onContinueServiceMessage" action="{!onContinueServiceMessage}" 
                             reRender="dlgOrder" 
                             oncomplete="startScheduling();" 
                             status="dlgProcessingStatus"/>
        <apex:actionFunction name="startScheduling" action="{!startScheduling}" 
                             reRender="dlgOrder" status="dlgProcessingStatus"/>

        <apex:outputPanel id="dlgServiceMessage" rendered="{!AND(NOT(ISNULL(serviceOrder)), canDispatch, NOT(IsCustomerLocked))}">
            <apex:pageBlock title="{!$Label.SC_app_CustomerProperty}" mode="edit">
                <apex:pageBlockSection columns="2">
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel value="{!$Label.SC_App_OrderParkingAvailable}"/>
                        <apex:inputField value="{!serviceOrder.CustPropIsParkingAvailable__c}" 
                                         styleClass="customerProperty" onchange="onChgCustomerProperty()"/>
                    </apex:pageBlockSectionItem>
                    <apex:pageMessage severity="info" strength="1" escape="false" title="{!$Label.SC_msg_OrderDispatchHint}"/> 
                    
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel value="{!$Label.SC_app_OrderParkingPermit}"/>
                        <apex:inputField value="{!serviceOrder.CustPropIsProvideParkingPermit__c}" 
                                         styleClass="customerProperty" onchange="onChgCustomerProperty()"/>
                    </apex:pageBlockSectionItem>
                    <apex:actionStatus id="dlgProcessingStatus" 
                                       startText="{!$Label.SC_btn_Processing}" 
                                       stopText="" 
                                       startStyleClass="processing"/>
                    
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel value="{!$Label.SC_app_OrderApplAccessible}"/>
                        <apex:inputField value="{!serviceOrder.CustPropIsApplAccessible__c}" 
                                         styleClass="customerProperty" onchange="onChgCustomerProperty()"/>
                    </apex:pageBlockSectionItem>
                    <apex:outputText value=""/>

                    <apex:pageMessage id="custPropWarning" severity="warning" strength="1" escape="false" 
                                      title="{!$Label.SC_msg_CustomerPropertyWarning}" rendered="{!showServiceMessageWarning}"/>
                </apex:pageBlockSection>
            </apex:pageBlock>
        </apex:outputPanel>

        <apex:pageBlockSection columns="1" title="{!$Label.SC_app_AppointmentProposals}" 
                               rendered="{!canDispatch && IsGeocoded && NOT(IsCustomerLocked) && canHaveMoreAppointments && ISNULL(serviceOrder)}">

            <apex:outputPanel >
                <apex:commandButton action="{!onGetAppointments}" value="{!$Label.SC_btn_GetAppointments}" status="dlgStatus" reRender="dlgForm"/> &nbsp;
                <apex:commandButton action="{!onGetAppointmentsNext}" value="{!$Label.SC_btn_More} {!apptMaxLastProposalDay}" status="dlgStatusMore" reRender="dlgForm" id="dlgMore"/> &nbsp;
                <apex:commandButton action="{!onCancel}" value="{!$Label.SC_btn_Cancel}"/> &nbsp;&nbsp;&nbsp;&nbsp;
                <apex:commandButton onclick="javascript:showAppointmentAnalysisDialog()" value="Info"/> 
                &nbsp;&nbsp;
                <b><apex:outputLabel value="{!$Label.SC_app_PostCodeArea}" rendered="{!appSettings.POSTCODEAREA_EVALUATION__c}"/></b>
                &nbsp;
                <apex:outputText value="{!areaInfo}" rendered="{!appSettings.POSTCODEAREA_EVALUATION__c}"/> 
                &nbsp;&nbsp;
                <b><apex:outputLabel value="{!$Label.SC_app_Standby}" rendered="{!appSettings.ENGINEERSTANDBY_EVALUATION__c}"/></b>
                &nbsp;
                <apex:outputText value="{!standbyInfo}" rendered="{!appSettings.ENGINEERSTANDBY_EVALUATION__c}"/> 
                &nbsp;&nbsp;
                <apex:actionStatus id="dlgStatus" startText="{!$Label.SC_app_Processing}{!order2.CustomerPrefStart__c}..."/>
                <apex:actionStatus id="dlgStatusMore" startText="{!$Label.SC_app_Processing}{!apptMaxLastProposalDay}..."/>
            </apex:outputPanel>    

            <apex:actionFunction name="saveSelected" action="{!save}" />
            <apex:actionFunction name="saveSelectedFixed" action="{!saveFixed}" />
            <apex:messages />
            
            <script>
                // START 2 OF 3 OF COLOR-APPOINTMENT FUNCTIONS
                // Need this to know what number of loop is currenty runs.
                var appcount = 0;
                // END 2 OF COLOR-APPOINTMENT FUNCTIONS
            </script>

            <apex:pageBlockTable id="suggestions" var="appt" value="{!apptList}" rendered="{!NOT(ISNULL(apptList))}">

                <apex:column headerValue="Auto">
                    <apex:inputCheckbox value="{!appt.selected}"
                        onclick="saveSelected();disableInputs();"/>
                </apex:column>

               <apex:column headerValue="{!$Label.SC_app_Date}" id="appointmentDateRow">
                    <apex:outputText value="{0, date, dd.MM.yyyy E}">
                        <apex:param value="{!appt.day}" />
                    </apex:outputText>
                </apex:column>
                <apex:column headerValue="{!$Label.SC_app_From}">
                    <apex:outputText value="{0, time, HH:mm}">
                        <apex:param value="{!appt.cdstart}" />
                    </apex:outputText>
                </apex:column>
                <apex:column headerValue="{!$Label.SC_app_To}">
                    <apex:outputText value="{0, time, HH:mm}">
                        <apex:param value="{!appt.cdend}" />
                    </apex:outputText>
                </apex:column>

               <apex:column headerValue="{!$Label.SC_app_Fixed}">
                    <apex:inputCheckbox value="{!appt.selectedFixed}"
                        onclick="saveSelectedFixed();disableInputs();"/>
               </apex:column>

               <apex:column headerValue="{!$ObjectType.SCResource__c.label}">
                   <apex:outputText >{!appt.employeefirstname} {!appt.employeename}</apex:outputText>
               </apex:column>

               <apex:column value="{!appt.employeeId}" headerValue="{!$ObjectType.SCResource__c.label}" />
               <apex:column value="{!appt.employeeFunc}" headerValue="{!$ObjectType.SCResource__c.fields.Type__c.label}" />

               <apex:column value="{!appt.address}" headerValue="{!$Label.SC_app_PreviousAddress}" />
               <apex:column value="{!appt.tour}" headerValue="{!$Label.SC_app_TourExtension}" />
               
               <apex:column headerValue="Delta" id="appDelta">
                    <script>
                        
                        // START 3 OF 3 OF COLOR-APPOINTMENT FUNCTIONS
                        // Checking the compatibility with innerText/textContent
                        // IE8: innerText, FF3.6: textContent
                        if(document.all){
                            // Getting the value (text) of current appointment date
                            var currentAppDate = document.getElementById('{!$Component.appointmentDateRow}').innerText;
                            currentAppDate = jQuery.trim(currentAppDate);
                        } else{
                            var currentAppDate = document.getElementById('{!$Component.appointmentDateRow}').textContent;
                            currentAppDate = jQuery.trim(currentAppDate);
                        }
                        
                        // Reading the day/month/year from the date string
                        // We need this to create an Javascript Date object from these strings,
                        // to be able to compare the dates.
                        var appDay = currentAppDate.substr(0,2);
                        var appMonth = currentAppDate.substr(3,2);
                        var appYear = currentAppDate.substr(6,4);
                                                
                        // Creating a new Date object for the todays date
                        // (we need to compare toays date and the current app. date to know the delta)
                        var currentDate = new Date();
                        
                        // Read the current appointment date and converting it to the Javascript Date object
                        var appDate = new Date();
                        appDate.setYear(appYear);
                        appDate.setMonth(appMonth-1);
                        appDate.setDate(appDay);
                        
                        // Calculating delta (days between today and current app. date and setting it to the row
                        var delta = daysBetween(currentDate, appDate);
                        document.getElementById('{!$Component.appDelta}').innerHTML = delta;
                        // Save delta into the array
                        appDeltas.push(delta);
                        // Save appointment-rowId & delta into the array
                        appIdDelta.push('{!$Component.appointmentDateRow},'+delta);
                        
                        appcount++;
                        
                        // If this is the last loop - colorize the rows
                        if(appcount == {!apptList.size}){
                            colorizeAppointments();
                        }
                        // END 3 OF COLOR-APPOINTMENT FUNCTIONS
                        
                    </script>
               </apex:column>

            </apex:pageBlockTable>

        </apex:pageBlockSection>
    </apex:pageBlock>    
</apex:form>

</apex:page>
