<!--
 * @(#)SCOrderExternalAssignment.page
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This page adds a new order extern assignment and their items to the order.
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
-->
<apex:page standardController="SCOrderExternalAssignment__c" extensions="SCOrderExternalAssignmentController" sidebar="true">

<apex:sectionHeader title="{!$Label.SC_app_CreationWizard}" subTitle="{!$ObjectType.SCOrderExternalAssignment__c.label}" />

<apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/jquery-1.6.2.min.js')}" />

<style>
.searching
{
    background:url(/img/loading.gif) no-repeat 0 0; 
    padding-left:20px; 
    margin-left:10px; 
    padding-bottom:5px; 
    font-size:12px;
    height:16px;
}
.thisBlock input[type=text], .thisBlock textarea, .thisBlock select{
    width:60%;
}
.removeServiceIcon {
    background-image: url("/img/feeds/follow_sprite.png");
    background-position: 0 -75px;
    background-repeat: no-repeat;
    width:15px;
    height:15px;
    cursor:pointer;
}
.removeServiceIcon:hover {
    background-position: 0 -54px;
}
.arrowUp {
    background: url(/img/colTitle_uparrow.gif) no-repeat 3px 2px #fff;
    width:13px;
    height:8px;
    border:solid 1px #B0B0B0;
    border-radius: 2px 2px 0 0;
    display:block;
    cursor:pointer;
}
.arrowUp:active {
    background-position: 3px 1px;
}
.arrowDown {
    background: url(/img/colTitle_downarrow.gif) no-repeat 3px 2px #fff;
    width:13px;
    height:8px;
    border:solid 1px #B0B0B0;
    border-radius: 0 0 2px 2px;
    display:block;
    cursor:pointer;
    border-top:none;
}
.arrowDown:active {
    background-position: 3px 3px;
}
</style>

<script>

/**
 * Selects / deselects all checkbockes for the services
 *
 * @param    obj    true or false - select or desect checkboxes
 */
function selectAll(obj)
{
    if(jQuery(obj).is(':checked'))
    {
        jQuery('.serviceClass input[type=checkbox]').each( function(){ 
            jQuery(this).attr('checked', 'checked'); 
        });
    }
    else
    {
        jQuery('.serviceClass input[type=checkbox]').each( function(){ 
            jQuery(this).removeAttr('checked'); 
        });
    }
}

/**
 * Process cancelation confirmation
 */
function confirmCancel() {
    var isCancel = confirm("{!$Label.SC_msg_ConfirmCancel}");
    if (isCancel) return true;
    return false;
}

/**
 * Replace the . to , in the sum price string
 * For example 3.99 to 3,99
 */
function fixPriceOutput()
{
    // Replacing dot to comma
    var value = jQuery('[id$=sumOutput]').text() + '';
    value = value.replace('.',',');
    jQuery('[id$=sumOutput]').html(value);
}

/**
 * Disables the vendor input field if no order selected
 * and hides the lookup icon
 *
 * @param    param    true or false - hide or show / disable or enable
 */
function hideVendorIfNoOrderId(param)
{
    <apex:outputText rendered="{!ISBLANK($CurrentPage.parameters.oid) || $CurrentPage.parameters.oid = ''}" escape="false">
        if(param)
        {
            jQuery('[id$=vendorField]').attr('disabled','disabled');
            jQuery('[id$=vendorField_lkwgt]').hide();
        }
        else
        {
            jQuery('[id$=vendorField]').removeAttr('disabled');
            jQuery('[id$=vendorField_lkwgt]').show();
        }
    </apex:outputText>
}

/**
 * The DOM ready function
 */
jQuery(document).ready(function()
{
    <apex:outputText rendered="{!ISBLANK($CurrentPage.parameters.oid) || $CurrentPage.parameters.oid = ''}" escape="false">
        hideVendorIfNoOrderId(true);
    </apex:outputText>
});

/** 
* Returns the accessible element ID
*
* @param myid   the id of the visualforce object
*/
function esc(myid) {
   return '#' + myid.replace(/(:|\.)/g,'\\\\$1');
}

/** 
* This function checks the value of the input field
* and if it not defined - sets the min value (1)
*
* @param obj  The DOM object to check
*/ 
function checkNumber(obj,checkboxId)
{    
    if(isNaN(obj.value))
    {    
        obj.value = '0';
        jQuery(esc(checkboxId)).removeAttr('checked');
    }
    else if(obj.value == '')
    {
        obj.value = '0';
        jQuery(esc(checkboxId)).removeAttr('checked');
    }
    else if(parseInt(obj.value) == 0)
    {
        jQuery(esc(checkboxId)).removeAttr('checked');
    }
    else if(parseInt(obj.value) > 0)
    {
        jQuery(esc(checkboxId)).attr('checked','checked');
    }
}

/** 
* This function calculates the value of the input field
* if the user clicks on the /\ or \/ buttons
*
* @param action  The type of action to be calculated (eg. plus or minus)
* @param type    Type of the input field set (eg. percent, order or group)
* @param element The target element for witch the value must be calculated
* @param list    Select list id
*/   
function manipulateValue(action, type, element, list, checkboxId)
{    
    var incrementTo = 1;
    var val = parseInt(parseFloat(jQuery(esc(element)).val()));
    if(isNaN(val))
    {
        val = 0;
    }
    
    
    // Depends on the type list value we set the new value to the single element (current target)
    // or the group of elements (input fields).
    if(jQuery('[id$=' + list + ']').val() == 'weekday' || list == 'null')
    {
        var targetId = esc(element);
    }
    if(jQuery('[id$=' + list + ']').val() == 'global')
    {
        alert(jQuery(esc(element)).attr('class'));
        var targetId = '.' + jQuery(esc(element)).attr('class');
    }
    
    if(action == 'plus')
    {
        if((val + incrementTo) > 100 && list != 'null')
        {
            jQuery(targetId).val('100');
        }
        else
        {
            jQuery(targetId).val(val + incrementTo);
            if(checkboxId != '')
            {
                jQuery(esc(checkboxId)).attr('checked','checked');
            }
        }
    }
    if(action == 'minus')
    {
        if((val - incrementTo) < 0)
        {
            jQuery(targetId).val('0');
            if(checkboxId != '')
            {
                jQuery(esc(checkboxId)).removeAttr('checked');
            }
        }
        else if((val - incrementTo) == 0)
        {
            jQuery(targetId).val(val - incrementTo);
            if(checkboxId != '')
            {
                jQuery(esc(checkboxId)).removeAttr('checked');
            }
        }
        else
        {
            jQuery(targetId).val(val - incrementTo);
        }
    }
}
function itemSelected()
    {
        var checkedbox = jQuery('input[id$=isSelectedCheckbox]:checked').length
        if(checkedbox >0)
        {            
            return true;
        }else
        {
            alert('{!$Label.SC_msg_NoVCSSelected}');
            return false;
        }
    }
    function gotoNext()
    {
        doGoToNext();
    }
    
    function disableButton(obj)
    {
        var d = document.getElementById(obj.id); 
        d.className = d.className + " btnDisabled"; 
        d.disabled = 'disabled';    
    }    
</script>

    <apex:form id="mainForm">
        <!--PMS38340 , at least on Vendor Contract Services should be selected  21.07.2014 GMSSW-->
    <apex:actionFunction name="doGoToNext" 
                         action="{!goNext}" 
                         />
        
    <apex:actionFunction name="loadServices"
                         reRender="allServices" 
                         status="readVendorContractItemsStatus"
                         action="{!getReadServices}"/>

    <apex:actionFunction name="loadItems"
                        reRender="vendorContractItems" 
                        status="readVendorContractStatus"
                        action="{!getListVendorContractItems}"
                        oncomplete="jQuery('#vendorContractItemsList').change();"/>

    <apex:outputPanel id="hiddenValues">
        <apex:inputHidden value="{!selectedVendorContract}"
                          id="hiddenVendorContract"/>
                          
        <apex:inputHidden value="{!selectedVendorContractItem}"
                          id="hiddenVendorContractItem"/>
    </apex:outputPanel>

    <apex:outputPanel styleClass="thisBlock">
    <apex:pageBlock title="{!$Label.SC_msg_Step + ' ' + TEXT(pageNum) + ' - 2'}" id="theBlock" mode="edit">

    <apex:pageMessages />    
    <apex:outputPanel id="messages">
        <apex:messages styleClass="message warningM4"/>
        <apex:pageMessage severity="warning" strength="2" detail="" summary="{!errorMessage}" rendered="{!showErrorMessage}"/>
    </apex:outputPanel>
    
    <apex:actionFunction name="doAfterSelectingOrder"
                         reRender="mainForm"
                         oncomplete="hideVendorIfNoOrderId({!!CanProcesOrder});" immediate="false"/>
        
        <!-- Buttons -->
        <apex:pageblockButtons location="top" id="pageButtons">
            <apex:commandButton value="{!$Label.SC_btn_Back}" 
                                style="width:100px;" 
                                disabled="{!IF(currentPage = 'first',true,false)}" 
                                reRender="theBlock" 
                                status="gotoPage">
                <apex:param assignTo="{!currentPage}" name="cpageBack" value="first" />
                <apex:param assignTo="{!pageNum}" name="P1" value="{!pageNum - 1}" />
            </apex:commandButton>
            <apex:commandButton value="{!$Label.SC_btn_Next}" 
                                style="width:100px;" 
                                rendered="{!IF(currentPage = 'first',true,false)}" 
                                reRender="messages,theBlock" 
                                status="gotoPage"
                                oncomplete="{!IF(ISBLANK(extAssignment.Order__c), 'hideVendorIfNoOrderId(true);','')}"
                                disabled="{!!CanProcesOrder}"
                                action="{!goNext}">
                <apex:param assignTo="{!currentPage}" name="cpageNext" value="second" />
                <apex:param assignTo="{!pageNum}" name="P1" value="{!pageNum + 1}" />
            </apex:commandButton>
            <!--PMS 38340, prevent the save button from being double-clicked -->
           <apex:actionStatus id="saveItemStatus">
             <apex:facet name="stop">
                 <apex:commandButton value="{!$Label.SC_btn_Save}" 
                                     action="{!save}" 
                                     rerender="dummy"
                                     rendered="{!IF(currentPage = 'second',true,false)}" 
                                     status="saveItemStatus"
                                     style="width:100px;" 
                                     />
             </apex:facet>
             <apex:facet name="start"> 
                 <apex:outputPanel >
                       <apex:image value="/img/loading32.gif" style="height: 15px;"/>
                       <apex:commandButton value="{!$Label.SC_app_Processing}" 
                                           status="saveItemStatus"
                                           disabled="true"
                                           style="width:100px;"/>
                 </apex:outputPanel>
             </apex:facet>
            </apex:actionStatus>           
            
            <!--apex:commandButton value="{!$Label.SC_btn_Save}" 
                                style="width:100px;" 
                                rendered="{!IF(currentPage = 'second',true,false)}"
                                
                                action="{!save}"

                                /-->
                                
                                
                                
            <apex:commandButton action="{!cancel}" 
                                value="{!$Label.SC_btn_Cancel}" 
                                onclick="return confirmCancel()" 
                                immediate="true"/>
            
            <apex:actionStatus id="gotoPage" 
                   startText="" 
                   stopText="" 
                   startStyleClass="searching" />
        </apex:pageblockButtons>
    
    
        <!-- Main Section -->
        <apex:pageBlockSection columns="2" rendered="{!IF(currentPage = 'first',true,false)}">
        
            <!-- Left Section -->
            <apex:pageBlockSection columns="1">
            
                <!--
                0. The user selects an order (if no id provided)
                1. The user selects a vendor
                2. After that the contract-list "changed" with jQuery and so will be refreshed
                3. After that the contract-item-list "changed" with jQuery and so will be refreshed
                4. After that the services-list "changed" with jQuery and so will be refreshed
                -->
                
                <!-- Order -->
                <apex:pageBlockSectionItem rendered="{!ISBLANK($CurrentPage.parameters.oid) || $CurrentPage.parameters.oid = ''}" >
                    <apex:outputLabel value="{!$ObjectType.SCOrder__c.label}"/>
                    <apex:inputField value="{!extAssignment.Order__c}" 
                                     required="true" >
                        <apex:actionSupport event="onchange"
                                            reRender="vendorField" 
                                            action="{!checkOrder}"
                                            oncomplete="doAfterSelectingOrder()"
                                            immediate="true"/>      
                    </apex:inputField>                                
                </apex:pageBlockSectionItem>
                
                <!-- Vendor -->
                <apex:pageBlockSectionItem labelStyle="width:35%;" dataStyle="width:65%;">
                    <apex:outputLabel value="{!$ObjectType.SCVendor__c.label}"/>
                    <apex:outputPanel >
                        
                        <apex:inputField value="{!extAssignment.Vendor__c}" 
                                         required="{!CanProcesOrder}" 
                                         id="vendorField" 
                                         style="float:left;">
                                         
                            <apex:actionSupport event="onchange"
                                                reRender="vendorContracts,hiddenValues,messages" 
                                                status="readVendorStatus"
                                                action="{!getListVendorContracts}"
                                                oncomplete="jQuery('#vendorContractsList').change();"/>
                        </apex:inputField>

                        <apex:actionStatus id="readVendorStatus" 
                                           startText="..." 
                                           stopText="" 
                                           startStyleClass="searching" startStyle="float:left;"/>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                
                <!-- Vendor Contracts -->
                <apex:pageBlockSectionItem id="vcon">
                    <apex:outputLabel value="{!$ObjectType.SCVendorContract__c.label}"/>
                    <apex:outputPanel id="vendorContracts">
                        <!--
                        selectedVendorContract: {!selectedVendorContract}<br/>
                        -->
                        
                        <select id="vendorContractsList" onchange="jQuery('[id$=hiddenVendorContract]').val(jQuery('#vendorContractsList option:selected').val());loadItems();">
                            <apex:repeat value="{!vContracts}" var="contract">
                                <option value="{!contract.id}">
                                    {!contract.name} ({!contract.Status__c})
                                </option>
                            </apex:repeat>
                        </select>
                        
                        <!-- Selecting the selectd option -->
                        <script>
                            jQuery('#vendorContractsList').val('{!selectedVendorContract}').attr('selected', 'selected')
                        </script>
                        
                        <apex:actionStatus id="readVendorContractStatus" 
                                           startText="..." 
                                           stopText="" 
                                           startStyleClass="searching" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                
                <!-- Vendor Contract Items -->
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$ObjectType.SCVendorContractItem__c.label}"/>
                    <apex:outputPanel id="vendorContractItems">
                        <!--
                        selectedVendorContractItem: {!selectedVendorContractItem}<br/>
                        -->
                        
                        <select id="vendorContractItemsList" onchange="jQuery('[id$=hiddenVendorContractItem]').val(jQuery('#vendorContractItemsList option:selected').val());loadServices();">
                            <apex:repeat value="{!vContractItems}" var="item">
                                <option value="{!item.id}">
                                    {!item.name} ({!item.Status__c})
                                </option>
                            </apex:repeat>
                        </select>
                        
                        <!-- Selecting the selectd option -->
                        <script>
                            jQuery('#vendorContractItemsList').val('{!selectedVendorContractItem}').attr('selected', 'selected')
                        </script>

                        <apex:actionStatus id="readVendorContractItemsStatus" 
                                           startText="..." 
                                           stopText="" 
                                           startStyleClass="searching" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
         
            </apex:pageBlockSection> 
            
            <!-- Right Section -->
            <apex:pageBlockSection columns="1">
            
                <!-- Wished Date -->
                <apex:pageBlockSectionItem labelStyle="width:35%;" dataStyle="width:65%;">
                    <apex:outputLabel value="{!$ObjectType.SCOrderExternalAssignment__c.fields.WishedDate__c.label}"/>
                    <apex:inputField value="{!extAssignment.WishedDate__c}"/>
                </apex:pageBlockSectionItem>
            
                <!-- Description -->
                <apex:inputField value="{!extAssignment.Description__c}"/>
            
            </apex:pageBlockSection> 
            
        </apex:pageBlockSection> 
        
        <!-- Vendor Contract Services -->
        <apex:pageBlockSection id="allServices" columns="1" rendered="{!IF(currentPage = 'first',true,false)}" >

            <apex:pageBlockSectionItem id="servicesBlock" dataStyleClass="serviceClass">
                <apex:outputLabel value="{!$ObjectType.SCVendorContractService__c.labelPlural}"/>
                <apex:outputPanel >
                    
                    <apex:pageBlockTable value="{!services}" var="s" style="{!IF(services.size > 0, '', 'display:none;')}width:90%;">
                        <apex:column headerValue="" width="5%">
                            <apex:facet name="header">
                                <apex:inputCheckbox onclick="selectAll(this);"/>
                            </apex:facet>
                            <apex:inputField value="{!s.IsSelected__c}" id="isSelectedCheckbox" styleClass=""/>
                        </apex:column>
                        <apex:column value="{!s.ServiceNumber__c}" width="25%"/>
                        <apex:column value="{!s.Name}" width="25%"/>
                        <apex:column headerValue="{!$ObjectType.SCOrderExternalAssignmentItem__c.fields.Qty__c.label}" width="15%">
                            <apex:outputField value="{!s.Qty__c}" id="serviceQuantity" />
                            <!--<apex:inputField value="{!s.Qty__c}" onblur="checkNumber(this,'{!$Component.isSelectedCheckbox}');" style="width:50px;float:left;margin: 3px 3px 0 0;" id="serviceQuantity"/>
                            <apex:outputPanel style="float:left;margin: 3px 0 0 0;">
                                <apex:outputPanel layout="inline" styleClass="arrowUp"   onmousedown="manipulateValue('plus','none','{!$Component.serviceQuantity}','null','{!$Component.isSelectedCheckbox}')"/>
                                <apex:outputPanel layout="inline" styleClass="arrowDown" onmousedown="manipulateValue('minus','none','{!$Component.serviceQuantity}','null','{!$Component.isSelectedCheckbox}')"/>    
                            </apex:outputPanel>-->
                        </apex:column>
                        <apex:column value="{!s.Unit__c}" width="15%"/>
                        <apex:column style="text-align:right;" width="15%">
                            <apex:facet name="header">
                                <apex:outputText value="{!$ObjectType.SCVendorContractService__c.fields.GrossPrice__c.label}" 
                                                 style="display:block;width:100%;text-align:right;"/>
                            </apex:facet>
                            <apex:outputField value="{!s.GrossPrice__c}"/>
                        </apex:column>
                    </apex:pageBlockTable>
                    <apex:outputLabel value="{!$Label.SC_app_NoServices}" style="display:{!IF(!ServicesDefined,'block','none')};" id="noService" />
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
            
            <!-- Extra Services -->
            <apex:variable value="{!0}" var="extraServiceRowNumber" />
            <apex:pageBlockSectionItem id="extraServicesBlock" dataStyleClass="serviceClass" >
                <apex:outputPanel id="panelWithVar">
                    <apex:outputLabel value="Additional Services"/>
                    <apex:variable value="{!0}" var="extraServiceRowNumber" />
                </apex:outputPanel>
                <apex:outputPanel >
                    <apex:pageBlockTable value="{!extraServices}" var="s" id="allExtraServices" style="{!IF(extraServices.size > 0, '', 'display:none;')}width:90%;margin:0 0 10px 0;">
                        <apex:column width="5%">
                            <apex:commandButton action="{!removeExtraService}" value=" X " reRender="allExtraServices,panelWithVar">
                                <apex:param name="p1" value="{!extraServiceRowNumber}" assignTo="{!selectedExtraServiceToDelete}"/>
                            </apex:commandButton>
                        </apex:column>
                        <apex:column headerValue="{!$ObjectType.SCOrderExternalAssignmentItem__c.fields.Description__c.label}" width="50%">
                            <apex:inputField value="{!s.Description__c}" style="height:15px;width:95%;"/>
                        </apex:column>
                        <apex:column headerValue="{!$ObjectType.SCOrderExternalAssignmentItem__c.fields.Qty__c.label}" width="15%">
                            <apex:inputField value="{!s.Qty__c}" onblur="checkNumber(this);" style="width:50px;float:left;margin: 3px 3px 0 0;" id="extraServiceQuantity" />
                            <apex:outputPanel style="float:left;margin: 3px 0 0 0;">
                                <apex:outputPanel layout="inline" styleClass="arrowUp"   onmousedown="manipulateValue('plus','none','{!$Component.extraServiceQuantity}','null','')"/>
                                <apex:outputPanel layout="inline" styleClass="arrowDown" onmousedown="manipulateValue('minus','none','{!$Component.extraServiceQuantity}','null','')"/>    
                            </apex:outputPanel>
                        </apex:column>
                        <apex:column headerValue="{!$ObjectType.SCOrderExternalAssignmentItem__c.fields.Unit__c.label}" width="15%">
                            <apex:inputField value="{!s.Unit__c}" required="true"/>
                        </apex:column>
                        <apex:column style="text-align:right;" width="15%">
                            <apex:facet name="header">
                                <apex:outputText value="{!$ObjectType.SCOrderExternalAssignmentItem__c.fields.Price__c.label}" 
                                                 style="display:block;width:100%;text-align:right;"/>
                            </apex:facet>
                            <div class="requiredInput" style="width:90%;float:right;">
                                <div class="requiredBlock"></div>
                                <apex:inputField value="{!s.Price__c}" style="text-align:right;width:90%;"/>
                            </div>
                            <apex:variable var="extraServiceRowNumber" value="{!extraServiceRowNumber + 1}" />
                        </apex:column>
                    </apex:pageBlockTable>
                    
                    <apex:outputPanel layout="block" style="margin:0 0 10px 0;" id="extraServicesPanel">
                        <apex:commandButton value="{!$Label.SC_btn_Add}" style="width:100px;margin:0 5px 0 0" action="{!addExtraService}" reRender="allExtraServices,panelWithVar"/>
                        <apex:selectList value="{!selectedNumberExtraServicesToAdd}" size="1" multiselect="false" style="width:40px;">
                            <apex:selectOptions value="{!NumberOfExtraServicesToAdd}" />
                        </apex:selectList>
                    </apex:outputPanel>
                    
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
        
        </apex:pageBlockSection>
        
        
        <!-- Second (info) Section -->
        <apex:pageBlockSection columns="2" rendered="{!IF(currentPage = 'second',true,false)}">
               
            <!-- Left Section -->
            <apex:pageBlockSection columns="1">
        
                <!-- Order -->
                <apex:outputField value="{!extAssignment.Order__c}" rendered="{!ISBLANK($CurrentPage.parameters.oid) || $CurrentPage.parameters.oid = ''}"/>
                           
                <!-- Vendor -->
                <apex:pageBlockSectionItem labelStyle="width:35%;" dataStyle="width:65%;">
                    <apex:outputLabel value="{!$ObjectType.SCVendor__c.label}" />
                    <apex:outputField value="{!extAssignment.Vendor__c}"/>
                </apex:pageBlockSectionItem>
                
                <!-- Vendor Contracts -->
                <apex:outputField value="{!extAssignment.VendorContract__c}"/>
                
                <!-- Vendor Contract Items -->
                <apex:outputField value="{!extAssignment.VendorContractItem__c}"/>
         
            </apex:pageBlockSection> 
            
            <!-- Right Section -->
            <apex:pageBlockSection columns="1">
            
                <!-- Wished Date -->
                <apex:pageBlockSectionItem labelStyle="width:35%;" dataStyle="width:65%;">
                    <apex:outputLabel value="{!$ObjectType.SCOrderExternalAssignment__c.fields.WishedDate__c.label}" />
                    <apex:outputField value="{!extAssignment.WishedDate__c}"/>
                </apex:pageBlockSectionItem>
            
                <!-- Description -->
                <apex:outputField value="{!extAssignment.Description__c}"/>
            
            </apex:pageBlockSection> 
        
        </apex:pageBlockSection> 
        
        <!-- Services -->
        <apex:pageBlockSection columns="1" rendered="{!IF(currentPage = 'second',true,false)}">
        
            <!-- Vendor Contract Services -->
            <apex:pageBlockSectionItem id="servicesBlock" dataStyleClass="serviceClass">
                <apex:outputLabel value="{!$ObjectType.SCVendorContractService__c.labelPlural}"/>
                <apex:outputPanel >
                    <apex:pageBlockTable value="{!ServicesSelected}" var="s" id="allServices" rendered="{!ServicesDefined}" style="width:90%;">
                        <apex:column value="{!s.ServiceNumber__c}" width="30%"/>
                        <apex:column value="{!s.name}" width="30%"/>
                        <apex:column value="{!s.Qty__c}" width="10%"/>
                        <apex:column value="{!s.Unit__c}" width="10%"/>

                        <apex:column style="text-align:right;" width="10%">
                            <apex:facet name="header">
                                <apex:outputText value="{!$ObjectType.SCVendorContractService__c.fields.GrossPrice__c.label}" 
                                                 style="display:block;width:100%;text-align:right;"/>
                            </apex:facet>
                            <apex:outputField value="{!s.GrossPrice__c}"/>

                            <apex:variable var="sum" value="{!sum + s.GrossPrice__c * s.Qty__c }"/>
                            <apex:facet name="footer">
                                <apex:outputPanel layout="block" style="width:100%;text-align:right;">
                                    <apex:outputText value="{!$Label.SC_app_Sum}"/>
                                </apex:outputPanel>
                            </apex:facet>

                        </apex:column>
                        <apex:column style="text-align:right;" rendered="{!s.IsSelected__c}" width="10%">
                            <apex:facet name="header">
                                <apex:outputText value="{!$Label.SC_app_Sum}" 
                                                 style="display:block;width:100%;text-align:right;"/>
                            </apex:facet>
                            <apex:outputText value="{!s.GrossPrice__c * s.Qty__c }"/>
                            <apex:facet name="footer">
                                <apex:outputText id="sumOutput" value="{!sum}" style="display:block;width:100%;text-align:right;"/>
                            </apex:facet>
                        </apex:column>

                    </apex:pageBlockTable>
                    <apex:outputText value="{!$Label.SC_app_NoServices}" rendered="{!!ServicesDefined}" id="noService" />
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
            
            <br/>
            
            <!-- Extra Services -->
            <apex:pageBlockSectionItem id="extraServicesBlockOutput" dataStyleClass="serviceClass" >
                <apex:outputPanel >
                    <apex:outputLabel value="Extra Services"/>
                </apex:outputPanel>
                <apex:outputPanel >
                    <apex:pageBlockTable value="{!extraServices}" var="s" id="allExtraServicesOutput" style="{!IF(extraServices.size > 0, '', 'display:none;')}width:90%;margin:0 0 10px 0;">
                        <apex:column headerValue="{!$ObjectType.SCOrderExternalAssignmentItem__c.fields.Description__c.label}"
                                     width="60%">
                            <apex:outputField value="{!s.Description__c}" style="height:15px;width:95%;" rendered="{!s.Price__c != null}"/>
                        </apex:column>
                        <apex:column headerValue="{!$ObjectType.SCOrderExternalAssignmentItem__c.fields.Qty__c.label}"
                                     width="10%">
                            <apex:outputField value="{!s.Qty__c}" style="width:15%;" rendered="{!s.Price__c != null}"/>
                        </apex:column>
                        <apex:column headerValue="{!$ObjectType.SCOrderExternalAssignmentItem__c.fields.Unit__c.label}"
                                     width="10%">
                            <apex:variable var="sum2" value="{!sum2 + s.Price__c * s.Qty__c }"/>
                            <apex:outputField value="{!s.Unit__c}" rendered="{!s.Price__c != null}"/>
                        </apex:column>
                        <apex:column width="10%" style="text-align:right;">
                            <apex:facet name="header">
                                <apex:outputText value="{!$ObjectType.SCOrderExternalAssignmentItem__c.fields.Price__c.label}" 
                                                 style="display:block;width:100%;text-align:right;"/>
                            </apex:facet>
                            <apex:outputField value="{!s.Price__c}" style="text-align:right;" rendered="{!s.Price__c != null}"/>
                            <apex:facet name="footer">
                                <apex:outputText value="{!$Label.SC_app_Sum}" 
                                                 style="display:block;width:100%;text-align:right;"/>
                            </apex:facet>
                        </apex:column>
                        <apex:column style="text-align:right;" 
                                     width="10%">
                            <apex:facet name="header">
                                <apex:outputText value="{!$Label.SC_app_Sum}" 
                                                 style="display:block;width:100%;text-align:right;"/>
                            </apex:facet>
                            <apex:outputText value="{!s.Price__c * s.Qty__c }" rendered="{!s.Price__c != null}"/>
                            <apex:facet name="footer">
                                <apex:outputText id="extraSumOutput" value="{!sum2}" style="display:block;width:100%;text-align:right;"/>
                            </apex:facet>
                        </apex:column>
                    </apex:pageBlockTable>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
            
        </apex:pageBlockSection>
        
    </apex:pageBlock>
    </apex:outputPanel>
    
    </apex:form>

</apex:page>
