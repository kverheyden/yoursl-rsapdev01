<!--
 * @(#)SCInterfaceClearing.page
 * 
 * Copyright 2013 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.

/**

 * @author Eugen Tiessen <etiessen@gms-online.de>
 * @version $Revision$, $Date$
 */
 
-->
<apex:page controller="SCInterfaceClearingController" tabStyle="SCInterfaceClearing__c">
    <style>
        .searching
            {
                background:url(/img/loading.gif) no-repeat 0 0; 
                padding-left:20px; 
                margin-left:10px; 
                padding-bottom:5px; 
                font-size:12px;
                
            }
    </style>
    <apex:form >
<!--        Placeholder for error messages -->
        <apex:outputPanel id="topErrorMessages">
            <apex:pageMessages rendered="{!showTopErrorMessage}" showDetail="true"/>
        </apex:outputPanel>
        <apex:pageBlock id="clearingItems" title="{! $ObjectType.SCInterfaceClearing__c.label}">
            <apex:pageBlockButtons >
                <apex:commandButton action="{!onDeleteAll}" 
                    value="Alle löschen"
                    reRender="clearingItemsTable,pagination,errorMessages, topErrorMessages" 
                    onclick="if(!confirm('Möchten Sie wirklich alle Elemente löschen ?'))return false;"
                />
            </apex:pageBlockButtons>
			<apex:pageBlockSection title="Filter"
				columns="4" id="clearingFilter"
			>
				<apex:outputPanel >
					<apex:outputLabel for="selectedTypes" value="{! $ObjectType.SCInterfaceClearing__c.fields.Type__c.label }: " />
					<apex:selectList id="selectedTypes" value="{!selectedTypes}" size="3" onchange="changeStatusSelection();" multiselect="true">
			            <apex:selectOptions value="{!typeOptionList}"/>
			        </apex:selectList>
			        
				</apex:outputPanel>  
				
				<apex:outputPanel >
					<apex:outputLabel for="dateFrom" value="{!$Label.SC_app_From}: " />
					<apex:inputField id="dateFrom" value="{!dummyObject4InputFields.Price_Valid_From__c}"  onchange="changeStatusSelection();"/>
					<BR />
					<apex:outputLabel for="dateTo" value="{!$Label.SC_app_To}: " />
					<apex:inputField id="dateTo" value="{!dummyObject4InputFields.Price_Valid_To__c}"  onchange="changeStatusSelection();"/>
				</apex:outputPanel>
				
				<apex:outputPanel >
					<apex:outputLabel for="selectedResource" value="{! $ObjectType.SCInterfaceClearing__c.fields.Resource__c.label }: " />
					<apex:inputField id="selectedResource" value="{!dummyObject4Resource.Resource__c}"  onchange="changeStatusSelection();"/>
					<BR />
					<apex:outputLabel for="selectedOrder" value="{! $ObjectType.SCInterfaceClearing__c.fields.Order__c.label }: " />
					<apex:inputField id="selectedOrder" value="{!dummyObject4InputFields.OrderOrigin__c}" onchange="changeStatusSelection();"/>
				</apex:outputPanel>  
				      
			</apex:pageBlockSection>
			<BR /><BR />
			<script>
		        twistSection(document.getElementById('{!$Component.clearingFilter}').getElementsByTagName('img')[0])
		    </script>
            <apex:outputPanel >
				{! $ObjectType.SCInterfaceClearing__c.fields.Status__c.label }: 
                <apex:selectList value="{!selectedStatus}" size="1" onchange="changeStatusSelection();">
                    <apex:selectOptions value="{!statusOptionList}"/>
                </apex:selectList>
                 
            </apex:outputPanel>  
            <apex:actionFunction name="changeStatusSelection" 
               	action="{!onChangeStatusSelection}" 
				rerender="clearingItemsTable,pagination,editSelection" 
               	status="changeStatus"
            />  
            
            <apex:actionStatus id="changeStatus">
                <apex:facet name="start">
                        <apex:outputText value="" styleClass="searching"/>
                    </apex:facet>
                </apex:actionStatus>
                
            <apex:pageBlockTable value="{! clearingList }" var="item" id="clearingItemsTable">
                 <apex:column headerValue="{!$Label.SC_app_Action}">
                    
                    <apex:actionStatus id="statusValidateClearing">
                        <apex:facet name="start">
                            <apex:outputText value="" styleClass="searching"/>
                        </apex:facet>
                        <apex:facet name="stop">
                            <apex:commandButton action="{!onValidate}"
                                reRender="editSelection, topErrorMessages"
                                value="Überprüfen"
                                status="statusValidateClearing"
                                oncomplete="location.href = '#selectedObjectAnchor'"
                            >
                                <apex:param name="itemId" 
                                    assignTo="{!selectedClearingItemId}" 
                                    value="{!item.Id}" 
                                />
                            </apex:commandButton>
                        </apex:facet>
                    </apex:actionStatus>
                    
                    <apex:actionStatus id="statusDeleteClearing">
                        <apex:facet name="start">
                            <apex:outputText value="" styleClass="searching"/>
                        </apex:facet>
                        <apex:facet name="stop">
                           <apex:commandButton action="{!onDelete}" 
                                value="{!$Label.SC_btn_Delete}"
                                reRender="clearingItemsTable, pagination, editSelection, topErrorMessages"
                                status="statusDeleteClearing"
                                onclick="if(!confirm('Möchten Sie wirklich den Eintrag {!item.Name} löschen ? '))return false;" 
                            >
                                <apex:param name="itemId" 
                                    assignTo="{!selectedClearingItemId}" 
                                    value="{!item.Id}" 
                                />
                            </apex:commandButton>
                        </apex:facet>
                    </apex:actionStatus>
                
<!--                    <apex:commandButton action="{!onValidate}" -->
<!--                        reRender="editSelection, topErrorMessages" -->
<!--                        value="Validate" -->
<!--                        status="actionStatus" -->
<!--                        oncomplete="location.href = '#selectedObjectAnchor'" -->
<!--                    > -->
<!--                        <apex:param name="itemId"  -->
<!--                            assignTo="{!selectedClearingItemId}"  -->
<!--                            value="{!item.Id}"  -->
<!--                        /> -->
<!--                    </apex:commandButton> -->
<!--                    <apex:commandButton action="{!onSelectClearingItem}" -->
<!--                        reRender="editSelection" -->
<!--                        value="{!$Label.SC_btn_Edit}" -->
<!--                        status="actionStatus" -->
<!--                        oncomplete="location.href = '#selectedObjectAnchor'" -->
<!--                    > -->
<!--                        <apex:param name="itemId"  -->
<!--                            assignTo="{!selectedClearingItemId}"  -->
<!--                            value="{!item.Id}"  -->
<!--                        /> -->
<!--                    </apex:commandButton> -->
<!--                    <apex:commandButton action="{!onDelete}"  -->
<!--                        value="{!$Label.SC_btn_Delete}" -->
<!--                        reRender="clearingItemsTable, pagination, editSelection, topErrorMessages" -->
<!--                        status="actionStatus" -->
<!--                        onclick="if(!confirm('Do your really want to delete {!item.Name}'))return false;"  -->
<!--                    > -->
<!--                        <apex:param name="itemId"  -->
<!--                            assignTo="{!selectedClearingItemId}"  -->
<!--                            value="{!item.Id}"  -->
<!--                        /> -->
<!--                    </apex:commandButton> -->
<!--                    <apex:actionStatus startText="" startStyleClass="searching" id="actionStatus"/> -->
                </apex:column>
                <apex:column value="{!item.Name}"/>
                <apex:column value="{!item.Status__c}"/> 
                <apex:column value="{!item.Type__c}"/>
                <apex:column value="{!item.Order__c}"/>
                <apex:column value="{!item.Attachments.size}" headerValue="Attachments"/>
                <apex:column value="{!item.Resource__c}"/> 
                <apex:column value="{!item.ResultInfo__c}" width="30%"/> 
                <apex:column value="{!item.ResultCode__c}"/>
                <apex:column value="{!item.LastModifiedDate}"/>
                <apex:column value="{!item.CreatedDate}"/>
               
            </apex:pageBlockTable>
            <c:SCPagination id="pagination" setController="{!clearingController}" pageRerender="clearingItemsTable" />
            
        </apex:pageBlock>
    </apex:form>
    <apex:form id="editSelection">
        <a name="selectedObjectAnchor" />
        <apex:pageBlock title="Selected Object"
            rendered="{! NOT(ISNULL(selectedClearingItem))}"
            id="selectedObject"
        >
            <apex:pageBlockButtons location="top">
                <apex:actionStatus id="statusSave">
                    <apex:facet name="start">
                        <apex:outputText value="" styleClass="searching"/>
                    </apex:facet>
                    <apex:facet name="stop">
                        <apex:commandButton action="{!onSave}" 
                            value="{!$Label.SC_btn_Save}"
                            reRender="clearingItemsTable, editSelection, topErrorMessages" 
                            status="statusSave"
                            rendered="{!isValid}"
                        />
                    </apex:facet>
                </apex:actionStatus>
                
                <apex:commandButton action="{!onValidate}"
                    reRender="editSelection, topErrorMessages"
                    value="Validate"
                    status="actionStatus2"
                    oncomplete="location.href = '#selectedObjectAnchor'"
                />
                <apex:commandButton action="{!onCancel}" 
                    value="{!$Label.SC_btn_Cancel}"
                    reRender="editSelection, clearingItemsTable, topErrorMessages"
                    status="actionStatus2" 
                />
                
                <apex:actionStatus startText="" startStyleClass="searching" id="actionStatus2"/>
            </apex:pageBlockButtons>
            
            <apex:pageMessages id="errorMessages"/>
            
            <apex:pageBlockSection >
                <apex:outputField value="{!selectedClearingItem.Name}"/>
                <apex:outputField value="{!selectedClearingItem.Status__c}"/> 
                <apex:outputField value="{!selectedClearingItem.Type__c}"/>
                <apex:outputField value="{!selectedClearingItem.Resource__c}"/> 
                <apex:outputField value="{!selectedClearingItem.ResultInfo__c}"/> 
                <apex:outputField value="{!selectedClearingItem.ResultCode__c}"/>
                <apex:inputField value="{!selectedClearingItem.Order__c}"/> 
                <apex:inputField value="{!selectedClearingItem.MaterialMovement__c}"/>
                <apex:outputField value="{!selectedClearingItem.LastModifiedDate}"/> 
                <apex:outputField value="{!selectedClearingItem.LastModifiedById}"/> 
                <apex:outputField value="{!selectedClearingItem.CreatedDate}"/>
            </apex:pageBlockSection>
            
            
            
            <apex:pageBlockSection rendered="{! NOT(ISNULL(attachmentList))}"
                title="Changes"
                columns="1"
            >
            
                <apex:pageBlockTable value="{! attachmentList }" 
                    var="item" 
                    
                    title="Changes"
                >
                    <apex:column headerValue="Action">
                        <!-- <apex:commandButton onclick="window.open('/apex/SCInterfaceClearingAttachment?id={!item.Id}','objectWindow','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no');return false;"
                            value="{!$Label.SC_btn_Edit}" rendered="{!CONTAINS(supportedObjects, item.name)}"
                        />
                        <apex:commandButton onclick="window.open('/servlet/servlet.FileDownload?file={!item.Id}','objectWindow','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no');return false;"
                            value="{!$Label.SC_btn_View}" rendered="{!NOT(CONTAINS(supportedObjects, item.name))}"
                        /> -->
                        <apex:commandButton onclick="window.open('/apex/SCInterfaceClearingAttachment?id={!item.Id}','objectWindow','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no');return false;"
                            value="{!$Label.SC_btn_Edit}" rendered="{!NOT(BEGINS(item.name, '$'))}"
                        />
                        <apex:commandButton onclick="window.open('/servlet/servlet.FileDownload?file={!item.Id}','objectWindow','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no');return false;"
                            value="{!$Label.SC_btn_View}" rendered="{!BEGINS(item.name, '$')}"
                        />
                        
                        <apex:actionStatus id="statusDelete">
                            <apex:facet name="start">
                                <apex:outputText value="" styleClass="searching"/>
                            </apex:facet>
                            <apex:facet name="stop">
                                <apex:commandButton value="{!$Label.SC_btn_Delete}"
                                    action="{!onDeleteAttachment}" 
                                    reRender="editSelection, topErrorMessages"
                                    onclick="if(!confirm('Möchten Sie wirklich den Eintrag {!item.Name} löschen ?')) return false;"
                                    oncomplete="location.href = '#selectedObjectAnchor'"
                                    status="statusDelete"  
                                >
                                    <apex:param name="attToDeleteId" 
                                        assignTo="{!attToDeleteId}" 
                                        value="{!item.Id}" 
                                    />
                                </apex:commandButton>
                            </apex:facet>
                        </apex:actionStatus>
                        
                        
                    </apex:column>
                    <apex:column headerValue="Valid" style="text-align:center;">
                        <apex:image value="/img/msg_icons/confirm16.png" rendered="{!attValidationMap[item.Id]}" />
                        <apex:image value="/img/msg_icons/error16.png" rendered="{!NOT(attValidationMap[item.Id])}" />
                    </apex:column>
                    <apex:column headerValue="Object" value="{!item.Name}"/>
                    <apex:column headerValue="Reference" >
                        <apex:outputLink target="_blank" 
                            value="/{!boClearing.objMap[item.id].Id}"
                            rendered="{!NOT(ISNULL(boClearing.objMap[item.id].Id))}"
                        >
                            <apex:outputText value="{!boClearing.objMap[item.id].Name}" 
                                rendered="{!NOT(ISNULL(boClearing.objMap[item.id].Name))}"
                            />
                            <apex:outputText value="Link" 
                                rendered="{!ISNULL(boClearing.objMap[item.id].Name)}"
                            />
                        </apex:outputLink>
                        
                    </apex:column>
                    <apex:column headerValue="Activity" value="{!item.Description}"/>
                    <apex:column headerValue="Size (Bytes)" value="{!item.BodyLength}"/> 
                    <apex:column value="{!item.LastModifiedById}"/>
                    <apex:column value="{!item.LastModifiedDate}"/> 
                    
                </apex:pageBlockTable>
            </apex:pageBlockSection>
            
            <br />
            
            <apex:pageBlockSection rendered="{! NOT(ISNULL(noteList))}"
                title="Info"
                columns="1"
                id="notes"
            >
                <apex:pageBlockTable value="{! noteList }" var="item">
                    <apex:column headerValue="Type" value="{!item.Title}"/>
                    <apex:column headerValue="Description" value="{!item.Body}"/>
                    <apex:column value="{!item.CreatedById}"/>
                    <apex:column value="{!item.CreatedDate}" /> 
                
                </apex:pageBlockTable>
            </apex:pageBlockSection>
        </apex:pageBlock>
        <apex:actionFunction action="{!onRefreshSelection}" 
            name="refreshSelection" 
            reRender="editSelection, topErrorMessages"
        />

    </apex:form>
</apex:page>
