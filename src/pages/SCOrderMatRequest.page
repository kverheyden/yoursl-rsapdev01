<!--
 * @(#)SCOrderMatRequest.page
 * 
 * Copyright 2010 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
-->
<apex:page controller="SCOrderMaterialController" 
    showHeader="false"
    title="{!$Label.SC_app_MaterialRequest}" 
    tabStyle="SCStock__c">
    
    <!-- ############# JS ################## -->
    <apex:stylesheet value="{!URLFOR($Resource.scjquery,'jquery/css/cupertino/jquery-ui-1.8.4.custom.css')}" />
    <apex:includeScript value="{!URLFOR($Resource.scjquery,'jquery/js/jquery-1.4.2.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.scjquery,'jquery/js/jquery-ui-1.8.4.custom.min.js')}" />

<script type="text/javascript" language="JavaScript">

var $dialog = 0;
var errMsg = '{!errMsg}';

$(document).ready(function()
{
    $dialog = $("<div></div>").dialog({
        autoOpen: false,
        resizable: false,
        height: 400,
        width: 400, 
        modal: true,
        buttons: 
        {
            "{!$Label.SC_btn_Yes}": function()
            {
                requestMat();
                $(this).dialog("close");
            }, 
            "{!$Label.SC_btn_No}": function()
            {
                $(this).dialog("close");
            }
        }
    });
    
    if (errMsg.length > 0)
    {
        showDialog(errMsg, '');
    }
});

function showDialog(message, title)
{
    $dialog.html(message);
    $dialog.dialog("option", "title", title);
    $dialog.dialog('open');
}

</script>

    <div style="height:2px;" />


    <apex:form id="mainform">
        <apex:actionFunction name="requestMat" action="{!requestMat}" />

        <apex:panelGroup rendered="{!NOT(assignmentOk)}">
            <script>
                alert('{!$Label.SC_msg_ReplenishmentNoAssignment}');
            </script>
        </apex:panelGroup>
        <apex:panelGroup rendered="{!requestOk}">
            <script>
                parent.window.opener.refreshData();
                window.close();
            </script>
        </apex:panelGroup>
        
        <apex:pageBlock >
            <apex:sectionHeader title="{!$Label.SC_app_MaterialRequest}" id="header" />
            
            <apex:pageMessages />

            <apex:pageBlockSection columns="1">
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$Label.SC_app_Origin}" />
                    <apex:selectList value="{!sourceStock}" size="1" multiselect="false">
                        <apex:selectOptions value="{!sourceList}"/>
                    </apex:selectList>
                </apex:pageBlockSectionItem>

                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$Label.SC_app_Receiver}" />
                    <apex:selectList value="{!receiverStock}" size="1" multiselect="false">
                        <apex:selectOptions value="{!receiverList}"/>
                    </apex:selectList>
                </apex:pageBlockSectionItem>

                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$ObjectType.SCMaterialMovement__c.fields.Type__c.label}" />
                    <apex:selectList value="{!matMoveType}" size="1" multiselect="false">
                        <apex:selectOptions value="{!typeList}"/>
                    </apex:selectList>
                </apex:pageBlockSectionItem>
                
                <apex:inputField value="{!matMove.ReplenishmentType__c}" rendered="{!ShowReplensihmentType}"/>

                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$Label.SC_app_RequireDate}" />
                    <apex:outputField value="{!dates.Start__c}"/>
                </apex:pageBlockSectionItem>

                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$Label.SC_app_RequestDate}" />
                    <apex:outputField value="{!dates.End__c}" />
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:pageBlock>
        
        <apex:pageBlock >
            <apex:pageBlockButtons location="bottom" >
                <!-- apex:commandButton action="{!selectAll}" value="{!$Label.SC_btn_SelectAll}" 
                                    disabled="{!NOT(hasArticles)}" rendered="{!NOT(isSelected)}"/ -->
                <!-- apex:commandButton action="{!selectAll}" value="{!$Label.SC_btn_DeselectAll}" 
                                    disabled="{!NOT(hasArticles)}" rendered="{!isSelected}"/ -->
                 <apex:actionStatus id="checkAndBook">
                 <apex:facet name="stop">
                     <apex:commandButton id="buttonOk"
                                         value="       {!$Label.SC_btn_Order}        " 
                                         action="{!checkAndRequestMat}" 
                                         rerender="mainform"
                                         status="checkAndBook"
                                         style="width:100px;" 
                                         disabled="{!NOT(hasArticles)}"
                                         />
                 </apex:facet>
                 <apex:facet name="start"> 
                     <apex:outputPanel >
                           <apex:image value="/img/loading32.gif" style="height: 15px;"/>
                           <apex:commandButton value="{!$Label.SC_app_Processing}" 
                                               status="checkAndBook"
                                               disabled="true"
                                               style="width:100px;"/>
                     </apex:outputPanel>
                 </apex:facet>
                </apex:actionStatus>      
               
                <!--apex:commandButton action="{!checkAndRequestMat}" value="   {!$Label.SC_btn_Order}   " 
                                    disabled="{!NOT(hasArticles)}" /-->
                <apex:commandButton action="javascript:window.close()" value="{!$Label.SC_btn_Cancel}" immediate="true"/>
            </apex:pageBlockButtons>

            <apex:pageBlockSection columns="1" >
                <apex:pageBlockTable value="{!artInfoList}" var="item">
                    <apex:column >
                        <apex:facet name="header"></apex:facet>
                        <apex:inputCheckbox value="{!item.isSet}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCArticle__c.fields.Name.label}</apex:facet>
                        <apex:outputText value="{!item.article.Name}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCArticle__c.fields.ArticleNameCalc__c.label}</apex:facet>
                        <apex:outputText value="{!item.article.ArticleNameCalc__c}" />
                    </apex:column>
                    <!-- SAP calculate the VType -->
                    <!--apex:column rendered="{!ValuationTypeActivated}"-->
                        <!--apex:facet name="header"--><!--{!$ObjectType.SCStockItem__c.fields.ValuationType__c.label}--><!--/apex:facet-->
                        <!--apex:selectList value="{!item.ValuationType}" size="1" rendered="{!AND(hasValTypeToBeSelected, NOT(item.existValType),item.hasArticleValType, item.isSet)}"-->
                                                                  <!-- required="{!AND(hasValTypeToBeSelected, NOT(item.existValType),item.hasArticleValType, item.isSet, ValuationTypeActivated)}" -->
                            <!--apex:selectOptions value="{!ValuationTypeOptions}"/-->
                        <!--/apex:selectList-->
                        <!--apex:outputText value="{!item.ValuationType}" rendered="{!OR(NOT(hasValTypeToBeSelected), item.existValType,NOT(item.hasArticleValType),NOT(item.isSet))}"/-->
                    <!--/apex:column-->
                    <apex:column styleClass="r_align">
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.Qty__c.label}</apex:facet>
                        <apex:outputText value="{0, number}" >
                            <apex:param value="{!item.qty}" />
                        </apex:outputText>
                    </apex:column>
                </apex:pageBlockTable> 
            </apex:pageBlockSection>   
        </apex:pageBlock>
    </apex:form>
</apex:page>
