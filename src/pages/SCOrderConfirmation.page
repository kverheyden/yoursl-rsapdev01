<!--
/*
 * @(#)SCOrderConfirmation.page
 *
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * The page displays the order details after recording the order data.
 *
 * @author Dietrich Herdt  <dherdt@gms-online.de>
 * @version $Revision$, $Date$
 */
-->

<apex:page showHeader="{!showPageHeader}" sidebar="{!showPageHeader}" tabStyle="SCOrder__c" id="orderConfirmationPage"
            title="Order Confirmation" controller="SCOrderConfirmationController"  >

<apex:stylesheet value="{!URLFOR($Resource.SCRes,'lib/jquery/css/cupertino/jquery-ui-1.8.4.custom.css')}" />
<apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/jquery-1.4.2.min.js')}" />
<apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/jquery-ui-1.8.4.custom.min.js')}" />


<style>
.searching
{
    background:url(/img/loading.gif) no-repeat 0 0; 
    padding-left:20px; 
    margin-left:10px; 
    padding-bottom:5px; 
    font-size:12px;
}
</style>


    <apex:form >
    
        <apex:actionPoller reRender="pageBlockOrderShort" enabled="{!IF(appFound == false && reload == 'true', true, false)}" interval="5" action="{!onInit}" status="dlyStatus" />
        
        <apex:pageBlock title="{!$ObjectType.SCOrder__c.fields.Name.label}: {!ord.order.Name}"  mode="edit" id="pageBlockOrderShort">

        <apex:pageBlockSection rendered="{!$Setup.SCApplicationSettings__c.DEFAULT_COUNTRY__c != 'GB'}">
        
                <apex:outputField value="{!ord.order.Type__c}"  />
                <apex:outputField value="{!ord.order.Brand__c}"  />
                
                <apex:outputField value="{!ord.order.Status__c}" />
                <apex:outputField value="{!ord.order.InvoicingType__c}"  />
                
                <apex:outputField value="{!ord.order.PriceList__c}"  />
                <apex:outputField value="{!ord.order.CustomerPriority__c}"  />
                
                
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$ObjectType.SCOrder__c.fields.CustomerTimewindow__c.label}" style="Start" styleClass="Start"/>
                    <apex:panelGroup layout="none">
                        <apex:outputField value="{!ord.order.CustomerPrefStart__c}" />&nbsp;-&nbsp;
                        <apex:outputField value="{!ord.order.CustomerPrefEnd__c}" />,&nbsp;
                        <apex:outputField value="{!ord.order.CustomerTimewindow__c}" />
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
                
                
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$Label.SC_app_ProductSearchBox}"/>
                    <apex:outputText value="{!product}" rendered="{!IF(product != null,true, false)}" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$ObjectType.SCOrder__c.fields.FailureType__c.label}" />
                    <apex:outputField value="{!ord.order.FailureType__c}" rendered="{!IF(ord.order.FailureType__c != null,true, false)}"/>
                </apex:pageBlockSectionItem>
                
                <!--apex:pageBlockSectionItem -->
                    <!--apex:outputLabel value="{!$ObjectType.SCOrder__c.fields.Description__c.label + ' (' + $ObjectType.SCOrder__c.label + ')'}" / -->
                    <!--apex:outputField value="{!ord.order.Description__c}" rendered="{!IF(ord.order.Description__c != null,true, false)}"/ -->
                <!--/apex:pageBlockSectionItem -->
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$ObjectType.SCOrder__c.fields.ERPLongtext__c.label + ' (' + $ObjectType.SCOrder__c.label + ')'}" />
                    <apex:outputField value="{!ord.order.ERPLongtext__c}" rendered="{!IF(ord.order.ERPLongtext__c != null,true, false)}"/>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$ObjectType.SCInstalledBase__c.fields.ProductSkill__c.label}" />
                    <apex:outputText value="{!productSkill}" rendered="{!IF(productSkill == '' || productSkill == null, false, true )}"/>
                </apex:pageBlockSectionItem>
                
                <!--apex:pageBlockSectionItem -->
                    <!--apex:outputLabel value="{!$ObjectType.SCOrder__c.fields.Description__c.label + ' (' + $ObjectType.SCOrderItem__c.label + ')'}" / -->
                    <!--apex:outputText value="{!IF(ord.boOrderItems.size > 0 ,ord.boOrderItems[0].orderItem.Description__c, '')}" / -->
                <!--/apex:pageBlockSectionItem -->
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$ObjectType.SCOrder__c.fields.DepartmentCurrent__c.label}" />
                    <apex:outputField value="{!ord.order.DepartmentCurrent__c}" />
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem id="employee">
                    <apex:outputLabel value="{!$ObjectType.SCAssignment__c.fields.Employee__c.label}" />
                    <apex:outputText id="employeeField"  styleClass="{!IF(appFound == false && reload == 'true', '','')}" escape="false" rendered="{!IF(appointments == null, false, true)}">
                        <apex:repeat value="{!appointments}" var="app">
                            <apex:outputText value="{!IF(app.Assignment__r.Employee__c == null, '', app.Assignment__r.Employee__c)}"/>
                            (<apex:outputField value="{!app.AssignmentStatus__c}" rendered="{!NOT(ISNULL(app.AssignmentStatus__c))}"/>&nbsp;-&nbsp;
                            <apex:outputField value="{!app.PlanningType__c}" rendered="{!NOT(ISNULL(app.PlanningType__c))}"/>)&nbsp;
                            <apex:outputText value="{!IF(app.Assignment__r.Resource__r.Mobile_txt__c == null, '', app.Assignment__r.Resource__r.Mobile_txt__c)}"/>
                            <apex:outputText value="{!IF(app.Assignment__r.Resource__r.Phone_txt__c == null, '', ', '+app.Assignment__r.Resource__r.Phone_txt__c)}"/>
                            <br/>
                        </apex:repeat>
                    </apex:outputText>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$ObjectType.SCContract__c.label}" />
                    <apex:outputLink value="/{!ord.order.Contract__c}" target="_blank">{!ord.order.Contract__r.util_details__c}</apex:outputLink>
                </apex:pageBlockSectionItem>

                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$ObjectType.SCAppointment__c.fields.Start__c.label}" />
                    <apex:outputText value="{!startDate}"/>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$Label.SC_app_ArrivalTime}" />
                    <apex:outputText value="{!arrivalTime}"/>
                </apex:pageBlockSectionItem>
                
        </apex:pageBlockSection>
        
        <apex:pageBlockSection columns="2" rendered="{!showPageHeader}">
            <apex:outputText >&nbsp;</apex:outputText>
            <apex:outputText >&nbsp;</apex:outputText>
            <apex:commandButton action="{!URLFOR($Page.SCCustomerSearchCCE)}" value="{!$Label.SC_btn_Continue} [s]" accesskey="s" style="width:100%"/>
            <apex:commandButton action="/{!ord.order.Id}" value="{!$Label.SC_btn_View} [o]" accesskey="o" style="width:100%"/>
            <apex:outputText >&nbsp;</apex:outputText>
            <apex:outputText >&nbsp;</apex:outputText>
        </apex:pageblockSection>

        <!-- tmp: to test the interface integration-->
        <c:SCOrderInterfaceMonitor id="monitor" oId="{!ord.order.id}" autopoll="true" showall="false"/>

        </apex:pageBlock>               
    </apex:form>
</apex:page>
