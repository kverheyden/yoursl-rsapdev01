<!--
 * @(#)SCAddContractItem.page
 * 
 * Copyright 2011 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This page shows a list of contract items for the actual contract.
 * The user can save new items or return to the contract page.
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
-->
<apex:page standardController="SCContractItem__c" extensions="SCAddContractItemController" recordSetVar="contractItemsRecordSet" showHeader="true" sidebar="true">
<apex:sectionHeader title="Contract items" subTitle="Add new contract items" />

<apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/jquery-1.4.2.min.js')}" />

<script>

// Number of the Locations at the page
// Onthe the first location may be opened by default
num=0;

// Saves all the values of the checked checkboxes (items, not locations)
// in one array and passed it into controller.
function prepareSave(){
    var selectedBase = new Array();
    var baseCheckboxes = jQuery("[id^='base-']");
    if(baseCheckboxes.length != 0)
    {
        for(i = 0; i < baseCheckboxes.length; i++)
        {
            if(baseCheckboxes[i].checked == true)
            {
                selectedBase.push(baseCheckboxes[i].value);
            }
        }
        document.getElementById(hiddenFieldId).value = selectedBase;
        saveItems();
    }
}

pageBlocks = new Array();

// Select all of the checkboxes at the page.
function SetAllCheckBoxes(CheckValue)
{
    if(!document.forms['{!$Component.orderItemsForm}'])
        return;
    var objCheckBoxes = document.forms['{!$Component.orderItemsForm}'].elements['checkItem'];
    if(!objCheckBoxes)
        return;
    var countCheckBoxes = objCheckBoxes.length;
    if(!countCheckBoxes)
        objCheckBoxes.checked = CheckValue;
    else
        // set the check value for all check boxes
        for(var i = 0; i < countCheckBoxes; i++)
            objCheckBoxes[i].checked = CheckValue;
}

// Turns item-checkbox-visibility off if the item was already assigned to the contract.
function checkAssigned(id,id2)
{   
    if(jQuery('#'+id+' :input').length == 0)
    {
        document.getElementById(id2).style.display = 'none';
    }
}

// Turns location-checkbox-visibility off if all items of it 
// was already assigned to the contract.
function checkPanelAssigned(id)
{
    if(jQuery('#'+id+' :input:visible:gt(0)').length == 0 )
    {
        document.getElementById('all-'+id).style.display = 'none';
    }
}

</script>

<style>
.ibaserow{
    display:block;
    margin: 5px 0 0 30px;
}
.iblocation{
    float:right;
    cursor:pointer;
    width:95%;
}
ul {
    list-style-type:none;
    margin:0;
    padding:0;
}
.nestedList {
    padding:0 0 0 12px;
    margin:5px 0 0 0;
}
.plusMinus {
    width:12px;
    height:12px;
    cursor:pointer;
    display:block;
    float:left;
    margin:3px 8px 0 10px;
}
.plusBg {
    background: url("/img/setup_plus.gif") no-repeat center transparent;
}
.minusBg {
    background: url("/img/setup_minus.gif") no-repeat center transparent;
}
.locationText {
    display:block;
    /*width:90%;*/
    float:left;
}
.panelStyle {
    color:#fff;
    background-color:#A3B1C2;
    height:22px;
    width:900px;
    padding:2px 0 0 0;
}

</style>  

   
    
<apex:form id="orderItemsForm">

    <apex:actionFunction name="saveItems" action="{!saveItems}" />

    <!-- Button Block top -->
    <div style="margin:0 0 15px 40px;width:500px;">
        <a href="#" onclick="SetAllCheckBoxes(true)">Select</a> or <a href="#" onclick="SetAllCheckBoxes(false)">Deselect</a> all items
        &nbsp;&nbsp;&nbsp;

        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        
        <apex:commandButton value="Cancel" action="{!cancelButton}" style="float:right;margin-left:10px;"/>
        
        <a href="javascript:prepareSave()" class="btn" style="text-decoration:none;width:80px;display:block;vertical-align:baseline;text-align:center;padding:3px 0 3px 0;float:right;">Save</a>
        
        <apex:inputHidden value="{!newSelectedItems}" id="itemsString"/>
        <script>var hiddenFieldId = document.getElementById("{!$Component.itemsString}").id;</script>
        
    </div>

    <apex:pageMessages />

<ul>

    <apex:dataTable value="{!IBL}" var="location" id="mainBlock" styleClass="tableClass" width="900" style="margin-left:40px;">
        <apex:column >
        
            <!-- Location -->
            <li id="{!location.id}" style="margin:0 0 10px 0;">
                <div class="panelStyle">
                <div class="plusMinus minusBg" id="plus-{!location.id}" onclick="togglePanel('block-{!location.id}','plus-{!location.id}')"></div>
                <span class="locationText">
                    <input type="checkbox" value="" id="all-{!location.id}" onclick="selectPanel('{!location.id}')" name="checkItem" />
                    <span style="cursor:pointer;" onclick="togglePanel('block-{!location.id}','plus-{!location.id}')"><b> {!location.address} </b></span>
                </span>
                
                <span style="float:right;margin:3px 10px 0 0;font-size:11px;">
                    <a href="/{!location.id}" target="_blank" style="color:#fff;">{!location.name}</a>
                </span>
                
                </div>
                <ul class="nestedList">
                    <div id="block-{!location.id}" style="margin:0;padding:0;overflow:hidden;">
                    <script> num++; if(num>1){ jQuery('#block-{!location.id}').slideToggle('100'); jQuery('#plus-{!location.id}').removeClass('minusBg').addClass('plusBg'); }</script>
                        <!-- System -->
                        <apex:repeat value="{!location.baseSystem}" var="system">
                        <li>
                            <div style="margin:5px 0 0 0;">
                            
                            <input type="checkbox"  name="checkItem" id="{!location.id}{!system.systemId}" value="" class="systemCheck" onclick="selectCheckboxSystem('{!location.id}{!system.systemId}')"/> <b>{!IF(ISNULL(system.systemName), 'Not assigned to any system', system.systemName)}</b>
                            
                            <table border="0" width="570" cellpacing="2" cellpadding="0" style="font-size:11px; float:right;font-weight:bold">
                            <tr>
                                <td align="right" style="center">Department</td>
                                <td width="120" align="right">Room</td>
                                <td width="60" align="center">Brand</td>
                                <td width="60" align="center">Serial No.</td>
                                <td width="70" align="center">Id Int.</td>
                                <td width="80" align="center">Name</td>
                            </tr>
                            </table>
                            
                            
                            <ul class="nestedList" id="baseContainer-{!location.id}{!system.systemId}">
                            <script> tempUlId = 'baseContainer-{!location.id}{!system.systemId}'; </script>
                                
                                <!-- Base -->
                                <apex:repeat value="{!system.installedBase}" var="base">
                                    <li>
                                        <div style="margin:3px 0 0 0;">
                                        <apex:outputPanel rendered="{!NOT(base.baseAssigned)}">
                                            <input type="checkbox" id="base-{!base.baseId}-{!location.id}{!system.systemId}" value="{!base.baseId}"  name="checkItem"/>
                                        </apex:outputPanel>
                                        <apex:outputPanel rendered="{!base.baseAssigned}">
                                            <apex:image url="/img/func_icons/util/lock12.gif" rendered="{!base.baseAssigned}" style="margin:0 4px 0 4px"/>
                                        </apex:outputPanel>
                                            <span style="color:#{!IF(base.baseAssigned != true,'000','7F7F7F')}">{!base.productModel}</span>
                                            
                                            <span style="float:right;font-size:11px;margin:0 10px 0 0;">
                                                <table border="0" width="100%" cellpacing="2" cellpadding="0">
                                                <tr>
                                                    <td align="right">{!base.baseDepartment}&nbsp;</td>
                                                    <td width="120" align="right">{!base.baseRoom}&nbsp;</td>
                                                    <td width="60" align="right">{!base.baseBrand}&nbsp;</td>
                                                    <td width="60" align="right">{!base.baseSerial}&nbsp;</td>
                                                    <td width="70" align="right">{!base.baseIdInt}&nbsp;</td>
                                                    <td width="80" align="right"><a href="/{!base.baseId}" target="_blank">{!base.baseName}&nbsp;</a></td>
                                                </tr>
                                                </table>
                                            </span>
                                            <hr noshade="noshade" size="1" style="border:0;width:100%;height:1px;background-color:#ececec;color:#ececec;"/>
                                        </div>
                                    </li>
                                </apex:repeat>
                                
                            </ul>
                            </div>
                        </li>
                        <script>checkAssigned(tempUlId,'{!location.id}{!system.systemId}');</script> 
                        </apex:repeat>                    
                        
                    </div>
                </ul>
            </li>
        <script> checkPanelAssigned('{!location.id}'); </script>
        </apex:column>
    </apex:dataTable>
    
</ul>

    <!-- Button Block bottom -->
    <div style="margin:0 0 15px 40px;width:500px;">
        <apex:commandButton value="Cancel" action="{!cancelButton}" style="float:right;margin-left:10px;"/>
        <a href="javascript:prepareSave()" class="btn" style="text-decoration:none;width:80px;display:block;vertical-align:baseline;text-align:center;padding:3px 0 3px 0;float:right;">Save</a>       
    </div>

</apex:form>

<!-- 29.06.2011 -->



<br />
<br />
    
    <script>
    
        // Toggles the installed-base-location pannel and +/- icon
        function togglePanel(id1,id2)
        {
            jQuery('#'+id1).slideToggle('100');
            if(jQuery('#'+id2).hasClass('plusBg')){
                jQuery('#'+id2).removeClass('plusBg').addClass('minusBg');
            }
            else{
                jQuery('#'+id2).removeClass('minusBg').addClass('plusBg');
            }
        }
   
        // Sets all the checkboxes of the system to 'checked/unchecked'
        function selectCheckboxSystem(param)
        {
            if(jQuery('#'+param).is(":checked"))
            jQuery('[id$='+param+']').attr('checked', true);
            else
            jQuery('[id$='+param+']').attr('checked', false);
        }
        
        // Sets all the checkboxes of the panel to 'checked/unchecked'
        function selectPanel(param){
            if(jQuery('#all-'+param).is(":checked")){
                jQuery('#'+param).find('input:checkbox').attr('checked', true);
            }
            else{
                jQuery('#'+param).find('input:checkbox').attr('checked', false);
            }
        }

    </script>

</apex:page>
