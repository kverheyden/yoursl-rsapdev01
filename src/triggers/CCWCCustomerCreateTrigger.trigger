/**
* @author		Development (AB)
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
*
* @description	Trigger for CCWCCustomerCreate
*
* @date			24.03.2014
*
*/

trigger CCWCCustomerCreateTrigger on Request__c (after update) {
    SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
    if(appset.DISABLETRIGGER__c){
        System.debug('warning: CCWCCustomerCreateTrigger is disabled - see SCApplicationSettings.DISABLETRIGGER');    
    }else{
		CCWCCustomerCreateHandler send = new CCWCCustomerCreateHandler();
		 if(Trigger.isUpdate && Trigger.isAfter){ 
			SalesAppSettings__c appSettings = SalesAppSettings__c.getInstance();
		 	System.debug('###appSettings:'+appSettings);
	    	List<Request__c> listReqProcess = new List<Request__c>();
	    	for(Request__c tmpReq : Trigger.new){
	    		if(tmpReq.Send_to_SAP__c) listReqProcess.add(tmpReq);
	    	}
	    	if(!listReqProcess.isEmpty()) send.sendCallout(listReqProcess, false, false);
	    }
    }
}
