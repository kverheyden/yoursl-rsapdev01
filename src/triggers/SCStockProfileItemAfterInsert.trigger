trigger SCStockProfileItemAfterInsert on SCStockProfileItem__c (after insert)
{
    SCMatMoveBaseExtension matMoveBase = new SCMatMoveBaseExtension();
    matMoveBase.correctStockItems(Trigger.new, Trigger.old, 
                                    SCMatMoveBaseExtension.TRIGGER_MODE_INSERT);
}
