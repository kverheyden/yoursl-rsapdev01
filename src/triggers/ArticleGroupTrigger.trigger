trigger ArticleGroupTrigger on ArticleGroup__c (before insert, before update) {
	TriggerFactory.createTriggerDispatcher(ArticleGroup__c.SObjectType);
}
