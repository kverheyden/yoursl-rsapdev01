trigger PictureOfSuccessTrigger on PictureOfSuccess__c (before insert, before update) {
	TriggerFactory.createTriggerDispatcher(PictureOfSuccess__c.SObjectType);
}
