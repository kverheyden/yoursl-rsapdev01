/**
* @author		Austen Buennemann
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				a.buennemann@yoursl.de
*
* @description	Trigger on SCInstalledBase__c after update
*
* @date			30.09.2014
*
* Timeline:
* Name               DateTime                  Description
* Austen Buennemann  30.09.2014 15:17          Class created
*/
trigger CCWCSCInstalledBaseTrigger on SCInstalledBase__c (after update) {
	CCWCAssetInventoryDataMobileUpdate send = new CCWCAssetInventoryDataMobileUpdate();
	    if(Trigger.isUpdate && Trigger.isAfter){ 
    		send.sendCallout(Trigger.new);
    }

}
