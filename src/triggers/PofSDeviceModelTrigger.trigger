trigger PofSDeviceModelTrigger on PofSDeviceModel__c (before insert, before update) {
	
	PofSInitialContentListAllocator allocateInitialContentList = new PofSInitialContentListAllocator();
	allocateInitialContentList.AllocateInitialContentList(trigger.new);
}
