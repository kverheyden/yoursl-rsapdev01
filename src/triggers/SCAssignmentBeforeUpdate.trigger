trigger SCAssignmentBeforeUpdate on SCAssignment__c (before update)
{
    SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
    if(appset.DISABLETRIGGER__c)
    {
        System.debug('warning: SCAssignmentBeforeUpdate trigger is disabled - see SCApplicationSettings.DISABLETRIGGER');    
    }
    else
    {
        SCboAssignment.BeforeUpdate(Trigger.new, Trigger.old);
    }
}
