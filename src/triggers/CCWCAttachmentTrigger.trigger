/**
* @author       YOUR SL
* @description  Trigger on Attachment for PDF creation
* @date         20.05.2014
*/

trigger CCWCAttachmentTrigger on Attachment (after insert) {
	 SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
    if(appset.DISABLETRIGGER__c){
        System.debug('warning: CCWCAttachmentTrigger is disabled - see SCApplicationSettings.DISABLETRIGGER');    
		return;
    }
	
    if(ContactAvoidTriggerRecursion.runOnce()) {
        CCWCPDFCreatorHandler creatorHandler = new CCWCPDFCreatorHandler();
		creatorHandler.UserSessionId = UserInfo.getSessionId();
        if (Trigger.isInsert && Trigger.isAfter) {
            creatorHandler.createContractsCallout(Trigger.new);
			
			// extract to separate class
			Map<Id, Attachment> requestAttachmentId_attachment = new Map<Id, Attachment>();
			for (Attachment a : Trigger.new) {
				if (a.ParentId.getSObjectType() == RequestAttachment__c.SObjectType)
					requestAttachmentId_attachment.put(a.ParentId, a);
			}			
			
			if (!requestAttachmentId_attachment.isEmpty()) {
				// Set AttachmentId__c
				Map<Id, RequestAttachment__c> requestAttachments = new Map<Id, RequestAttachment__c>([SELECT Request__r.Send_to_SAP__c, Request__r.NumberOfAttachments__c, AttachmentId__c FROM RequestAttachment__c WHERE Id IN: requestAttachmentId_attachment.keySet()]);
				List<RequestAttachment__c> requestAttachmentsToUpdate = new List<RequestAttachment__c>();
				List<Request__c> requestsToUpdate = new List<Request__c>();
				
				for (RequestAttachment__c ra : requestAttachments.values()) {
					Attachment a = requestAttachmentId_attachment.get(ra.Id);
					if (a == null)
						continue;
					ra.AttachmentId__c = a.Id;
					requestAttachmentsToUpdate.add(ra);
				}
				update requestAttachmentsToUpdate;
				
				// Set IsReadyToSendToSAP__c
				List<RequestAttachment__c> requAttachments = [SELECT Request__r.Send_to_SAP__c, AttachmentId__c FROM RequestAttachment__c WHERE Id IN: requestAttachments.keySet()];
								
				for (RequestAttachment__c ra: requAttachments) {
					
					Request__c r = [SELECT NumberOfAttachments__c, (SELECT AttachmentId__c, Type__c FROM Request_Attachments__r WHERE AttachmentId__c != NULL) FROM Request__c WHERE Id =: ra.Request__c LIMIT 1];
					if (r.NumberofAttachments__c == r.Request_Attachments__r.size()) {
						ra.Request__r.IsReadyToSendToSAP__c = true;
						requestsToUpdate.add(ra.Request__r);
					}
				}
				
				update requestsToUpdate;
				
			}
		}
    }
}
