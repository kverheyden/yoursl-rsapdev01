trigger PofSValidationTrigger on PictureOfSuccess__c ( before insert, before update ) {
    String mode = 'update';
    if(Trigger.isInsert){ 
        mode='insert';
    }
    
    List< PictureOfSuccess__c > LL_PictureOfSuccess = new List< PictureOfSuccess__c >();
    for( PictureOfSuccess__c LO_PictureOfSuccess: Trigger.new ){
    	if( LO_PictureOfSuccess.Active__c ){
    		LL_PictureOfSuccess.add( LO_PictureOfSuccess );
    	}
    }
    PofSValidation pofsval = new PofSValidation();
    pofsval.validateData( LL_PictureOfSuccess, mode );
}
