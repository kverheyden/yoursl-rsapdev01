trigger ECOMSetCloudCrazeCurrencyTrigger on User (before insert) 
{    
    Profile profile = [SELECT Id FROM Profile WHERE Name='CloudCraze Customer Community User'];
    
    for (User user : trigger.new) {
        if (user.ProfileId == profile.Id) {
            user.ccrz__CC_CurrencyCode__c = 'EUR';
        }
    }   
}
