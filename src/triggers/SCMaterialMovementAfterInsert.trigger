/*
 * Called when material movement records are created.
 * - checks if sparepart package received notifications are to be sent to SAP (### TODO: move to post processing job - to avoid that package recieved comes before all receipt confirmations are sent)
 * - material requisitions are processed correclt (if more than 10 items are requested we get currently an limit exceeded exception 
 */
trigger SCMaterialMovementAfterInsert on SCMaterialMovement__c (after insert)
{
    SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
    if(appset.DISABLETRIGGER__c)
    {
        System.debug('warning: SCMaterialMovementAfterInsert trigger is disabled - see SCApplicationSettings.DISABLETRIGGER');    
    }
    else
    {
        afterInsert(Trigger.new);
    }
    
    public void afterInsert(List<SCMaterialMovement__c> items)
    {
        // simple version ## if required implement extended version to avoid soql exceptions
        
        // for each material movement of the tpe "package received" select the corresponding material movements
        /*
        //GMSGB 12.09.2013 deactivated
        for(SCMaterialMovement__c pack : items)
        {
            // check if we have a package received material movement record
            if(pack.type__c == SCfwConstants.DOMVAL_MATMOVETYP_SPAREPART_PACKAGE_RECEIVED && 
               pack.source__c == 'mobile' && pack.DeliveryNote__c != null)
            {
                // now check for all package received records from the mobile system (backend function is to be ignored)
                // read the related material movements 
                List<SCMaterialMovement__c> movements = [select id, name from SCMaterialMovement__c where
                    stock__c = :pack.stock__c and 
                    DeliveryNote__c = :pack.DeliveryNote__c and
                    source__c = 'mobile' and 
                    type__c = :SCfwConstants.DOMVAL_MATMOVETYP_RECEIPT_CONFIRMATION and 
                    status__c = :SCfwConstants.DOMVAL_MATSTAT_BOOKED and
                    erpstatus__c = 'none' 
                    order by name];    
                
                // now determine the ids to be used in the callout   
                List<String> movementids = new List<String>();
                for(SCMaterialMovement__c mov : movements)
                {
                    movementids.add(mov.id);
                }     
                // if there are confirmed material movements - initiate the callouts
                if(movementids.size() > 0)
                {
                    // now send the information to 
                    CCWCMaterialMovementCreate.callout(movementids, true, false);

                    // and close the package
                    CCWCMaterialPackageReceived.callout(pack.id, true, false);
                    
                    //GMSNA 04.04.2013 RISK
                    // as both future callouts are put to the queue we can't ensure that the 
                    // 1. material movment is sent before the 
                    // 2. package received is sent
                    
                    // TEST REQUIRED
                }
            }    
//GMSNA 20.05.2013 moved to SCbtcClearingPostProcess called externally by jenkins 
//GMSGB 10.09.2013 deactivated
           /*else if(pack.type__c == SCfwConstants.DOMVAL_MATMOVETYP_MATREQUEST_EMPL 
               && pack.source__c == 'mobile' )
           {
                //### TODO: move to another trigger so that we can use multiple items in one package
                List<String> movementids = new List<String>();
                movementids.add(pack.id);
                CCWCMaterialReservationCreate.callout(movementids, true, false);
            }*/
        ////GMSGB 12.09.2013 deactivated } // for...
    }
}
