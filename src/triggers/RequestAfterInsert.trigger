/**
* @author		Andre Mergen
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
*
* @description	Trigger on Request__c
*
* @date			01.07.2014
*
* Timeline:
* Name               DateTime                  Description
* Andre Mergen       01.07.2014 09:00          Class created
*/

trigger RequestAfterInsert on Request__c (after insert) {

	if(Trigger.isAfter)
	{
		if(Trigger.isInsert)
		{
			RequestAfterInsertCLS RAIC = new RequestAfterInsertCLS();
			RAIC.prepareRequestAccountUpdate(Trigger.new);
		}
	}
}
