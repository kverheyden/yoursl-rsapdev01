trigger ATPStockQuantityTrigger on ATPStockQuantity__c (before insert, before update) 
{
    ATPStockQuantity__c[] quants = trigger.new;

    EComProduct2ToCloudcrazeProductResolver logic = new EComProduct2ToCloudcrazeProductResolver();
    logic.setCloudCrazeProductReferenceFromProduct2Reference(quants, 'Product2__c', 'ccrz_product__c');
}
