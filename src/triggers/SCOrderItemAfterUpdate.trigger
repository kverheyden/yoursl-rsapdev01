/**
* @author           Oliver Preuschl
*
* @description      Send the data of a SCOrder/SCOrderItem to SAP
*
* @date             27.08.2014
*
* Timeline:
* Name              DateTime                Version        Description
* Oliver Preuschl   27.08.2014              *1.0*          Created
*/

trigger SCOrderItemAfterUpdate on SCOrderItem__c ( after update ) {
    
    SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
    if (appset.DISABLETRIGGER__c){
        System.debug('warning: SCOrderItemAfterUpdate trigger is disabled - see SCApplicationSettings.DISABLETRIGGER');    
    }else{
        fsFASCOrderCreate.sendSCOrdersToSAP( Trigger.oldMap, Trigger.newMap );
    }
}
