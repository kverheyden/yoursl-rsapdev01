trigger SCAppointmentAfterDelete on SCAppointment__c (after delete)
{
    SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
    if(appset.DISABLETRIGGER__c)
    {
        System.debug('warning: SCAppointmentAfterDelete trigger is disabled - see SCApplicationSettings.DISABLETRIGGER');    
    }
    else
    {
        SCboAppointment boAppointment = new SCboAppointment ();
        boAppointment.AfterDelete(Trigger.old);
    }
}
