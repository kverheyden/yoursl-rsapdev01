trigger SCInstalledBaseTrigger on SCInstalledBase__c bulk (before insert,before update,after update) {

    
    System.debug('###SCInstalledBaseTrigger');
    
    if (Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
        //DEACTIVATED AS BY GEORG BIRKENHEUER 2014-12-03
        //SCInstalledBaseMethods.doBeforeInsertUpdate(Trigger.new);
    } 
    
    if(Trigger.isAfter && Trigger.isUpdate) {
        System.debug('###SCInstalledBaseTrigger - isAfterUpdate (new & old)');
        SCInstalledBaseMethods.doAfterUpdate(Trigger.new);
        SCInstalledBaseMethods.doAfterUpdate(Trigger.Oldmap, Trigger.Newmap);        
    }

}
