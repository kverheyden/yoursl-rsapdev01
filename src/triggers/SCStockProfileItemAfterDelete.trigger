trigger SCStockProfileItemAfterDelete on SCStockProfileItem__c (after delete)
{
    SCMatMoveBaseExtension matMoveBase = new SCMatMoveBaseExtension();
    matMoveBase.correctStockItems(Trigger.new, Trigger.old, 
                                    SCMatMoveBaseExtension.TRIGGER_MODE_DELETE);
}
