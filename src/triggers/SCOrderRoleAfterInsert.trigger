/**
 * This trigger sets the redundant order role lookup fields.
 * The fields are used in reports and filtered lists
 *
 * @author narmbruster@gms-online.de
 */
trigger SCOrderRoleAfterInsert on SCOrderRole__c (after insert) 
{
    SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();    
    if(appset.DISABLETRIGGER__c)    
    {        
        System.debug('warning: SCOrderRoleAfterInsert trigger is disabled - see SCApplicationSettings.DISABLETRIGGER');        
    }    
    else    
    {     
        Map<String, SCOrder__c> orders = new Map<String, SCOrder__c>();
        for(SCOrderRole__c r : trigger.new)
        {
            SCOrder__c ord = orders.get(r.order__c);
            if(ord == null)
            {
                ord = new SCOrder__c(id = r.order__c);
                orders.put(r.order__c, ord);
            }
            
            // now evaluate the roles
            if(r.OrderRole__c == SCfwConstants.DOMVAL_ORDERROLE_LE)
            {
                ord.roleSR__C = r.id;
            }
            else if(r.OrderRole__c == SCfwConstants.DOMVAL_ORDERROLE_AG)
            {
                ord.roleCA__C = r.id;
            }
            else if(r.OrderRole__c == SCfwConstants.DOMVAL_ORDERROLE_RE)
            {
                ord.roleIR__C = r.id;
            }
            else if(r.OrderRole__c == SCfwConstants.DOMVAL_ORDERROLE_RG)
            {
                ord.rolePY__C = r.id;
            }
            
        } 
        // now fill the redundant fields on order level
        List<SCOrder__c>items = orders.values();
        if(items.size() > 0)
        {
            update items;        
        }
    }
}
