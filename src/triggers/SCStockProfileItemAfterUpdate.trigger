trigger SCStockProfileItemAfterUpdate on SCStockProfileItem__c (after update)
{
    SCMatMoveBaseExtension matMoveBase = new SCMatMoveBaseExtension();
    matMoveBase.correctStockItems(Trigger.new, Trigger.old, 
                                    SCMatMoveBaseExtension.TRIGGER_MODE_UPDATE);
}
