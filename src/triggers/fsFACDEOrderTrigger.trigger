/**********************************************************************

Name: fsFACDEOrderCreateBatch

======================================================

Purpose: 
 
This trigger creates new SCOrders, SCOrderItems based on the new CDEOrderItems

======================================================

History 

------- 

Date            AUTHOR              DETAIL

06/04/2014      Oliver Preuschl     INITIAL DEVELOPMENT
08/14/2014		Austen Buennemann	add block [08/14/2014] 

***********************************************************************/

trigger fsFACDEOrderTrigger on CDEOrder__c ( after insert, after update ) {
	//Create SCOrders for CDEOrders with Send_to_SAP = true
	if( !System.isFuture() && !System.isBatch() ){
		fsFACDEOrderCreate.createSCOrdersFromCDEOrders( Trigger.newMap.KeySet() );
	}
	// [08/14/2014
    SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
    if(appset.DISABLETRIGGER__c){
        System.debug('warning: fsFACDEOrderTrigger is disabled - see SCApplicationSettings.DISABLETRIGGER');    
    }else{
    	if(Trigger.isAfter && Trigger.isInsert){
    		AccountRelationOfRequestChields relation = new AccountRelationOfRequestChields();
			relation.relateCdeOrder(Trigger.new);
    	}    	
	}
	// 08/14/2014]
}
