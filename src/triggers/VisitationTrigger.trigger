/**
* @author		Austen Buennemann
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
*				a.buennemann@yoursl.de
*
* @description	Trigger on Visitation__c
*
* @date			14.08.2014
*
* Timeline:
* Name               DateTime                  Version         Description
* Austen Buennemann  14.08.2014 18:07          *1.0*           created the trigger
*/


trigger VisitationTrigger on Visitation__c (after insert) {
    SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
    if(appset.DISABLETRIGGER__c){
        System.debug('warning: VisitationTrigger is disabled - see SCApplicationSettings.DISABLETRIGGER');    
    }else{
    	if(Trigger.isAfter && Trigger.isInsert){
    		AccountRelationOfRequestChields relation = new AccountRelationOfRequestChields();
			relation.relateVisitation(Trigger.new);
    	}    	
	}

}
