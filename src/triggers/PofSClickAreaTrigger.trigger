trigger PofSClickAreaTrigger on PofSClickArea__c (before insert, before update) {
	TriggerFactory.createTriggerDispatcher(PofSClickArea__c.SObjectType);
}
