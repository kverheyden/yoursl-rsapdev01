trigger SCProductModelTrigger on SCProductModel__c (before insert) {
    TriggerFactory.createTriggerDispatcher(SCProductModel__c.SObjectType);
}
