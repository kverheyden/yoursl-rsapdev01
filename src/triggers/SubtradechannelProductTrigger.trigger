trigger SubtradechannelProductTrigger on SubtradechannelProduct__c (before insert, before update) {
    SubtradechannelProduct__c[] stcProducts = trigger.new;
    
    EComProduct2ToCloudcrazeProductResolver logic = new EComProduct2ToCloudcrazeProductResolver();
	logic.setCloudCrazeProductReferenceFromProduct2Reference(stcProducts, 'Product2__c', 'ccrz_Product__c');
}
