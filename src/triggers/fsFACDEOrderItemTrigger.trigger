/**********************************************************************

Name: fsFACDEOrderCreateBatch

======================================================

Purpose: 
 
This trigger creates new SCOrders, SCOrderItems based on the new CDEOrderItems

======================================================

History 

------- 

Date            AUTHOR              DETAIL

06/04/2014      Oliver Preuschl     INITIAL DEVELOPMENT

***********************************************************************/

trigger fsFACDEOrderItemTrigger on CDEOrderItem__c ( after insert, after update ) {
	//Create SCOrders for CDEOrderItems with status = 'New'
	if( !System.isFuture() && !System.isBatch() ){
		fsFACDEOrderCreate.createSCOrdersFromCDEOrderItems( Trigger.newMap.KeySet() );
	}
}
