/*******************************************************
created 2014-05-15
Update Account Owner based on the CCAccountRole
*******************************************************/
trigger CCAccountRoleTrigger on CCAccountRole__c (after insert, after update) {
	
	if(Trigger.isAfter)
	{
		if(Trigger.isInsert || Trigger.isUpdate)
		{
			CCAccountRoleTriggerCLS CCART = new CCAccountRoleTriggerCLS();
			CCART.prepareAccountUpdate(Trigger.new); 
		}

	} 

}
