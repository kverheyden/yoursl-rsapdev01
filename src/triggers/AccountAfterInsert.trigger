/**
* @author		Andre Mergen
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
*
* @description	Trigger on Account
*
* @date			01.07.2014
*
* Timeline:
* Name					Datetime 				Description
* Andre Mergen       	01.07.2014 09:00        Class created
* Austen Buennemann  	03.09.2014 09:59        ckeck and comments
* Bernd Werner			21.11.22014 11:00		Changed entries method to enrich data for ecom
* Oliver Preuschl		21.11.22014 11:00		Update field "HasOrderForNextDeliveryDate" to true, if there is an order with a fitting request date
*/

trigger AccountAfterInsert on Account (after insert, after update) {
	AccountRequestDataTransfer accHeandler = new AccountRequestDataTransfer();
	// CCAppAccountSyncStatus appchk = new CCAppAccountSyncStatus();

	if (Trigger.isInsert){
		accHeandler.insertEntry(Trigger.newMap);
	}
	if (Trigger.isUpdate){
		accHeandler.updateEntry(Trigger.newMap, Trigger.oldMap);
		CCOrderHandler.updateAccounts( Trigger.oldMap, Trigger.newMap );
	}

	/*
	if (Trigger.isInsert){	
		accHeandler.enrichData(Trigger.new);
	}
	if (Trigger.isUpdate){
		List<Account>processAcc = new List<Account>();
		for(Id tmpId : Trigger.newMap.keySet()){
			if(Trigger.newMap.get(tmpId).RequestID__c!=Trigger.oldMap.get(tmpId).RequestID__c){
				processAcc.add(Trigger.newMap.get(tmpId));
			}
		}
        // Commented by BW 19.09.2014 while SAP only sends the Roles ones the Account is ready (finalized).
		// appchk.setAccountOwner(Trigger.new);
		
		if(processAcc.size()>0){
			accHeandler.enrichData(processAcc);
		}
	}
	*/
}
