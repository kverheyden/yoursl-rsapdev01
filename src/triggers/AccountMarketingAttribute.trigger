/**
* @author		Austen Buennemann
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				a.buennemann@yoursl.de
*
* @description	Trigger for AccountMarketingAttribute__c
*
* @date			25.09.2014
*
* Timeline:
* Name               DateTime                  Description
* Austen Buennemann  25.09.2014 13:54          Class created
*/

trigger AccountMarketingAttribute on AccountMarketingAttribute__c (after update) {
    SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
    if(appset.DISABLETRIGGER__c){
        System.debug('warning: AccountMarketingAttribute is disabled - see SCApplicationSettings.DISABLETRIGGER');    
    }else{
    	if(Trigger.isAfter && Trigger.isUpdate){
    		CCAccountMarketingAttributeHandler handler = new CCAccountMarketingAttributeHandler();
    		handler.deleteMarketingAttribute(Trigger.new);
    	}		
    }
}
