/**
* @author		Austen Buennemann
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				a.buennemann@yoursl.de
*
* @description	Trigger for CCWCSalesOrderHandler
*
* @date			26.05.2014
*
*/

trigger CCWCSalesOrderTrigger on SalesOrder__c (after insert, after update) {
	//String error = null;
    SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
    if(appset.DISABLETRIGGER__c){
        System.debug('warning: CCWCSalesOrderTrigger is disabled - see SCApplicationSettings.DISABLETRIGGER');    
    }else{
    	if(Trigger.isAfter && Trigger.isInsert){
    		AccountRelationOfRequestChields relation = new AccountRelationOfRequestChields();
			relation.relateSalesOrder(Trigger.new);
    	}    	
    	if(Trigger.isAfter && Trigger.isUpdate){
			CCWCSalesOrderHandler send = new CCWCSalesOrderHandler();
	    	//send.forwardCallout(Trigger.new, Trigger.old, false);
	    	send.sendCalloutbyCheckbox(Trigger.new, false);  	
    	}
	}
}
