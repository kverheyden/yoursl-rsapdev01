/**
 * @created 2014-10-08T15:45:00+02:00
 * @author "thomas richter" <thomas@meformobile.com>
 * 
 * @todo maybe its better to add also delete functionalities here - but i dont think 
 * only the "shipto/soldto" role can be removed without account ;)
 */
trigger CCAccountRoleSetSoldToTrigger on CCAccountRole__c (before update, before insert, after delete) {
    CCAccountRole__c[] roles = trigger.new;    
    
    // yes - non static ;)
    CCAccountRoleSetSoldToTriggerCLS logic = new CCAccountRoleSetSoldToTriggerCLS();
    
    if (Trigger.isDelete) {
        logic.removeIsSoldToFlag(trigger.old);
        logic.removeSoldToReferences(trigger.old);
    }else{
    	logic.updateIsSoldToFlag(trigger.new);
        logic.updateSoldToReferences(trigger.new);
    }
}
