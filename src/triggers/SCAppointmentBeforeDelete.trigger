trigger SCAppointmentBeforeDelete on SCAppointment__c (before delete) 
{
    SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
    if(appset.DISABLETRIGGER__c)
    {
        System.debug('warning: SCAppointmentBeforeDelete trigger is disabled - see SCApplicationSettings.DISABLETRIGGER');    
    }
    else
    {
        SCboAppointment boAppointment = new SCboAppointment ();
        boAppointment.BeforeDelete(Trigger.old);
    }
}
