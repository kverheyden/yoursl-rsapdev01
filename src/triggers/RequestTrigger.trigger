trigger RequestTrigger on Request__c (after insert,after update) {
	TriggerFactory.createTriggerDispatcher(Request__c.SObjectType);
}
