trigger AttachmentTrigger on Attachment (after insert, after update, before delete) {
	TriggerFactory.createTriggerDispatcher(Attachment.SObjectType);
}
