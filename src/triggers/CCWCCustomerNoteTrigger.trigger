/**
* @author		Development (AB)
* 				H&W Consult GmbH
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				http://www.hundw.com
*
* @description	Trigger for CCWCCustomerNote.
*
* @date			17.12.2013
*
*/

trigger CCWCCustomerNoteTrigger on FeedItem (after insert) {
	CCWCCustomerNoteHandler send = new CCWCCustomerNoteHandler();
	    if(Trigger.isInsert && Trigger.isAfter){ 
    		send.sendCallout(Trigger.new, false, false);
    }

}
