/*
Creater: Alexander Placidi
Description: The contacts with firstname "Verkaufsberater" will get assigned to the account via the account lookup "PrimarySalesContact__c".
*
Object: Contact
TestClass: ContactMethodsTest
Last Modified: 20.01.2014
*/
trigger ContactTrigger on Contact ( after insert, after update ) {

 	if( Trigger.isAfter && ( Trigger.isInsert || Trigger.isUpdate ) ) {	 		
		ContactMethods.updateAccountPrimarySalesContacts( Trigger.new );
 	}

}
