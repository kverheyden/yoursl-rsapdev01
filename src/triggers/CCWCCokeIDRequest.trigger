/**
* @author       Austen Buennemann
*               Bahnhofstr. 3
*               21244 Buchholz i.d.N.
*               a.buennemann@yoursl.de
*
* @description  Trigger for CCWCCustomerRegistrationHandler 
*
* @date         17.11.2014
*
*/
trigger CCWCCokeIDRequest on CokeIDRequest__c (after update) {
    SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
    if(appset.DISABLETRIGGER__c){
        System.debug('warning: CCWCCokeIDRequest trigger is disabled - see SCApplicationSettings.DISABLETRIGGER');    
    }else{
        CCWCCustomerRegistrationHandler send = new CCWCCustomerRegistrationHandler();
        send.prepareRegistration(Trigger.new);
    }
}
