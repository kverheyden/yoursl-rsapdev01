/**********************************************************************

History 

------- 

Date            AUTHOR              DETAIL
08/14/2014		Austen Buennemann	add block [08/14/2014]

***********************************************************************/
trigger RedSurveyResultAfterInsert on RedSurveyResult__c (after insert) {
	
	T_RedSurveyResultAfterInsert TRSRAI = new T_RedSurveyResultAfterInsert ();
	TRSRAI.VisUpdate(Trigger.new);
	// [08/14/2014
    SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
    if(appset.DISABLETRIGGER__c){
        System.debug('warning: RedSurveyResultAfterInsert is disabled - see SCApplicationSettings.DISABLETRIGGER');    
    }else{
    	if(Trigger.isAfter && Trigger.isInsert){
    		AccountRelationOfRequestChields relation = new AccountRelationOfRequestChields();
			relation.relateRedSurveyResult(Trigger.new);
    	}    	
	}
	// 08/14/2014]
	
}
