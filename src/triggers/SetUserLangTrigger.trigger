/* Trigger to update the language custom field that is used to display 
 * language specific article and product model names in the application.
 * The custom field is required as we can't access the User.LanguageLocaleKey in 
 * formulas directly.
 * 2010        Mr. Steinebach SF - created
 * 2012-07-03  Mrs. Steinberg - special treatment for China - use english locale
 * 2012-07-04  GMSNA - updated to support en_GB locale
 */
trigger SetUserLangTrigger on User (before insert, before update) {
  for(User userTemp : Trigger.new)
  {
    //  workaround for setting the Brithish English language 
    if(userTemp.LanguageLocaleKey == '2')
    {
        // salesforce does not set the 'en_GB' and uses '2' instead (why ever)
        userTemp.CurrentLanguage__c = 'en_GB';
    }
    // special treatment for China - use standard english translations
    else if(userTemp.LanguageLocaleKey == 'zh_CN')
    {
        userTemp.CurrentLanguage__c = 'en_US';
    }
    else
    {  
        userTemp.CurrentLanguage__c = userTemp.LanguageLocaleKey;
    }
    
    //FKR: concat assigned brands and countries -  field needed for validation rule on custom object ent_prod_rel__c
    userTemp.CalcBrandsCountries__c = '' + userTemp.Brand__c + ' | ' + userTemp.AssignedCountries__c;
  }
}
