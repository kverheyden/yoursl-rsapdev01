/*
 * @(#)SCDomainRefTrigger.trigger
 * 
 * Copyright 2014 by GMS Development GmbH, Karl-Schurz-Strasse 29, 
 * DE-33100 Paderborn. All rights reserved.
 * 
 * @history 
 * 2014-11-17  GMSSS   created
 *
 * @review 
 * 
 */
trigger SCDomainRefTrigger on SCDomainRef__c (before insert, before update)
{
    if (Trigger.isBefore && Trigger.isInsert)
    {
        for (SCDomainRef__c ref : Trigger.new)
        {
            if (ref.Id2__c != null && ref.Id2__c != '' && String.isNotEmpty(ref.Id2__c))
            {
                try
                {
                    ref.Id__c = Decimal.valueOf(ref.Id2__c);
                }
                catch(Exception e) { }
            }
        }
    }//Before Insert
    
    if (Trigger.isBefore && Trigger.isUpdate)
    {
        for (Id key : Trigger.newMap.keySet())
        {
            SCDomainRef__c refNew = Trigger.newMap.get(key);
            SCDomainRef__c refOld = Trigger.oldMap.get(key);
            
            if (   refNew.Id2__c != null && refNew.Id2__c != '' && String.isNotEmpty(refNew.Id2__c)
                && refOld.Id2__c != null && refOld.Id2__c != '' && String.isNotEmpty(refOld.Id2__c)
                && (!refNew.Id2__c.equalsIgnoreCase(refOld.Id2__c)))
            {
                try
                {
                    refNew.Id__c = Decimal.valueOf(refNew.Id2__c);
                }
                catch(Exception e) { }
            }
        }
    }//Before Update
}//SCDomainRefTrigger
