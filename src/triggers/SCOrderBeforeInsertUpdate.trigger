/**
* This trigger sets the grouping fields with the createddate. The fields are required
* as SOQL does not support functions or formulas that can be grouped when counting records.
* 
* Author              |Author-Email                       |Date        |Comment
* --------------------|---------------------------------- |------------|----------------------------------------------------
* Norbert Armbruster  |narmbruster@gms-online.de          |21.09.2010  |Implemented
*/
trigger SCOrderBeforeInsertUpdate on SCOrder__c (before insert, before update) 
{
    for (SCOrder__c ord:trigger.new) 
    {
    	if(ord.CreatedDate != null)
    	{
	    	ord.util_createddate_yyyymm__c = ord.CreatedDate.format('yyyy-MM');
	    	ord.util_createddate_yyyyww__c = ord.CreatedDate.format('yyyy-ww');
	    	ord.util_createddate_yyyy__c   = ord.CreatedDate.format('yyyy');
    	}
    	else
    	{
            DateTime now = datetime.now();
	    	ord.util_createddate_yyyymm__c = now.format('yyyy-MM');
	    	ord.util_createddate_yyyyww__c = now.format('yyyy-ww');
	    	ord.util_createddate_yyyy__c   = now.format('yyyy');
    	}
    } 
}
