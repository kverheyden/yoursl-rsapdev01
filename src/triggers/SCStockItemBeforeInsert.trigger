/**
 * This trigger sets the activity flag to "I".
 * 
 * @author Eugen Tiessen <etiessen@gms-online.de>
 * @version $Revision$, $Date$
 */
trigger SCStockItemBeforeInsert on SCStockItem__c (before insert) 
{
	if (SCApplicationSettings__c.getInstance().DISABLETRIGGER__c)
    {
        System.debug('warning: SCStockAfterInsert trigger is disabled - see SCApplicationSettings.DISABLETRIGGER');
        return;    
    }
    
	for(SCStockItem__c stockItem : Trigger.new)
	{
		if(stockItem.Activity__c == null)
		{
			stockItem.Activity__c = 'I';
		}
		
	}
	 
}
