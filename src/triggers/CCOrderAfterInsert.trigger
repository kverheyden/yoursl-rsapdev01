/**
* @author		Oliver Preuschl
*
* @description	Update of the related Account when a CCOrder is created
*
* @date			20.11.2014
*
* Timeline:
* Name               Date                      Description
* Oliver Preuschl    20.11.2014				   Created
*
*/
trigger CCOrderAfterInsert on ccrz__E_Order__c ( after insert ) {
	if( Trigger.isAfter && Trigger.isInsert ){
		CCOrderHandler.updateAccounts( Trigger.new );
	}
}
