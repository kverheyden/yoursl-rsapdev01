<!--
 * @(#)SCMaterialAvailability.component
 * 
 * Copyright 2010 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author Alexei Geiger <ageiger@gms-online.de>
 * @version $Revision$, $Date$
-->

<apex:component controller="SCMaterialAvailabilityController" allowDML="true">

<apex:attribute name="isDisplaySearchField1" type="Boolean" description="Show search input fields" required="false" assignTo="{!isDisplaySearchField1}"/>
<apex:attribute name="isDisplaySearchField2" type="Boolean" description="Show search input fields" required="false" assignTo="{!isDisplaySearchField2}"/>
<apex:attribute name="articles" type="Id[]" required="false" assignTo="{!articleIds}" description="Array of Article ids"/>
<apex:attribute name="stocks" type="Id[]" required="false" assignTo="{!stockIds}" description="Array of stock ids"/>
<apex:attribute name="plants" type="Id[]" required="false" assignTo="{!plantIds}" description="Array of plant ids"/>
<apex:attribute name="resources" type="Id[]" required="false" assignTo="{!resourceIds}" description="Array of resource ids"/>
<apex:attribute name="pageController" type="SCfwPageControllerBase" assignTo="{!pageController}" required="false" description="The controller for the page." />


<apex:stylesheet value="{!URLFOR($Resource.SCRes,'lib/jquery/css/cupertino/jquery-ui-1.8.4.custom.css')}" />
<apex:stylesheet value="{!URLFOR($Resource.SCRes,'css/cp-jquery-ui.css')}" />

<style>
    .searching
    {
        background:url(/img/loading.gif) no-repeat 0 0; 
        padding-left:20px; 
        margin-left:10px; 
        padding-bottom:5px; 
        font-size:12px;
    }
    .searchFinished
    {
        background:none;
    }
    .selectColumn
    {
        width:80px;
    }
</style>

<script type="text/javascript" language="JavaScript">
var isValidArticleId;

function matAvailabilityTabEnter()
{
    jQuery('[id$=closeJob]').hide();
}

function matAvailabilityTabLeave()
{
    jQuery('[id$=closeJob]').show();
}
/**
 * Callback function that is called from the article search dialog
 * which moves the dialog away from the from, call the controller to
 * update the article and closes the dialog.
 * 
 * @param {String} articleId SFDC ID of the article
 */
function articleSelectedMatAv(articleId, articleName)
{
    jQuery('#cp-article-search').parent().appendTo(jQuery("body"));
    
    jQuery('input[type=hidden][id$=returnArticleIdMatAv]').val(articleId);
    jQuery('input[id$=returnArticleNameMatAv]').val(articleName);

    jQuery('#cp-article-search').dialog( "close" );
}

function checkArticleId()
{
    if ( isValidArticleId == false )
    {
        openArticleSearch(this, '{!JSENCODE($Label.SC_app_SpareParts)}', 0, 'articleSelectedMatAv', {!articleSearchAutoStart});
    }
}

/**
 * Handle keypress on article element and open
 * the article search dialog by calling the click event
 * of the lookup icon.
 */
function articleEnter(element, event)
{
    // Catch the return key
    if (event.keyCode == 13)
    {
        // prevent Firefox from submitting the first 
        // submit button even hidden ones
        try
        {
            event.preventDefault();
            event.stopPropagation();
            event.stopImmediatePropagation();
        }
        catch (err)
        {
        }

        jQuery(element).next().click();
        return false;
    }
    else
    {
        // empty the hidden field which contains the article id
        jQuery(element).prev().val('');
    }

    return false;
}

function copyDateToOrder()
{
    var deliveryDate = jQuery('[id$=deliveryDate]').val();
    var callbackname = "{!callback}";
    if ('' != callbackname)
    {
        try
        {
            {!callbackComplete}
            window.close();
        }
        catch(e)
        {
            alert('{!callbackComplete}')
            alert('error when calling the callback function' + e);
        }
    }
}
</script>

<apex:actionFunction name="getArticles" reRender="searchField2, table" action="{!onArticleExt}" status="loadingArticles" />

<apex:actionRegion >
<!-- 
    searchField1 pageBlock is used on Material Availability page
 -->
    <apex:pageBlock id="searchField1" rendered="{!isDisplaySearchField1}">
        <apex:pageBlockSection >
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="{!$Label.SC_app_ArticleSearchBox}" />
                <apex:panelGroup layout="none">
                    <apex:inputHidden id="returnArticleIdMatAv" value="{!articleId}"/>
                    <apex:inputText id="returnArticleNameMatAv" value="{!articleName}" onkeypress="articleEnter(this, event)"/>
                    <img src="/s.gif"
                         class="lookupIcon"
                         alt="{!JSENCODE($Label.SC_app_SpareParts)}"
                         onmouseover="this.className = 'lookupIconOn';this.className = 'lookupIconOn';"
                         onmouseout="this.className = 'lookupIcon';this.className = 'lookupIcon';"
                         onfocus="this.className = 'lookupIconOn';"
                         onblur="this.className = 'lookupIcon';"
                         onclick="openArticleSearch(this, '{!JSENCODE($Label.SC_app_SpareParts)}', 0, 'articleSelectedMatAv', {!articleSearchAutoStart})" />
                        <apex:outputLabel value="{!$Label.SC_app_SimilarArticle}" />
                    <apex:inputCheckbox value="{!searchingReplaceableSparePart}" selected="true" />
                </apex:panelGroup>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="{!$Label.SC_app_Date}" />
                <apex:inputField value="{!dateRequired.Start__c}" required="false" />
            </apex:pageBlockSectionItem>
            
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="{!$Label.SC_app_Stock}" />
                <apex:panelGroup layout="none">
                    <apex:inputField value="{!stockItemInput.Stock__c}" required="false" />
                </apex:panelGroup>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="{!$Label.SC_app_Plant}" />
                <apex:panelGroup layout="none">
                    <apex:inputField value="{!stock.Plant__c}" required="false" />
                </apex:panelGroup>
            </apex:pageBlockSectionItem>
          
            <apex:pageBlockSectionItem >
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="{!$Label.SC_app_BusinessUnit}" />
                <apex:panelGroup layout="none">
                    <apex:inputField value="{!businessUnit.Department__c}" required="false" />
                </apex:panelGroup>
            </apex:pageBlockSectionItem>
        </apex:pageBlockSection>
        
        <apex:pageBlockButtons location="bottom" id="buttons">
            <apex:commandButton action="{!onSearchArticle}" value="{!$Label.SC_btn_CheckMatAvail}" 
                                style="width:25%" status="searchArticles1" 
                                oncomplete="isValidArticleId = {!isValidArticleId};checkArticleId();" reRender="table"/>
            <apex:commandButton action="{!onSearchArticleExt}" value="{!$Label.SC_btn_CheckMatAvailExt}" 
                                style="width:25%" status="searchArticles1" rendered="{!isExternCheck}" 
                                oncomplete="isValidArticleId = {!isValidArticleId};checkArticleId();" reRender="extResult"/>
            <apex:actionStatus id="searchArticles1" startText="{!$Label.SC_app_Processing}" stopText="" startStyleClass="searching" />
        </apex:pageBlockButtons>
    </apex:pageBlock>
    
<!-- 
    searchField2 pageBlock is used on SCProcessOrder page
 -->
    <apex:pageBlock id="searchField2" rendered="{!isDisplaySearchField2}">
        <apex:actionStatus id="loadingArticles" startText="{!$Label.SC_app_Processing}" stopText="" startStyleClass="searching" stopStyleClass="searchFinished"/>

        <apex:pageBlockSection columns="1" rendered="{!isDisplaySearchField2}">
            <apex:panelGroup layout="none">
                <apex:outputLabel value="{!$Label.SC_app_ArticleSearchBox}: " style="font-weight:bold;"/>
                <apex:inputHidden id="returnArticleIdMatAv" value="{!articleId}"/>
                <apex:inputText id="returnArticleNameMatAv" value="{!articleName}" onkeypress="articleEnter(this, event)"/>
                <img src="/s.gif"
                     id="searchArticleBtn"
                     class="lookupIcon"
                     alt="{!JSENCODE($Label.SC_app_SpareParts)}"
                     onmouseover="this.className = 'lookupIconOn';this.className = 'lookupIconOn';"
                     onmouseout="this.className = 'lookupIcon';this.className = 'lookupIcon';"
                     onfocus="this.className = 'lookupIconOn';"
                     onblur="this.className = 'lookupIcon';"
                     onclick="openArticleSearch(this, '{!JSENCODE($Label.SC_app_SpareParts)}', 0, 'articleSelectedMatAv', {!articleSearchAutoStart})" />
                &nbsp;&nbsp;&nbsp;
                <apex:outputLabel value="{!$Label.SC_app_SimilarArticle}: " style="font-weight:bold;"/>
                <apex:inputCheckbox value="{!searchingReplaceableSparePart}" selected="true" />
                &nbsp;&nbsp;&nbsp;
                <apex:commandButton action="{!onSearchArticle}" value="{!$Label.SC_btn_CheckMatAvail}" style="width:200px" 
                                    status="searchArticles2" oncomplete="isValidArticleId = {!isValidArticleId};checkArticleId();" reRender="table"/>
                <apex:commandButton action="{!onSearchArticleExt}" value="{!$Label.SC_btn_CheckMatAvailExt}" style="width:200px" rendered="{!isExternCheck}" 
                                    status="searchArticles2" oncomplete="isValidArticleId = {!isValidArticleId};checkArticleId();" reRender="extResult"/>
                <apex:actionStatus id="searchArticles2" startText="{!$Label.SC_app_Processing}" stopText="" startStyleClass="searching" />
            </apex:panelGroup>
        </apex:pageBlockSection>
        
        <apex:pageBlockSection columns="2" rendered="{!isDisplaySearchField2}">
            <apex:pageBlockTable value="{!articlesExt}" var="article" rendered="{!NOT(ISNULL(articlesExt))}">
                <apex:column headerValue="{!$Label.SC_btn_select}" headerClass="selectColumn">
                    <apex:inputCheckbox value="{!article.isSelected}"/>
                </apex:column>
                <apex:column value="{!article.article.Name}" headerValue="{!$ObjectType.SCArticle__c.fields.Name.label}" />
                <apex:column value="{!article.article.ArticleNameCalc__c}" headerValue="{!$ObjectType.SCArticle__c.fields.ArticleNameCalc__c.label}" />
                <apex:column headerValue="{!$ObjectType.SCStockItem__c.fields.Qty__c.label}" >
                    <apex:outputText value="{0, number}" >
                        <apex:param value="{!article.qty}" />
                    </apex:outputText>
                </apex:column>
            </apex:pageBlockTable>
        </apex:pageBlockSection>
    </apex:pageBlock>

<!-- 
    The table below displays the result of the intern search
 -->
    <apex:pageBlock id="table">
        <apex:pageBlockSection columns="1" title="{!$Label.SC_app_InternResults}">
            <apex:pageBlockTable value="{!spareParts}" var="item" id="spareParts" rendered="{!isArticleTableRender}">
                <apex:column value="{!item.stockItem.Article__r.Name}" style="background-color : {! IF(item.isColorMark == true, '#F2F2F2', 'white') };"/>
                <apex:column value="{!item.stockItem.Article__r.ArticleNameCalc__c}" style="background-color : {! IF(item.isColorMark == true, '#F2F2F2', 'white') };"/>
                <apex:column value="{!item.replacesArticle}" headerValue="{!$Label.SC_app_Replaces}" style="background-color : {! IF(item.isColorMark == true, '#F2F2F2', 'white') };"/>
                <apex:column value="{!item.stockItem.Article__r.LockType__c}" style="background-color : {! IF(item.isColorMark == true, '#F2F2F2', 'white') };"/>
                <apex:column headerValue="{!$ObjectType.SCStockItem__c.fields.Qty__c.label}" style="background-color : {! IF(item.isColorMark == true, '#F2F2F2', 'white') };">
                    <apex:outputText value="{0, number}" >
                        <apex:param value="{!item.stockItem.Qty__c}" />
                    </apex:outputText>
                </apex:column>
                <apex:column value="{!item.stockItem.Stock__r.Name}" style="background-color : {! IF(item.isColorMark == true, '#F2F2F2', 'white') };"/>
                <apex:column value="{!item.stockItem.Stock__r.Plant__r.Name}" style="background-color : {! IF(item.isColorMark == true, '#F2F2F2', 'white') };"/>
                <apex:column value="{!item.resourceAssignment.Resource__r.FirstName__c} {!item.resourceAssignment.Resource__r.LastName__c}" 
                             headerValue="{!$Label.SC_app_CH_Name}"
                             style="background-color : {! IF(item.isColorMark == true, '#F2F2F2', 'white') };"/>
                <apex:column value="{!item.resourceAssignment.Resource__r.Mobile__c}" style="background-color : {! IF(item.isColorMark == true, '#F2F2F2', 'white') };"/>
                <apex:column headerValue="{!$Label.SC_app_AccAddressInfo}" style="background-color : {! IF(item.isColorMark == true, '#F2F2F2', 'white') };">
                    {!item.resourceAssignment.Street__c} {!item.resourceAssignment.HouseNo__c}, {!item.resourceAssignment.PostalCode__c} {!item.resourceAssignment.City__c}
                </apex:column>
            </apex:pageBlockTable>
            </apex:pageBlockSection>
    </apex:pageBlock>

<!-- 
    The table below displays the result of the extern search
 -->
    <apex:pageBlock id="extResult" rendered="{!isExternCheck}">
        <apex:pageBlockSection columns="1" title="{!$Label.SC_app_ExternResults}">
            <apex:pageMessage severity="warning" strength="2" title="{!errMsgExtern}" escape="false" 
                              rendered="{!NOT(ISNULL(errMsgExtern))}"/> 

            <apex:pageBlockTable value="{!resultData}" var="result" id="results">
                <apex:column headerValue="{!$ObjectType.SCArticle__c.fields.Name.label}" 
                             value="{!result.articleNr}"/>
                <apex:column headerValue="{!$ObjectType.SCStockItem__c.fields.Qty__c.label}">
                    <apex:outputText value="{0, number}" >
                        <apex:param value="{!result.qty}"/>
                    </apex:outputText>
                </apex:column>
                <apex:column headerValue="{!$Label.SC_app_DeliveryDate}" 
                             value="{!result.deliveryDateStr}"/>
            </apex:pageBlockTable>
        </apex:pageBlockSection>
        
        <apex:pageBlockSection columns="1" rendered="{!AND(hasResults, NOT(ISNULL(callback)))}">
            <apex:panelGroup layout="none">
                <apex:commandButton onclick="javascript: copyDateToOrder(); return false;" value="{!$Label.SC_btn_UseAsDesiredDate}"/>
                <apex:selectList id="deliveryDate" size="1" multiselect="false" >
                    <apex:selectOptions value="{!deliveryDates}"/>
                </apex:selectList>
            </apex:panelGroup>
        </apex:pageBlockSection>
    </apex:pageBlock>
</apex:actionRegion>

</apex:component>
