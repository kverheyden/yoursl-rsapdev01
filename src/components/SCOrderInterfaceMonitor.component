<!-- Component used to monitor SAP interface callouts and enable retry proesing -->
<!-- (c) 2012 GMS Development, Paderborn -->

<apex:component controller="SCOrderInterfaceMonitorController" id="mainComponent"  allowDML="true">
    <apex:attribute name="autopoll" description="true starts automatically to poll for the response" type="Boolean" default="false" required="false" assignTo="{!autopoll}"/>
    <apex:attribute name="showall" description="true show all items, fals only the creation" type="Boolean" default="true" required="false" assignTo="{!showall}"/>
    <apex:attribute name="oid" description="the order id defines the monitoring context" type="String" required="true" assignTo="{!oid}"/>

   <style>
        .waiting
            {
		    background:url(/img/loading.gif) no-repeat center center; 
			width: 166px;
			display: inline-block;
			padding: 0;
			height: 17px;
			margin: 0 0 -5px 0;        
            }
    </style>
    
<script>
    function disableButton(obj)
    {
        var d = document.getElementById(obj.id); 
        d.className = d.className + " btnDisabled"; 
        d.disabled = 'disabled';    
    }
</script>

<!--
    <apex:actionFunction name="setpollStatusTrue" reRender="pageblock" >
        <apex:param name="p1" assignTo="{!pollStatus}" value="true"/>
    </apex:actionFunction>
-->
    <!-- the monitoring page needs an external form -->
    <apex:pageBlock id="pageblock" mode="edit">
        <apex:pageBlockSection columns="{!IF(showall, 3, 2)}"  >
        
            <!-- The next button is used to simplify the interface processing and retry for end users -->
            <apex:outputPanel rendered="{!showall}">
                <!--apex:commandButton value="Next" action="{!OnCallNext}" style="width:51%" disabled="{!!orderCallNextActive || true}" reRender="pageblock,ordiflog" rendered="{!showall}"/-->
            </apex:outputPanel>
            <apex:outputfield value="{!ord.ERPOrderNo__c}" />
            <apex:outputText value="" rendered="{!showall}"/>

           
            <!-- Transaction [Order Create] -->
            <apex:outputPanel >
            
            <apex:actionStatus id="statusCreateOrder" startStyleClass="waiting">
                        <apex:facet name="stop">
                				<apex:commandButton value="{!$Label.SC_btn_CreateOrder}" 
                                    action="{!OnOrderCreate}" 
                                    style="width:35%" 
                                    disabled="{!!orderCreateActive}" 
                                    rendered="{!showall}" 
                                    reRender="pageblock,ordiflog" 
                                    onclick="disableButton(this);"
                                    status="statusCreateOrder"/>
                        </apex:facet>
            </apex:actionStatus>
                    

                                    
            <apex:actionStatus id="statusCreateOrderCancelRetry" startStyleClass="waiting">
                        <apex:facet name="stop">                                    
                			<apex:commandButton value="{!$Label.SC_btn_CancelRetry}" 
                                    title="cancel the idoc {!ord.IDocOrderCreate__c} and retry the transaction" 
                                    action="{!OnOrderCreateRetry}" 
                                    style="width:30%" 
                                    disabled="{!!orderCreateRetryActive}" 
                                    rendered="{!showall}" 
                                    reRender="pageblock,ordiflog" 
                                    onclick="disableButton(this);"
                                    status="statusCreateOrderCancelRetry"/>
                         </apex:facet>
            </apex:actionStatus>
                                               
                                    
                <apex:outputpanel rendered="{!ord.ERPStatusOrderCreate__c == 'pending'}">
                    <apex:commandButton action="{!OnOrderCreateReset}" 
                                        title="set transaction to none" image="/img/search_dismiss.gif" 
                                        rendered="{!pollingTimeoutReached}" 
                                        reRender="pageblock,ordiflog"
                                        onclick="disableButton(this);"
                                        />
                                        
                    <apex:image value="/img/staleValue.gif"/>
                </apex:outputpanel>
            </apex:outputPanel>
            <apex:outputfield value="{!ord.ERPStatusOrderCreate__c}" />
            <apex:outputfield value="{!ord.IDocOrderCreate__c}" rendered="{!showall}"/>

            <!-- Transaction [Order Update Equipment] -->
            <apex:outputPanel rendered="{!showall}">
            	<apex:actionStatus id="statusUpdateOrder" startStyleClass="waiting">
                        <apex:facet name="stop">
                		<apex:commandButton value="{!$Label.SC_btn_UpdateOrder}" 
                                    action="{!OnOrderUpdateEquipment}" 
                                    style="width:35%" 
                                    disabled="{!!orderEquipmentUpdateActive}" 
                                    reRender="pageblock,ordiflog" 
                                    onclick="disableButton(this);"
                                    status="statusUpdateOrder"/>
                        </apex:facet>
                </apex:actionStatus>
            
            
            	<apex:actionStatus id="statusUpdateOrderCancelRetry" startStyleClass="waiting">
                        <apex:facet name="stop">
                			<apex:commandButton value="{!$Label.SC_btn_CancelRetry}" 
                                    action="{!OnOrderUpdateEquipmentRetry}" 
                                    style="width:30%" 
                                    disabled="{!!orderEquipmentUpdateRetryActive}" 
                                    reRender="pageblock,ordiflog" 
                                    onclick="disableButton(this);"
                                    status="statusUpdateOrderCancelRetry"
                                    />
                        </apex:facet>
                </apex:actionStatus>
                
                
                <apex:outputpanel rendered="{!ord.ERPStatusEquipmentUpdate__c == 'pending' || ord.ERPStatusEquipmentUpdate__c == 'error'}">
                    <apex:commandButton action="{!OnOrderUpdateEquipmentReset}" 
                                        title="set transaction to none" 
                                        image="/img/search_dismiss.gif" 
                                        rendered="{!pollingTimeoutReached || ord.ERPStatusEquipmentUpdate__c == 'error'}" 
                                        reRender="pageblock,ordiflog"
                                        onclick="disableButton(this);"
                                        status="prepollstat"
                                        />
                    <apex:image value="/img/staleValue.gif" rendered="{!ord.ERPStatusEquipmentUpdate__c == 'pending'}"/>
                </apex:outputpanel>
            </apex:outputPanel>
            <apex:outputfield value="{!ord.ERPStatusEquipmentUpdate__c}" rendered="{!showall}"/>
            <apex:outputfield value="{!ord.IDocOrderUpdate__c}" rendered="{!showall}"/>

            <!-- Transaction [Material reservations - order related] -->
            <apex:outputPanel rendered="{!showall}">
            
            	<apex:actionStatus id="statusReserveMaterial" startStyleClass="waiting">
                        <apex:facet name="stop">      
                			<apex:commandButton value="{!$Label.SC_btn_ReserveMaterial}" 
                                    action="{!OnOrderMaterialReservation}" 
                                    style="width:35%" 
                                    disabled="{!!orderMaterialReservationActive}" 
                                    reRender="pageblock,ordiflog"
                                    onclick="disableButton(this);"
                                    status="statusReserveMaterial"
                                    />
                         </apex:facet>
                </apex:actionStatus>
                                                   
            	<apex:actionStatus id="statusReserveMaterialReset" startStyleClass="waiting">
                        <apex:facet name="stop">                                        
                			<apex:commandButton value="Reset"
                                    action="{!OnMaterialReservationReset}" 
                                    style="width:30%" 
                                    disabled="{!!orderMaterialReservationRetryActive}" 
                                    reRender="pageblock,ordiflog"
                                    onclick="disableButton(this);"
                                    status="statusReserveMaterialReset"
                                    />
                         </apex:facet>
                </apex:actionStatus>
                
                <apex:outputpanel rendered="{!ord.ERPStatusMaterialReservation__c == 'pending' || ord.ERPStatusMaterialReservation__c == 'error'}">
                    <apex:commandButton action="{!OnMaterialReservationReset}" 
                                        title="set transaction to none" 
                                        image="/img/search_dismiss.gif" 
                                        rendered="{!pollingTimeoutReached || ord.ERPStatusMaterialReservation__c == 'error'}" 
                                        reRender="pageblock,ordiflog"
                                        onclick="disableButton(this);"
                                        />
                    <apex:image value="/img/staleValue.gif" rendered="{!ord.ERPStatusMaterialReservation__c == 'pending'}"/>
                </apex:outputpanel>
            </apex:outputPanel>
            <apex:outputfield value="{!ord.ERPStatusMaterialReservation__c}" rendered="{!showall}"/>
            <apex:outputText value="{!$Label.SC_btn_MultipleIDocs}" rendered="{!showall}" style="text-align: right;"/>

            <!-- Transaction [Material consumption - order related] -->
            <apex:outputPanel rendered="{!showall}">
            
            	<apex:actionStatus id="statusMoveMaterial" startStyleClass="waiting">
                        <apex:facet name="stop">                
                			<apex:commandButton value="{!$Label.SC_btn_MoveMaterial}" 
                                    action="{!OnOrderMaterialConsumption}" 
                                    style="width:35%" 
                                    disabled="{!!orderMaterialConsumptionActive}" 
                                    reRender="pageblock,ordiflog"
                                    onclick="disableButton(this);"
                                    status="statusMoveMaterial"
                                    />
                         </apex:facet>
                </apex:actionStatus>
            	<apex:actionStatus id="statusMoveMaterialRetry" startStyleClass="waiting">
                        <apex:facet name="stop">                                                      
                			<apex:commandButton value="Reset"
                                    action="{!OnMaterialReset}" 
                                    style="width:30%" 
                                    disabled="{!!orderMaterialConsumptionRetryActive}" 
                                    reRender="pageblock,ordiflog"
                                    onclick="disableButton(this);"
                                    status="statusMoveMaterialRetry"
                                    />
                         </apex:facet>
                </apex:actionStatus>
                
                <apex:outputpanel rendered="{!ord.ERPStatusMaterialMovement__c == 'pending' || ord.ERPStatusMaterialMovement__c == 'error'}">
                    <apex:commandButton action="{!OnMaterialReset}" 
                                        title="set transaction to none" 
                                        image="/img/search_dismiss.gif" 
                                        rendered="{!pollingTimeoutReached || ord.ERPStatusMaterialMovement__c == 'error'}" 
                                        reRender="pageblock,ordiflog"
                                        onclick="disableButton(this);"
                                        status="prepollstat"
                                        />
                    <apex:image value="/img/staleValue.gif" rendered="{!ord.ERPStatusMaterialMovement__c == 'pending'}"/>
                </apex:outputpanel>
            </apex:outputPanel>
            <apex:outputfield value="{!ord.ERPStatusMaterialMovement__c}" rendered="{!showall}"/>
            <apex:outputText value="{!$Label.SC_btn_MultipleIDocs}" rendered="{!showall}" style="text-align: right;"/>

            <!-- Transaction [Add external operation] rendered="[!ExtOpAdd2Transfer}" -->
            <apex:outputPanel rendered="{!showall}">
            
            

            	<apex:actionStatus id="statusExtAdd" startStyleClass="waiting">
                        <apex:facet name="stop">  
                			<apex:commandButton value="{!$Label.SC_btn_ExtAdd} {!ordpendingExtOpAdd}" 
                                    action="{!OnOrderExtOpAdd}" 
                                    style="width:35%" 
                                    disabled="{!!OrderExtOpAddActive}" 
                                    reRender="pageblock,ordiflog"
                                    onclick="disableButton(this);"
                                    status="statusExtAdd"
                                    />
                		</apex:facet>
                </apex:actionStatus> 
             	<apex:actionStatus id="statusExtAddCancelRetry" startStyleClass="waiting">
                        <apex:facet name="stop">                                                  
                			<apex:commandButton value="{!$Label.SC_btn_CancelRetry}"  
                                    action="{!OnOrderExtOpAddRetry}" 
                                    style="width:30%" 
                                    disabled="{!!OrderExtOpAddRetryActive}" 
                                    reRender="pageblock,ordiflog"
                                    onclick="disableButton(this);"
                                    status="statusExtAddCancelRetry"
                                    />
                		</apex:facet>
                </apex:actionStatus> 
                
                <apex:outputpanel rendered="{!ord.ERPStatusExternalAssignmentAdd__c == 'pending' || ord.ERPStatusExternalAssignmentAdd__c == 'error'}">
                    <apex:commandButton action="{!OnOrderExtOpAddReset}" 
                                        title="set transaction to none" 
                                        image="/img/search_dismiss.gif" 
                                        rendered="{!pollingTimeoutReached || ord.ERPStatusExternalAssignmentAdd__c == 'error'}" 
                                        reRender="pageblock,ordiflog"
                                        onclick="disableButton(this);"
                                        />
                    <apex:image value="/img/staleValue.gif" rendered="{!ord.ERPStatusExternalAssignmentAdd__c == 'pending'}"/>
                </apex:outputpanel>

            </apex:outputPanel>
            <apex:outputfield value="{!ord.ERPStatusExternalAssignmentAdd__c}" rendered="{!showall}"/>
            <apex:outputfield value="{!ord.IDocOrderExOpAdd__c}"  rendered="{!showall}"/>


            <!-- Transaction [Remove external operation] rendered="[!ExtOpRem2Transfer}" -->
            <apex:outputPanel rendered="{!showall}">

             	<apex:actionStatus id="statusExtRem" startStyleClass="waiting">
                        <apex:facet name="stop">              
                			<apex:commandButton value="{!$Label.SC_btn_ExtRem} {!ordpendingExtOpRem}" 
                                    action="{!OnOrderExtOpRem}" 
                                    style="width:35%" 
                                    disabled="{!!OrderExtOpRemActive}" 
                                    reRender="pageblock,ordiflog"
                                    onclick="disableButton(this);"
                                    status="statusExtRem"
                                    />
                		</apex:facet>
                </apex:actionStatus> 
             	<apex:actionStatus id="statusExtRemCancelRetry" startStyleClass="waiting">
                        <apex:facet name="stop">                                      
                				<apex:commandButton value="{!$Label.SC_btn_CancelRetry}" 
                                    action="{!OnOrderExtOpRemRetry}" 
                                    style="width:30%" 
                                    disabled="{!!OrderExtOpRemRetryActive}" 
                                    reRender="pageblock,ordiflog"
                                    onclick="disableButton(this);"
                                    status="statusExtRemCancelRetry"
                                    />
                		</apex:facet>
                </apex:actionStatus> 
                <apex:outputpanel rendered="{!ord.ERPStatusExternalAssignmentRem__c == 'pending' || ord.ERPStatusExternalAssignmentRem__c == 'error'}">
                    <apex:commandButton action="{!OnOrderExtOpRemReset}" 
                                        title="set transaction to none" 
                                        image="/img/search_dismiss.gif" 
                                        rendered="{!pollingTimeoutReached || ord.ERPStatusExternalAssignmentRem__c == 'error'}" 
                                        reRender="pageblock,ordiflog"
                                        onclick="disableButton(this);"
                                        />
                    <apex:image value="/img/staleValue.gif" rendered="{!ord.ERPStatusExternalAssignmentRem__c == 'pending'}"/>
                </apex:outputpanel>
            </apex:outputPanel>
            <apex:outputfield value="{!ord.ERPStatusExternalAssignmentRem__c}" rendered="{!showall}"/>
            <apex:outputfield value="{!ord.IDocOrderExOpRem__c}" rendered="{!showall}" />


            <!-- Transaction [Close or cancel order] -->
            <apex:outputPanel rendered="{!showall}">
            

             	<apex:actionStatus id="statusCloseOrder" startStyleClass="waiting">
                        <apex:facet name="stop">                
                			<apex:commandButton value="{!$Label.SC_btn_CloseOrder}" 
                                    action="{!OnOrderClose}" 
                                    style="width:35%"  
                                    disabled="{!!orderCloseActive}" 
                                    reRender="pageblock,ordiflog"
                                    onclick="disableButton(this);"
                                    status="statusCloseOrder"
                                    />
                		</apex:facet>
                </apex:actionStatus> 
             	<apex:actionStatus id="statusCloseOrderCancelRetry" startStyleClass="waiting">
                        <apex:facet name="stop">                                        
                			<apex:commandButton value="{!$Label.SC_btn_CancelRetry}" 
                                    action="{!OnOrderCloseRetry}" 
                                    style="width:30%" 
                                    disabled="{!!orderCloseRetryActive}" 
                                    reRender="pageblock,ordiflog"
                                    onclick="disableButton(this);"
                                    status="statusCloseOrderCancelRetry"
                                    />
                		</apex:facet>
                </apex:actionStatus> 
                <!--apex:commandButton value="Cancel Order" action="{!OnOrderCancel}" style="width:25%"  disabled="{!!orderCancelActive}" reRender="pageblock,ordiflog"/-->
                <apex:outputpanel rendered="{!ord.ERPStatusOrderClose__c == 'pending' || ord.ERPStatusOrderClose__c == 'error'}">
                    <apex:commandButton action="{!OnOrderCloseReset}" 
                                        title="set transaction to none" 
                                        image="/img/search_dismiss.gif" 
                                        rendered="{!pollingTimeoutReached || ord.ERPStatusOrderClose__c == 'error'}" 
                                        reRender="pageblock,ordiflog"
                                        onclick="disableButton(this);"
                                        />
                    <apex:image value="/img/staleValue.gif" rendered="{!ord.ERPStatusOrderClose__c == 'pending'}"/>
                </apex:outputpanel>
            </apex:outputPanel>
            <apex:outputfield value="{!ord.ERPStatusOrderClose__c}" rendered="{!showall}"/>
            <apex:outputfield value="{!ord.IDocOrderClose__c}"  rendered="{!showall}"/>


            <!-- Transaction [ArchiveDocumentInsert] -->
            <apex:outputPanel rendered="{!showall}">

             	<apex:actionStatus id="statusArchiveDoc" startStyleClass="waiting">
                        <apex:facet name="stop">              
                <apex:commandButton value="{!$Label.SC_btn_ArchiveDoc}" 
                                    action="{!OnArchiveDocument}" 
                                    style="width:35%"  
                                    disabled="{!!ArchiveDocumentActive}" 
                                    reRender="pageblock,ordiflog"
                                    onclick="disableButton(this);"
                                    status="statusArchiveDoc"
                                    />
                		</apex:facet>
                </apex:actionStatus> 
             	<apex:actionStatus id="statusArchiveDocCancelRetry" startStyleClass="waiting">
                        <apex:facet name="stop">                                      
                <apex:commandButton value="{!$Label.SC_btn_CancelRetry}" 
                                    action="{!OnArchiveDocumentRetry}" 
                                    style="width:30%" 
                                    disabled="{!!ArchiveDocumentRetryActive}" 
                                    reRender="pageblock,ordiflog"
                                    onclick="disableButton(this);"
                                    status="statusArchiveDocCancelRetry"
                                    />
                		</apex:facet>
                </apex:actionStatus>                                     
                <!--apex:commandButton value="Cancel Order" action="{!OnOrderCancel}" style="width:25%"  disabled="{!!orderCancelActive}" reRender="pageblock,ordiflog"/-->
                <apex:outputpanel rendered="{!ord.ERPStatusArchiveDocumentInsert__c == 'pending' || ord.ERPStatusArchiveDocumentInsert__c == 'error'}">
                    <apex:commandButton action="{!OnArchiveDocumentReset}" 
                                        title="set transaction to none" 
                                        image="/img/search_dismiss.gif" 
                                        rendered="{!pollingTimeoutReached || ord.ERPStatusArchiveDocumentInsert__c == 'error'}" 
                                        reRender="pageblock,ordiflog"
                                        onclick="disableButton(this);"
                                        />
                    <apex:image value="/img/staleValue.gif" rendered="{!ord.ERPStatusArchiveDocumentInsert__c == 'pending'}"/>
                </apex:outputpanel>
            </apex:outputPanel>
            <apex:outputfield value="{!ord.ERPStatusArchiveDocumentInsert__c}" rendered="{!showall}"/>
            <apex:outputText value="{!$Label.SC_btn_MultipleIDocs}" rendered="{!showall}" style="text-align: right;"/>

            <apex:outputPanel />
            <apex:outputPanel />
            <apex:outputPanel />

            <!-- used by the action poller to show the status progress-->   
            <apex:pageMessage summary="Warte auf [{!currentTransaction}Response]..." severity="info" strength="1" rendered="{!pollStatus}"/>
            <apex:pageMessage summary="Transaktionsfehler: [{!FailedTransactions}] - Bitte Daten prüfen und erneut versuchen" severity="warning" strength="1" rendered="{!errorStatus && !pollStatus}"/>

            <!-- required to show the user an immediate response when pressing a button-->      
            <apex:actionStatus id="prepollstat" >
                <apex:facet name="start">
                    <apex:pageMessage summary="Warte auf [{!currentTransaction}Response]..." severity="info" strength="1"/>
                </apex:facet>
            </apex:actionStatus>
                               
            <apex:actionStatus id="pollstat" 
                               startText="......" 
                               stopText="."/>

        </apex:pageBlockSection>
        <apex:pageBlockSection title="{!$Label.SC_app_NoteMobileOrders}" rendered="{!showall}" columns="1">
            <apex:pageMessage summary="Überprüfung der vom Techniker zurückgemeldeten Auftragsdaten erforderlich. <BR/>Bitte Schnittstellen-Clearing durchführen, damit der Auftrag abgeschlossen werden kann." 
                              escape="false"  severity="warning" strength="1" rendered="{!isInClearing}"/>

            <apex:pageBlockTable value="{!ord.Interface_Clearing__r}" var="v">
                <apex:column >
                    <apex:outputLink value="/{!v.id}" target="_blank">{!v.name}</apex:outputLink>
                </apex:column>
                <apex:column value="{!v.lastmodifieddate}"/>
                <apex:column value="{!v.Resource__r.name}"/>
                <apex:column value="{!v.Type__c}"/>
                <apex:column value="{!v.Status__c}"/>
                <apex:column value="{!v.ResultInfo__c}"/>
            </apex:pageBlockTable>
        </apex:pageBlockSection>
        <apex:actionPoller action="{!OnPollStatus}" oncomplete="changeLinkTarget()" reRender="testabc,pageblock,ordiflog" interval="5" enabled="{!pollStatus}" status="pollstat" />
    </apex:pageBlock>

<apex:outputPanel id="testabc">
    <c:SCRelatedList id="ordiflog" object="SCInterfaceLog__c" refid="{!ord.id}" reffield="order__c" fieldset="orderoverviewlist" orderby="name desc" rendered="{!showall}"/>
    <c:SCRelatedList id="ordmatmov" object="SCMaterialMovement__c" refid="{!ord.id}" reffield="order__c" fieldset="orderoverviewlist" orderby="name desc" rendered="{!showall}"/>

</apex:outputPanel>
<script>
    //PMS 35966/GMS: Schnittstellenansicht verzogen, öffnet intern neues Fenster
	//always create a target blank (show in new tab) for all hyperlinks
	function changeLinkTarget()
	{
		var links = document.getElementsByTagName("a");
		for (var i = 0; i < links.length; i++) 
		{
			links[i].target = "_blank";
		}
	}
	changeLinkTarget();
</script>

</apex:component>
