<apex:component controller="cc_cceag_ctrl_CheckoutOneStep" allowdml="true">
    <script id="AGCheckoutHeader-Desktop" type="text/template">
    </script>

    <script id="CheckoutOneStep-Desktop" type="text/template">
        <form class="checkoutOneStep">
            <div class="widget widget-alert widget-alert-alarm alarm-terms" style="display: none">
                <div>
                    <div>
                        <p>Bitte akzeptieren Sie die AGB</p>
                    </div>
                </div>
            </div>
            <div class="widget widget-alert widget-alert-alarm alarm-orderId" style="display: none">
                <div>
                    <div>
                        <p class="orderIdError"></p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="widget widget-padded">
                        <h2 class="ico"><em class="ico ico-id"></em>Eigene Bestellnummer</h2>
                        <p>Sie können dieser Bestellung eine eigene Order-ID zuweisen, damit Sie diese Bestellung besser verwalten können.</p>
                        <div class="group group-order-id">
                            <label for="order-id">Order-ID</label>
                            <input type="text" name="order-id" id="order-id" placeholder="Ihre Order-ID ..." value="" class="framed" maxlength="30"/>
                        </div>
                    </div>

                    {{#ifEquals shippingMethod 'Pickup'}}
                        <div class="widget widget-padded">
                            <h2 class="ico"><em class="ico ico-clock"></em>Geplante Uhrzeit für die Abholung</h2>
                            <div class="group group-date">
                                <label for="collect-date">Abholwunsch:</label>
                                <input type="text" name="collect-date" id="collect-date" value="{{formatDate requestedDate}}" readonly="readonly" class="readonly" />
                            </div>
                            <div class="group group-time">
                                <label for="collect-time">Uhrzeit:</label>
                                <select name="collect-time" id="collect-time" class="select2 no-searchable searchable" data-placeholder="Bitte wählen">
                                    <option value="08:00 - 08:30">08:00 - 08:30</option>
                                    <option value="08:30 - 09:00">08:30 - 09:00</option>
                                    <option value="09:00 - 09:30">09:00 - 09:30</option>
                                    <option value="09:30 - 10:00">09:30 - 10:00</option>
                                    <option value="10:00 - 10:30">10:00 - 10:30</option>
                                    <option value="10:30 - 11:00">10:30 - 11:00</option>
                                    <option value="11:00 - 11:30">11:00 - 11:30</option>
                                    <option value="11:30 - 12:00">11:30 - 12:00</option>
                                    <option value="12:00 - 12:30">12:00 - 12:30</option>
                                    <option value="12:30 - 13:00">12:30 - 13:00</option>
                                    <option value="13:00 - 13:30">13:00 - 13:30</option>
                                    <option value="13:30 - 14:00">13:30 - 14:00</option>
                                    <option value="14:00 - 14:30">14:00 - 14:30</option>
                                    <option value="14:30 - 15:00">14:30 - 15:00</option>
                                    <option value="15:00 - 15:30">15:00 - 15:30</option>
                                    <option value="15:30 - 16:00">15:30 - 16:00</option>
                                    <option value="16:00 - 16:30">16:00 - 16:30</option>
                                    <option value="16:30 - 17:00">16:30 - 17:00</option>
                                </select>
                            </div>
                        </div>
                    {{/ifEquals}}
                </div>

                <div class="col-sm-6">
                    <div class="widget widget-padded">
                        <h2 class="ico"><em class="ico ico-edit"></em>Bemerkung zur Lieferung</h2>
                        <p>Hier können Sie uns eine Nachricht bezüglich Ihrer Bestellung mitteilen.</p>
                        <div class="group group-message">
                            <label for="message">Nachricht</label>
                            <textarea name="message" id="message" placeholder="Bemerkung zur Lieferung ..."></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row equalsheight">
                <div class="col-sm-6">
                    <div class="widget widget-box">
                        {{#ifEquals shippingMethod 'Pickup'}}
                            <h2 class="ico"><em class="ico ico-truck"></em>Abholadresse</h2>
                            <p>Abholung Ihrer Bestellung unter der folgenden Adresse:</p>
                        {{else}}
                            <h2 class="ico"><em class="ico ico-truck"></em>Lieferung</h2>
                            <p>Ihr Bestellung wird an folgende Adresse geliefert:</p>
                        {{/ifEquals}}
                        <p>
                            {{#ifEquals shippingMethod 'Delivery'}} {{shippingAddress.companyName}} {{/ifEquals}}<br />
                            {{shippingAddress.address1}}, {{shippingAddress.postalCode}} {{shippingAddress.city}}
                        </p>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="widget widget-box">
                        <h2 class="ico"><em class="ico ico-invoice"></em>Rechnung</h2>
                        <p>
                            Ihre Rechnung erhalten Sie nach Lieferung an die folgende Adresse:
                        </p>
                        <p>
                            <b>Rechnungsadresse:</b><br />
                            {{billingAddress.companyName}}<br />
                            {{billingAddress.address1}}, {{billingAddress.postalCode}} {{billingAddress.city}}
                        </p>
                    </div>
                </div>
            </div>

            {{#if terms.length}}
                <div class="widget">
                    <h2 class="nomargin">Geschäftsbedingungen akzeptieren</h2>
                    {{#each terms}}
                        <div class="group group-checkbox">
                            <label for="flag-toc{{id}}">{{{description}}}</label>
                            <input type="checkbox" class="termscb" id="flag-toc{{id}}" name="flag-toc{{id}}" value="true" />
                        </div>
                    {{/each}}
                    <p style="padding: 5px 0 0 20px">
                        Bitte haben Sie Verständnis dafür, daß alle aufgegebenen Bestellungen unter
                        dem ausdrücklichen Vorbehalt stehen, dass bei der CCE AG die entsprechenden
                        Produktions- und Distributionskapazitäten im Einzelfall vorhanden sind.
                    </p>
                </div>
            {{/if}}

            <div class="row">
                <div class="widget col-sm-7">
                    <h2 class="nomargin">Kundennewsletter bestellen</h2>
                    <div class="group group-checkbox">
                        <label for="flag-newsletter">Ich möchte den Kundennewsletter der CCE AG bestellen.</label>
                        <input type="checkbox" id="flag-newsletter" name="flag-newsletter" value="true" />
                    </div>
                </div>

                <!-- WIDGET - CALL TO ACTION -->
                <div class="widget widget-cta widget-cta-order col-sm-5">
                    <button type="button" class="placeOrder" >Bestellung absenden</button>
                </div>
                <!-- WIDGET - CALL TO ACTION -->
            </div>
        </form>
    </script>

    <script>
        jQuery(function($) {
            CCRZ.views.CheckoutOneStepView = CCRZ.CloudCrazeView.extend({
                templatePhone : CCRZ.util.template("CheckoutOneStep-Desktop"),
                templateDesktop : CCRZ.util.template("CheckoutOneStep-Desktop"),
                viewName : "CheckoutOneStepView",
                managedSubView : true,
                navIndex : 0,
                events : {
                    "click .placeOrder" : "placeOrder",
                    "click .processReview" : "processReview",
                    "click .termscb" : "processTerms"
                },
                init: function() {
                    this.dataSet = { orderIdRequired: false };
                },
                initSetup: function(callback) {
                    var v = this;
                    Visualforce.remoting.Manager.invokeAction('{!$RemoteAction.cc_cceag_ctrl_CheckoutOneStep.fetchAccountParams}', CCRZ.pagevars.remoteContext, function(response) {
                        if (response.success) {
                            v.dataSet = response.data;
                        }
                        callback();
                    }, {buffer: false });
                },
                renderPhone : function() {
                    this.renderView(this.templatePhone);
                },
                renderDesktop : function() {
                    this.renderView(this.templateDesktop);
                },
                renderView: function(currTemplate) {
                    var v = this;
                    v.$el.html(currTemplate(CCRZ.cartCheckoutModel.toJSON()));
                    CCRZ.pubSub.trigger("view:"+v.viewName+":refresh", this);
                    $(".group input[type='checkbox']").on("change", function(event) {
                        var $that = $(this);
                        var $parent = $that.parent(".group");

                        if ($that.is(":checked")) {
                            $parent.addClass("active");
                        } else {
                            $parent.removeClass("active");
                        }
                    });
                    if ($.fn.equalHeights !== undefined && $(".equalsheight div.widget, .equalsheight div.article").length > 0) {
                        $(".equalsheight div.widget, .equalsheight div.article").equalHeights();
                        $(window).resize(function () {
                            $(".equalsheight div.widget, .equalsheight div.article").css("height", "auto");
                            $(".equalsheight div.widget, .equalsheight div.article").equalHeights();
                        });
                    }
                    if ($.fn.equalHeights !== undefined && $(".equalsheight-2 div.article").length > 0) {
                        $(".equalsheight-2 div.article").equalHeights();
                        $(window).resize(function () {
                            $(".equalsheight-2 div.article").css("height", "auto");
                            $(".equalsheight-2 div.article").equalHeights();
                        });
                    }
                    $("button.action-send").on("click", function(event) {
                        var $step1 = $(this).parents(".step1");
                        var $step2 = $step1.next();
                        $step1.hide();
                        $step2.show();
                    });
                    $("select.select2.no-searchable").select2({
                        "dropdownMarginRight": 28,
                        "minimumResultsForSearch": -1
                    });
                },
                processReview: function(event) {
                    CCRZ.cartCheckoutView.slideRight();
                },
                processTerms: function(event) {
                    if ($('.widget-alert-alarm.alarm-terms').is(':visible')) {
                        var terms = $('.termscb').length;
                        var checkedTerms = $('.termscb:checked').length;
                        if (terms == 0 || terms == checkedTerms)
                            $('.widget-alert-alarm.alarm-terms').hide();
                    }
                },
                placeOrder: function(event) {
                    var v = this;
                    var terms = $('.termscb').length;
                    var checkedTerms = $('.termscb:checked').length;
                    var orderId = $('#order-id').val();
                    var hasErrors = false;
                    if (v.dataSet.orderIdRequired) {
                        if (orderId.length != v.dataSet.orderIdLength) {
                            $('.orderIdError').html(CCRZ.processPageLabelMap('OrderIDRequired', [ v.dataSet.orderIdLength ]).string);
                            $('.widget-alert-alarm.alarm-orderId').show();
                            hasErrors = true;
                        }
                        else
                            $('.widget-alert-alarm.alarm-orderId').hide();
                    }
                    else 
                        $('.widget-alert-alarm.alarm-orderId').hide();
                    if (!hasErrors) {
                        if (terms == 0 || terms == checkedTerms) {
                            $('.widget-alert-alarm.alarm-terms').hide();
                        }
                        else {
                            $('.widget-alert-alarm.alarm-terms').show();
                            hasErrors = true;
                        }
                    }

                    if (!hasErrors) {
                        $("#content .row").append(Handlebars.compile($("#loadingOverlay").html())());
                        var paymentData = new Object();
                        if (v.dataSet.paymentType && v.dataSet.paymentType == '111')
                            paymentData.paymentMethod = 'Cash';
                        else
                            paymentData.paymentMethod = 'Invoice';
                        paymentData.accountNumber = 'Internal';
                        v.className = 'cc_ctrl_CheckoutRD';
                        v.invokeContainerLoadingCtx($('.checkoutContent'), 'placeOrder', paymentData, CCRZ.cartCheckoutModel.attributes.sfid, 
                            function(response){
                                if (response.success){
                                    var orderNum = response.data;
                                    var note = $('#message').val();
                                    var newsletter = $('#flag-newsletter').is(':checked');
                                    var deliveryTime = "";
                                    if (CCRZ.cartCheckoutModel.attributes.shippingMethod == 'Pickup')
                                        deliveryTime = $("#collect-time").val();
                                    v.className = 'cc_cceag_ctrl_CheckoutOneStep';
                                    Visualforce.remoting.Manager.invokeAction('{!$RemoteAction.cc_cceag_ctrl_CheckoutOneStep.save}', CCRZ.pagevars.remoteContext, orderNum, orderId, note, deliveryTime, newsletter, paymentData.paymentMethod,
                                        function(response){
                                            if(response.success){
                                                $("#content .row").append(Handlebars.compile($("#loadingOverlay").html())());
                                                orderDetails(orderNum);
                                            }else{
                                                //@TODO
                                            }
                                        }
                                    );
                                }else{
                                    //@TODO
                                }
                            }
                        );
                    }
                    else {
                        window.scroll(0, 1);
                        $("html, body").animate({ scrollTop: 0 }, "slow");
                    }
                }
            });
        });
    </script>
</apex:component>
