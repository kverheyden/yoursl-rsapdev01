/*
 * @(#)GenericResponseHookExample.cls 
 * 
 * Copyright 2014 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * ===============================================================================================================================
 * Note: Please do not change this class. It is the template for YourSL team, that ####-> can be copied and then the copy can be changed.
 * ===============================================================================================================================
 * This is an example class.
 *
 * This class is dynamically instantiated in the CCWSGenericResponse in the case of SAP operation that are to be worked out by
 * YourSL software. 
 * The name of the class is to be defined in the CCSettings__c.GenericResponseHookClassName__c.
 *
 * This class is used by GMS test class GenericResponseHookExampleTest
 * 
 * The function 'process' makes a frame work that takes full input structure from SAP searches the input structure for important information
 * and allow to call two kinds of methods:
 * - methods to work out the list of references get from SAP
 * - methods to work out of single references get from SAP
 *
 * This function also logs the interface in the SCInterfaceLog__c record.   
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
global without sharing class GenericResponseHookExample implements GenericResponseHookInterface
{
	// used by the GMS test class GenericResponseHookExmapleTest
	final public static String extraListOperationImplementedByYourSL = 'operation implemented by YourSL for working out of lists';
	final public static String extraSingleOperationImplementedByYourSL = 'single operation implemented by YourSL';
	final public static String extraIdTypeForSingleOperation = 'id type to be determined by SAP and YourSL';
	/**
	 *	Processes the message from SAP.
	 * 
	 *
	 */
	global void process(CCWSGenericResponse.GenericServiceResponseMessageClass GenericServiceResponseMessage)
	{
        System.debug('GenericResponseHookExample.process');
        List<SCInterfaceLog__c> interfaceLogList = new List<SCInterfaceLog__c>();
        String messageID = null;
        String requestMessageID = null;
        String interfaceName = 'SAP_GENERIC_RESPONSE';
        String interfaceHandler = 'GenericResponseHookExample';
        String type = 'INBOUND';
        String direction = 'inbound';
        ID referenceID = null;
        String refType = null;
        ID referenceID2 = null;
        String refType2 = '';
        ID responseID = null;
        String resultCode = 'E000';
        String resultInfo = 'Success'; 
        Datetime start = DateTime.now(); 
        Integer count = 0;
        CCWSGenericResponse.LogItemClass[] logItemArr = null;
		SCInterfaceLog__c interfaceLogResponse;
        
        // create and write an interface log object
        // we need the id of the interface log object to write into the SCMaterialMovement__c.
        String jsonInput = JSON.serialize(GenericServiceResponseMessage);
        SCInterfaceBase ib = new SCInterfaceBase();
        String fromJSONMap = ib.getDataFromJSON(jsonInput);
        debug('from json: ' + fromJSONMap);
        String data = fromJSONMap;
        if(data.length() > 32000)
        {
        	data = data.left(32000);
        }
        interfaceLogResponse = SCInterfaceBase.addInterfaceLog(interfaceLogList, interfaceName, interfaceHandler, type, direction, messageID,
              referenceID, refType, referenceID2, refType2, responseID, resultCode, resultInfo, 
              data, start, count);            
        debug('GenericResponseHookExample initial interfaceLogResponse:' + interfaceLogResponse);
        upsert interfaceLogList;

        debug('GenericResponseHookExample GenericServiceResponseMessage: ' + GenericServiceResponseMessage);
        if(GenericServiceResponseMessage != null)
        {
            messageID = null;
            if(GenericServiceResponseMessage.MessageHeader != null)
            {
                // get message id
                messageID = GenericServiceResponseMessage.MessageHeader.MessageID;
                if(messageID != null)
                {
                    messageID = messageID.trim();
                }   
                // get the message id of the previous outgoing call
                requestMessageID = GenericServiceResponseMessage.MessageHeader.ReferenceID;
                debug('GenericResponseHookExample requestMessageID: ' + requestMessageID);
                if(requestMessageID != null)
                {
                    requestMessageID = requestMessageID.trim();
                }   
            }
            else
            {
                throw new SCfwException('MessageID is null');
            }
            if(GenericServiceResponseMessage.References != null)
            {
                // get the operation name
                String operation = GenericServiceResponseMessage.References.Operation;
                if(operation != null)
                {
                    operation = operation.trim();
                }
                // get the head external id
                String headExternalID = GenericServiceResponseMessage.References.ExternalID;
                if(headExternalID != null)
                {
                    headExternalID = headExternalID.trim();
                }
                debug('GenericResponseHookExample operation: ' + operation + ', headExternalID: ' + headExternalID);
                if(operation != null && operation != ''
                   && headExternalID != null && headExternalID != '')
                {   
					
					if(GenericServiceResponseMessage.References.References == null)
					{
						GenericServiceResponseMessage.References.References = CCWSGenericResponse.getDefaultReferences(GenericServiceResponseMessage.References, operation, headExternalID);
					}
                    if(GenericServiceResponseMessage.References.References != null)
                    {
                        if(GenericServiceResponseMessage.References.References.Reference != null)
                        {
                            // get and save the idoc number of the idoc that has been created on behalf of previous outgoing web call
                            CCWSGenericResponse.ReferenceItem referenceIDocItem = CCWSGenericResponse.getReferenceItem(GenericServiceResponseMessage.References.References.Reference, 
                                                              'IDoc number');
                            if(referenceIDocItem != null)
                            {
                                interfaceLogResponse.IDoc__c = referenceIDocItem.ReferenceID;
                            }
                            // get a maximal severity code
                            String maxSeverityCode = null; 
                            if(GenericServiceResponseMessage.Log != null)
                            {
                                maxSeverityCode = GenericServiceResponseMessage.Log.MaximumLogItemSeverityCode;
                                if(maxSeverityCode != null)
                                {
                                    maxSeverityCode = maxSeverityCode.trim();
                                }
                                debug('GenericResponseHookExample maxSeverityCod: ' + maxSeverityCode);
                                // get a log item array
                                if(GenericServiceResponseMessage.Log.Item != null)
                                {
                                    logItemArr = GenericServiceResponseMessage.Log.Item;
                                }
                            }
                            else
                            {
                                //throw new SCfwException('There is no Log item!');
                            }
                            debug('GenericResponseHookExample ReferenceItem arr: ' + GenericServiceResponseMessage.References.References.Reference);
                            Integer cnt = GenericServiceResponseMessage.References.References.Reference.size();
                            debug('GenericResponseHookExample cnt: ' + cnt);
                            /////////////////////////////////////
                            // processing the list of references
                            // please extend here
                            /////////////////////////////////////
                            if(operation == extraListOperationImplementedByYourSL)
                            {
                                debug('GenericResponseHookExample YourSL list operation');
                                Boolean insertExternalID = false;
                                for(CCWSGenericResponse.ReferenceItem rf: GenericServiceResponseMessage.References.References.Reference)
                                {
                                    debug('ReferenceItem: ' + rf);
                                    // correct the externalID 
                                    if(rf.ExternalID == null || rf.ExternalID == '')
                                    {
                                        if(insertExternalID == false)
                                        {
                                            rf.ExternalID = headExternalID;
                                        }
                                        else
                                        {
                                            throw new SCfwException('More than one reference items has its externalID not set');
                                        }   
                                    }
                                
                                }
                                ///////////////////////////////////////////////////////////
                                // In many cases such as MaterialMovementCreate SAP delivers
                                // a subset of the refrenenceItems in the element References that could be worked out by SAP
                                // For the items that could not be worked out SAP delivers only
                                // information within Log with the severityCode = 3
                                // Our software ought work out also these items.
                                // So we extend the list of references with the references created
                                // on the base of the log entries to ensure complete processing
                                // of all items that were worked out by SAP
                                ///////////////////////////////////////////////////////////////
                                List<CCWSGenericResponse.ReferenceItem> extendedList = new List<CCWSGenericResponse.ReferenceItem>();
                                extendedList.addAll(GenericServiceResponseMessage.References.References.Reference);
                                CCWSGenericResponse.extendReferenceList(extendedList, logItemArr); 
                                processList(messageId, requestMessageID, operation, headExternalID, extendedList, //GenericServiceResponseMessage.References.References.Reference, 
                                            maxSeverityCode, logItemArr, interfaceLogList, GenericServiceResponseMessage,
                                            interfaceLogResponse);  
                            }
                            ///////////////////////////////////
                            // processing of the single references
                            // please extend here by adding the similar if branch for the next operation
                            //////////////////////////////////
                            else if(operation == extraSingleOperationImplementedByYourSL)
                            {
                            	debug('operation: ' + operation);
                            	String idType = extraIdTypeForSingleOperation;
                                CCWSGenericResponse.ReferenceItem referenceItem = CCWSGenericResponse.getReferenceItem(GenericServiceResponseMessage.References.References.Reference, 
                                                                idType, headExternalID);
                                debug('referenceItem: ' + referenceItem);
                                if(referenceItem != null)
                                {
                                	// the reference item has externalId = headExternalID
                                	debug('referenceItem != null');
                                    CCWCYourSLResponseExample1.process(messageID, requestMessageID, headExternalID, 
                                                           referenceItem, maxSeverityCode, 
                                                           logItemArr, interfaceLogList, GenericServiceResponseMessage,
                                                           interfaceLogResponse);
                                }
                                else
                                {
                                	debug('referenceItem = null');
                                    // no required reference has been found
                                    referenceItem = CCWSGenericResponse.getReferenceItem(GenericServiceResponseMessage.References.References.Reference, 
                                                                idType);
                                    debug('referenceItem: ' + referenceItem);
                                    if(referenceItem != null)
                                    {
                                    	// the reference item has no external id set
                                        referenceItem.externalID = headExternalID;
                                        CCWCYourSLResponseExample1.process(messageID, requestMessageID, headExternalID, 
                                                               referenceItem, maxSeverityCode, 
                                                               logItemArr, interfaceLogList, GenericServiceResponseMessage,
                                                               interfaceLogResponse);
                                    }                            
                                    else if(maxSeverityCode == '3')
                                    {
                                    	// there is no reference item in the input structure
                                    	// and the severity cod is equal 3
										debug('maxSeverityCode == 3');
                                        // work out if the severity code indicates an error to log the error
                                        referenceItem = new CCWSGenericResponse.ReferenceItem();
                                        referenceItem.IDType = idType; 
                                        referenceItem.externalID = headExternalID;
                                        referenceItem.referenceID = null;
                                        CCWCYourSLResponseExample1.process(messageID, requestMessageID, headExternalID, 
                                                               referenceItem, maxSeverityCode, 
                                                               logItemArr, interfaceLogList, GenericServiceResponseMessage,
                                                               interfaceLogResponse);
                                    }                                                                  
                                    else
                                    {
                                        interfaceHandler = 'CCWCYourSLResponseExample1';
                                        throw new SCfwException('There is no reference element with IDType: ' + idType + ' and MaxSeverityCode is ' + maxSeverityCode);
                                    }                       
                                }                                                               
                            }
                        }
                        else
                        {
                        	// already checked in CCWSGenericResponse
                            //throw new SCfwException('GenericServiceResponseMessage.References.References.Reference is null');
                        }    
                    }
                    else
                    {
                    	// already checked in CCWSGenericResponse
                        //throw new SCfwException('GenericServiceResponseMessage.References.References is null');
                    }
                }
                else
                {
	            	// already checked in CCWSGenericResponse
                    // throw new SCfwException('Operation or headExternalID is null or empty.');
                }    
            }
            else
            {
            	// already checked in CCWSGenericResponse
                // throw new SCfwException('GenericServiceResponseMessage.Reference is null');
            }           
        }
        interfaceLogResponse.ResultCode__c = CCWSGenericResponse.getResultCode(logItemArr);
        if(interfaceLogResponse.ResultInfo__c != null && interfaceLogResponse.ResultInfo__c != '')
        {
            interfaceLogResponse.ResultInfo__c += '\n';
        }
        interfaceLogResponse.ResultInfo__c = CCWSGenericResponse.getResultInfoForAll(logItemArr);
        interfaceLogResponse.Data__c = 'Data delivered by SAP: ' + fromJSONMap;
        interfaceLogResponse.Data__c = interfaceLogResponse.Data__c.left(32000);          
	}// process

    /**
     * processes list of references
     * please extend here 
     * please see how following interfaces have been implemented:
     * CCWCMaterialReservationCreateResponse,
     * CCWCMaterialMovementCreateResponse
     * CCWCMaterialPackageReceivedResponse
     * 
     */
    public static void processList(String messageID, String requestMessageID, String operation, String headExternalID, 
                               List<CCWSGenericResponse.ReferenceItem> referenceList, String MaximumLogItemSeverityCode, 
                               CCWSGenericResponse.LogItemClass[] allLogItemArr, List<SCInterfaceLog__c> interfaceLogList, 
                               CCWSGenericResponse.GenericServiceResponseMessageClass GenericServiceResponseMessage,
                               SCInterfaceLog__c interfaceLogResponse)
    {
        if(operation == extraListOperationImplementedByYourSL)
        {
        	debug('operation == extraListOperationImplementedByYourSL');
			// It is only one example for YourSL. On this place you can use your classes.
			// Please comment out the call to CCWCMaterialReservationCreateResponse.processMaterialMovementReservationResponse !!!!
			// ===============================================================================================================
            CCWCMaterialReservationCreateResponse.processMaterialMovementReservationResponse(messageID, requestMessageID, headExternalID, referenceList, MaximumLogItemSeverityCode, 
                                                               allLogItemArr, interfaceLogList, GenericServiceResponseMessage,
                                                               interfaceLogResponse);
        }
    	
    	/*
        if(operation == 'CreateMaterialReservation')
        {
            CCWCMaterialReservationCreateResponse.processMaterialMovementReservationResponse(messageID, requestMessageID, headExternalID, referenceList, MaximumLogItemSeverityCode, 
                                                               allLogItemArr, interfaceLogList, GenericServiceResponseMessage,
                                                               interfaceLogResponse);
        }
        else if(operation == 'MaterialMovement')
        {
            CCWCMaterialMovementCreateResponse.processMaterialMovementCreateResponse(messageID, requestMessageID, headExternalID, referenceList, MaximumLogItemSeverityCode, 
                                                               allLogItemArr, interfaceLogList, GenericServiceResponseMessage, 
                                                               interfaceLogResponse);
        }
        else if(operation == 'PackageReceived')
        {
            CCWCMaterialPackageReceivedResponse.processMaterialPackageReceivedResponse(messageID, requestMessageID, headExternalID, referenceList, MaximumLogItemSeverityCode, 
                                                               allLogItemArr, interfaceLogList, GenericServiceResponseMessage, 
                                                               interfaceLogResponse);
        }
        else if(operation == 'EquipmentMovement')
        {
            throw new SCfwException('operation: ' + operation + ' not supported!');
        }
        else
        {
            throw new SCfwException('operation: ' + operation + ' not supported!');
        }
        */
    }// process

    public static void debug(String msg)
    {
        System.debug('###...' +  msg);        
    }

}
