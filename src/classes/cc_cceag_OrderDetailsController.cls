global without sharing class cc_cceag_OrderDetailsController {
    global cc_cceag_OrderDetailsController() {
        
    }

    @RemoteAction
    global static String getOrderNumber(String orderId){
        String orderNumber = null;
        try{
            ccrz__E_Order__c order = [SELECT Name, ccrz__OrderId__c, ccrz__PONumber__c, ccrz__Contact__c FROM ccrz__E_Order__c WHERE Id = :orderId LIMIT 1];
            orderNumber = (order.ccrz__PONumber__c != null) ? String.valueOf(order.ccrz__PONumber__c) : order.Name;
        }catch(Exception e){
            System.debug(LoggingLevel.INFO, e.getMessage());
            System.debug(LoggingLevel.INFO, e.getStackTraceString());
        }
        return orderNumber;
    }

    @RemoteAction
    global static void sendOrderEmail(String orderId, String emailAddress){
        try{
            Contact c = new Contact(LastName='temp',Email=emailAddress);
            insert c;
            EmailTemplate et = [SELECT id FROM EmailTemplate WHERE developerName = 'CC_CCEAG_Order' limit 1];
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTemplateId(et.id);
            mail.setWhatId(orderId);
            mail.setTargetObjectId(c.Id);
            mail.setSenderDisplayName('Webstore');
            mail.setSaveAsActivity(false);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            delete c;
        }catch(Exception e){
            System.debug(LoggingLevel.INFO, e.getMessage());
            System.debug(LoggingLevel.INFO, e.getStackTraceString());
        }
    }

}
