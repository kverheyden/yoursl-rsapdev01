@RestResource(urlMapping='/SalesOrder/*') 
global with sharing class CCWCRestSalesOrder{
	
	private static final String SALES_ORDER_OBJECT_TYPE = 'SalesOrder__c';
	private static final String SALES_ORDER_INDIRECT = 'IndirectOrder';
	private static final String SALES_ORDER_DIRECT = 'DirectOrder';
	
	public static SalesOrder__c NewSalesOrder { get; private set; }
	
	private static RecordType DirectOrderRecordType = [SELECT Id FROM RecordType WHERE DeveloperName =: SALES_ORDER_DIRECT AND SObjectType =: SALES_ORDER_OBJECT_TYPE LIMIT 1];
	
	private without sharing class InsertSalesOrder {
		public void insertWithoutSharing(SalesOrder__c so) {
			insert so;
		}
	}
	
	private without sharing class QuerySupplierWithoutSharing {
		public Account getSupplierById(Id supplierId) {
			Account supplier = [SELECT GFGHOrderDeadlineTime__c FROM Account WHERE Id =: supplierId];
			return supplier;
		}
	}
	
	global class SalesOrderWrapper{
        public String AccountId { get; set; }
		public String CustomerNumberAtSupplier { get; set; }		
		public String EarliestDeliveryDate { get; set; }
		public String OrderDate { get; set; }
        public String OwnerId { get; set; }
        public String Pricebook2Id { get; set; } 
		public String RecordType { get; set; }
		public String RequestedDeliveryDate { get; set; }
		public String RequestId { get; set; }
		public List<SalesOrderItemWrapper> SalesOrderItems { get; set; }
        public String Status { get; set; }
        public String SupplierId { get; set; }
		public String Type { get; set; }
		public String Description { get; set; }
		public String AppIdentifierKey { get; set; }
    }
	
	global class SalesOrderItemWrapper {
		public String Description { get; set; }
		public String PricebookEntryDetailId { get; set; }
		public String Product2Id { get; set; }
		public String Quantity { get; set; }
		public String SortOrder { get; set; }		
	}
    
    global class ResponseWrapper{
        public String StatusCode { get; set; }
        public String StatusMessage { get; set; }
		public String Stacktrace { get; set; }
		public String AppIdentifierKey { get; set; }
        public SalesOrder__c Order { get; set; }
    }
	
	private static RecordType getRecordType(String sObjectName, String recordTypeName) {
		return [SELECT Id, SobjectType, DeveloperName ,Name FROM RecordType WHERE Name =: recordTypeName AND SobjectType =: sObjectName  LIMIT 1];
	}
	
	private static void setCustomerNumberAtSupplier(String masterAccountId, String slaveAccountId, String customerNumberAtSupplier) {
		if (masterAccountId == null || masterAccountId == '' ||
			slaveAccountId == null || slaveAccountId == '' ||
			customerNumberAtSupplier == null || customerNumberAtSupplier == '')
		return;
		
		CCAccountRole__c accRole = [SELECT CustomerNumberAtSlaveAccount__c FROM CCAccountRole__c 
									WHERE MasterAccount__c =: masterAccountId AND SlaveAccount__c =: slaveAccountId  LIMIT 1];
		if (accRole != null && accRole.CustomerNumberAtSlaveAccount__c != customerNumberAtSupplier) {
			accRole.CustomerNumberAtSlaveAccount__c = customerNumberAtSupplier;
			update accRole;
		}
	}
	
	private static String getCustomerNumberAtSupplier(String masterAccountId, String slaveAccountId) {
		if (masterAccountId == null || masterAccountId == '' || 
			slaveAccountId == null || slaveAccountId == '')
			return '';
		
		List<CCAccountRole__c> accountRoles = [SELECT CustomerNumberAtSlaveAccount__c FROM CCAccountRole__c
											   WHERE MasterAccount__c =: masterAccountId AND 
										       SlaveAccount__c =: slaveAccountId AND 
											   AccountRole__c = 'ZV'];
		
		if (accountRoles.size() > 0)
			return accountRoles[0].CustomerNumberAtSlaveAccount__c;
		
		return '';
		
	}
	
	private static Map<Id, PricebookEntryDetail__c> getPricebookEntryDetailMap(List<SalesOrderItemWrapper> salesOrderItems) {
		Set<Id> pricebookEntryDetailIds = new Set<Id>();
		for(SalesOrderItemWrapper i : salesOrderItems)
			if (i.PricebookEntryDetailId != null && i.PricebookEntryDetailId != '')
				pricebookEntryDetailIds.add(i.PricebookEntryDetailId);
		
		return new Map<Id, PricebookEntryDetail__c>([SELECT Product2__c FROM PricebookEntryDetail__c WHERE Id IN : pricebookEntryDetailIds]);		
	}
	
	private static void setSendToSupplierDate(SalesOrder__c salesOrder) {
		QuerySupplierWithoutSharing withoutSharingQuery = new QuerySupplierWithoutSharing();
		Account supplier = withoutSharingQuery.getSupplierById(salesOrder.Supplier__c);
		Datetime gfghOrderDeadline = supplier.GFGHOrderDeadlineTime__c;
		if (gfghOrderDeadline == null)
			return;
		if (gfghOrderDeadline <= System.now())
			salesOrder.SendToSupplierDate__c = gfghOrderDeadline.addDays(1).addHours(2);
		else
			salesOrder.SendToSupplierDate__c = gfghOrderDeadline.addHours(2);
	}
	
	@HttpPost
    global static void doPost(SalesOrderWrapper SalesOrder){
        Datetime requestStart = System.now();
		ResponseWrapper resp = new ResponseWrapper();
        SalesOrder__c existingOrder = null;
        NewSalesOrder = new SalesOrder__c();
		try{
			List<SalesOrder__c> existingOrders = [SELECT ExternalId__c FROM SalesOrder__c WHERE ExternalId__c =: SalesOrder.AppIdentifierKey]; 
			if (existingOrders.size() > 0) 
				existingOrder = existingOrders[0];
			
			if (existingOrder == null) {
				if (SalesOrder.AppIdentifierKey != null)
					NewSalesOrder.ExternalId__c = SalesOrder.AppIdentifierKey;
				if (SalesOrder.AccountId != null)
					NewSalesOrder.Account__c = Id.valueOf(SalesOrder.AccountId);
				if (SalesOrder.SupplierId != null)
					NewSalesOrder.Supplier__c = Id.valueOf(SalesOrder.SupplierId);
				NewSalesOrder.Type__c = SalesOrder.Type;
				NewSalesOrder.Status__c = SalesOrder.Status;
				if (SalesOrder.EarliestDeliveryDate != null)
					NewSalesOrder.EarliestDeliveryDate__c = Date.valueOf(SalesOrder.EarliestDeliveryDate);
				if (SalesOrder.RequestedDeliveryDate != null)
					NewSalesOrder.RequestedDeliveryDate__c = Date.valueOf(SalesOrder.RequestedDeliveryDate);
				if (SalesOrder.OrderDate != null)
					NewSalesOrder.OrderDate__c = DateTime.valueOf(SalesOrder.OrderDate);			
				if (SalesOrder.OwnerId != null) 
					NewSalesOrder.OwnerId = SalesOrder.OwnerId;
				if (SalesOrder.Pricebook2Id != null)
					NewSalesOrder.Pricebook2__c = SalesOrder.Pricebook2Id;
				NewSalesOrder.Request__c = SalesOrder.RequestId;
				if (SalesOrder.CustomerNumberAtSupplier != null) {
					NewSalesOrder.CustomerNumberAtSupplier__c = SalesOrder.CustomerNumberAtSupplier;	
					//setCustomerNumberAtSupplier(SalesOrder.SupplierId, SalesOrder.CustomerNumberAtSupplier);
					setCustomerNumberAtSupplier(SalesOrder.AccountId, SalesOrder.SupplierId, SalesOrder.CustomerNumberAtSupplier);
				}
				RecordType recType = getRecordType(SALES_ORDER_OBJECT_TYPE, SalesOrder.RecordType);
				if (recType != null) {
					NewSalesOrder.RecordTypeId = recType.Id;
					
					if (recType.DeveloperName == SALES_ORDER_INDIRECT && 
						NewSalesOrder.CustomerNumberAtSupplier__c != null && 
						NewSalesOrder.CustomerNumberAtSupplier__c != '') {
						NewSalesOrder.CustomerNumberAtSupplier__c = getCustomerNumberAtSupplier(SalesOrder.AccountId, SalesOrder.SupplierId);
					}
					
					if (recType != null && recType.DeveloperName == SALES_ORDER_INDIRECT) {
						setSendToSupplierDate(NewSalesOrder);
					}					
				}
				NewSalesOrder.Description__c = SalesOrder.Description;
				
				InsertSalesOrder insertSO = new InsertSalesOrder();
				insertSO.insertWithoutSharing(NewSalesOrder);
				
				if (SalesOrder.SalesOrderItems != null) {
					List<SalesOrderItem__c> newOrderItems = new List<SalesOrderItem__c>();
					Map<Id, PricebookEntryDetail__c> pricebookEntryDetails = getPricebookEntryDetailMap(SalesOrder.SalesOrderItems);
					
					for(SalesOrderItemWrapper item : SalesOrder.SalesOrderItems) {
						SalesOrderItem__c newItem = new SalesOrderItem__c();
						if (item.PricebookEntryDetailId != null && item.PricebookEntryDetailId != '') {
							newItem.PricebookEntryDetail__c = item.PricebookEntryDetailId;
							newItem.Product2__c = pricebookEntryDetails.get(item.PricebookEntryDetailId).Product2__c;
						}
						if (item.Product2Id != null && newItem.Product2__c == null && item.Product2Id != '')
							newItem.Product2__c = item.Product2Id;
						if (item.Quantity != null)
							newItem.Quantity__c = Decimal.valueOf(item.Quantity);
						if (item.SortOrder != null)
							newItem.SortOrder__c = Double.valueOf(item.SortOrder);
						newItem.Description__c = item.Description;
						newItem.SalesOrder__c = NewSalesOrder.Id;
						newOrderItems.add(newItem);
					}
						
					insert newOrderItems;
					System.debug('Set is read to send to SAP RecordTypeDeveloperName: ' + NewSalesOrder.RecordType.DeveloperName);
					System.debug('Set is read to send to SAP RecordTypeName: ' + NewSalesOrder.RecordType.Name);
					System.debug('Set is read to send to SAP RecordTypeId: ' + NewSalesOrder.RecordTypeId);
					System.debug('Set is read to send to SAP RecordTypeId: ' + DirectOrderRecordType.Id);
					if (NewSalesOrder.RecordTypeId == DirectOrderRecordType.Id && NewSalesOrder.Account__c != null) {
						NewSalesOrder.IsReadyToSendToSAP__c = true;
						update NewSalesOrder;
					}
					
				}
			}
        } catch(Exception e){
            resp.StatusCode = 'Error';
            resp.StatusMessage = e.getTypeName() + ': ' + e.getMessage();
			resp.Stacktrace = e.getStackTraceString();
            RestContext.response.addHeader('Content-Type', 'application/json');     
			RestContext.response.responseBody = Blob.valueOf(JSON.serialize(resp));
			//RestServiceUtilities.insertNewLog(requestStart, false);
			System.debug('An unexpected error occurred: ' + e.getMessage());
			return;
        }
		if (existingOrder == null) {
	        resp.StatusCode = 'Done';
	        resp.StatusMessage = 'Success';
			resp.AppIdentifierKey = SalesOrder.AppIdentifierKey;
			resp.Order = NewSalesOrder;
        }
		else {
			resp.StatusCode = 'Done';
	        resp.StatusMessage = 'Order allready exists';
			resp.AppIdentifierKey = SalesOrder.AppIdentifierKey;
			resp.Order = existingOrder;
		}
		RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(resp));
		RestServiceUtilities.insertNewLog(requestStart, SalesOrder, true);
    }
}
