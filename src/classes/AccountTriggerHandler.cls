public with sharing class AccountTriggerHandler{
	
	public class AIHandler extends TriggerHandlerBase {
		
		public override void mainEntry(TriggerParameters tp) {
			AccountRequestDataTransfer accHeandler = new AccountRequestDataTransfer();
        	accHeandler.enrichData((List<Account>)tp.newList);
		}
	}
	
	public class AUHandler extends TriggerHandlerBase {

		public override void mainEntry(TriggerParameters tp) {
			System.debug('AccountAfterUpdateTriggerHandler: ' + tp.newMap.keySet());
			AccountRequestDataTransfer accHeandler = new AccountRequestDataTransfer();
			List<Account>processAcc = new List<Account>();
			
			for(Id tmpId : tp.newMap.keySet()){
				System.debug(((Account)tp.newMap.get(tmpId)).RequestID__c + ' != ' + ((Account)tp.oldMap.get(tmpId)).RequestID__c);
				if( ((Account)tp.newMap.get(tmpId)).RequestID__c != ((Account)tp.oldMap.get(tmpId)).RequestID__c){
					processAcc.add((Account)(tp.newMap.get(tmpId)));
				}
			}
			if(!processAcc.isEmpty())
				accHeandler.enrichData(processAcc);
		}
	}
	
	public class BIHandler extends TriggerHandlerBase {
		
		public override void mainEntry(TriggerParameters tp) {
			for (Account acc : (List<Account>)tp.newList ) {
			
				if ((acc.BillingCountryState__c != null) && (acc.BillingState != acc.BillingCountryState__c)) 
					acc.BillingState = acc.BillingCountryState__c;
				
				if ((acc.BillingCountry__c != null) && (acc.BillingCountry != acc.BillingCountry__c)) 
				acc.BillingCountry = acc.BillingCountry__c;
				
				if ((acc.ShippingCountryState__c != null) && (acc.ShippingState != acc.ShippingCountryState__c)) 
					acc.ShippingState = acc.ShippingCountryState__c;
				
				if ((acc.ShippingCountry__c != null) && (acc.ShippingCountry != acc.ShippingCountry__c)) 
					acc.ShippingCountry = acc.ShippingCountry__c;
				
				if (acc.LastName__c != null) {
					acc.isPersonAccount__c = true;
					if (acc.FirstName__c != null) 
						acc.Name = acc.FirstName__c + ' ' + acc.LastName__c;				
					else 
						acc.Name = acc.LastName__c;
				}
			
			}
		}
	}
	
	public class BUHandler extends TriggerHandlerBase {
		
		public override void mainEntry(TriggerParameters tp) {
			
			for (Account acc : (List<Account>)tp.newList ) {
				Account accOld;
				if (acc.BillingState != acc.BillingCountryState__c) {
					if (acc.BillingState == null) {
						acc.BillingState = acc.BillingCountryState__c;
					}
					else {
						accOld = accOld == null ? null : (Account)tp.oldMap.get(acc.Id);
						
						if ((accOld == null)||(accOld.BillingState != acc.BillingState)) 
							acc.BillingCountryState__c = acc.BillingState;
						acc.BillingState = acc.BillingCountryState__c;
					}
				}
				if (acc.BillingCountry != acc.BillingCountry__c) {
					if (acc.BillingCountry == null) {
						acc.BillingCountry = acc.BillingCountry__c;
					}
					else {
						accOld = accOld == null ? null : (Account)tp.oldMap.get(acc.Id);

						if ((accOld == null)||(accOld.BillingCountry != acc.BillingCountry)) 
							acc.BillingCountry__c = acc.BillingCountry;
						acc.BillingCountry = acc.BillingCountry__c;
					}
				}
				if (acc.ShippingState != acc.ShippingCountryState__c) {
					if (acc.ShippingState == null) {
						acc.ShippingState = acc.ShippingCountryState__c;
					}
					else {
						accOld = accOld == null ? null : (Account)tp.oldMap.get(acc.Id);

						if ((accOld == null)||(accOld.ShippingState != acc.ShippingState)) 
							acc.ShippingCountryState__c = acc.ShippingState;
						acc.ShippingState = acc.ShippingCountryState__c;
					}
				}
				if (acc.ShippingCountry != acc.ShippingCountry__c) {
					if (acc.ShippingCountry == null) {
						acc.ShippingCountry = acc.ShippingCountry__c;
					}
					else {
						accOld = accOld == null ? null : (Account)tp.oldMap.get(acc.Id);

						if ((accOld == null)||(accOld.ShippingCountry != acc.ShippingCountry))
							acc.ShippingCountry__c = acc.ShippingCountry;
						acc.ShippingCountry = acc.ShippingCountry__c;
					}
				}
				if (acc.isPersonAccount__c)
					acc.Name = String.isEmpty(acc.FirstName__c) ? acc.LastName__c : acc.FirstName__c + ' '+ acc.LastName__c;
			}
		}
	}
}
