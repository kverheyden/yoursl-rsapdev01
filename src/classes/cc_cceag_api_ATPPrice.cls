global without sharing class cc_cceag_api_ATPPrice extends ccrz.cc_api_PriceAdjustment {
    public cc_cceag_api_ATPPrice() {
        
    }

    global override Map<String, Object> computePricingCart(Map<String, Object> inputData) {
        System.debug(LoggingLevel.INFO, 'POINT BEGIN:' + System.now());
        ccrz.cc_bean_CartSummary cartData = (ccrz.cc_bean_CartSummary) inputData.get(PARAM_CARTBEAN);
        try {
            String context = cartData.extrinsic.remove('context');
            if (context != null && context.equals('executePrice')) {
                processATP(inputData, true);
                if (processMinimumReq(inputData))
                    cartData.extrinsic.put('processedATP', 'true');
                processPricing(inputData);
                String displayTotals = 'true';
                for (ccrz.cc_bean_CartItem cartItemBean : cartData.cartItems) {
                    if (cartItemBean.miscMessage == 'hide')
                        displayTotals = 'false';
                }
                cartData.extrinsic.put('displayTotals', displayTotals);
            }
            else {
                processATP(inputData, false);
                if (processMinimumReq(inputData))
                    cartData.extrinsic.put('processedATP', 'true');
            }
        }
        catch (Exception e) {
            System.debug(System.LoggingLevel.INFO, e.getMessage());
            System.debug(System.LoggingLevel.INFO, e.getStackTraceString());
        }
        System.debug(LoggingLevel.INFO, 'POINT END:' + System.now());
        return inputData;
    }

    private void processATP(Map<String, Object> inputData, boolean displayPricing) {
        System.debug(LoggingLevel.INFO, 'POINT ATP BEGIN:' + System.now());
        ccrz.cc_bean_CartSummary cartData = (ccrz.cc_bean_CartSummary)inputData.get(PARAM_CARTBEAN);
        System.debug(LoggingLevel.INFO, cartData.extrinsic);
        ccrz__E_Cart__c cart = [
            select
                 Id
                ,ccrz__ShipTo__r.ccrz__Partner_Id__c
                ,ccrz__RequestDate__c
            from
                ccrz__E_Cart__c
            where
                Id = :cartData.sfid
        ];
        String shiptoId = cart.ccrz__ShipTo__r.ccrz__Partner_Id__c;
        Account shipTo = [SELECT DeliveringPlant__c, AccountNumber FROM Account WHERE AccountNumber =: shiptoId LIMIT 1];
        Map<Id,ccrz__E_CartItem__c> cartItemMap = new Map<Id,ccrz__E_CartItem__c>([
            select
                 ccrz__Product__c
                ,ccrz__Product__r.ccrz__SKU__c
            from
                ccrz__E_CartItem__c
            where
                ccrz__Cart__c = :cartData.sfid
        ]);


        Boolean noItemFlagged = false;
        Boolean adjustedItemFlagged = false;
        CCWSSalesOrderRequestResponse_Out.IM_MATPLANT_element IM_MATPLANT = new CCWSSalesOrderRequestResponse_Out.IM_MATPLANT_element();
        IM_MATPLANT.item = new CCWSSalesOrderRequestResponse_Out.x_xCCC_xOTCS_MATAVAIL_LONG[]{};
        System.debug(LoggingLevel.INFO, 'cartItems = ' + cartData.cartItems);
        for(ccrz.cc_bean_CartItem cartItem: cartData.cartItems) {
            if (cartItem.itemStatus != 'Invalid') {
                CCWSSalesOrderRequestResponse_Out.x_xCCC_xOTCS_MATAVAIL_LONG item = new CCWSSalesOrderRequestResponse_Out.x_xCCC_xOTCS_MATAVAIL_LONG();
                item.MATERIAL = formatSku(cartItemMap.get(cartItem.itemID).ccrz__Product__r.ccrz__SKU__c);
                item.PLANT = shipTo.DeliveringPlant__c;
                item.REQ_DATE = formatDate(cart.ccrz__RequestDate__c);
                item.REQ_QTY = String.valueOf(cartItem.quantity) + '000';
                item.REQ_UNIT = null;
                IM_MATPLANT.item.add(item);
            }
            else {
                cartItem.productInventoryMsg = 'NO_INVENTORY';
                noItemFlagged = true;
                cartItem.miscMessage = 'hide';
            }
        }

        if (cartData.messages == null)
            cartData.messages = new List<ccrz.cc_bean_Message>();

        if (IM_MATPLANT.item.size() > 0) {
            System.debug(LoggingLevel.INFO, 'POINT ATP CALLOUT BEGIN:' + System.now());
            CCWSSalesOrderRequestResponse_Out.CCWSSalesOrderRequestResponse_OutPort stub = new CCWSSalesOrderRequestResponse_Out.CCWSSalesOrderRequestResponse_OutPort();
            CCWSUtil utils = new CCWSUtil();
            stub.endpoint_x = utils.getEndpoint('SalesOrderRequestResponse');
            stub.inputHttpHeaders_x = utils.getBasicAuth();
            stub.timeout_x = utils.getTimeOut();
            CCWSSalesOrderRequestResponse_Out.CC_SalesOrderATPCheck_Response_element response = stub.ATPCheck(
                 null           //String IM_CHECK_RULE
                ,IM_MATPLANT    //CCWSSalesOrderRequestResponse_Out.IM_MATPLANT_element IM_MATPLANT
            );
            System.debug(LoggingLevel.INFO, 'POINT ATP CALLOUT END:' + System.now());
            system.debug(logginglevel.info, 'request(JSON)=>' + JSON.serialize(IM_MATPLANT));
            system.debug(logginglevel.info, 'response(JSON)=>' + JSON.serialize(response));
            system.debug(logginglevel.info, 'request=>' + IM_MATPLANT);
            system.debug(logginglevel.info, 'response=>' + response);
            if(null == response.EX_COM_QTY.item) throw new CCWSSalesOrderException('TRANSLATE: No ATP Check Data Received.');

            for (ccrz.cc_bean_CartItem cartItemBean : cartData.cartItems) {
                cartItemBean.price = 0;
                cartItemBean.SubAmount = 0;
                cartItemBean.productInventory = 0;
                if (cartItemBean.itemStatus != 'Invalid')
                    cartItemBean.productInventoryMsg = null;
                ccrz__E_CartItem__c sObj = cartItemMap.get(cartItemBean.itemID);
                cartItemBean.miscMessage = 'hide';
                for(CCWSSalesOrderRequestResponse_Out.x_xCCC_xOTCS_QTY_CDD item : response.EX_COM_QTY.item) {
                    if(formatSku(sObj.ccrz__Product__r.ccrz__SKU__c) == item.MATERIAL) {
                        Decimal quantity = Decimal.valueOf(item.COM_QTY);
                        cartItemBean.productInventory = quantity;
                        if (0 >= quantity) {
                            cartItemBean.productInventoryMsg = 'NO_INVENTORY';
                            noItemFlagged = true;
                        } else if (quantity < cartItemBean.quantity) {
                            cartItemBean.productInventoryMsg = 'ADJUSTED_INVENTORY';
                            cartItemBean.productInventory = quantity;
                            Integer origQty = cartItemBean.quantity;
                            cartItemBean.quantity = quantity.intValue();
                            cartItemBean.comments = String.valueOf(origQty);
                            adjustedItemFlagged = true;
                        }
                        else
                          cartItemBean.productInventoryMsg = null; 
                    }
                }
            }
        }

        if (noItemFlagged) {
            cartData.messages.add(cc_cceag_util_Utils.buildErrorMessage('0', 'MSG_CART_NO_ITEMS', 'messagingSection-Global-Error'));
        }
        if (adjustedItemFlagged) {
            cartData.messages.add(cc_cceag_util_Utils.buildErrorMessage('adjusted', 'MSG_CART_LIMITED_ITEMS', 'messagingSection-Adjusted-Error'));
        }
        System.debug(LoggingLevel.INFO, 'POINT ATP END:' + System.now());
    }

    private boolean processMinimumReq(Map<String, Object> inputData) {
        ccrz.cc_bean_CartSummary cartData = (ccrz.cc_bean_CartSummary)inputData.get(PARAM_CARTBEAN);
        String zfkMapStr = (String) cartData.extrinsic.get('zfkMap');
        if (zfkMapStr != null) {
            Map<String, Object> zfkData = (Map<String, Object>) JSON.deserializeUntyped(zfkMapStr);
            Decimal minQty = Decimal.valueOf(cartData.extrinsic.get('zfkMiamfoac'));
            Decimal currZfk = 0.0;
            for (ccrz.cc_bean_CartItem cartItemBean : cartData.cartItems) {
                if (cartItemBean.itemStatus != 'Invalid') {
                	if(cartItemBean.mockProduct != null) {
                    	String sfid = cartItemBean.mockProduct.id;
	                    if (zfkData.get(sfid) != null) {
	                        Decimal zfkVal = (Decimal) zfkData.get(sfid);
	                        currZfk += zfkVal * cartItemBean.quantity;
	                    }
                	}
                }
            }
            System.debug(LoggingLevel.INFO, 'currZfk= ' + currZfk);
            System.debug(LoggingLevel.INFO, 'minQty= ' + minQty);
            if (currZfk < minQty) {
                if (cartData.messages == null)
                    cartData.messages = new List<ccrz.cc_bean_Message>();
                ccrz.cc_bean_Message msg = new ccrz.cc_bean_Message();
                msg.labelId = 'MSG_MIN_QTY_NOT_MET';
                msg.message = '0';
                msg.severity = ccrz.cc_bean_Message.MessageSeverity.ERROR;
                msg.type = ccrz.cc_bean_Message.MessageType.CUSTOM;
                msg.classToAppend = 'messagingSection-Global-Error';
                cartData.messages.add(msg);
                return false;
            }
            else
                return true;
        }
        else
            return true;
    }

    private void processPricing(Map<String, Object> inputData) {
        System.debug(LoggingLevel.INFO, 'POINT PRICE BEGIN:' + System.now());
        ccrz.cc_bean_CartSummary cartData = (ccrz.cc_bean_CartSummary)inputData.get(PARAM_CARTBEAN);
        ccrz__E_Cart__c cart = [
            select
                 Id
                ,ccrz__ShipTo__r.ccrz__Partner_Id__c
                ,ccrz__RequestDate__c
            from
                ccrz__E_Cart__c
            where
                Id = :cartData.sfid
        ];
        Map<Id,ccrz__E_CartItem__c> cartItemMap = new Map<Id,ccrz__E_CartItem__c>([
            select
                ccrz__RequestDate__c
                ,ccrz__Product__c
                ,ccrz__Product__r.ccrz__SKU__c
            from
                ccrz__E_CartItem__c
            where
                ccrz__Cart__c = :cartData.sfid
        ]);

        // Need to get the specific product ids
        Map<String,String> productMap = new Map<String,String>();
        for (ccrz__E_CartItem__c item : cartItemMap.values())
            productMap.put(item.ccrz__Product__c, item.ccrz__Product__r.ccrz__SKU__c);

        CCWSUtil utils = new CCWSUtil();
        CCWSSalesOrderRequestResponse_Out.CCWSSalesOrderRequestResponse_OutPort stub = new CCWSSalesOrderRequestResponse_Out.CCWSSalesOrderRequestResponse_OutPort();
        stub.endpoint_x = utils.getEndpoint('SalesOrderRequestResponse');
        stub.inputHttpHeaders_x = utils.getBasicAuth();
        stub.timeout_x = utils.getTimeOut();

        CCWSSalesOrderRequestResponse_Out.BAPISDHEAD orderHeader = new CCWSSalesOrderRequestResponse_Out.BAPISDHEAD();
        orderHeader.DOC_TYPE = 'ZOR';       // Fixed Value
        orderHeader.SALES_ORG = '0120';     // Fixed Value
        orderHeader.DISTR_CHAN = 'Z1';      // Fixed Value
        orderHeader.DIVISION = 'Z0';        // Fixed Value
        orderHeader.REQ_DATE_H = formatDate(cart.ccrz__RequestDate__c);     // Optional
        orderHeader.PURCH_NO = cart.Id;     // External Order Number of customer

        CCWSSalesOrderRequestResponse_Out.ORDER_ITEMS_IN_element orderItems = new CCWSSalesOrderRequestResponse_Out.ORDER_ITEMS_IN_element();
        orderItems.item = new CCWSSalesOrderRequestResponse_Out.BAPIITEMIN[]{};

        Integer counter = INCREMENT;
        for (ccrz.cc_bean_CartItem cartItemBean : cartData.cartItems) {
            if (cartItemBean.itemStatus != 'Invalid') {
                ccrz__E_CartItem__c sObj = cartItemMap.get(cartItemBean.itemID);
                CCWSSalesOrderRequestResponse_Out.BAPIITEMIN reqitem = new CCWSSalesOrderRequestResponse_Out.BAPIITEMIN();
                reqitem.ITM_NUMBER = leftPadAndIncrement(counter);          // Incremented item number
                reqitem.HG_LV_ITEM = '000000';                              // Fixed Value
                reqitem.MATERIAL = formatSku(productMap.get(sObj.ccrz__Product__c));   // Product number
                reqitem.REQ_QTY = String.valueOf(cartItemBean.quantity) + '000';// Quantity
                orderItems.item.add(reqitem);
                System.debug(System.LoggingLevel.INFO, reqitem);
            }
        }

        CCWSSalesOrderRequestResponse_Out.ORDER_PARTNERS_element orderPartners = new CCWSSalesOrderRequestResponse_Out.ORDER_PARTNERS_element();
        orderPartners.item = new CCWSSalesOrderRequestResponse_Out.BAPIPARTNR[]{};
        CCWSSalesOrderRequestResponse_Out.BAPIPARTNR partnerItem = new CCWSSalesOrderRequestResponse_Out.BAPIPARTNR();
        partnerItem.PARTN_ROLE = 'WE';                                      //  Fixed Value
        partnerITem.PARTN_NUMB = padData(cart.ccrz__ShipTo__r.ccrz__Partner_Id__c, 10, '0');  // Ship to number, selected by customer
        orderPartners.item.add(partnerItem);

        System.debug(System.LoggingLevel.INFO, partnerItem);

        // Send the request
        System.debug(LoggingLevel.INFO, 'POINT PRICE CALLOUT BEGIN:' + System.now());
        CCWSSalesOrderRequestResponse_Out.CC_SalesOrderSimulateOrderResponse_element response = stub.SimulateOrder(
             null           //String CONVERT_PARVW_AUART
            ,orderHeader    //CCWSSalesOrderRequestResponse_Out.BAPISDHEAD ORDER_HEADER_IN
            ,null           //CCWSSalesOrderRequestResponse_Out.EXTENSIONIN_element EXTENSIONIN
            ,null           //CCWSSalesOrderRequestResponse_Out.MESSAGETABLE_element MESSAGETABLE
            ,null           //CCWSSalesOrderRequestResponse_Out.ORDER_CCARD_element ORDER_CCARD
            ,null           //CCWSSalesOrderRequestResponse_Out.ORDER_CCARD_EX_element ORDER_CCARD_EX
            ,null           //CCWSSalesOrderRequestResponse_Out.ORDER_CFGS_BLOB_element ORDER_CFGS_BLOB
            ,null           //CCWSSalesOrderRequestResponse_Out.ORDER_CFGS_INST_element ORDER_CFGS_INST
            ,null           //CCWSSalesOrderRequestResponse_Out.ORDER_CFGS_PART_OF_element ORDER_CFGS_PART_OF
            ,null           //CCWSSalesOrderRequestResponse_Out.ORDER_CFGS_REF_element ORDER_CFGS_REF
            ,null           //CCWSSalesOrderRequestResponse_Out.ORDER_CFGS_VALUE_element ORDER_CFGS_VALUE
            ,null           //CCWSSalesOrderRequestResponse_Out.ORDER_CONDITION_EX_element ORDER_CONDITION_EX
            ,null           //CCWSSalesOrderRequestResponse_Out.ORDER_INCOMPLETE_element ORDER_INCOMPLETE
            ,orderItems     //CCWSSalesOrderRequestResponse_Out.ORDER_ITEMS_IN_element ORDER_ITEMS_IN
            ,null           //CCWSSalesOrderRequestResponse_Out.ORDER_ITEMS_OUT_element ORDER_ITEMS_OUT
            ,orderPartners  //CCWSSalesOrderRequestResponse_Out.ORDER_PARTNERS_element ORDER_PARTNERS
            ,null           //CCWSSalesOrderRequestResponse_Out.ORDER_SCHEDULE_EX_element ORDER_SCHEDULE_EX
            ,null           //CCWSSalesOrderRequestResponse_Out.ORDER_SCHEDULE_IN_element ORDER_SCHEDULE_IN
            ,null           //CCWSSalesOrderRequestResponse_Out.PARTNERADDRESSES_element PARTNERADDRESSES
        );

        System.debug(LoggingLevel.INFO, 'POINT PRICE CALLOUT END:' + System.now());
        if(null == response.ORDER_ITEMS_OUT.item) throw new CCWSSalesOrderException('TRANSLATE: No Pricing Data Received.');


        // Insert the response values
        // Yes, this is O(N^2) but the size of the array is not likely to be very large.
        String prodId;
        Decimal taxTotal = 0;
        Decimal depositTotal = 0;
        for (ccrz.cc_bean_CartItem cartItemBean : cartData.cartItems) {
            if (cartItemBean.productInventoryMsg == 'NO_INVENTORY') {
                cartItemBean.price = 0;
                cartItemBean.SubAmount = 0;
                cartItemBean.miscMessage = 'show';
            }
            else {
                ccrz__E_CartItem__c sObj = cartItemMap.get(cartItemBean.itemID);
                prodId = formatSku(productMap.get(sObj.ccrz__Product__c));
                for (CCWSSalesOrderRequestResponse_Out.BAPIITEMEX respitem : response.ORDER_ITEMS_OUT.item) {
                    if (prodId.equals(respitem.MATERIAL)) {
                        System.debug(LoggingLevel.INFO, 'respitem.SUBTOTAL4: ' + respitem.SUBTOTAL4);
                        System.debug(LoggingLevel.INFO, 'respitem.NET_VALUE: ' + respitem.NET_VALUE);
                        System.debug(LoggingLevel.INFO, 'respitem.SUBTOTAL_4: ' + respitem.SUBTOTAL_4);
                        System.debug(LoggingLevel.INFO, 'respitem.TX_DOC_CUR: ' + respitem.TX_DOC_CUR);
                        System.debug(LoggingLevel.INFO, 'respitem.SUBTOTAL5: ' + respitem.SUBTOTAL5);
                        cartItemBean.SubAmount = Decimal.valueOf(respitem.SUBTOTAL4);
                        cartItemBean.price = cartItemBean.SubAmount / cartItemBean.quantity;
                        taxTotal += Decimal.valueOf(respitem.TX_DOC_CUR);
                        depositTotal += Decimal.valueOf(respitem.SUBTOTAL5);
                        cartItemBean.miscMessage = 'show';
                    }
                }
            }
        }
        cartData.extrinsic.put('taxTotal', String.valueOf(taxTotal));
        cartData.extrinsic.put('depositTotal', String.valueOf(depositTotal));
        System.debug(LoggingLevel.INFO, 'POINT PRICE END:' + System.now());
    }

    // Returns formatted string with up to 6 leading zeros and increments in the input by 10.
    private String leftPadAndIncrement(Integer input) {
        String inStr = String.valueOf(input);
        input += INCREMENT;
        return ('000000' + inStr).substring(inStr.length());
    }

    private String formatDate(Date inputDate) {
        if (inputDate != null) {
            DateTime dt = DateTime.newInstanceGmt(inputDate, Time.newInstance(0, 0, 0, 0));
            return dt.format('yyyyMMdd');
        }
        else
            return '';
    }

    private String padData(Object inputData, Integer length, String padChar) {
        if (inputData == null)
            return null;
        String val = String.valueOf(inputData);
        while (val.length() < length)
            val = padChar + val;
        return val;
    }

    private String formatSku(String sku) {
        return '000000000000' + sku;
        //return sku;
    }

    private final Integer INCREMENT = 10;

}
