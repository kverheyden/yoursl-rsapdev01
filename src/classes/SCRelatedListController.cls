/* 
 * @(#)SCRelatedListController.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.  
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use  
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author narmbruster@gms-online.de
 */

/*
 * Constructs a related list based on the fieldset of an obect and reads the data 
 * based on the field and id value. Use this component to implmenent related lists 
 * with more than 10 columns and to support additional functionallity. 
 */
public with sharing class SCRelatedListController 
{
    transient List<SObject> datalist;

    // Manatory attributes used to build the related list based on the object / fieldset
    public String attrObject {get; set;}
    public String attrRefField {get; set;}
    public String attrRefId {get; set;}
    public String attrOrderBy {get; set;}
    public String attrFieldSet {get; set;}

    // Optional attributes that control the related list
    public String attrTitle {get; set;}

    public Boolean attrEnableLink {get; set;}
    public Boolean attrEnableNew {get; set;}
    public Boolean attrEnableEdit {get; set;}
    public Boolean attrEnableDel {get; set;}
    
    // internal helper that are used to process the data
    Schema.SObjectType  sObjectType;
    List<Schema.FieldSetMember> fieldSetFields;


   /*
    * Constructor used to initialize basic member variables 
    */
    public SCRelatedListController()
    {
    } 


   /*
    * Load on demand - called when the component is to be viewed   
    * @return list of records with the required fields
    */
    public List<SObject> getData()
    {
        if(attrRefId != null) // always read the data to enalbe "rerender"
        {
            datalist = readData(attrRefId, getDataFields());
        }
        return datalist;
    }
    
    public List<Schema.FieldSetMember> getDataFields() 
    {
        if(sObjectType == null)
        {
            sObjectType = Schema.getGlobalDescribe().get(attrObject);        
        
        }
        if(sObjectType != null && fieldSetFields == null)
        {
           Schema.DescribeSObjectResult d = sObjectType.getDescribe();
           Map<String, Schema.FieldSet> fsMap = d.fieldSets.getMap();
           Schema.FieldSet fsSchema = fsMap.get(attrFieldSet);
           if(fsSchema != null)
           {
               fieldSetFields = fsSchema.getFields();
           }
        }
        //return SObjectType.SCOrderRole__c.FieldSets.orderoverviewlist.getFields();
        return fieldSetFields;
    }
    
   /*
    * Read the data based on the field set
    * @param refid  the id of the master object to read
    * @param fields the field set list that is used to read the data records
    * @return a list of objects 
    */
    List<SObject> readData(String refid, List<Schema.FieldSetMember> fields)
    {
        if(fields != null && refid != null)
        {
            String query = 'SELECT ';
            for(Schema.FieldSetMember f : getDataFields()) 
            {
                query += f.getFieldPath() + ', ';
            }
            query += 'Id FROM ' + attrObject  + ' WHERE ' + attrRefField + '= :refid ';
            if(attrOrderBy != null)
            {
                query += ' ORDER BY ' + attrOrderBy;
            }
            
            // currently ony this limit is supported (pagination is to be implemented, yet)
            query += ' limit 100';
            
            return Database.query(query);
        }
        return null;
    }        
}
