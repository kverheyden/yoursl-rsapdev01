/*
 * @(#)SCInstalledBaseTreeControllerTest.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  
 */
 
 @isTest
private class SCInstalledBaseTreeControllerTest
{
    static testMethod void runTest1() 
    {
        // Prepare some test data
        SCHelperTestClass.createInstalledBaseLocation(true);
        
        Test.startTest();
    
        SCInstalledBaseTreeController con = new  SCInstalledBaseTreeController();
        
        con.accountId = SCHelperTestClass.account.id;
        System.assertEquals(SCHelperTestClass.account.id, con.AccountId);
    
        List<SCInstalledBaseTreeController.ReadLocation> res = con.getReadLocation();
        System.assertEquals(res[0].installedBaseLocation.Id, SCHelperTestClass.location.Id);
        
        Test.stopTest();
    }
}
