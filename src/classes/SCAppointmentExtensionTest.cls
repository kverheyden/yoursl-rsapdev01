/*
 * @(#)SCAppointmentExtensionTest.cls SCCloud    dh 20.09.2010
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author DH <dherdt@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCAppointmentExtensionTest
{   
    /**
     * Appointment extension under test
     */
    private static SCAppointmentExtension appointmentExtension;
        
    static testMethod void testAppointmentExtension1()
    {   
        SCHelperTestClass3.createCustomSettings('XX', true);
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createInstalledBase(true);
        SCHelperTestClass.createOrderItem(true);
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.orderRolePerson(SCHelperTestClass.order, SCHelperTestClass.account, '50301', true);  
        
        ApexPages.StandardController controller = new ApexPages.StandardController(SCHelperTestClass.orderItem);
        appointmentExtension = new SCAppointmentExtension(controller, true);
        System.assertNotEquals(null, appointmentExtension.getAreaInfo());
        appointmentExtension.getStandbyInfo();
        appointmentExtension.getLocationAddress();
        appointmentExtension.getInstalledBaseDetails();
        //TODO System.assertEquals(true, appointmentExtension.canDispatch);
        appointmentExtension.GetIsGeocoded();
        appointmentExtension.GetIsTimewindowValid();
        appointmentExtension.GetHasPending();
        appointmentExtension.getCanHaveMoreAppointments();
        appointmentExtension.getCanHaveAdditionalEmployee();
        appointmentExtension.getIsCustomerLocked();
        appointmentExtension.OnUpdateData('', appointmentExtension.locaddr);
        appointmentExtension.onGetAppointments();
        appointmentExtension.onGetAppointmentsNext();
        appointmentExtension.returnToOrderOnCancel = true;
        appointmentExtension.saveFixed();
        appointmentExtension.save();
        appointmentExtension.onCancel();
        appointmentExtension.returnToOrderOnCancel = false;
        appointmentExtension.saveFixed();
        appointmentExtension.save();
        appointmentExtension.onCancel();
        appointmentExtension.getOrder2();
        appointmentExtension.getOrderItem();
        appointmentExtension.getAreaItems();
        appointmentExtension.getAppointments();
        appointmentExtension.refresh();
        appointmentExtension.cancelApp();
        appointmentExtension.saveOrder();
        appointmentExtension.clearAppList();
        appointmentExtension.onScheduleContractVisit();
    }

    static testMethod void testAppointmentExtension2()
    {   
        SCHelperTestClass3.createCustomSettings('XX', true);
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createInstalledBase(true);
        SCHelperTestClass.createOrderItem(true);
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.orderRolePerson(SCHelperTestClass.order, SCHelperTestClass.account, '50301', true);
        
        SCOrderPageController pageController = new SCOrderPageController(SCHelperTestClass.account.Id);
        pageController.newInstalledBase = SCHelperTestClass.installedBaseSingle.Id;
        pageController.addNewOrderItem(pageController.order);
        appointmentExtension = new SCAppointmentExtension(pageController, true);
        appointmentExtension.getAppointmentsEx();        
        appointmentExtension.getIsCustomerLocked();
        System.assertEquals(true, appointmentExtension.showAppointments);
        appointmentExtension.saveOrder();
        appointmentExtension.save();
        appointmentExtension.saveAppointment();
    }

/*TODO
    static testMethod void testAppointmentExtension3()
    {   
        SCHelperTestClass3.createCustomSettings('GB', true);
        SCHelperTestClass.createDomsForOrderCreation();

        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        SCHelperTestClass.createOrderTestSet3(true);
        appointmentExtension = new SCAppointmentExtension();
        
        ApexPages.currentPage().getParameters().put('oid', SCHelperTestClass.order.Id);
        appointmentExtension.onScheduleContractVisit();
        appointmentExtension.serviceOrder.CustPropIsParkingAvailable__c = 'yes';
        appointmentExtension.serviceOrder.CustPropIsProvideParkingPermit__c = 'no';
        appointmentExtension.serviceOrder.CustPropIsApplAccessible__c = 'yes';
        appointmentExtension.onContinueServiceMessage();
        System.assertEquals(appSettings.CUSTOMERPROPERTY_NOPERMIT_FOLLOWUP1__c, appointmentExtension.serviceOrder.FollowUp1__c);
        System.assertEquals(appSettings.CUSTOMERPROPERTY_NOPERMIT_FOLLOWUP2__c, appointmentExtension.serviceOrder.FollowUp2__c);
        
        appointmentExtension.serviceOrder.CustPropIsParkingAvailable__c = 'no';
        appointmentExtension.serviceOrder.CustPropIsProvideParkingPermit__c = 'yes';
        appointmentExtension.serviceOrder.CustPropIsApplAccessible__c = 'yes';
        appointmentExtension.onContinueServiceMessage();
        System.assertEquals(appSettings.CUSTOMERPROPERTY_NOPARKING_FOLLOWUP1__c, appointmentExtension.serviceOrder.FollowUp1__c);
        System.assertEquals(appSettings.CUSTOMERPROPERTY_NOPARKING_FOLLOWUP2__c, appointmentExtension.serviceOrder.FollowUp2__c);
        
        appointmentExtension.serviceOrder.CustPropIsParkingAvailable__c = 'yes';
        appointmentExtension.serviceOrder.CustPropIsProvideParkingPermit__c = 'yes';
        appointmentExtension.serviceOrder.CustPropIsApplAccessible__c = 'yes';
        appointmentExtension.onContinueServiceMessage();
        System.assertEquals(null, appointmentExtension.serviceOrder);
        
        appointmentExtension = new SCAppointmentExtension();
        ApexPages.currentPage().getParameters().put('oid', SCHelperTestClass.order.Id);
        appointmentExtension.onScheduleContractVisit();
        appointmentExtension.serviceOrder.CustPropIsParkingAvailable__c = 'yes';
        appointmentExtension.serviceOrder.CustPropIsProvideParkingPermit__c = 'yes';
        appointmentExtension.serviceOrder.CustPropIsApplAccessible__c = 'no';
        appointmentExtension.onContinueServiceMessage();
        System.assertEquals(null, appointmentExtension.serviceOrder.FollowUp1__c);
        System.assertEquals(null, appointmentExtension.serviceOrder.FollowUp2__c);
    }
*/    

    static testMethod void testAppointmentExtension4()
    {   
        SCHelperTestClass3.createCustomSettings('GB', true);
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createInstalledBase(true);
        SCHelperTestClass.createOrderItem(true);
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.orderRolePerson(SCHelperTestClass.order, SCHelperTestClass.account, '50301', true);
        
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        SCOrderPageController pageController = new SCOrderPageController(SCHelperTestClass.account.Id);
        appointmentExtension = new SCAppointmentExtension(pageController, true);
        pageController.showServiceMessage = 1;
        appointmentExtension.getCanHaveMoreAppointments();
        
        appointmentExtension.getAppointmentsEx();        
        pageController.order.order.CustPropIsParkingAvailable__c = 'yes';
        pageController.order.order.CustPropIsProvideParkingPermit__c = 'no';
        pageController.order.order.CustPropIsApplAccessible__c = 'yes';
        appointmentExtension.onContinueServiceMessage();
        System.assertEquals(appSettings.CUSTOMERPROPERTY_NOPERMIT_FOLLOWUP1__c, pageController.order.order.FollowUp1__c);
        System.assertEquals(appSettings.CUSTOMERPROPERTY_NOPERMIT_FOLLOWUP2__c, pageController.order.order.FollowUp2__c);
        
        pageController.order.order.CustPropIsParkingAvailable__c = 'no';
        pageController.order.order.CustPropIsProvideParkingPermit__c = 'yes';
        pageController.order.order.CustPropIsApplAccessible__c = 'yes';
        appointmentExtension.onContinueServiceMessage();
        System.assertEquals(appSettings.CUSTOMERPROPERTY_NOPARKING_FOLLOWUP1__c, pageController.order.order.FollowUp1__c);
        System.assertEquals(appSettings.CUSTOMERPROPERTY_NOPARKING_FOLLOWUP2__c, pageController.order.order.FollowUp2__c);
        
        pageController.order.order.CustPropIsParkingAvailable__c = 'yes';
        pageController.order.order.CustPropIsProvideParkingPermit__c = 'yes';
        pageController.order.order.CustPropIsApplAccessible__c = 'yes';
        appointmentExtension.onContinueServiceMessage();
        System.assertEquals(null, pageController.order.order.FollowUp1__c);
        System.assertEquals(null, pageController.order.order.FollowUp2__c);
        
        pageController.order.order.CustPropIsParkingAvailable__c = 'no';
        pageController.order.order.CustPropIsProvideParkingPermit__c = 'yes';
        pageController.order.order.CustPropIsApplAccessible__c = 'no';
        appointmentExtension.onContinueServiceMessage();
        System.assertEquals(null, pageController.order.order.FollowUp1__c);
        System.assertEquals(null, pageController.order.order.FollowUp2__c);
        System.assertEquals(SCfwConstants.DOMVAL_ORDERSTATUS_CANCELLED, pageController.order.order.Status__c);
        
        pageController.showServiceMessage = 2;
        pageController.dispatchManuallyEx();
        pageController.order.order.CustPropIsParkingAvailable__c = 'yes';
        pageController.order.order.CustPropIsProvideParkingPermit__c = 'yes';
        pageController.order.order.CustPropIsApplAccessible__c = 'yes';
        appointmentExtension.onContinueServiceMessage();
    }
    
    
    //@author GMSS
    static testMethod void CodeCoverageA()
    {
        try { SCHelperTestClass3.createCustomSettings('DE',true); } catch(Exception e) {}
        try { SCHelperTestClass.createDomsForOrderCreation(); } catch(Exception e) {}
           
        SCAppointmentExtension obj = new SCAppointmentExtension(true);
        
        try { Boolean tmp = obj.canDispatch; } catch(Exception e) {}
        try { obj.CompOrderId = null; } catch(Exception e) {}
        try { SCAppointmentExtension tmp = new SCAppointmentExtension(new ApexPages.StandardController(null)); } catch(Exception e) {}
        Test.setCurrentPage(Page.SCAppointmentPageContract);
        try { SCAppointmentExtension tmp = new SCAppointmentExtension(new ApexPages.StandardController(null)); } catch(Exception e) {}
        try { SCAppointmentExtension tmp = new SCAppointmentExtension(new SCOrderPageController()); } catch(Exception e) {}
        try { obj.getAreaInfo(); } catch(Exception e) {}
        try { obj.getLocationAddress(); } catch(Exception e) {}
        try { obj.GetIsGeocoded(); } catch(Exception e) {}
        try { obj.GetIsTimewindowValid(); } catch(Exception e) {}
        try { obj.getCanHaveMoreAppointments(); } catch(Exception e) {}
        try { obj.getAppointments('Test'); } catch(Exception e) {}
        try { obj.getAppointments('Test', Date.Today()); } catch(Exception e) {}
        try { obj.curRow = 1; } catch(Exception e) {}
        try { obj.callout(); } catch(Exception e) {}
        try { obj.callout(); } catch(Exception e) {}
        try { obj.callout(); } catch(Exception e) {}
        try { obj.saveAppointment(); } catch(Exception e) {}
        
        try
        {
            SCAppointmentExtension tmp = new SCAppointmentExtension(new SCOrderPageController());
            tmp.saveAppointment();
        }
        catch(Exception e) {}
    }
}
