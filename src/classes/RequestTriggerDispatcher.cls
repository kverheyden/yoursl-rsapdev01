public class RequestTriggerDispatcher extends TriggerDispatcherBase{
	
	private static Boolean isAfterUpdateProcessing = false;
	private static Boolean isAfterInsertProcessing = false;
	
	public override void afterUpdate(TriggerParameters tp) {
		if (!isAfterUpdateProcessing) {
			isAfterUpdateProcessing = true;
			execute( new RequestAfterUpdateTriggerHandler(), tp, TriggerParameters.TriggerEvent.afterUpdate);
			isAfterUpdateProcessing = false;
		} 
		else 
			execute(null, tp, TriggerParameters.TriggerEvent.afterUpdate);
	}

	public override void afterInsert(TriggerParameters tp) {
		if (!isAfterUpdateProcessing) {
			execute( new RequestAfterInsertTriggerHandler(), tp, TriggerParameters.TriggerEvent.afterInsert);
		}
		else
			execute(null, tp, TriggerParameters.TriggerEvent.afterInsert);
	}
}
