@isTest
private class cc_cceag_test_Cart{
	
	@isTest static void testAllowCheckout() {
		cc_cceag_api_CartValidation cv = new cc_cceag_api_CartValidation();
		System.assert(cv.allowCheckout());
	}
	
	@isTest static void testGetCartMessages() {
		cc_cceag_api_CartValidation cv = new cc_cceag_api_CartValidation();
		System.assertEquals(null, cv.getCartMessages(new Map<String, Object>{'CartBean' => null}));
	}

	@isTest static void testGetDeliveryDates() {
		Account a = cc_cceag_test_TestUtils.createAccount(true);

		List<cc_bean_Outlet> outletList = new List<cc_bean_Outlet>{new cc_bean_Outlet(a)};
		Map<String, Object> outletMap = new Map<String, Object>{'shipTos' => outletList};

		cc_cceag_api_DeliveryDate ctrl_dd = new cc_cceag_api_DeliveryDate();
		Map<String,Object> result = ctrl_dd.getDeliveryDates(outletMap);

		System.assertEquals(1, result.size());
		System.assertNotEquals(null, result.get('11111'));


	}


	@isTest static void testApiInventory(){

		List<ccrz__E_Product__c> ps = new List<ccrz__E_Product__c>{
            new ccrz__E_Product__c(Name='Product1', ccrz__Sku__c='sku1', ccrz__ProductId__c='sku1', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12),
            new ccrz__E_Product__c(Name='Product2', ccrz__Sku__c='sku2', ccrz__ProductId__c='sku2', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12),
            new ccrz__E_Product__c(Name='Product3', ccrz__Sku__c='sku3', ccrz__ProductId__c='sku3', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12),
            new ccrz__E_Product__c(Name='Product4', ccrz__Sku__c='sku4', ccrz__ProductId__c='sku4', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12),
            new ccrz__E_Product__c(Name='Product1_1', ccrz__Sku__c='sku1_1', ccrz__ProductId__c='sku1_1', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12),
            new ccrz__E_Product__c(Name='Product1_2', ccrz__Sku__c='sku1_2', ccrz__ProductId__c='sku1_2', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12),
            new ccrz__E_Product__c(Name='Product1_3', ccrz__Sku__c='sku1_3', ccrz__ProductId__c='sku1_3', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12)
        };
        insert ps;
        
        List<Product2> prod2s = new List<Product2> {
        	new Product2(Name='Product1', ID2__c=ps.get(0).ccrz__ProductId__c),
        	new Product2(Name='Product2', ID2__c=ps.get(1).ccrz__ProductId__c),
        	new Product2(Name='Product3', ID2__c=ps.get(2).ccrz__ProductId__c),
        	new Product2(Name='Product4', ID2__c=ps.get(3).ccrz__ProductId__c),
        	new Product2(Name='Product1_1', ID2__c=ps.get(4).ccrz__ProductId__c),
        	new Product2(Name='Product1_2', ID2__c=ps.get(5).ccrz__ProductId__c),
        	new Product2(Name='Product1_3', ID2__c=ps.get(6).ccrz__ProductId__c)
        };
        insert prod2s;

		SCPlant__c plant = new SCPlant__c(Name='1111');
        insert plant;

        list<ATPStockQuantity__c> stockqtyItems = new list<ATPStockQuantity__c> {
            new ATPStockQuantity__c(DateDay1__c=Date.today(), Quantity1__c=100, SCPlant__c=plant.id, Product2__c=prod2s.get(0).Id, ccrz_product__c = ps.get(0).Id),
            new ATPStockQuantity__c(DateDay2__c=Date.today(), Quantity2__c=100, SCPlant__c=plant.id, Product2__c=prod2s.get(1).Id, ccrz_product__c = ps.get(1).Id),
            new ATPStockQuantity__c(DateDay3__c=Date.today(), Quantity3__c=100, SCPlant__c=plant.id, Product2__c=prod2s.get(2).Id, ccrz_product__c = ps.get(2).Id),
            new ATPStockQuantity__c(DateDay4__c=Date.today(), Quantity4__c=100, SCPlant__c=plant.id, Product2__c=prod2s.get(3).Id, ccrz_product__c = ps.get(3).Id),
            new ATPStockQuantity__c(DateDay5__c=Date.today(), Quantity5__c=100, SCPlant__c=plant.id, Product2__c=prod2s.get(4).Id, ccrz_product__c = ps.get(4).Id),
            new ATPStockQuantity__c(DateDay1__c=Date.today(), Quantity1__c=100, SCPlant__c=plant.id, Product2__c=prod2s.get(5).Id, ccrz_product__c = ps.get(5).Id),
            new ATPStockQuantity__c(DateDay1__c=Date.today(), Quantity1__c=100, SCPlant__c=plant.id, Product2__c=prod2s.get(6).Id, ccrz_product__c = ps.get(6).Id)
        };
        insert stockqtyItems; 
        list<ATPStockQuantity__c> stockqtyItemsQ = [SELECT ccrz_product__c, SCPlant__r.Name, DateDay1__c FROM ATPStockQuantity__c];
        System.debug(LoggingLevel.INFO, stockqtyItemsQ);

        ccrz__E_ContactAddr__c addr = new ccrz__E_ContactAddr__c(ccrz__AddressFirstLine__c='500 Main', ccrz__City__c='Chicago',ccrz__MailStop__c=plant.Name);
        insert addr;
        ccrz__E_Cart__c cart = new ccrz__E_Cart__c(
            ccrz__SessionID__c = 'test session',
            ccrz__storefront__c='DefaultStore',
            ccrz__RequestDate__c = Date.today(),
            ccrz__ActiveCart__c = true,
            ccrz__CartStatus__c = 'Open',
            ccrz__CartType__c = 'Cart',
            ccrz__ShipTo__c = addr.Id,
            ccrz__BillTo__c = addr.Id,
            Ownerid = UserInfo.getUserId()
        );

		cc_cceag_api_InventoryExt ctrl_inventory = new cc_cceag_api_InventoryExt();
		Map<Id, ccrz__E_ProductInventoryItem__c> invMap = ctrl_inventory.getAvailabilityQtyMsg(new List<Id>{ps.get(0).Id,ps.get(1).Id,ps.get(2).Id,ps.get(3).Id,ps.get(4).Id});

		insert cart;
		
		ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
		ctx.portalUserId = UserInfo.getUserId();
		ctx.storefront = 'DefaultStore';
		ccrz.cc_CallContext.initRemoteContext(ctx);
		invMap = ctrl_inventory.getAvailabilityQtyMsg(new List<Id>{ps.get(0).Id,ps.get(1).Id,ps.get(2).Id,ps.get(3).Id,ps.get(4).Id});
		System.assertNotEquals(null, invMap);
	}

	@isTest static void testProductQuantityRule(){
		cc_cceag_api_ProductQuantityRule ctrl_rule = new cc_cceag_api_ProductQuantityRule();
		ccrz__E_Product__c p = cc_cceag_test_TestUtils.createProduct(true);
		System.assertNotEquals(null, ctrl_rule.getRules(new List<String>{'1111'}));
	}
	
}
