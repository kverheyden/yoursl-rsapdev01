public with sharing class ArticleGroupTriggerDispatcher extends TriggerDispatcherBase {

	public override void beforeUpdate(TriggerParameters tp) {
		execute(new ArticleGroupTriggerHandler.ArticleGroupBUHandler(), tp, TriggerParameters.TriggerEvent.beforeUpdate);
	}

	public override void beforeInsert(TriggerParameters tp) {
		execute(new ArticleGroupTriggerHandler.ArticleGroupBIHandler(), tp, TriggerParameters.TriggerEvent.beforeInsert);
	}
}
