/*
 * @(#)SCOrderRecalculatePriceExtensionTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCOrderRecalculatePriceExtensionTest 
{
    /**
     * Class under test
     * @author Thorsten Klein <tklein@gms-online.de>
     *
     */
    static testMethod void testBasicUsage()
    {

        // create test order
        SCHelperTestClass.createOrderTestSet(true);
        upsert SCHelperTestClass.appointments;
        
        ApexPages.currentPage().getParameters().put('id', SCHelperTestClass.order.id);
        
        List<SCOrderLine__c> orderLines = new List<SCOrderLine__c>();
        
        for (SCboOrderLine boOrderLine : SCHelperTestClass.boOrderLines)
        {
            orderLines.add(boOrderLine.orderLine);
        }
        
        List<SCOrderLine__c> setLines = orderLines.deepClone(true);

        Test.startTest();
        
        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(setLines);
        controller.setSelected(setLines);

        SCOrderRecalculatePriceExtension ext = new SCOrderRecalculatePriceExtension(controller);

        // REMARK: Optimised for code coverage and nothing else!
        ext.getSelectedCount();
        ext.onCancel();
        
        try
        {
            ext.onContinue();
        }
        catch (Exception e)
        {
        
        }
        Test.stopTest();
    }
}
