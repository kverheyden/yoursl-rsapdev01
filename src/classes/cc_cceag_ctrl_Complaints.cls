global with sharing class cc_cceag_ctrl_Complaints 
{
    public static ccrz.cc_RemoteActionResult handleException(Exception e, ccrz.cc_RemoteActionResult result)
    {
        System.debug(System.LoggingLevel.ERROR, e.getMessage());
        System.debug(System.LoggingLevel.ERROR, e.getStackTraceString());
        ccrz.cc_bean_Message msg = new ccrz.cc_bean_Message();
        msg.type = ccrz.cc_bean_Message.MessageType.CUSTOM;
        msg.classToAppend = 'messagingSection-Error';
        msg.message = e.getStackTraceString();
        msg.severity = ccrz.cc_bean_Message.MessageSeverity.ERROR;
        result.messages.add(msg);        
        return result;
    }
    
    public static ccrz.cc_RemoteActionResult setupResultFromContext(ccrz.cc_RemoteActionContext ctx)
    {
		ccrz.cc_CallContext.initRemoteContext(ctx);
		ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
		result.inputContext = ctx;
		result.success = false; 
		return result;        
    }
    
	@RemoteAction
	global static ccrz.cc_RemoteActionResult fetchData(ccrz.cc_RemoteActionContext ctx) 
    {       		
		ccrz.cc_RemoteActionResult result = setupResultFromContext(ctx);
		try {
			// getting picklist values for "thema"
			List<Object> options = new List<Object>();
			Schema.DescribeFieldResult fieldResult = Case.ECOM_Thematische_Zuordnung__c.getDescribe();
   			List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();        
   			for( Schema.PicklistEntry f : ple) {
                if (f.isActive()) {
                    Map<String,Object> option = new Map<String,Object>();
                    option.put('value', f.getValue());
                    option.put('label', f.getLabel());
                	options.add(option);
                }
   			}
   			
   			result.data = options;       
			
			result.success = true;
		} catch(Exception e) { result = handleException(e,result); }
		return result;
	}

	@RemoteAction
	global static ccrz.cc_RemoteActionResult saveData(ccrz.cc_RemoteActionContext ctx, String topic, String message) {
		ccrz.cc_RemoteActionResult result = setupResultFromContext(ctx);
        try {
			String userId = cc_cceag_dao_User.getFlowUserWithContext(ctx);

			User user = [SELECT Contact.Id, Contact.AccountId From User WHERE Id=:userId];

			Case c = new Case();
            RecordType rt = [select Id from RecordType where Name = 'E-Commerce' and SobjectType = 'Case'];
			c.RecordTypeId = rt.Id;
			c.Thematische_Zuordnung__c = topic;
            c.ECOM_Thematische_Zuordnung__c = topic;
			c.Description = message;
			c.Kontaktrichtung__c = 'E-Commerce';
			c.ContactId = user.Contact.Id;
			c.AccountId = user.Contact.AccountId;
			c.Priority = 'Mittel';
			
			insert c;
			
			Map<string,string> data = new Map<string,string>();
			data.put('caseId',c.Id);
			result.data = data;
			result.success = true;
		} catch(Exception e) { result = handleException(e,result); }
		return result;
	}
	
	@RemoteAction
    global static Map<string,string> doUploadAttachment(ccrz.cc_RemoteActionContext ctx, String caseId, String attachmentBody, String attachmentName, String attachmentId) 
    {
    	Map<string,string> r = new Map<string,string>();
    	
    	if(caseId == null) {
    		r.put('error','Case Id was null');
    		return r;
    	}
    	if(attachmentBody == null) {
    		r.put('error','Attachment Body was null');
    		return r;
    	}
		ccrz.cc_CallContext.initRemoteContext(ctx);
		String userId =  cc_cceag_dao_User.getFlowUserWithContext(ctx);
    	
        System.Debug('fetching details for user');
        User user = [SELECT Contact.Id, Contact.AccountId From User WHERE Id=:userId];
        System.Debug(user);
        Case caseObj;
        System.Debug('Fetching case for id='+caseId+' and contactID='+user.Contact.Id);
        List<Case> cases = [SELECT Id From Case Where Id=:caseId AND ContactId=:user.Contact.Id];
        if (cases.size()>0) caseObj = cases.get(0);
        if(caseObj == null) {
        	r.put('error','Case not found!');
        	return r;
        }
        
		Attachment att = getAttachment(attachmentId);
		String newBody = '';
		if(att.Body != null) {
			newBody = EncodingUtil.base64Encode(att.Body);
		}
		newBody += attachmentBody;
		att.Body = EncodingUtil.base64Decode(newBody);
		if(attachmentId == null) {
			att.Name = attachmentName;
			att.parentId = caseObj.Id;
		}
		upsert att;
		
		r.put('attachmentId',att.Id);
		r.put('success','1');
		return r;
    }	
    
    private static Attachment getAttachment(String attId) 
    {
        list<Attachment> attachments = [SELECT Id, Body FROM Attachment WHERE Id =: attId];
        if(attachments.isEmpty()) {
            Attachment a = new Attachment();
            return a;
        } else {
            return attachments[0];
        }
    }
}
