/**
* @author		Development (AB)
* 				H&W Consult GmbH
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				http://www.hundw.com
*
* @description	Handler for FeedItem callouts of CCWCCustomerNote
*
* @date			17.12.2013
*
*/
public with sharing class CCWCCustomerNoteHandler {
	
	/**
	* Prepare callout for CustomerNote to SAP
	*
	* @param newFeedItems 	Feed Items
	* @param async			Async for calloutCore
	* @param test			Testmode
	* 
	*/
    public void sendCallout(FeedItem [] newFeedItems, boolean async, boolean test){   	
		Set<Id> accIds = new Set<Id>();
		Set<Id> usrIds = new Set<Id>();	
		Set<Id> feedIds = new Set<Id>();
		for(FeedItem feet : newFeedItems){
			usrIds.add(feet.CreatedById);
			accIds.add(feet.ParentId);
			feedIds.add(feet.Id);
		}			
		List<Account> accounts = [SELECT Id, ID2__c FROM Account WHERE Id =: accIds];				
  		List<User> users = [SELECT Id, ID2__c FROM User WHERE Id =: usrIds];  	
  		List<FeedItem> items = [Select Title, Id, ParentId, CreatedBy.FirstName, CreatedBy.LastName, CreatedById, Body From FeedItem where Id =:feedIds];
  		List<String> FeedItemData = new List<String>();
  		String userName;
		for(FeedItem feedI : items){
			if (feedI != null){
				for(Account acc: accounts){
					if(acc != null){
						for(User usr: users){
							if(usr != null){
								if(acc.Id == feedI.ParentId && usr.Id == feedI.CreatedById){
									FeedItemData.add(feedI.Title);				
									FeedItemData.add(feedI.Body);
									FeedItemData.add(feedI.CreatedById);	
									FeedItemData.add(feedI.ParentId);	
									if(feedI.CreatedBy.FirstName != null){
            							userName = feedI.CreatedBy.FirstName + ' ' + feedI.CreatedBy.LastName;
            						}else{
            							userName = feedI.CreatedBy.LastName;
            						}
									//System.debug('###...INPUT: feedId:' + feedI.Id + ', OutletNr: ' + acc.ID2__c +  ', OwnerId2: ' + usr.ID2__c + ', FeedItem String: ' + FeedItemData + ', Name: ' + userName + ', Async: ' + async + ', Test: ' + test);
									CCWCCustomerNote.futurecallout(feedI.Id, async, test, acc.ID2__c, usr.ID2__c, FeedItemData, userName);
								}
							}
						}
					}
				}
			}
		}
    }
}
