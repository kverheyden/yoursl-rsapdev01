public with sharing class cc_cceag_util_Utils {
    public cc_cceag_util_Utils() {
        
    }

    public static ccrz.cc_RemoteActionResult initAction(ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
        result.inputContext = ctx;
        result.success = false;
        return result;
    }

    public static ccrz.cc_bean_Message buildExceptionMessage(Exception e) {
        ccrz.cc_bean_Message msg = new ccrz.cc_bean_Message();
        msg.type = ccrz.cc_bean_Message.MessageType.CUSTOM;
        msg.classToAppend = 'messagingSection-Error';
        msg.message = e.getStackTraceString() + ' ' + e.getMessage();
        msg.severity = ccrz.cc_bean_Message.MessageSeverity.ERROR;
        return msg;
    }

    public static ccrz.cc_bean_Message buildErrorMessage(String message, String labelId, String classToAppend) {
        return buildMessage(message, labelId, classToAppend, ccrz.cc_bean_Message.MessageSeverity.ERROR);
    }

    public static ccrz.cc_bean_Message buildMessage(String message, String labelId, String classToAppend, ccrz.cc_bean_Message.MessageSeverity severity) {
        ccrz.cc_bean_Message msg = new ccrz.cc_bean_Message();
        msg.type = ccrz.cc_bean_Message.MessageType.CUSTOM;
        if (classToAppend != null)
            msg.classToAppend = classToAppend;
        else
            msg.classToAppend = 'messagingSection-Error';
        msg.message = message;
        msg.labelId = labelId;
        msg.severity = severity;
        return msg;
    }
}
