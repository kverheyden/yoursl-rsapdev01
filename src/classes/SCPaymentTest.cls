/*
 * @(#)SCOrderTabComponentControllerTest.cls
 *  
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * @author Alexei Geiger <ageiger@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCPaymentTest 
{
    static testMethod void constructor() 
    {
        SCPaymentData pd = new SCPaymentData();
        pd.mode = SCPaymentData.MODE_SALESFORCE_TEST;
        
        SCPayment.call(pd);
    }

}
