/**
* @author		Development (AB)
* 				H&W Consult GmbH
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N. 
* 				http://www.hundw.com 
*
* @description	Test class for CCWCCustomerNoteHandler
*
* @date			04.12.2013
*
*/

@isTest
public with sharing class CCWCCustomerNoteHandlerTest {


	@isTest 
	static void test()
	{
		CCWCCustomerNoteHandler send = new CCWCCustomerNoteHandler();
		FeedItem note = prepareTestData();
		List<FeedItem> feeds = new List<FeedItem>();
		feeds.add(note);	
		send.sendCallout(feeds, false, true);
		send.sendCallout(feeds, false, false);	
	}


	public static FeedItem prepareTestData()
	{
		CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        appSettings = SCApplicationSettings__c.getInstance();
        
        CCSettings__c ccSettings = new CCSettings__c();
        ccSettings.SAPDispBasicAuthUsername__c = 'user';
        ccSettings.SAPDispBasicAuthPassword__c = 'passsword';
        ccSettings.SAPDispEndpointNotizenOut__c = 'NoteEndpoint';
        ccSettings.SAPDispServer__c = 'Server';
        insert ccSettings;
               
 
        // Create Account
		Account account = createAccount();
				
		//Create User
		User usr = createUser();
        
		// create Note
        FeedItem note = new FeedItem(
        	Title = 'Test', 
			ParentId = account.Id,
			Body  = 'Ein Test.',
			CreatedById = usr.Id
         );

        upsert note;
        return  note;

	}
	
	public static Account createAccount()
	{	
	    Account  account = new Account();
        account.LastName__c = 'testaccount 00';
        account.FirstName__c = 'Person';
        account.TargetGroup__c = '001';
        account.SubTargetGroup__c = 'DE-005';
        account.BillingPostalCode = '33106 PB';
        account.BillingCountry__c = 'DE';
        account.CurrencyIsoCode = 'EUR';
        account.Type = SCfwConstants.DOMVAL_ACCOUNT_TYPE_CUSTOMER;
        account.TaxType__c = SCfwConstants.DOMVAL_TAXACCOUNT_TAX;
        account.BillingStreet = 'Karl-Schurz Str.';
        account.BillingHouseNo__c = '29';
        account.BillingPostalCode = '33100 PB';
        account.BillingCity = 'Padebrorn';
        account.BillingCountry__c = 'PB';
        account.GeoX__c = 6.454447;
        account.GeoY__c = 53.253582;
        account.GeoApprox__c = false;
        insert account;
        return account;
	}
	
	public static User createUser()
	{	
	    User  usr = new User();
        usr.LastName = 'Muster';
        usr.Alias = 'Max';
        usr.Email = 'max@muster.de';
        usr.Username = 'Muster@muster.de';
        usr.CommunityNickname = 'Musi';
        usr.TimeZoneSidKey = 'Europe/Berlin';
        usr.LocaleSidKey = 'de_DE_EURO';
        usr.EmailEncodingKey = 'ISO-8859-1';
        Profile p = [select id from profile where name = 'System Administrator' LIMIT 1];
        usr.ProfileId = p.Id;
        usr.LanguageLocaleKey = 'de';
        insert usr;
        return usr;
	}	
	
}
