/*
 * @(#)SCMaintenancePlanningExtensionTest.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCMaintenancePlanningExtensionTest
{
    private static SCMaintenancePlanningExtension maintenancePlanningExtension;
    
    static testMethod void testMaintenancePlanningExtension1()
    {         
        SCHelperTestClass.createOrder(true);
    
        System.debug('#### SCHelperTestClass.businessUnit.id: ' + SCHelperTestClass.businessUnit.id);
    
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.createInstalledBaseLocation(true);
        SCHelperTestClass.createInstalledBase(true);
        SCHelperTestClass.createProductModel(true);
        SCHelperTestClass.createTestUsers(true);
        SCHelperTestClass2.createContractTemplates(true);
        SCHelperTestClass2.createContract('Contract', SCHelperTestClass2.insuranceTemplate.Id, SCHelperTestClass.account.Id, SCHelperTestClass.installedBaseSingle.Id, true);
        
        
        
        SCHelperTestClass2.contracts.get(0).AccountOwner__c = SCHelperTestClass.account.Id;
        
        SCContractSearchController pageController = new SCContractSearchController();
        
        Test.startTest();
        
        
        pageController.actualContractId = SCHelperTestClass2.contracts.get(0).Id;    
        
        pageController.textOutput = '';
        pageController.sortDir = 'asc';
        pageController.sortField = 'Name';
        Date newDate = System.today();
        pageController.account = SCHelperTestClass.account;
        pageController.accountOwner = SCHelperTestClass.account;
        pageController.accountRole = SCHelperTestClass.account;
        pageController.accountPostalCode = SCHelperTestClass.account.BillingPostalCode;
        pageController.contractNumber = SCHelperTestClass2.contracts.get(0).Name;
        pageController.accountName = SCHelperTestClass.account.LastName__c;
        pageController.accountStreet = SCHelperTestClass.account.BillingStreet;
        pageController.accountCity = SCHelperTestClass.account.BillingCity;
        pageController.selordertype  = null;
        pageController.selcontractstatus  = SCHelperTestClass2.contracts.get(0).Status__c;
        
                    
        pageController.onSearchMaintenancePlanning();
        pageController.maintenancePlanning() ;
        
        pageController.selectedDepartments = new List<String>();
        
        pageController.selectedDepartments.add(String.valueOf(SCHelperTestClass.businessUnit.id));
        
        
        maintenancePlanningExtension = new SCMaintenancePlanningExtension(pageController);
        
        maintenancePlanningExtension.init();
        maintenancePlanningExtension.getSetCon();
        maintenancePlanningExtension.getContractVisits();
        maintenancePlanningExtension.getNumberOfCollectiveMaintenace();
        maintenancePlanningExtension.getNumberOfCollectiveMaintenaceCapacity();
        maintenancePlanningExtension.getNumberOfStandardMaintenace();
        maintenancePlanningExtension.getNumberOfStandardMaintenaceCapacity();
        maintenancePlanningExtension.getEngineerCapacityType();
        maintenancePlanningExtension.getTeamCapacityType();
        maintenancePlanningExtension.showSettings();
        maintenancePlanningExtension.hideSettings();
        maintenancePlanningExtension.gotoStep1();
        maintenancePlanningExtension.gotoStep2();
        maintenancePlanningExtension.getNumberOfSelectedEngineer();
        maintenancePlanningExtension.reloadEngineer();
        
        /*        
        maintenancePlanningExtension.selectedItems = maintenancePlanningExtension.getContractVisits()[0].id;
        maintenancePlanningExtension.getReloadSetController();
        */
        
        maintenancePlanningExtension.contractSchedule = new SCContractSchedule__c();
        
        maintenancePlanningExtension.onSaveProcess();
        maintenancePlanningExtension.onCancel();
        
        Test.stopTest();
    }
}
