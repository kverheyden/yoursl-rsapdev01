/*
 * @(#)SCOrderInterfaceMonitorController.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  
 */
 
/**
 * Controller handling the order related interface items
 * Based on an example from the SF team.
 * @author gmsna 20.11.2012
 */
public with sharing class SCOrderInterfaceMonitorController
{
    public String ordno {get;set;}
    public SCOrder__c ord {get;set;}
    public Boolean pollStatus {get;set;}
    public Integer pollcounter {get;set;}
    public Integer pollcountermin {get;set;}
    public String currentTransaction {get;set;}
    public Boolean showall {get;set;}


    public String ordpendingExtOpAdd  {get;set;}    
    public String ordpendingExtOpAddId  {get;set;}

    public String ordpendingExtOpRem  {get;set;}    
    public String ordpendingExtOpRemId  {get;set;}
    
    /**
     * Standard constructor
     */ 
    public SCOrderInterfaceMonitorController()
    {
        ord = new SCOrder__c();
        
        pollStatus = false;
        pollcounter = 0;
        pollcountermin = -1;
        autopoll = false;
        currentTransaction = '';
    }
    
    /**
     * Initializes the monitoring of interface callouts when the order id is set.
     */ 
    public String oid 
    {
       get; 
       set 
       {
            oid = value;
            OnPollStatus(); 
        }
    }

    public Boolean autopoll {get; set;}
/*
    {
        get;
        set
        {
            if(value == true)
            {
                pollcountermin = 3;
                pollStatus = true;
             }
            else
            {
                pollcountermin = 0;
            }
        }
    }
*/


    
    // ------------------------------------------------------------------------
    // Status handling automation
    // ------------------------------------------------------------------------
    

   /*
    * automatic processing (simplification for user)
    */
    public PageReference OnCallNext() 
    {
        // the following status sequence 
        // 1. order must exist in SF 
        // 2. create order in SAP
        // 3. update order equipment (even if this call is optional it is required to ensure that all subitems are in SAP)
        // 4. order related material consumptions transferred to SAP
        // 5. order closed
        //### external op
        
        if(getOrderCreateActive())
        {
            OnOrderCreate(); 
        }
        else if(getOrderCreateRetryActive())
        {
            ord.ERPStatusOrderCreate__c = 'retry';
            OnOrderCreate(); 
        }
        else if(getOrderEquipmentUpdateActive() && ord.ERPStatusEquipmentUpdate__c == 'none')
        {
            OnOrderUpdateEquipment(); 
        }
        else if(getOrderEquipmentUpdateRetryActive())
        {
            ord.ERPStatusEquipmentUpdate__c = 'retry';
            OnOrderUpdateEquipment(); 
        }
        else if(getOrderMaterialConsumptionActive())
        {
            OnOrderMaterialConsumption(); 
        }
        // PMS 37312/GMS: Cancel & Retry -Button korrigieren (CCWCMaterialMovementCreate)
        else if(getOrderMaterialReservationRetryActive())
        {
            ord.ERPStatusMaterialReservation__c = 'retry';
            OnOrderMaterialReservation(); 
        }
        else if(getOrderMaterialConsumptionRetryActive())
        {
            ord.ERPStatusMaterialMovement__c = 'retry';
            OnOrderMaterialConsumption(); 
        }
        else if(getOrderCloseActive())
        {
            OnOrderClose(); 
        }
        else if(getOrderCloseRetryActive())
        {
            ord.ERPStatusOrderClose__c = 'retry';
            OnOrderClose(); 
        }
        return null;
    }

    public Boolean getOrderCallNextActive() 
    {
        return isOrderAvailable() && ord.ERPStatusOrderClose__c != 'ok';
    }


    // ------------------------------------------------------------------------
    // Monitoring button status handling 
    // ------------------------------------------------------------------------

    public  Boolean getOrderCreateActive() 
    {
        // Order create can be called if
        // 1. the order exists in SF
        // 2. the order has not yet been created in SAP (ERPOrderNo__c == null)
        // 3. there is no pending call to sap ERPStatusOrderCreate__c == none
        //
        // ERPStatusOrderCreate__c:
        //    none       order was created in CP and can be transferred to SAP
        //    pending    order is currently being transferred to SAP
        //    ok         order has been successfully transferred to SAP
        //    error      an error occured in the callout or the respose (retry is required)   
        
        return isOrderAvailable() && ord.ERPOrderNo__c == null && ord.ERPStatusOrderCreate__c == 'none';
    }

    public Boolean getOrderCreateRetryActive() 
    {
        // Order create retry can be called if
        // 1. the order exists in SF
        // 2. the order has not yet been created in SAP (ERPOrderNo__c == null)
        // 3. there is no pending call to sap ERPStatusOrderCreate__c == error
        
        return isOrderAvailable() && ord.ERPOrderNo__c == null && ord.ERPStatusOrderCreate__c == 'error';
    }
    

    // ------------------------------------------------------------------------

    public Boolean getOrderEquipmentUpdateActive() 
    {
        // Order equipment update can be called (multiple times) if
        // 1. the order exists in SF
        // 2. the order was created in SAP - ERPStatusOrderCreate__c = 'ok'
        // 3. there are no pending call to SAP
        // 4. there was no or a successful call to update the quipment
        // 5. order close has not yet been called
        // note: order update can be executed independently  
        //
        // ERPStatusEquipmentUpdate__c:
        //    none       order eq update was not yet called
        //    pending    order eq update is currently being transferred to SAP
        //    ok         order eq update has been successfully transferred to SAP
        //    error      an error occured in the callout or the respose (retry is required)+
        
        // and the order has no dummy equipment
        // PMS 37380: EQ-Update unterdrücken, wenn kein EQ da (Aufstellung mit Dummy-EQ im Auftrag)
           
        boolean result = (isOrderAvailable() && ord.ERPStatusOrderCreate__c == 'ok' && !transactionPending() &&
              (ord.ERPStatusEquipmentUpdate__c == 'none' || ord.ERPStatusEquipmentUpdate__c == 'ok') &&
               ord.ERPStatusOrderClose__c == 'none');
        if(result && ord.OrderItem__r.size() > 0 )
        {
        	if((ord.OrderItem__r[0].InstalledBase__r.IdExt__c == null ||
        	ord.OrderItem__r[0].InstalledBase__r.IdExt__c == ''  ) &&
        	(ord.OrderItem__r[0].InstalledBase__r.ArticleEAN__c == null ||
        	ord.OrderItem__r[0].InstalledBase__r.ArticleEAN__c == '' ) && 
        	(ord.OrderItem__r[0].InstalledBase__r.SerialNo__c  == null ||
        	ord.OrderItem__r[0].InstalledBase__r.SerialNo__c  == '' ) 
        	)
	        {
	 			result = false;
	        	
	        }
        }
        if(ord.OrderItem__r.size() == 0 )  
        {
        	result = false;
        }     
               
        return result;
    }

    public Boolean getOrderEquipmentUpdateRetryActive() 
    {
        // Order eq update create retry can be called if
        // 1. the order exists in SF
        // 2. the order has been created in SAP ord.ERPStatusOrderCreate__c == 'ok'
        // 3. there are no pending call to SAP
        // 4. there was a previous error while transferring the order eq update to SAP
        
        return isOrderAvailable() && ord.ERPStatusOrderCreate__c == 'ok' && !transactionPending() &&
               ord.ERPStatusEquipmentUpdate__c == 'error';
    }
    
    // ------------------------------------------------------------------------

    public  Boolean getOrderMaterialReservationActive() 
    {
        // Order material reservation can be called if
        // 1. the order exists in SF
        // 2. the order was created in SAP - ERPStatusOrderCreate__c = 'ok'
        // 3. there are no pending calls to SAP
        // 4. there was no or a successful material reservation 
        // 5. order close has not yet been called
        // note: material reservations can be called multiple times

        // ERPStatusMaterialReservation__c:
        //    none       materal reservation create was not yet called
        //    pending    materal reservation create is currently being transferred to SAP
        //    ok         materal reservation create has been successfully transferred to SAP
        //    error      an error occured in the callout or the respose (retry is required)   
        
        return isOrderAvailable() && ord.ERPStatusOrderCreate__c == 'ok' && !transactionPending() &&
                (ord.ERPStatusMaterialReservation__c == 'none' || ord.ERPStatusMaterialReservation__c == 'ok') &&
                ord.ERPStatusOrderClose__c == 'none';
    }

    public boolean getOrderMaterialReservationRetryActive() 
    {
        // Order material movements (consumptions) can be created if
        // 1. the order exists in SF
        // 2. the order was created in sap ERPStatusOrderCreate__c == ok
        // 3. the order eq was updated (optionally) ERPStatusEquipmentUpdate__c == none or ok 
        // 4. the material movement (consumptions) were transferred with errors
        // 5. order close has not yet been called
        // note: material reservations can be called multiple times
        
        return isOrderAvailable() && ord.ERPStatusOrderCreate__c == 'ok' && !transactionPending() &&
                ord.ERPStatusMaterialReservation__c == 'error' && 
                ord.ERPStatusOrderClose__c == 'none';
    }


    // ------------------------------------------------------------------------

    public  Boolean getOrderMaterialConsumptionActive() 
    {
        // Order material movements (consumptions) can be called if
        // 1. the order exists in SF
        // 2. the order was created in SAP - ERPStatusOrderCreate__c = 'ok'
        // 3. there is no pending call to SAP - ERPStatusEquipmentUpdate__c == none or ok
        // 4. there was order related material movement 
        // note: material reservations can be called multiple times
        //
        // ERPStatusMaterialMovement__c:
        //    none       materal movement create was not yet called
        //    pending    materal movement  create is currently being transferred to SAP
        //    ok         materal movement  create has been successfully transferred to SAP
        //    error      an error occured in the callout or the respose (retry is required)   
        
        return isOrderAvailable() && ord.ERPStatusOrderCreate__c == 'ok' && !transactionPending() &&
                (ord.ERPStatusMaterialMovement__c == 'none' || ord.ERPStatusMaterialMovement__c == 'ok');
                // PMS 37529: Retry bei den Materialreservierungen und Bewegungen wieder nutzbar machen
                //&&
                //ord.ERPStatusOrderClose__c == 'none';

    }

    public boolean getOrderMaterialConsumptionRetryActive() 
    {
        // Order material movements (consumptions) can be created if
        // 1. the order exists in SF
        // 2. the order was created in sap ERPStatusOrderCreate__c == ok
        // 3. the order eq was updated (optionally) ERPStatusEquipmentUpdate__c == none or ok 
        // 4. the material movement (consumptions) were transferred with errors
        
        return isOrderAvailable() && ord.ERPStatusOrderCreate__c == 'ok' && !transactionPending() &&
                ord.ERPStatusMaterialMovement__c == 'error';
                // PMS 37529: Retry bei den Materialreservierungen und Bewegungen wieder nutzbar machen
                //&&
                //ord.ERPStatusOrderClose__c == 'none';
    }


    // ------------------------------------------------------------------------

    public  Boolean getOrderCloseActive() 
    {
        // Order can be closed if
        // 1. the order exists in SF
        // 2. the order was created in SAP - ERPStatusOrderCreate__c = 'ok'
        // 3. there are no pending calls to SAP
        // 4. 
        // 5. order close has not yet been called
        //
        // ERPStatusOrderClose__c: 
        //    none       order close is yet to be transferred to SAP
        //    pending    order close is currently being transferred to SAP    
        //    ok         order close was successfully transfert to sAP    
        //    error      error during order close
        
        return isOrderAvailable() && ord.ERPStatusOrderCreate__c == 'ok' && !transactionPending() && !getIsInClearing() &&
               (ord.ERPStatusEquipmentUpdate__c == 'none' || ord.ERPStatusEquipmentUpdate__c == 'ok') && 
               (ord.ERPStatusMaterialReservation__c == 'none' || ord.ERPStatusMaterialReservation__c == 'ok') &&
               (ord.ERPStatusMaterialMovement__c == 'none' || ord.ERPStatusMaterialMovement__c == 'ok') && 
                ord.ERPStatusOrderClose__c == 'none';
    }

    public boolean getOrderCloseRetryActive() 
    {
        // Order close can be created if
        // 1. the order exists in SF
        // 2. the order was created in sap ERPStatusOrderCreate__c == ok
        // 3. the order eq was updated (optionally) ERPStatusEquipmentUpdate__c == none or ok 
        // 4. the material movement (consumptions) have been transferred to SAP
        // 5. last 
        
        return isOrderAvailable() && ord.ERPStatusOrderCreate__c == 'ok' && !transactionPending() &&
                ord.ERPStatusOrderClose__c == 'error';
    }



    public  Boolean getOrderCancelActive() 
    {
        // Order can be cancelled if
        // 1. the order exists in SF
        // 2. the order was created in SAP - ERPStatusOrderCreate__c = 'ok'
        // 3. there is no pending or booked material movements 
        // 4. order is not closed
        
        return isOrderAvailable() && ord.ERPStatusOrderCreate__c == 'ok' && !transactionPending() && 
               (ord.ERPStatusEquipmentUpdate__c == 'none'  && ord.ERPStatusEquipmentUpdate__c == 'ok' ) &&
               (ord.ERPStatusMaterialReservation__c == 'none' ) &&
               (ord.ERPStatusMaterialMovement__c == 'none' ) && 
               (ord.ERPStatusOrderClose__c == 'none' || ord.ERPStatusOrderClose__c == 'error');
    }



    // ------------------------------------------------------------------------

    public  Boolean getOrderExtOpAddActive() 
    {
        // Add external operation (related list of order)
        // 1. the order exists in SF
        // 2. the order was created in sap ERPStatusOrderCreate__c == ok
        // 3. no other transaction is currently pending
        // 4. there must exist an external operation that has not yet been transferred to SAP
        // 5. the order close has not been called yet
        
        return isOrderAvailable() && ord.ERPStatusOrderCreate__c == 'ok' && !transactionPending() &&
               (ord.ERPStatusExternalAssignmentAdd__c == 'none' || ord.ERPStatusExternalAssignmentAdd__c == 'ok') &&
                isExtOpAdd2Transfer() &&
                ord.ERPStatusOrderClose__c == 'none';
    }

    public boolean getOrderExtOpAddRetryActive() 
    {
        return isOrderAvailable() && ord.ERPStatusOrderCreate__c == 'ok' && !transactionPending() &&
               isExtOpAdd2Transfer() && 
               ord.ERPStatusExternalAssignmentAdd__c == 'error' &&
               ord.ERPStatusOrderClose__c == 'none';
    }


    // ------------------------------------------------------------------------

    public  Boolean getOrderExtOpRemActive() 
    {
        // Add external operation (related list of order)
        // 1. the order exists in SF
        // 2. the order was created in sap ERPStatusOrderCreate__c == ok
        // 3. no other transaction is currently pending
        // 4. there must exist an external operation that has not yet been transferred to SAP
        // 5. the order close has not been called yet
        
        return isOrderAvailable() && ord.ERPStatusOrderCreate__c == 'ok' && !transactionPending() &&
               isExtOpRem2Transfer() &&
               ord.ERPStatusExternalAssignmentAdd__c == 'ok' &&
               ord.ERPStatusOrderClose__c == 'none';
    }

    public boolean getOrderExtOpRemRetryActive() 
    {
        return isOrderAvailable() && ord.ERPStatusOrderCreate__c == 'ok' && !transactionPending() &&
               isExtOpRem2Transfer() &&
               ord.ERPStatusExternalAssignmentRem__c == 'error' &&
               ord.ERPStatusOrderClose__c == 'none';
               return true;
    }



    // ------------------------------------------------------------------------

    public  Boolean getArchiveDocumentActive() 
    {
        // Archive document can be created if
        // 1. the order exists in SF
        // 2. the order was created in SAP - ERPStatusOrderCreate__c = 'ok'
        // 3. there are no pending calls to SAP
        // 4. 
        //
        // ERPStatusArchiveDocumentInsert__c: 
        //    none       document has not yet been transferred to SAP
        //    pending    document is currently being transferred to SAP    
        //    ok         document was successfully transfert to sAP    
        //    error      error during document transfer
        
        return isOrderAvailable() && ord.ERPStatusOrderCreate__c == 'ok' && !transactionPending() && !getIsInClearing() &&
                (ord.ERPStatusArchiveDocumentInsert__c == 'none' || 
                 ord.ERPStatusArchiveDocumentInsert__c == 'ok');  // retransmission is possible
    }

    public boolean getArchiveDocumentRetryActive() 
    {
        // Archive document can be created if
        // 1. the order exists in SF
        // 2. the order was created in sap ERPStatusOrderCreate__c == ok
        // 3. no transaction is pending
        // 4. the achtive transaction failed
        
        return isOrderAvailable() && ord.ERPStatusOrderCreate__c == 'ok' && !transactionPending() &&
                ord.ERPStatusArchiveDocumentInsert__c == 'error';
    }





    // ------------------------------------------------------------------------

    // true, if the order has been read and exists in SF
    public Boolean isOrderAvailable()
    {
        return ord.id != null;
    }


//###

    public Boolean isExtOpToBeTransfered()
    {
        return isExtOpAdd2Transfer() || isExtOpAdd2Transfer();
    }


    public Boolean isExtOpAdd2Transfer()
    {
        ordpendingExtOpAdd  = null;
        ordpendingExtOpAddId = null;
    
        // there is always only one record (max)
        for(SCOrderExternalAssignment__c ex : ord.OrderExternalAssignments__r)
        {
            if(ex.ERPStatusAdd__c == 'none' || ex.ERPStatusAdd__c == 'error')
            {
                ordpendingExtOpAdd  = ex.name;
                ordpendingExtOpAddId = ex.id;
                return true;
            }
        }
        return false;
    }


    public Boolean isExtOpRem2Transfer()
    {
        ordpendingExtOpRem  = null;
        ordpendingExtOpRemId = null;
    
        // there is always only one record (max)
        for(SCOrderExternalAssignment__c ex : ord.OrderExternalAssignments__r)
        {
            // there must be something that can be removed
            if(ex.ERPStatusAdd__c == 'ok' && (ex.ERPStatusRem__c == 'none' || ex.ERPStatusRem__c == 'error'))
            {
                ordpendingExtOpRem  = ex.name;
                ordpendingExtOpRemId = ex.id;
                return true;
            }
        }
        return false;
    }
    


    // ------------------------------------------------------------------------
    // Callouts - Order Interface
    // ------------------------------------------------------------------------

   /*
    * Calls the SAP interface to create an SAP order based on the current
    */
    public PageReference OnOrderCreate() 
    {
        // asynchronous mode is allways required
        CCWCOrderCreate.callout(oid, true, false);
        startPolling();
        currentTransaction = 'OrderCreate';
        return null;
    }
    
   /*
    * Retries the SAP interface to create an SAP order based on the current
    */
    public PageReference OnOrderCreateRetry() 
    {
        if(ord.IDocOrderCreate__c != null)
        {    
            // asynchronous mode is allways required
            CCWCReprocessingRequest.wscalloutOperationRetry(ord.Name, 'SAP_ORDER_CREATE', ord.IDocOrderCreate__c, 'SCOrder__c', false);

            // ensure that the cascading response mechanism can start even if the archiving failed (the archiving will restartet automatically
            resetArchiveStatus();
    
            startPolling();
            currentTransaction = 'OrderCreate';
        }
        else
        {
            // if an idoc number is missing - the standard order create has to be started
            OnOrderCreate(); 
        }
        return null;
    }
    
    

   /*
    * If the SAP system does not answer and the status is still 'pending' we can reset the transaction 
    */
    public PageReference OnOrderCreateReset() 
    {
        ord.ERPStatusOrderCreate__c = 'none';
        update ord;
    
        return null;
    }


   /* -------------------------------------------------------------------------
    * Calls the SAP interface to create update order related data (order header, order role, order item)
    */
    public PageReference OnOrderUpdateEquipment() 
    {
        CCWCOrderEquipmentUpdate.callout(oid, false, false);
        startPolling();
        currentTransaction = 'OrderUpdate';
        return null;
    }
    
    
   /* -------------------------------------------------------------------------
    * Retries the SAP interface to create update order related data (order header, order role, order item)
    */
    public PageReference OnOrderUpdateEquipmentRetry() 
    {
    
        if(ord.IDocOrderUpdate__c != null)
        {
            CCWCReprocessingRequest.wscalloutOperationRetry(ord.Name, 'SAP_ORDER_EQUIPMENT_UPDATE', ord.IDocOrderUpdate__c, 'SCOrder__c', false);
            // ensure that the cascading response mechanism can start even if the archiving failed (the archiving will restartet automatically
            resetArchiveStatus();
            startPolling();
            currentTransaction = 'OrderUpdate';
        }
        else
        {
            OnOrderUpdateEquipment();
        }
        return null;
    }

   /*
    * If the SAP system does not answer and the status is still 'pending' we can reset the transaction 
    */
    public PageReference OnOrderUpdateEquipmentReset() 
    {
        ord.ERPStatusEquipmentUpdate__c = 'none';
        update ord;
    
        return null;
    }



   /* -------------------------------------------------------------------------
    * Calls the SAP interface to create  order related material reservations
    */
    public PageReference OnOrderMaterialReservation() 
    {
        CCWCMaterialReservationCreate.callout(oid, false, false);
        // ensure that the cascading response mechanism can start even if the archiving failed (the archiving will restartet automatically
        resetArchiveStatus();
        startPolling();
        currentTransaction = 'MaterialReservation';
        return null;
    }

   /*
    * If the SAP system does not answer and the status is still 'pending' we can reset the transaction 
    */
    public PageReference OnMaterialReservationReset() 
    {
        ord.ERPStatusMaterialReservation__c = 'none';
        setMaterialReservationsToNone(ord.id);
        update ord;
    	startPolling();
    	
    	return null;
    }

	/*
	* Material Reservation can only be retried, if the ERPStatus__c  is none.
	* For the retry operation, select all material movements that belong to the 
	* Order and have status error. Set the status to 'none'.
	* PMS 37312/GMS: Cancel & Retry -Button korrigieren (CCWCMaterialMovementCreate)
	*/ 
   private static void setMaterialReservationsToNone(String ordId)
   {
   	        List<SCMaterialMovement__c> items = [select id from SCMaterialMovement__c where order__c = :ordId 
            and Status__c = '5402'  // MATSTAT_ORDERED
            and Type__c = '5202'    // MATREQUEST_EMPL
            and Article__c <> null and Article__r.ERPRelevant__c = true and Article__r.orderable__c = true
            and ERPStatus__c = 'error' and source__c = 'mobile'];
        if(items != null && items.size() > 0)
        {
            List<String> movementids = new List<String>();
            for(SCMaterialMovement__c item : items)
            {
                item.ERPStatus__c = 'none';
                //item.Status__c  = '5402';  // MATSTAT_ORDER
            }
            
            update items;
        }   
   	
   }

	/*
	* Material consumptions can only be retried, if the ERPStatus__c  is none.
	* For the retry operation, select all material movements that belong to the 
	* Order and have status error. Set the status to 'none'.
	* PMS 37312/GMS: Cancel & Retry -Button korrigieren (CCWCMaterialMovementCreate)
	*/ 
   private static void setMaterialMovementsToNone(String ordId)
   {
   	        List<SCMaterialMovement__c> items = [select id from SCMaterialMovement__c where order__c = :ordId 
            and Status__c = '5408'  // MATSTAT_BOOKED
            and Type__c = '5204'    // CONSUMPTION
            and Article__c <> null and Article__r.ERPRelevant__c = true and Article__r.orderable__c = true
            and ERPStatus__c = 'error' and source__c = 'mobile'];
        if(items != null && items.size() > 0)
        {
            List<String> movementids = new List<String>();
            for(SCMaterialMovement__c item : items)
            {
                item.ERPStatus__c = 'none';
                //item.Status__c  = '5402';  // MATSTAT_ORDER
            }
            
            update items;
        }   
   	
   }
   

   /* -------------------------------------------------------------------------
    * Calls the SAP interface to book order related material consumptions
    */
    public PageReference OnOrderMaterialConsumption() 
    {
        CCWCMaterialMovementCreate.callout(oid, false, false);
        // ensure that the cascading response mechanism can start even if the archiving failed (the archiving will restartet automatically
        resetArchiveStatus();

        startPolling();
        currentTransaction = 'Material(Consumptions)';
        return null;
    }

   /*
    * If the SAP system does not answer and the status is still 'pending' we can reset the transaction 
    */
    public PageReference OnMaterialReset() 
    {
        ord.ERPStatusMaterialMovement__c = 'none';
        setMaterialMovementsToNone(ord.id);
        update ord;
    	startPolling();
    
        return null;
    }


   /* -------------------------------------------------------------------------
    * Calls the SAP interface to close or cancel the order
    */
    public PageReference OnOrderClose() 
    {
        CCWCOrderClose.callout(oid, false, false);
        startPolling();
        currentTransaction = 'OrderClose';
        return null;
    }

   /* -------------------------------------------------------------------------
    * Retries the SAP interface to close or cancel the order
    */
    public PageReference OnOrderCloseRetry() 
    {
        if(ord.IDocOrderClose__c != null)
        {
            CCWCReprocessingRequest.wscalloutOperationRetry(ord.Name, 'SAP_ORDER_CLOSE', ord.IDocOrderClose__c, 'SCOrder__c', false);
            // ensure that the cascading response mechanism can start even if the archiving failed (the archiving will restartet automatically
            resetArchiveStatus();
            
            startPolling();
            currentTransaction = 'OrderClose';
        }
        else
        {
            OnOrderClose();
            // ensure that the cascading response mechanism can start even if the archiving failed (the archiving will restartet automatically
            resetArchiveStatus();
        }
        return null;
    }

   /*
    * If the SAP system does not answer and the status is still 'pending' we can reset the transaction 
    */
    public PageReference OnOrderCloseReset() 
    {
        ord.ERPStatusOrderClose__c = 'none';
        update ord;
    
        return null;
    }


   /* -------------------------------------------------------------------------
    * Calls the SAP interface to add an external operation
    */
    public PageReference OnOrderExtOpAdd() 
    {
        if(ordpendingExtOpAddId != null)
        {
            CCWCOrderExternalOperationAdd.callout(ordpendingExtOpAddId, false, false);
            startPolling();
            currentTransaction = 'ExtOpAdd';
        }
        return null;
    }
    
   /* -------------------------------------------------------------------------
    * Retries the SAP interface to add an external operation
    */
    public PageReference OnOrderExtOpAddRetry() 
    {
        if(ordpendingExtOpAddId != null)
        {
            //CCWCOrderExternalOperationAdd.callout(ordpendingExtOpAddId, false, false);
            CCWCReprocessingRequest.wscalloutOperationRetry(ord.Name, 'SAP_ORDER_EXTOP_ADD', ord.IDocOrderExOpAdd__c, 'SCOrderAssigment__c', false);
            startPolling();
            currentTransaction = 'ExtOpAdd';
        }
        return null;
    }
    
    

   /*
    * If the SAP system does not answer and the status is still 'pending' we can reset the transaction 
    */
    public PageReference OnOrderExtOpAddReset() 
    {
        ord.ERPStatusExternalAssignmentAdd__c = 'none';
        update ord;
    
        return null;
    }


   /* -------------------------------------------------------------------------
    * Calls the SAP interface to remove an external operation
    */
    public PageReference OnOrderExtOpRem() 
    {
        if(ordpendingExtOpRemId != null)
        {
            CCWCOrderExternalOperationRem.callout(ordpendingExtOpRemId, false, false);
            startPolling();
            currentTransaction = 'ExtOpRem';
        }
        return null;
    }
    
   /* -------------------------------------------------------------------------
    * Retries the SAP interface to remove an external operation
    */
    public PageReference OnOrderExtOpRemRetry() 
    {
        if(ordpendingExtOpRemId != null)
        {
            CCWCReprocessingRequest.wscalloutOperationRetry(ord.Name, 'SAP_ORDER_EXTOP_REM', ord.IDocOrderExOpRem__c, 'SCOrderAssigment__c', false);
            startPolling();
            currentTransaction = 'ExtOpRem';
        }
        return null;
    }


   /*
    * If the SAP system does not answer and the status is still 'pending' we can reset the transaction 
    */
    public PageReference OnOrderExtOpRemReset() 
    {
        ord.ERPStatusExternalAssignmentRem__c = 'none';
        update ord;
    
        return null;
    }



   /* -------------------------------------------------------------------------
    * Calls the SAP interface to archive documents (attachments)
    */
    public PageReference OnArchiveDocument() 
    {
        CCWCArchiveDocumentInsert.calloutOid(oid, true, false);
        startPolling();
        currentTransaction = 'ArchiveDocument';
        return null;
    }

   /* -------------------------------------------------------------------------
    * Retries the SAP interface to archive the documents
    */
    public PageReference OnArchiveDocumentRetry() 
    {

		OnArchiveDocument();
		
        return null;
    }

   /*
    * If the SAP system does not answer and the status is still 'pending' we can reset the transaction 
    */
    public PageReference OnArchiveDocumentReset() 
    {
        ord.ERPStatusArchiveDocumentInsert__c = 'none';
        update ord;
    
        return null;
    }






   /* -------------------------------------------------------------------------
    * Calls the SAP interface to close or cancel and order
    */

    public PageReference OnOrderCancel() 
    {
        CCWCOrderClose.cancel(oid, false, false);
        startPolling();
        currentTransaction = 'OrderCancel';
        return null;
    }
    
    
    public void resetArchiveStatus()
    {
        if(ord.ERPStatusArchiveDocumentInsert__c == 'error')
        {
           ord.ERPStatusArchiveDocumentInsert__c = 'none'; 
           
           SCOrder__c tmp = new SCOrder__c(id = ord.id);
           tmp.ERPStatusArchiveDocumentInsert__c = 'none'; 
           update tmp;
        }
    }
    


    public Boolean getErrorStatus()
    {
        return(ord.ERPStatusOrderCreate__c == 'error' ||
           ord.ERPStatusEquipmentUpdate__c == 'error' ||
           ord.ERPStatusMaterialReservation__c == 'error' || 
           ord.ERPStatusMaterialMovement__c == 'error' || 
           ord.ERPStatusExternalAssignmentAdd__c == 'error' ||
           ord.ERPStatusExternalAssignmentRem__c == 'error' ||
           ord.ERPStatusOrderClose__c == 'error');
    }

    public String getFailedTransactions() 
    {
        String res = ' ';
        if(ord.ERPStatusOrderCreate__c == 'error')
        {
            res += 'OrderCreate ';
        }
        if(ord.ERPStatusEquipmentUpdate__c == 'error')
        {
            res += 'OrderUpdate ';
        }
        if(ord.ERPStatusMaterialReservation__c == 'error')
        {
            res += 'Material(Reservation) ';
        }
        if(ord.ERPStatusMaterialMovement__c == 'error')
        {
            res += 'Material(Consumptions) ';
        }
        if(ord.ERPStatusExternalAssignmentAdd__c == 'error')
        {
            res += 'ExtOpAdd ';
        }
        if(ord.ERPStatusExternalAssignmentAdd__c == 'error')
        {
            res += 'ExtOpRemove ';
        }
        if(ord.ERPStatusOrderClose__c == 'error')
        {
            res += 'OrderClose ';
        }
        return res;
    }

    public Boolean transactionPending()
    {
        return(ord.ERPStatusOrderCreate__c == 'pending' ||
           ord.ERPStatusEquipmentUpdate__c == 'pending' ||
           ord.ERPStatusMaterialReservation__c == 'pending' || 
           ord.ERPStatusMaterialMovement__c == 'pending' || 
           ord.ERPStatusOrderClose__c == 'pending' ||
           ord.ERPStatusExternalAssignmentAdd__c == 'pending' ||
           ord.ERPStatusExternalAssignmentRem__c == 'pending'); 
    
    }

    public void startPolling()
    {
        pollStatus = true;
        pollCounter = 0;
    }

    public PageReference OnPollStatus() 
    {
        OnReadOrder(); 
        if(autopoll && pollcountermin < 0)
        {
            pollcountermin = 3;
            autopoll = false;
            pollStatus = true;
        }
        // enalbe automatic polling during order creation time (the apex job needs some time to start)
        if(pollCounter < pollcountermin)
        {
           pollCounter++; 
        }
        else
        {
            pollcountermin = 0;
            
            // set the poll couter depending on the penting status
            pollStatus = transactionPending();
            if(pollStatus)
            {
                pollCounter++;
            }
            else
            {
                pollCounter = 0;
            }
        }            
        return null;
    }

    public Boolean getPollingTimeoutReached() 
    {
        // enable to cancel pending callouts if we have reached response timeout 
        return pollStatus && pollCounter >= 3;
    }


    public Boolean getWaitingForResult() 
    {
        return // ord.ERPStatusOrderCreate__c == 'none' ||
               ord.ERPStatusOrderCreate__c == 'pending' ||
               ord.ERPStatusEquipmentUpdate__c == 'pending' ||
               ord.ERPStatusMaterialReservation__c == 'pending' || 
               ord.ERPStatusMaterialMovement__c == 'pending' || 
               ord.ERPStatusOrderClose__c == 'pending';
    }

    // ------------------------------------------------------------------------
    // Helper - Order Interface
    // ------------------------------------------------------------------------

    public PageReference OnReadOrder()
    {
        if(oid != null)
        {
            ord = [select id, name, type__c, status__c, invoicingstatus__c, 
                   ERPStatusOrderCreate__c, ERPStatusEquipmentUpdate__c, ERPStatusMaterialReservation__c,
                   ERPStatusMaterialMovement__c, ERPStatusOrderClose__c, ERPOrderNo__c, ERPResultDate__c,
                   ERPStatusExternalAssignmentAdd__c, ERPStatusExternalAssignmentRem__c,
                   ERPStatusArchiveDocumentInsert__c, 
                   IDocOrderCreate__c, IDocOrderUpdate__c, IDocOrderClose__c, IDocOrderExOpAdd__c, IDocOrderExOpRem__c,                   
                   (select id, name, ERPStatusAdd__c, ERPStatusRem__c from OrderExternalAssignments__r where ERPStatusAdd__c <> 'ok' or ERPStatusRem__c <> 'ok' order by name),
                   (select id, name, status__c, lastmodifieddate, ResultInfo__c, Resource__r.name, Type__c from Interface_Clearing__r order by name desc)
                   //PMS 37380: EQ-Update unterdrücken, wenn kein EQ da (Aufstellung mit Dummy-EQ im Auftrag)
                   ,(select id, name, InstalledBase__r.IdExt__c, InstalledBase__r.ArticleEAN__c, InstalledBase__r.SerialNo__c from OrderItem__r )
                    from SCOrder__c where id = :oid];
        }
        return null;
    }

    public Boolean getIsInClearing()
    {
        if(ord != null && ord.Interface_Clearing__r != null)
        {
            for(SCInterfaceClearing__c ic : ord.Interface_Clearing__r)
            {
                if(ic.Status__c == 'check' || ic.Status__c == 'pending')
                {
                    return true;
                }
            }
        }
        return false;        
    }

    
    
}
