/*
 * @(#)SCStockItemCopyExtension.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$ 
 */
public with sharing class SCStockItemCopyExtension
{
    private SCStock__c stock;
    
    public List<SelectOption> sourceList;
    public Id sourceStock { get; set; }
    public Boolean copyOk { get; private set; }
    public Boolean copyMinQty { get; set; }
    public Boolean copyMaxQty { get; set; }
    public Boolean copyReplQty { get; set; }
    public Boolean deleteItems { get; set; }


    /**
     * Gets a list of possible source stocks.
     */
    public List<SelectOption> getSourceList()
    {
        if (null == sourceList)
        {
            String key;
            String label;
            SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
            sourceList = new List<SelectOption>();
            
            // fill thelist with the infos from the assignment
            for (SCResourceAssignment__c assignments: 
                    [Select Id, Name, Stock__c, Stock__r.Plant__r.Name, Stock__r.Name, 
                            Resource__r.Alias_txt__c, Resource__r.Name, Resource__r.FirstName_txt__c, Resource__r.LastName_txt__c
                       from SCResourceAssignment__c 
                      where Stock__c != :stock.Id 
                        and Stock__r.Plant__r.Name = :appSettings.DEFAULT_PLANT__c 
                        and ValidFrom__c <= :Date.today() 
                        and ValidTo__c >= :Date.today()])
            {
                // if the assigment has a stock, add them to the receiver map
                if (null != assignments.Stock__c)
                {
                    key = assignments.Stock__r.Plant__r.Name + '/' + 
                          assignments.Stock__r.Name;
                    label = assignments.Resource__r.Alias_txt__c + ' ' + 
                            assignments.Resource__r.FirstName_txt__c + ' ' + 
                            assignments.Resource__r.LastName_txt__c + 
                            ' (' + key + ')';
                    sourceList.add(new SelectOption(assignments.Stock__c, label));
                } // if (null != assignments.Stock__c)
            } // for (SCResourceAssignment__c assignment: [Select Id, ... from SCResourceAssignment__c where Stock__c = ...])
        } // if (null == receiverList)
        return sourceList;
    } // getSourceList

    
    /**
     * SCMaterialTransferExtension
     */
    public SCStockItemCopyExtension(ApexPages.StandardController controller)
    {
        this.stock = (SCStock__c)controller.getRecord();
        copyOk = false;
        copyMinQty = false;
        copyMaxQty = false;
        copyReplQty = false;
        deleteItems = false;
    } // SCStockItemCopyExtension

    /**
     * Methode is called, when the user clicks [OK].
     * Checks if stock items should be deleted, if yes the items and mat moves will be deleted.
     * If no, a map of the current stock items is build up.
     * After this the copy is started.
     */
    public PageReference onCopyItems()
    {
        Map<String, SCStockItem__c> mapItems = new Map<String, SCStockItem__c>();
        List<SCStockItem__c> curItems = [Select Id, Article__c, Qty__c, MinQty__c, MaxQty__c, ReplenishmentQty__c, ValuationType__c 
                                           from SCStockItem__c 
                                          where Stock__c = :stock.Id];

        if (deleteItems)
        {
            List<SCMaterialMovement__c> matMoves = [Select Id from SCMaterialMovement__c 
                                                     where Stock__c = :stock.Id];
                                                     
            delete matMoves;
            delete curItems;
        }
        else
        {
            for (SCStockItem__c item :curItems)
            {
                mapItems.put(item.Article__c + '-' + (item.ValuationType__c != null ? item.ValuationType__c : ''), item);
            }
        }
        
        Boolean upsertItems = false;
        SCStockItem__c item;
        // read all stock items from the source stock
        for (SCStockItem__c srcItem :[Select Id, Article__c, Qty__c, MinQty__c, MaxQty__c, ReplenishmentQty__c, ValuationType__c
                                        from SCStockItem__c 
                                       where Stock__c = :sourceStock])
        {
            upsertItems = true;
            
            // look for the corresponding entry inthe own stock
            item = mapItems.get(srcItem.Article__c + '-' + (srcItem.ValuationType__c != null ? srcItem.ValuationType__c : ''));
            // if there is now stock item with the article in the own stock ...
            if (null == item)
            {
                // create a copy of the stock item
                item = srcItem.clone(false, true);
                // and set the stock reference
                item.Stock__c = stock.Id;
                // set the quantity to 0
                item.Qty__c = 0;
                // reset the quantities
                item.MinQty__c = null;
                item.MaxQty__c = null;
                item.ReplenishmentQty__c = null;
                
                // add it to the map
                mapItems.put(srcItem.Article__c, item);
            } // if (null == item)
            
            if (copyMinQty)
            {
                item.MinQty__c = srcItem.MinQty__c;
            }
            if (copyMaxQty)
            {
                item.MaxQty__c = srcItem.MaxQty__c;
            }
            if (copyReplQty)
            {
                item.ReplenishmentQty__c = srcItem.ReplenishmentQty__c;
            }
        } // for (SCStockItem__c srcItem :[Select Id, Article__c, Qty__c, MinQty__c, MaxQty__c, ReplenishmentQty__c ...
        
        if (upsertItems)
        {
            upsert mapItems.values();
        }
        copyOk = true;
        return null;
    } // onCopyItems
} // SCStockItemCopyExtension
