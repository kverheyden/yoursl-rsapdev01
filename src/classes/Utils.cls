/**********************************************************************
Name:  Utils()
======================================================
Purpose:         
Contains organization indipendent methods which can potentially used in other classes.                                                     
======================================================
History                                                            
-------                                                            
Date    AUTHOR  DETAIL
05/29/2014 Jan Mensfeld INITIAL DEVELOPMENT          
***********************************************************************/

public with sharing class Utils{
	
	
	public static String getSObjectTypeName(SObject so) {
		return so.getSObjectType().getDescribe().getName();
	}
	
	public static List<Schema.PicklistEntry> getPicklistEntries(SObjectField field){                
		if (field.getDescribe().getType() == Schema.DisplayType.Picklist)
			return field.getDescribe().getPicklistValues();
		return null;
	}
	
	public static Map<String, String> getPicklistEntryMap(SObjectField field) {
		List<Schema.PicklistEntry> entries = getPicklistEntries(field);
		if (entries == null) 
			return null;
		Map<String, String> picklistValues = new Map<String, String>();
		for (Schema.PicklistEntry pe : entries) {
			picklistValues.put(pe.Value, pe.Label);
		}
		return picklistValues;
	}
	
	public static List<Schema.FieldSetMember> getFieldSetMember(SObjectType sObjectType, String fieldSetName) {
		Schema.DescribeSObjectResult describeSObjectResult = sObjectType.getDescribe();
	    Schema.FieldSet objFieldSet = describeSObjectResult.FieldSets.getMap().get(fieldSetName);
		return objFieldSet == null ? null : objFieldSet.getFields();
	}
	
	public static List<String> getFieldSetMemberStringList(SObjectType sObjectType, String fieldSetName) {
		List<Schema.FieldSetMember> fieldSetMemberList = getFieldSetMember(sObjectType, fieldSetName);
		if (fieldSetMemberList == null)
			return null;
		
		List<String> fieldList = new List<String>();	
		for (Schema.FieldSetMember f : fieldSetMemberList) 
			fieldList.add(f.getFieldPath());	

		return fieldList;
	}
	
	public static Map<String, Schema.DisplayType> getFieldSetMemberTypeMap(SObjectType sObjectType, String fieldSetName) {
		List<Schema.FieldSetMember> fieldSetMemberList = getFieldSetMember(sObjectType, fieldSetName);
		if (fieldSetMemberList == null)
			return null;
		
		Map<String, Schema.DisplayType> fieldSetMemberTypeMap = new Map<String, Schema.DisplayType>();
		for(Schema.FieldSetMember f : fieldSetMemberList) 
			fieldSetMemberTypeMap.put(f.getFieldPath(), f.Type);
		
		return fieldSetMemberTypeMap;
	}
	
	public static String getIdPrefix(SObjectType soType) {
		Schema.DescribeSObjectResult r = soType.getDescribe();
		return r.getKeyPrefix();
	}
	
	public static String getSFDCInstance(){
		String instanceUrl = URL.getSalesforceBaseUrl().getHost();
		List<String> instanceUrlParts = instanceUrl.split('\\.');
		return instanceUrlParts.get(0) + '.' + instanceUrlParts.get(1);
	}
	
	public static String getSFDCVisualforceURL(String pageName) {
		String instanceUrl = URL.getSalesforceBaseUrl().getHost();
		List<String> instanceUrlParts = instanceUrl.split('\\.');
		return 'https://' + instanceUrlParts.get(0) + '--c.' + instanceUrlParts.get(1) + '.visual.force.com/apex/' + pageName;
	}
	
	public static String getSFDCContentURL(Id fileId) {
		return 'https://' + getSFDCInstance() + '.content.force.com/servlet/servlet.ImageServer?' + fileId;
	}
	
	public static Datetime getCurrentDateTimeLocal() {
		String timeZone = [select timeZoneSidKey from User where id=:userinfo.getUserId()].timeZoneSidKey;
		String formatteddate = System.now().format('yyyy-MM-dd HH:mm:ss',timeZone);
		return Datetime.valueOfGmt(formatteddate);
	}
	
	
}
