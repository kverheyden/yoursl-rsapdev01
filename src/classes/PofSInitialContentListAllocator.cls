/**********************************************************************

Name: PofSController
 
======================================================

Purpose: 

This class is called by a trigger on the PofSDeviceModel__c Object and relates a initial content list to an selected device model

======================================================

History 

------- 

Date 			AUTHOR 				DETAIL

01/07/2013 		Bernd Werner	 	INITIAL DEVELOPMENT
29/08/2014		Sascha Weicht		Change of Mapping from CDE_Type_M to CDE_Type_Mspd

***********************************************************************/
public with sharing class PofSInitialContentListAllocator {

	// Map with all giveb IDs of PofSDeviceModel
	public Map<Id,String> modelContentListMap = new Map<Id,String>();
	// Set with all types of Types M
	public set<String> typeSet = new set<String>();
	// Map with Type M numbers and IDs
	public Map<String,Id> masterListMap = new Map<String,Id>();
	// List with all PofSDeviceModel__c that needs to become updated
	public List<PofSDeviceModel__c> updateList = new List<PofSDeviceModel__c>();
	// tmp

	// Constructor
	public PofSInitialContentListAllocator(){
		
	}

	public void AllocateInitialContentList(List<PofSDeviceModel__c> modelList){
		for(PofSDeviceModel__c tmpPofDevMod : modelList){
			if(	tmpPofDevMod.ProductModelCdeTypeMspd__c != null && 
				tmpPofDevMod.ProductModelCdeTypeMspd__c != '' &&
				!typeSet.contains(tmpPofDevMod.ProductModelCdeTypeMspd__c)
			){
					typeSet.add(tmpPofDevMod.ProductModelCdeTypeMspd__c);
			}
		}
		
		// Collect all required Content Lists
		for(InitialContentList__c tmpContList : [SELECT ID, ValidCdeTypeMspd__c FROM InitialContentList__c WHERE ValidCdeTypeMspd__c IN : typeSet]){
			masterListMap.put(tmpContList.ValidCdeTypeMspd__c,tmpContList.Id);
		}
		
		// finally allocate values
		for (PofSDeviceModel__c tmpPofDevMod : modelList){
			if(	tmpPofDevMod.ProductModelCdeTypeMspd__c != null && 
				tmpPofDevMod.ProductModelCdeTypeMspd__c != '' &&
				masterListMap.containsKey(tmpPofDevMod.ProductModelCdeTypeMspd__c)
			){
				tmpPofDevMod.InitialContentList__c = masterListMap.get(tmpPofDevMod.ProductModelCdeTypeMspd__c);
			}
		}
	}


}
