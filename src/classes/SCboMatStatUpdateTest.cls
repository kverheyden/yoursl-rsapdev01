/*
 * @(#)SCboOrderTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @version $Revision$, $Date$
 */
@isTest
private class SCboMatStatUpdateTest
{
    public static SCfwDomain domMatMove = new SCfwDomain('DOM_MATERIALMOVEMENT_TYPE');
    public static SCboMaterialMovement boMatMove = new SCboMaterialMovement(domMatMove);
    
    static testMethod void syncOrder() 
    {
        SCHelperTestClass.createOrderTestSet3(true);
        SCboMatStatUpdate matUpdate = new SCboMatStatUpdate();
        Boolean ret = matUpdate.syncOrder(SCHelperTestClass.order.Id);    
        System.assertEquals(true, ret);
    }
    
    static testMethod void syncOrderLines()
    {
        SCHelperTestClass.createOrderTestSet3(true);
        SCHelperTestClass.createTestArticles(true);
        
        SCboOrder boOrder = new SCboOrder(SCHelperTestClass.order);
        SCboOrderItem boOrderItem = new SCboOrderItem(SCHelperTestClass.orderItem);

        Map<Id, SCOrderLine__c> mapOrderLines = new Map<Id, SCOrderLine__c>([Select Id from SCOrderLine__c 
                                                                              where Order__c = :SCHelperTestClass.order.Id 
                                                                                and OrderItem__c = :SCHelperTestClass.orderItem.Id]);
        SCboMatStatUpdate matStatUpdate = new SCboMatStatUpdate();
        List<Id> orderLineIdList = new List<Id>();
        orderLineIdList.addAll(mapOrderLines.keySet());

        SCboMatStatUpdate matUpdate = new SCboMatStatUpdate();
        Boolean ret = matUpdate.syncOrderLines(orderLineIdList);    
        System.assertEquals(false, ret);
    }
    
    static testMethod void testMatMovementStatUpdate() 
    {
        

        // first create all necessary test data
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);
        SCHelperTestClass.createTestCalendar(true);
        SCHelperTestClass.createTestBusinessUnit(SCHelperTestClass.stocks.get(2).Id, 
                                               SCHelperTestClass.Calendar.Id, true);
        List<SCStock__c> assignStocks = new List<SCStock__c>();
        assignStocks.add(SCHelperTestClass.stocks.get(0));
        assignStocks.add(SCHelperTestClass.stocks.get(1));
        SCHelperTestClass.createOrderTestSet3(true);
        
        // delete all order lines for the order, we create our own
        List<SCOrderline__c> orderLines = [Select Id from SCOrderLine__c where Order__c = :SCHelperTestClass.order.Id];
        delete orderLines;
        
        SCHelperTestClass.createTestAssignments(SCHelperTestClass.resources, assignStocks, 
                                              SCHelperTestClass.businessUnit.Id, true);
        SCboOrderItem boOrderItem = new SCboOrderItem(SCHelperTestClass.orderItem);
        
        // Id orderId, Id orderItemId, Id articleId, Id assignmentId
        SCboOrderLine boOrderLine1 = new SCboOrderLine(SCHelperTestClass.order.Id,
                                                       SCHelperTestClass.orderItem.Id,
                                                       SCHelperTestClass.articles[1].Id,
                                                       SCHelperTestClass.assignmentMap.get('assignmentDispo1').Id);
        SCboArticle boArticle1 = new SCboArticle(SCHelperTestClass.articles[1].Id);
        boOrderLine1.orderLine.Article__r = boArticle1.article;
        
        // set the required fields manually
        boOrderLine1.orderLine.Type__c = SCfwConstants.DOMVAL_ORDERLINETYPE_INV;
        boOrderLine1.orderLine.MaterialStatus__c = SCfwConstants.DOMVAL_MATSTAT_ORDERABLE;
        boOrderLine1.orderLine.Qty__c = 1.0;
        boOrderLine1.orderLine.ListPrice__c = 10.00;
        boOrderLine1.orderLine.Order__c = SCHelperTestClass.order.Id;
        boOrderItem.boOrderLines.add(boOrderLine1);

        SCboOrderLine boOrderLine2 = new SCboOrderLine(SCHelperTestClass.order.Id,
                                                       SCHelperTestClass.orderItem.Id,
                                                       SCHelperTestClass.articles[2].Id,
                                                       SCHelperTestClass.assignmentMap.get('assignmentDispo1').Id);
        SCboArticle boArticle2 = new SCboArticle(SCHelperTestClass.articles[2].Id);
        boOrderLine2.orderLine.Article__r = boArticle2.article;
        
        // set the required field manually
        boOrderLine2.orderLine.Type__c = SCfwConstants.DOMVAL_ORDERLINETYPE_INV;
        boOrderLine2.orderLine.MaterialStatus__c = SCfwConstants.DOMVAL_MATSTAT_ORDERABLE;
        boOrderLine2.orderLine.Qty__c = 1.0;
        boOrderLine2.orderLine.ListPrice__c = 20.00;
        boOrderLine2.orderLine.Order__c = SCHelperTestClass.order.Id;
        boOrderItem.boOrderLines.add(boOrderLine2);
        boOrderItem.save(); 
        
        SCOrderLine__c orderLine1 = [Select Id from SCOrderLine__c 
                                      where Order__c = :SCHelperTestClass.order.Id 
                                        and OrderItem__c = :SCHelperTestClass.orderItem.Id 
                                        and Article__c = :SCHelperTestClass.articles[1].Id];
        SCOrderLine__c orderLine2 = [Select Id from SCOrderLine__c 
                                      where Order__c = :SCHelperTestClass.order.Id 
                                        and OrderItem__c = :SCHelperTestClass.orderItem.Id 
                                        and Article__c = :SCHelperTestClass.articles[2].Id];

        // create a material movement for an article
        boMatMove.createMatMove(SCHelperTestClass.stocks.get(0).Id, boArticle1.article.Id, 
                                1.0, SCfwConstants.DOMVAL_MATSTAT_ORDER, 
                                SCfwConstants.DOMVAL_MATMOVETYP_MATREQUEST_EMPL, 
                                null, null, SCHelperTestClass.order.Id, orderLine1.Id);
        boMatMove.createMatMove(SCHelperTestClass.stocks.get(0).Id, boArticle2.article.Id, 
                                1.0, SCfwConstants.DOMVAL_MATSTAT_ORDER, 
                                SCfwConstants.DOMVAL_MATMOVETYP_MATREQUEST_EMPL, 
                                null, null, SCHelperTestClass.order.Id, orderLine2.Id);
        
        Map<Id, SCOrderLine__c> mapOrderLines = new Map<Id, SCOrderLine__c>([Select Id from SCOrderLine__c 
                                                                              where Order__c = :SCHelperTestClass.order.Id 
                                                                                and OrderItem__c = :SCHelperTestClass.orderItem.Id]);
        SCboMatStatUpdate matStatUpdate = new SCboMatStatUpdate();
        List<Id> orderLineIdList = new List<Id>();
        orderLineIdList.addAll(mapOrderLines.keySet());
        if (matStatUpdate.syncOrderLines(orderLineIdList))
        {
            matStatUpdate.syncOrder(SCHelperTestClass.order.Id);
        } 

        // check the material status of the order lines and the order after creating the material movements
        orderLine1 = [Select Id, MaterialStatus__c from SCOrderLine__c 
                       where Order__c = :SCHelperTestClass.order.Id 
                         and OrderItem__c = :SCHelperTestClass.orderItem.Id 
                         and Article__c = :SCHelperTestClass.articles[1].Id];
        System.assertEquals(SCfwConstants.DOMVAL_MATSTAT_ORDER, orderLine1.MaterialStatus__c);
        orderLine2 = [Select Id, MaterialStatus__c from SCOrderLine__c 
                       where Order__c = :SCHelperTestClass.order.Id 
                         and OrderItem__c = :SCHelperTestClass.orderItem.Id 
                         and Article__c = :SCHelperTestClass.articles[2].Id];
        System.assertEquals(SCfwConstants.DOMVAL_MATSTAT_ORDER, orderLine2.MaterialStatus__c);

        SCOrder__c order = [Select Id, MaterialStatus__c from SCOrder__c 
                             where Id = :SCHelperTestClass.order.Id];
        System.assertEquals(SCfwConstants.DOMVAL_MATSTAT_ORDER, order.MaterialStatus__c);
        
        Test.startTest();

        // now update the material status in the material movements
        List<SCMaterialMovement__c> matMoves = [Select Id, Status__c, Article__c from SCMaterialMovement__c 
                                                 where Order__c = :SCHelperTestClass.order.Id];
        for (SCMaterialMovement__c matMove :matMoves)
        {
            if (matMove.Article__c == SCHelperTestClass.articles[1].Id)
            {
                matMove.Status__c = SCfwConstants.DOMVAL_MATSTAT_DELIVERED;
            }
            if (matMove.Article__c == SCHelperTestClass.articles[2].Id)
            {
                matMove.Status__c = SCfwConstants.DOMVAL_MATSTAT_ORDERED;
            }
        }
        update matMoves;
        
        // now check the material status of the order lines and the order again
        orderLine1 = [Select Id, MaterialStatus__c from SCOrderLine__c 
                       where Order__c = :SCHelperTestClass.order.Id 
                         and OrderItem__c = :SCHelperTestClass.orderItem.Id 
                         and Article__c = :SCHelperTestClass.articles[1].Id];
        System.assertEquals(SCfwConstants.DOMVAL_MATSTAT_DELIVERED, orderLine1.MaterialStatus__c);
        orderLine2 = [Select Id, MaterialStatus__c from SCOrderLine__c 
                       where Order__c = :SCHelperTestClass.order.Id 
                         and OrderItem__c = :SCHelperTestClass.orderItem.Id 
                         and Article__c = :SCHelperTestClass.articles[2].Id];
        System.assertEquals(SCfwConstants.DOMVAL_MATSTAT_ORDERED, orderLine2.MaterialStatus__c);

        order = [Select Id, MaterialStatus__c from SCOrder__c 
                  where Id = :SCHelperTestClass.order.Id];
        System.assertEquals(SCfwConstants.DOMVAL_MATSTAT_ORDERED, order.MaterialStatus__c);

        Test.stopTest();
    } // testMatMovementStatUpdate
}
