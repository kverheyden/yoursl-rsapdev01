/*
 * @(#)SCInventoryUploadController.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *  
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This class parses a user input text (Article|ValuationType|Qty) 
 * and updates Qty in associated inventory items.
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCInventoryUploadController
{

    public transient List<UploadedObject> unknownArticles {get; set;}
    public transient List<List<UploadedObject>> unknownArticlesMainList { get; set; }
    public Boolean skipUnknownArticles { get; set; }
    public String input { get; set; }
    public Boolean show { get; set; }
    private Id oid;
    public List<UploadedObject> uploads { get; set; }
    public List<List<UploadedObject>> uploadsMain { get; set; }
    public SCInventory__c currentInventory {get; set;}
    private SCboInventory boInventory = new SCboInventory();
    
    //Map<String, SCArticle__c> articlesFromInventoryItems = new Map<String, SCArticle__c>();
    Map<String, SCArticle__c> articlesFromDB = new Map<String, SCArticle__c>();
    
    public Boolean mustProcessAgain { get; set; }
    private String pickListValuesAsString;
    public Boolean showstats { get; set; }
    public Boolean finish { get; set; }
    public Boolean showValuationType { get; set; }
    
    public Integer newInventoryItems { get; set; }
    public Integer newStocktems { get; set; }
    public Integer updateItems { get; set; }
    
    public String errorMessage { get; set; }
    public Boolean showError { get; set; }
    
    public static SCApplicationSettings__c appSetting = SCApplicationSettings__c.getInstance();
    
    public Boolean hasInventoryItems = false;
    
    /**
     * Constructor
     */
    public SCInventoryUploadController()
    {   
        currentInventory = new SCInventory__c();
    
        mustProcessAgain = false;
        pickListValuesAsString = getPickListValues();
        showError = false;
        errorMessage = '';
        show = false;
        showstats = false;
        
        newInventoryItems = 0;
        newStocktems = 0;
        finish = false;
        showValuationType = true;
        
        skipUnknownArticles = true;
        
        if(ApexPages.currentPage().getParameters().containsKey('oid') && ApexPages.currentPage().getParameters().get('oid') != '')
        {
            // Defining the object id (inventory)
            oid = ApexPages.currentPage().getParameters().get('oid');
            
            // Reading current inventory
            //currentInventory = boInventory.readById(oid);
            // GMSSU 25.11.2013 PMS 36491/INC0100065: Inventur: "Aggregate query has too many rows for direct assignment, use FOR loop"
            // We need Limit 1 for the child relationship InventoryItem__r
            currentInventory = [Select Id, Name, Description__c, ERPResultDate__c, Stock__r.Plant__c, Stock__r.Plant__r.Name, 
                                       ERPStatus__c, FiscalYear__c, Full__c, InventoryDate__c, 
                                       PlannedCountDate__c, Status__c, Plant__c, Stock__r.name, Stock__r.ValuationType__c, Stock__c,
                                       (Select Id From InventoryItem__r Limit 1)
                                From SCInventory__c
                                Where Id = : oid];

            if(currentInventory.InventoryItem__r != null && !currentInventory.InventoryItem__r.isEmpty())
            {
                hasInventoryItems = true;
            }
            
            if(currentInventory.Stock__r.ValuationType__c == false)
            {
                showValuationType = false;
            }
             
            /*
            // Now creating a map of all articles from the inventory items 
            // to use for compare the entered values with exists values
            //if(!currentInventory.InventoryItem__r.isEmpty())
            if(!invItems.isEmpty())
            {
                //for(SCInventoryItem__c i : currentInventory.InventoryItem__r)
                for(SCInventoryItem__c i : invItems)
                {
                    //articlesFromInventoryItems.put(i.Article__r.Name, i.Article__r);
                    stockItemsFromInventoryItems.put(i.StockItem__r.Article__r.Name, i.StockItem__r);
                    
                    String key = i.Inventory__r.Stock__c + '-' + i.article__r.id + '-' + i.ValuationType__c;
                    if(currentInventory.Stock__r.ValuationType__c == false)
                    {
                        key = i.Inventory__r.Stock__c + '-' + i.article__r.id;
                    }
                    
                    inventoryItems.put(key, i);
                }
            }
            */
        }
        else
        {
            oid = null;
            showError = true;
            errorMessage = 'Es existiert keine Inventur-ID!';
            //errorMessage = 'There is no inventory ID';
        }
    }
    
    /**
     * Helper class to hold the entered data from the user
     */
    public Class UploadedObject
    {
        public SCArticle__c article { get; set; }
        public SCInventoryItem__c inventoryItem  { get; set; }
        
        public Boolean artNumOk { get; set; }          // valid article
        public Boolean valTypeOk { get; set; }         // valid valuation type
        public Boolean countedQtyOk { get; set; }      // valid quantity  
        public String badCountedQty { get; set; }      // the textual represantation of the invalid qty
        
        public UploadedObject()
        {
        }
        
        UploadedObject(UploadedObject o)
        {
            this.article = o.article;
            this.inventoryItem = o.inventoryItem;
            this.artNumOk = o.artNumOk;
            this.valTypeOk = o.valTypeOk;
            this.countedQtyOk = o.countedQtyOk;
            this.badCountedQty = o.badCountedQty;
        }
    }
    
    /**
     * Getting all picklist values from SCInventoryItem__c.ValuationType__c
     *
     * @return string with all picklist values as one string
     */
    private String getPickListValues()
    {
        Schema.DescribeFieldResult fieldResult = SCInventoryItem__c.ValuationType__c.getDescribe();
        List<Schema.PicklistEntry> pickListValues = fieldResult.getPicklistValues();
        
        return String.valueOf(pickListValues);
    }
    
    /**
     * true if the valuation type is active
     */
    public Boolean getValuationTypeEnabled()
    {
        return (currentInventory != null && currentInventory.Stock__r != null && currentInventory.Stock__r.ValuationType__c == true);
    }
    
    /**
     * Processing the text again in case there are still errors.
     * Need this because this method works with our class object and not with a plain text anymore.
     *
     * @return pageReference null if there are errors or redirect to inventorys page
     */
    public PageReference processAgain()
    {
        Boolean artNoIncorrect = false;
        Boolean valIncorrect = false;
        Boolean qtyNoIncorrect = false;
        
        unknownArticles = new List<UploadedObject>(); 
        unknownArticlesMainList = new List<List<UploadedObject>>();
        
        mustProcessAgain = false;
        
        if(!uploadsMain.isEmpty())
        {
            // Creating a set with all article names from the user input
            Set<String> userArticles = new Set<String>();
            articlesFromDB = new Map<String, SCArticle__c>();
            
            for(List<UploadedObject> up : uploadsMain)
            {
                for(UploadedObject u : up)
                {      
                    userArticles.add(u.article.name);
                }
            }
            
            System.debug('#### userArticles: ' + userArticles.size());
            
            // Now selecting articles that are similar to the names from the user input
            try
            {
                for(SCArticle__c a : [ Select Name From SCArticle__c Where Name IN :userArticles ])
                {
                    articlesFromDB.put(a.name, a);
                }
            }
            catch(Exception e)
            {
                System.debug('#### Error 1: ' + e.getMessage());
                ApexPages.addMessages(e);
            }
            
            Integer maxQuantity = (Integer) appSetting.INVENTORY_MAX_QUANTITY__c;
            maxQuantity = (maxQuantity != null && maxQuantity >= 100) ? maxQuantity : 100;
            
            
            for(List<UploadedObject> up : uploadsMain)
            {
                for(UploadedObject uo : up)
                {
                    // Article Number && Checking whether the article exists in the current inventory items
                    if(String.isNotBlank(uo.article.name) && articlesFromDB.containsKey(uo.article.name.deleteWhitespace()))
                    {                       
                        uo.artNumOk = true;
                    }
                    else
                    {
                        uo.artNumOk = false;                    
                        if(skipUnknownArticles)
                        {
                            // add to the list of unknown article numbers (to be shown on the confirmation page)
                            if(uo.countedQtyOk && uo.inventoryItem.CountedQty__c > 0)
                            {
                                if(unknownArticles.size() == 1000)
                                {
                                    unknownArticlesMainList.add(unknownArticles);
                                    unknownArticles = new List<UploadedObject>();
                                }
                                unknownArticles.add(uo);
                            }
                            
                            artNoIncorrect = false;
                        }
                        else
                        {    
                            // invalid articles exist
                            artNoIncorrect = true;
                        }
                    }
                    
                    // Valuation Type && Checking, whether the value is a valid picklist value getValuationTypeEnabled()
                    if(currentInventory.Stock__r.ValuationType__c == true)
                    {
                        if(String.isNotBlank(uo.inventoryItem.ValuationType__c) && pickListValuesAsString.contains(uo.inventoryItem.ValuationType__c))
                        {                   
                            uo.valTypeOk = true;
                            valIncorrect = false;
                        }
                        else
                        {
                            //GMSNA: 07.06.2012 enable blank valuation types
                            //uo.valTypeOk = false;
                            //valIncorrect = true;
                            uo.valTypeOk = true;
                        }
                    }
                    
                    // Counted Qty
                    if(uo.inventoryItem.CountedQty__c != null && uo.inventoryItem.CountedQty__c <= maxQuantity && uo.inventoryItem.CountedQty__c >= 0)
                    {
                        uo.countedQtyOk = true;
                        qtyNoIncorrect = false;
    
                    }
                    else
                    {
                        uo.countedQtyOk = false;
                        
                        qtyNoIncorrect = true;
                    }
                    
                    if(artNoIncorrect || valIncorrect || qtyNoIncorrect)
                    {
                        mustProcessAgain = true;
                    }
                }
            }
            
            unknownArticlesMainList.add(unknownArticles);

        }
      
        
        if(!mustProcessAgain && finish)
        {
            if(updateItems())
            {
                showstats = true;
            }
            else
            {
                System.debug('#### Update failed!');
                System.debug('#### mustProcessAgain my: ' + mustProcessAgain);
            }
        }
        
        if(mustProcessAgain)
        {
            finish = false;
        }
        
        
        
        return null;
    }
    
    /**
     * Updates inventory / stock items with following rules:
     *
     * Article exists    ValType exists    Qty ok    Inv/Stock Item exists    Action
     *       no                -              -                -              show error
     *       yes               no             -                -              show error

     *       yes               yes         <0 or >500          -              show warning
     *       yes               yes            ok               yes            update Qty counted on inventory items
     *       yes               yes            ok               no             create new Inv/Stock item
     *
     * @return false if updade fails, otherwise true
     */
    private Boolean updateItems()
    {
        updateItems = 0;
        newInventoryItems = 0;
        newStocktems = 0;
        
        List<SCInventoryItem__c> newInventoryItemList = new List<SCInventoryItem__c>();
        List<SCStockItem__c> newStockItemList = new List<SCStockItem__c>();
        
        Map<String, SCStockItem__c> mapStockItems = new Map<String, SCStockItem__c>();
        
        // Now creating a map of all articles from the inventory items 
        // to use for compare the entered values with exists values
        Map<String, SCStockItem__c> stockItemsFromInventoryItems = new Map<String, SCStockItem__c>();
        Map<String, SCInventoryItem__c> inventoryItems = new Map<String, SCInventoryItem__c>();
        List<SCInventoryItem__c> invItems = new List<SCInventoryItem__c>();
        
        List<SCStockItem__c> stockItemsForPartialInventory = new List<SCStockItem__c>();

        //if(currentInventory.Full__c)
        if(hasInventoryItems)
        {
            invItems = [Select Id, CurrentQty__c, CountedQty__c, Inventory__c, 
                               Article__r.Id, Article__r.Name, 
                               ValuationType__c, StockItem__r.Qty__c, StockItem__r.Article__r.Name, Inventory__r.Stock__c
                        From SCInventoryItem__c
                        Where Inventory__c = : oid
                        Order By Article__r.Name, ValuationType__c asc];
                        
            if(!invItems.isEmpty())
            {
                for(SCInventoryItem__c i : invItems)
                {
                    stockItemsFromInventoryItems.put(i.StockItem__r.Article__r.Name + '-' + i.ValuationType__c, i.StockItem__r);
                    
                    String key = i.Inventory__r.Stock__c + '-' + i.article__r.id + '-' + i.ValuationType__c;
                    if(currentInventory.Stock__r.ValuationType__c == false)
                    {
                        key = i.Inventory__r.Stock__c + '-' + i.article__r.id;
                    }
                    
                    inventoryItems.put(key, i);
                }
            }
        }
        else
        {
            stockItemsForPartialInventory = [Select Id, Article__r.Name, Article__c, ValuationType__c, Stock__c, Qty__c
                                             From SCStockItem__c
                                             Where Stock__c = : currentInventory.Stock__c];
                                             
            if(!stockItemsForPartialInventory.isEmpty())




            {
                for(SCStockItem__c i : stockItemsForPartialInventory)





                {
                    stockItemsFromInventoryItems.put(i.Article__r.Name + '-' + i.ValuationType__c, i);
                    
                    /*
                    String key = i.Stock__c + '-' + i.Article__r.id + '-' + i.ValuationType__c;
                    if(currentInventory.Stock__r.ValuationType__c == false)
                    {
                        key = i.Stock__c + '-' + i.article__r.id;
                    }
                    */
                }


            }
        }


        
        if(!mustProcessAgain && !uploadsMain.isEmpty())
        {
            List<SCInventoryItem__c> itemsToUpdate = new List<SCInventoryItem__c>();
            
            Savepoint sp = Database.setSavepoint();
            
            for(List<UploadedObject> up : uploadsMain)
            {
                for(UploadedObject u : up)
                {
                    // do not write invalid records with missing article references
                    if(!u.artNumOk)
                    {
                        continue;
                    }
                
                    String key;
                    if(currentInventory.Stock__r.ValuationType__c == false)
                    {
                        key = currentInventory.Stock__c + '-' + u.article.id;
                    }
                    else
                    {
                        key = currentInventory.Stock__c + '-' + u.article.id + '-' + u.inventoryItem.ValuationType__c;
                    }
                    
                    System.debug('#### key: ' + key);
                    System.debug('#### inventoryItems: ' + inventoryItems);
                    
                    if(hasInventoryItems && inventoryItems.containsKey(key))
                    {
                        System.debug('#### key ok');
                    
                        SCInventoryItem__c i = inventoryItems.get(key);
                        i.CountedQty__c = u.inventoryItem.CountedQty__c;
    
                        if(currentInventory.Stock__r.ValuationType__c == true)
                        {
                            i.ValuationType__c = u.inventoryItem.ValuationType__c;
                        }
                        updateItems++;
                        itemsToUpdate.add(i);
                    }
                    else
                    {
                        System.debug('#### key false');
                        
                        // We cannot create a new inventory item without stock item
                        Boolean canCreateNewInventoryItem = true;
                        
                        // Ctreate new inventory item
                        SCInventoryItem__c i = new SCInventoryItem__c();
                        i.CountedQty__c = u.inventoryItem.CountedQty__c;
                        i.Article__c = u.article.id;
                        i.Inventory__c = oid;
                        if(currentInventory.Stock__r.ValuationType__c == true)
                        {
                            i.ValuationType__c = u.inventoryItem.ValuationType__c;
                        }
                                                                       
                        // Stock item exists? Yes - updating existing item, if not - creating a new stock item only if the quantity > 0
                        if(stockItemsFromInventoryItems.containsKey(u.article.name + '-' + u.inventoryItem.ValuationType__c))
                        {
                            i.StockItem__c = stockItemsFromInventoryItems.get(u.article.name + '-' + u.inventoryItem.ValuationType__c).id;
                            i.CurrentQty__c = stockItemsFromInventoryItems.get(u.article.name + '-' + u.inventoryItem.ValuationType__c).Qty__c;
                            System.debug('#### stock item exists');
                        }
                        else
                        {   
                            System.debug('#### stock item NOT exists');

                            // GMSSU 05.04.2013: was extended as desired by Mr. Schmitz.
                            if(u.inventoryItem.CountedQty__c > 0)
                            {
                                SCStockItem__c s = new SCStockItem__c();
                                s.Qty__c = u.inventoryItem.CountedQty__c;
                                s.Stock__c = currentInventory.Stock__c;
                                s.Article__c = u.article.id;
                                String id2valueValType = '';
                                if(currentInventory.Stock__r.ValuationType__c == true)
                                {
                                    s.ValuationType__c = u.inventoryItem.ValuationType__c;
                                    if(String.isNotBlank(u.inventoryItem.ValuationType__c))
                                    {
                                        id2valueValType = u.inventoryItem.ValuationType__c;
                                    }
                                }
                                
                                // GMSSU 18.08.2014 PMS 38579
                                // We need to create a value for the ID2 field.
                                // Pattern: Plant__c & '-' & Stock__r.Name & '-' & Article__r.Name & '-' & TEXT(ValuationType__c)
                                String id2valuePlant = currentInventory.Stock__r.Plant__r.Name != null ? currentInventory.Stock__r.Plant__r.Name : '';
                                String id2valueArticle = u.article.Name != null ? u.article.Name : '';  
                                
                                String id2value = id2valuePlant + '-' + currentInventory.Stock__r.Name + '-' + id2valueArticle + '-' + id2valueValType;
                                
                                s.ID2__c = id2value;
                                
                                newStocktems++;
                                mapStockItems.put(u.article.id + '-' + u.inventoryItem.ValuationType__c, s);
                                newStockItemList.add(s);
                            }
                            else
                            {
                                System.debug('#### Cannot create new inventory item because stock item couldnt be created: ' + i.Article__c);
                                canCreateNewInventoryItem = false;
                            }
                        }
                        
                        if(canCreateNewInventoryItem)
                        {
                            System.debug('#### New Inventory Item created with article-id: ' + i.Article__c);
                            newInventoryItemList.add(i);
                            newInventoryItems++;
                        }
                    }
                }                
            }
            
            try
            {
                // Updating inventory items
                if(itemsToUpdate != null && !itemsToUpdate.isEmpty())
                {
                    upsert itemsToUpdate;
                }
                
                // Inserting new stock items
                if(!newStockItemList.isEmpty())
                {
                    insert newStockItemList;
                }
                
                // Inserting / updating relevant inventory items
                if(!newInventoryItemList.isEmpty())
                {        

                    for(SCInventoryItem__c i : newInventoryItemList)
                    {                        
                        if(i.StockItem__c == null)
                        {

                            i.StockItem__c = mapStockItems.get(i.Article__c + '-' + i.ValuationType__c).id;
                        }
                    }
                    
                    upsert newInventoryItemList;
                }
                
                System.debug('#### Update was OK');
                return true;
            }
            catch(Exception e)
            {
                System.debug('#### Cannot update items: ' + e.getMessage());
                ApexPages.addMessages(e);
                Database.rollback(sp);
                return false;
            }
        }
    
        return false;
    }
    
    
    /**
     * Parsing and processing the text for the first time.
     * This method works with a plain text (user input).
     * The correct input must have following format: 
     *  ArtNr<tab>ValuationType<tab>CountedQty
     *  or
     *  ArtNr<space>ValuationType<space>CountedQty
     *
     * @return pageReference null if there are errors or redirect to inventorys page
     */
    public PageReference processText()
    {   
        // split the text into single lines (Article | Valuation Type | Qty)
        if(String.isNotBlank(input) && input.split('\n').size() > 0)
        {
            List<String> listOne = input.split('\n');
            
            // Removing duplicate entries
            Set<String> tmpSet = new Set<String>();
            tmpSet.addAll(listOne);
            listOne = new List<String>();
            listOne.addAll(tmpSet);
            listOne.sort();
            
            show = true;   
            uploads = new List<UploadedObject>();
            uploadsMain = new List<List<UploadedObject>>();
            //unknownArticles = new List<UploadedObject>(); 
            Set<String> checkList = new Set<String>();
            
            Boolean artNoIncorrect = false;
            Boolean valIncorrect = false;
            Boolean qtyNoIncorrect = false;
            
            Boolean canProcessString = true;
            
            System.debug('#### listOne: ' + listOne.size());
             
            if(!listOne.isEmpty())
            { 
                // extract the values (article, valuation type and qty) for each line
                
                // Creating a set with all article names from the user input
                Set<String> userArticles = new Set<String>();
                for(String s : listOne)
                {       
                    List<String> str = new List<String>();
                        
                    if(s.contains('\t'))
                    {
                        str = s.split('\t');
                    }
                    else if(s.contains(' '))
                    {
                        str = s.normalizeSpace().split(' ');
                    }
                    else
                    {
                        str = null;
                        canProcessString = false;
                    }
                    
                    if(str != null) // GMSNA fix 4.3.2013 - do not join this conditions (str.size is evaluated and causes null pointer exception)
                    {
                        if((currentInventory.Stock__r.ValuationType__c == true && str.size() == 3) ||
                           (currentInventory.Stock__r.ValuationType__c == false && str.size() == 2))
                        {
                            String st = str[0];
                            st = st.escapeHtml4();
                            st = st.remove('&nbsp;');
                            st = st.trim();
                            
                            userArticles.add(st);
                        }
                    }
                }
                
                // Now selecting articles that are similar to the names from the user input
                if(userArticles.size() > 0)
                {
                    for(SCArticle__c a : [ Select Name From SCArticle__c Where Name IN :userArticles ])
                    {
                        articlesFromDB.put(a.name, a);
                    }
                    
                    System.debug('#### articlesFromDB: ' + articlesFromDB.size());
                }
                
                
                Integer maxQuantity = (Integer) appSetting.INVENTORY_MAX_QUANTITY__c;
                maxQuantity = (maxQuantity != null && maxQuantity >= 100) ? maxQuantity : 100;
                
                
                // Now validating the user inputs
                for(String s : listOne)
                {       
                    List<String> str = new List<String>();
                        
                    if(s.contains('\t'))
                    {
                        str = s.split('\t');
                    }
                    else if(s.contains(' '))
                    {
                        str = s.normalizeSpace().split(' ');
                    }
                    else
                    {
                        str = null;
                    }
    
                    if(str != null) // GMSNA fix 4.3.2013 - do not join this conditions (str.size is evaluated and causes null pointer exception)
                    { 
                        if((currentInventory.Stock__r.ValuationType__c == true && str.size() == 3) || 
                           (currentInventory.Stock__r.ValuationType__c != true && str.size() == 2))
                        {
                            SCInventoryItem__c item = new SCInventoryItem__c(Inventory__c = oid);
                            UploadedObject uo = new UploadedObject();
                            uo.inventoryItem = new SCInventoryItem__c(Inventory__c = oid);
                             
                            String st = str[0];
                            st = st.escapeHtml4();
                            st = st.remove('&nbsp;');
                            st = st.trim();
                            
                            // Article Number && Checking whether the article exists in the current inventory items
                            if(String.isNotBlank(st) && articlesFromDB.containsKey(st))
                            {   
                                item.Article__c = articlesFromDB.get(st).id;
                                item.Article__r = articlesFromDB.get(st);
                                
                                uo.artNumOk = true;
                                uo.article = articlesFromDB.get(st);
                                uo.inventoryItem.Article__c = articlesFromDB.get(st).id;
                                
                                artNoIncorrect = false;
                                
                                System.debug('#### art found: ');
                            }
                            else
                            {
                                uo.artNumOk = false;
                                SCArticle__c a = new SCArticle__c(Name = st);
                                uo.article = a;
                                
                                if(!skipUnknownArticles)
                                {
                                    artNoIncorrect = true;
                                }
                                //unknownArticles.add(uo);
                                
                                System.debug('#### art NOT found: ' + st);
                                System.debug('#### articlesFromDB: ' + articlesFromDB.size());
                            }
                            
                            // Valuation Type && Checking, whether the value is a valid picklist value
                            if(currentInventory.Stock__r.ValuationType__c == true)
                            {
                                if(String.isNotBlank(str[1]) && pickListValuesAsString.contains(str[1]))
                                {
                                    item.ValuationType__c = str[1];
                                    
                                    uo.valTypeOk = true;
                                    uo.inventoryItem.ValuationType__c = str[1];
                                    
                                    valIncorrect = false;
                                }
                                else
                                {
                                     uo.valTypeOk = true;
                                    //GMSNA: 07.06.2012 enable blank valuation types
                                    //uo.valTypeOk = false;
                                    //uo.inventoryItem.ValuationType__c = str[1];
                                    //valIncorrect = true;
                                }
                            }
                            
                            // Counted Qty
                            Integer num = 2;
                            if(currentInventory.Stock__r.ValuationType__c == false)
                            {
                                num = 1;
                            }
                                
                            if(str[num].contains(','))
                            {
                                str[num] = str[num].replace(',','.');
                            }
                            
                            str[num] = str[num].trim();
                            str[num] = str[num].normalizeSpace();
                            str[num] = str[num].escapeHtml4();
                            str[num] = str[num].remove('&nbsp;');
                            str[num] = str[num].remove('\t');
                            str[num] = str[num].remove('\n');
    
                            if(String.isNotBlank(str[num]) && str[num].containsOnly('01234567890.') && Double.valueOf(str[num]) <= maxQuantity && Double.valueOf(str[num]) >= 0)
                            {
                                item.CountedQty__c = Double.valueOf(str[num]);
                                
                                uo.countedQtyOk = true;
                                uo.inventoryItem.CountedQty__c = Double.valueOf(str[num]);
                                
                                qtyNoIncorrect = false;
                                
                                System.debug('#### str[num]: ok ');
                            }
                            else
                            {
                                uo.countedQtyOk = false;
                                uo.badCountedQty = str[num];
                                
                                qtyNoIncorrect = true;
                                
                                System.debug('#### str[num]: NOT ok ');
                            }
                            
                            if(artNoIncorrect || valIncorrect || qtyNoIncorrect)
                            {
                                mustProcessAgain = true;
                            }
                            
                            if(uploads.size() == 1000)
                            {
                                uploadsMain.add(uploads);
                                uploads = new List<UploadedObject>();
                            }
                            
                            if(checkList.isEmpty())
                            {
                                uploads.add( new UploadedObject(uo) );
                            }
                            else
                            {
                                if(!checkList.contains(uo.article.name + '-' + uo.inventoryItem.ValuationType__c))
                                {
                                    uploads.add( new UploadedObject(uo) );
                                }
                            }
                            
                            checkList.add(uo.article.name + '-' + uo.inventoryItem.ValuationType__c);
                            
                            System.debug('#### 3 vars: ' + artNoIncorrect + valIncorrect + qtyNoIncorrect);
                        }
                    }
                }
                
                uploadsMain.add(uploads);
                
                
                System.debug('#### must do again: ' + mustProcessAgain);
                
                if(!mustProcessAgain)
                {
                    //Boolean saveOk = updateItems();
                    
                    //if(saveOk)
                    //{                
                        /*
                        PageReference p = new PageReference('/' + oid);
                        return p;
                        */
                        //showstats = true;
                    //}
                    
                    //processAgain();
                    showstats = false;
                    
                }
                else
                {
                    System.debug('#### Update failed!');
                }
            }   
            
            showError = false;
            errorMessage = '';
        }
        else
        {
            showError = true;
            errorMessage = 'Bitte geben Sie etwas ein um fortzufahren!';
            //errorMessage = 'Please enter the text to process!';
            show = false;
        }
        
        return null;
    }

    
}
