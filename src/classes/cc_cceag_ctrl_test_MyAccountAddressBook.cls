/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class cc_cceag_ctrl_test_MyAccountAddressBook {
    
	static final string testPortalUserProfile = 'Customer Community User';

	public static user createTestAccountPortalUser(String profileName) {
        
		User u = [select timezonesidkey from user where id = :userinfo.getUserId()];
		Account acc = new Account(name='CloudCrazeTestAccount', OwnerId=userinfo.getUserId());
		insert acc;
		Contact c = new Contact(FirstName='joe', LastName='Smith', accountId=acc.id);
		insert c;
		String randomUserName = String.valueOf(System.now().getTime()) + '@fido.com';
		User userObj=null;
		for(Profile p:[Select p.name, p.id From Profile p where p.Name= :profileName]) {
			userObj = new User(alias = 'flotv', email='fido@fido.com',
			emailencodingkey='UTF-8', lastname='TestUser1', languagelocalekey='en_US',
			localesidkey='en_US', profileid = p.Id,
			timezonesidkey=u.TimeZoneSidKey, username=randomUserName, isActive=true, ContactId=c.id);
			insert userObj;
			break;
		}
		
        Account shipTo = new Account(Name = 'ShipTo');
        insert shipTo;
        
        Account re = new Account(Name = 'RE');
        insert re;
        
        
        // add AG role
        insert new CCAccountRole__c(AccountRole__c = 'AG', SlaveAccount__c = acc.Id, MasterAccount__c = acc.Id);
        insert new CCAccountRole__c(AccountRole__c = 'WE', SlaveAccount__c = shipTo.Id, MasterAccount__c = acc.Id);
        insert new CCAccountRole__c(AccountRole__c = 'RE', SlaveAccount__c=re.Id, MasterAccount__c=acc.Id);
        
		return userObj;
	}
	
    static testMethod void testFetchData() {
        
    	insert new ecomSettings__c(name='editContactDataEmailRecipient', value__c='g.cueto@yoursl.de');
    	User testUser = createTestAccountPortalUser(cc_cceag_ctrl_test_MyAccountAddressBook.testPortalUserProfile);
    	
    	system.runas(testUser) {
            
    		test.startTest();
    		//exception flow
    		cc_cceag_ctrl_MyAccountAddressBook.fetchData(null);
    		//happy flow
	        PageReference page = System.Page.ccrz__HomePage;
			page.getParameters().put('portalUser', UserInfo.getUserId());
			Test.setCurrentPage(page);
			ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
			cc_cceag_ctrl_MyAccountAddressBook.fetchData(ctx);
			// 
			list<Account> accounts = cc_cceag_ctrl_MyAccountAddressBook.getAccountsWithAddresses(testUser.Contact.Account.id);
			system.assert(accounts != null);
			test.stopTest();
    	}
    }
    
    
    static testMethod void testSaveData() {
        
    	insert new ecomSettings__c(name='editContactDataEmailRecipient', value__c='g.cueto@yoursl.de');
    	insert new ecomSettings__c(name='systemEmailAddress', value__c='no-reply@cceag.de');
        
    	/*cc_cceag_ctrl_MyAccountAddressBook.saveData(null, null);
    	
    	*/
        
        Account account = new Account(
            Name = 'Account',
            AccountNumber = 'lkjh89126z91hn',
			BillingStreet = 'BillingStreet',
            BillingCity = 'BillingCity',
            BillingPostalCode = 'BillingPostalCode'
        );
        insert account;
        
        Map<String, String> emailData = new Map<String, String>{
            'id' => String.valueOf(account.Id),
    		'userId' => userInfo.getUserId(),
    		'newContactName' => 'john smith',
    		'newContactStreet' => 'test drive',
    		'newContactStreetNumber' => '111',
    		'newContactZipCode' => '11111',
    		'newContactCity' => 'test city',
    		'newContactCountry' => 'test country',
    		'newContactPhone' => '111-222-3333',
    		'newContactMobile' => '222-333-4444',
    		'newBillingName' => 'john smith',
    		'newBillingStreet' => '111 test',
    		'newBillingStreetNumber' => '111 test',
    		'newBillingZipCode' => '77777',
    		'newBillingCity' => 'test city',
    		'newBillingCountry' => 'test country'
    	};

        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        
    	ccrz.cc_RemoteActionResult result = cc_cceag_ctrl_MyAccountAddressBook.saveData(ctx, emailData);
        
        System.assertEquals(true, result.success);
        // Map<String, String> responseData = (Map<String, String>) result.responseData;
        // System.assertEquals('true', responseData.get('emailSent'));
    }


    public static testMethod void testHandleException ()
    {
        insert new ecomSettings__c(name='editContactDataEmailRecipient', value__c='g.cueto@yoursl.de');
        
        ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
        NullPointerException thrownException = new NullPointerException();
        
        cc_cceag_ctrl_MyAccountAddressBook.handleException(result, thrownException);
               
        List<ccrz.cc_bean_Message> messages = (List<ccrz.cc_bean_Message>) result.messages;
        System.assertEquals(1, messages.size());
        ccrz.cc_bean_Message message = messages.get(0);
        System.assertEquals(ccrz.cc_bean_Message.MessageType.CUSTOM, message.type);
        System.assertEquals('messagingSection-Error', message.classToAppend);
        System.assertEquals(ccrz.cc_bean_Message.MessageSeverity.ERROR, message.severity);
    }
}
