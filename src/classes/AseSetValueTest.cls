/*
 * @(#)AseSetValueTest.cls SCCloud    hs 28.Feb.2011
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author HS <hschroeder@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class AseSetValueTest 
{
    /**
     * Class under test
     */
    static testMethod void testSetValue()
    {
        // create test order
        System.debug('Testing');
        SCHelperTestClass.createOrderTestSet(true);
        SCAppointment__c appointment = SCHelperTestClass.appointments[0];
        System.debug('Appointment before fix: ' + appointment);
        
        // fix appointment plus 30 mins
        Datetime start = appointment.Start__c;
        start = Datetime.newInstance(start.getTime() + 30 * 60000);        

        // inform ASE engine about fixed appointment with 30 min shift
        AseSetValue job = new AseSetvalue();
        job.setParams(appointment.id, 'datstart', start.format());
        job.getValueCtx('userID');
        job.getValue('key');
        System.debug('Fixing appointment to: ' + start.format());
    }
}
