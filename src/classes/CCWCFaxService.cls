/**
* @author		Austen Buennemann
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				a.buennemann@yoursl.de
*
* @description	FaxService to send XML to SAP Webservice with PDF as base64 code.
*
* @date			15.09.2014
*
* Timeline:
* Name               DateTime                  Description
* Austen Buennemann  15.09.2014 16:22          Class created
*/

public with sharing class CCWCFaxService  extends SCInterfaceExportBase{
	private static Account parentAccount;
	private static String pdfOrderSum;

    public CCWCFaxService()
    {
    }  
    
    public CCWCFaxService(String interfaceName, String interfaceHandler, String refType, String subclassName)
    {
        super(interfaceName, interfaceHandler, refType, subclassName);
    }
    
    public CCWCFaxService(String interfaceName, String interfaceHandler, String refType, Account account, String document)
    {
    	parentAccount = account;
    	pdfOrderSum = document;
    }
          
     /**
     * Prepare to make the web service callout synchrounously or asynchronously (future)
     *
     * ### Set the interface name, its interface handler and a refType for head id in an interface object!
     *
     * @param ibid Account id
     * @param testMode true: emulate call
     * @param accString JSON String with Account elements
     * @param document String base64 String;
     */
     
    //@future(callout=true)     
	public static void executecallout(String accString, String document, boolean testMode){			
										
		Account acc = (Account) JSON.deserialize(accString, Account.class);
		CCWCFaxService.callout(acc.Id, false, testMode, acc, document);
	}
    
    public static String callout(String ibid, boolean async, boolean testMode, Account account, String document)
    {
        String interfaceName = 'FaxService';
        String interfaceHandler = 'CCWCFaxServiceHandler';
        String refType = 'Account';
        CCWCFaxService request = new CCWCFaxService(interfaceName, interfaceHandler, refType, account, document);
        debug('Request ID: ' + ibid);
        return request.calloutCore(ibid, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCFaxService'); 
    }  

     /**
     * Reads records to send
     *
     * @param Id Account
     * @param retValue Map Object
     * @param testMode if true no call is made, because in the test mode the salesforce framework prevent it.
     */
    public override void readSalesforceData(String Id, Map<String, Object> retValue, Boolean testMode)
    {    	
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();                
        retValue.put('ROOT', parentAccount);
        debug('Account: ' + retValue);
    }

    /**
     * Fills the send structure and sends the call to SAP
     * 
     * @param dataMap with the Account Item under the key ROOT
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure
     *           message = data sent to ERP
     *           message_v1 = endpoint 
     *           message_v2 = a return value of web service 
     *           message_v3 = a mandatory fields error
     *
     */
      
    public override Boolean fillAndSendERPData(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs)
    {
        Boolean retValue = true;
        Account acc = (Account)dataMap.get('ROOT');
        
        // instantiate the client of the web service
        piCceagDeSfdcFaxservice.HTTPS_Port ws = new piCceagDeSfdcFaxservice.HTTPS_Port();
        
        // sets the basic authorization for the web service
        ws.inputHttpHeaders_x = u.getBasicAuth();
        
        //Test
        //Map<String, String> autMap = new Map<String, String>(); //
        //autMap.put('Authorization: Basic', 'TEST'); //
		//ws.inputHttpHeaders_x = autMap; //
		
        // sets the endpoint of the web service
        ws.endpoint_x  = u.getEndpoint('FaxService');
        
        //Test
        //ws.endpoint_x  = 'http://requestb.in/13qzxbo1'; //  
        
        debug('endpoint: ' + ws.endpoint_x);
        rs.message_v1 = ws.endpoint_x;
		ws.timeout_x = u.getTimeOut();
        String errormessage = '';
        
        
        try
        {            
            piCceagDeSfdcFaxservice.FaxService fax = new piCceagDeSfdcFaxservice.FaxService();
                        
            setRequesteData(fax, u, testMode);
            
            String jsonsfdcItems = JSON.serialize(fax);
            String fromJSONMapRequest = getDataFromJSON(jsonsfdcItems);
            debug('from json Fax data: ' + fromJSONMapRequest);
                                                            
            rs.message = '\n messageData: ' + 'To: ' + fax.to + ', Subject: ' + fax.subject + ', Attachment: ' + fax.attachment.left(20) + ' ... more';
            
            // check if there are missing mandatory fields
            if(rs.message_v3 == null || rs.message_v3 == '')
            {
                // All mandatory fields are filled
                // callout
                if(!testMode)
                {
                    // It is not the test from the test class
                    errormessage = 'Call ws.FaxService: \n\n';
                    rs.setCounter(1);
                    ws.FaxService_Out(fax.to, fax.subject, fax.attachment);
                    rs.Message_v2 = 'void';
                }
            }
        }
        catch(Exception e)
        {
            String prevMsg = e.getMessage();
            if(prevMsg.contains('Parser was expecting element') && prevMsg.contains('FaxService:SFDCResponse')){
            	system.debug('ignore Error in async Webservice: Parser was expecting element ... FaxService:SFDCResponse ...');
            }else{
	            String newMsg = 'fillAndSendERPData: ' + errormessage + ' \n\n ' + prevMsg;
	            debug(newMsg);
	            e.setMessage(newMsg);
	            throw e;
            }
        }
                
        return retValue;
    }
   
     /**
     * sets the call out Request structure with data form an Account and Attachment
     *
     * @param fax callout to and subject elements
     * @param u instance of the util class CCWSUtil
     * @param testMode
     * @return ###
     */
    public String setRequesteData(piCceagDeSfdcFaxservice.FaxService fax, CCWSUtil u, Boolean testMode)
    {
        CCSettings__c ccset = CCSettings__c.getInstance();
    
        String retValue = '';
     	      
        try 
        {
        	fax.to 			= parentAccount.GFGHPreferedOrderFax__c;
        	fax.subject 	= 'Bestellzusammenfassung vom ' + System.now().format('dd.MM.yyyy') + ' für ' + parentAccount.Name;
        	fax.attachment 	= pdfOrderSum;
						 
        }
        catch(Exception e)
        {
            String prevMsg = e.getMessage();
            String newMsg = 'set_FaxService_Data: ' + 'To: ' + fax.to + ', Subject: ' + fax.subject + ', Attachment: ' + fax.attachment.left(20) + ' ... more' + ' ' + prevMsg;
            debug(newMsg);
            e.setMessage(newMsg);
            throw e;
        }
        
        return retValue;    
    }

   /**
     * Sets the ERPStatusEquipmentUpdate__c in the root object to 'pending'
     * 
     * @param dataMap of objects to dispatch
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure with error messages
     * @param error true if some error has occurred
     */
    public override void setERPStatus(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs, Boolean error)
    {
    }
    

    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }

}
