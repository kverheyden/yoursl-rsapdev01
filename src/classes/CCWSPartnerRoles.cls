/*
 * @(#)CCWSPartnerRoles.cls 
 */
 
/**
 * Implements services to modify the partner roles
 *
 * @author Falk Wiegand
 */
global with sharing class CCWSPartnerRoles
{
    //String CCWSDeletePartnerRoles( string listOfSAPIds )
    WebService static String deleteroles(String listOfSAPIds)
    {
    	if( listOfSAPIds==null) return 'NOK ; empty list';
    	if( listOfSAPIds.length() == 0 ) return 'SUCC ; empty list';
    	
    	String result = 'SUCC';
    	
    	try
    	{
    		Set<String> ids = new Set<String>();
    		for( String id: listOfSAPIds.split(';'))
    		{
    			ids.Add(id);
    		}

    		List<CCAccountRole__c> rolesToDelete = [Select Id FROM CCAccountRole__c 
      								WHERE MasterAccount__r.ID2__c in: ids ];
      								
      		if( rolesToDelete.size() > 0 )
    			delete rolesToDelete;
    		
    		result = 'SUCC ; ' + rolesToDelete.size();
    	} 
    	catch( Exception ex )
    	{
    		result = 'NOK ; ' + ex;
    	}
        
        return result;
    }
}
