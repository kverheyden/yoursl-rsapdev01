@isTest
private class cc_cceag_test_StatusBar {
	
	@isTest static void testRemoteAction() {
		Account a = cc_cceag_test_TestUtils.createAccount(true);
		User u = cc_cceag_test_TestUtils.getPortalUser(a);
		insert u;

		Id portalUserId = u.Id;
		ccrz__E_Cart__c cart = cc_cceag_test_TestUtils.createCart(a, true);
		String cartId = [SELECT ccrz__EncryptedId__c FROM ccrz__E_Cart__c WHERE Id = :cart.Id].ccrz__EncryptedId__c;

		Test.startTest();

		// test with no products in the cart
		cc_cceag_ctrl_StatusBar.StatusBarModel model = cc_cceag_ctrl_StatusBar.showStatusBar(portalUserId, cartId);
		System.assertNotEquals(null, model);
		System.assertEquals(0, model.discountLevel);
		System.assertEquals(10, model.zfkMiamfoac);
		System.assertEquals(0, model.zfkTotal);

		Integer itemCount = cc_cceag_ctrl_StatusBar.getUniqueCartCount(cartId);
		System.assertEquals(0, itemCount);

		// test with a product in the cart
		ccrz__E_Product__c product1 = cc_cceag_test_TestUtils.createProduct(true);
		ccrz__E_CartItem__c cartItem1 = new ccrz__E_CartItem__c(ccrz__Quantity__c=1, ccrz__product__r=product1, ccrz__cart__c=cart.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0, ccrz__cartItemType__c='Major');
		insert cartItem1;

		model = cc_cceag_ctrl_StatusBar.showStatusBar(portalUserId, cartId);
		System.assertNotEquals(null, model);
		System.assertEquals(0, model.discountLevel);
		System.assertEquals(10, model.zfkMiamfoac);
		System.assertEquals(0, model.zfkTotal);

		Test.stopTest();
	}
	
}
