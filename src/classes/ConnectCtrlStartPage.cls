public with sharing class ConnectCtrlStartPage {
    
    public User userProfileInfo                                     { get; private set; }
    
    public ConnectCtrlStartPage(){
        List<User> currentUser = [SELECT ID, FirstName, LastName, SmallPhotoUrl FROM User WHERE Id =: UserInfo.getUserId()];
        if ( currentUser.size() > 0){
            userProfileInfo = currentUser[0];
        }
    }
}
