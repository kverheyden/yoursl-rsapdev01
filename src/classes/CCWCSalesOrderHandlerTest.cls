/**
* @author		Austen Buennemann
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				a.buennemann@yoursl.de
*
* @description	UnitTest for CCWCSalesOrderHandler
*
* @date			26.05.2014
*
*/

@isTest(SeeAllData=true)
public with sharing class CCWCSalesOrderHandlerTest {
	@isTest(SeeAllData=true)
	static void test()
	{
		//create Pricebook
		Pricebook2 standardPB = [Select Id, Name, IsActive From Pricebook2 where IsStandard = true LIMIT 1];
		if (!standardPB.isActive) {
            standardPB.isActive = true;
            update standardPB;
        }
		Pricebook2 pricebook = new Pricebook2();
		pricebook.Name = 'TestPricebook';
		pricebook.DeliveryType__c = 'TestDeliveryType';
		pricebook.Type__c = 'TestType';
		insert pricebook; 
		
		//create Account
		Account  account = new Account();
        account.LastName__c = 'testaccount 00';
        account.FirstName__c = 'Person';
        account.TargetGroup__c = '001';
        account.SubTargetGroup__c = 'DE-005';
        account.BillingPostalCode = '33106 PB'; 
        account.BillingCountry__c = 'DE';
        account.CurrencyIsoCode = 'EUR';
        account.Type = SCfwConstants.DOMVAL_ACCOUNT_TYPE_CUSTOMER;
        account.TaxType__c = SCfwConstants.DOMVAL_TAXACCOUNT_TAX;
        account.BillingStreet = 'Karl-Schurz Str.';
        account.BillingHouseNo__c = '29';
        account.BillingPostalCode = '33100 PB';
        account.BillingCity = 'Padebrorn';
        account.BillingCountry__c = 'PB';
        account.GeoX__c = 6.454447;
        account.GeoY__c = 53.253582;
        account.GeoApprox__c = false;
        account.ID2__c = '0000000test';
        account.Pricebook__c = pricebook.Id;
        insert account;
        
        //create AccountRole
        CCAccountRole__c accountrole = new CCAccountRole__c();
        accountrole.MasterAccount__c = account.Id;
        accountrole.SlaveAccount__c = account.Id;
        accountrole.Status__c = 'Active';
        accountrole.AccountRole__c = 'ZV';
        insert accountrole;
        
        //create Product
        Product2 product = new Product2();
        product.Name = 'Cola';
        product.ProductCode = 'Cola100';
        insert product;
        
        //create PricebookEntry
		PricebookEntry standardPrice = new PricebookEntry(
			Pricebook2Id = standardPB.Id, 
			Product2Id = product.Id, 
			UnitPrice = 0.70, 
			IsActive = true, 
			UseStandardPrice = false);
        insert standardPrice;        
        PricebookEntry bookentry = new PricebookEntry();
        bookentry.Pricebook2Id =  pricebook.Id;
        bookentry.Product2Id = product.Id;
        bookentry.UnitPrice = 0.75;
        bookentry.IsActive = true;
        insert bookentry;
        
		//create PricebookEntryDetail__c
		PricebookEntryDetail__c bookdetail = new PricebookEntryDetail__c();
		bookdetail.ValidUntil__c = date.today();
		bookdetail.ValidFrom__c = date.today();
		bookdetail.Product2__c = product.Id;
		bookdetail.Pricebook2__c = pricebook.Id;
		bookdetail.ListingIndicator__c = '1000';
		bookdetail.ID2__c = '0000000test';
		bookdetail.DeliveryType__c = '3213';
		bookdetail.ExternalArticleNumber__c = '25454';
		insert bookdetail;
		
		SalesOrder__c salesorder = new SalesOrder__c();
		salesorder.Account__c = account.id;
		salesorder.Status__c = Label.CCWCSalesOrder_Status;
		salesorder.Send_to_SAP__c = true;		
		salesorder.RequestedDeliveryDate__c = date.valueOf('2014-05-25');
		insert salesorder;
		SalesOrder__c salesorderOLD = salesorder;
		salesorderOLD.Status__c = 'Draft';
		
		SalesOrderItem__c salesline = new SalesOrderItem__c();
		salesline.Product2__c = product.id;
		salesline.PricebookEntryDetail__c = bookdetail.Id;
		salesline.Quantity__c = 10;
		salesline.SortOrder__c = 1;
		salesline.SalesOrder__c = salesorder.Id;
		insert salesline;
		CCWCSalesOrderHandler send = new CCWCSalesOrderHandler();
		List<SalesOrder__c> salesorders = new List<SalesOrder__c>();
		salesorders.add(salesorder);	
		List<SalesOrder__c> salesordersOLD = new List<SalesOrder__c>();
		salesordersOLD.add(salesorderOLD);			
		List<SalesOrderItem__c> saleslines = new List<SalesOrderItem__c>();
		saleslines.add(salesline);
		System.debug('CALLOUT MEHTOD FROM CCWCSalesOrder');
		send.callout(salesorders[0].Id, false);
		send.forwardCallout(salesorders, salesordersOLD, false);		
		send.sendCalloutbyCheckbox(salesorders, false);	
		
		SalesOrder__c orderupdate = new SalesOrder__c();
		orderupdate.Id = salesorder.Id;
		orderupdate.RequestedDeliveryDate__c = date.valueOf('2014-05-28');
		orderupdate.Send_to_SAP__c = true;
		update orderupdate;
	}
}
