/*
 * @(#)SCVPEquipmentController.cls
 * 
 * Copyright 2014 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * 
 * This is the equipment overview page 
 *
 * 
 * @author Mirco Stein
*/

public with sharing class SCVPEquipmentController extends SCPortal	
{
	public class InnerStocks
	{
		public SCStock__c InnerStock { get; set; }	
		public SCVPEquipmentController g_outerController;
		
		public InnerStocks(SCVPEquipmentController p_outerController)
		{
			g_outerController = p_outerController;		
		} 
		
		public void rebook()
		{
			g_outerController.doReBooking(InnerStock.id);	
		}
		
	}

	

//###################################### GETTER & SETTER ##################################################
 
   /*
    * Equipment Type values
    * @return list of select options
  	*/
    public List<SelectOption> getEquipmentType()
    {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = SCInstalledBase__c.Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        SelectOption noneOption = new SelectOption('', 'Alle');
        options.add(noneOption);
        for(Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getValue(), f.getLabel()));
        }
        return options;
	} 
 
   /*
    * Equipment Status values
    * @return list of select options
  	*/
    public List<SelectOption> getEquipmentStatus()
    {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = SCInstalledBase__c.Status__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        SelectOption noneOption = new SelectOption('', 'Alle');
        options.add(noneOption);
        for(Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getValue(), f.getLabel()));
        }
        return options;
	}

   /*
   	* Getter for the SelectList Filter of the available stocks
    * This list is used on the visualforce page.
    * @return List of the equipments
  	*/	
	public List<SelectOption> getStocks() 
	{
	 	fetchStocks();	
	 	getReBookStocks();
	 	
	 	stockOptions = new List<SelectOption>();
	 	
	 	for(SCStock__c stock : stocklist)
	 	{
	 		stockOptions.add(new SelectOption(stock.id,stock.name + ' (' + stock.type__c + ')' ));	
	 	}	

	 	return stockOptions;
	}

   
   /*
    * Getter for the Listed Equipments fetched by the setCon Method below
    * This list is used on the visualforce page.
    * @return List of the equipments
  	*/		
	public List<SCInstalledBase__c> getEquipment()
	{
		if (setCon != null)
		{
	 		return (List<SCInstalledBase__c>) setCon.getRecords();		
		}

		return null;
	}
	
	public List<InnerStocks> getReBookStocks()
	{
		g_reBookStocks = new List<InnerStocks>();
		System.debug('###getReBookStocks - stockList: ' + stockList);	
		if(stockList != null )
		{
			
			
			for(SCStock__c stock : stockList)
			{
				InnerStocks innerStock = new InnerStocks(this);
				innerStock.InnerStock = stock; 	
				g_reBookStocks.add(innerStock);	
			}
				

				
		}
		System.debug('###getReBookStocks - g_reBookStocks: ' + g_reBookStocks);
		return g_reBookStocks;
	}

        

	
//####################################  GLOBAL VARIABLES  #################################################
	
	private List<SCInstalledBase__c> 					g_EquipmentList 		= new List<SCInstalledBase__c>();
	public 	List<SCStock__c> 							stockList 				= new List<SCStock__c>();
	public 	List<InnerStocks>							g_reBookStocks;
	public List<String>									g_CDSEquipmentStockTypes;
	public List<String>									g_VendorEquipmentStockTypes;
	private SCVPEquipmentPageSettings__c					g_PageSettings;

	
//######################################   ATTRIBUTES    ##################################################
	
	public List<SelectOption> 							stockOptions			{ get; set; } // Avaialble Stocks	
	public String 										objectName 	 		 	{ get; set; } // This object	
	public List<String> 								selectedStatusValues 	{ get; set; } // Selected Filter values
	public List<String> 								selectedTypeValues   	{ get; set; } // Selected Filter values
	public String										selectedVendorId    	{ get; set; } // Helper for the Vendor Filter	
	public SCOrderExternalAssignment__c					selectedVendor 			{ get; set; } // Helper for the Vendor Filter
		
	// Variables used for the list sorting
	public String  										sortField 				{ get; set; } // Column the equips are sorted by
	public String  										sortOrder 				{ get; set; } // Order in which the columns are sorted
	public String  										sortFieldTmp = '';					  // HelperField for order and sort	
	public Boolean 										reloadForSort 			{ get; set; } // Helper for re-fetching records 	
 
	// Need this flag to be able to query equipments again
	public Boolean 										filterQuery 			{ get; set; }
	
	public String										chosenStockId    		{ get; set; }
	public String										filterStockId    		{ get; set; }

    // Helper object used in the custom filter form
    public SCInstalledBase__c 							helperEqip 				{ get; set; }
    public String 										helperbrand				{ get; set; }
    public String 										helperMatNr				{ get; set; }
    public String 										helperMatSD				{ get; set; }
    
	// Order Edit Mode Flag
	public Boolean 										equipEdit 				{ get; set; }    

	// Selected Equip ID
	public String 										selectedEquipID 		{ get; set; }

	// Rebook & Scrapping Helper
	public String 										selectedItemIDs  		{ get; set; }
	public String										reBookStock				{ get; set; }
	public String										scrappingStock			{ get; set; }
	public String										scrappingStockName	= 'EQReadyForScrapping';
	
	

//######################################  CONSTRUCTOR(S) ##################################################




//######################################   MAIN METHODS  ##################################################
	
	public void filterUpdateCustom()
	{
		filterUpdate();
		selectedItemIDs = '';
	}

   /*
    * Method to rebook Equipments from one stock to another
    * 
  	*/
    public void checkForItems() 
    {
		if(selectedItemIDs == '')
		{
			addPageMessage(Label.SC_msg_noItemsSelected);			
		}
 
    }

    public void ConfirmScrapping()
    {
    	List<id> rebookItems = new List<id>();
    	List<SCInstalledBase__c> updateItems = new List<SCInstalledBase__c>();
    	System.debug('###ConfirmScrapping - selectedItemIDs: ' + selectedItemIDs);
    	if(selectedItemIDs != '')
    	{
    		if(selectedItemIDs.contains(','))
    		{
    			rebookItems = selectedItemIDs.split(','); 	
    		}
    		else
    		{
    			rebookItems.add(selectedItemIDs); 	
    		}
    	}
    	else
    	{
    		addPageMessage('No items selected');		
    	}    	
    	System.debug('###ConfirmScrapping - rebookItems: ' + rebookItems);
    	for(SCInstalledBase__c equipment : [SELECT id, stock__c, ScrappingApproved__c FROM SCInstalledBase__c WHERE id IN :rebookItems Limit 1000])
    	{
    		equipment.ScrappingApproved__c = true;
    		updateItems.add(equipment);	
    	}
    	
    	update updateItems;    	
    }



    public void doReBooking(String p_targetSTock)
    {
    	List<id> rebookItems = new List<id>();
    	List<SCInstalledBase__c> updateItems = new List<SCInstalledBase__c>();
    	System.debug('###doReBooking - selectedItemIDs: ' + selectedItemIDs);
    	if(selectedItemIDs != '')
    	{
    		if(selectedItemIDs.contains(','))
    		{
    			rebookItems = selectedItemIDs.split(','); 	
    		}
    		else
    		{
    			rebookItems.add(selectedItemIDs); 	
    		}
    	}
    	else
    	{
    		addPageMessage('No items selected');		
    	}    	
    	System.debug('###doReBooking - rebookItems: ' + rebookItems);
    	for(SCInstalledBase__c equipment : [SELECT id, stock__c FROM SCInstalledBase__c WHERE id IN :rebookItems Limit 1000])
    	{
    		equipment.stock__c = p_targetSTock;
    		updateItems.add(equipment);	
    	}
    	
    	update updateItems;        
    }
    

   /*
    * Initialiting Set-Controller
    * @return set of the order external assignments
  	*/
    public ApexPages.StandardSetController setCon 
    {
        get 
        {
        	System.debug('###setCon - chosenStockId: ' + chosenStockId);
        	System.debug('###setCon - selectedVendor: ' + selectedVendor);

        	getStocks();
			if (setCon == null || reloadForSort || filterQuery) 
            {  
				try
				{
		        	if (!stocklist.isempty())
		        	{ 	   	
		            	if (!filterQuery)
		            	{
			            	if (sortFieldTmp != sortField)
			            	{
			            		sortOrder = 'DESC';
			            	}
			            	else
			            	{
			            		if (sortOrder == 'DESC')
			            		{
			            			sortOrder = 'ASC';
			            		}
			            		else
			            		{
			            			sortOrder = 'DESC';
			            		}
			            	}
		            	}
						
						sortFieldTmp = sortField; 
						
						if (chosenStockId == null)
						{
							if (!stocklist.isEmpty())
							{
								chosenStockId = stocklist[0].id;	
							}	
						}
		
						String query = 'SELECT ';
		
						for(Schema.FieldSetMember f : this.getFields()) 
						{
		            		query += f.getFieldPath() + ', ';
		        		}
		        		
						query += 'id, Name, ScrappingApproved__c, toLabel(Status__c) ';
						
						query += 'FROM SCInstalledBase__c ';
						
						
						// Status
						if (!selectedStatusValues.isEmpty() && String.isNotBlank(selectedStatusValues[0]))
						{	
							System.debug('###selectedStatusValues: ' + selectedStatusValues);
							query += checkWhereOrAnd() + ' Status__c = :selectedStatusValues ';
						}
		
						
						// Type
						if (!selectedTypeValues.isEmpty() && String.isNotBlank(selectedTypeValues[0]))
						{
							System.debug('###selectedTypeValues: ' + selectedTypeValues);
							query += checkWhereOrAnd() + ' Type__c = :selectedTypeValues ';
						}
					
						// Brand
						if (helperEqip.brand__c != null)
						{
							System.debug('###Brand: ' + helperEqip.brand__c);
		
							query += checkWhereOrAnd() + ' Brand__c =' + '\'' + helperEqip.brand__c + '\'';
						}				
						
						// MaterialNr
						if (helperMatNr != null && helperMatNr != '')
						{
							System.debug('###MatNr: ' + helperMatNr);
							query += checkWhereOrAnd() + ' ProductModel__r.ProductNameCalc__c = :helperMatNr ';
						}					
				
						// Material Short Description
						if (helperMatSD != null && helperMatSD != '')
						{
							System.debug('###MatSD: ' + helperMatSD);
							query += checkWhereOrAnd() + ' ProductModel__r.name = :helperMatSD ';
						}	
				
						// Manufacturer SerialNo
						if (helperEqip.ManufacturerSerialNo__c != null)
						{
							query += checkWhereOrAnd() + ' ManufacturerSerialNo__c =' + '\'' + helperEqip.ManufacturerSerialNo__c + '\'';
						}				
				

						// SerialNo
						if (helperEqip.SerialNo__c != null)
						{
							query += checkWhereOrAnd() + ' SerialNo__c =' + '\'' + helperEqip.SerialNo__c + '\'';
						}	
				
				
						if (chosenStockId != null && chosenStockId != '')
						{
							query +=  checkWhereOrAnd() + ' stock__c = ' + '\'' + chosenStockId + '\'';	
						}
	
						// Setting this flag back because it should be set by sorter function on the page
						if (reloadForSort)
						{
							reloadForSort = false;
						}
						if (filterQuery)
						{
							filterQuery = false;
						}
	
						
		       			// Sort entries
						query += ' Order By ' + sortField + ' ' + sortOrder + ' NULLS LAST Limit 1000';				
						System.debug('###query: ' + sortField); 
						System.debug('###query: ' + query); 	
						setCon = new ApexPages.StandardSetController(Database.getQueryLocator(query)); 	
						whereOrAnd = '';
		        	}
				}
				catch(QueryException e)
				{
					addPageMessage(e, Label.SC_msg_VP_DatabaseQueryError);
				}
            }
            System.debug('###sortfield: ' + sortfield);      
            return setCon;           
        }
        set;
    }

 
   /*
    * Initialiting Set-Controller
    * @return set of equipments
  	*/
    public List<SCStock__c> fetchStocks()
    {
		System.debug('###SCVPEquipmentController - fetchStocks');
		stockList.clear();
		
		if (!g_VendorEquipmentStockTypes.isEmpty())
		{
			try
			{	
				if (isVendor)
				{
					System.debug('###SCVPEquipmentController - fetchStocks - VENDOR');	

					stockList = [	Select id, Name, Name__c, Vendor__c, Plant__c, Plant__r.name, TotalArticles__c, TotalArticleCount__c, Replenishment__c, toLabel(Type__c) 
					                From SCStock__c
					                Where Vendor__c = :vendor.id AND Type__c IN :g_VendorEquipmentStockTypes Limit 1000];				
				}
			}
			catch(exception e)	
			{
				addPageMessage('Error: ' + e);	
			}		
		}
		
		if (!g_CDSEquipmentStockTypes.isEmpty())
		{
			try
			{	
				if (!isVendor && (selectedVendor != null && selectedVendor.Vendor__c != null))
				{
					System.debug('###SCVPEquipmentController - fetchStocks - NOT VENDOR');
					System.debug('###SCVPEquipmentController - fetchStocks - selectedVendor: ' + selectedVendor.Vendor__c);
					System.debug('###SCVPEquipmentController - fetchStocks - g_CDSEquipmentStockTypes: ' + g_CDSEquipmentStockTypes );	
		
					stockList = [	Select id, Name, Name__c, Vendor__c, Plant__c, Plant__r.name, TotalArticles__c, TotalArticleCount__c, Replenishment__c, toLabel(Type__c) 
					                From SCStock__c
					                Where Vendor__c = :selectedVendor.Vendor__c AND Type__c IN :g_CDSEquipmentStockTypes Limit 1000];			
				}
			}
			catch(exception e)	
			{
				addPageMessage('Error: ' + e);	
			}				
		}
		
		
		if(!stockList.isEmpty())
		{
			for(SCStock__c aStock :stockList)
			{
				if(aStock.type__c == scrappingStockName)
				{
					scrappingStock = aStock.id; 	
				}	
			}			
		}

		
		
		
		System.debug('###SCVPEquipmentController - fetchStocks - stockList: ' + stockList);	                    
	   return stockList; 		                    
	}
	
	
	public Pagereference GoToReport()
	{
  		System.debug('###SCVPEquipmentController - GoToReport - g_PageSettings: ' + g_PageSettings);
  		System.debug('###SCVPEquipmentController - GoToReport - chosenStockId: ' + chosenStockId);	
	
  		if(g_PageSettings != null && (g_PageSettings.ScrapReportID__c == null || g_PageSettings.ScrapReportID__c == ''))
  		{
  			String retUrl = URL.getCurrentRequestUrl().toExternalForm(); 
  			String ForwardUrl = '/' + g_PageSettings.id + '/e?retURL=' + String.valueof(retUrl);
  			addPageMessage(Label.SC_msg_equipmentNoScrapReportSet + ' (' + '<a href="' + ForwardUrl + '" target="_blank">Custom Setting</a>' + ')', ApexPages.Severity.warning);
  			return new Pagereference(ForwardUrl);		
  		}
		if(chosenStockId == null || chosenStockId == '')
		{
			addPageMessage(Label.SC_msg_equipment_noStockChosen);	
			return null;
		}
		else
		{
			return new Pagereference('/' + g_PageSettings.ScrapReportID__c +  '?pv0=' + String.valueof(chosenStockId).substring(0,15) + '&export=1&enc=UTF-8&xf=xls');
		}
	}
 

//######################################  HELPER METHODS ################################################## 
  
	public static SCVPEquipmentPageSettings__c InitAndReturnPageSettings()
	{
		SCVPEquipmentPageSettings__c tmp_pageSettings;
		
		tmp_pageSettings = SCVPEquipmentPageSettings__c.getValues('default');
		System.debug('###SCVPEquipmentController - InitAndReturnStockTypesToDisplayCustomsetting - tmp_pageSettings: ' + tmp_pageSettings);
		
		if (tmp_pageSettings == null)
		{
			System.debug('###SCVPEquipmentController - InitAndReturnStockTypesToDisplayCustomsetting - tmp_pageSettings == null');
			tmp_pageSettings			=	new SCVPEquipmentPageSettings__c();
			tmp_pageSettings.name	=	'default';
			tmp_pageSettings.StocksForCdsUsers__c			=	'EQReadyToMarket;EQReadyForScrapping;EQReadyForRefurbishment;EQMarketCustomerOwned;EQMarket';
			tmp_pageSettings.StocksForVendorUsers__c		=	'EQReadyToMarket;EQReadyForScrapping;EQReadyForRefurbishment';
			insert tmp_pageSettings;
			System.debug('###SCVPEquipmentController - InitAndReturnStockTypesToDisplayCustomsetting - tmp_pageSettings: ' + tmp_pageSettings);			
		}		

		return tmp_pageSettings;
	}
  

  public void init()
  {  		

  		g_PageSettings = SCVPEquipmentController.InitAndReturnPageSettings();
  		
  		if(g_PageSettings != null && (g_PageSettings.ScrapReportID__c == null || g_PageSettings.ScrapReportID__c == ''))
  		{
  			String url = '/' + g_PageSettings.id + '/e';
  			addPageMessage(Label.SC_msg_equipmentNoScrapReportSet + ' (' + '<a href="' + url + '" target="_blank">Custom Setting</a>' + ')', ApexPages.Severity.warning);		
  		}

  
  		System.debug('###SCVPEquipmentController - g_PageSettings: ' + g_PageSettings);
		
		if (g_PageSettings != null)
		{

			if (g_PageSettings.StocksForCdsUsers__c != null && g_PageSettings.StocksForCdsUsers__c != '')
			{
				g_CDSEquipmentStockTypes = g_PageSettings.StocksForCdsUsers__c.split(';'); 
		
			}
			if (g_PageSettings.StocksForVendorUsers__c != null && g_PageSettings.StocksForVendorUsers__c != '')
			{
				g_VendorEquipmentStockTypes = g_PageSettings.StocksForVendorUsers__c.split(';');
 		
			}				
		}
		
				
		System.debug('###SCVPEquipmentController - g_CDSEquipmentStockTypes: ' + g_CDSEquipmentStockTypes);
		System.debug('###SCVPEquipmentController - g_VendorEquipmentStockTypes: ' + g_VendorEquipmentStockTypes);

		// Order edit mode ist disabled by default
		equipEdit = false;
		chosenStockId = '';
		selectedEquipID = '';
		selectedItemIDs = '';
		filterObjectType = 'SCInstalledBase__c';
		filterGroupId    = '004'; // Vendor Portal
		sortField = 'name';
		sortOrder = 'DESC';
		helperbrand = '';
		helperMatNr = '';
		helperMatSD = '';
		selectedVendorId = '';
		filterQuery = false;
		reloadForSort = false;
		isVendor = false;

		// First of all read current user details
		readUser();
		// Then reading the vendor
		readVendor();	

		if (vendor!=null)
		{
			selectedVendorId = vendor.id;	
		}

		if (selectedVendor == null)
		{
			selectedVendor = new SCOrderExternalAssignment__c();	
		}
		
		// Now reading search filters for current user and object
		readFilters();	
  		checkDefaultFilter();
		fetchStocks();


		System.debug('###Init - selectedVendor: ' + selectedVendor);
		System.debug('###Init - vendor: ' + vendor);		
		
		selectedStatusValues = new List<String>{''};
		selectedTypeValues = new List<String>{''};
		helperEqip = new SCInstalledBase__c();		
		g_reBookStocks = getReBookStocks();
		System.debug('###Init - g_reBookStocks: ' + g_reBookStocks);
		// Now reading search filters for current user and object
		readFilters();	
  		checkDefaultFilter();
  		filterUpdateCustom();
  		

  }
   
   /*
    * Standard Fields for this Controller via FieldSet
    */
    public List<Schema.FieldSetMember> getFields() 
    {
	    return SObjectType.SCInstalledBase__c.FieldSets.EquipmentPage.getFields();
    }
   
   /*
    * Assign selected filter values to the variables
    */
    public override void assignFilter(SCSearchFilter__c filter)
    {
        System.debug('###SCVPEquipmentController - assignFilter');
        System.debug('###SCVPEquipmentController - assignFilter - filter: ' + filter);
        helperEqip = new SCInstalledBase__c();		
        
        if(filter.Filter__c != null)
        {
            Map<String,String> mapOfFilters = (Map<String,String>)JSON.deserialize(filter.Filter__c, Map<String,String>.class);
			System.debug('###assignFilter - mapOfFilters: ' + mapOfFilters);
           
            if (mapOfFilters.get('Status__c') != null)
            {               
                selectedStatusValues = (List<String>)JSON.deserialize(mapOfFilters.get('Status__c'),List<String>.class);
                System.debug('###assignFilter - selectedStatusValues: ' + selectedStatusValues);
            }                           
            if (mapOfFilters.get('Type__c') != null)
            {
                selectedTypeValues = (List<String>)JSON.deserialize(mapOfFilters.get('Type__c'),List<String>.class);
                System.debug('###assignFilter - selectedTypeValues: ' + selectedTypeValues);
            }          
           
            if (mapOfFilters.get('helperEqip.SerialNo__c') != null)
            {
                
                helperEqip.SerialNo__c = mapOfFilters.get('helperEqip.SerialNo__c');
                System.debug('###assignFilter - helperEqip: ' + helperEqip);
            }  
            
            
            if (mapOfFilters.get('helperEqip.brand__c') != null)
            {               
                helperEqip.brand__c = mapOfFilters.get('helperEqip.brand__c');
            }            
            if (mapOfFilters.get('helperMatSD') != null)
            {               
                helperMatSD = mapOfFilters.get('helperMatSD');
            } 
            if (mapOfFilters.get('helperMatNr') != null)
            {               
                helperMatNr = mapOfFilters.get('helperMatNr');
            } 
            if (mapOfFilters.get('helperEqip.ManufacturerSerialNo__c') != null)
            {               
                helperEqip.ManufacturerSerialNo__c = mapOfFilters.get('helperEqip.ManufacturerSerialNo__c');
            }                        
            
            if (mapOfFilters.get('chosenStockId') != null)
            {               
                chosenStockId = mapOfFilters.get('chosenStockId');
            }   
 
 			
            // Selected Vendor
            if (!isVendor)
            {
                if(mapOfFilters.get('selectedVendor.vendor__c') != null)
                {
                    selectedVendor.vendor__c = String.valueOf(mapOfFilters.get('selectedVendor.vendor__c'));
                }
            } 

            if (mapOfFilters.get('Name') != null)
            {
                objectName = mapOfFilters.get('Name');
            }
        }
         
        selectedFilter = filter.Id;
    }
    
    
/*
    * Generates a JSON string from all filter fields.
    * This string will be saved to the Filter__c field of the SCSearchFilter_c object
    * @return JSON string
    */
    public override String generateJSONFilter()
    {
       	
       	System.debug('###SCVPEquipmentController - generateJSONFilter');
       	System.debug('###SCVPEquipmentController - generateJSONFilter - objectName: ' + objectName);
       	
        Map<String,String> params = new Map<String, String>();
        params.put('Name',objectName);      

        params.put('Type__c',  JSON.serialize(selectedTypeValues));
        params.put('Status__c',JSON.serialize(selectedStatusValues));

        if (helperEqip.SerialNo__c != null)
        {
            params.put('helperEqip.SerialNo__c',String.valueOf(helperEqip.SerialNo__c));
        }
    	
    	// Selected Vendor
    	if (!isVendor && selectedVendor.Vendor__c != null)
    	{
    		params.put('selectedVendor.vendor__c',String.valueOf(selectedVendor.Vendor__c));
    	}
      
    

        if (helperEqip.brand__c != null)
        {
            params.put('helperEqip.brand__c',String.valueOf(helperEqip.brand__c));
        }
        if (helperMatSD != '')
        {
            params.put('helperMatSD',String.valueOf(helperMatSD));
        }
        if (helperMatNr != '')
        {
            params.put('helperMatNr',String.valueOf(helperMatNr));
        }
        if (helperEqip.ManufacturerSerialNo__c != null)
        {
            params.put('helperEqip.ManufacturerSerialNo__c',String.valueOf(helperEqip.ManufacturerSerialNo__c));
        }

        if (chosenStockId != '')
        {
            params.put('chosenStockId',String.valueOf(chosenStockId));
        }



        
        return JSON.serialize(params);
    }    
    
}
