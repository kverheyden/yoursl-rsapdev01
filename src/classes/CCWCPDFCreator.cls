global with sharing class CCWCPDFCreator{
		
	public static final String COOLER_SIGNATURE = 'Signature CoolerContract';
	private static String visualforceURL = getVisualforceURL();//Utils.getSFDCVisualforceURL('');			
	
	private static String getVisualforceURL() {
		SalesAppSettings__c vfPageURL = SalesAppSettings__c.getValues('VFPageUrl');
		if (vfPageURL == null)
			return '';
		return vfPageURL.Value__c;		
	}
	
	@TestVisible
	private static List<Attachment> generatedPDFs = new List<Attachment>();
	
	// RequestAttachment__c shall have only one Attachment so parentIds can be used in a Set
	private static Map<Id, Attachment> parentId_generatedAttachment = new Map<Id, Attachment>(); 
	
	@TestVisible
	private static HttpRequest request;
	
	
	@TestVisible
	private static void initHttpRequest(String userSessionId) {
		request = new HttpRequest();
		request.setMethod('GET');		
		request.setHeader('Authorization', 'OAuth ' + userSessionId);

	}
	
	@TestVisible
	private static void generateContract(Attachment signature) {
		String endpointURL = getEndpointForContract(signature);	
		System.debug('VISUALFORCE URL: ' + visualforceURL);
		System.debug('ENDPOINT URL: ' + endpointUrl);
		if (endpointURL == '') {
			// even without a contract we have to set AttachmentId__c on RequestAttachment__c
			parentId_generatedAttachment.put(signature.parentId, signature);
			return;
		}
		String requestBody = CoolerContractController.ORDER_PARAMETER_LABEL + '=' + signature.parentId + '&' +
		                     CoolerContractController.ATTACHMENT_PARAMETER_LABEL + '=' + signature.Id;
		request.setHeader('Content-Length', String.valueOf(requestBody.length()));
		request.setBody(requestBody);
		request.setEndpoint(endpointURL);		
		Attachment generatedContract = getPDFAttachment(signature.Name.replace('Signature ', ''), signature.ParentId);
		generatedPDFs.add(generatedContract);
	}	
	
	@TestVisible
	private static void generateSalesOrderSummary(Id accountId) {
		String endpointUrl = visualforceURL + 'SalesOrderSummary?supplierId=' + accountId;
		request.setEndpoint(endpointUrl);
		Attachment salesOrderSummary = getPDFAttachment('SalesOrderSummary', accountId);
		generatedPDFs.add(salesOrderSummary);
	}

	@TestVisible
	private static String getEndpointForContract(Attachment signature){
		String template = getContractTemplateName(signature);
		if (template == '')
			return '';
		return visualforceURL + template + 
			'?' + CoolerContractController.ORDER_PARAMETER_LABEL + '=' + signature.parentId + '&' +
		                     CoolerContractController.ATTACHMENT_PARAMETER_LABEL + '=' + signature.Id;
	}
	
	@TestVisible
	private static String getContractTemplateName(Attachment signature) {
		String templateName = '';
		if (signature.Name == COOLER_SIGNATURE)
			templateName = 'srLeihvertrag_Cooler_Muster_hdm_090714';
		return templateName;
	}
	
	@TestVisible
	private static Attachment getPDFAttachment(String attachmentName, Id parentId) {
		HttpResponse response = new Http().send(request);
		System.debug(UserInfo.getSessionId());
		System.debug(response.getBodyAsBlob());
		Attachment generatedContract = new Attachment();
		generatedContract.ContentType = 'application/pdf';
		generatedContract.Name = attachmentName;
		generatedContract.Body = response.getBodyAsBlob();
		generatedContract.ParentId = parentId;	
		
		parentId_generatedAttachment.put(parentId, generatedContract);
		
		return generatedContract;
	}
	
	@future (callout=true)
    public static void createContractsCallout(Map<Id, Id> attachmentId_ParentIds, String userSessionId) {
		System.debug('createContractCallout userSessionId = ' + userSessionId);
		Set<Id> requestAttachmentIds = new Set<Id>(attachmentId_ParentIds.values());
		List<Attachment> signatureAttachments = [SELECT Id, ParentId, Name, ContentType, Body FROM Attachment WHERE Id IN : attachmentId_ParentIds.keySet()];
		List<Attachment> signaturesToDelete = new List<Attachment>();
		
		initHttpRequest(userSessionId);
		for (Attachment a : signatureAttachments) {
			if (a.Name == COOLER_SIGNATURE)
				signaturesToDelete.add(a);
			generateContract(a);
		}
		
		ContactAvoidTriggerRecursion.run = false;
		if (Test.isRunningTest())
            upsert generatedPDFs;
        else
        	insert generatedPDFs;
		delete signaturesToDelete;
		setIsReadyToSendToSAPOnParentObjects(attachmentId_ParentIds.values());
	}
	
	private static void setIsReadyToSendToSAPOnParentObjects(List<Id> parentIds) {
		Set<Id> requestAttachmentIds = new Set<Id>();
		Set<Id> cdeOrderIds = new Set<Id>();
		
		for (Id pId : parentIds) {
			if (pId.getSObjectType() == CdeOrder__c.SObjectType)
				cdeOrderIds.add(pId);
		}
		setIsReadyToSendToSAPOnCdeOrders(cdeOrderIds);		
	}
	
	private static void setIsReadyToSendToSAPOnCdeOrders(Set<Id> cdeOrderIds) {
		System.debug('setIsReadyToSendToSAPOnCdeOrders' + cdeOrderIds);
		if (cdeOrderIds.size() > 0) {
			List<CdeOrder__c> cdeOrders = [SELECT IsReadyToSendToSAP__c FROM CdeOrder__c WHERE Id IN: cdeOrderIds AND Account__c != null];
			List<CdeOrder__c> cdeOrdersToUpdate = new List<CdeOrder__c>();
			for (CdeOrder__c o : cdeOrders) {
				o.IsReadyToSendToSAP__c = true;
				cdeOrdersToUpdate.add(o);
			}
			update cdeOrdersToUpdate;
		}
	}
	
	private static void setIsReadyToSendToSAPOnRequests(List<Id> requestAttachmentIds) {
		
		if (requestAttachmentIds.size() > 0) {
			List<RequestAttachment__c> requestAttachments = [SELECT Request__r.Send_to_SAP__c, AttachmentId__c FROM RequestAttachment__c WHERE Id IN: requestAttachmentIds];
			List<Request__c> requestsToUpdate = new List<Request__c>();
			
			for (RequestAttachment__c ra: requestAttachments) {
				if (!isFinalRequestAttachment(ra))
					continue;
				ra.Request__r.IsReadyToSendToSAP__c = true;
				requestsToUpdate.add(ra.Request__r);
			}
			
			update requestsToUpdate;
			System.debug('Set Send_to_SAP__c on ' + requestsToUpdate.size() + 'Requests.' + requestsToUpdate);
		}	
	}
	
	private static Boolean isFinalRequestAttachment(RequestAttachment__c ra) {
		if (ra.AttachmentId__c != null) {
			Request__c r = [SELECT NumberOfAttachments__c, (SELECT AttachmentId__c, Type__c FROM Request_Attachments__r WHERE AttachmentId__c != NULL OR Type__c =: 'none') FROM Request__c WHERE Id =: ra.Request__c LIMIT 1];
			return r.NumberofAttachments__c == r.Request_Attachments__r.size();
		}		
		return false;
	}
	
	private static void setNewAttachmentIdsOnParents() {
		List<RequestAttachment__c> requestAttachments = [SELECT AttachmentId__c FROM RequestAttachment__c WHERE Id IN: parentId_generatedAttachment.keySet()];
		List<RequestAttachment__c> requestAttachmentsToUpdate = new List<RequestAttachment__c>();
		for (RequestAttachment__c ra : requestAttachments) {
			Attachment a = parentId_generatedAttachment.get(ra.Id);
			if (a == null)
				continue;
			ra.AttachmentId__c = a.Id;
			requestAttachmentsToUpdate.add(ra);
		}
		update requestAttachmentsToUpdate;
	}
	
	@future (callout=true)
	public static void createSalesOrderSummariesAsyncCallout(Set<Id> accountIds, String userSessionId) {
		createSalesOrderSummariesCallout(accountIds, userSessionId);
	}
	
	public static void createSalesOrderSummariesCallout(Set<Id> accountIds, String userSessionId) {
		System.debug('CREATE SUMMARIES FOR SALES Order');
		System.debug(userSessionId);
		initHttpRequest(userSessionId);
		for (Id accountId : accountIds) {
			try {
				generateSalesOrderSummary(accountId);
			}
			catch (Exception e) {
				System.debug(e.getTypeName() + ' ' + e.getMessage() + ' ' + e.getStacktraceString());
			}
		}
		ContactAvoidTriggerRecursion.run = false;
		
       // Send FaxService Webservice
       try
       	{
       		CCWCFaxServiceHandler sendFax = new CCWCFaxServiceHandler();
			sendFax.readAttachment(generatedPDFs); 
       	}
       catch(Exception e)
        {
			System.debug('ERROR:' + e);
        }
		
		for (Attachment a: generatedPDFs) {
			if (a.Body == null)
				System.debug('Attachment Body is empty');
		}
		
		insert generatedPDFs;
		sendGeneratedPDFs(accountIds);
	}
	
	@TestVisible
	private static void sendGeneratedPDFs(Set<Id> accountIds) {
		System.debug('SEND GENERATED PDFs');
		Map<Id, Account> accountMap = new Map<Id, Account>([SELECT Name, GFGHPreferredOrderTransactionMethod__c, 
									   GFGHPreferedOrderFax__c, Fax, 
									   GFGHPreferedOrderEmail__c, Email__c FROM Account
									   WHERE Id IN : accountIds]);	
		
		for (Attachment a : generatedPDFs) {
			if (a.ParentId.getSobjectType() != Account.SObjectType) 
				continue;
			
			Account parentAccount = accountMap.get(a.ParentId);
			if (parentAccount.GFGHPreferredOrderTransactionMethod__c == 'fax')
				sendGeneratedPDFAsFax(parentAccount, a);
			if (parentAccount.GFGHPreferredOrderTransactionMethod__c == 'email')
				sendGeneratedPDFAsMail(parentAccount, a);
		}
	}
	
	private static void sendGeneratedPDFAsFax(Account parentAccount, Attachment a) {
		String faxNumber = String.isBlank(parentAccount.GFGHPreferedOrderFax__c) ? parentAccount.Email__c : parentAccount.GFGHPreferedOrderFax__c;
		if (String.isBlank(faxNumber))
			return;
	}
	
	private static void sendGeneratedPDFAsMail(Account parentAccount, Attachment a) {
		System.debug('SENDING MAIL');
		String mailAddress = String.isBlank(parentAccount.GFGHPreferedOrderEmail__c) ? parentAccount.Email__c : parentAccount.GFGHPreferedOrderEmail__c;
		if (String.isBlank(mailAddress))
			return;
		
		Messaging.EmailFileAttachment mailAttachment = new Messaging.EmailFileAttachment();
        mailAttachment.setFileName('SalesOrderSummary.pdf');
        mailAttachment.setBody(a.Body);
		
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setToAddresses(new List<String> {mailAddress});
		mail.setsubject('SalesOrderSummary for ' + parentAccount.Name);
		mail.setHtmlBody('Bestellzusammenfassung vom ' + System.now().format('dd.MM.yyyy') + ' für ' + parentAccount.Name);
		mail.setFileAttachments(new List<Messaging.EmailFileAttachment> {mailAttachment});
		
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
}
