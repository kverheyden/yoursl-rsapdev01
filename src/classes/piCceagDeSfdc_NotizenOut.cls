//Generated by wsdl2apex

public class piCceagDeSfdc_NotizenOut {
    public class parameters_element { 	
        public piCceagDeSfdc_NotizenOut.parameter_element parameter;
        private String[] parameter_type_info = new String[]{'parameter','http://pi.cceag.de/SFDC','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://pi.cceag.de/SFDC','false','false'};
        private String[] field_order_type_info = new String[]{'parameter'};        
    }
    public class HTTPS_Port {
        public String endpoint_x = 'https://sap-pi-dev0.dcc.cc-eag.de:44342/XISOAPAdapter/MessageServlet?senderParty=&senderService=SFDC&receiverParty=&receiverService=&interface=Notizen_Out&interfaceNamespace=http%3A%2F%2Fpi.cceag.de%2FSFDC';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://pi.cceag.de/SFDC', 'piCceagDeSfdc_NotizenOut'};
        public void Notizen_Out(piCceagDeSfdc_NotizenOut.item_element[] item,piCceagDeSfdc_NotizenOut.parameters_element[] parameters) {
            piCceagDeSfdc_NotizenOut.Notizen request_x = new piCceagDeSfdc_NotizenOut.Notizen();
            piCceagDeSfdc_NotizenOut.SFDCResponse_element response_x;
            request_x.item = item;
            request_x.parameters = parameters;
            Map<String, piCceagDeSfdc_NotizenOut.SFDCResponse_element> response_map_x = new Map<String, piCceagDeSfdc_NotizenOut.SFDCResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://sap.com/xi/WebService/soap1.1',
              'http://pi.cceag.de/SFDC',
              'Notizen_Message',
              'http://pi.cceag.de/SFDC',
              'SFDCResponse',
              'piCceagDeSfdc_NotizenOut.SFDCResponse_element'}
            );
            response_x = response_map_x.get('response_x');
        }
    }
    public class Notizen {
        public piCceagDeSfdc_NotizenOut.item_element[] item;
        public piCceagDeSfdc_NotizenOut.parameters_element[] parameters;
        private String[] item_type_info = new String[]{'item','http://pi.cceag.de/SFDC',null,'0','-1','false'};
        private String[] parameters_type_info = new String[]{'parameters','http://pi.cceag.de/SFDC',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://pi.cceag.de/SFDC','false','false'};
        private String[] field_order_type_info = new String[]{'item','parameters'};
    }
    public class SFDCResponse_element {
        private String[] apex_schema_type_info = new String[]{'http://pi.cceag.de/SFDC','false','false'};
        private String[] field_order_type_info = new String[]{};
    }
    public class parameter_element {
    	public String parameter;
        public String qualifier;
        private String[] parameter_type_info = new String[]{'parameter','http://pi.cceag.de/SFDC','string','1','1','false'};
        private String[] qualifier_att_info = new String[]{'qualifier'};        
        private String[] apex_schema_type_info = new String[]{'http://pi.cceag.de/SFDC','false','false'};
        private String[] field_order_type_info = new String[]{'parameter'};        
    }
    public class item_element {
        public String Ziel;
        public String OutletNumber;
        public String OwnerId;
        public String UserName;
        public String ActivityDate;
        public String Subject;
        public String Description;
        public String Status;
        public String Prio;
        private String[] Ziel_type_info = new String[]{'Ziel','http://pi.cceag.de/SFDC',null,'1','1','false'};
        private String[] OutletNumber_type_info = new String[]{'OutletNumber','http://pi.cceag.de/SFDC',null,'1','1','false'};
        private String[] OwnerId_type_info = new String[]{'OwnerId','http://pi.cceag.de/SFDC',null,'0','1','false'};
        private String[] UserName_type_info = new String[]{'UserName','http://pi.cceag.de/SFDC',null,'0','1','false'};
        private String[] ActivityDate_type_info = new String[]{'ActivityDate','http://pi.cceag.de/SFDC',null,'0','1','false'};
        private String[] Subject_type_info = new String[]{'Subject','http://pi.cceag.de/SFDC',null,'0','1','false'};
        private String[] Description_type_info = new String[]{'Description','http://pi.cceag.de/SFDC',null,'0','1','false'};
        private String[] Status_type_info = new String[]{'Status','http://pi.cceag.de/SFDC',null,'0','1','false'};
        private String[] Prio_type_info = new String[]{'Prio','http://pi.cceag.de/SFDC',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://pi.cceag.de/SFDC','false','false'};
        private String[] field_order_type_info = new String[]{'Ziel','OutletNumber','OwnerId','UserName','ActivityDate','Subject','Description','Status','Prio'};
    }
}
