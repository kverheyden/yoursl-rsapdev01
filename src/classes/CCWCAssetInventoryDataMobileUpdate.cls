/**
* @author		Development (AB)
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
*
* @description	The class calls CCWCAssetInventoryMobileUpdateRequest
*				Note:
*				The checkbox Mobile_Update_Indicator__c is true if the record by a mobile application has changed and is to be transferred to SAP. 
* 				After transmission, the checkbox is set to false.
*
* @date			13.10.2014
*
*/

public with sharing class CCWCAssetInventoryDataMobileUpdate {

	/**
	* @description	call webservice class CCWCAssetInventoryMobileUpdateRequest
	* @param		sibs SCInstalledBase__c records
	*/
	
    public void sendCallout(SCInstalledBase__c [] sibs){  	
    	Set<Id> ibIds = new Set<Id>();
   		for (SCInstalledBase__c sib : sibs){
    		if (sib != null && sib.Mobile_Update_Indicator__c == true){
	     		String sibString = JSON.serialize(sib);
				boolean testMode = false;
				CCWCAssetInventoryMobileUpdateRequest.executecallout(sibString, testMode);
				ibIds.add(sib.Id);
    		}
    	}
    	
    	if(!ibIds.isEmpty()){
	    	List<SCInstalledBase__c> sibUpdates = new List<SCInstalledBase__c>();
	   		List<SCInstalledBase__c> installedbases = new List<SCInstalledBase__c>([Select Mobile_Update_Indicator__c, Id From SCInstalledBase__c Where Id IN: ibIds]);
			for(SCInstalledBase__c installedbase : installedbases){
				if(installedbase != null){
					installedbase.Mobile_Update_Indicator__c = false;
					sibUpdates.add(installedbase);
				}
			}
			
			if(!sibUpdates.isEmpty()){
				update sibUpdates;
			}
    	}
    } 	
}
