/*
 * @(#)SCHoverControllerTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @version $Revision$, $Date$
 */
  
@isTest
private class SCHoverControllerTest {

    static testMethod void constructorAccount() 
    {
        SCHelperTestClass.createOrderTestSet(true);        
        ApexPages.currentPage().getParameters().put('oid', '' + SCHelperTestClass.account.Id);
        ApexPages.currentPage().getParameters().put('mode', 'ACCOUNT');
        SCHoverController controller = new SCHoverController();
        System.assertNotEquals(null, controller.items);
    }

    static testMethod void constructorContract() 
    {
        SCHelperTestClass.createOrderTestSet(true);        
        SCHoverController controller = new SCHoverController(SCHelperTestClass.account.Id, 'CONTRACT');
        System.assertNotEquals(null, controller.items);
    }

    static testMethod void constructorContractRef() 
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass2.createContractTemplates(true);
        SCHelperTestClass2.createContract('Insurance', 
                                        SCHelperTestClass2.insuranceTemplate.Id, 
                                        SCHelperTestClass.account.Id, 
                                        SCHelperTestClass.installedBaseSingle.Id, 
                                        true);
        
        SCContract__c contract = [Select Id, Name from SCContract__c where Id = :SCHelperTestClass2.contracts[0].Id];
        
        SCHoverController controller = new SCHoverController(SCHelperTestClass.account.Id, 'CONTRACTREF', contract.Name);
        System.assertNotEquals(null, controller.items);
        System.assertNotEquals(null, controller.getColumns());
        System.assertNotEquals(null, controller.getAddColumns());
        System.assertNotEquals(null, controller.getWidth());
        System.assertNotEquals(null, controller.getArrowOffset());
        System.assertNotEquals(null, controller.getColumnsWidth());
        System.assertNotEquals(null, controller.getColumnClasses());
    }
    
    static testMethod void constructorLoyalty() 
    {
        SCHelperTestClass.createOrderTestSet(true);        
        ApexPages.currentPage().getParameters().put('oid', '' + + SCHelperTestClass.account.Id);
        ApexPages.currentPage().getParameters().put('mode', 'LOYALTY');
        SCHoverController controller = new SCHoverController();
        System.assertNotEquals(null, controller.items);
    }
    
    static testMethod void constructorCases() 
    {
        SCHelperTestClass.createOrderTestSet(true);        
        ApexPages.currentPage().getParameters().put('oid', '' + + SCHelperTestClass.account.Id);
        ApexPages.currentPage().getParameters().put('mode', 'CASES');
        SCHoverController controller = new SCHoverController();
        System.assertNotEquals(null, controller.items);
    }
}
