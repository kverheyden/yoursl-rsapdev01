/*
 * @(#)SCHistoryPopupController.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.   
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This controller shows the history of an object. 
 * The object depends on the given parameter.
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public without sharing class SCHistoryPopupController
{
    public String oid { get; set; }
    public String mode { get; set; }

       
    public SCHistoryPopupController()
    {
        oid = ApexPages.CurrentPage().getParameters().get('oid');
        mode = ApexPages.CurrentPage().getParameters().get('mode');
    }
}
