/**
* @author		Austen Buennemann
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
*				a.buennemann@yoursl.de
*
* @description	Unit Test for AccountRelationOfRequestChields
*
* @date			15.08.2014
*
* Timeline:
* Name               DateTime                  Version         Description
* Austen Buennemann  15.08.2014 11:36          *1.0*           created the class
*/

@isTest 
public with sharing class AccountRelationOfRequestChieldsTest {
	
	@isTest 
	static void test() {
		
		Account  account = new Account();
        account.LastName__c = 'testaccount 00';
        account.FirstName__c = 'Person';
        account.BillingPostalCode = '33106 PB'; 
        account.BillingCountry__c = 'DE';
        //account.CurrencyIsoCode = 'EUR';
        account.BillingStreet = 'Karl-Schurz Str.';
        account.BillingHouseNo__c = '29';
        account.BillingPostalCode = '33100 PB';
        account.BillingCity = 'Padebrorn';
        account.BillingCountry__c = 'PB';
        insert account;
				
		Request__c req 	= new Request__c();
		req.OutletCompanyName__c = 'Muster GmbH';
		req.ShipToCity__c = 'Musterhausen';
		req.PayerEmail__c = 'abc@def.de';
		req.Account__c = account.Id;
		insert req;
		
		SalesOrder__c recordS = new SalesOrder__c();
		recordS.Request__c = req.Id;
		insert recordS;
		
		CdeOrder__c recordC = new CdeOrder__c();
		recordC.Request__c = req.Id;
		insert recordC;

		Visitation__c recordV = new Visitation__c();
		recordV.Request__c = req.Id;
		insert recordV;

		RedSurveyResult__c recordR = new RedSurveyResult__c();
		recordR.Request__c = req.Id;
		insert recordR;
		
		AccountRelationOfRequestChields setAccountId = new AccountRelationOfRequestChields();
		
		List<SalesOrder__c>	recordsS = new List<SalesOrder__c>();
		recordsS.add(recordS);
		setAccountId.relateSalesOrder(recordsS);		
		recordS = [SELECT Account__c FROM SalesOrder__c WHERE Id =: recordS.Id];
		System.assertEquals(req.Account__c,recordS.Account__c);		
		
		List<CdeOrder__c> recordsC = new List<CdeOrder__c>();
		recordsC.add(recordC);
		setAccountId.relateCdeOrder(recordsC);				
		recordC = [SELECT Account__c FROM CdeOrder__c WHERE Id =: recordC.Id];
		System.assertEquals(req.Account__c,recordC.Account__c);		

		List<Visitation__c>	recordsV = new List<Visitation__c>();
		recordsV.add(recordV);
		setAccountId.relateVisitation(recordsV);				
		recordV = [SELECT Account__c FROM Visitation__c WHERE Id =: recordV.Id];
		System.assertEquals(req.Account__c,recordV.Account__c);			
		
		List<RedSurveyResult__c> recordsR = new List<RedSurveyResult__c>();
		recordsR.add(recordR);
		setAccountId.relateRedSurveyResult(recordsR);				
		recordR = [SELECT Account__c FROM RedSurveyResult__c WHERE Id =: recordR.Id];
		System.assertEquals(req.Account__c,recordR.Account__c);				
		
	}
	

}
