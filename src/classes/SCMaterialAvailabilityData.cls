/**
 * SCMaterialAvailabilityData.cls    aw  24.05.2012
 * 
 * Copyright (c) 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * The class is used as a data holder for the material availability data.
 *
 */
public class SCMaterialAvailabilityData
{
    public static String MODE_PROD = 'PROD';
    public static String MODE_TEST = 'TEST';


    public class SCServiceData
    {
        public String mode;
        public Id orderId;
        public String orderNo;
        public String country;
        public String brand;
        public String erpKey;
        public Date deliveryDate;
        public List<SCArticleData> articles;
        public String accNumber;
        
        public SCServiceData()
        {
            mode = MODE_PROD;
            articles = new List<SCArticleData>();
        }
    } // class SCServiceData
    
    public class SCArticleData
    {
        public String articleNo;
        public Integer qty;
        
        public SCArticleData()
        {
        }

        public SCArticleData(String articleNo, Integer qty)
        {
            this.articleNo = articleNo;
            this.qty = qty;
        }
    } // class SCArticleData

    public class SCResult
    {
        public Boolean success;
        public String errorMsg;
        public List<SCResultData> results;
    } // class SCResult

    public class SCResultData
    {
        public String articleNr { get; set;} 
        public Date deliveryDate { get; set;} 
        public String deliveryDateStr { get; set;} 
        public String qtyStr { get; set;} 
        public Decimal qty { get; set;} 
    } // class SCResultData
} // class SCMaterialAvailabilityData
