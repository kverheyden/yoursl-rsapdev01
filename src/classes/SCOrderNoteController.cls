/*
 * @(#)SCOrderNoteController.cls
 *
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * Creates a new note for the closed order.
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
public without sharing class SCOrderNoteController
{
    public String oid = null;
    public String retURL {get; set;}
    public String errorText { get; set; }
    
    public Note note { get; set; }
    public String errorTextNote { get; set; }
    
    public Attachment attachment { get; set; }
    public String errorTextAtt { get; set; }
    
     
    
    /**
     * Default Constructor
     */
    public SCOrderNoteController()
    {
        note = new Note();
        attachment = new Attachment();
        errorText = '';
        
        // Reading the order ID
        if(ApexPages.currentPage().getParameters().containsKey('oid'))
        {
            oid = ApexPages.currentPage().getParameters().get('oid');
        }
        else
        {
            errorText = 'No order selected!';
        }
        
        if(ApexPages.currentPage().getParameters().containsKey('retURL'))
        {
            this.retUrl = ApexPages.currentPage().getParameters().get('retURL');
        }
    }
    
    /**
     * Create a new Note for the selected order.
     *
     * @return    Page Reference    Redirect back to the order.
     */ 
    public PageReference onCreateNote()
    {
        if(oid != null && oid.trim() != '')
        {
            note.Title = note.Title;
            note.Body = note.Body;
            note.isPrivate = false;
            note.ParentId = oid;
            
            try
            {
                upsert note;
                String url = (this.retURL != null) ? this.retURL : '/' + oid;
                PageReference pageRef = new PageReference(url);
                return pageRef.setRedirect(true);
            }
            catch(Exception e)
            {
                errorTextNote = String.valueOf(e);
            }
            
        }
        
        return null;
    }
    
    /**
     * Create a new Attachment for the selected order.
     *
     * @return    Page Reference    Redirect back to the order.
     * @author Eugen Tiessen <etiessen@gms-online.de>
     */ 
    public PageReference onCreateAttachment()
    {
        if(oid != null && oid.trim() != '')
        {
            attachment.isPrivate = false;
            attachment.ParentId = oid;
            
            if(attachment.Name == null || attachment.Body == null)
            {
            	 errorTextAtt = 'no attachment selected';
            	 return null;
            }
            
            try
            {
                upsert attachment;
                String url = (this.retURL != null) ? this.retURL : '/' + oid;
                PageReference pageRef = new PageReference(url);
                return pageRef.setRedirect(true);
            }
            catch(Exception e)
            {
                errorTextAtt = String.valueOf(e);
            }
            
        }
        
        return null;
    }
    
    /**
     * Cancel function for the cancel button.
     *
     * @return    Page Reference    Redirect back to the order.
     */ 
    public PageReference onCancel()
    {
        String url = (this.retURL != null) ? this.retURL : '/' + oid;
        PageReference pageRef = new PageReference(url);
        
        return pageRef.setRedirect(true);
    }

}
