global with sharing class cc_cceag_ctrl_SaleConsultant 
{
    without sharing class PriviledgedRetriever
    {
        public List<Account> getAccounts(List<Id> accountIds)
        {
            return [SELECT Id, Name, BillingAddress__c FROM Account WHERE Id IN :accountIds];
        }
                
        public Id postChatterFeedMessage(String userId, String targetAccountID, String message)
        {
            FeedItem f = new FeedItem();
            f.Body = message;
            f.NetworkScope = 'AllNetworks';     
            f.ParentId = targetAccountId;
            f.Type = 'TextPost';
            f.Visibility = 'AllUsers';
            insert f;
            return f.Id;
        }
    }
    
    @RemoteAction
    global static ccrz.cc_RemoteActionResult postMessage(ccrz.cc_RemoteActionContext ctx, String targetAccountId, String message) 
    {
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
        result.inputContext = ctx;
        result.success = false; 
        try {
            String userId = cc_cceag_dao_User.getFlowUserWithContext(ctx);
        
            PriviledgedRetriever retriever = new PriviledgedRetriever();
            ID feedItemId = retriever.postChatterFeedMessage(userId,targetAccountId, message);
            result.data = feedItemId;
            result.success = true;
            
        } catch(Exception e) { handleException(result, e); }
        
        return result;
    }
    
    @RemoteAction
    global static ccrz.cc_RemoteActionResult fetchData(ccrz.cc_RemoteActionContext ctx) {
        
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
        
        result.inputContext = ctx;
        result.success = false; 
        
        try {
            
            String userId = cc_cceag_dao_User.getFlowUserWithContext(ctx);
                                    
            // getting picklist values for "thema"
            List<Map<string,string>> addresses = new List<Map<String,String>>();
            
            User user = [
                SELECT
                    Id,
                    Contact.AccountId
                FROM
                    User
                WHERE
                    Id = :userId
            ];
            Id accountId = user.Contact.AccountId;
            
            List<Account> accountsWithAddresses = getAccountsWithAddresses(accountId);          
            
            for (Account a : accountsWithAddresses) {
                Map<String,String> address = new Map<string,string>();              
                address.put('id', String.valueOf(a.Id));
                address.put('name',a.Name);
                address.put('address',a.BillingAddress__c);
                addresses.add(address);
            }
            
            result.data = addresses;            
            result.success = true;
            
        } catch(Exception e) { handleException(result, e); }
        
        return result;
    }
    
    static List<Account> getAccountsWithAddresses(Id accountId)
    {
        List<Id> accountIds = new List<Id>();
        accountIds.add(accountId);
        
        if (!cc_cceag_dao_Account.isWEAccount(accountId)) {
            
            // get all WE for this account
            List<CCAccountRole__c> roles = [
                SELECT
                	SlaveAccount__c
                FROM
                	CCAccountRole__c
                WHERE
                	MasterAccount__c = :accountId
                	AND AccountRole__c = 'WE'
            ];
            
            for (CCAccountRole__c role : roles) {
                accountIds.add(role.SlaveAccount__c);
            }
        }
        
        PriviledgedRetriever retriever = new PriviledgedRetriever();
        return retriever.getAccounts(accountIds);
    }
    
    
   public static void handleException (ccrz.cc_RemoteActionResult result, Exception e) {
        
        System.debug(System.LoggingLevel.ERROR, e.getMessage());
        System.debug(System.LoggingLevel.ERROR, e.getStackTraceString());
        
        ccrz.cc_bean_Message msg = new ccrz.cc_bean_Message();
        msg.type = ccrz.cc_bean_Message.MessageType.CUSTOM;
        msg.classToAppend = 'messagingSection-Error';
        msg.message = e.getStackTraceString();
        msg.severity = ccrz.cc_bean_Message.MessageSeverity.ERROR;
        
        result.messages.add(msg);
    }
}
