global with sharing class cc_cceag_api_CartValidation extends ccrz.cc_api_cartExtension {

    global override boolean allowCheckout(Map<String, Object> inputData){
       	return true;
    }

    global override List<ccrz.cc_bean_Message> getCartMessages(Map<String, Object> inputData){
    	ccrz.cc_bean_CartSummary cartData = (ccrz.cc_bean_CartSummary)inputData.get(PARAM_CARTBEAN);
    	return null;
    }

}
