public with sharing class SCAddressValidationNLController extends SCfwComponentControllerBase
{
    //--<Attributes>----------------------------------
    // Show the map in the address validation
    public boolean showMap {get; set;}
    // Show the edit fields in the address validation 
    public boolean showEdit {get; set;}

    // In case the actual address could not be found for geocoding
    // use an address in the neighbourhood for geocoding 
    // (only geocoordinates will be overwritten)
    public Boolean approxSearch { get; set; }

    // Store the original address here as well to keep it in case of 
    // a neigbour search where only geo coordinates shall be overwritten
    // REMARK: This will blow up the viewstate in might result in 
    //         poorer performance.
    private AvsAddress originalAddress;

    //--<Data>----------------------------------------
    // Edit fields
    public SCInstalledBaseLocation__c addrDataEdit {get; set;}
    // Fields got from QAS
    public class ExtraFields
    {
        public String title {get; set;}
        public String firstname {get; set;}
        public String surname {get; set;}
        public String organisation {get; set;}
        public String telephoneNumber {get; set;}
        public String faxNumber {get; set;}
        public Date birthdate {get; set;}
    }
    public ExtraFields extraFields;
        
    // The address validation results (temporary)
    public AvsResult addrResult {get; set;}
    public AvsResult geocodingResult {get; set;}

    
    // The address that has to be validate (internally converted to an installed base location object)
    public void setAddr(AvsAddress a)
    {
        if (a == null)
        {
            return;
        }
        
        if (originalAddress == null)
        {
            originalAddress = a;
        }
        
        if (addrDataEdit == null)
        {
            addrDataEdit = new SCInstalledBaseLocation__c();
            extraFields = new ExtraFields();
        }
        
        addrDataEdit.country__c      = a.country;
        addrDataEdit.countrystate__c = a.countrystate;
        addrDataEdit.county__c       = a.county;
        addrDataEdit.postalcode__c   = a.postalcode;
        addrDataEdit.city__c         = a.city;
        addrDataEdit.district__c     = a.district;
        addrDataEdit.street__c       = a.street;
        addrDataEdit.houseno__c      = a.housenumber;
        
        extraFields.firstname        = a.firstname;              
        extraFields.surname          = a.surname;                
        extraFields.organisation     = a.organisation;       
        extraFields.telephoneNumber  = a.telephoneNumber;    
        extraFields.faxNumber        = a.faxNumber;          
        extraFields.birthdate        = a.birthdate;               

    }  
    
    
    public AvsAddress getAddr()
    {
        AvsAddress a = new AvsAddress();
        
        if (addrDataEdit == null)
        {
            return a;
        }
        
        a.country = addrDataEdit.country__c;
        a.countrystate = addrDataEdit.countrystate__c;
        a.county = addrDataEdit.county__c;
        a.postalcode = addrDataEdit.postalcode__c;
        a.city = addrDataEdit.city__c;
        a.district = addrDataEdit.district__c;
        a.street = addrDataEdit.street__c;
        a.housenumber = addrDataEdit.houseno__c;

        a.firstname = extraFields.firstname;
        a.surname = extraFields.surname;
        a.organisation = extraFields.organisation;
        a.telephoneNumber = extraFields.telephoneNumber;
        a.faxNumber = extraFields.faxNumber;
        a.birthdate = extraFields.birthdate;
        
        return a;
    }
    
    
    // index of the selected address in the result object
    public Integer addrSelectedItem {get; set;}

    public boolean getResultsAvailable() 
    {
        return true;
        
        /*
    
        if(addrResult != null && addrResult.items != null &&
               addrSelectedItem >= 0 && addrSelectedItem < addrResult.items.size())
        {    
            return true;
        }
        return false;
        */
    }
 

    public boolean getIsSelectedAddrGeocoded()
    {
    
        if(addrResult != null && 
           addrResult.items != null &&
           addrSelectedItem >= 0 && 
           addrSelectedItem < addrResult.items.size() &&
           addrResult.items[addrSelectedItem].GeoX != null &&
           addrResult.items[addrSelectedItem].GeoY != null &&
           (addrResult.items[addrSelectedItem].GeoX != 0 ||
            addrResult.items[addrSelectedItem].GeoY != 0) )
        {
            return true;
        }
        return  false;
    }
      
      
    public AvsAddress getAddrSel()
    {
        System.debug('#### getAddrSel()');
    
        if(addrResult != null && addrResult.items != null &&
           addrSelectedItem >= 0 && addrSelectedItem < addrResult.items.size())
        {
            System.debug('\n.........................' + addrResult.items[addrSelectedItem]);
            return addrResult.items[addrSelectedItem];
        }
        return  new AvsAddress();
    }
      
      
      
    //--<Functions>-----------------------------------------
    
    // Constructor initialisation of member variables
    public SCAddressValidationNLController()
    { 
        // enable the edit fields
        showEdit = true;
        // hide the map display
        showMap = false;
        

        init();
    } // SCAddressValidationNLController

    /**
     * Initialize internal objects
     */
    private void init()
    {
        // initialize internal objects
        addrSelectedItem = 0;
        addrDataEdit = null; //new SCInstalledBaseLocation__c();
        addrResult = new AvsResult();
        geocodingResult = new AvsResult();
    }

    public PageReference OnValidateAddress() 
    {
        AvsAddress a = getAddr();
        
        SCAddressValidation validation = new SCAddressValidation();
        addrResult = validation.check(a);
        
        addrSelectedItem = 0;

        OnSelectItem();
        
        return null;
    }

    public PageReference OnSelectItem() {
        
        if(addrResult != null && addrResult.items != null &&
           addrSelectedItem >= 0 && addrSelectedItem < addrResult.items.size())
        {
            if(addrResult.items[addrSelectedItem].geox == 0 && addrResult.items[addrSelectedItem].geoy == 0)
            {
                SCAddressValidation validation = new SCAddressValidation();
                geocodingResult = validation.geocode(addrResult.items[addrSelectedItem]);
                if(geocodingResult.status == AvsResult.STATUS_EXACT)
                {
                    // when geocoded there is always an item available 
                    addrResult.items[addrSelectedItem].geox = geocodingResult.items[0].geox;
                    addrResult.items[addrSelectedItem].geoy = geocodingResult.items[0].geoy;
                    
                    if(    (addrResult.items[addrSelectedItem].postalcode == null
                            || addrResult.items[addrSelectedItem].postalcode.equals('')
                            ) 
                        && geocodingResult.items[0].postalcode !=null)
                    {
                        addrResult.items[addrSelectedItem].postalcode = geocodingResult.items[0].postalcode;
                    }
                    if(    (addrResult.items[addrSelectedItem].county == null 
                            || addrResult.items[addrSelectedItem].county.equals('')
                           )
                         && geocodingResult.items[0].county != null)
                    {
                        addrResult.items[addrSelectedItem].county = geocodingResult.items[0].county;
                    }
                    if(geocodingResult.items[0].city != null && !geocodingResult.items[0].city.equals(''))
                    {
                        addrResult.items[addrSelectedItem].city = geocodingResult.items[0].city;
                    }
                   
                    if(    (addrResult.items[addrSelectedItem].street == null 
                            || addrResult.items[addrSelectedItem].street.equals('')
                            )
                        && geocodingResult.items[0].street != null)
                    {
                        addrResult.items[addrSelectedItem].street = geocodingResult.items[0].street;
                    }
                    if(    (addrResult.items[addrSelectedItem].housenumber == null
                            || addrResult.items[addrSelectedItem].housenumber.equals('')
                            )
                     && geocodingResult.items[0].housenumber != null)
                    {
                        addrResult.items[addrSelectedItem].housenumber = geocodingResult.items[0].housenumber;
                    }
                    if(    (addrResult.items[addrSelectedItem].district == null
                            || addrResult.items[addrSelectedItem].district.equals('')
                            )
                      && geocodingResult.items[0].district != null)
                    {
                        addrResult.items[addrSelectedItem].district = geocodingResult.items[0].district;
                    }
                   if(    (addrResult.items[addrSelectedItem].firstname == null
                            || addrResult.items[addrSelectedItem].firstname.equals('')
                            )
                     && geocodingResult.items[0].firstname != null)
                    {
                        addrResult.items[addrSelectedItem].firstname = geocodingResult.items[0].firstname;
                    }

                    if(    (addrResult.items[addrSelectedItem].surname == null
                            || addrResult.items[addrSelectedItem].surname.equals('')
                            )
                     && geocodingResult.items[0].surname != null)
                    {
                        addrResult.items[addrSelectedItem].surname = geocodingResult.items[0].surname;
                    }

                    if(    (addrResult.items[addrSelectedItem].organisation == null
                            || addrResult.items[addrSelectedItem].organisation.equals('')
                            )
                     && geocodingResult.items[0].organisation != null)
                    {
                        addrResult.items[addrSelectedItem].organisation = geocodingResult.items[0].organisation;
                    }

                    if(    (addrResult.items[addrSelectedItem].telephoneNumber == null
                            || addrResult.items[addrSelectedItem].telephoneNumber.equals('')
                            )
                     && geocodingResult.items[0].telephoneNumber != null)
                    {
                        addrResult.items[addrSelectedItem].telephoneNumber = geocodingResult.items[0].telephoneNumber;
                    }

                    if(    (addrResult.items[addrSelectedItem].faxNumber == null
                            || addrResult.items[addrSelectedItem].faxNumber.equals('')
                            )
                     && geocodingResult.items[0].faxNumber != null)
                    {
                        addrResult.items[addrSelectedItem].faxNumber = geocodingResult.items[0].faxNumber;
                    }

                    if(addrResult.items[addrSelectedItem].birthdate == null
                     && geocodingResult.items[0].birthdate != null)
                    {
                        addrResult.items[addrSelectedItem].birthdate = geocodingResult.items[0].birthdate;
                    }
                }
            }
        } 
        return null;
    }

    public PageReference OnButtonShowMap() 
    {
        showMap = true; 
        System.debug('###na OnButtonShowMap = ' + showMap);
        return null;
    }
 
    public PageReference OnClickShowMap() 
    {
        System.debug('###na OnClickShowMap = ' + showMap);
        return null;
    }



    //####### tmp #######
    public PageReference OnApproximate() {
        return null;
    }
    public PageReference OnGeocode() {
        return null;
    }


    public PageReference OnDoit() 
    {
        AvsAddress a = getAddrSel();

        if (approxSearch == true && a != null)
        {
            originalAddress.geoApprox = approxSearch;
            originalAddress.GeoX = a.GeoX;
            originalAddress.GeoY = a.GeoY;
            
            String result = pageController.OnUpdateData('validated', originalAddress);
        }
        else if (a != null)
        {
            a.geoApprox = false;
            String result = pageController.OnUpdateData('validated', a);
        }
        
        init();
        
        return null;
    }
    
    
    public String getMapUrl()
    {
         AvsAddress a = getAddrSel();
         if(a != null)
         {
            return 'center=' + a.geoy + ',' + a.geox + '&zoom=16&size=800x400&sensor=false' + 
                   '&markers=color:blue|label:ABC|' + + a.geoy + ',' + a.geox;
         }
         return '';
    }
    


}
