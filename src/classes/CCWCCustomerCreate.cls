/**
* @author		Development (AB)
* 				H&W Consult GmbH
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				http://www.hundw.com
*
* @description	The class exports Salesforce Request__c records with RequestAttachment__c entries to SAP
*
* @date			24.03.2014
*
*/
public without sharing class CCWCCustomerCreate extends SCInterfaceExportBase{
	
	private static Request__c RequestItem;
	private static List<RequestAttachment__c> RequestAttachments;

    public CCWCCustomerCreate()
    {
    }  
    
    public CCWCCustomerCreate(String interfaceName, String interfaceHandler, String refType, String subclassName)
    {
        super(interfaceName, interfaceHandler, refType, subclassName);
    }
    
    public CCWCCustomerCreate(String interfaceName, String interfaceHandler, String refType, Request__c item, List<RequestAttachment__c> attachments)
    {
    	RequestItem = item;
    	RequestAttachments = attachments;
    }
          
	
     /**
     * Prepare to make the web service callout synchrounously or asynchronously (future)
     *
     * ### Set the interface name, its interface handler and a refType for head id in an interface object!
     *
     * @param ibid      Request__c id
     * @param testMode true: emulate call
     * @param item String list with Request__c elements
     * @param attachments String list RequestAttachment__c elements
     */
    @future(callout=true)
	public static void futurecallout(String ibid, Boolean isTestMode, String item, String[] attachments){											
		simpleCallout(ibid, isTestMode, item, attachments);
	}
    
	// What stands ibid for?
	public static void simpleCallout(String ibid, Boolean isTestMode, String item, String[] attachments) {
		
		System.debug('Customer creation: isFuture = ' + System.isFuture());
		Request__c requestEntry = (Request__c)JSON.deserialize(item, Request__c.class);
		List<RequestAttachment__c> reqAttachments = new List<RequestAttachment__c>();
		if(attachments != null){
			for(String attachment: attachments){
				reqAttachments.add((RequestAttachment__c)JSON.deserialize(attachment, RequestAttachment__c.class));
			}
			CCWCCustomerCreate.callout(ibid, false, isTestMode, requestEntry, reqAttachments);
		}else{
			CCWCCustomerCreate.callout(ibid, false, isTestMode, requestEntry, null);
		}
	}
	
	
    public static String callout(String ibid, boolean async, boolean testMode, Request__c item, List<RequestAttachment__c> attachments)
    {
        String interfaceName = 'CustomerCreateSFDC_Out';
        String interfaceHandler = 'CCWCCustomerCreateHandler';
        String refType = 'Request__c';
        CCWCCustomerCreate request = new CCWCCustomerCreate(interfaceName, interfaceHandler, refType, item, attachments);
        debug('Request ID: ' + ibid);
        return request.calloutCore(ibid, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCCustomerCreate');
        
    }  

     /**
     * Reads an Request__c record to send
     *
     * @param Id Request__c
     * @param retValue Map Object
     * @param testMode if true no call is made, because in the test mode the salesforce framework prevent it.
     */
    public override void readSalesforceData(String Id, Map<String, Object> retValue, Boolean testMode)
    {    	
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();                
        retValue.put('ROOT', RequestItem);
        debug('Request__c: ' + retValue);
    }

    /**
     * Fills the send structure and sends the call to SAP
     * 
     * @param dataMap with the Request__c Item under the key ROOT
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure
     *           message = data sent to ERP
     *           message_v1 = endpoint 
     *           message_v2 = a return value of web service 
     *           message_v3 = a mandatory fields error
     *
     */
    public override Boolean fillAndSendERPData(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs)
    {
        Boolean retValue = true;
        Request__c request = (Request__c)dataMap.get('ROOT');
        // instantiate the client of the web service
        piCceagDeSfdc_CustomerCreate.HTTPS_Port ws = new piCceagDeSfdc_CustomerCreate.HTTPS_Port();
        
        // sets the basic authorization for the web service
        ws.inputHttpHeaders_x = u.getBasicAuth();
        
        //Test (for testing uncomment the next rhree lines)
        // Map<String, String> autMap = new Map<String, String>(); //
        // autMap.put('Authorization: Basic', 'TEST'); //
		// ws.inputHttpHeaders_x = autMap; //
		// End TEST setting

        // sets the endpoint of the web service
        ws.endpoint_x  = u.getEndpoint('CustomerCreate');
        
        //Test (for testing uncomment the next line)
        // ws.endpoint_x  = 'http://requestb.in/ynqyapyn'; //  
        // End TEST setting
        
        debug('endpoint: ' + ws.endpoint_x);
        rs.message_v1 = ws.endpoint_x;
		ws.timeout_x = u.getTimeOut();
        String errormessage = '';
        
        try
        {
            // instantiate the body of the call
            
            piCceagDeSfdc_CustomerCreate.Request_xc entry = new piCceagDeSfdc_CustomerCreate.Request_xc ();  
            List<piCceagDeSfdc_CustomerCreate.Attachment_element> attachments = new List<piCceagDeSfdc_CustomerCreate.Attachment_element>();
            piCceagDeSfdc_CustomerCreate.General_element general = new piCceagDeSfdc_CustomerCreate.General_element ();            
            List<piCceagDeSfdc_CustomerCreate.PricingDataInformation_element> PricingDataInformation = new List<piCceagDeSfdc_CustomerCreate.PricingDataInformation_element>();
            // set the data to the body
            setRequesteData(general, attachments, entry, request, PricingDataInformation, u, testMode);
            // piCceagDeSfdc_CustomerCreate.PricingDataInformation_element[] PricingDataInformation
            String jsonsfdcItems = JSON.serialize(general);
            String fromJSONMapRequest = getDataFromJSON(jsonsfdcItems);
            debug('from json Request__c Item: ' + fromJSONMapRequest);
            
			jsonsfdcItems = JSON.serialize(attachments);
            fromJSONMapRequest = getDataFromJSON(jsonsfdcItems);
            debug('from json Attachment Items: ' + fromJSONMapRequest);              
            
            jsonsfdcItems = JSON.serialize(entry);
            fromJSONMapRequest = getDataFromJSON(jsonsfdcItems);
            debug('from json User General Item: ' + fromJSONMapRequest);            
                                    
            rs.message = '\n messageData: ' + general + attachments + entry;
            
            // check if there are missing mandatory fields
            if(rs.message_v3 == null || rs.message_v3 == '')
            {
                // All mandatory fields are filled
                // callout
                if(!testMode)
                {
                    // It is not the test from the test class
                    // go
                    errormessage = 'Call ws.CustomerCreate: \n\n';
                    rs.setCounter(1);
                    ws.CustomerCreateSFDC_Out(general, attachments, entry, PricingDataInformation);
                    rs.Message_v2 = 'void';
                }
            }
        }
        catch(Exception e)
        {
            String prevMsg = e.getMessage();
            String newMsg = 'fillAndSendERPData: ' + errormessage + ' \n\n ' + prevMsg;
            debug(newMsg);
            e.setMessage(newMsg);
            throw e;

        }
                
        return retValue;
    }
   
     /**
     * sets the call out Request structure with data form an Request__c
     *
     * @param gso callout General structure
     * @param aso callout Attachment structure 
     * @param cso callout Request structure
     * @param item Request item with input data
     * @param u instance of the util class CCWSUtil
     * @param testMode
     * @return ###
     */
    public String setRequesteData(piCceagDeSfdc_CustomerCreate.General_element gso, List<piCceagDeSfdc_CustomerCreate.Attachment_element> aso, piCceagDeSfdc_CustomerCreate.Request_xc cso, Request__c item, piCceagDeSfdc_CustomerCreate.PricingDataInformation_element[] PricingDataInformation, CCWSUtil u, Boolean testMode)
    {
        CCSettings__c ccset = CCSettings__c.getInstance();
    
        String retValue = '';
     	      
        try 
        {
			
          	if (RequestAttachments != null){
	        	for (RequestAttachment__c attachment : RequestAttachments){
	          		piCceagDeSfdc_CustomerCreate.Attachment_element asoEntry = new piCceagDeSfdc_CustomerCreate.Attachment_element();        		
	        		asoEntry.Id = 							attachment.AttachmentId__c;
	        		asoEntry.Type_xc = 						attachment.Type__c;
	        		asoEntry.ValidFrom = 					string.valueOf(attachment.ValidFrom__c);
	        		asoEntry.ValidTo = 						string.valueOf(attachment.ValidTo__c);
	        		asoEntry.DocumentNumber_xc =			string.valueOf(attachment.DocumentNumber__c);
	        		aso.add(asoEntry);
	        	}
         	}
         	
         	/***********************************************
         	// Pricing details!!
         	***********************************************/
         	// not, if we have a CustomerCahange request
         	if(item.ChangedFields__c=='' || item.ChangedFields__c==null){ 
	         	piCceagDeSfdc_CustomerCreate.PricingDataInformation_element tmpPricingElement = new piCceagDeSfdc_CustomerCreate.PricingDataInformation_element();
	         	piCceagDeSfdc_CustomerCreate.PricingDataInformation_element tmpPricingElement2 = new piCceagDeSfdc_CustomerCreate.PricingDataInformation_element();
	         	// Jahresabnahmemenge (Tabelle KAV)
	         	if(item.PricingAnnualOrderDiscount__c!=null){
	         		if(item.PricingAnnualOrderDiscount__r.DiscountLevel__c!=null && item.PricingAnnualOrderDiscount__r.DiscountLevel__c>0){
		         		tmpPricingElement = new piCceagDeSfdc_CustomerCreate.PricingDataInformation_element();
		         		tmpPricingElement.Category	= 'Z7';
		         		tmpPricingElement.Rating	= String.valueof(item.PricingAnnualOrderDiscount__r.DiscountLevel__c);
		         		tmpPricingElement.ValidFrom	= String.valueof(item.PricingStartDate__c);
		         		PricingDataInformation.add(tmpPricingElement);
	         		}
	         	}
	        	
	        	// Tabelle generelle Konditionen (PaymentConditionDiscount__c)
	        	//if(item.PricingPaymentDiscount__c!=null){
	         		//if(item.PricingPaymentDiscount__r.PaymentType__c=='Central Effort'){
	         		if(item.CentralEffort__c){
	         			tmpPricingElement = new piCceagDeSfdc_CustomerCreate.PricingDataInformation_element();
	         			tmpPricingElement.Category 	= 'Z13';
	         			tmpPricingElement.Rating	= '2';
	         			tmpPricingElement.ValidFrom	= String.valueof(item.PricingStartDate__c);
	         			PricingDataInformation.add(tmpPricingElement);
	         		}
	         	//}
	        	
	        	// Tabelle F?rderkondition (SponsoringDiscount__c)
	        	// if(item.PricingSponsoringDiscount__c!=null){
	         		// New field SponsoringDiscountLevel__c
	         		// if(item.PricingSponsoringDiscount__r.DiscountLevel__c!=null){
	         		if(item.SponsoringDiscountLevel__c!=null){
	         			tmpPricingElement = new piCceagDeSfdc_CustomerCreate.PricingDataInformation_element();
	         			tmpPricingElement.Category 	= 'Z4';
	         			// tmpPricingElement.Rating	= String.valueOf(item.PricingSponsoringDiscount__r.DiscountLevel__c);
	         			tmpPricingElement.Rating	= String.valueOf(item.SponsoringDiscountLevel__c);
	         			tmpPricingElement.ValidFrom	= String.valueof(item.PricingStartDate__c);
	         			PricingDataInformation.add(tmpPricingElement);
	         		}
	         	//}
	        	
	        	// Tabelle Beh?lterverg?tung (UnitThroughputDiscount__c)
	        	if(item.PricingUnitThroughputDiscount__c!=null){
	         		if(item.PricingUnitThroughputDiscount__r.DiscountLevel__c!=null){
	         			tmpPricingElement = new piCceagDeSfdc_CustomerCreate.PricingDataInformation_element();
	         			if(item.PricingUnitThroughputDiscount__r.PemPom__c=='POM'){
	         				/*
	         				tmpPricingElement2 = new piCceagDeSfdc_CustomerCreate.PricingDataInformation_element();
	         				tmpPricingElement2.Category		= 'Z8';
	         				tmpPricingElement2.Rating		= '16';
	         				tmpPricingElement2.ValidFrom	= String.valueof(item.PricingStartDate__c);
	         				PricingDataInformation.add(tmpPricingElement2);
	         				*/
	         				tmpPricingElement.Category 	= 'Z9';
	         				tmpPricingElement.Rating	= String.valueOf(item.PricingUnitThroughputDiscount__r.DiscountLevel__c);
	         				tmpPricingElement.ValidFrom	= String.valueof(item.PricingStartDate__c);
	         			}
	         			else{
	         				tmpPricingElement.Category 	= 'Z8';
	         				tmpPricingElement.Rating	= String.valueOf(item.PricingUnitThroughputDiscount__r.DiscountLevel__c);
	         				tmpPricingElement.ValidFrom	= String.valueof(item.PricingStartDate__c);
	         			}
	         			PricingDataInformation.add(tmpPricingElement);
	         		}
	         	}
	        	
	        	// Water concept
	        	if(item.PricingWaterConceptDiscount__c!=null){
	         		if(item.PricingWaterConceptDiscount__r.DiscountLevel__c!=null && item.PricingWaterConceptDiscount__r.DiscountLevel__c>0){
	         			// all fields that are needed to be filled.
	         			String fieldList = 'Z18,Z19,Z20,Z21,Z22,Z23,Z24,Z35,Z36,Z37,Z38,Z39,Z40,Z41,Z42,Z43,Z44,Z45,Z46,Z47,Z48,Z49,Z50,Z51,Z52,Z53,Z54,Z62';
	         			tmpPricingElement = new piCceagDeSfdc_CustomerCreate.PricingDataInformation_element();
	        			// Make a List of the fields to loop thrue
	        			List<String> fieldNameStrings = fieldList.split(',');
	        			for(String stringField : fieldNameStrings){
	        				tmpPricingElement = new piCceagDeSfdc_CustomerCreate.PricingDataInformation_element();
	        				tmpPricingElement.Category 	= stringField;
	         				tmpPricingElement.Rating	= String.valueOf(item.PricingWaterConceptDiscount__r.DiscountLevel__c);
	         				tmpPricingElement.ValidFrom	= String.valueof(item.PricingStartDate__c);
	        				PricingDataInformation.add(tmpPricingElement);
	        			}
	         		}
	        	}
         	}// End if ChangedFields__c
        	/***********************************************
         	// END Pricing details!!
         	***********************************************/
        	
        	
        	gso.SalesGroup_xc =							item.CreatedBy.SalesGroup__c;
        	gso.EmployeeNumberGVL_xc =					item.EmployeeNumberOfCreatorsGVL__c;
            // gso.ERPDistributionChannel_xc =        		item.CreatedBy.ERPDistributionChannel__c;
            if(item.DistributionChannel__c==null || item.DistributionChannel__c==''){
        		item.DistributionChannel__c='Z1';
            }
            gso.ERPDistributionChannel_xc =				item.DistributionChannel__c;
        	gso.ERPDivision_xc =						item.CreatedBy.ERPDivision__c;
        	gso.ERPSalesArea_xc =						item.CreatedBy.ERPSalesArea__c;
        	gso.SalesOffice_xc =						item.CreatedBy.SalesOffice__c;
        	gso.EmployeeNumber_xc =						item.EmployeeNumberOfCreator__c;
        	gso.ERPCompanyCode_xc =						item.CreatedBy.ERPCompanyCode__c;
        	gso.CreatedByDeNumber_xc = 					item.CreatedByDeNumber__c;
        	
        	
  			cso.BillToCustomerNumber_xc =               item.BillToCustomerNumber__c;
			cso.BillingCity_xc =                        item.ShipToCity__c;
			cso.BillingEmail_xc =                       item.ShipToEmail__c;
			cso.BillingFax_xc =               			item.ShipToFax__c;			
			cso.BillingHouseNo_xc =                     item.ShipToHouseNo__c;
			cso.BillingMobile_xc =                      item.ShipToMobile__c;
			cso.BillingName2_xc =                       item.ShipToName2__c;
			cso.BillingName_xc =                        item.ShipToName__c;
			cso.BillingPhone_xc =                       item.ShipToPhone__c;
			cso.BillingPostalCode_xc =                  item.ShipToPostalCode__c;
			cso.BillingStreet_xc =                      item.ShipToStreet__c;
			cso.ContactFirstName_xc =                   item.ContactFirstName__c;
			cso.ContactLastName_xc =                    item.ContactLastName__c;
			cso.ContactPhone_xc =                       item.ContactPhone__c;
			cso.ContactSalutation_xc =                  item.ContactSalutation__c;
			cso.ContractObligationBeer_xc =             item.ContractObligationBeer__c;
			cso.ContractObligationCola_xc =             item.ContractObligationCola__c;
			cso.ContractObligationWater_xc =            item.ContractObligationWater__c;
			cso.Comment_xc =  				            item.Comment__c;		
			cso.DeletedFieldsCSV_xc=					item.DeletedFieldsCSV__c;
			if (item.ChangedFields__c=='' || item.ChangedFields__c==null){
				cso.CreatedBy = 						item.CreatedBy.Name;
				cso.CreatedDate =                       item.CreatedDate;
				cso.CurrencyIsoCode =                   item.CurrencyIsoCode;
				cso.IsDeleted =                         item.IsDeleted;	
				cso.Owner =                             item.Owner.Name;
				cso.RecordType =                        item.RecordType.Name;
				cso.RelatedAccount_xc = 				item.RelatedAccount__c;
				cso.SystemModstamp =                    item.SystemModstamp;
			}
			
			cso.DHFridayFrom_xc =                       item.DHFridayFrom__c;
			cso.DHFridayTo_xc =                         item.DHFridayTo__c;
			cso.DHMondayFrom_xc =                       item.DHMondayFrom__c;
			cso.DHMondayTo_xc =                         item.DHMondayTo__c;
			cso.DHThursdayFrom_xc =                     item.DHThursdayFrom__c;
			cso.DHThursdayTo_xc =                       item.DHThursdayTo__c;
			cso.DHTuesdayFrom_xc =                      item.DHTuesdayFrom__c;
			cso.DHTuesdayTo_xc =                        item.DHTuesdayTo__c;
			cso.DHWednesdayFrom_xc =                    item.DHWednesdayFrom__c;
			cso.DHWednesdayTo_xc =                      item.DHWednesdayTo__c;
			cso.DeliveryType_xc =                       item.DeliveryType__c;
			cso.EndDateBeerContractBinding_xc =         item.EndDateBeerContractBinding__c;
			cso.EndDateColaContractBinding_xc =         item.EndDateColaContractBinding__c;
			cso.EndDateWaterContractBinding_xc =        item.EndDateWaterContractBinding__c;
			cso.GfghCustomerNumbersCSV_xc =             item.GfghCustomerNumbersCSV__c;			
			cso.ID2_xc =                                item.ID2__c;	
			cso.LeadingOutlet_xc =                      item.LeadingOutlet__r.ID2__c;
			cso.LeadingOutlet_xr =                      item.LeadingOutlet__r.ID2__c;
			cso.LowEmissionZoneType_xc =                item.LowEmissionZoneType__c;
			cso.MainBrandBeer_xc =                      item.MainBrandBeer__c;
			cso.MainBrandWater_xc =                     item.MainBrandWater__c;
			cso.MainBrandWheatBeer_xc =                 item.MainBrandWheatBeer__c;
			cso.MarketingAttributeIDsCSV_xc =           item.MarketingAttributeIDsCSV__c;
			cso.Market_xc =        					    item.Market__c;	
			cso.Name =                                  item.Name;		
			cso.OperatorCompanyName_xc =                item.OperatorCompanyName__c;
			cso.OperatorFirstName_xc =                  item.OperatorFirstName__c;
			cso.OperatorLastName_xc =                   item.OperatorLastName__c;
			cso.OperatorSalutation_xc =                 item.OperatorSalutation__c;
			cso.OutletCity_xc =                         item.OutletCity__c;
			cso.OutletCompanyName_xc =                  item.OutletCompanyName__c;
			cso.OutletCustomerClass_xc =                item.OutletCustomerClass__c;
			cso.OutletDistrict_xc =                     item.OutletDistrict__c;
			cso.OutletEmail_xc =                        item.OutletEmail__c;
			cso.OutletFax_xc =                          item.OutletFax__c;
			cso.OutletHouseNo_xc =                      item.OutletHouseNo__c;
			cso.OutletKeyAccountNumber_xc =             item.OutletKeyAccountNumber__c;
			cso.OutletMobile_xc =                       item.OutletMobile__c;
			cso.OutletPhone_xc =                        item.OutletPhone__c;
			cso.OutletPostalCode_xc =                   item.OutletPostalCode__c;
			cso.OutletSeason1From_xc =                  item.OutletSeason1From__c;
			cso.OutletSeason1To_xc =                    item.OutletSeason1To__c;
			cso.OutletSeason2From_xc =                  item.OutletSeason2From__c;
			cso.OutletSeason2To_xc =                    item.OutletSeason2To__c;
			cso.OutletStreet_xc =                       item.OutletStreet__c;
			cso.OutletWebsite_xc =                      item.OutletWebsite__c;	
			cso.PayerFax_xc =          				    item.PayerFax__c;					
			cso.PayerCity_xc =                          item.PayerCity__c;
			cso.PayerCustomerNumber_xc =                item.PayerCustomerNumber__c;
			cso.PayerEmail_xc =                         item.PayerEmail__c;
			cso.PayerHouseNo_xc =                       item.PayerHouseNo__c;
			cso.PayerIban_xc =                          item.PayerIban__c;
			cso.PayerMobile_xc =                        item.PayerMobile__c;
			cso.PayerName2_xc =                         item.PayerName2__c;
			cso.PayerName_xc =                          item.PayerName__c;
			cso.PayerPhone_xc =                         item.PayerPhone__c;
			cso.PayerPostalCode_xc =                    item.PayerPostalCode__c;
			cso.PayerStreet_xc =                        item.PayerStreet__c;
			cso.PaymentType_xc =                        item.PaymentType__c;
			cso.PricingStartDate_xc =                   item.PricingStartDate__c;
			cso.PricingUnitThroughputYearly_xc =        item.PricingUnitThroughputYearly__c;
			cso.PricingWaterConcept_xc =                item.PricingWaterConcept__c;
			cso.RecyclingCity_xc =                      item.RecyclingCity__c;
			cso.RecyclingCreditTo_xc =                  item.RecyclingCreditTo__c;
			cso.RecyclingEmail_xc =                     item.RecyclingEmail__c;
			cso.RecyclingFax_xc =        			    item.RecyclingFax__c;
			cso.RecyclingHouseNo_xc =                   item.RecyclingHouseNo__c;
			cso.RecyclingIban_xc =                      item.RecyclingIban__c;
			cso.RecyclingMobile_xc =                    item.RecyclingMobile__c;
			cso.RecyclingName2_xc =                     item.RecyclingName2__c;
			cso.RecyclingName_xc =                      item.RecyclingName__c;
			cso.RecyclingPhone_xc =                     item.RecyclingPhone__c;
			cso.RecyclingPostalCode_xc =                item.RecyclingPostalCode__c;
			cso.RecyclingStreet_xc =                    item.RecyclingStreet__c;
			cso.SalesVolumeOrder_xc =                   item.SalesVolumeOrder__c;
			cso.SalesVolumeYear_xc =                    item.SalesVolumeYear__c;
			cso.SecondMainBrandWater_xc =               item.SecondMainBrandWater__c;
			cso.Source_xc =                             item.Source__c;
			cso.Sponsoring_xc =                         item.Sponsoring__c;
			cso.Subtradechannel_xc =                    item.Subtradechannel__c;		
			cso.TaxNumber_xc =                          item.TaxNumber__c;
			cso.VHFridayFrom_xc =                       item.VHFridayFrom__c;
			cso.VHFridayTo_xc =                         item.VHFridayTo__c;
			cso.VHMondayFrom_xc =                       item.VHMondayFrom__c;
			cso.VHMondayTo_xc =                         item.VHMondayTo__c;
			cso.VHThursdayFrom_xc =                     item.VHThursdayFrom__c;
			cso.VHThursdayTo_xc =                       item.VHThursdayTo__c;
			cso.VHTuesdayFrom_xc =                      item.VHTuesdayFrom__c;
			cso.VHTuesdayTo_xc =                        item.VHTuesdayTo__c;
			cso.VHWednesdayFrom_xc =                    item.VHWednesdayFrom__c;
			cso.VHWednesdayTo_xc =                      item.VHWednesdayTo__c;
			cso.VatId_xc =                              item.VatId__c;
			cso.VerificationDocumentTypes_xc =          item.VerificationDocumentTypes__c; 
			cso.CustomDeviceThroughputIndicator_xc =	item.CustomDeviceThroughputIndicator__c;
			
			// Checkboxes
				// // not, if we have a CustomerCahange request
        	if (item.ChangedFields__c=='' || item.ChangedFields__c==null || item.ChangedFields__c.contains('PricingCaseCompensation__c')){
				cso.PricingCaseCompensation_xc = item.PricingCaseCompensation__c;
        	}
        	if (item.ChangedFields__c=='' || item.ChangedFields__c==null || item.ChangedFields__c.contains('CentralEffort__c')){
				cso.CentralEffort_xc = item.CentralEffort__c;
        	}
        	if (item.ChangedFields__c=='' || item.ChangedFields__c==null || item.ChangedFields__c.contains('RecyclingCreditToBankAccount__c')){
				cso.RecyclingCreditToBankAccount_xc = item.RecyclingCreditToBankAccount__c;
        	}
        	if (item.ChangedFields__c=='' || item.ChangedFields__c==null || item.ChangedFields__c.contains('PricingCustomerOwnedUnit__c')){
				cso.PricingCustomerOwnedUnit_xc = item.PricingCustomerOwnedUnit__c;
        	}
			if (item.ChangedFields__c=='' || item.ChangedFields__c==null || item.ChangedFields__c.contains('PaymentDirectDebit__c')){
				cso.PaymentDirectDebit_xc = item.PaymentDirectDebit__c;
			}
			if (item.ChangedFields__c=='' || item.ChangedFields__c==null || item.ChangedFields__c.contains('OutletHasChain__c')){
				cso.OutletHasChain_xc = item.OutletHasChain__c;
			}
			if (item.ChangedFields__c=='' || item.ChangedFields__c==null || item.ChangedFields__c.contains('IntroductionCateringBottle__c')){
				cso.IntroductionCateringBottle_xc = item.IntroductionCateringBottle__c;
			}
			if (item.ChangedFields__c=='' || item.ChangedFields__c==null || item.ChangedFields__c.contains('OperatorIsIndividual__c')){
				cso.OperatorIsIndividual_xc = item.OperatorIsIndividual__c;
			}
			if (item.ChangedFields__c=='' || item.ChangedFields__c==null || item.ChangedFields__c.contains('PayerIsIndividual__c')){
				cso.PayerIsIndividual_xc = item.PayerIsIndividual__c;
			}
			if (item.ChangedFields__c=='' || item.ChangedFields__c==null || item.ChangedFields__c.contains('RecyclingIsIndividual__c')){
				cso.RecyclingIsIndividual_xc = item.RecyclingIsIndividual__c;
			}
			if (item.ChangedFields__c=='' || item.ChangedFields__c==null || item.ChangedFields__c.contains('ShipToIsIndividual__c')){
				cso.ShipToIsIndividual_xc = item.ShipToIsIndividual__c;
			}
			if (item.ChangedFields__c=='' || item.ChangedFields__c==null || item.ChangedFields__c.contains('IsInLowEmissionZone__c')){
				cso.IsInLowEmissionZone_xc = item.IsInLowEmissionZone__c;
			}
			if (item.ChangedFields__c=='' || item.ChangedFields__c==null || item.ChangedFields__c.contains('OutletIsBillTo__c')){
				cso.OutletIsBillTo_xc = item.OutletIsBillTo__c;
			}
			if (item.ChangedFields__c=='' || item.ChangedFields__c==null || item.ChangedFields__c.contains('OutletIsPayer__c')){
				cso.OutletIsPayer_xc = item.OutletIsPayer__c;
			}
			if (item.ChangedFields__c=='' || item.ChangedFields__c==null || item.ChangedFields__c.contains('OutletIsSeasonal__c')){
				cso.OutletIsSeasonal_xc = item.OutletIsSeasonal__c;
			}
			if (item.ChangedFields__c=='' || item.ChangedFields__c==null || item.ChangedFields__c.contains('RecyclingViaCceag__c')){
				cso.RecyclingViaCceag_xc = item.RecyclingViaCceag__c;	
			}
			if (item.ChangedFields__c=='' || item.ChangedFields__c==null || item.ChangedFields__c.contains('UnrestrictedDelivery__c')){
				cso.UnrestrictedDelivery_xc = item.UnrestrictedDelivery__c;
			}
			// Formular fields
			cso.AccountID2_xc =            				item.AccountID2__c; // Always	
			cso.SFID_xc =                               item.SFID__c;	 // Always
			if (item.ChangedFields__c=='' || item.ChangedFields__c==null || item.ChangedFields__c.contains('PayerBankCode__c')){
				cso.PayerBankCode_xc = item.PayerBankCode__c;
			}
			if (item.ChangedFields__c=='' || item.ChangedFields__c==null || item.ChangedFields__c.contains('RecyclingBankCode__c')){
				cso.RecyclingBankCode_xc = item.RecyclingBankCode__c;
			}
			if (item.ChangedFields__c=='' || item.ChangedFields__c==null || item.ChangedFields__c.contains('BlockingReason__c')){
				cso.BlockingReason_xc = item.BlockingReason__c;	 
			}
			if (item.ChangedFields__c=='' || item.ChangedFields__c==null || item.ChangedFields__c.contains('CollectionAuth__c')){
				cso.CollectionAuth_xc = item.CollectionAuth__c;
			}
			if (item.ChangedFields__c=='' || item.ChangedFields__c==null || item.ChangedFields__c.contains('PaymentMethod__c')){
				cso.PaymentMethod_xc = item.PaymentMethod__c;
			}
			
			// Other Criteria to consider
			if (item.ChangedFields__c=='' || item.ChangedFields__c==null || item.ChangedFields__c.contains('PaymentType__c')){
				cso.DeliveryTransactionCode_xc =         	item.DeliveryTransactionCode__c;
			} 
			
			//neu ab 25.06.2014
			cso.BillingCompanyName_xc =               	item.BillingCompanyName__c;
			cso.BillingFirstName_xc =               	item.BillingFirstName__c;
			cso.BillingLastName_xc =               		item.BillingLastName__c;
			cso.BillingOutletName_xc =               	item.BillingOutletName__c;
			cso.ContactAdditionalInformation_xc =       item.ContactAdditionalInformation__c;
			cso.ContactBirthdate_xc =        		    item.ContactBirthdate__c;
			cso.DeletedFieldsCSV_xc =              		item.DeletedFieldsCSV__c;
			cso.NumberofAttachments_xc =         		item.NumberofAttachments__c;
			cso.PayerCompanyName_xc =              		item.PayerCompanyName__c;
			cso.PayerFirstName_xc =               		item.PayerFirstName__c;
			cso.PayerLastName_xc =               		item.PayerLastName__c;
			cso.PayerOutletName_xc =               		item.PayerOutletName__c;
			cso.RecyclingCompanyName_xc =              	item.RecyclingCompanyName__c;
			cso.RecyclingFirstName_xc =                	item.RecyclingFirstName__c;
			cso.RecyclingLastName_xc =              	item.RecyclingLastName__c;
			cso.RecyclingOutletName_xc =              	item.RecyclingOutletName__c;
			cso.ShipToCity_xc =              			item.ShipToCity__c; //
			cso.ShipToCompanyName_xc =               	item.ShipToCompanyName__c;
			cso.ShipToEmail_xc =               			item.ShipToEmail__c; //
			cso.ShipToFax_xc =               			item.ShipToFax__c; //
			cso.ShipToFirstName_xc =               		item.ShipToFirstName__c;
			cso.ShipToHouseNo_xc =               		item.ShipToHouseNo__c; //
			cso.ShipToLastName_xc =               		item.ShipToLastName__c;
			cso.ShipToMobile_xc =               		item.ShipToMobile__c; //
			cso.ShipToName2_xc =               			item.ShipToName2__c; //
			cso.ShipToName_xc =               			item.ShipToName__c; //
			cso.ShipToOutletName_xc =               	item.ShipToOutletName__c;
			cso.ShipToPhone_xc =               			item.ShipToPhone__c; //
			cso.ShipToPostalCode_xc =               	item.ShipToPostalCode__c; //
			cso.ShipToStreet_xc =               		item.ShipToStreet__c; //
			cso.Status_xc =               				item.Status__c;	
        	
        	
        }
        catch(Exception e)
        {
            String prevMsg = e.getMessage();
            String newMsg = 'setGeneral_Request_Item: ' + gso + ' ' + cso + ' ' + prevMsg;
            debug(newMsg);
            e.setMessage(newMsg);
            throw e;
        }
        
        return retValue;    
    }

   /**
     * Sets the ERPStatusEquipmentUpdate__c in the root object to 'pending'
     * 
     * @param dataMap of objects to dispatch
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure with error messages
     * @param error true if some error has occurred
     */
    public override void setERPStatus(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs, Boolean error)
    {
    }
    

    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }

}
