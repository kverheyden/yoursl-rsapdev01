/*
 * @(#)SCMapTourItem.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/*
 * One item in an engineer's tour (e.g. an order location)
 */
public class SCMapTourItem 
{
    // ENG location of an engineer
    // ORD location of an order
    public String itemtype { get; set; }
    // the appointment type
    public String apptype {get; set;}
    
    // location longitude
    public double GeoX { get; set; }
    // location latitude
    public double GeoY { get; set; }
    
    // planned start and end (internal appointment)
    public Datetime dtStart { get; set; }
    public Datetime dtEnd { get; set; }
    // assignment status
    public String status  { get; set; }

    // order number 
    public String orderno { get; set; }
    // order id
    public String orderid { get; set; }
    
    // information - the location address
    public String infoAddress { get; set; }
    // information - order details    
    public String infoOrder { get; set; }
    // information - product details    
    public String infoProduct { get; set; }
    
}
