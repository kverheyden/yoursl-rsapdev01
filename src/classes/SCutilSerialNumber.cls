/**
 * @(#)SCutilSerialNumber.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 *
 *
 * Class checks serial numbers for validity.
 * @author Dietrich Herdt <dherdt@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCutilSerialNumber {

    public Boolean isValid { get; private set; }
    public String articleNumber { get; private set; }
    public String constructionYear { get; private set; }
    public String constructionWeek { get; private set; }
    
    public String serialNumber 
    { 
        get;
        private set
        {
            // System.debug(serialNumber + value);

            // Preparation of serial numbers with length 30.
            // These need to be handled just like serial numbers with length 28
            // A logistics partner just added two digits for palette numbers without
            // talking to VAILLANT standard's board.
            String no = value;
             
            if (no != null && no.length() == 30)
            {
                no = no.substring(0, 28);
            }
            
            this.serialNumber = no;
        } 
    }

    public SCutilSerialNumber(String serialNo)
    {
        this.serialNumber = serialNo;    
        this.isValid = extractSerialNumberDetails(this.serialNumber);    
    }
 

     /**
      * Serial number validity check.
      *
      * @param  serialNo Input serial number.
      * @return isSerialNumber True or false value for validity.
      */
       
     public static boolean isValidSerialNumber(String serialNo)
     {
         //#### CCE ###
         return true;
     
        /** 
         * As long as the number is not recognised as valid,
         * the number is wrong.
         */
         /*boolean isSerialNumber = false;
         Integer endPos = 0;
         String testNumber = '';
         Long testNumberNew = 0;
         Long checksum = 0;              
         
         try  
         {             
             //** 
             //* If the length of the input number is 8 or 9,
             //* the number is valid.
             //** 
             if (serialNo.length() == 8)
             {
                 //**
                 //* If input contains not only numbers, it is false.
                 //**
                 if (!isDouble(serialNo))
                 {    
                     return isSerialNumber;
                 }
                 else
                 {
                     isSerialNumber = true;
                 }
             }
             
             if (serialNo.length() == 9)
             {
                 //**
                 //* If input contains not only numbers, the number is not valid.
                 //**
                 if (!isDouble(serialNo))
                 {    
                     return isSerialNumber;
                 }
                 else
                 {
                     isSerialNumber = true;
                 }
             }          
    
             //**
             //* Test of serial numbers with length 20 and 22.
             //**          
             if (serialNo.length() == 20 || serialNo.length() == 22)
             {
                 //**
                 //* If input contains not only numbers, the number is not valid.
                 //**
                 if (!isDouble(serialNo))
                 {    
                     return isSerialNumber;
                 }
                 
                 //Last string of the input string is the check digit 
                 testNumber = serialNo.substring(serialNo.length()-1,serialNo.length());
                 //System.debug('Input --> testNumberString is:' + testNumber);
                 
                 for (Integer i = 0; i <= serialNo.length()-2; i++) 
                 {    
                     //**
                     //* Current character of input string.
                     //**
                     //String actString = serialNo.substring(i,endPos);
                     String actString = serialNo.substring(i,i+1);                     
                         
                     //system.debug('Current position is: ' + i);
                     //system.debug('Current substring of testNumberString is: ' + actString); 
                     checksum = checksum + (1 + math.mod(i+1, 2) * 2) * long.ValueOf(actString);
                     //system.debug('Current sum is: ' + checksum); 
                 }
                 //system.debug('Die Summe ergibt:' + checksum);
                 
                 testNumberNew = math.mod(10 - math.mod(checksum, 10), 10);                 
                 //system.debug('testNumberNew ist:' + testNumberNew);
                 
                 if (testNumberNew == long.ValueOf(testNumber))
                 {
                     isSerialNumber = true;
                 }          
             } // if (serialNo.length() == 20 || serialNo.length() == 22)

             // Preparation of serial numbers with length 30.
             // These need to be handled just like serial numbers with length 28
             // A logistics partner just added two digits for palette numbers without
             // talking to VAILLANT standard's board. 
             if (serialNo.length() == 30)
             {
                serialNo = serialNo.substring(0, 28);
             }

             //Test of serial numbers with length 28.
             if (serialNo.length() == 28)
             {
                 //system.debug('testNumber is:' + testNumber);
                 testNumber = serialNo.substring(serialNo.length()-1,serialNo.length());
                 //system.debug('testNumberString is:' + testNumber);
                 for (Integer i = 0; i <= serialNo.length()-2; i++)
                 {
                    //**
                    //* Current character of input string.
                    //**
                    //String actString = serialNo.substring(i,endPos);
                    String actString = serialNo.substring(i,i+1);
                    
                    //system.debug('Current substring of testNumberString is:' + actString);
                    //system.debug('Current ASCII-value of testNumberString is:' + ASCII (actString));
                    //Originalcode: checksum = checksum + i * (ASCII (SUBSTR (p_serialnr, i, 1)) - 32)
                    checksum = checksum + (i+1) * (ASCII(actString) - 32);
                    //system.debug('Current sum is:' + checksum);
                 }
                 //system.debug('The sum is:' + checksum);
                 
                 //Originalcode: checksum = checksum MOD 10;
                 testNumberNew = math.mod(checksum, 10);                 
                 //system.debug('testNumberNew ist:' + testNumberNew);
                 
                 //system.debug('Comparison between testNumberString, testNumberNew:' + testNumber + ' ' + testNumberNew);
                 if (testNumberNew == long.ValueOf(testNumber))
                 {
                    isSerialNumber = true;
                 }          
             } // if (serialNo.length() == 28)     
         return isSerialNumber;
         } // try
         catch(Exception e)  
         { 
             return isSerialNumber;  
         }*/ 
     }
    
     public static boolean isDouble(String input)  
     {  
        try  
        {  
             Double proofValue;
             //system.debug('Here comes the number test ...');
             proofValue = double.valueOf(input);
             return true;  
        }  
        catch(TypeException e)  
        {  
             return false;  
        }  
     }
     
     /**
      * Ascii character table with positions from 32 to 127, starts at 32.
      * For example ASCII(' ') returns 32, ASCII('u') returns Zero 117.
      *
      * @param  inputChar Input character.
      * @return charPosition Character position of input character in the ascii table.
      */
     public static Integer ASCII (String inputChar)
     {
          Integer charPosition = 0;
          List <String> ASCII = new String [] {' ','!','"','#','$','%','&',
          '\'','(',')','*','+',',','-','.','/','0','1',
          '2','3','4','5','6','7','8','9',':',';','<',
          '=','>','?','@','A','B','C','D','E','F','G',
          'H','I','J','K','L','M','N','O','P','Q','R','S',
          'T','U','V','W','X','Y','Z','[','\\',']','^','_',
          '`','a','b','c','d','e','f','g','h','i','j','k',
          'l','m','n','o','p','q','r','s','t','u','v','w',
          'x','y','z','{','|','}','~','DEL'};
          
          for (Integer i = 0; i<=95; i++)
          {  
             //search for input character in ASCII-array
             if (inputChar.equals(ASCII[i]))
             {
                 //add 32 positions of ASCII table 
                 charPosition = i;
                 //return i;
                 charPosition = charPosition + 32;
                 break;
             }
          }
          return charPosition;
     }
 
     /**
      * Set details for serial number
      * (articleNumber, ConstructionYear, constructionWeek).
      *
      * @param serialNo Input serial number.
      */      
     private Boolean extractSerialNumberDetails (String serialNo)
     {
         if (isValidSerialNumber(serialNo))
         {              
            /** 
             * If the length of the input number is 28,
             * the article number begins with the character at 7 and ends at 16. 
             * 10 digit or 6 digit material number SAP R/3. 
             * 6 digit numbers are left-aligned (e.g. 306304): 306304<<<<
             * 
             * Construction year: digits 3 and 4
             * Construction week: digits 5 and 6
             */ 
            if (serialNo.length() == 28)
            {
                if (serialNo.contains('<<<<'))
                {
                    articleNumber = serialNo.substring(6,12);
                }
                else if (serialNo.contains('<<')) {
                    articleNumber = serialNo.substring(6,14);
                }               
                else
                {
                    articleNumber = serialNo.substring(6,16);
                }
                constructionYear = serialNo.substring(2,4);
                constructionWeek = serialNo.substring(4,6);
            }
            
            /** 
             * If the length of the input number is 22,
             * the article number begins with the character at 5 
             * and ends at 12 (8 digits),
             * Construction year: digits 3 and 4.
             */ 
            else if (serialNo.length() == 22)
            {
                articleNumber = serialNo.substring(4,12);
                ConstructionYear = serialNo.substring(2,4);                  
            }
            
            /** 
             * If the length of the input number is 20,
             * the article number begins with the character at 3 and 
             * ends at 10 (8 digits),
             * Construction year: digits 1 and 2.
             */              
            else if (serialNo.length() == 20)
            {
                articleNumber = serialNo.substring(2,10);
                ConstructionYear = serialNo.substring(0,2);                   
            }   
            return true;              
         }//if (isValidSerialNumber(serialNo)
         else
         {
            return false;
         }
     }
     
     /**
      * Extraction of details from serial number.
      * 
      * @return serialNumberDetails <inputSerialNumber, 
      * (articleNumber, ConstructionYear, constructionWeek)>.
      */
     public Map <String, String> getSerialNumberDetails ()
     {
         //String articleNumber = 'Article number not found. Serial number is not valid.';
         //<articleNumber, ConstructionYear, constructionWeek>
         
        Map <String, String> serialNumberDetails = new Map <String, String> (); 

        serialNumberDetails.put('serialNumber', serialNumber);
        serialNumberDetails.put('articleNumber', articleNumber);
        serialNumberDetails.put('constructionYear', constructionYear);
        serialNumberDetails.put('constructionWeek', constructionWeek);         

        return serialNumberDetails;
     }
         
     /**
      * Extraction of details from serial number.
      * 
      * @param serialNo Input serial number.
      * @return serialNumberDetails <inputSerialNumber, 
      * (articleNumber, ConstructionYear, constructionWeek)>.
      */
     public Map <String, String> getSerialNumberDetails (String serialNo)
     {
        extractSerialNumberDetails(serialNo);
         
        Map <String, String> serialNumberDetails = new Map <String, String> (); 

        serialNumberDetails.put('serialNumber', serialNo);
        serialNumberDetails.put('articleNumber', articleNumber);
        serialNumberDetails.put('constructionYear', constructionYear);
        serialNumberDetails.put('constructionWeek', constructionWeek);         

        return serialNumberDetails;
     }
      
 }
