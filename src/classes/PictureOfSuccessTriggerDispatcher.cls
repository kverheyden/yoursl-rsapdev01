public with sharing class PictureOfSuccessTriggerDispatcher extends TriggerDispatcherBase{

	public override void beforeUpdate(TriggerParameters tp) {
		execute(new PictureOfSuccessTriggerHandler.PictureOfSuccessBUHandler(), tp, TriggerParameters.TriggerEvent.beforeUpdate);
	}

	public override void beforeInsert(TriggerParameters tp) {
		execute(new PictureOfSuccessTriggerHandler.PictureOfSuccessBIHandler(), tp, TriggerParameters.TriggerEvent.beforeInsert);
	}
}
