/*
 * @(#)SCExceptionTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCExceptionTest
{
    private static SCException exc;
    
    /**
     * Test the basic exception this class depends on
     */
    static testMethod void scExceptionTest1() {
        
        try
        {
            throw new SCException();
        }
        catch (SCException e)
        {
            // proper exception matched
            System.assert(true);    
        }
        catch (Exception e)
        {
            // wrong exception matched
            System.assert(false);
        }
    }

    /**
     * Test new constructor containing two arguments
     */
    static testMethod void scExceptionTest2() {
        
        try
        {
            throw new SCException(true, 'Message');
        }
        catch (SCException e)
        {
            // proper exception matched
            System.assert(true);    
        }
        catch (Exception e)
        {
            // wrong exception matched
            System.assert(false);
        }
    }
}
