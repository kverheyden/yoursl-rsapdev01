/*
 * @(#)SCTimereportViewControllerTest.cls
 *
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author Dietrich Herdt <dherdt@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCTimereportViewControllerTest
{
    private static SCTimereportViewController timereportViewController;

    /**
     * ItemEditController under positiv test 1
     */
    static testMethod void timereportViewControllerPositiv1()
    {
        SCHelperTestClass2.createTimeReportSet(true);

        timereportViewController = new SCTimereportViewController();

        // set the main parameter in controller: rid and day
        Datetime myDate = Datetime.newInstance(2010, 9, 1, 1, 0, 0);
        String myDateString = myDate.format('yyyyMMdd');
        //timereportViewController.rid = SCHelperTestClass.resources.get(0).Id;
        //timereportViewController.day = myDateString;
        timereportViewController.dayStartListString = SCHelperTestClass2.timeReportDay1.get(0).Id;
        //timereportViewController.getitemsEvaluation();
        timereportViewController.getMultiTimeReport();

        // the list should be not empty
        System.assertEquals(true, timereportViewController.getHasItems());

        // no errors in current time report
        System.assertEquals(false, timereportViewController.errorIntoTimeReport);

        // time report can be closed, no errors; the time report had index 0 in the map
        timereportViewController.timeReportIndex = 0;
        timereportViewController.closeTimeReport();
    }

    /**
     * ItemEditController under positiv test 2
     * Time report without errors, delete item method test.
     */
    static testMethod void timereportViewControllerPositiv2()
    {
        SCHelperTestClass2.createTimeReportSet(true);

        timereportViewController = new SCTimereportViewController();

        // set the main parameter in controller: rid and day
        Datetime myDate = Datetime.newInstance(2010, 9, 1, 1, 0, 0);
        String myDateString = myDate.format('yyyyMMdd');
        //timereportViewController.rid = SCHelperTestClass.resources.get(0).Id;
        //timereportViewController.day = myDateString;
        timereportViewController.dayStartListString = SCHelperTestClass2.timeReportDay1.get(0).Id;
        //timereportViewController.getitemsEvaluation();
        timereportViewController.getMultiTimeReport();

        // no errors in current time report
        System.assertEquals(false, timereportViewController.errorIntoTimeReport);

        // the list should be not empty
        System.assertEquals(true, timereportViewController.getHasItems());

        // set selected item, tid
        ApexPages.currentPage().getParameters().put('tid', SCHelperTestClass2.timeReportDay1.get(3).Id);
        System.debug('#### TID TEST is'+ SCHelperTestClass2.timeReportDay1.get(3).Id);

        // selectItem again, only for test
        timereportViewController.selectItem();
        // delete the selected item
        timereportViewController.deleteItem();

        // controlling, that item not exists in the list
    }

    /**
     * ItemEditController under positiv test 3
     * Time report without errors, delete "day end" method test.
     */
    static testMethod void timereportViewControllerPositiv3()
    {
        SCHelperTestClass2.createTimeReportSet(true);

        timereportViewController = new SCTimereportViewController();

        // set the main parameter in controller: rid and day
        //Datetime myDate = Datetime.newInstance(2010, 9, 2, 1, 0, 0);
        //String myDateString = myDate.format('yyyyMMdd');
        //timereportViewController.rid = SCHelperTestClass.resources.get(1).Id;
        //timereportViewController.day = myDateString;
        timereportViewController.dayStartListString = SCHelperTestClass2.timeReportDay1.get(4).Id;
        //timereportViewController.getitemsEvaluation();
        timereportViewController.getMultiTimeReport();

        // no errors in current time report
        System.assertEquals(false, timereportViewController.errorIntoTimeReport);

        // the list should be not empty
        System.assertEquals(true, timereportViewController.getHasItems());

        // set selected item, tid
        ApexPages.currentPage().getParameters().put('tid',
                            SCHelperTestClass2.timeReportDay1.get(6).Id);
        System.debug('#### TID TEST is'+
                            SCHelperTestClass2.timeReportDay1.get(6).Id);

        // selectItem again, only for test
        timereportViewController.selectItem();
        // delete the selected item
        timereportViewController.deleteItem();

        /* controlling, that item not exists in the list and all
         * over item have status open
         */
    }

    /**
     * ItemEditController under positiv test 4
     * Time report with errors, optimize method test.
     */
    static testMethod void timereportViewControllerPositiv4()
    {
        SCHelperTestClass2.createTimeReportSet(true);

        timereportViewController = new SCTimereportViewController();

        // set the main parameter in controller: rid and day
        //Datetime myDate = Datetime.newInstance(2010, 9, 3, 1, 0, 0);
        //String myDateString = myDate.format('yyyyMMdd');
        //timereportViewController.rid = SCHelperTestClass.resources.get(2).Id;
        //timereportViewController.day = myDateString;
        timereportViewController.dayStartListString = SCHelperTestClass2.timeReportDay1.get(9).Id;
        //timereportViewController.getitemsEvaluation();
        timereportViewController.getMultiTimeReport();


        // the list should be not empty
        System.assertEquals(true, timereportViewController.getHasItems());

        // errors in current time report
        System.assertEquals(true, timereportViewController.errorIntoTimeReport);

        // optimize current time report
        timereportViewController.optimize(SCHelperTestClass2.timeReportDay1.get(9).Id);

        // no errors after optimize
        System.assertEquals(false, timereportViewController.errorIntoTimeReport);
    }
}
