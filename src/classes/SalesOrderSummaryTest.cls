/**********************************************************************
Name:  SalesOrderSummaryTest
======================================================
Purpose:                                                            
Test class for SalesOrderSummary* classes                                                   
======================================================
History                                                            
-------                                                            
Date  	AUTHOR	DETAIL 
06/11/2014 Jan Mensfeld  
***********************************************************************/

@isTest
private class SalesOrderSummaryTest {
	static Account a;

	static testMethod void testSalesOrderSummaryBatch() {
        insertTestData();
		Test.startTest();
		SalesOrderSummaryBatch batch = new SalesOrderSummaryBatch();
		Database.executeBatch(batch);
		Test.stopTest();
	}
	
	static testMethod void testSalesOrderSummaryBatchScheduler() {
		Test.startTest();
		String chronExp = '0 0 * * * ?';
		String scheduleName = 'Test hourly salesOrderSummary';
		
		String sessionId = UserInfo.getSessionId();
		SalesOrderSummaryBatchScheduler schedule = new SalesOrderSummaryBatchScheduler(sessionId);
		
		System.schedule(scheduleName, chronExp, schedule);
		Test.stopTest();
	}
	
	
	
	static testMethod void testSalesOrderSummaryControllerWithoutParameter() {
		insertTestData();
		
		PageReference salesOrderSummaryPage = Page.SalesOrderSummary;
		Test.setCurrentPage(salesOrderSummaryPage);
		
		SalesOrderSummaryController soc = new SalesOrderSummaryController();
		System.assertEquals(null, soc.SalesOrders);
	}
	
	static testMethod void testSalesOrderSummaryControllerWithParameter() {
		
		insertTestData();
		
		PageReference salesOrderSummaryPage = Page.SalesOrderSummary;
		Test.setCurrentPage(salesOrderSummaryPage);
		ApexPages.currentPage().getParameters().put('supplierId', a.Id);
		
		SalesOrderSummaryController soc = new SalesOrderSummaryController();
		System.assertEquals(1, soc.SalesOrders.size());
	}
	
	static void insertTestData() {
		a = new Account();
		a.Name = 'Test Account';
		insert a;
		
		SalesOrder__c so = new SalesOrder__c();
		so.Account__c = a.Id;
        so.Supplier__c = a.ID;
       	so.SendToSupplierDate__c = System.now();
        so.Status__c = 'Active';
		so.RecordTypeId = Schema.SObjectType.SalesOrder__c.getRecordTypeInfosByName().get('Indirect Order').getRecordTypeId();
		insert so;
		
		SalesOrderItem__c soi = new SalesOrderItem__c();
		soi.Quantity__c = 2;
		soi.SalesOrder__c = so.Id;
		insert soi;
        
        Attachment att = new Attachment();
        att.ParentId = a.Id;
        att.Body = Blob.valueOf('test');
        att.Name = 'SalesOrderSummary';
        insert att;
	}
}
