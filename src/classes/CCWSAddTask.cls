/**********************************************************************
Name:  addTask()
======================================================
Purpose:                                                            
Represents a web service. Receives task information, create the task with the given info and inserts it to the database.                                                      
======================================================
History                                                            
-------                                                            
Date  	AUTHOR	DETAIL 
10/30/2013	Stephan Dieckmann, Alexander Placidi, Chris Sandra Schautt		INITIAL DEVELOPMENT           
***********************************************************************/


global with sharing class CCWSAddTask {
	
	WebService static OperationResult addTask(String sOwnerID, String sAccountID, String sSubject, String sDescription, Date dDate ){
		
		//required local variables
		List<Account> listAccounts = new List<Account>(); // accounts with the given id
		List<User> listUsers = new List<User>(); //users with the given id
		Task objTasktoInsert = new Task(); //the task to insert
		OperationResult objOpResult = new OperationResult();
		
		//+++++++++++++++++ STEP 1: Validate User ++++++++++++++++++++++++++++++++++++
		
		// String to hold the original User-ID of Salesforce.com
		String sUserSFDCId = '';
		
		//Query User and validate result
		try{
			listUsers = [SELECT Name, IsActive, Id, ID2__c FROM User WHERE ID2__c = : sOwnerID]; 
		}catch (DmlException e){
			objOpResult.addError(e.getMessage());
			return objOpResult;
		}
		// check, if list is empty
		if(listUsers.size()==0){
			objOpResult.addError('No User found for ID '+sOwnerID+'.');
			return objOpResult;
		}
		// Check, if we found to much Users
		else if(listUsers.size()>1){
			objOpResult.addError('Found '+listUsers.size()+' Users for StrOwnerID '+sOwnerID+'.');
			return objOpResult;
		}
		else{
			sUserSFDCId = listUsers[0].Id;
			objOpResult.addUser(sUserSFDCId);
		}	
		
		//+++++++++++++++++ STEP 2: Validate Account ++++++++++++++++++++++++++++++++++++
		
		// String to hold the original Account-ID of Salesforce.com
		String sAccountSFDCId = '';
		
		//Query Account and validate result
		try{
			listAccounts = [SELECT Name, Id, ID2__c FROM Account WHERE ID2__c = : sAccountID];
		}catch (DmlException e){
			objOpResult.addError(e.getMessage());
			return objOpResult;
		}
		// check, if list is empty
		if(listAccounts.size()==0){
			objOpResult.addError('No Account found for ID '+sAccountID+'.');
			return objOpResult;
		}
		// Check, if we found to many Accounts
		else if(listAccounts.size()>1){
			objOpResult.addError('Found '+listAccounts.size()+' Accounts for StrAccountID '+sAccountID+'.');
			return objOpResult;
		}
		else{
			sAccountSFDCId = listAccounts[0].Id;
			objOpResult.addUser(sAccountSFDCId);
		}	

		
		//+++++++++++++++++ STEP 3: Create Task ++++++++++++++++++++++++++++++++++++
				
		//set required fields
		objTasktoInsert.OwnerId = listUsers[0].Id;
		objTasktoInsert.WhatId = listAccounts[0].Id;
		objTasktoInsert.Subject = sSubject;
		objTasktoInsert.Description = sDescription;
		objTasktoInsert.ActivityDate = dDate;
		
		//insert or update task
		try{
			upsert objTasktoInsert;
		}
		catch (DmlException e){
			objOpResult.addError(e.getMessage());
			return objOpResult;
		}
		
		// return value if success
		objOpResult.bSuccess = true;
	
		return objOpResult;	
	}
	
	
	/************************************************************************
	**
	**	Class to prepare and return a reult to the caller
	**
	************************************************************************/
	global class OperationResult{
		
		// Class variables
		webservice String MessageUUID{get;set;} 
		webservice String sAccountID{get;set;}
		webservice String sUserID{get;set;}
		webservice String sError{get;set;}
		webservice Boolean bSuccess{get;set;}
		
		// Constructor
		private OperationResult(){
			// MessageUUID='5257AA3FC97C0FE0E10080000A621094'; 
		}
		
		public boolean addUser(String sUserID){
			this.sUserID = sUserID;
			return true;
		}
		
		public boolean addAccount(String sAccountID){
			this.sAccountID = sAccountID;
			return true;
		}
		
		public boolean addError(String sError){
			this.sError = sError;
			this.bSuccess = false;
			return true;
		}
	}
}
