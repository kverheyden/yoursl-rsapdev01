public with sharing class cc_cceag_dao_User 
{
    private static final string CSR_PORTAL_USER_ID_QUERY = 'portalUser';
    
    public static string getFlowUserWithContext(ccrz.cc_RemoteActionContext ctx)
    {
        String userId = null;
        if (string.isNotBlank(ctx.portalUserId)) {
        	userId =  ctx.portalUserId;
        }else{
            userId = getFlowUser();
        }
        return userId;
    }
    
    public static string getFlowUser() 
    {
    	string usr = UserInfo.getUserId();
		if(Apexpages.currentPage() != null) {
			string portalUserId = Apexpages.currentPage().getParameters().get(CSR_PORTAL_USER_ID_QUERY);
			if(string.isNotBlank(portalUserId)) {
				usr = portalUserId;
				//XSS prevention
				usr = usr.escapeHtml4();
			}
		}
		return usr;
	}
}
