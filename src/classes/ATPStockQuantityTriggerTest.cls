@isTest
public class ATPStockQuantityTriggerTest 
{
    /**
     * @author thomas richter <thomas@meformobile.com>
     */
    static testMethod void testTrigger()
    {
        // trigger should look on product and populate corresponding cc product based on MatNr/SKU (Product2.ID2__c = ccrz__E_Product__c.ccrz__ProductId__c)
        
        Product2 product = new Product2(ID2__c='MatNr',Name='MyProduct');
        insert product;
        ccrz__E_Product__c ccProduct = new ccrz__E_Product__c(ccrz__ProductId__c='MatNr',Name='CCMyProduct', ccrz__SKU__c='MatNr',NumberOfSalesUnitsPerPallet__c=1);
        insert ccProduct;
        
        Test.startTest();

        ATPStockQuantity__c quant = new ATPStockQuantity__c();
        quant.Product2__c = product.Id;

		insert quant;        
        
        quant = [SELECT Id,ccrz_product__c FROM ATPStockQuantity__c WHERE Id=:quant.Id];
        System.assertEquals(ccProduct.Id, quant.ccrz_product__c, 'ccrz_product__c should reference the corresponding cloudcraze product');
        
        Test.stopTest();
    }
}
