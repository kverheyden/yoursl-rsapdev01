/*
 * @(#)SCAddContractItemControllerTest.cls
 *
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCAddContractItemControllerTest
{

    private static SCAddContractItemController aci;
    
    static testMethod void addContractItemControllerPositiv1()
    {

        aci = new SCAddContractItemController();
        
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.createInstalledBaseLocation(true);
        SCHelperTestClass.createInstalledBase(true);
        SCHelperTestClass.createProductModel(true);
        
        System.debug('#### installedBaseMy' + SCHelperTestClass.installedBaseSingle.id);
        
        SCHelperTestClass2.createContractTemplates(true);
        SCHelperTestClass2.createContract('Contract', SCHelperTestClass2.insuranceTemplate.Id, SCHelperTestClass.account.Id, SCHelperTestClass.installedBaseSingle.Id, true);
        
        ApexPages.currentPage().getParameters().put('id', SCHelperTestClass2.contracts.get(0).Id);
        
        aci = new SCAddContractItemController();
        
        aci.contractId = SCHelperTestClass2.contracts.get(0).Id;
        aci.contractno = [select name from SCContract__c where id = :aci.contractId].name;
        
        aci.getIBL();
        aci.cancelButton();
        
        aci.newSelectedItems = String.valueOf(SCHelperTestClass.installedBaseSingle.id);
        aci.saveItems();
        
    }
}
