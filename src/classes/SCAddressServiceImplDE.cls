/*
 * @(#)SCAddressServiceImplDE.cls 
 * 
 * Copyright 2011 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Implements the address validation service that accesses 
 * the google maps api. This service is activated
 * depending on the country by SCAddressValidation.
 *
 *    SCAddressValidation v = new SCAddressValidation();
 *    List<AvsAddress> foundaddr;
 *    foundaddr = v.check('de', '91207 Lauf Weigmannstr 2');
 *    System.debug('result:' + foundaddr);
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
*/
public virtual class SCAddressServiceImplDE implements SCAddressService
{
    public static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
    
    /*
     * Return the signed url.
     *
     * @param   url   the original url
     * @param   id    a client id (optional, only for testing)
     * @param   key   a private key (optional, only for testing)
     *
     * @return    the signed url
     */
    private String getSignedUrl(String url)
    {
        return getSignedUrl(url, null, null);
    } // getSignedUrl
    public String getSignedUrl(String url, String id, String key)
    {
        System.debug('#### SCAddressServiceImplDE.getSignedUrl()');
        String signedUrl = url;
        String clientID = SCbase.isSet(id) ? id : appSettings.GOOGLE_GEOCODE_CLIENTID__c;
        if(clientID != null && clientID != '')
        {
            signedUrl += '&client=' + clientID;
            // System.debug('#### SCAddressServiceImplDE.getSignedUrl(): url -> ' + signedUrl);
            
            URL urlObj = new URL(signedUrl);
            
            // Convert the private key id from 'web safe' base 64 to binary
            String privateKey = SCbase.isSet(key) ? key : appSettings.GOOGLE_GEOCODE_PRIVATE_KEY__c;
            if(privateKey != null && privateKey != '')
            {
                privateKey = privateKey.replace('-', '+').replace('_', '/');
                privateKey = privateKey.replace('_', '/');
                Blob decodeKey = EncodingUtil.base64Decode(privateKey);
        
                // System.debug('#### SCAddressServiceImplDE.getSignedUrl(): path + query -> ' + 
                //              urlObj.getPath() + '?' + urlObj.getQuery());
                Blob mac = Crypto.generateMac('HMacSHA1', 
                                              Blob.valueOf(urlObj.getPath() + '?' + urlObj.getQuery()),
                                              decodeKey);
        
                // base 64 encode the binary signature
                String signature = EncodingUtil.base64Encode(mac);
                
                // convert the signature to 'web safe' base 64
                signature = signature.replace('+', '-');
                signature = signature.replace('/', '_');      
                
                signedUrl += '&signature=' + signature;
            }        
        }    
        // System.debug('#### SCAddressServiceImplDE.getSignedUrl(): signedUrl -> ' + signedUrl);
        return signedUrl;
    } // getSignedUrl
    
    /*
     * Create the address string for the url.
     *
     * @param    addr    address object that contains the address
     *                   to be checked
     *
     * @return    the address string
     */
    public String createAddrUrl(AvsAddress addr)
    {
        System.debug('#### SCAddressServiceImplDE.createAddrUrl()');
        String addrUrl = '';
        if (SCBase.isSet(addr.street))
        {
            addrUrl += addr.street;
        }
        if (SCBase.isSet(addr.housenumber))
        {
            if (addrUrl.length() > 0)
            {
                addrUrl += '+';
            }
            addrUrl += addr.housenumber;
        }
        if (addrUrl.length() > 0)
        {
            addrUrl += ',';
        }
        if (SCBase.isSet(addr.postalcode))
        {
            addrUrl += addr.postalcode;
        }
        if (SCBase.isSet(addr.city))
        {
            if (addrUrl.length() > 0)
            {
                addrUrl += '+';
            }
            addrUrl += addr.city;
        }
        if ((addrUrl.length() > 0) && (addrUrl.substring(addrUrl.length()-1) != ','))
        {
            addrUrl += ',';
        }
        if (SCBase.isSet(addr.country))
        {
            addrUrl += addr.country;
        }
        System.debug('#### SCAddressServiceImplDE.createAddrUrl(): addrUrl -> ' + addrUrl);
        return EncodingUtil.urlEncode(addrUrl, 'UTF-8');
    } // createAddrUrl
    
    /*
     * Retrieves the status string from the http request.
     *
     * @param    root    the rot xml node of the result
     *
     * @return    the address string
     */
    private String getStatus(Dom.XMLNode root)
    {
        System.debug('#### SCAddressServiceImplDE.getStatus()');
        String status = '';
        Dom.XmlNode[] rootChilds = root.getChildElements();
        
        for (Integer i=0, cnt1=rootChilds.size(); i<cnt1; i++)
        {
            if (rootChilds[i].getName() == 'status')
            {
                status = rootChilds[i].getText();
                break;
            } // if (rootChilds[i].getName() == 'status')
        } // for (Integer i=0, cnt1=rootChilds.size(); i<cnt1; i++)
        System.debug('#### SCAddressServiceImplDE.getStatus(): status -> ' + status);
        return status;
    } // getStatus
    
    /*
     * Retrieves the results from the http request.
     *
     * @param    root    the rot xml node of the result
     *
     * @return   a list of xml nodes
     */
    private List<Dom.XMLNode> getResults(Dom.XMLNode root)
    {
        System.debug('#### SCAddressServiceImplDE.getResults()');
        List<Dom.XMLNode> resultNodes = new List<Dom.XMLNode>();
        Dom.XmlNode[] rootChilds = root.getChildElements();
        
        // iterate over all childs of the root element
        for (Integer i=0, cnt1=rootChilds.size(); i<cnt1; i++)
        {
            // if we have found a result element
            if (rootChilds[i].getName() == 'result')
            {
                Dom.XmlNode[] resultChilds = rootChilds[i].getChildElements();
        
                // iterate over all childs of the result element
                for (Integer j=0, cnt2=resultChilds.size(); j<cnt2; j++)
                {
                    // if we have found the type element
                    if (resultChilds[j].getName() == 'type')
                    {
                        String resultType = resultChilds[j].getText();
                        System.debug('#### SCAddressServiceImplDE.getResults(): resultType -> ' + resultType);
                        // check the type
                        if ((resultType == 'route') || (resultType == 'street_address')
                            || (resultType == 'locality'))
                        {
                            // add only elements with street info to the list
                            resultNodes.add(rootChilds[i]);
                        } // if ((resultType == 'route') || (resultType == 'street_address'))
                        break;
                    } // if (resultChilds[j].getName() == 'type')
                } // for (Integer j=0, cnt2=resultChilds.size(); j<cnt2; j++)
            } // if (rootChilds[i].getName() == 'result')
        } // for (Integer i=0, cnt1=rootChilds.size(); i<cnt1; i++)
        System.debug('#### SCAddressServiceImplDE.getResults(): resultNodes no -> ' + resultNodes.size());
        return resultNodes;
    } // getResults
    
    /*
     * Extracts the address data from the xml node list
     * into the avsresult.
     *
     * @param    postalCode     the postalcode of the search address
     * @param    city           the city of the search address
     * @param    resultNodes    the xml node list for input
     * @param    result         the avsresult
     */
    private void getAddresses(String postalCode, String city, 
                              List<Dom.XMLNode> resultNodes, AvsResult result)
    {
        System.debug('#### SCAddressServiceImplDE.getAddresses()');
        String compType = '';
        String compTextS = '';
        String compTextL = '';
        String locType = '';
        
        for (Dom.XMLNode resultAddr :resultNodes)
        {
            Boolean itemAdd = false;
            AvsAddress item = new AvsAddress();
            item.countryState   = ''; // not supported by google gecoding api
            item.district       = ''; // not supported by google gecoding api
            item.nofrom         = ''; // not supported by google gecoding api
            item.noto           = ''; // not supported by google gecoding api
            Dom.XmlNode[] resultChilds = resultAddr.getChildElements();
    
            // iterate over all childs of the result element
            for (Integer i=0, cnt1=resultChilds.size(); i<cnt1; i++)
            {
                // System.debug('#### SCAddressServiceImplDE.getAddresses(): resultChild -> ' + 
                //               resultChilds[i].getName());
                // if we have found an address component
                if (resultChilds[i].getName() == 'address_component')
                {
                    Dom.XmlNode[] compChilds = resultChilds[i].getChildElements();
                    compType = '';
                    compTextS = '';
                    compTextL = '';
            
                    for (Integer j=0, cnt2=compChilds.size(); j<cnt2; j++)
                    {
                        if ((compChilds[j].getName() == 'type') && 
                            (compChilds[j].getText() != 'political'))
                        {
                            compType = compChilds[j].getText();
                        } // if ((compChilds[j].getName() == 'type') && ...
                        else if (compChilds[j].getName() == 'long_name')
                        {
                            compTextL = compChilds[j].getText();
                        } // if (compChilds[j].getName() == 'long_name')
                        else if (compChilds[j].getName() == 'short_name')
                        {
                            compTextS = compChilds[j].getText();
                        } // if (compChilds[j].getName() == 'short_name')
                    } // for (Integer j=0, cnt2=compChilds.size(); j<cnt2; j++)
                    
                    // System.debug('#### SCAddressServiceImplDE.getAddresses(): compType  -> ' + compType);
                    // System.debug('#### SCAddressServiceImplDE.getAddresses(): compTextS -> ' + compTextS);
                    // System.debug('#### SCAddressServiceImplDE.getAddresses(): compTextL -> ' + compTextL);
                    // transfer the result into the return object
                    if ((compType == 'route') || (compType == 'street_address'))
                    {
                        item.street = compTextL;
                    }
                    else if (compType == 'street_number')
                    {
                        item.housenumber = compTextL;
                    }
                    else if (compType == 'locality')
                    {
                        item.city = compTextL;
                        // we add the item to the list only if the 
                        // city of the current result starts
                        // with the city of the search address
                        // or if no city is given
                        if ((!SCBase.isSet(city) || 
                             (SCBase.isSet(city) && compTextL.startsWith(city))) && 
                            !itemAdd)
                        {
                            result.items.add(item);
                            itemAdd = true;
                        }
                    }
                    else if (compType == 'administrative_area_level_1')
                    {
                        item.county = compTextL;
                    }
                    else if (compType == 'postal_code')
                    {
                        item.postalcode = compTextL;
                        // we add the item to the list only if the 
                        // postalcode of the current result starts
                        // with the postalcode of the search address
                        // or if no postalcode is given
                        if ((!SCBase.isSet(postalCode) || 
                             (SCBase.isSet(postalCode) && compTextL.startsWith(postalCode))) && 
                            !itemAdd)
                        {
                            result.items.add(item);
                            itemAdd = true;
                        }
                    }
                    else if (compType == 'country')
                    {
                        item.country = compTextS;
                    }
                } // if (resultChilds[i].getName() == 'address_component')
                // if we have found a geometry component
                else if (resultChilds[i].getName() == 'geometry')
                {
                    Dom.XmlNode[] geoChilds = resultChilds[i].getChildElements();
            
                    for (Integer j=0, cnt2=geoChilds.size(); j<cnt2; j++)
                    {
                        // we have found the loaction infos
                        if (geoChilds[j].getName() == 'location')
                        {
                            Dom.XmlNode[] locChilds = geoChilds[j].getChildElements();
                    
                            for (Integer k=0, cnt3=locChilds.size(); k<cnt3; k++)
                            {
                                if (locChilds[k].getName() == 'lat')
                                {
                                    item.geoY = Double.valueOf(locChilds[k].getText());
                                } // if (locChilds[k].getName() == 'lat')
                                else if (locChilds[k].getName() == 'lng')
                                {
                                    item.geoX = Double.valueOf(locChilds[k].getText());
                                } // if (locChilds[k].getName() == 'lng')
                            } // for (Integer k=0, cnt3=locChilds.size(); k<cnt3; k++)
                            break;
                        } // if (geoChilds[j].getName() == 'location')
                        else if (geoChilds[j].getName() == 'location_type')
                        {
                            locType = geoChilds[j].getText();
                            item.geoApprox = !SCBase.isSet(locType) || 
                                             !locType.equals('ROOFTOP');
                        } // else if (geoChilds[j].getName() == 'location_type')
                    } // for (Integer j=0, cnt2=geoChilds.size(); j<cnt2; j++)
                } // else if (resultChilds[i].getName() == 'geometry')
            } // for (Integer i=0, cnt1=resultChilds.size(); i<cnt1; i++)
        } // for (Dom.XMLNode resultAddr :resultNodes)
        System.debug('#### SCAddressServiceImplDE.getAddresses(): result -> ' + result);
    } // getAddresses
    
    /**
     * Validate the address and return the result set using
     * the google geocoding api.
     * 
     * @param    addr    address object that contains the address
     *                   to be checked
     *
     * @return    the result object with the addresses
     */
    public virtual AvsResult check(AvsAddress addr)
    {
        AvsResult result = new AvsResult();
        try
        {
            Dom.Document doc;
            if ((addr.matchInfo != null) && (addr.matchInfo == 'test'))
            {
                doc = new Dom.Document();
                doc.load(getXmlTestString());
            }
            else
            {
                // call the web service
                String language = UserInfo.getLanguage();
                language = language.substring(0, 2);
                Http http = new Http();
                HttpRequest req = new HttpRequest();
                String url = appSettings.GOOGLE_GEOCODE_ENDPOINT__c + '/xml?';
//                url += 'language=' + language + 
//                       '&sensor=false&address=';
                url += 'region=' + (SCBase.isSet(addr.country) ? addr.country : 'de') + 
                       '&language=' + language + 
                       '&channel=SCAddressServiceImplDE' +
                       '&sensor=false&address=';
                url += createAddrUrl(addr);
                System.debug('#### SCAddressServiceImplDE.check(): url -> ' + url);
                req.setEndpoint(getSignedUrl(url));
                req.setMethod('GET');
                HttpResponse res = http.send(req);
                
                doc = res.getBodyDocument();
            }
            
            // Retrieve the root element for this document.
            Dom.XMLNode root = doc.getRootElement();
            System.debug('### result root: ' + root);
            
            // get the status
            String status = getStatus(root);
            if (status == 'OK')
            {
                System.debug('### status: OK');
                List<Dom.XMLNode> resultNodes = getResults(root);
                getAddresses(addr.postalcode, addr.city, resultNodes, result);
            } // if (status == 'OK')
            else if (status == 'ZERO_RESULTS')
            {
                result.status = AvsResult.STATUS_EMPTY;
                result.statusInfo = System.Label.SC_app_AddressValidationEmpty;
                /*
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 
                                                                System.Label.SC_app_AddressValidationEmpty);
                ApexPages.addMessage(myMsg);
                */
                return result;
            } // else if (status == 'ZERO_RESULTS')
            else
            {
                result.status = AvsResult.STATUS_ERROR;
                result.statusInfo = convertStatus(status);
                /*
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 
                                                                result.statusInfo);
                ApexPages.addMessage(myMsg);
                */
                return result;
            }

            System.debug('#### SCAddressServiceImplDE.check(): items size -> ' + result.items.size());
            // now set the status values
            if (result.items.size() == 1)
            {
                result.status = AvsResult.STATUS_EXACT;
            } 
            else if (result.items.size() > 1)
            {
                result.status = AvsResult.STATUS_MULTI;
            }
            else
            {
                // It is possible, that the service retrieves results, but
                // they has been deleted, because they have not the correct 
                // postalcode (see method getResults). 
                // So the service status is ok, but no results.
                // Therefore we change the status here.
                status = 'ZERO_RESULTS';
                result.status = AvsResult.STATUS_BLANK;
            }
            // set the originial status info of the service
            result.statusInfo = convertStatus(status); 
        }
        catch(Exception e)
        {
            result.status = AvsResult.STATUS_ERROR;
            result.statusInfo = '';
            if(e instanceof SCfwException)
            {
                result.statusInfo += e.getMessage();    
            }
            String msg = SCfwException.getExceptionInfo(e);
            result.statusInfo += msg;
            System.debug('#### Exception: ' + msg);
        }
        System.debug('#### SCAddressServiceImplDE.check(): result -> ' + result);
        return result;
    } // check

    /**
     * Determines the longitude and latitude for an address.
     * Only required if the external address validation service 
     * does not deliver the geocodes in the addres validateion.
     * 
     * @param addr address object with all needed data
     * @return the address with the longitude and latitude
     */
    public virtual AvsResult geocode(AvsAddress addr)
    {
        addr.geox = 0;
        addr.geoy = 0;

        // do the same as on checking the address
        AvsResult result = check(addr);
        return result;        
    } // geocode

    /*
     * Converts the status string into a readable string.
     *
     * @param status    the status
     *
     * @return string containing the status string plus english description.
     */
    private String convertStatus(String status)
    {
        String result = 'Google Geocoding API - ';
        if (SCbase.isSet(status))
        {
            if (status == 'OK')
            {
                //result += '[OK] Ok';
                result += System.Label.SC_app_AddressValidationOk;
            } 
            else if (status == 'ZERO_RESULTS')
            {
                //result += '[ZERO_RESULTS] No results found';
                result += System.Label.SC_app_AddressValidationEmpty;
            } 
            else if (status == 'OVER_QUERY_LIMIT')
            {
                //result += '[OVER_QUERY_LIMIT] The limit of send queries for today has been reached';
                result += System.Label.SC_app_AddressValidationQueryLimit;
            }
            else if (status == 'REQUEST_DENIED')
            {
                //result += '[REQUEST_DENIED] The request has been denied, check the parameters';
                result += System.Label.SC_app_AddressValidationRequestDenied;
            }
            else if (status == 'INVALID_REQUEST')
            {
                //result += '[INVALID_REQUEST] The request is invalid, check the parameters';
                result += System.Label.SC_app_AddressValidationInvalidRequest;
            }
            else
            {
                //result += '[UNKNOWN] The request retrieves an unknown status';
                result += System.Label.SC_app_AddressValidationUnknownStatus;
            }
        }
        return result;
    } // convertStatus 

    /*
     * Converts the status string into a readable string.
     *
     * @param status    the status
     *
     * @return string containing the status string plus english description.
     */
    private String getXmlTestString()
    {
        String xmlTestString = '';
        
        xmlTestString += '<GeocodeResponse>';
        xmlTestString += ' <status>OK</status>';
        xmlTestString += ' <result>';
        xmlTestString += '  <type>street_address</type>';
        xmlTestString += '  <formatted_address>Weigmannstraße 33, 91207 Lauf, Deutschland</formatted_address>';
        xmlTestString += '  <address_component>';
        xmlTestString += '   <long_name>33</long_name>';
        xmlTestString += '   <short_name>33</short_name>';
        xmlTestString += '   <type>street_number</type>';
        xmlTestString += '  </address_component>';
        xmlTestString += '  <address_component>';
        xmlTestString += '   <long_name>Weigmannstraße</long_name>';
        xmlTestString += '   <short_name>Weigmannstraße</short_name>';
        xmlTestString += '   <type>route</type>';
        xmlTestString += '  </address_component>';
        xmlTestString += '  <address_component>';
        xmlTestString += '   <long_name>Lauf</long_name>';
        xmlTestString += '   <short_name>Lauf</short_name>';
        xmlTestString += '   <type>sublocality</type>';
        xmlTestString += '   <type>political</type>';
        xmlTestString += '  </address_component>';
        xmlTestString += '  <address_component>';
        xmlTestString += '   <long_name>Lauf</long_name>';
        xmlTestString += '   <short_name>Lauf</short_name>';
        xmlTestString += '   <type>locality</type>';
        xmlTestString += '   <type>political</type>';
        xmlTestString += '  </address_component>';
        xmlTestString += '  <address_component>';
        xmlTestString += '   <long_name>Nürnberger Land</long_name>';
        xmlTestString += '   <short_name>Nürnberger Land</short_name>';
        xmlTestString += '   <type>administrative_area_level_2</type>';
        xmlTestString += '   <type>political</type>';
        xmlTestString += '  </address_component>';
        xmlTestString += '  <address_component>';
        xmlTestString += '   <long_name>Bayern</long_name>';
        xmlTestString += '   <short_name>BY</short_name>';
        xmlTestString += '   <type>administrative_area_level_1</type>';
        xmlTestString += '   <type>political</type>';
        xmlTestString += '  </address_component>';
        xmlTestString += '  <address_component>';
        xmlTestString += '   <long_name>Deutschland</long_name>';
        xmlTestString += '   <short_name>DE</short_name>';
        xmlTestString += '   <type>country</type>';
        xmlTestString += '   <type>political</type>';
        xmlTestString += '  </address_component>';
        xmlTestString += '  <address_component>';
        xmlTestString += '   <long_name>91207</long_name>';
        xmlTestString += '   <short_name>91207</short_name>';
        xmlTestString += '   <type>postal_code</type>';
        xmlTestString += '  </address_component>';
        xmlTestString += '  <geometry>';
        xmlTestString += '   <location>';
        xmlTestString += '    <lat>49.5079800</lat>';
        xmlTestString += '    <lng>11.2868300</lng>';
        xmlTestString += '   </location>';
        xmlTestString += '   <location_type>ROOFTOP</location_type>';
        xmlTestString += '   <viewport>';
        xmlTestString += '    <southwest>';
        xmlTestString += '     <lat>49.5048324</lat>';
        xmlTestString += '     <lng>11.2836824</lng>';
        xmlTestString += '    </southwest>';
        xmlTestString += '    <northeast>';
        xmlTestString += '     <lat>49.5111276</lat>';
        xmlTestString += '     <lng>11.2899776</lng>';
        xmlTestString += '    </northeast>';
        xmlTestString += '   </viewport>';
        xmlTestString += '  </geometry>';
        xmlTestString += '  <partial_match>true</partial_match>';
        xmlTestString += ' </result>';
        xmlTestString += '</GeocodeResponse>';
        return xmlTestString;
    } // getXmlTestString
}
