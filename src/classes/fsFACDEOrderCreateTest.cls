/**
* @author           Oliver Preuschl
*
* @description      Testclass for fsFACDEOrderCreate
*
* @date             25.08.2014
*
* Timeline:
* Name              DateTime                Version        Description
* Oliver Preuschl   25.08.2014              *1.0*          Created
*/

@isTest
private class fsFACDEOrderCreateTest {
  
  public static List< SCOrder__c > GL_SCOrders;

  @isTest static void generateSCOrdersFROMCDEOrders() {
    //Load User
    User LO_User1 = loadUser( '1' );
    User LO_User2 = loadUser( '2' );

    System.runAs( LO_User1 ){

      //Load SCApplicationSettings
      loadApplicationSettings();

      //Load SCBusinessUnits
      loadBusinessUnits();

      //Load SCPricelists
      loadPricelists();

      //Load Accounts
      List< Account > LL_Accounts = loadAccounts();
      insert( LL_Accounts );

      //Load CDEOrders
      List< CDEOrder__c > LL_CDEOrders = loadCDEOrders();
      for( CDEOrder__c LO_CDEOrder: LL_CDEOrders ){
        LO_CDEOrder.Account__c = LL_Accounts.get( 0 ).Id;
      }
      insert( LL_CDEOrders );

      //Load CDEOrderItems
      List< SCProductModel__c > LL_ProductModels = loadProductModels();
      insert( LL_ProductModels );

      //Load CDEOrderItems
      List< CDEOrderItem__c > LL_CDEOrderItems = loadCDEOrderItems();
      for( CDEOrderItem__c LO_CDEOrderItem: LL_CDEOrderItems ){
        LO_CDEOrderItem.CDEOrder__c = LL_CDEOrders.get( 0 ).Id;
        LO_CDEOrderItem.ProductModel__c = LL_ProductModels.get( 0 ).Id;
      }
      insert( LL_CDEOrderItems );

    }
    System.runAs( LO_User2 ){

      Test.startTest();

      //Check initial status of CDEOrderItems
      List< CDEOrderItem__c > LL_CDEOrderItems = [ SELECT SCOrder__c FROM CDEOrderItem__c ];
      for( CDEOrderItem__c LO_CDEOrderItem: LL_CDEOrderItems ){
        System.assertEquals( null, LO_CDEOrderItem.SCOrder__c );
      }

      //Trigger SCOrder creation
      List< CDEOrder__c > LL_CDEOrders = [ SELECT Send_to_SAP__c, OwnerId FROM CDEOrder__c ];
      LL_CDEOrders.get( 0 ).Send_to_SAP__c = true;
      update( LL_CDEOrders );

      Test.stopTest();

      //List< SCInterfaceLog__c > LL_SPs = [ SELECT Id, ResultInfo__c FROM SCInterfaceLog__c ];
      //System.debug( System.LoggingLevel.Error, 'LL_SPsCount: ' + LL_SPs.size() );
      //System.debug( System.LoggingLevel.Error, 'LL_SPs: ' + LL_SPs.get( 0 ).ResultInfo__c );

      //Check again, SCOrders should have been created
      LL_CDEOrderItems = [ SELECT SCOrder__c FROM CDEOrderItem__c WHERE SCOrder__c != null ];
      List< CDEOrderItem__c > LL_CDEOrderItemsDebug = [ SELECT Id FROM CDEOrderItem__c WHERE ( Status__c = 'Processing Error' ) ];
      List< SCORder__c > LL_SCOrders = [ SELECT Id, Name, OwnerId, SalesArea__c FROM SCOrder__c ];
      List< SCInterfaceLog__c > LL_SCInterfaceLogs = [ SELECT Name, Start__c, Stop__c, Interface__c, InterfaceHandler__c, ResultInfo__c, Data__c FROM SCInterfaceLog__c WHERE ( ResultCode__c = 'E101' ) ORDER BY CreatedDate DESC ];
      System.debug( LL_SCInterfaceLogs );
      System.assert( LL_CDEOrderItems.size() == 3, 'Only ' + LL_CDEOrderItems.size() + '/3 SCOrder were related to the CDEOrderItems. ' + LL_CDEOrderItemsDebug.size() + ' CDEOrderItem Processing Errors. SCOrders: ' + LL_SCOrders + ', Logs: ' + LL_SCInterfaceLogs );
      for( SCORder__c LO_SCOrder: LL_SCOrders ){
        System.assertEquals( LO_User1.Id, LO_SCOrder.OwnerId );
        System.assertEquals( LO_User1.ERPSalesArea__c, LO_SCOrder.SalesArea__c );
      }
    }

  }

  @isTest static void generateSCOrderFROMCDEOrderAndSendToSAPAsyncAll() {
    //Load User
    User LO_User = loadUser( '1' );

    System.runAs( LO_User ){

      //Load SCApplicationSettings
      loadApplicationSettings();

      //Load SCBusinessUnits
      loadBusinessUnits();

      //Load SCPricelists
      loadPricelists();

      //Load Accounts
      List< Account > LL_Accounts = loadAccounts();
      insert( LL_Accounts );

      //Load CDEOrders
      List< CDEOrder__c > LL_CDEOrders = loadCDEOrders();
      for( CDEOrder__c LO_CDEOrder: LL_CDEOrders ){
        LO_CDEOrder.Account__c = LL_Accounts.get( 0 ).Id;
      }
      insert( LL_CDEOrders );

      //Load CDEOrderItems
      List< SCProductModel__c > LL_ProductModels = loadProductModels();
      insert( LL_ProductModels );

      //Load CDEOrderItems
      List< CDEOrderItem__c > LL_CDEOrderItems = loadCDEOrderItems();
      LL_CDEOrderItems.get( 0 ).CDEOrder__c = LL_CDEOrders.get( 0 ).Id;
      LL_CDEOrderItems.get( 0 ).ProductModel__c = LL_ProductModels.get( 0 ).Id;
      insert( LL_CDEOrderItems.get( 0 ) );

      Test.startTest();

      //Check initial status of CDEOrderItems
      LL_CDEOrderItems = [ SELECT SCOrder__c FROM CDEOrderItem__c WHERE ( Id IN: LL_CDEOrderItems ) ];
      for( CDEOrderItem__c LO_CDEOrderItem: LL_CDEOrderItems ){
        System.assertEquals( null, LO_CDEOrderItem.SCOrder__c );
      }

      //Trigger SCOrder creation
      LL_CDEOrders.get( 0 ).Send_to_SAP__c = true;
      update( LL_CDEOrders );

      SCbtcCDEOrderToSAPScheduler LO_CDEOrderToSAPScheduler = new SCbtcCDEOrderToSAPScheduler();
      String LV_Schedule = '0 0 23 * * ?';
      System.schedule( 'Test CDEOrderToSAPScheduler', LV_Schedule, LO_CDEOrderToSAPScheduler );

      Test.stopTest();

      //List< SCInterfaceLog__c > LL_SPs = [ SELECT Id, ResultInfo__c FROM SCInterfaceLog__c ];
      //System.debug( System.LoggingLevel.Error, 'LL_SPsCount: ' + LL_SPs.size() );
      //System.debug( System.LoggingLevel.Error, 'LL_SPs: ' + LL_SPs.get( 0 ).ResultInfo__c );

      //Check again, SCOrders should have been created
      LL_CDEOrderItems = [ SELECT SCOrder__c FROM CDEOrderItem__c WHERE SCOrder__c != null ];
      List< CDEOrderItem__c > LL_CDEOrderItemsDebug = [ SELECT Id FROM CDEOrderItem__c WHERE ( Status__c = 'Processing Error' ) ];
      List< SCORder__c > LL_SCOrdersDebug = [ SELECT Id, Name FROM SCOrder__c ];
      List< SCInterfaceLog__c > LL_SCInterfaceLogs = [ SELECT Name, Start__c, Stop__c, Interface__c, InterfaceHandler__c, ResultInfo__c, Data__c FROM SCInterfaceLog__c WHERE ( ResultCode__c = 'E101' ) ORDER BY CreatedDate DESC ];
      System.debug( LL_SCInterfaceLogs );
      System.assert( LL_CDEOrderItems.size() == 1, 'Only ' + LL_CDEOrderItems.size() + '/1 SCOrder were related to the CDEOrderItems. ' + LL_CDEOrderItemsDebug.size() + ' CDEOrderItem Processing Errors. SCOrders: ' + LL_SCOrdersDebug + ', Logs: ' + LL_SCInterfaceLogs );

      //SCbtcCDEOrderToSAP.asyncTransferAll( 0, 'trace' );

      List< SCOrder__c > LL_SendSCOrders = [ SELECT Id FROM SCORder__c WHERE ( ERPStatusOrderCreate__c = 'none' ) ];
      System.assertEquals( 1, LL_SendSCOrders.size() );

    }

  }

  @isTest static void generateSCOrderFROMCDEOrderAndSendToSAPAsyncAllWithCondition() {
    //Load User
    User LO_User = loadUser( '1' );

    System.runAs( LO_User ){

      //Load SCApplicationSettings
      loadApplicationSettings();

      //Load SCBusinessUnits
      loadBusinessUnits();

      //Load SCPricelists
      loadPricelists();

      //Load Accounts
      List< Account > LL_Accounts = loadAccounts();
      insert( LL_Accounts );

      //Load CDEOrders
      List< CDEOrder__c > LL_CDEOrders = loadCDEOrders();
      for( CDEOrder__c LO_CDEOrder: LL_CDEOrders ){
        LO_CDEOrder.Account__c = LL_Accounts.get( 0 ).Id;
      }
      insert( LL_CDEOrders );

      //Load CDEOrderItems
      List< SCProductModel__c > LL_ProductModels = loadProductModels();
      insert( LL_ProductModels );

      //Load CDEOrderItems
      List< CDEOrderItem__c > LL_CDEOrderItems = loadCDEOrderItems();
      LL_CDEOrderItems.get( 0 ).CDEOrder__c = LL_CDEOrders.get( 0 ).Id;
      LL_CDEOrderItems.get( 0 ).ProductModel__c = LL_ProductModels.get( 0 ).Id;
      insert( LL_CDEOrderItems.get( 0 ) );

      Test.startTest();

      //Check initial status of CDEOrderItems
      LL_CDEOrderItems = [ SELECT SCOrder__c FROM CDEOrderItem__c WHERE ( Id IN: LL_CDEOrderItems ) ];
      for( CDEOrderItem__c LO_CDEOrderItem: LL_CDEOrderItems ){
        System.assertEquals( null, LO_CDEOrderItem.SCOrder__c );
      }

      //Trigger SCOrder creation
      LL_CDEOrders.get( 0 ).Send_to_SAP__c = true;
      update( LL_CDEOrders );

      Test.stopTest();

      //List< SCInterfaceLog__c > LL_SPs = [ SELECT Id, ResultInfo__c FROM SCInterfaceLog__c ];
      //System.debug( System.LoggingLevel.Error, 'LL_SPsCount: ' + LL_SPs.size() );
      //System.debug( System.LoggingLevel.Error, 'LL_SPs: ' + LL_SPs.get( 0 ).ResultInfo__c );

      //Check again, SCOrders should have been created
      LL_CDEOrderItems = [ SELECT SCOrder__c FROM CDEOrderItem__c WHERE SCOrder__c != null ];
      List< CDEOrderItem__c > LL_CDEOrderItemsDebug = [ SELECT Id FROM CDEOrderItem__c WHERE ( Status__c = 'Processing Error' ) ];
      List< SCOrder__c > LL_SCOrdersDebug = [ SELECT Id, Name FROM SCOrder__c ];
      List< SCInterfaceLog__c > LL_SCInterfaceLogs = [ SELECT Name, Start__c, Stop__c, Interface__c, InterfaceHandler__c, ResultInfo__c, Data__c FROM SCInterfaceLog__c WHERE ( ResultCode__c = 'E101' ) ORDER BY CreatedDate DESC ];
      System.debug( LL_SCInterfaceLogs );
      System.assert( LL_CDEOrderItems.size() == 1, 'Only ' + LL_CDEOrderItems.size() + '/1 SCOrder were related to the CDEOrderItems. ' + LL_CDEOrderItemsDebug.size() + ' CDEOrderItem Processing Errors. SCOrders: ' + LL_SCOrdersDebug + ', Logs: ' + LL_SCInterfaceLogs );

      SCbtcCDEOrderToSAP.asyncTransferAll( 0, 'trace', 'Id = \'' + LL_CDEOrderItems.get( 0 ).SCOrder__c + '\'' );
    }

  }

  @isTest static void generateSCOrderFROMCDEOrderAndSendToSAPAsync() {
    //Load User
    User LO_User = loadUser( '1' );

    System.runAs( LO_User ){

      //Load SCApplicationSettings
      loadApplicationSettings();

      //Load SCBusinessUnits
      loadBusinessUnits();

      //Load SCPricelists
      loadPricelists();

      //Load Accounts
      List< Account > LL_Accounts = loadAccounts();
      insert( LL_Accounts );

      //Load CDEOrders
      List< CDEOrder__c > LL_CDEOrders = loadCDEOrders();
      for( CDEOrder__c LO_CDEOrder: LL_CDEOrders ){
        LO_CDEOrder.Account__c = LL_Accounts.get( 0 ).Id;
      }
      insert( LL_CDEOrders );

      //Load CDEOrderItems
      List< SCProductModel__c > LL_ProductModels = loadProductModels();
      insert( LL_ProductModels );

      //Load CDEOrderItems
      List< CDEOrderItem__c > LL_CDEOrderItems = loadCDEOrderItems();
      LL_CDEOrderItems.get( 0 ).CDEOrder__c = LL_CDEOrders.get( 0 ).Id;
      LL_CDEOrderItems.get( 0 ).ProductModel__c = LL_ProductModels.get( 0 ).Id;
      insert( LL_CDEOrderItems.get( 0 ) );

      Test.startTest();

      //Check initial status of CDEOrderItems
      LL_CDEOrderItems = [ SELECT SCOrder__c FROM CDEOrderItem__c WHERE ( Id IN: LL_CDEOrderItems ) ];
      for( CDEOrderItem__c LO_CDEOrderItem: LL_CDEOrderItems ){
        System.assertEquals( null, LO_CDEOrderItem.SCOrder__c );
      }

      //Trigger SCOrder creation
      LL_CDEOrders.get( 0 ).Send_to_SAP__c = true;
      update( LL_CDEOrders );

      Test.stopTest();

      //List< SCInterfaceLog__c > LL_SPs = [ SELECT Id, ResultInfo__c FROM SCInterfaceLog__c ];
      //System.debug( System.LoggingLevel.Error, 'LL_SPsCount: ' + LL_SPs.size() );
      //System.debug( System.LoggingLevel.Error, 'LL_SPs: ' + LL_SPs.get( 0 ).ResultInfo__c );

      //Check again, SCOrders should have been created
      LL_CDEOrderItems = [ SELECT SCOrder__c FROM CDEOrderItem__c WHERE SCOrder__c != null ];
      List< CDEOrderItem__c > LL_CDEOrderItemsDebug = [ SELECT Id FROM CDEOrderItem__c WHERE ( Status__c = 'Processing Error' ) ];
      List< SCOrder__c > LL_SCOrdersDebug = [ SELECT Id, Name FROM SCOrder__c ];
      List< SCInterfaceLog__c > LL_SCInterfaceLogs = [ SELECT Name, Start__c, Stop__c, Interface__c, InterfaceHandler__c, ResultInfo__c, Data__c FROM SCInterfaceLog__c WHERE ( ResultCode__c = 'E101' ) ORDER BY CreatedDate DESC ];
      System.debug( LL_SCInterfaceLogs );
      System.assert( LL_CDEOrderItems.size() == 1, 'Only ' + LL_CDEOrderItems.size() + '/1 SCOrder were related to the CDEOrderItems. ' + LL_CDEOrderItemsDebug.size() + ' CDEOrderItem Processing Errors. SCOrders: ' + LL_SCOrdersDebug + ', Logs: ' + LL_SCInterfaceLogs );

      SCbtcCDEOrderToSAP.asyncTransfer( LL_CDEOrderItems.get( 0 ).SCOrder__c, 0, 'trace' );
    }

  }

  @isTest static void generateSCOrderFROMCDEOrderAndSendToSAPsync() {
    //Load User
    User LO_User = loadUser( '1' );

    System.runAs( LO_User ){

      //Load SCApplicationSettings
      loadApplicationSettings();

      //Load SCBusinessUnits
      loadBusinessUnits();

      //Load SCPricelists
      loadPricelists();

      //Load Accounts
      List< Account > LL_Accounts = loadAccounts();
      insert( LL_Accounts );

      //Load CDEOrders
      List< CDEOrder__c > LL_CDEOrders = loadCDEOrders();
      for( CDEOrder__c LO_CDEOrder: LL_CDEOrders ){
        LO_CDEOrder.Account__c = LL_Accounts.get( 0 ).Id;
      }
      insert( LL_CDEOrders );

      //Load CDEOrderItems
      List< SCProductModel__c > LL_ProductModels = loadProductModels();
      insert( LL_ProductModels );

      //Load CDEOrderItems
      List< CDEOrderItem__c > LL_CDEOrderItems = loadCDEOrderItems();
      LL_CDEOrderItems.get( 0 ).CDEOrder__c = LL_CDEOrders.get( 0 ).Id;
      LL_CDEOrderItems.get( 0 ).ProductModel__c = LL_ProductModels.get( 0 ).Id;
      insert( LL_CDEOrderItems.get( 0 ) );

      Test.startTest();

      //Check initial status of CDEOrderItems
      LL_CDEOrderItems = [ SELECT SCOrder__c FROM CDEOrderItem__c WHERE ( Id IN: LL_CDEOrderItems ) ];
      for( CDEOrderItem__c LO_CDEOrderItem: LL_CDEOrderItems ){
        System.assertEquals( null, LO_CDEOrderItem.SCOrder__c );
      }

      //Trigger SCOrder creation
      LL_CDEOrders.get( 0 ).Send_to_SAP__c = true;
      update( LL_CDEOrders );

      Test.stopTest();

      //List< SCInterfaceLog__c > LL_SPs = [ SELECT Id, ResultInfo__c FROM SCInterfaceLog__c ];
      //System.debug( System.LoggingLevel.Error, 'LL_SPsCount: ' + LL_SPs.size() );
      //System.debug( System.LoggingLevel.Error, 'LL_SPs: ' + LL_SPs.get( 0 ).ResultInfo__c );

      //Check again, SCOrders should have been created
      LL_CDEOrderItems = [ SELECT SCOrder__c FROM CDEOrderItem__c WHERE SCOrder__c != null ];
      List< CDEOrderItem__c > LL_CDEOrderItemsDebug = [ SELECT Id FROM CDEOrderItem__c WHERE ( Status__c = 'Processing Error' ) ];
      List< SCOrder__c > LL_SCOrdersDebug = [ SELECT Id, Name FROM SCOrder__c ];
      List< SCInterfaceLog__c > LL_SCInterfaceLogs = [ SELECT Name, Start__c, Stop__c, Interface__c, InterfaceHandler__c, ResultInfo__c, Data__c FROM SCInterfaceLog__c WHERE ( ResultCode__c = 'E101' ) ORDER BY CreatedDate DESC ];
      System.debug( LL_SCInterfaceLogs );
      System.assert( LL_CDEOrderItems.size() == 1, 'Only ' + LL_CDEOrderItems.size() + '/1 SCOrder were related to the CDEOrderItems. ' + LL_CDEOrderItemsDebug.size() + ' CDEOrderItem Processing Errors. SCOrders: ' + LL_SCOrdersDebug + ', Logs: ' + LL_SCInterfaceLogs );

      SCbtcCDEOrderToSAP.syncTransfer( LL_CDEOrderItems.get( 0 ).SCOrder__c, 0, 'trace' );
    }

  }

  private static User loadUser( String PV_Number ){
    Profile LO_Profile = [ SELECT Id FROM Profile WHERE Name = 'Systemadministrator' OR Name = 'System Administrator' ]; 
    User LO_User = new User(
        Alias = 'hwtcu' + PV_Number, 
        Email = 'test13579' + PV_Number + '@testorg.com', 
        EmailEncodingKey = 'UTF-8', 
        LastName = 'Testing' + PV_Number, 
        LanguageLocaleKey = 'en_US', 
        LocaleSidKey = 'en_US', 
        ProfileId = LO_Profile.Id, 
        TimeZoneSidKey = 'America/Los_Angeles', 
        UserName = 'test13579' + PV_Number + '@testorg.com',
        ERPSalesArea__c = '123' + PV_Number
      );
      insert( LO_User );
      return LO_User;
  }

  public static List<List<String>> parseCSV(String contents,Boolean skipHeaders) {
    List<List<String>> allFields = new List<List<String>>();

    // replace instances where a double quote begins a field containing a comma
    // in this case you get a double quote followed by a doubled double quote
    // do this for beginning and end of a field
    contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
    // now replace all remaining double quotes - we do this so that we can reconstruct
    // fields with commas inside assuming they begin and end with a double quote
    contents = contents.replaceAll('""','DBLQT');
    // we are not attempting to handle fields with a newline inside of them
    // so, split on newline to get the spreadsheet rows
    List<String> lines = new List<String>();
    try {
      lines = contents.split('\n');
    } catch (System.ListException e) {
      System.debug('Limits exceeded?' + e.getMessage());
    }
    Integer num = 0;
    for(String line : lines) {
      // check for blank CSV lines (only commas)
      if (line.replaceAll(',','').trim().length() == 0) break;
      
      List<String> fields = line.split(',');  
      List<String> cleanFields = new List<String>();
      String compositeField;
      Boolean makeCompositeField = false;
      for(String field : fields) {
        if (field.startsWith('"') && field.endsWith('"')) {
          cleanFields.add(field.replaceAll('DBLQT','"'));
        } else if (field.startsWith('"')) {
          makeCompositeField = true;
          compositeField = field;
        } else if (field.endsWith('"')) {
          compositeField += ',' + field;
          cleanFields.add(compositeField.replaceAll('DBLQT','"'));
          makeCompositeField = false;
        } else if (makeCompositeField) {
          compositeField +=  ',' + field;
        } else {
          cleanFields.add(field.replaceAll('DBLQT','"'));
        }
      }
      
      allFields.add(cleanFields);
    }
    if (skipHeaders) allFields.remove(0);
    return allFields;    
  }

  //Load ApplicationSettings Testdata
  private static List< SCApplicationSettings__c > loadApplicationSettings(){
    //List< SCApplicationSettings__c > LL_ApplicationSettings = Test.loadData( SCApplicationSettings__c.SObjectType, 'fsFATestDataSCApplicationSettings' );
    String fieldNameStr = 'ACCOUNT_RECORDTYPE_NAME__c,"ADDRESS_SERVICE_PW__c","ADDRESS_SERVICE_QAS_PW__c","ADDRESS_SERVICE_QAS_U__c","ADDRESS_SERVICE_U__c","ADDRESS_SERVICE_WSNL_PW__c","ADDRESS_SERVICE_WSNL_U__c","ADD_REPAIRCODE_TEXT__c","ALLOWED_TO_CREATE_NEW_CUSTOMER__c","ARTICLESEARCH_FROMSTOCK_AUTOSEARCH__c","ASE_CALLASYNC__c","ASE_ENDPOINT_URL__c","ASE_RELEASEJOB_CALENDAR__c","ASE_RELEASEJOB_DAYSTOPREVIEW__c","ASE_RELEASEJOB_DAYSTORELEASE__c","ASE_RELEASEJOB_DEPARTMENT__c","ASE_RELEASEJOB_ORDERTYPES__c","ASE_RELEASEJOB_SETLOCKS__c","ASE_TOUROPTIMIZERJOB_DEPARTMENT__c","ASE_TOUROPTIMIZERJOB_DURATION__c","ASE_TOUROPTIMIZERJOB_HINT__c","ASE_TOUROPTIMIZERJOB_TIMESPAN__c","ASE_USE_NOT_EXTCALL__c","BATCH_GEOCODE_WEB_CALLS_DAILY_MAXIMUM__c","BTC_BATCH_SIZE_GEOCODE_ACCOUNT__c","BTC_BATCH_SIZE_GEOCODE_INSTBAS_LOCATION__c","BTC_BATCH_SIZE_GEOCODE_RESOURCES__c","CONTRACT_ACCOUNTING_COSTS_PER_MINUTE__c","CONTRACT_ACCOUNTING_COSTS_TRAVEL__c","CONTRACT_BTCS_MAX_CYCLES__c","CONTRACT_DISPATCH_PREFSTART_THRESHOLD__c","CONTRACT_FIELD_NAME__c","CONTRACT_SCHEDULING_DAYS__c","CONTR_VALIDATE_EXCEPT_COMPANIES__c","CREDITNOTE_ARTNO_TRANSACTION_FEE__c","CurrencyIsoCode","CUSTOMERPROPERTY_NOPARKING_FOLLOWUP1__c","CUSTOMERPROPERTY_NOPARKING_FOLLOWUP2__c","CUSTOMERPROPERTY_NOPERMIT_FOLLOWUP1__c","CUSTOMERPROPERTY_NOPERMIT_FOLLOWUP2__c","CUSTOMERPROPERTY__c","CUSTOMORDERPAGE_GB__c","DEFAULT_ARTICLESEARCH_RESULT_LIMT__c","DEFAULT_BRAND__c","DEFAULT_BUSINESSUNITNO__c","DEFAULT_CHANNEL__c","DEFAULT_COMBOXSTATUS__c","DEFAULT_COUNTRY__c","DEFAULT_CUSTOMERPREFINTERVALL__c","DEFAULT_CUSTOMERPREFSTART__c","DEFAULT_CUSTOMERPRIO__c","DEFAULT_CUSTOMERTIMEWINDOW__c","DEFAULT_DISTANCEZONE__c","DEFAULT_FAILURETYPE__c","DEFAULT_INBOX_ORDERFILTER__c","DEFAULT_INBOX_TIMEFILTER__c","DEFAULT_INVOICINGSUBTYPE__c","DEFAULT_INVOICINGTYPE__c","DEFAULT_MAINTENANCE_ARTICLE_NO__c","DEFAULT_ORDERITEM_DURATION__c","DEFAULT_ORDERREASON__c","DEFAULT_ORDERTYPE__c","DEFAULT_PAYMENTTYPE__c","DEFAULT_PLANT__c","DEFAULT_PRICELIST__c","DEFAULT_REPLENISHMENT_TYPE__c","DEFAULT_ROUNDMINUTES_GRID__c","DEFAULT_SERIALNUMBERTIMER__c","DEFAULT_WARRANTY_CALLTYPE__c","DISABLEMESSAGES__c","DISABLETRIGGER_ORDERAFTERUPDATE__c","DISABLETRIGGER__c","DISABLE_BRAND_IN_ARTICLESEARCH__c","DISTANCE_FACTOR__c","DISTANCE_UNIT__c","EMAIL_SERVICE_REPORT_REPLY_TO__c","EMAIL_SERVICE_REPORT_SENDERDISPLAYNAME__c","EMAIL_SERVICE_REPORT_TEMPLATE__c","ENGINEERSTANDBY_EVALUATION__c","FOLLOW_UP_ACTION_CLOSE_ORDER__c","GOOGLE_GEOCODE_CLIENTID__c","GOOGLE_GEOCODE_ENDPOINT__c","GOOGLE_GEOCODE_PRIVATE_KEY__c","ID2__c","INSTBASE_INSTDATE_DAYSINFUTURE__c","INSTBASE_INSTDATE_EARLIESTDATE__c","INSTBASE_MODIFY_TYPE__c","INSTBASE_PURCHDATE_DAYSINFUTURE__c","INSTBASE_PURCHDATE_EARLIESTDATE__c","INSURANCE_HOMESERVE_ENDPOINT__c","INSURANCE_HOMESERVE_FOLLOWUP2__c","INSURANCE_HOMESERVE_FOLLOWUP__c","INSURANCE_HOMESERVE_MODE__c","INSURANCE_HOMESERVE_PWD__c","INSURANCE_HOMESERVE_USER__c","INTERFACE_CLEARING__c","INVENTORY_MAX_QUANTITY__c","INVOICINGTYPE_PAYCOSTS__c","IS_EXTERNAL_DISPATCHER__c","LOCKTYPE_IMAGES__c","MAPVIEW_MAXSEARCHRESULTS__c","MATERIALAVAILABILITY_ACCNUMBER__c","MATERIALAVAILABILITY_SERVICE_ENDPOINT__c","MATERIALAVAILABILITY_SERVICE_MODE__c","MATERIALAVAILABILITY_SERVICE_PWD__c","MATERIALAVAILABILITY_SERVICE_TYPE__c","MATERIALAVAILABILITY_SERVICE_USER__c","MATERIALAVAILABILITY__c","MATERIAL_BOOKING__c","MATERIAL_ENABLE_DELIVCONF__c","MATERIAL_REORDER_LOCKTYPE__c","MESSAGEONTODAY__c","MESSAGE_EXISTS_GATEWAY__c","MOBILE_USER__c","Name","NAMESPACE_PREFIX__c","NOTIFICATION_CODE7_AMOUNT__c","NOTIFICATION_OVERWRITE_PASS__c","ORDERCREATION_ALTERNATIVEAPPOINTMENTS__c","ORDERCREATION_APPOINTMENTWARNING__c","ORDERCREATION_COMBOX__c","ORDERCREATION_CUSTPREFSTARTS__c","ORDERCREATION_MINDURATION__c","ORDERCREATION_OPENINSTALLEDBASE__c","ORDERCREATION_ORDTYPES_FOR_CASH_PAYMENT__c","ORDERCREATION_PAGE__c","ORDERCREATION_PRIO_DETERMINATION_DEBUG__c","ORDERCREATION_PRIO_DETERMINATION__c","ORDERCREATION_SHOW_ERRSYMS__c","ORDERCREATION_SHOW_HISTORY__c","ORDERCREATION_USE_PRODUCTSKILL__c","ORDERFEEDBACK_ARTNO_TRAVELTIME__c","ORDERFEEDBACK_ARTNO_WORKTIME__c","ORDERFEEDBACK_AUTOPARTBOOKING__c","ORDERFEEDBACK_AUTOREPLENISHMENTTYPE__c","ORDERFEEDBACK_FOLLOWUP1__c","ORDERFEEDBACK_FOLLOWUP2__c","ORDERFEEDBACK_TIMEREPORT__c","ORDERLINE_ACCESS_RIGHTS__c","ORDERLINE_ARTNO_PAYMENT__c","ORDERLINE_QTY_THRESHOLD__c","ORDERLINE_VIEW_TYPES__c","OrderRoleMode__c","ORDERSEARCH_CUSTOMORDERPAGE__c","ORDERSEARCH_MAXRESULTS__c","ORDER_FIELD_NAME__c","PAYMENTSERVICE_ENDPOINT__c","PAYMENTSERVICE_FOLLOWUP2__c","PAYMENTSERVICE_FOLLOWUP__c","PAYMENTSERVICE_MODE__c","PAYMENTSERVICE_PWD__c","PAYMENTSERVICE_TYPE__c","POSTCODEAREA_EVALUATION__c","PRICING_MAX_DISCOUNT__c","PRINTINGTEMPLATE_CONTRACT__c","PRINTINGTEMPLATE_ITBTEMPLATE_INSTALLED__c","PRINTINGTEMPLATE_ORDER__c","QUALIFICATION_CREATE__c","QUALIFICATION_DISABLEBRAND__c","QUALIFICATION_DISABLELANGUAGE__c","QUALIFICATION_DISABLEORDTYPE__c","QUALIFICATION_UNITCLASSEXCEPTIONS__c","QUALIFICATION_UNITTYPEEXCEPTIONS__c","QUALIFICATION_USE_ACCS_SCHEDULING__c","RESOURCE_CHANGE_ALLOWED__c","RISKCLASS_IMAGES__c","SCHEDULING_MULTIPLE_APPOINTMENTS__c","SEARCHLOG_MINTIME__c","SetupOwnerId","SHOW_PRICING__c","SHOW_REPLENISHMENT_TYPE__c","STOCK_ALLOWNEGATIVESTOCK__c","STOCK_ITEM_FILTER_EMPTY__c","TAXACCOUNT__c","TIMEREPORT_CHRONOLOGICAL__c","TIMEREPORT_DEFAULTDURATION__c","TIMEREPORT_EDITCLOSED__c","TIMEREPORT_MAXDURATION__c","TIMEREPORT_MAXFUTURE__c","TIMEREPORT_MAXOPENDAYS__c","TIMEREPORT_MULTIPERDAY__c","TIMEREPORT_OPENDAYBUTTON__c","TIMEREPORT_OPENDAYSINTHEPAST__c","TIMEREPORT_OPTIMIZEBUTTON__c","TIMEREPORT_PAST_PROCESS__c","VALIDATE_PICKLIST_VALUES__c","VIPLEVEL_IMAGES__c","WizardPersonAccount__c","WORKSHOPAPP_ARTICLESEARCH_RESULT_LIMIT__c","WORKSHOPAPP_CREATE_LOGS__c","WORKSHOPAPP_SHOW_LAST_APPOINTMENT__c","WORKSHOPAPP_UPDATE_URL__c","WORKSHOPAPP_VERSION__c"';
        String fieldValueStr = ',"","","","","","","","","true","0.0","","","","","","","false","","","","","","30000.0","","","","","","0.0","","","7.0","","","EUR","2855","2955","2855","2956","0","","500.0","","1","","","DE","7.0","0.0","2101","12401","","","5501,5503,5509,5510","11","180001","170001","","60.0","","5701","13202","DE Plant","Standard","375002","15.0","3000.0","","","false","false","","1.0","","","","","false","","","","","","","","","","","","","","PRODUCTIVE","","","","","","","","100.0","","","PROD","","SAP","","","","true","","false","","false","a01c0000005PYrY","","","","","","","","15.0","1.0","","","false","0.0","true","true","0.0","","","2.0","375001","","","true","","","9.0","","CASR","","300.0","","","","","","","","true","100.0","Wartung_Serienbrief","","Service_DE_Order","PRODUCTMODEL","false","false","false","","","1.0","","","1","1000.0","00Dc0000001xwfnEAA","0.0","0.0","true","false","","false","","false","","","","false","true","false","false","false","0","","true","100.0","","10.0","",""';
        List<String> fields = fsfacdeOrderCreateTest.parseCSV(fieldNameStr, false).get(0);
        List<String> values = fsfacdeOrderCreateTest.parseCSV(fieldValueStr, false).get(0);
        
        system.assertEquals(fields.size(), values.size());
        SCApplicationSettings__c rec = new SCApplicationSettings__c();
        for(Integer i = 0 ; i< fields.size(); i++){
            String field = fields[i].replace('"','');
            String value = values[i].replace('"','');
            if(value == null || value == '')
                continue;
            if(value.toUpperCase() == 'TRUE')
                rec.put(field, true);
            else if(value.toUpperCase() == 'FALSE')
                rec.put(field, false);
            else {
                try{
                    Object o = Integer.valueOf(value);
                    if(o instanceof Integer){
                        rec.put(field, (Integer) o);
                    }
                }catch(Exception e){
                    rec.put(field, value);
                }
            }
        }
        rec.SetupOwnerId = UserInfo.getOrganizationId();
        insert rec;
        
        return new List<SCApplicationSettings__c>{rec};
  }

  //Load BusinessUnit Testdata
  private static List< SCBusinessUnit__c > loadBusinessUnits(){
    //List< SCBusinessUnit__c > LL_BusinessUnits = Test.loadData( SCBusinessUnit__c.SObjectType, 'fsFATestDataSCBusinessUnits' );
    //return LL_BusinessUnits;
        
        SCCalendar__c cal = new SCCalendar__c();
        cal.Name = 'Test cal';
        cal.country__c = 'DE';
        insert cal;
        
        SCBusinessUnit__c unit = new SCBusinessUnit__c();
        unit.Calendar__c = cal.ID;
        unit.GeoApprox__c = false;
        unit.ID2__c = 'DE Plant';
        unit.Name = '359';
        unit.Name__c = '359';
        unit.Operational__c = false;
        unit.Type__c = '5907';
        insert unit;
        return new List<SCBusinessUnit__c>{unit};
  }

  //Load Pricelist Testdata
  private static List< SCPricelist__c > loadPricelists(){
    List< SCPricelist__c > LL_Pricelists = Test.loadData( SCPricelist__c.SObjectType, 'fsFATestDataSCPricelists' );
    return LL_Pricelists;
  }

  //Load Account Testdata
  private static List< Account > loadAccounts(){
    List< Account > LL_Accounts = new List< Account >();
    Account LO_Account = new Account(
        Name        =  'TestAccount 1',
        AccountNumber    =  '1'
      );
    LL_Accounts.add( LO_Account );
    return LL_Accounts;
  }

  //Load CDEOrder Testdata
  private static List< CDEOrder__c > loadCDEOrders(){
    List< CDEOrder__c > LL_CDEOrders = new List< CDEOrder__c >();
    CDEOrder__c LO_CDEOrder = new CDEOrder__c();
    LL_CDEOrders.add( LO_CDEOrder );
    return LL_CDEOrders;
  }

  //Load CDEOrderItem Testdata
  private static List< CDEOrderItem__c > loadCDEOrderItems(){
    List< CDEOrderItem__c > LL_CDEOrderItems = new List< CDEOrderItem__c >();
    for( Integer i = 0; i < 10; i++ ){
      CDEOrderItem__c LO_CDEOrderItem = new CDEOrderItem__c(
          Quantity__c        =  10
        );
      LL_CDEOrderItems.add( LO_CDEOrderItem );
    }
    return LL_CDEOrderItems;
  }

  //Load ProductModel Testdata
  private static List< SCProductModel__c > loadProductModels(){
    List< SCProductModel__c > LL_ProductModels = new List< SCProductModel__c >();
    SCProductModel__c LO_ProductModel = new SCProductModel__c(
        Name          =  'Cooler',
        ID2__c          =  'D036900_4712'
      );
    LL_ProductModels.add( LO_ProductModel );
    return LL_ProductModels;
  }



    public static List< SCOrder__c > getExistingSCOrders(){
        //Get Object Type
        Schema.SObjectType LO_SCOrderType = SCOrder__c.getSObjectType();
        Schema.SObjectType LO_SCOrderItemType = SCOrderItem__c.getSObjectType();
        //Get Field Names
        Set<String> LL_SCOrderFields = LO_SCOrderType.getDescribe().fields.getMap().keySet();
        Set<String> LL_SCORderItemFields = LO_SCOrderItemType.getDescribe().fields.getMap().keySet();
        
        //Generate SCOrder Field String
        String LV_SCOrderFieldString = '';
        String LV_Separator = '';
        for(String LV_Field: LL_SCOrderFields){
            LV_SCOrderFieldString += LV_Separator + LV_Field;
            if (LV_Separator == '') LV_Separator = ',';
        }

        //Generate SCOrderItem Field String
        String LV_SCOrderItemFieldString = '';
        LV_Separator = '';
        for(String LV_Field: LL_SCORderItemFields){
            LV_SCOrderItemFieldString += LV_Separator + LV_Field;
            if (LV_Separator == '') LV_Separator = ',';
        }
        
        //Get record with all fields
        String LV_QueryString = 'SELECT ' + LV_SCOrderFieldString + ', ( SELECT ' + LV_SCOrderItemFieldString + ' FROM OrderItem__r ) FROM SCOrder__c';
        
        List< SCOrder__c > LL_SCOrders = Database.query(LV_QueryString);
               
        return LL_SCOrders;
        
    }
   
}
