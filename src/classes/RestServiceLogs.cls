public with sharing class RestServiceLogs{
	
	public static final String ERROR = 'Error';
	public static final String WARNING = 'Warning';
	public static final String SUCCESS = 'Success';
	private static final String DEBUG_IS_ACTIVE = 'DebugIsActive';
	
	public Datetime webRequestStart { get; private set; }
	public RestRequest webRequest { get; private set; }
	public RestResponse webResponse { get; private set; }
			
	public WSLogs__c log { get; private set; }
	
	public RestServiceLogs() {
		webRequestStart = System.now();
		log = new WSLogs__c();
	}
	
	public RestServiceLogs(Object obj){
		webRequestStart = System.now();
		log = new WSLogs__c();
		log.RequestBody__c = JSON.serialize(obj);
	}
	
	public void insertLog(String status) {
		if (!debugIsActive())
			return;
		
		webRequest = RestContext.request;
		webResponse = RestContext.response;
		
		log.HttpMethod__c = webRequest.httpMethod;
		log.URI__c = getRequestUrl();
		log.Source__c = 'Internal';
		if (webRequest.RequestBody != null)
			log.RequestBody__c = webRequest.RequestBody.toString();
		log.Status__c = status;
		log.RemoteAddress__c = webRequest.remoteAddress;
		log.RawResponse__c = getResponseBody();
		log.ResponseTime__c = System.now().getTime() - webRequestStart.getTime();
		log.ServiceName__c = getServiceName();
		
		insert log;
		insertLogAttachment();
	}
	
	private static Boolean debugIsActive() {
		try {
			System.debug('JM Debug Webservicelog active');
			WSDebugSettings__c debugSetting = WSDebugSettings__c.getValues(DEBUG_IS_ACTIVE);
			return debugSetting.Value__c == '1';			
		} catch (Exception e) {
			System.debug(e.getTypeName() + ': ' + e.getMessage());
			System.debug('JM Debug Webservicelog inactive');
			return false;
		}
	}
	
	private String getRequestUrl() {
		String uri = webRequest.requestURI;
		String parameters = getParameters();

		return uri + parameters;
	}
	
	private String getParameters() {
		Map<String, String> mapParameters = webRequest.params;
		List<String> listParameters = new List<String>();
		for (String s : mapParameters.keySet())
			listParameters.add(s + '=' + mapParameters.get(s));
		if (listParameters.size() > 0 )
			return '?' + String.join(listParameters, '&');
		return '';
	}
	
	private String getResponseBody() {
		String responseBody = webResponse.ResponseBody.toString();
		if (responseBody.length() > 32000)
			return responseBody.subString(0, 30000) + '[...]';
		return responseBody;
	}
	
	private void insertLogAttachment() {
		Attachment attachment = new Attachment();
		attachment.Body = webResponse.responseBody;
		attachment.ParentId = log.Id;
		attachment.ContentType = 'text/plain';
		attachment.Name = 'Raw Response.txt';
		insert attachment;
	}
	
	private static String getServiceName() {
		List<String> wsURIParts = RestContext.Request.RequestURI.split('/');
		if (wsURIParts.isEmpty())
			return '';
		return wsURIParts[wsURIParts.size() - 1];
	}
}
