/*
 * @(#)SCReplenishmentExtensionTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 * <p>
 * This class contains unit tests for replenishment.
 * A detailed test for managing assocs will be done in 
 * SCTestReplenishment.cls
 * <p>
 * A complete testing environment is created first. Remember that all data
 * created in a Unit test is not commited to the database!!
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */

@isTest
private class SCReplenishmentExtensionTest
{
    /**
     * testForOneStock
     * ===============
     *
     * Test the extension class for replenishment for one stock.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    static testMethod void testForOneStock()
    {
        System.debug('########################################################');
        System.debug('#### Starting Test:  testForOneStock()');
        System.debug('########################################################');

        // create all neccessary datas
        Id plantId = SCHelperTestClass4.createTestPlant();
        List<SCStock__c> stocks = SCHelperTestClass4.createTestStocks(plantId);
        List<SCStock__c> originStocks = new List<SCStock__c>();
        originStocks.add(stocks.get(0));
        originStocks.add(stocks.get(1));
        List<SCStockOrigin__c> origins = SCHelperTestClass4.createTestStockOrigins(originStocks);
        List<SCArticle__c> articleList = SCHelperTestClass4.createTestArticles();
        List<User> users = SCHelperTestClass4.createTestUsers();
        List<SCResource__c> resources = SCHelperTestClass4.createTestResources(users);
        Id calId = SCHelperTestClass4.createTestCalendar();
        Id unitId = SCHelperTestClass4.createTestBusinessUnit(stocks.get(2).Id, calId);
        List<SCStock__c> assignStocks = new List<SCStock__c>();
        assignStocks.add(stocks.get(3));
        assignStocks.add(stocks.get(4));
        List<SCResourceAssignment__c> assignments = 
                        SCHelperTestClass4.createTestAssignments(resources, 
                                                               assignStocks, 
                                                               unitId);
        List<SCStockItem__c> items = 
                        SCHelperTestClass4.createTestStockItemsForReplenishment(articleList, 
                                                                              assignStocks);
        
        Test.startTest();
        
        SCReplenishmentExtension replExt = 
                     new SCReplenishmentExtension(new ApexPages.StandardController(stocks.get(4)));
        System.assert(replExt.assignmentOk);
        
        // test the methodes only for code coverage
        List<SelectOption> typeList = replExt.getTypeList();
        // turn replenishment date one day back, refillLists will check the date
        replExt.assignment.ValidFrom__c = Date.today().addDays(-1);
        replExt.refillLists();
        
        replExt.sourceStock = replExt.sourceList.get(0).getValue();
        // select the first item in the receiver list, 
        // this is the stock of the employee
        replExt.receiverStock = replExt.receiverList.get(0).getValue();
        
        replExt.startReplenishment();
        // deselect all
        replExt.selectAll();
        // select all
        replExt.selectAll();
        
        replExt.orderMatMoves();
        System.assert(replExt.creatingOk);

        Test.stopTest();
    } // testForOneStock

    /**
     * testForOneStockWithMatMoves
     * ===========================
     *
     * Test the extension class for replenishment for one stock, 
     * with existing material movements.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    static testMethod void testForOneStockWithMatMoves()
    {
        System.debug('########################################################');
        System.debug('#### Starting Test:  testForOneStockWithMatMoves()');
        System.debug('########################################################');

        // create all neccessary datas
        Id plantId = SCHelperTestClass4.createTestPlant();
        List<SCStock__c> stocks = SCHelperTestClass4.createTestStocks(plantId);
        List<SCStock__c> originStocks = new List<SCStock__c>();
        originStocks.add(stocks.get(0));
        originStocks.add(stocks.get(1));
        List<SCStockOrigin__c> origins = SCHelperTestClass4.createTestStockOrigins(originStocks);
        List<SCArticle__c> articleList = SCHelperTestClass4.createTestArticles();
        List<User> users = SCHelperTestClass4.createTestUsers();
        List<SCResource__c> resources = SCHelperTestClass4.createTestResources(users);
        Id calId = SCHelperTestClass4.createTestCalendar();
        Id unitId = SCHelperTestClass4.createTestBusinessUnit(stocks.get(2).Id, calId);
        List<SCStock__c> assignStocks = new List<SCStock__c>();
        assignStocks.add(stocks.get(3));
        assignStocks.add(stocks.get(4));
        List<SCResourceAssignment__c> assignments = 
                        SCHelperTestClass4.createTestAssignments(resources, 
                                                               assignStocks, 
                                                               unitId);
        List<SCStockItem__c> items = 
                        SCHelperTestClass4.createTestStockItemsForReplenishment(articleList, 
                                                                              assignStocks);
        
        Test.startTest();
        
        SCReplenishmentExtension replExt = 
                     new SCReplenishmentExtension(new ApexPages.StandardController(stocks.get(4)));
        System.assert(replExt.assignmentOk);
        
        // test the methodes only for code coverage
        List<SelectOption> typeList = replExt.getTypeList();
        // turn replenishment date one day back, refillLists will check the date
        replExt.assignment.ValidFrom__c = Date.today().addDays(-1);
        replExt.refillLists();
        
        replExt.sourceStock = replExt.sourceList.get(0).getValue();
        // select the first item in the receiver list, 
        // this is the stock of the employee
        replExt.receiverStock = replExt.receiverList.get(0).getValue();
        
        replExt.startReplenishment();
        
        // now create two temporary material movement entries
        List<SCMaterialMovement__c> matMoveList = new List<SCMaterialMovement__c>();
        SCMaterialMovement__c tmpMatMove = new SCMaterialMovement__c();
        tmpMatMove.Stock__c = replExt.receiverStock;
        tmpMatMove.DeliveryStock__c = replExt.sourceStock;
        tmpMatMove.Article__c = articleList.get(5).Id;
        tmpMatMove.Qty__c = 1.0;
        tmpMatMove.Status__c = SCfwConstants.DOMVAL_MATSTAT_ORDERED;
        tmpMatMove.Type__c = SCfwConstants.DOMVAL_MATMOVETYP_REPLENISHMENT;
        tmpMatMove.RequisitionDate__c = Date.today();
        matMoveList.add(tmpMatMove);

        tmpMatMove = new SCMaterialMovement__c();
        tmpMatMove.Stock__c = replExt.receiverStock;
        tmpMatMove.DeliveryStock__c = replExt.sourceStock;
        tmpMatMove.Article__c = articleList.get(5).Id;
        tmpMatMove.Qty__c = 1.0;
        tmpMatMove.Status__c = SCfwConstants.DOMVAL_MATSTAT_ORDERED;
        tmpMatMove.Type__c = SCfwConstants.DOMVAL_MATMOVETYP_REPLENISHMENT;
        tmpMatMove.RequisitionDate__c = Date.today().addDays(-1);
        matMoveList.add(tmpMatMove);
        insert matMoveList;
        
        replExt.orderMatMoves();
        replExt.createReplenishment();
        System.assert(replExt.creatingOk);

        Test.stopTest();
    } // testForOneStockWithMatMoves

    /**
     * testForEmplsOfUnit
     * ==================
     *
     * Test the extension class for replenishment for all employees
     * of a business unit.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    static testMethod void testForEmplsOfUnit()
    {
        System.debug('########################################################');
        System.debug('#### Starting Test:  testForEmplsOfUnit()');
        System.debug('########################################################');

        // create all neccessary datas
        Id plantId = SCHelperTestClass4.createTestPlant();
        List<SCStock__c> stocks = SCHelperTestClass4.createTestStocks(plantId);
        List<SCStock__c> originStocks = new List<SCStock__c>();
        originStocks.add(stocks.get(0));
        originStocks.add(stocks.get(1));
        List<SCStockOrigin__c> origins = SCHelperTestClass4.createTestStockOrigins(originStocks);
        List<SCArticle__c> articleList = SCHelperTestClass4.createTestArticles();
        List<User> users = SCHelperTestClass4.createTestUsers();
        List<SCResource__c> resources = SCHelperTestClass4.createTestResources(users);
        Id calId = SCHelperTestClass4.createTestCalendar();
        Id unitId = SCHelperTestClass4.createTestBusinessUnit(stocks.get(2).Id, calId);
        List<SCStock__c> assignStocks = new List<SCStock__c>();
        assignStocks.add(stocks.get(3));
        assignStocks.add(stocks.get(4));
        assignStocks.add(stocks.get(5));
        List<SCResourceAssignment__c> assignments = 
                        SCHelperTestClass4.createTestAssignments(resources, 
                                                               assignStocks, 
                                                               unitId);
        List<SCStockItem__c> items = 
                        SCHelperTestClass4.createTestStockItemsForReplenishment(articleList, 
                                                                              assignStocks);
        
        Test.startTest();
        
        SCReplenishmentExtension replExt = 
                     new SCReplenishmentExtension(new ApexPages.StandardController(stocks.get(4)));
        System.assert(replExt.assignmentOk);
        
        // test the methodes only for code coverage
        List<SelectOption> typeList = replExt.getTypeList();
        // turn replenishment date one day back, refillLists will check the date
        replExt.assignment.ValidFrom__c = Date.today().addDays(-1);
        replExt.refillLists();
        
        replExt.sourceStock = replExt.sourceList.get(0).getValue();
        // select the second item in the receiver list, 
        // this is the entry for all employees of a business unit
        replExt.receiverStock = replExt.receiverList.get(1).getValue();
        
        replExt.startReplenishment();
        // deselect all
        replExt.selectAll();
        // select all
        replExt.selectAll();
        
        replExt.orderMatMoves();
        System.assert(replExt.creatingOk);

        Test.stopTest();
    } // testForEmplsOfUnit

    /**
     * testForEmplsOfUnitWithMatMoves
     * ==============================
     *
     * Test the extension class for replenishment for all employees
     * of a business unit, with existing material movements.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    static testMethod void testForEmplsOfUnitWithMatMoves()
    {
        System.debug('########################################################');
        System.debug('#### Starting Test:  testForEmplsOfUnitWithMatMoves()');
        System.debug('########################################################');

        // create all neccessary datas
        Id plantId = SCHelperTestClass4.createTestPlant();
        List<SCStock__c> stocks = SCHelperTestClass4.createTestStocks(plantId);
        List<SCStock__c> originStocks = new List<SCStock__c>();
        originStocks.add(stocks.get(0));
        originStocks.add(stocks.get(1));
        List<SCStockOrigin__c> origins = SCHelperTestClass4.createTestStockOrigins(originStocks);
        List<SCArticle__c> articleList = SCHelperTestClass4.createTestArticles();
        List<User> users = SCHelperTestClass4.createTestUsers();
        List<SCResource__c> resources = SCHelperTestClass4.createTestResources(users);
        Id calId = SCHelperTestClass4.createTestCalendar();
        Id unitId = SCHelperTestClass4.createTestBusinessUnit(stocks.get(2).Id, calId);
        List<SCStock__c> assignStocks = new List<SCStock__c>();
        assignStocks.add(stocks.get(3));
        assignStocks.add(stocks.get(4));
        assignStocks.add(stocks.get(5));
        List<SCResourceAssignment__c> assignments = 
                        SCHelperTestClass4.createTestAssignments(resources, 
                                                               assignStocks, 
                                                               unitId);
        List<SCStockItem__c> items = 
                        SCHelperTestClass4.createTestStockItemsForReplenishment(articleList, 
                                                                              assignStocks);
        
        Test.startTest();
        
        SCReplenishmentExtension replExt = 
                     new SCReplenishmentExtension(new ApexPages.StandardController(stocks.get(4)));
        System.assert(replExt.assignmentOk);
        
        // test the methodes only for code coverage
        List<SelectOption> typeList = replExt.getTypeList();
        // turn replenishment date one day back, refillLists will check the date
        replExt.assignment.ValidFrom__c = Date.today().addDays(-1);
        replExt.refillLists();
        
        replExt.sourceStock = replExt.sourceList.get(0).getValue();
        // select the second item in the receiver list, 
        // this is the entry for all employees of a business unit
        replExt.receiverStock = replExt.receiverList.get(1).getValue();
        
        replExt.startReplenishment();
        // deselect all
        replExt.selectAll();
        // select all
        replExt.selectAll();
        
        // now create two temporary material movement entries
        List<SCMaterialMovement__c> matMoveList = new List<SCMaterialMovement__c>();
        SCMaterialMovement__c tmpMatMove = new SCMaterialMovement__c();
        tmpMatMove.Stock__c = stocks.get(4).Id;
        tmpMatMove.DeliveryStock__c = replExt.sourceStock;
        tmpMatMove.Article__c = articleList.get(5).Id;
        tmpMatMove.Qty__c = 1.0;
        tmpMatMove.Status__c = SCfwConstants.DOMVAL_MATSTAT_ORDERED;
        tmpMatMove.Type__c = SCfwConstants.DOMVAL_MATMOVETYP_REPLENISHMENT;
        tmpMatMove.RequisitionDate__c = Date.today();
        matMoveList.add(tmpMatMove);

        tmpMatMove = new SCMaterialMovement__c();
        tmpMatMove.Stock__c = stocks.get(4).Id;
        tmpMatMove.DeliveryStock__c = replExt.sourceStock;
        tmpMatMove.Article__c = articleList.get(5).Id;
        tmpMatMove.Qty__c = 1.0;
        tmpMatMove.Status__c = SCfwConstants.DOMVAL_MATSTAT_ORDERED;
        tmpMatMove.Type__c = SCfwConstants.DOMVAL_MATMOVETYP_REPLENISHMENT;
        tmpMatMove.RequisitionDate__c = Date.today().addDays(-1);
        matMoveList.add(tmpMatMove);
        insert matMoveList;
        
        replExt.orderMatMoves();
        replExt.createReplenishment();
        System.assert(replExt.creatingOk);

        Test.stopTest();
    } // testForEmplsOfUnitWithMatMoves

    /**
     * testForEmplsOfUnit2
     * ====================
     *
     * Test the extension class for replenishment for all employees 
     * of a business unit, but one employee has no assigned stock.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    static testMethod void testForEmplsOfUnit2()
    {
        System.debug('########################################################');
        System.debug('#### Starting Test:  testForEmplsOfUnit2()');
        System.debug('########################################################');

        // create all neccessary datas
        Id plantId = SCHelperTestClass4.createTestPlant();
        List<SCStock__c> stocks = SCHelperTestClass4.createTestStocks(plantId);
        List<SCStock__c> originStocks = new List<SCStock__c>();
        originStocks.add(stocks.get(0));
        originStocks.add(stocks.get(1));
        List<SCStockOrigin__c> origins = SCHelperTestClass4.createTestStockOrigins(originStocks);
        List<SCArticle__c> articleList = SCHelperTestClass4.createTestArticles();
        List<User> users = SCHelperTestClass4.createTestUsers();
        List<SCResource__c> resources = SCHelperTestClass4.createTestResources(users);
        Id calId = SCHelperTestClass4.createTestCalendar();
        Id unitId = SCHelperTestClass4.createTestBusinessUnit(stocks.get(2).Id, calId);
        List<SCStock__c> assignStocks = new List<SCStock__c>();
        assignStocks.add(stocks.get(3));
        assignStocks.add(stocks.get(4));
        List<SCResourceAssignment__c> assignments = 
                        SCHelperTestClass4.createTestAssignments(resources, 
                                                               assignStocks, 
                                                               unitId);
        List<SCStockItem__c> items = 
                        SCHelperTestClass4.createTestStockItemsForReplenishment(articleList, 
                                                                              assignStocks);
        
        Test.startTest();
        
        SCReplenishmentExtension replExt = 
                     new SCReplenishmentExtension(new ApexPages.StandardController(stocks.get(4)));
        System.assert(replExt.assignmentOk);
        
        // test the methodes only for code coverage
        List<SelectOption> typeList = replExt.getTypeList();
        // turn replenishment date one day back, refillLists will check the date
        replExt.assignment.ValidFrom__c = Date.today().addDays(-1);
        replExt.refillLists();
        
        replExt.sourceStock = replExt.sourceList.get(0).getValue();
        // select the second item in the receiver list, 
        // this is the entry for all employees of a business unit
        replExt.receiverStock = replExt.receiverList.get(1).getValue();
        
        replExt.startReplenishment();
        
        replExt.orderMatMoves();
        System.assert(replExt.orderStep2);
        replExt.orderStep2();
        System.assert(replExt.creatingOk);

        Test.stopTest();
    } // testForEmplsOfUnit2

    /**
     * testForEmplsOfUnit2WithMatMoves
     * ==============================
     *
     * Test the extension class for replenishment for all employees 
     * of a business unit, but one employee has no assigned stock,
     * with existing material movements.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    static testMethod void testForEmplsOfUnit2WithMatMoves()
    {
        System.debug('########################################################');
        System.debug('#### Starting Test:  testForEmplsOfUnit2WithMatMoves()');
        System.debug('########################################################');

        // create all neccessary datas
        Id plantId = SCHelperTestClass4.createTestPlant();
        List<SCStock__c> stocks = SCHelperTestClass4.createTestStocks(plantId);
        List<SCStock__c> originStocks = new List<SCStock__c>();
        originStocks.add(stocks.get(0));
        originStocks.add(stocks.get(1));
        List<SCStockOrigin__c> origins = SCHelperTestClass4.createTestStockOrigins(originStocks);
        List<SCArticle__c> articleList = SCHelperTestClass4.createTestArticles();
        List<User> users = SCHelperTestClass4.createTestUsers();
        List<SCResource__c> resources = SCHelperTestClass4.createTestResources(users);
        Id calId = SCHelperTestClass4.createTestCalendar();
        Id unitId = SCHelperTestClass4.createTestBusinessUnit(stocks.get(2).Id, calId);
        List<SCStock__c> assignStocks = new List<SCStock__c>();
        assignStocks.add(stocks.get(3));
        assignStocks.add(stocks.get(4));
        List<SCResourceAssignment__c> assignments = 
                        SCHelperTestClass4.createTestAssignments(resources, 
                                                               assignStocks, 
                                                               unitId);
        List<SCStockItem__c> items = 
                        SCHelperTestClass4.createTestStockItemsForReplenishment(articleList, 
                                                                              assignStocks);
        
        Test.startTest();
        
        SCReplenishmentExtension replExt = 
                     new SCReplenishmentExtension(new ApexPages.StandardController(stocks.get(4)));
        System.assert(replExt.assignmentOk);
        
        // test the methodes only for code coverage
        List<SelectOption> typeList = replExt.getTypeList();
        // turn replenishment date one day back, refillLists will check the date
        replExt.assignment.ValidFrom__c = Date.today().addDays(-1);
        replExt.refillLists();
        
        replExt.sourceStock = replExt.sourceList.get(0).getValue();
        // select the second item in the receiver list, 
        // this is the entry for all employees of a business unit
        replExt.receiverStock = replExt.receiverList.get(1).getValue();
        
        replExt.startReplenishment();
        
        // now create two temporary material movement entries
        List<SCMaterialMovement__c> matMoveList = new List<SCMaterialMovement__c>();
        SCMaterialMovement__c tmpMatMove = new SCMaterialMovement__c();
        tmpMatMove.Stock__c = stocks.get(4).Id;
        tmpMatMove.DeliveryStock__c = replExt.sourceStock;
        tmpMatMove.Article__c = articleList.get(5).Id;
        tmpMatMove.Qty__c = 1.0;
        tmpMatMove.Status__c = SCfwConstants.DOMVAL_MATSTAT_ORDERED;
        tmpMatMove.Type__c = SCfwConstants.DOMVAL_MATMOVETYP_REPLENISHMENT;
        tmpMatMove.RequisitionDate__c = Date.today();
        matMoveList.add(tmpMatMove);

        tmpMatMove = new SCMaterialMovement__c();
        tmpMatMove.Stock__c = stocks.get(4).Id;
        tmpMatMove.DeliveryStock__c = replExt.sourceStock;
        tmpMatMove.Article__c = articleList.get(5).Id;
        tmpMatMove.Qty__c = 1.0;
        tmpMatMove.Status__c = SCfwConstants.DOMVAL_MATSTAT_ORDERED;
        tmpMatMove.Type__c = SCfwConstants.DOMVAL_MATMOVETYP_REPLENISHMENT;
        tmpMatMove.RequisitionDate__c = Date.today().addDays(-1);
        matMoveList.add(tmpMatMove);
        insert matMoveList;
        
        replExt.orderMatMoves();
        System.assert(replExt.orderStep2);
        replExt.orderStep2();
        replExt.createReplenishment();
        System.assert(replExt.creatingOk);

        Test.stopTest();
    } // testForEmplsOfUnit2WithMatMoves
} // SCReplenishmentExtensionTest
