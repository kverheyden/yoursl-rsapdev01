global with sharing class CCWCPDFCreatorHandler{
	
	global String UserSessionId { get; set; }
	
	public void createContractsCallout(List<Attachment> attachments) {
		Map<Id, Id> attachmentId_ParentIdMap = new Map<Id, Id>();
		for (Attachment a : attachments) {
			if (a.ParentId.getSObjectType() == CdeOrder__c.SObjectType)
    			attachmentId_ParentIdMap.put(a.Id, a.ParentId);		    		
		}
		if (attachmentId_ParentIdMap.keySet().size() > 0)
			CCWCPDFCreator.createContractsCallout(attachmentId_ParentIdMap, UserSessionId);
	}
	
	public void createSalesOrderSummariesCallout(Set<Id> accountIds) {
		System.debug('SALES ORDER SUMMARY CALLOUT');
		CCWCPDFCreator.createSalesOrderSummariesCallout(accountIds, UserSessionId);
	}
	
}
