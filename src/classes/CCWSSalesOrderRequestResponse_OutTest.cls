@isTest
private class CCWSSalesOrderRequestResponse_OutTest {
 
    @isTest static void basic_coverage() {
        // Most of the generated code won't be used, create the inner classes at a minimum to help our test coverage.
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPIADDR1());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPICCARD());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPICCARD_EX());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPICOND());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPICUBLB());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPICUCFG());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPICUINS());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPICUPRT());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPICUVAL());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPIINCOMP());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPIITEMEX());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPIITEMIN());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPIPAREX());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPIPARNR());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPIPARTNR());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPIPAYER());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPIRET2());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPIRETURN());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPISCHDL());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPISDHD1());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPISDHEAD());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPISDHEDU());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPISDITM());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPISDTEXT());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPISHIPTO());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.BAPISOLDTO());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.CC_SalesOrderATPCheck_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.CC_SalesOrderATPCheck_Response_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.CC_SalesOrderCreateOrder_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.CC_SalesOrderCreateOrderResponse_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.CC_SalesOrderGetRequestDate_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.CC_SalesOrderGetRequestDateResponse_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.CC_SalesOrderSimulateOrder_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.CC_SalesOrderSimulateOrderResponse_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.CCWSSalesOrderRequestResponse_OutPort());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.EX_COM_QTY_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.EXTENSIONIN_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.IM_MATPLANT_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.MESSAGETABLE_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.ORDER_CCARD_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.ORDER_CCARD_EX_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.ORDER_CFGS_BLOB_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.ORDER_CFGS_INST_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.ORDER_CFGS_PART_OF_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.ORDER_CFGS_REF_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.ORDER_CFGS_VALUE_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.ORDER_CONDITION_EX_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.ORDER_CONDITIONS_IN_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.ORDER_INCOMPLETE_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.ORDER_ITEMS_IN_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.ORDER_ITEMS_OUT_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.ORDER_PARTNERS_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.ORDER_SCHEDULE_EX_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.ORDER_SCHEDULE_IN_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.ORDER_SCHEDULES_IN_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.ORDER_TEXT_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.PARTNERADDRESSES_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.RETURN_element());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.x_xCCC_xOTCS_MATAVAIL_LONG());
        System.assertNotEquals(null,new CCWSSalesOrderRequestResponse_Out.x_xCCC_xOTCS_QTY_CDD());

    }
    
/*
 * AB, 2014-11-28: As a test method defined methods do not support Web services call-outs. Test commented out.
 *
    @isTest static void test_getRequestDate() {
        // Will be tested in more detail in another class.
        CCWSSalesOrderRequestResponse_Out.CCWSSalesOrderRequestResponse_OutPort stub = new CCWSSalesOrderRequestResponse_Out.CCWSSalesOrderRequestResponse_OutPort();
        CCWSSalesOrderRequestResponse_Out.ORDER_SCHEDULE_IN_element response = stub.GetRequestDate(
             null //String CONVERT_PARVW_AUART
            ,null //CCWSSalesOrderRequestResponse_Out.BAPISDHEAD ORDER_HEADER_IN
            ,null //CCWSSalesOrderRequestResponse_Out.ORDER_PARTNERS_element ORDER_PARTNERS
        );
    }

    @isTest static void test_simulateOrder() {
        // Will be tested in more detail in another class.
        CCWSSalesOrderRequestResponse_Out.CCWSSalesOrderRequestResponse_OutPort stub = new CCWSSalesOrderRequestResponse_Out.CCWSSalesOrderRequestResponse_OutPort();
        CCWSSalesOrderRequestResponse_Out.CC_SalesOrderSimulateOrderResponse_element response = stub.SimulateOrder(
             null //String CONVERT_PARVW_AUART
            ,null //CCWSSalesOrderRequestResponse_Out.BAPISDHEAD ORDER_HEADER_IN
            ,null //CCWSSalesOrderRequestResponse_Out.EXTENSIONIN_element EXTENSIONIN
            ,null //CCWSSalesOrderRequestResponse_Out.MESSAGETABLE_element MESSAGETABLE
            ,null //CCWSSalesOrderRequestResponse_Out.ORDER_CCARD_element ORDER_CCARD
            ,null //CCWSSalesOrderRequestResponse_Out.ORDER_CCARD_EX_element ORDER_CCARD_EX
            ,null //CCWSSalesOrderRequestResponse_Out.ORDER_CFGS_BLOB_element ORDER_CFGS_BLOB
            ,null //CCWSSalesOrderRequestResponse_Out.ORDER_CFGS_INST_element ORDER_CFGS_INST
            ,null //CCWSSalesOrderRequestResponse_Out.ORDER_CFGS_PART_OF_element ORDER_CFGS_PART_OF
            ,null //CCWSSalesOrderRequestResponse_Out.ORDER_CFGS_REF_element ORDER_CFGS_REF
            ,null //CCWSSalesOrderRequestResponse_Out.ORDER_CFGS_VALUE_element ORDER_CFGS_VALUE
            ,null //CCWSSalesOrderRequestResponse_Out.ORDER_CONDITION_EX_element ORDER_CONDITION_EX
            ,null //CCWSSalesOrderRequestResponse_Out.ORDER_INCOMPLETE_element ORDER_INCOMPLETE
            ,null //CCWSSalesOrderRequestResponse_Out.ORDER_ITEMS_IN_element ORDER_ITEMS_IN
            ,null //CCWSSalesOrderRequestResponse_Out.ORDER_ITEMS_OUT_element ORDER_ITEMS_OUT
            ,null //CCWSSalesOrderRequestResponse_Out.ORDER_PARTNERS_element ORDER_PARTNERS
            ,null //CCWSSalesOrderRequestResponse_Out.ORDER_SCHEDULE_EX_element ORDER_SCHEDULE_EX
            ,null //CCWSSalesOrderRequestResponse_Out.ORDER_SCHEDULE_IN_element ORDER_SCHEDULE_IN
            ,null //CCWSSalesOrderRequestResponse_Out.PARTNERADDRESSES_element PARTNERADDRESSES
        );
    }

    @isTest static void test_CreateOrder() {
        // Will be tested in more detail in another class.
        CCWSSalesOrderRequestResponse_Out.CCWSSalesOrderRequestResponse_OutPort stub = new CCWSSalesOrderRequestResponse_Out.CCWSSalesOrderRequestResponse_OutPort();
        CCWSSalesOrderRequestResponse_Out.CC_SalesOrderCreateOrderResponse_element response = stub.CreateOrder(
             null //CCWSSalesOrderRequestResponse_Out.BAPISDHD1 ORDER_HEADER_IN
            ,null //String SALESDOCUMENTIN
            ,null //String TESTRUN
            ,null //CCWSSalesOrderRequestResponse_Out.ORDER_ITEMS_IN_element ORDER_ITEMS_IN
            ,null //CCWSSalesOrderRequestResponse_Out.ORDER_PARTNERS_element ORDER_PARTNERS
            ,null //CCWSSalesOrderRequestResponse_Out.ORDER_SCHEDULES_IN_element ORDER_SCHEDULES_IN
            ,null //CCWSSalesOrderRequestResponse_Out.ORDER_TEXT_element ORDER_TEXT
        );
    }

    @isTest static void test_ATPCheck() {
        // Will be tested in more detail in another class.
        CCWSSalesOrderRequestResponse_Out.CCWSSalesOrderRequestResponse_OutPort stub = new CCWSSalesOrderRequestResponse_Out.CCWSSalesOrderRequestResponse_OutPort();
        CCWSSalesOrderRequestResponse_Out.CC_SalesOrderATPCheck_Response_element response = stub.ATPCheck(
             null //String IM_CHECK_RULE
            ,null //CCWSSalesOrderRequestResponse_Out.IM_MATPLANT_element IM_MATPLANT
        );
    }
 */

}
