/**
 * AseCalloutConstants.cls  mt  28.09.2009
 * 
 * Copyright (c) 2009 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 /**
 * AseCalloutConstants includes all constants
 * 
 * @author mt
 * @version $Revision$, $Date$
 */
public class AseCalloutConstants
{
    //constants for the interface to the ase-webservice
    public static final integer ASE_TYPE_UNKNOWN = 1;
    public static final integer ASE_TYPE_SCHEDULEPARA = 2;
    public static final integer ASE_TYPE_CALENDAR = 3;
    public static final integer ASE_TYPE_CALENDAR_ENTRY = 4;
    public static final integer ASE_TYPE_LOCATION = 5;
    public static final integer ASE_TYPE_ORGUNIT = 6;
    public static final integer ASE_TYPE_RESOURCE = 7;
    public static final integer ASE_TYPE_USERLOGIN = 8;
    public static final integer ASE_TYPE_NORMWORK = 9;
    public static final integer ASE_TYPE_USER = 10;
    public static final integer ASE_TYPE_SKILLOBJ = 11;
    public static final integer ASE_TYPE_SKILLOBJROLE = 12;
    public static final integer ASE_TYPE_EMPLNORMWORK = 13;
    public static final integer ASE_TYPE_EMPLHIST = 14;
    public static final integer ASE_TYPE_DEMAND = 15;
    public static final integer ASE_TYPE_APPOINTMENT = 16;
    public static final integer ASE_TYPE_SCHEDPARAM = 17;
    public static final integer ASE_TYPE_SERVICEITEM = 18;
    public static final integer ASE_TYPE_CONTEXT = 19;
    public static final integer ASE_TYPE_SERVICECONTROL = 29;

    public static final String ASE_TYPE_UNKNOWN_STR = '<unknown>';
    public static final String ASE_TYPE_SCHEDULEPARA_STR = 'scheduleParameter';
    public static final String ASE_TYPE_CALENDAR_STR = 'calendar';
    public static final String ASE_TYPE_CALENDAR_ENTRY_STR = 'calendarEntry';
    public static final String ASE_TYPE_LOCATION_STR = 'location';
    public static final String ASE_TYPE_ORGUNIT_STR = 'orgunit';
    public static final String ASE_TYPE_RESOURCE_STR = 'resource';
    public static final String ASE_TYPE_USERLOGIN_STR = 'userlogin';
    public static final String ASE_TYPE_NORMWORK_STR = 'normwork';
    public static final String ASE_TYPE_USER_STR = 'user';
    public static final String ASE_TYPE_SKILLOBJ_STR = 'skillobj';
    public static final String ASE_TYPE_SKILLOBJROLE_STR = 'skillobjrole';
    public static final String ASE_TYPE_EMPLNORMWORK_STR = 'emplnormwork';
    public static final String ASE_TYPE_EMPLHIST_STR = 'emplhist';
    public static final String ASE_TYPE_DEMAND_STR = 'demand';
    public static final String ASE_TYPE_APPOINTMENT_STR = 'appointment';
    public static final String ASE_TYPE_SCHEDPARAM_STR = 'schedparam';
    public static final String ASE_TYPE_SERVICEITEM_STR = 'serviceitem';
    public static final String ASE_TYPE_CONTEXT_STR = 'context';
    public static final String ASE_TYPE_RELJOB_STR = 'reljob';
    public static final String ASE_TYPE_STATUS_STR = 'status';
    public static final String ASE_TYPE_SERVICECONTROL_STR = 'servicecontrol';
    public static final String ASE_TYPE_PARASELECT_STR = 'paraselect';
    public static final String ASE_TYPE_TOUROPTIMIZERJOB_STR = 'touroptimizer';

    /* the endpoint where the ASEWebservice is located */
    public static final String ENDPOINT;
    
    /* the default dateformat is there is a date which hast to be send over interface */
    public static final String ASE_DATE_FORMAT = 'yyyyMMdd';
    
    /* the name of the tenant which can be used for tests */
    public static final String TEST_TENANT = 'TestTenant';
    
    /* the maximal size of dataentries are allowed in a list */
    public static final integer MAX_DATAENTRY_SIZE = 1000;
    
    /* the maximal size of dataentries are allowed in a list in testing mode */
    public static final integer MAX_DATAENTRY_SIZETESTING = 200;

    /* determine the endpoint URL from custom settings */
    
    static
    {
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        ENDPOINT = appSettings.ASE_ENDPOINT_URL__c;
    }
}
