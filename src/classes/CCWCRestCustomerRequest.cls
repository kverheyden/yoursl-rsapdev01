/**********************************************************************
Name: CCWCRestCustomerRequest()
======================================================
Purpose: 
Represents a web service. Creates or updates a Request__c object by HTTP POST
======================================================
History 
------- 
Date AUTHOR DETAIL 
05/05/2014 Jan Mensfeld INITIAL DEVELOPMENT
***********************************************************************/

@RestResource(urlMapping='/CustomerRequest/*') 
global with sharing class CCWCRestCustomerRequest{
	private class CustomerRequestException extends Exception { }
	
    global class ResponseWrapper{
        public ResponseWrapper() {
			Warnings = new List<String>();
		}
		
		public String StatusCode { get; set; }
        public String StatusMessage { get; set; }
		public String Stacktrace { get; set; }
		public List<String> Warnings { get; set; }
		public Id CreatedRequestId { get; set; }
		public Id CreatedRequestId18 { get; set; }
		
    }
	@HttpPost
    global static void doPost(){
		DateTime requestStart = System.now();
		ResponseWrapper response = new ResponseWrapper();
		Request__c requestedRequest = new Request__c();	
		
		Integer describeLimit = Limits.getLimitFieldsDescribes();
		try {		
			RestRequest request = RestContext.Request;
			
			Map<String, SObjectField> requestFieldMap = Schema.SObjectType.Request__c.Fields.getMap();
			Map<String, Schema.DisplayType> requestFieldTypeMap = Utils.getFieldSetMemberTypeMap(Request__c.SObjectType, 'CustomerRequestFields');
			String requestJSON = request.RequestBody.toString();
			System.debug('JSON REQUEST: ' + requestJSON);
			
			Map<String, Object> jsonMap = (Map<String, Object>)JSON.deserializeUntyped(requestJSON);
			Object externalId = jsonMap.get('ExternalId__c');
			Object numberOfAttachments = jsonMap.get('NumberofAttachments__c');
			Request__c existingRequest = getRequestByExternalId(externalId);
			
			if (existingRequest != null) {
				// extract response... into method
				response.StatusCode = 'Done';
				response.StatusMessage = 'Request__c object allready exists';
				response.CreatedRequestId = existingRequest.Id;
				response.CreatedRequestId18 = existingRequest.SFID__c;
				RestContext.response.addHeader('Content-Type', 'application/json');     
				RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
				RestServiceUtilities.WSLog log = new RestServiceUtilities.WSLog()
					.setStart(requestStart);
				log.insertLog();
			}
			
			JSONParser parser = JSON.createParser(requestJSON);
			
			Set<JSONToken> JSONValueTokens = new Set<JSONToken> (JSONToken.values());
			while (parser.nextValue() != null) {
				if (parser.getCurrentName() == null)
					continue;
				JSONToken token = parser.getCurrentToken();
				if (JSONValueTokens.contains(token)) {					
					SObjectField requestField = requestFieldMap.get(parser.getCurrentName());
					if (requestField != null) {						
						try {
							Schema.DisplayType fieldType = requestFieldTypeMap.get(parser.getCurrentName());
							if (fieldType == null) {
								if (Limits.getFieldsDescribes() >= Limits.getLimitFieldsDescribes() - 1)
									throw new CustomerRequestException('JSON contains too many fields (' + Limits.getFieldsDescribes() + '+)');								
								fieldType = requestField.getDescribe().getType();						
							}
							if (fieldType == Schema.DisplayType.DATE)
								requestedRequest.put(parser.getCurrentName(), parser.getDateValue());
							else if (fieldType == Schema.DisplayType.BOOLEAN)
								requestedRequest.put(parser.getCurrentName(), parser.getBooleanValue());
							else if (fieldType == Schema.DisplayType.INTEGER)
								requestedRequest.put(parser.getCurrentName(), parser.getIntegerValue());							
							else if (fieldType == Schema.DisplayType.DOUBLE)
								requestedRequest.put(parser.getCurrentName(), parser.getDecimalValue());
							else if (fieldType == Schema.DisplayType.ID || fieldType == Schema.DisplayType.REFERENCE)
								requestedRequest.put(parser.getCurrentName(), Id.valueOf(parser.getIdValue()));
							else						
								requestedRequest.put(parser.getCurrentName(), parser.getText());
						}
						catch(Exception e) {
							response.Warnings.add(e.getMessage() + ' - ' + parser.getCurrentName());
						}
					}
					else
						response.Warnings.add('Field does not exist: ' + parser.getCurrentName());
				}
			}
			if (numberOfAttachments == null || numberOfAttachments == 0)
				requestedRequest.IsReadyToSendToSAP__c = true;
			Database.DMLOptions dml = new Database.DMLOptions();
			dml.allowFieldTruncation = true;
			requestedRequest.setOptions(dml);
			insert requestedRequest;
		} 
		catch (Exception e) {
			response.StatusCode = 'Error';
            response.StatusMessage = e.getTypeName() + ': ' + e.getMessage();
			response.Stacktrace = e.getStackTraceString();
            RestContext.response.addHeader('Content-Type', 'application/json');     
			RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
			RestServiceUtilities.WSLog log = new RestServiceUtilities.WSLog()
				.setStart(requestStart);
			log.setStatus(RestServiceUtilities.WSStatus.ERROR);
			log.insertLog();
			return;
		}
		response.StatusCode = 'Done';
		response.StatusMessage = 'Success';
		response.CreatedRequestId = requestedRequest.Id;
		response.CreatedRequestId18 = requestedRequest.SFID__c;
		System.debug('ID: ' + requestedRequest.ID);
		System.debug('SFID: ' + requestedRequest.SFID__c);
		RestContext.response.addHeader('Content-Type', 'application/json');     
		RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
		RestServiceUtilities.WSLog log = new RestServiceUtilities.WSLog()
			.setStart(requestStart);
		if (response.Warnings.size() > 0)	
			log.setStatus(RestServiceUtilities.WSStatus.WARNING);
		log.insertLog();
	}
	
	private static Request__c getRequestByExternalId(Object externalId) {
		List<Request__c> requests = new List<Request__c>();
		if (externalId != null)
			requests = [SELECT Id, SFID__c FROM Request__c WHERE ExternalId__c =: String.valueOf(externalId)];
		if (requests.size() == 1)
			return requests[0];
		return null;
	}
	/*
	private static Boolean isNewCustomerRequest(Map<String, Object> jsonMap) {
		Object recordTypeId = jsonMap.get('RecordTypeId');		
		return isNewCustomerRequestRecordType(recordTypeId);
	}
	
	private static Boolean isNewCustomerRequestRecordType(Object recordTypeId) {
		if (RecordTypeId == null)
			return false;
		
		SalesAppSettings__c objSalesAppSetting = SalesAppSettings__c.getValues('RequestNewCustomerRecordTypeId');
		return String.valueOf(RecordTypeId) == objSalesAppSetting.Value__c;
	}
	*/	
}
