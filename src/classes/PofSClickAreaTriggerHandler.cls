public with sharing class PofSClickAreaTriggerHandler {
	public static final String DEVELOPERNAME_FIELDNAME = 'PictureDocDevName__c';
	public static final String IDTEXT_FIELDNAME = 'Picture__c';


	public class PofSClickAreaBIHandler extends TriggerHandlerBase {
		public override void mainEntry(TriggerParameters tp) {
			setIdTextByDeveloperName(tp.newList, 'PictureDocDevName__c', 'Picture__c');
			setIdTextByDeveloperName(tp.newList, 'IconDocDevName__c', 'Icon__c');
		}
	}

	public class PofSClickAreaBUHandler extends TriggerHandlerBase {
		public override void mainEntry(TriggerParameters tp) {
			setIdTextByDeveloperName(tp.newList, 'PictureDocDevName__c', 'Picture__c');
			setIdTextByDeveloperName(tp.newList, 'IconDocDevName__c', 'Icon__c');
		}
	}

	public static void setIdTextByDeveloperName(List<PofSClickArea__c> clickAreas, String devNameField, String idTextField) {
		Set<String> documentDeveloperNames = new Set<String>();
		for (PofSClickArea__c ca: clickAreas) {
			String devName = (String)ca.get(devNameField);
			if (!String.isBlank(devName))
				documentDeveloperNames.add(devName);
		}
		if (documentDeveloperNames.isEmpty())
			return;

		List<Document> documents = [SELECT Id, DeveloperName FROM Document WHERE DeveloperName IN: documentDeveloperNames];
		if (documents.isEmpty())
			return;
		
		Map<String, Id> developerNamesWithId = new Map<String, Id>();
		for (Document d : documents)
			developerNamesWithId.put(d.DeveloperName, d.Id);

		for (PofSClickArea__c ca: clickAreas) {
			String devName = (String)ca.get(devNameField);
			Id documentId = developerNamesWithId.get(devName);
			ca.put(idTextField, documentId);
		}
	}
}
