/**
 * @(#)SCOrderLineController.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 *
 * This class controlles displaying order lines for one order, 
 * by selection a view (= order line type).
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 *
 */

public with sharing class SCOrderLineController extends SCfwOrderControllerBase
{
    private Set<String> accessRights;
    
    // Id for the order
    public Id oid { get; set;}

    private Boolean stockValType = true;
    // GMSSU 16.04.2013 PMS 34794
    //private Boolean stockValType = false;
    
    public SCboOrder parboOrder { get; set;}
    
    public SCboOrder boOrder
    {
        get
        {
            if (null == boOrder)
            {
                try
                {
                    if (SCBase.isSet(oid))
                    {
                        boOrder = new SCboOrder();
                        boOrder.readbyId(oid);
                    }
                    else if (null != parboOrder)
                    {
                        boOrder = parboOrder;
                    }
                    else if ((null != componentController) && 
                             (componentController instanceOf SCOrderPageController))
                    {
                        boOrder = ((SCOrderPageController)componentController).order;
                        orderMat = ((SCOrderPageController)componentController).isRebook;
                    }
                    
                    if (!SCBase.isSet(brandName) && boOrder.boOrderItems.size() > 0)
                    {
                        brandName = boOrder.boOrderItems[0].orderItem.InstalledBase__r.Brand__r.Name;
                    } // if (!SCBase.isSet(brandName) && order.boOrderItems.size() > 0)
    
                    SCOrderRole__c roleIR = boOrder.getRole(SCfwConstants.DOMVAL_ORDERROLE_RE);
                    cardHolder = '';
                    if(roleIR != null)
                    {
                        if (SCBase.isSet(roleIR.Account__r.FirstName__c))
                        {
                            cardHolder = roleIR.Account__r.FirstName__c + ' ';
                        } // if (SCBase.isSet(roleIR.Account__r.FirstName__c))
                        cardHolder += roleIR.Account__r.LastName__c;
                    }
                    onCalculateSums();
                    
                    stock = null;
                } // try
                catch (Exception e)
                {
                    boOrder = null;
                } // catch (Exception e)
            }
            return boOrder;
        }
        set;
    }

    public SCStock__c stock
    {
        get
        {
            System.debug('#### stock getter 1');
            if (null == stock)
            {
                System.debug('#### stock getter 2');
                try
                {
                    System.debug('#### stock getter 3');
                    System.debug('#### selectedResId: ' + selectedResId);
                    // CCEAG: SCAppointment__c app = boOrder.getFirstOpenAppointment();
                    SCAppointment__c app = boOrder.appointments[0];
                    if (String.isNotBlank(selectedResId))
                    {
                        System.debug('#### stock getter 4');
                        System.debug('#### getBoOrder(): Resource -> ' + boOrder.appointments.get(0).Assignment__r.Resource__c);
                        SCResourceAssignment__c assignment = 
                                                [Select Id, Name, Stock__c, Stock__r.Plant__r.Name, Stock__r.ValuationType__c,
                                                        Stock__r.Name, Stock__r.Id, Department__r.Stock__c
                                                   from SCResourceAssignment__c 
                                                  where Resource__c = :selectedResId
                                                    and ValidFrom__c <= :system.today() 
                                                    and ValidTo__c >= :system.today()];
                                                    
                        System.debug('#### assignment.Stock__c: ' + assignment.Stock__c);
                        System.debug('#### assignment.Department__r.Stock__c: ' + assignment.Department__r.Stock__c);
                                                    
                        if ((null == assignment.Stock__c) || (null == assignment.Department__r.Stock__c))
                        {
                            System.debug('#### stock getter 5');
                            // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                            //                                System.Label.SC_msg_ReplenishmentNoAssignment));                        
                        }
                        else
                        {
                            stock = assignment.Stock__r;
                            stockValType = assignment.Stock__r.ValuationType__c;
                            System.debug('#### stock getter 6');
                        }
                    } // if (boOrder.getHasAppointments())
                    
                    System.debug('#### stock getter 7');
                } // try
                catch (Exception e)
                {
                    System.debug('#### stock getter 8');
                    // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                    //                      System.Label.SC_msg_ReplenishmentNoAssignment));                        
                } // catch (Exception e)
            }
            System.debug('#### stock getter 9');
            return stock;
        }
        set;
    }
    
   /*
    * Read all non cancelled assignmengs (also the 
    */
    public List<String> getResourceFromAppointments()
    {
       List<String> resources = new List<String>();
       if(boOrder != null && boOrder.appointments != null)
       {
           resources = new  List<String>();
       
           List<SCAssignment__c> apps = [select id, status__c, resource__c from SCAssignment__c where 
                                           status__c != :SCfwConstants.DOMVAL_ORDERSTATUS_CANCELLED and 
/*                                           status__c != :SCfwConstants.DOMVAL_ORDERSTATUS_ACCEPTED and 
                                           status__c != :SCfwConstants.DOMVAL_ORDERSTATUS_PLANNED_FOR_PRECHECK and
                                           status__c != :SCfwConstants.DOMVAL_ORDERSTATUS_RELEASED_FOR_PROCESSING and
                                           status__c != :SCfwConstants.DOMVAL_ORDERSTATUS_PLANNED
*/                                           
                                           Order__c = :boOrder.order.id];
       
            for (SCAssignment__c app : apps)
            {
                resources.add(app.resource__c);
            }
        }
        return resources;
    }

    public SCTool__c priceTool
    {
        get
        {
            if ((null != componentController) && 
                (componentController instanceOf SCOrderPageController))
            {
                return ((SCOrderPageController)componentController).tool;
            }
            else
            {
                return tool;
            }
        }
        set;
    }
    
    
    /**
     * Getter for the sum of tax for all order lines.
     *
     * @return    sum of tax as a decimal value
     */
    public Decimal sumLineTax
    { 
        get
        {
            if ((null != componentController) && 
                (componentController instanceOf SCOrderPageController))
            {
                return ((SCOrderPageController)componentController).sumTax;
            }
            else
            {
                return sumTax;
            }
        } 
        private set; 
    }
    
    /**
     * Getter for the sum of netto prices for all order lines.
     *
     * @return    sum of netto prices as a decimal value
     */
    public Decimal sumLinePriceNet
    { 
        get
        {
            if ((null != componentController) && 
                (componentController instanceOf SCOrderPageController))
            {
                return ((SCOrderPageController)componentController).sumPriceNet;
            }
            else
            {
                return sumPriceNet;
            }
        }
        private set; 
    }
    
    /**
     * Getter for the sum of netto prices for all order lines.
     *
     * @return    sum of netto prices as a decimal value
     */
    public Decimal sumLinePriceGross
    { 
        get
        {
            if ((null != componentController) && 
                (componentController instanceOf SCOrderPageController))
            {
                return ((SCOrderPageController)componentController).sumPriceGross;
            }
            else
            {
                return sumPriceGross;
            }
        }
        private set; 
    }

    /**
     * List index of the currently selected order line
     */
    public Double curLineRow
    {
        get
        {
            if ((null != componentController) && 
                (componentController instanceOf SCOrderPageController))
            {
                return ((SCOrderPageController)componentController).currentLineRow;
            }
            else
            {
                return currentLineRow;
            }
        }
        set
        {
            if ((null != componentController) && 
                (componentController instanceOf SCOrderPageController))
            {
                ((SCOrderPageController)componentController).currentLineRow = value;
            }
            else
            {
                currentLineRow = value;
            }
        }
    }

    /**
     * The currently selected order line view type.
     */
    public String curOrdLineType
    {
        get
        {
            if ((null != componentController) && 
                (componentController instanceOf SCOrderPageController))
            {
                return ((SCOrderPageController)componentController).curOrderLineType;
            }
            else
            {
                return curOrderLineType;
            }
        }
        set
        {
            if ((null != componentController) && 
                (componentController instanceOf SCOrderPageController))
            {
                ((SCOrderPageController)componentController).curOrderLineType = value;
            }
            else
            {
                curOrderLineType = value;
            }
        }
    }

    // reference to the controller of the parent page
    public SCfwComponentControllerBase componentController { get; set;}

    // Article Id that was selected and has to be used in the order line
    public String selectedArticleId { get; set; }
    public String selectedValuationType { get; set; }
    
    public String desiredStart {get; set; }
    
    public Boolean showPriceLists { get; set; }
    public Boolean showViews { get; set; }
    public Boolean addNewOrderLine { get; set; }
    public Boolean addSpareParts { get; set; }
    public Boolean addServiceCharges { get; set; }
    public Boolean addPayment { get; set; }
    public Boolean takePayment { get; set; }
    public Boolean saveChanges { get; set; }
    public Boolean orderMat { get; set; }
    public Boolean checkMatAvail { get; set; }

    public Boolean isRebook 
    { 
        get
        {
            return ApexPages.currentPage().getParameters().containsKey('reBook');
        } 
        
        private set; 
    }
    
    public Boolean getShowPricing()
    {
        if (appSettings.SHOW_PRICING__c == 1) { return true; }
        return false;
    }
    
    public String getarticleSearchAutoStart ()
    {
        return appSettings.ARTICLESEARCH_FROMSTOCK_AUTOSEARCH__c ? '1' : '0';
    }
    
    public String articleIds
    {
        set;
        get
        {
            String artNoTravelTime = appSettings.ORDERFEEDBACK_ARTNO_TRAVELTIME__c;
            String artNoWorkTime = appSettings.ORDERFEEDBACK_ARTNO_WORKTIME__c;
            String articles = '';
            String qty = '&qtys=';
            for (SCboOrderLine orderLine :getBoOrderLinesEx())
            {
                if ((SCfwConstants.DOMVAL_ARTICLECLASS_SPAREPART == orderLine.orderLine.Article__r.Class__c) && 
                    (artNoTravelTime != orderLine.orderLine.Article__r.Name) && 
                    (artNoWorkTime != orderLine.orderLine.Article__r.Name))
                {
                    articles += orderLine.orderLine.Article__c + ',';
                    qty += orderLine.orderLine.Qty__c + ',';
                }
            }
            if (articles.length() > 0)
            {
                articles = articles.substring(0, articles.length() - 1);
                qty = qty.substring(0, qty.length() - 1);
            }
            
            articles += qty;

            System.debug('#### articleIds(): get Ids&Qtys -> ' + articles);
            return articles;
        }
    }
    
    List<SelectOption> resources = new List<SelectOption>();

    /**
     * Default constructor
     *
     */
    public SCOrderLineController()
    {    
        selectedResId = '';
        componentController = null;
        showPriceLists = false;
        showViews = true;
        addNewOrderLine = false;
        addSpareParts = false;
        addServiceCharges = false;
        addPayment = false;
        takePayment = false;
        saveChanges = false;
        orderMat = false;
        checkMatAvail = false;
        
        accessRights = new Set<String>();
        if (SCBase.isSet(appSettings.ORDERLINE_ACCESS_RIGHTS__c))
        {
            accessRights.addAll(appSettings.ORDERLINE_ACCESS_RIGHTS__c.split(','));
        }
    }

    /**
     * Retrieves true if the employee of the current assignment has a stock.
     *
     * @return    true if the current assignment has a stock
     */
    public Boolean getHasStock()
    {
       return (null != stock);
    } // getHasStock

    /**
     * Getter for access rights.
     */
    public Boolean getCanAddSpareParts()
    {
        return ((accessRights.size() == 0) || accessRights.contains('ADDPARTS'));
    }
    public Boolean getCanAddServiceCharges()
    {
        return ((accessRights.size() == 0) || accessRights.contains('ADDCHARGES'));
    }
    public Boolean getCanAddPayment()
    {
        return ((accessRights.size() == 0) || accessRights.contains('ADDPAYMENT'));
    }
    public Boolean getCanDelLine()
    {
        return ((accessRights.size() == 0) || accessRights.contains('DELLINE'));
    }
    public Boolean getCanSelPriceList()
    {
        return ((accessRights.size() == 0) || accessRights.contains('SELPRICELIST'));
    }
    public Boolean getCanDoPayment()
    {
        return (((accessRights.size() == 0) || accessRights.contains('DOPAYMENT')) && 
                (null != boOrder) && SCBase.isSet(boOrder.order.PaymentType__c));
    }
    public Boolean getCanChgQty()
    {
        return ((accessRights.size() == 0) || accessRights.contains('CHGQTY'));
    }
    public Boolean getCanChgNetPrice()
    {
        return ((accessRights.size() == 0) || accessRights.contains('CHGNETPRICE'));
    }
    public Boolean getCanChgDiscount()
    {
        return ((accessRights.size() == 0) || accessRights.contains('CHGDISCOUNT'));
    }
    public Boolean getCanChgInvType()
    {
        return ((accessRights.size() == 0) || accessRights.contains('CHGINVTYPE'));
    }
    public Boolean getCanChgTax()
    {
        return ((accessRights.size() == 0) || accessRights.contains('CHGTAX'));
    }
    public Boolean getCanOrderMat()
    {
        return ((accessRights.size() == 0) || accessRights.contains('ORDERMAT'));
    }
    
    public Boolean getIsOrderClosed()
    {
        return ((null == boOrder) || 
                (SCfwConstants.DOMVAL_ORDERSTATUS_CANCELLED == boOrder.order.Status__c) || 
                (SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL == boOrder.order.Status__c) || 
                 boOrder.order.InvoicingReleased__c);
    }
    
    public Boolean getShowValuationTypeColumn()
    {
        return true;
    
        //System.debug('###+++###boorder: ' + boOrder);
        
        if (stockValType == true) { return true; }
        
        if ((boOrder != null) && (boOrder.getIsOwnedByCustomer()))
        {
            stockValType = true;
            return true;
        }
        
        for (SCboOrderLine boOrderLine : getBoOrderLinesEx())
        {
            if (boOrderLine.orderLine.ValuationType__c != null)
            {
                stockValType = true;
                return true;
            }
        }
        
        return stockValType;
    }
    
    /**
     * Calculate the tax sum of shown order lines. Used after changing 
     * the order line type in the page.
     */
    public PageReference onCalculateSums()
    {
        if ((null != componentController) && 
            (componentController instanceOf SCOrderPageController))
        {
            ((SCOrderPageController)componentController).onCalculateSums();
        }
        else if (null != boOrder)
        {
            calculateSums(boOrder, getBoOrderLines(boOrder));
        }
        return null;
    } // onCalculateSums

    /**
     * Recalculate the pricing on order lines.
     *
     * @return    the page reference
     */
    public PageReference onReCalculatePricing()
    {
        if ((null != componentController) && 
            (componentController instanceOf SCOrderPageController))
        {
            
            ((SCOrderPageController)componentController).recalculatePricing();
        }
        else if (null != boOrder)
        {
            recalculatePricing(boOrder, getBoOrderLines(boOrder));
        }
        
        onRecalculateInvoicingType();
        return null;
    } // onReCalculatePricing


    /**
     * Recalculate the pricing on order lines.
     *
     * @return    the page reference
     */
    public PageReference onRecalculateInvoicingType()
    {
        if ((null != componentController) && 
            (componentController instanceOf SCOrderPageCCController))
        {
            
            //((SCOrderPageCCController)componentController).onRecalculateInvoicingType();
            SCOrderPageCCController pageController = (SCOrderPageCCController) componentController;
            system.debug('#### change pricelist from ' + boOrder.order.PriceList__c + ' to ' + pageController.order.order.PriceList__c);
            
            //pageController.order.order.PriceList__c =  boOrder.order.PriceList__c;
            pageController.onRecalculateInvoicingType();
        }
        else
        {
            calculateInvoicingType(boOrder);
        }
        return null;
    } // onReCalculatePricing
    
    /**
     * Do the actual pricing on the current order line.
     *
     * @return    the page reference
     */
    public PageReference calculatePricing()
    {
        if ((null != componentController) && 
            (componentController instanceOf SCOrderPageController))
        {
            ((SCOrderPageController)componentController).calculatePricing();
        }
        else if (null != boOrder)
        {
            calculatePricing(boOrder);
        }
        return null;
    } // calculatePricing

    /**
     * Gets a list of all available prices lists directly usable for 
     * a select list.
     *
     * @return    list of select options with the price lists
     */
    public List<SelectOption> getPriceLists()
    {
        if (null != boOrder)
        {
            return getPriceLists(boOrder);
        }
        else
        {
            return new List<SelectOption>();
        }
    } // getPriceLists
    
    
    
    /*
     * Methods for the valuationType selected list
     */
    
    private static Map<String,String> mapValuationTypeTranslation = new Map<String,String>();
    
    public List<SelectOption> getValuationTypeOptions()
    {
        List<SelectOption> listOption = new List<SelectOption>();
        listOption.add(new SelectOption('-none-', '-none-'));
        
        if ((mapValuationTypeTranslation == null) || (mapValuationTypeTranslation.size() == 0))
        {
            for (Schema.PicklistEntry schemaPickEntry : SCOrderLine__c.ValuationType__c.getDescribe().getPicklistValues())
            {
                mapValuationTypeTranslation.put(schemaPickEntry.getValue().toLowerCase(), schemaPickEntry.getLabel());
            }
        }
        
        for (String valType : mapValuationTypeTranslation.keySet())
        {
            listOption.add(new SelectOption(valType, mapValuationTypeTranslation.get(valType)));
        }
        return listOption;
    }
    
    /**
     * Get a list of all order lines for this order.
     * This includes order lines from order items as well as
     * order lines from the order itself.
     *
     * @return    List of business objects with all order lines
     */
    public List<SCboOrderLine> getBoOrderLinesEx()
    {
        System.debug('#### getBoOrderLinesEx()');
        if ((null != componentController) && 
            (componentController instanceOf SCOrderPageController))
        {
            return ((SCOrderPageController)componentController).getBoOrderLinesEx();
        }
        else if (null != boOrder)
        {
            return getBoOrderLinesEx(boOrder);
        }
        return new List<SCboOrderLine>();
    } // getBoOrderLinesEx
    
    /**
     * @return Prefix of Order line Object
     */
    public String getOrderLinePrefix()
    {
        return SCOrderLine__c.SObjectType.getDescribe().getKeyPrefix();
    } // getOrderLinePrefix
    
    /**
     * @return Field name of order
     */
    public String getOrderFieldName()
    {
        return appSettings.ORDER_FIELD_NAME__c;
    } // getOrderFieldName

    /**
     * Add a new order line for an article.
     *
     * @return    the page reference
     */
    public PageReference updateArticle()
    {
        System.debug('#### updateArticle()');
        System.debug('#### selectedArticleId -> ' + selectedArticleId);
        
        System.debug('#### selectedValuationType: ' + selectedValuationType);

        // selectedArticle Id is a list of comma separated SFDC IDs 
        // for article
        for (String processArticleId : selectedArticleId.split(','))
        {
            if ( boOrder.boOrderItems.size() > 0)
            {
                currentOrderLine = new SCboOrderLine(boOrder.order, boOrder.boOrderItems.get(0).orderitem.Id, null, null);
                boOrder.boOrderItems.get(0).boOrderLines.add(currentOrderLine);
                currentOrderLine.itemIdentifier = boOrder.boOrderItems.get(0).orderItem.InstalledBase__r.ProductModel__r.Name;
            }
            else
            {
                currentOrderLine = new SCboOrderLine(boOrder.order, null, null, null);
                boOrder.boOrderLines.add(currentOrderLine);
                currentOrderLine.itemIdentifier = boOrder.order.Name;
            }

            // new row added, so we need to set the line row
            currentLineRow = getBoOrderLines(boOrder).size() - 1;

            System.debug('processArticleId -> ' + processArticleId);
            System.debug('currentOrderLine -> ' + currentOrderLine);
            SCboArticle boArticle = new SCboArticle(processArticleId);
            
            currentOrderLine.orderLine.Article__c = boArticle.article.Id;
            currentOrderLine.orderLine.Article__r = boArticle.article;
            if (boArticle.article.Orderable__c || 
                boArticle.article.Stockable__c)
            {
                currentOrderLine.orderLine.MaterialStatus__c = SCfwConstants.DOMVAL_MATSTAT_ORDERABLE;
            } // if (boArticle.article.Orderable__c || ...
            else
            {
                currentOrderLine.orderLine.MaterialStatus__c = SCfwConstants.DOMVAL_MATSTAT_NOMATERIAL;
            } // else [if (boArticle.article.Orderable__c || ...]
     
            // we had to finde out the index of the new added order line in the list, 
            // because inside the calculatePrce methode the methode getBoOrderLinesEx
            // retrives the lins in another sorting
            Integer index = 0; 
            for (SCboOrderLine line :getBoOrderLinesEx(boOrder))
            {
                //not feasible. 
                //It is possible that an older orderline with the same 
                //name and invoice type with price == null already exists
                //This if statement selects the first positive result an may select the wrong index
                if ((SCfwConstants.DOMVAL_ORDERLINETYPE_INV == line.orderLine.Type__c) && 
                    (boArticle.article.Name == line.orderline.Article__r.name) && 
                    (null == line.orderline.UnitNetPrice__c))
                {
                    break;
                }
            }
            
            if (stockValType && currentOrderLine.orderline.Article__r.ValuationType__c && String.isNotBlank(selectedValuationType))
            {
                // GMSSU 16.04.2013 PMS 34794
                //currentOrderLine.orderLine.ValuationType__c = SCBase.getPicklistValue('SCStockItem__c', 'ValuationType__c', selectedValuationType);
                String valType = [select valuationtype__c from scstockitem__c where id = : selectedValuationType].valuationtype__c;
                currentOrderLine.orderLine.ValuationType__c = valType;
                
                //PMS 34692/Review: Review
                //currentOrderLine.orderLine.ValuationType__c = boOrder.getValuationTypePropsal();
                //currentOrderLine.orderLine.ValuationType__c = currentOrderLine.orderline.Article__r.ValuationType__c;
            }
            // do the price calculation for this order line
            currentLineRow -= index;
            curLineRow = getIndexOfBoOrderLineEx(currentOrderLine, boOrder);
            system.debug('#### currentRow: ' + curLineRow);
            if(curLineRow >= 0)
                calculatePricing();
        }

        return null;
    } // updateArticle

    /**
     * Add a new order line with the article, specified with ORDERLINE_ARTNO_PAYMENT.
     */
    public PageReference onAddPayment() 
    {
        if (null != boOrder)
        {
            onAddPayment(boOrder);
        }
        return null;
    } // onAddPayment

    /**
     * Starts the payment service and do all necessary steps after.
     *
     * @return    the page reference
     */
    public PageReference doPaymentService()
    {
        if (null != boOrder)
        {
            doPaymentService(boOrder);
        }
        if (isPaymentOk)
        {
            boOrder = null;
        }
        return null;
    } // doPaymentService

    /**
     * Gets the payment fee. 
     * The sum of all prices and taxes of all order lines with a selected flag.
     */
    public String getPaymentFee()
    {
        return getPaymentFee(boOrder);
    } // getPaymentFee

    /**
     * Retrieves true if the payment fee is greater than 0.
     */
    public Boolean getHasPaymentFee()
    {
        Double fee = (null != boOrder) ? getPaymentFeeVal(boOrder) : 0;
        return (fee > 0);
    } // getHasPaymentFee

    /**
     * Remove an order line and the dependend records like order repair codes.
     *
     * @return    the page reference
     */
    public PageReference removeOrderLine()
    {
        if ((null != componentController) && 
            (componentController instanceOf SCOrderPageController))
        {
            ((SCOrderPageController)componentController).removeOrderLine();
        }
        else if (null != boOrder)
        {
            removeOrderLine(boOrder);
        }
        return null;
    } // removeOrderLine

    /**
     * Saves the changes to the order.
     *
     * @return    the page reference
     */
    public PageReference onSave()
    {
        if (null != boOrder)
        {
            boOrder.save();
            boOrder = null;
        }
        return null;
    } // onSave

    /**
     * Resets the flag,that indocates, that the payment was ok.
     * Needed for calling the payment dialog more than once.
     *
     * @return    the page reference
     */
    public PageReference onResetPaymentFlag()
    {
        isPaymentOk = false;
        return null;
    } // onResetPaymentFlag

    /**
     * Refrehes the whole order data.
     *
     * @return    the page reference
     */
    public PageReference onRefreshData()
    {
        if (null != boOrder)
        {
            boOrder.readbyId(boOrder.order.Id);
        }
        return null;
    } // onRefreshData

    /**
     * Set the desired start and end in the order.
     *
     * @return    the page reference
     */
    public PageReference onSetDesiredStart()
    {
        Integer interval = 0;

        try
        {
            interval = appSettings.DEFAULT_CUSTOMERPREFINTERVALL__c.intValue();
        }
        catch (TypeException e)
        {
            interval = 0;
        }

        if (SCBase.isSet(desiredStart) && (null != boOrder))
        {
            Datetime desiredDt = Datetime.newInstance(Long.valueOf(desiredStart));
            boOrder.order.CustomerPrefStart__c = desiredDt.date();
            boOrder.order.CustomerPrefEnd__c = boOrder.order.CustomerPrefStart__c + interval;
        }
        return null;
    } // onSetDesiredStart
    
    public String getResourceId()
    {
        String str = '';
        
        if (boOrder != null && boOrder.appointments != null && boOrder.appointments.size() > 0)
        {
            //CCEAG SCAppointment__c app = boOrder.getFirstOpenAppointment();
            SCAppointment__c app = boOrder.appointments[0];
            if(app != null)
            {
                str = app.Assignment__r.Resource__c;
            }
        }
        
        return str;
    }
    
    /**
     * Reads all resources
     *
     * @return options list
     * @author Sergey Utko <sutko@gms-online.de>
     */
    public String selectedResId { get; set; }
    public List<SelectOption> getAllResources()
    {
        String selectResourceIds = getSelectResourceIds();
    
        String resString = selectResourceIds != null ? selectResourceIds : '';
        
        resString  += ';' + (userResourceId != null ? userResourceId : '');
        //resString  += ';' + (resourceId != null ? resourceId : '');
        
        SelectOption noneOption = new SelectOption('', System.Label.SC_flt_None);
        
        Set<Id> selRes = new Set<Id>();
        for (String resStr : resString.split(';',0))
        {
            try
            {
                Id curId = Id.valueOf(resStr);
                selRes.add(curId);
            }
            catch(Exception e) 
            { 
                System.debug('#### Error: ' + e.getMessage());
            }
        }
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(noneOption);
        
        SelectOption userOpt;
        
        List<SCResourceAssignment__c> resAssmnts = [
            Select Employee__c, Plant__c, Resource__c, Stock__r.Name 
            From SCResourceAssignment__c 
            Where Resource__c IN :selRes
            And ValidFrom__c <= today 
            And ValidTo__c >= today
            Order By Resource__r.Name
        ];
        
        if(!resAssmnts.isEmpty())
        {
            selectedResId = resAssmnts[0].Resource__c;
            
            for (SCResourceAssignment__c res : resAssmnts)
            {
                options.add(new SelectOption(res.Resource__c, res.Employee__c + ' (' + res.Plant__c + '/' + res.Stock__r.Name + ')'));
            }
            
            if (userOpt != null)
            {
                options.add(userOpt);
            }
            
            return options;
        }
        else
        {
            return null;
        }
    }
    
    
   /*
    * Used to pass the resource ids to the article search dialog
    */
    public String getSelectResourceIds()
    {
        String str = '';
        List<String> resources = getResourceFromAppointments();
        if(resources != null)
        {
            for(String res : resources)
            {
                str += res + ';';   
            }
        }
        
        system.debug('####na####' + str);

        /* OLD
        if (boOrder != null && boOrder.appointments != null)
        {
            for (Integer i=0; i<boOrder.appointments.size(); i++)
            {
                try 
                {
                    str += boOrder.appointments.get(i).Assignment__r.Resource__c + ';';
                } 
                catch (Exception e) 
                { 
                }
            }
        }
        */
        
        return str;
    }
    
    public String userResourceId
    {
        public get
        {
            if (userResourceId == null)
            {
                try
                {
                    userResourceId = [SELECT Id fROM SCResource__c WHERE Employee__c = :UserInfo.getUserId() LIMIT 1].Id;
                }
                catch (Exception e)
                {
                    userResourceId = 'none';
                    return null;
                }
            }
            else if (userResourceId == 'none')
            {
                return null;
            }
            return userResourceId;
        }
        
        private set;
    }
} // SCOrderLineController
