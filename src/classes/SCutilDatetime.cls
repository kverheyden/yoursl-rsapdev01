/*
 * @(#)SCutilDatetime.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Helper to converte text into date / time objects
 *
 * Please see SCBase for additional formatting tools (e.g. SCBase.FormatInterval(start, end)
 * 
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCutilDatetime
{
    // Object for application settings
    private static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();

    /**
     * Converts the input to a valid time.
     *
     * @param    timeStr    string that contains a time to be converted
     * @return   string that contains the converted time or the 
     *           input if no conversion is possible
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static String convertTime(String timeStr)
    {
        System.debug('### convertTime: input -> ' + timeStr);
        String retTime = timeStr;
        Boolean isAlphaNum = false;
        try
        {
            Integer.valueOf(timeStr);
            isAlphaNum = true;
        }
        catch (Exception e)
        {
        }
        
        // if input is like 1:30
        if ((timeStr.length() == 4) && (timeStr.indexOf(':') == 1))
        {
            retTime = '0' + timeStr;
        } // if ((timeStr.length() == 4) && (timeStr.indexOf(':') == 1))
        
        // if input is like :30
        if ((timeStr.length() == 3) && (timeStr.indexOf(':') == 0))
        {
            retTime = '00' + timeStr;
        } // if ((timeStr.length() == 3) && (timeStr.indexOf(':') == 0))
        
        // if input is like 5 or 12
        if ((timeStr.length() < 4) && isAlphaNum)
        {
            // set a new value for the input value, 
            // so the next code lines will convert the time 
            timeStr = (Integer.valueOf(timeStr) / 60.0).format();
            if (timeStr.indexOf(',') < 0)
            {
                timeStr += ',0';
            } // if (timeStr.indexOf(',') < 0)
            System.debug('### convertTime: new input -> ' + timeStr);
        } // if ((timeStr.length() < 4) && isAlphaNum)
        
        // if input is like 0,5 or 0.5 or 1,5 or 1.5
        timeStr = timeStr.replace(',', '.');
        if (timeStr.indexOf('.') >= 0)
        {
            try
            {
                System.debug('### convertTime: new input -> ' + timeStr);
                String hourStr = timeStr.substring(0, timeStr.indexOf('.'));
                System.debug('### convertTime: hourStr -> ' + hourStr);
                
                Decimal dur = Decimal.valueOf(timeStr);
                Integer hour = dur.intValue();
                dur = dur - hour;
                Integer minute = (dur * 60.0).round().intValue();
                System.debug('### convertTime: hour -> ' + hour);
                System.debug('### convertTime: minute -> ' + minute);
                
                retTime = ((hour < 10) ? '0' : '') + hour.format();
                retTime += ':';
                retTime += ((minute < 10) ? '0' : '') + minute.format();
            } // try
            catch (Exception e)
            {
                retTime = timeStr;
            } // catch (Exception e)
        } // if (timeStr.indexOf('.') >= 0)
        
        System.debug('### convertTime: output -> ' + retTime);
        return retTime;
    } // convertTime
    
    /**
     * Converts the input to a valid time.
     *
     * @param    timeStr    string that contains a time to be converted
     * @return   string that contains the converted time or the 
     *           input if no conversion is possible
     *
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static String convertTimeHour(String timeStr)
    {
        System.debug('### convertTimeHour: input -> ' + timeStr);
        String retTime = timeStr;
        Boolean isAlphaNum = false;
        try
        {
            Integer.valueOf(timeStr);
            isAlphaNum = true;
        }
        catch (Exception e)
        {
        }
        
        // if input is like 1:30
        if ((timeStr.length() == 4) && (timeStr.indexOf(':') == 1))
        {
            retTime = '0' + timeStr;
        } // if ((timeStr.length() == 4) && (timeStr.indexOf(':') == 1))
        
        // if input is like :30
        if ((timeStr.length() == 3) && (timeStr.indexOf(':') == 0))
        {
            retTime = '00' + timeStr;
        } // if ((timeStr.length() == 3) && (timeStr.indexOf(':') == 0))
        
        // if input is like 815
        if ((timeStr.length() == 3) && isAlphaNum)
        {
            retTime = '0' + timeStr.substring(0, 1) + ':' + timeStr.substring(1);
        } // if ((timeStr.length() == 3) && isAlphaNum)
        
        // if input is like 1445
        if ((timeStr.length() == 4) && isAlphaNum)
        {
            retTime = timeStr.substring(0, 2) + ':' + timeStr.substring(2);
        } // if ((timeStr.length() == 4) && isAlphaNum)
        
        // if input is like 8 15
        if ((timeStr.length() == 4) && (timeStr.indexOf(' ') == 1))
        {
            retTime = '0' + timeStr.substring(0, 1) + ':' + timeStr.substring(2);
        } // if ((timeStr.length() == 4) && (timeStr.indexOf(' ') == 1))
        
        // if input is like 14 45
        if ((timeStr.length() == 5) && (timeStr.indexOf(' ') == 2))
        {
            retTime = timeStr.substring(0, 2) + ':' + timeStr.substring(3);
        } // if ((timeStr.length() == 5) && (timeStr.indexOf(' ') == 2))
        
        // if input is like 12
        if ((timeStr.length() == 2) && isAlphaNum)
        {
            retTime = timeStr + ':00';
        } // if ((timeStr.length() == 2) && isAlphaNum)
        
        // if input is like 8
        if ((timeStr.length() == 1) && isAlphaNum)
        {
            retTime = '0' + timeStr + ':00';
        } // if ((timeStr.length() == 1) && isAlphaNum)
        
        System.debug('### convertTimeHour: output -> ' + retTime);
        return retTime;
    } // convertTimeHour

    /**
     * Rounds a date to a time grid, defined by the appsettings.
     *
     * @param    orgDate    datetime object which should be rounded
     * @return   datetime object that contains the rounded value
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static Datetime roundMinutes(Datetime orgDate)
    {
        Decimal grid = appSettings.DEFAULT_ROUNDMINUTES_GRID__c;
        if (null == grid)
        {
            grid = 15;
        } // if (null == grid)
        return roundMinutes(orgDate, grid.intValue());
    } // roundMinutes

    /**
     * Rounds a date to a time grid
     * Example: roundMinutes(orgDate, 15) fastens the date to a 15 minutes grid
     *
     * @param    orgDate    datetime object which should be rounded
     * @param    gridValue  grid for rounding
     * @return   datetime object that contains the rounded value
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static Datetime roundMinutes(Datetime orgDate, Integer gridValue)
    {
        Datetime roundDate;
        Integer cur = orgDate.minute();
        Integer grid = 0;

        if (gridValue != 0)
        {
            while (grid  < cur)
            {
                grid += gridValue;
            } // while (grid  < cur)
        } // if (gridValue != 0)

        if (grid > 60)
        {
             roundDate = Datetime.newInstance(orgDate.year(), orgDate.month(), orgDate.day(),
                                              orgDate.hour(), 0, 0).addHours(1);
        } // if (grid > 60)
        else
        {
             roundDate = Datetime.newInstance(orgDate.year(), orgDate.month(), orgDate.day(),
                                              orgDate.hour(), grid, 0);
        } // else [if (grid > 60)]

        return roundDate;
    } // roundMinutes
    
    /**
     * Calculates a duration between two times in minutes.
     * 
     * @param dateOne first date time
     * @param dateTwo secound date time
     * @return difference between dateOne and dateTwo in minutes
     * @author Dietrich Herdt <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static Integer minutesBetween(Datetime dateOne, Datetime dateTwo)
    {
        Integer minutes = Math.Floor((dateOne.getTime() - 
                                      dateTwo.getTime())/(1000.0*60.0)).abs().IntValue();

        return minutes;
    }
} // SCutilDatetime
