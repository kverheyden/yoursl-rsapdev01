/*
 * @(#)SCFixAppointmentControllerTest.cls SCCloud    hs 03.Mar.2010
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * @author HS <hschroeder@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCFixAppointmentControllerTest
{   
    /**
     * Controller under test
     */
    private static SCFixAppointmentController controller = new SCFixAppointmentController();
    
    /**
     * Test appointment to change
     */
    private static SCAppointment__c appointment;
    
    /**
     * Prepares the test
     */
    static void setUpBeforeClass()
    {
        // create test appointment
        if (appointment == null)
        {
            SCHelperTestClass.createOrderTestSet(true);
            appointment = SCHelperTestClass.appointments[0];
            System.debug('Test appointment: ' + appointment);
            
            // set appointmentID
            controller.appointmentID = appointment.ID;
        }
    }
    
    /**
     * Tests the hasMessage() method
     */
    static testMethod void testHasMessage()
    {
        setUpBeforeClass();
        Boolean result = controller.hasMessage;
        System.assert(result == false);
    }
            
    /**
     * Tests the title property
     */
    static testMethod void testTitle()
    {
        setUpBeforeClass();
        String title = controller.title;
        System.assert(title.length() > 0);
    }
    
    /**
     * Tests the startTime property
     */
    static testMethod void testStartTime()
    {
        setUpBeforeClass();
        String startTime = controller.startTime;
        System.assert(startTime.length() > 0);
    }
    
    /**
     * Tests the duration property
     */
    static testMethod void testDuration()
    {
        setUpBeforeClass();
        String duration = controller.duration;
        System.assert(duration.length() > 0);
    }
    
    /**
     * Tests the getStartTimes() method
     */
    static testMethod void testStartTimes()
    {
        setUpBeforeClass();
        // test underflow
        controller.startTimeID = '01:00';
        List<SelectOption> entries = controller.getStartTimes();
        // test round up
        controller.startTimeID = '10:40';
        entries = controller.getStartTimes();
        // test round down
        controller.startTimeID = '10:19';
        entries = controller.getStartTimes();
        // test overflow
        controller.startTimeID = '23:00';
        entries = controller.getStartTimes();
        // test normal
        controller.startTimeID = '11:00';
        entries = controller.getStartTimes();

        System.assert(entries.size() > 0);
    }
    
    /**
     * Tests the fixAppointment method
     */
    static testMethod void testFixAppointment()
    {
        setUpBeforeClass();
        String startTime = controller.startTimeID;
        // reschedule
        controller.startTimeID = '08:00';
        controller.fixAppointment();
        
        String message = controller.getMessage();
        System.assert(message != null);
    }
    
    /**
     * Tests the updateAppointment() method
     */
    static testMethod void testUpdateAppointment()
    {
        setUpBeforeClass();
        controller.description = 'Test change';
        controller.updateAppointment();
    }        
    
    /**
     * Tests the getMessage() method
     */
    static testMethod void testMessage()
    {
        setUpBeforeClass();
        String message = controller.getMessage();
    }
}
