/**********************************************************************

Name: PofSDocumentController

======================================================

Purpose: 

This class serves as the data management controller for all PofS document functions.

======================================================

History 

------- 

Date 			AUTHOR 				DETAIL

11/05/2013 		Oliver Preuschl 	INITIAL DEVELOPMENT

***********************************************************************/
public class PofSDocumentController{
    
    //Non-Static Variables-----------------------------------------------------------------------------
	public String GV_DocumentURL;
    private Map< String, Id > GM_DocumentFolders;
    
    public Document GO_UploadFile;
        
    public List< SelectOption > GL_Backgrounds;

    public List< SelectOption > GL_ClickAreaIcons;

    public List< SelectOption > GL_ClickAreaQuestions;

	//Constructors-----------------------------------------------------------------------------------
    public PofSDocumentController( ApexPages.StandardController PO_Controller ){
        GV_DocumentURL = 'https://' + getSFDDCInstance() + '.content.force.com/servlet/servlet.FileDownload?file=';
        GO_UploadFile = new Document();
    }

    public PofSDocumentController(){
        GV_DocumentURL = 'https://' + getSFDDCInstance() + '.content.force.com/servlet/servlet.FileDownload?file=';
        GO_UploadFile = new Document();
    }
	
	//getter & setter methods------------------------------------------------------------------------
	public String getGV_DocumentURL(){
		return GV_DocumentURL;
	}
	public void setGV_DocumentURL( String PL_Value ){
		GV_DocumentURL = PL_Value;
	}

	public Document getGO_UploadFile(){
		return GO_UploadFile;
	}
	public void setGO_UploadFile( Document PL_Value ){
		GO_UploadFile = PL_Value;
	}

	public List< SelectOption > getGL_Backgrounds(){
		if( GL_Backgrounds == null ){
            GL_Backgrounds = getFiles( 'PofSBackgrounds' );
        }
		return GL_Backgrounds;
	}
	public void setGL_Backgrounds( List< SelectOption > PL_Value ){
		GL_Backgrounds = PL_Value;
	}
	
	public List< SelectOption > getGL_ClickAreaIcons(){
		if( GL_ClickAreaIcons == null ){
            GL_ClickAreaIcons = getFiles( 'PofSClickareaIcons' );
        }
		return GL_ClickAreaIcons;
	}
	public void setGL_ClickAreaIcons( List< SelectOption > PL_Value ){
		GL_ClickAreaIcons = PL_Value;
	}
	
	public List< SelectOption > getGL_ClickAreaQuestions(){
		if( GL_ClickAreaQuestions == null ){
            GL_ClickAreaQuestions = getFiles( 'PofSRedSurveyPictures' );
        }
		return GL_ClickAreaQuestions;
	}
	public void setGL_ClickAreaQuestions( List< SelectOption > PL_Value ){
		GL_ClickAreaQuestions = PL_Value;
	}

	//Action Methods---------------------------------------------------------------------------
	
	/*******************************************************************
	 Purpose: 		Get the SFDC instance
	 Return:		Instance String
	 ********************************************************************/
	private String getSFDDCInstance(){
		String LV_Instance = URL.getSalesforceBaseUrl().getHost();
		List< String > LL_Instance = LV_Instance.split( '\\.' );
		return LL_Instance.get( 0 ) + '.' + LL_Instance.get( 1 );
	}
	
	/*******************************************************************
	 Purpose: 		Get the folder id of a specific folder
	 Parameters:	String PV_FolderName
	 Return:		Folder Id
	 ********************************************************************/
	private Id getDocumentFolder( String PV_FolderName ){
		System.debug( 'Entering getDocumentFolder: ' + PV_FolderName );
        if( GM_DocumentFolders == null ){
            GM_DocumentFolders = new Map< String, Id >();
        }
        if( !GM_DocumentFolders.containsKey( PV_FolderName ) ){
            Id LV_DocumentFolderId = Id.valueOf( PofSDocumentsFolder__c.getInstance( PV_FolderName ).FolderId__c );
            GM_DocumentFolders.put( PV_FolderName, LV_DocumentFolderId );
        }
		System.debug( 'Exiting getDocumentFolder: ' + GM_DocumentFolders.get( PV_FolderName ) );
        return GM_DocumentFolders.get( PV_FolderName );
    }
    
	/*******************************************************************
	 Purpose: 		Get all files contained in a specific folder
	 Parameters:	String PV_FolderName
	 Return:		List of files
	 ********************************************************************/
    private List< SelectOption > getFiles( String PV_FolderName ){
		System.debug( 'Entering getFiles: ' + PV_FolderName );
        ID LV_FolderID = getDocumentFolder( PV_FolderName );
        List< Document > LL_Files = [ SELECT ID, Name FROM Document WHERE ( FolderID =: LV_FolderID ) AND ( Name != 'None' ) Order By Name ];
        //List< Document > LL_NoFiles = [ SELECT ID, Name FROM Document WHERE ( Name = 'None' ) ];
        String LV_NoFileId = '000000000000000';
        //if( LL_NoFiles.size() > 0 ){
        //    LV_NoFileId = LL_NoFiles[ 0 ].Id;
        //}
        List< SelectOption > LL_FileOptions = new List< SelectOption >();
        LL_FileOptions.add( new SelectOption( LV_NoFileId, 'None' ) );
        for( Document LO_File: LL_Files ){
            LL_FileOptions.add( new SelectOption( LO_File.Id, LO_File.Name ) );
        }
		System.debug( 'Exiting getFiles: ' + LL_FileOptions );
        return LL_FileOptions;
    }
    
	/*******************************************************************
	 Purpose: 		Get the id of a file for a given name
	 Parameters:	String PV_FolderName, String PV_FileName 
	 Return:		Id of the file
	 ********************************************************************/
    private String getFileIdByName( String PV_FolderName, String PV_FileName ){
		System.debug( 'Entering getFileIdByName: ' + PV_FolderName + ', ' + PV_FileName );
        ID LV_FolderID = getDocumentFolder( PV_FolderName );
        List< Document > LL_Files = [ SELECT ID FROM Document WHERE ( ( FolderID =: LV_FolderID ) AND ( Name =: PV_FileName ) ) Order By CreatedDate DESC ];
        if( LL_Files.size() > 0 ){
			System.debug( 'Exiting getFileIdByName: ' + LL_Files.get( 0 ).Id );
        	return LL_Files.get( 0 ).Id;
		}else{
			System.debug( 'Exiting getFileIdByName: ' + '000000000000000' );
			return '000000000000000';
		}
    }
    
	/*******************************************************************
	 Purpose: 		save a new background document
	 Return:		the id of the uploaded file
	 ********************************************************************/
    public Id uploadBackground(){
		System.debug( 'Entering uploadBackground: ' );
        if( GO_UploadFile != null ){
            GO_UploadFile.FolderId = getDocumentFolder( 'PofSBackgrounds' );
        	insert( GO_UploadFile );
        }
		System.debug( 'Exiting uploadBackground: ' );
        return GO_UploadFile.Id;
    }
    
	/*******************************************************************
	 Purpose: 		Initialize the list of backgrounds with null so that it will be reloaded
	 ********************************************************************/
    public void refreshGL_Backgrounds(){
		System.debug( 'Entering refreshGL_Backgrounds: ' );
        GL_Backgrounds = null;
        System.debug( 'Exiting refreshGL_Backgrounds: ' );
    }

	/*******************************************************************
	 Purpose: 		Get a specific background image for a given name
	 Return:		Id of the file
	 ********************************************************************/
    public String getBackgroundByName( String PV_BackgroundName ){
		System.debug( 'Entering getBackgroundByName: ' + PV_BackgroundName );
		System.debug( 'Exiting getBackgroundByName: ' + getFileIdByName( 'PofSBackgrounds', PV_BackgroundName ) );
        return getFileIdByName( 'PofSBackgrounds', PV_BackgroundName );
    }
    
	/*******************************************************************
	 Purpose: 		upload a new ClickArea image
	 Return:		The id of the document
	 ********************************************************************/
    public Id uploadClickAreaQuestion(){
		System.debug( 'Entering uploadClickAreaQuestion: ' );
        if( GO_UploadFile != null ){
            GO_UploadFile.FolderId = getDocumentFolder( 'PofSRedSurveyPictures' );
        	insert( GO_UploadFile );
        }
		System.debug( 'Exiting uploadClickAreaQuestion: ' + GO_UploadFile.Id );
        return GO_UploadFile.Id;
    }
    
	/*******************************************************************
	 Purpose: 		Initialize the list of ClickArea images with null so that it will be reloaded
	 ********************************************************************/
    public void refreshGL_ClickAreaQuestionImages(){
		System.debug( 'Entering refreshGL_ClickAreaQuestionImages: ' );
        GL_ClickAreaQuestions = null; 
		System.debug( 'Exiting refreshGL_ClickAreaQuestionImages: ' );
    }
	
	/*******************************************************************
	 Purpose: 		Get a specific ClickArea image for a given name
	 ********************************************************************/
	public String getClickAreaQuestionImageByName( String PV_ClickAreaQuestionImageName ){
		System.debug( 'Entering getClickAreaQuestionImageByName: ' + PV_ClickAreaQuestionImageName );
		System.debug( 'Exiting getClickAreaQuestionImageByName: ' + getFileIdByName( 'PofSRedSurveyPictures', PV_ClickAreaQuestionImageName ) );
        return getFileIdByName( 'PofSRedSurveyPictures', PV_ClickAreaQuestionImageName );
    }
}
