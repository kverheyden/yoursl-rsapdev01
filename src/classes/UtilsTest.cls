/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class UtilsTest {

    static testMethod void testGetIdPrefix() {
        Account a = new Account(Name = 'Some Account');
        insert a;
        
        String idPrefix = Utils.getIdPrefix(Account.SObjectType);
        
        System.assert(((String)a.Id).startsWith(idPrefix));
    }
    
    static testMethod void testGetSFDCContentURL() {
    	Account a = new Account(Name = 'Some Account');
    	insert a;
    	
    	Attachment att = new Attachment(Name='Some Attachment', ParentId = a.Id);
    	att.Body = Blob.valueOf('Some content');
    	insert att;	
    	
    	String contentUrl = Utils.getSFDCContentURL(att.Id);
    	System.assert(contentUrl.endsWith('.content.force.com/servlet/servlet.ImageServer?' + att.Id));
    }
    
    static testMethod void testGetSFDCInstance() {
    	String host = URL.getSalesforceBaseUrl().getHost();
    	System.assert(host.contains(Utils.getSFDCInstance()));
    }
    
    static testMethod void testGetSFDCVisualforceURL() {
    	String pageUrl = Utils.getSFDCVisualforceURL('SOME_PAGE');
    	System.assert(pageUrl.endsWith('.visual.force.com/apex/SOME_PAGE'));
    }
    
    
    // fSFA specific test
    static testMethod void testGetFieldSetMemberStringList() {
    	List<String> fieldSetMemberStringList = Utils.GetFieldSetMemberStringList(Account.SObjectType, 'SalesAppQueryAccountWithDetailsFields');
    	System.assert(fieldSetMemberStringList.size() > 0);
    }
    
    static testMethod void testGetPicklistEntryMap() {
    	Map<String, String> mapPicklistEntryMap = Utils.getPicklistEntryMap(Task.Status);
    	System.assert(mapPicklistEntryMap.size() > 0);
    }
}
