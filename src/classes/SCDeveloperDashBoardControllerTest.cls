/*
 * @(#)SCDeveloperDashBoardControllerTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
class SCDeveloperDashBoardControllerTest
{
    public static testMethod void testOnSearch()
    {
        System.currentPagereference().getParameters().put('search', 'Account');
        SCDeveloperDashBoardController dashBoard = new SCDeveloperDashBoardController();
        System.assertEquals(UserInfo.getOrganizationId(), dashBoard.getOrgId());
        
        dashBoard.search = '' ;
        dashBoard.onSearch();
        
        dashBoard.search = 'Order';
        dashBoard.onSearch();
        
        dashBoard.radioSel = '1';
        dashBoard.resource.Employee__c = null;
        dashBoard.days = null;
        dashBoard.onSearch();
    }

    public static testMethod void testEditFavorites()
    {
        SCDeveloperDashBoardController dashBoard = new SCDeveloperDashBoardController();
        System.assertEquals(false, dashBoard.editFavorites);
        dashBoard.onEditFavorites();
        System.assertEquals(true, dashBoard.editFavorites);
        dashBoard.onCancel();
        System.assertEquals(false, dashBoard.editFavorites);
        dashBoard.onEditFavorites();
        
        dashBoard.curClassIds = dashBoard.allClasses[0].getValue();
        dashBoard.curPageIds = dashBoard.allPages[0].getValue();
        dashBoard.curTriggerIds = dashBoard.allTriggers[0].getValue();
        dashBoard.curComponentIds = dashBoard.allComponents[0].getValue();
        dashBoard.curObjectIds = dashBoard.allObjects[0].getValue();
        dashBoard.curOtherIds = dashBoard.allOthers[0].getValue();
            
        dashBoard.onSaveFavorites();
        
        List<SCDeveloperFavorite__c> favorites = [Select Id from SCDeveloperFavorite__c where User__c = :UserInfo.getUserId()];
        System.assertEquals(true, favorites.size() > 0);
    }
}
