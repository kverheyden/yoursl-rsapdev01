/**
* @author           Oliver Preuschl
*                   H&W Consult GmbH
*                   Bahnhofstr. 3
*                   21244 Buchholz i.d.N.
*                   http://www.hundw.com
*
* @description      Sends internal information emails when a custom document is about to expire (in two weeks)
*
* @date             22.07.2014
*
* Timeline:
* Name              DateTime                Version        Description
* Oliver Preuschl   22.07.2014              *1.0*          Creatd the class
*/

public with sharing class fsFACustomDocumentAlert {
	
	//Map of Lists of the expiring custpm documents per OwnerId
	private Map< Id, List< CustomDocument__c > > GM_OwnerIds_ExpiringCustomDocuments{
		get{
			if( GM_OwnerIds_ExpiringCustomDocuments == null ){
				GM_OwnerIds_ExpiringCustomDocuments = new Map< Id, List< CustomDocument__c > >();
				//Date LV_SendingDate = Date.Today() + 14;
				for( CustomDocument__c LO_CustomDocument: [ SELECT Id, Name, OwnerId FROM CustomDocument__c WHERE ValidTo__c =: GV_SendingDate ] ){
					if( !GM_OwnerIds_ExpiringCustomDocuments.containsKey( LO_CustomDocument.OwnerId ) ){
						GM_OwnerIds_ExpiringCustomDocuments.put( LO_CustomDocument.OwnerId, new List< CustomDocument__c >() );
					}
					GM_OwnerIds_ExpiringCustomDocuments.get( LO_CustomDocument.OwnerId ).add( LO_CustomDocument );
				}
			}
			return GM_OwnerIds_ExpiringCustomDocuments;
		}
		set;

	}

	private Date GV_SendingDate{
		get{
			if( GV_SendingDate == null ){
				Integer LV_ExpirationThresholdDays = 14;
		        SalesAppSettings__c LO_ExpirationThresholdDays = SalesAppSettings__c.getInstance( 'CustomDocumentExpirationThresholdDays' );
		        if( LO_ExpirationThresholdDays != null ){
		            try{
		                LV_ExpirationThresholdDays = Integer.valueOf( LO_ExpirationThresholdDays.Value__c );
		            }catch( Exception LO_Exception ){
		                LV_ExpirationThresholdDays = 14;
		            }
		        }
		    	GV_SendingDate = System.today() + LV_ExpirationThresholdDays;
			}
			return GV_SendingDate;
		}
		set;
	}

	//The Email tempate that is used for the alert
	private EmailTemplate GO_EmailTemplate{
		get{
			if( GO_EmailTemplate == null ){
				try{
					GO_EmailTemplate = [ SELECT Id, Subject, HTMLValue, Body FROM EmailTemplate WHERE DeveloperName = 'CustomDocumentExpirationAlert' ];
				}catch( Exception LO_Exception ){
					System.debug( 'The Email Template "CustomDocumentExpirationAlert" could not be found.' );
				}
			}
			return GO_EmailTemplate;
		}
		set;
	}


	public fsFACustomDocumentAlert() {
	}

	//Sends the expiration alert emails
	public void sendExpiringCustomDocumentsEmails(){
		//If the Email tempate was not found do nothing
		if( GO_EmailTemplate != null ){
			List< Messaging.SingleEmailMessage > LL_Emails = new List< Messaging.SingleEmailMessage >();
			//Loop over the OwnerIds, a summary email will be send for every owner
			for( Id LV_OwnerId: GM_OwnerIds_ExpiringCustomDocuments.KeySet() ){
				//Get the custom documents that are about to expire for the current ownerid
				List< CustomDocument__c > LL_ExpiringCustomDocuments = GM_OwnerIds_ExpiringCustomDocuments.get( LV_OwnerId );
				//Generate the customer document list for the email
				String LV_ExpiringCustomDocumentsListHTML = '';
				String LV_ExpiringCustomDocumentsListText = '';
				for( CustomDocument__c LO_CustomDocument: LL_ExpiringCustomDocuments ){
					LV_ExpiringCustomDocumentsListHTML += '<br><a href="' + URL.getSalesforceBaseURL().toExternalForm() + '/' + LO_CustomDocument.Id + '">' + LO_CustomDocument.Name + '</a>';
					LV_ExpiringCustomDocumentsListText += '\n\r' + LO_CustomDocument.Name + ' (' + URL.getSalesforceBaseURL().toExternalForm() + '/' + LO_CustomDocument.Id + ')';
				}
				//Generate the email bodies
				String LV_EmailHTMLBody = GO_EmailTemplate.HTMLValue.replace( '{!ExpiringCustomDocuments}', LV_ExpiringCustomDocumentsListHTML );
				String LV_EmailTextBody = GO_EmailTemplate.Body.replace( '{!ExpiringCustomDocuments}', LV_ExpiringCustomDocumentsListText );
				//Generate the email
				Messaging.SingleEmailMessage LO_Email = new Messaging.SingleEmailMessage();
				LO_Email.setTargetObjectId( LV_OwnerId );
				//LO_Email.ToAddresses = new List< String >{ 'activation@hundw.com' };
				LO_Email.setSubject( GO_EmailTemplate.Subject );
				LO_Email.setHTMLBody( LV_EmailHTMLBody );
				LO_Email.setPlainTextBody( LV_EmailTextBody );
				LO_Email.saveAsActivity = false;
				LL_Emails.add( LO_Email );
			}

			//Check if the neccessary email capacits is still available and send the emails
			Boolean LV_EmailCapacityExeeded;
			try{
				Messaging.reserveSingleEmailCapacity( LL_Emails.size() );
				LV_EmailCapacityExeeded = false;
			}catch( Exception LO_Exception ){
				LV_EmailCapacityExeeded = true;
				System.debug( 'The EmailCapacity is exeeded.' );
			}
			if( !LV_EmailCapacityExeeded ){
				Messaging.sendEmail( LL_Emails );
			}
		}
	}
}
