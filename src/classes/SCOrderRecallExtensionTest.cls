/*
 * @(#)SCOrderRecallExtensionTest.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCOrderRecallExtensionTest
{   
    static testMethod void testOrderRecall()
    {   
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        SCHelperTestClass.createOrderTestSet3(true);
        
        
        
        
        // set an order reason with a recall period of 30 days in the order
        
        // set the id2 in the order, should be removed in the recall order, to prevent errors
        SCHelperTestClass.order.Id2__c = 'GMS TEST order Recall';
        update SCHelperTestClass.order;
        
        // add some time reports
        List<SCTimeReport__c> timeReports = new List<SCTimeReport__c>();
        Datetime now = Datetime.now();
        // the dates in the time reports are set 40 days in the past
        Datetime startTime = Datetime.newInstance(now.year(), now.month(), now.day(), 8, 0, 0).addDays(-40);
        Datetime driveTime = Datetime.newInstance(now.year(), now.month(), now.day(), 9, 0, 0).addDays(-40);
        Datetime endTime = Datetime.newInstance(now.year(), now.month(), now.day(), 10, 0, 0).addDays(-40);
        // day start
        timeReports.add(new SCTimeReport__c(ID2__c = 'timeReport 1',
                            Resource__c = SCHelperTestClass.resources.get(3).Id,
                            Start__c = startTime,
                            End__c = startTime,
                            Type__c = SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART,
                            Status__c = SCfwConstants.DOMVAL_TIMEREPORTSTATUS_OPENED,
                            CompensationType__c = SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_DEFAULT,
                            Description__c = 'day start',
                            Mileage__c = 12987,
                            ReportLink__c = null,
                            Assignment__c = null,
                            Calendar__c = null));
        // drive time
        timeReports.add(new SCTimeReport__c(ID2__c = 'timeReport 2',
                            Resource__c = SCHelperTestClass.resources.get(3).Id,
                            Start__c = startTime,
                            End__c = driveTime,
                            Type__c = SCfwConstants.DOMVAL_TIMEREPORTTYPE_DRIVETIME,
                            Order__c = SCHelperTestClass.order.Id, 
                            Status__c = SCfwConstants.DOMVAL_TIMEREPORTSTATUS_OPENED,
                            CompensationType__c = SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_DEFAULT,
                            Description__c = 'drive time',
                            Distance__c = 20,
                            ReportLink__c = null, 
                            Assignment__c = null,
                            Calendar__c = null));
        // working time
        timeReports.add(new SCTimeReport__c(ID2__c = 'timeReport 3',
                            Resource__c = SCHelperTestClass.resources.get(3).Id,
                            Start__c = driveTime,
                            End__c = endTime,
                            Type__c = SCfwConstants.DOMVAL_TIMEREPORTTYPE_WORKTIME,
                            Order__c = SCHelperTestClass.order.Id, 
                            Status__c = SCfwConstants.DOMVAL_TIMEREPORTSTATUS_OPENED,
                            CompensationType__c = SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_DEFAULT,
                            Description__c = 'work time',
                            Distance__c = null,
                            ReportLink__c = null, 
                            Assignment__c = null,
                            Calendar__c = null));
        // day end
        timeReports.add(new SCTimeReport__c(ID2__c = 'timeReport 4',
                            Resource__c = SCHelperTestClass.resources.get(3).Id,
                            Start__c = endTime,
                            End__c = endTime,
                            Type__c = SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYEND,
                            Status__c = SCfwConstants.DOMVAL_TIMEREPORTSTATUS_CLOSED,
                            CompensationType__c = SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_DEFAULT,
                            Description__c = 'day closed',
                            Mileage__c = 12997,
                            ReportLink__c = null, 
                            Assignment__c = null,
                            Calendar__c = null));
        insert timeReports;
        
        Test.startTest();
        
        // try to create a recall for an open order
        ApexPages.StandardController controller = new ApexPages.StandardController(SCHelperTestClass.order);
        SCOrderRecallExtension recallExt = new SCOrderRecallExtension(controller);
        System.assertEquals(false, recallExt.canRecall);
        System.assertEquals(false, recallExt.recallNotValid);
        System.assertNotEquals(0, recallExt.getLocationAddress().length());
        System.assertEquals(false, recallExt.getIsCustomerLocked());
        
        // set the order status to 'completed'
        SCOrder__c order = new SCOrder__c(Id = SCHelperTestClass.order.Id, 
                                          Status__c = SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED, 
                                          Closed__c = Datetime.newInstance(2012, 3, 22, 10, 0, 0));
        update order;

        // creation the recall is still not possible, because the time reports are to far in the past
        recallExt = new SCOrderRecallExtension(controller);
        System.assertEquals(true, recallExt.canRecall);
        //System.assertEquals(true, recallExt.recallNotValid);
        
        // correct the time reports
        for (SCTimeReport__c report :timeReports)
        {
            report.Start__c = report.Start__c.addDays(30);
            report.End__c = report.End__c.addDays(30);
        }
        update timeReports;
        
        // recall is now possible
        recallExt = new SCOrderRecallExtension(controller);
        //System.assertEquals(true, recallExt.canRecall);
        //System.assertEquals(false, recallExt.recallNotValid);
        
        // save without a recall reason
        PageReference page = recallExt.onSave();
        //System.assertEquals(null, page);
        
        // save without a recall reason description (necessary when reason = 'other')
        recallExt.newOrder.RecallReason__c = 'other';
        page = recallExt.onSave();
        //System.assertEquals(null, page);
        
        // save with all data
        recallExt.newOrder.RecallReasonDescription__c = 'test';
        page = recallExt.onSave();
        //System.assertNotEquals(null, page);
        //System.assertEquals(true, page.getUrl().contains(appSettings.ORDERCREATION_PAGE__c));
        
        // return page should contains all three parameter
        Map<String, String> paramMap = page.getParameters();
        //System.assertNotEquals(null, paramMap.get('aid'));
        //System.assertNotEquals(null, paramMap.get('oid'));
        //System.assertNotEquals(null, paramMap.get('emplId'));
        
        SCOrder__c newOrder = [Select Id, OrderOrigin__c from SCOrder__c where Id = :paramMap.get('oid')];
        
        //System.assertEquals(SCHelperTestClass.order.Id, newOrder.OrderOrigin__c);
        
        Test.stopTest();
    }
}
