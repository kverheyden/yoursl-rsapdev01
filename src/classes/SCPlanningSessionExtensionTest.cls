/*
 * @(#)SCPlanningSessionExtensionTest.cls
 *
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Testclass for the "Contract creation Wizard"
 * 
 * @author Alexei Geiger <ageiger@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCPlanningSessionExtensionTest
{

    private static SCPlanningSessionExtension pse;
    
    static testMethod void planningSessionExtensionPositiv1()
    {   
        SCHelperTestClass.createOrderItem(true);
        SCHelperTestClass.createOrderRoles(true);
        ApexPages.StandardController sc = new ApexPages.StandardController(SCHelperTestClass.orderItem);
     
        pse = new SCPlanningSessionExtension(sc, true);
        //System.assertEquals(true, pse.GetCanDispatch());
        pse.getCanHaveMoreAppointments();
        System.assertEquals(false, pse.getIsCustomerLocked());
    }
    
    static testMethod void planningSessionExtensionPositiv2()
    {
        pse = new SCPlanningSessionExtension();
        pse.getService();
        pse.getCodebase();
        pse.getUserLang();
        //System.assertEquals(false, pse.GetCanDispatch());
        pse.sessionID = '3242gfd43241212';
    }
}
