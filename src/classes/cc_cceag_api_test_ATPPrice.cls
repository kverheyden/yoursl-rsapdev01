/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class cc_cceag_api_test_ATPPrice {
 
    static testMethod void myUnitTest() {
    	ccrz__E_AccountGroup__c agA = new ccrz__E_AccountGroup__c(Name='A');
		insert agA;
		 //create an account with the account group
        Account acc = new Account();
     	acc.Name = 'daoTestAccount';
     	acc.AccountNumber = '123456';
     	acc.Subtradechannel__c = '0';
     	acc.ccrz__E_AccountGroup__c = agA.Id;
     	insert acc;
     	//create the billTo and shipTo contacts including the partnerId for the account
        ccrz__E_ContactAddr__c billTo = new ccrz__E_ContactAddr__c(
			ccrz__AddressFirstline__c='100 Pine Street', ccrz__City__c='SmallTown', ccrz__DaytimePhone__c='(847) 555-1212',
			ccrz__FirstName__c='Test', ccrz__HomePhone__c='(847) 555-1212', ccrz__LastName__c='User',
			ccrz__PostalCode__c='60601', ccrz__State__c='Idaho', ccrz__StateISOCode__c='ID',
			ccrz__CountryISOCode__c='USA');
		ccrz__E_ContactAddr__c shipTo = new ccrz__E_ContactAddr__c(
			ccrz__AddressFirstline__c='100 Pine Street', ccrz__City__c='SmallTown', ccrz__DaytimePhone__c='(847) 555-1212',
			ccrz__FirstName__c='Craig', ccrz__HomePhone__c='(847) 555-1212', ccrz__LastName__c='Traxler',
			ccrz__PostalCode__c='60601', ccrz__State__c='Idaho', ccrz__StateISOCode__c='ID',
			ccrz__CountryISOCode__c='USA', ccrz__Partner_Id__c=acc.AccountNumber);
		insert new List<ccrz__E_ContactAddr__c> {billTo, shipTo};
        ccrz__E_Cart__c cart = new ccrz__E_Cart__c(
        	ccrz__BillTo__c = billTo.Id,
			ccrz__ShipTo__c = shipTo.Id,
            ccrz__SessionID__c = 'test session',
            ccrz__storefront__c='DefaultStore',
            ccrz__RequestDate__c = Date.today(),
            ccrz__ActiveCart__c = true,
            ccrz__CartStatus__c = 'Open',
            ccrz__CartType__c = 'Cart'
        );
        insert cart;
        ccrz__E_Product__c p1 = new ccrz__E_Product__c(Name='Product1', ccrz__Sku__c='sku1', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12);
        ccrz__E_Product__c p2 = new ccrz__E_Product__c(Name='Product2', ccrz__Sku__c='sku2', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12);
        ccrz__E_Product__c p3 = new ccrz__E_Product__c(Name='Product3', ccrz__Sku__c='sku3', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12);
        ccrz__E_Product__c p4 = new ccrz__E_Product__c(Name='Product4', ccrz__Sku__c='sku4', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12);
        ccrz__E_Product__c p1_1 = new ccrz__E_Product__c(Name='Product1_1', ccrz__Sku__c='sku1_1', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12);
        ccrz__E_Product__c p1_2 = new ccrz__E_Product__c(Name='Product1_2', ccrz__Sku__c='sku1_2', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12);
        ccrz__E_Product__c p1_3 = new ccrz__E_Product__c(Name='Product1_3', ccrz__Sku__c='sku1_3', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12);
        
         List<ccrz__E_Product__c> ps = new List<ccrz__E_Product__c>{
            p1,p2,p3,p4,p1_1,p1_2,p1_3
        };
        insert ps;
        List<ccrz__E_CartItem__c> cartItems = new List<ccrz__E_CartItem__c> {
            new ccrz__E_CartItem__c(ccrz__Quantity__c=4, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku1'), ccrz__cart__c=cart.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0, ccrz__cartItemType__c='Major'),
            new ccrz__E_CartItem__c(ccrz__Quantity__c=4, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku2'), ccrz__cart__c=cart.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0, ccrz__cartItemType__c='Major'),
            new ccrz__E_CartItem__c(ccrz__Quantity__c=4, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku3'), ccrz__cart__c=cart.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0, ccrz__cartItemType__c='Major')
        };
        insert cartItems;
        CCSettings__c ccSettings = new CCSettings__c();
        ccSettings.SAPDispBasicAuthUsername__c = 'user';
        ccSettings.SAPDispBasicAuthPassword__c = 'passsword';
        ccSettings.SAPDispEndpointAssetInventory__c = 'AssettEndpoint';
        ccSettings.SAPDispServer__c = 'Server';
        ccSettings.SAPDispEndpointSalesOrderRequestResponse__c='Server';
        insert ccSettings;
        Test.startTest();
        cart = cc_cceag_dao_Cart.retrieveCartWithItems(cart.id);
        Test.setMock(WebServiceMock.class, new cc_cceag_ATPCalloutMock());
        cc_cceag_api_ATPPrice atpPrice = new cc_cceag_api_ATPPrice();
        ccrz.cc_bean_CartSummary cartBean = new ccrz.cc_bean_CartSummary(cart, true);
        list<ccrz.cc_bean_CartItem> cartItemBeans = new list<ccrz.cc_bean_CartItem>(); 
        for(ccrz__E_CartItem__c ci : cart.ccrz__E_CartItems__r) {
        	ccrz.cc_bean_CartItem cib = new ccrz.cc_bean_CartItem();
        	cib.quantity = integer.valueOf(ci.ccrz__Quantity__c);
        	cib.itemID = ci.id;
        	cartItemBeans.add(cib);
        }
        cartBean.cartItems = cartItemBeans;
        Map<String, integer> zfkMap = new Map<String, integer>{
        	p1.id=>1, p2.id=>5, p3.id=>5, p4.id=>5
        };
        
        cartBean.extrinsic.put('zfkMap', json.serialize(zfkMap));
        cartBean.extrinsic.put('zfkMiamfoac', '10');
        
        Map<String, Object> inputData = new Map<String, Object> {
          ccrz.cc_api_cartExtension.PARAM_CARTBEAN=>cartBean
        };
        atpPrice.computePricingCart(inputData);
        cartBean.extrinsic.put('context', 'executePrice');
        atpPrice.computePricingCart(inputData);
        Test.stopTest();
    }
}
