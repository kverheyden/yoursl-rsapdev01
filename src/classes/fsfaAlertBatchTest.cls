/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class fsfaAlertBatchTest {

    static testMethod void myUnitTest() {
    	List<PictureOfSuccess__c> listPofs	= new List<PictureOfSuccess__c>();
    	date validToDate				= date.today()+14; 
        PictureOfSuccess__c pofs 		= new PictureOfSuccess__c();
        pofs.Bezeichnung__c				= 'Test 1';
        pofs.ValidFrom__c				= date.today();
        pofs.ValidUntil__c				= validToDate;
        pofs.Subtradechannel__c			= 'xx';
        pofs.RedWeightingActivation__c	= 20;
        pofs.RedWeightingArea__c		= 20;
        pofs.RedWeightingAssortment__c	= 20;
        pofs.RedWeightingEquipment__c	= 40;
        listPofs.add(pofs);
        
        pofs					 		= new PictureOfSuccess__c();
        pofs.Bezeichnung__c				= 'Test 2';
        pofs.ValidFrom__c				= date.today();
        pofs.ValidUntil__c				= validToDate;
        pofs.Subtradechannel__c			= 'yy';
        pofs.RedWeightingActivation__c	= 20;
        pofs.RedWeightingArea__c		= 20;
        pofs.RedWeightingAssortment__c	= 20;
        pofs.RedWeightingEquipment__c	= 40;
        listPofs.add(pofs);
        
        insert listPofs;
        
        
        Test.StartTest();
		fsfaAlertBatch batch = new fsfaAlertBatch();
		Id batchId = Database.executeBatch(batch);
		// System.abortJob(batchId);
		Test.StopTest();
    }
}
