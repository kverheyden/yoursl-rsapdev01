/*
* @(#)SCPortalTest.cls
* 
* Copyright 2014 by GMS Development GmbH, Karl-Schurz-Strasse 29, 
* DE-33100 Paderborn. All rights reserved.
* 
* Test implementation of the SCPortal class
* 
* @history 
* 2014-11-03 GMSSU created
* 
* @review 
*/
@isTest (oninstall=true seealldata=false)
private class SCPortalTest 
{

   /* Data needed for the test:
    * 
    * SCOrder__c
    * User
    * SCVendor__c
    * SCVendorUser__c
    * SCSearchFilter__c
    * SCOrderExternalAssignment__c
    */ 
    static testMethod void testPositivForVendor1() 
    {
                
        // Initializing controller
        SCPortal con = new SCPortal();
        // Setting some variable values
		con.filterObjectType = 'SCOrder__c';
		con.filterGroupId = '001';
        // Creating an order object
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet(true);
        // Creating test user
        User testUser;
        for(User u : SCHelperTestClass4.createTestUsers())
        {
        	if(u.Alias == 'GMSTest1')
        	{
        		testUser = u;
        	}
        }
        // Vendor
        SCVendor__c vendor = new SCVEndor__c(Name 			= '0123456789',
        							     	 Name1__c 		= 'My test vendor',
        							   		 Country__c 	= 'DE',
        							   		 PostalCode__c 	= '33100',
        							   		 City__c		= 'Paderborn',
        							   		 Street__c		= 'Bahnhofstrt',
        							   		 LockType__c	= '5001',
        							   		 Status__c		= 'Active');
		insert vendor;
		// Vendor - User
		SCVendorUser__c vendorUser = new SCVendorUser__c(User__c   = testUser.Id,
														 Vendor__c = vendor.id);
		insert vendorUser;
        // Order External Asignment
        SCOrderExternalAssignment__c extAssignment = new SCOrderExternalAssignment__c(Order__c  		= SCHelperTestClass.order.id,
        																		  	  Vendor__c 		= vendor.id,
        																			  Status__c 		= 'assigned',
        																			  WishedDate__c 	= Date.today());
		insert extAssignment;

		// Inserting a test Document
		/*
		Document document = null;
		Boolean docExist = false;
		try
		{
			document = [Select Id From Document Where Name = 'Muster CSV-Datei für Verdor Portal']; // DeveloperName = 'Muster_CSV_Datei_Verdor_Portal'];
			if(document != null)
			{
				docExist = true;
			}
		}
		catch(Exception e)
		{
		}
		
		System.debug('#### docExist: ' + docExist);
		System.debug('#### document: ' + document);
		
		if(!docExist)
		{
			List<Folder> listFolder = [Select Name, IsReadonly, Id, DeveloperName 
									   From Folder
									   Where  AccessType = 'Public'
									   And IsReadonly = false
									   And Type = 'Document'];
									  
			System.debug('#### listFolder: ' + listFolder);
			
	        if(listFolder.size() > 0)
	        {
	        	document = new Document(FolderId = listFolder.get(0).Id, Name='Muster CSV-Datei für Verdor Portal',Keywords = 'Test', DeveloperName = 'Muster_CSV_Datei_Verdor_Portal');
				System.debug('#### inserting document');
	        	insert document;
			}
		}
		*/

        Test.startTest();
        
        System.runAs(testUser) 
        {
        	
        	// User
        	System.assertEquals(testUser.id, con.readUser().id);
        	
        	// Vendor
        	con.readVendor();
        	system.assertEquals(vendor.id, con.vendor.id);
        	
        	// Fieldset
        	con.readFieldSet('SCOrder__c','SCVendorOrderFields');
        	
        	// Get format
        	Date d = Date.today();
        	Datetime newDateTime = Datetime.newInstance(d.year(), d.month(), d.day(),0,0,0);
        	system.assertEquals(newDateTime.format('yyyy-MM-dd') + 'T00:00:01.000Z', con.getFormat(d, true));
        	system.assertEquals(newDateTime.format('yyyy-MM-dd') + 'T23:59:59.000Z', con.getFormat(d, false));
        	system.assertEquals(newDateTime.format('yyyy-MM-dd') + 'T00:00:01.000Z', con.getFormat(d));
        	
        	// Excel datetime format
        	con.getCurrentDateTimeFormatForExcel();
        	
        	// Error mesage
        	try
        	{
        		String nullString = null;
        		SCOrder__c order = new SCOrder__c(Status__c = nullString);
        		order.addError('My Error Message');
        		insert order;
        	}
        	catch(Exception e)
        	{
        		ApexPages.getMessages().clear();
        		
        		// Exception, custom text, severity
        		SCPortal.addPageMessage(e, '', ApexPages.Severity.INFO);
				List<Apexpages.Message> msgs = ApexPages.getMessages();
			    Boolean b = false;
			    for(Apexpages.Message msg : msgs)
			    {
			        if (msg.getDetail().contains('My Error Message'))
			        {
			        	b = true;
			        }
			    }
			    system.assert(b);
								
				// null, custom text, severity
				SCPortal.addPageMessage(null, 'My Error Message', ApexPages.Severity.INFO);
				
				// Exception, custom text
				SCPortal.addPageMessage(e, 'My Error Message');
				
				// custom text, severity
				SCPortal.addPageMessage('My Error Message', ApexPages.Severity.ERROR);
				
				// custom text
				SCPortal.addPageMessage('My Error Message');
        	}

			// Where or And
			system.assertEquals(con.checkWhereOrAnd(), ' Where ');
			system.assertEquals(con.checkWhereOrAnd(), ' And ');
        	
        	// Filters: check filter on start
        	con.checkDefaultFilter();
        	List<SCSearchFilter__c> filters = [Select Id From SCSearchFilter__c Where User__c = :testUser.Id];
        	system.assertEquals(filters.size(), 1);
        	
        	// Filters: Create new Filter
        	con.createNewFilter();
        	con.readFilters();
        	system.assertEquals(con.filters.size(), 2);
        	
        	// Filters: options size
        	system.assertEquals(con.getLoadFilters().size(), 2);
        	
        	// Filters: switch filter
        	// First setting some selected filter value (it should be not default filter)
        	for(SCSearchFilter__c f : con.filters.values())
        	{
        		if(!f.Default__c)
        		{
        			con.selectedFilter = String.valueOf(f.id);
        			break;
        		}
        	}
        	con.switchFilter();
        	// Now checking that early not default filter is now default one
        	system.assertEquals(con.filters.get(con.selectedFilter).Default__c, true);
        	
        	// Filters: rename filter
        	// We need to compare the title of the selected filter before and after rename
        	String titleBefore = con.filters.get(con.selectedFilter).Title__c;
        	con.filterRename();
        	system.assertNotEquals(con.filters.get(con.selectedFilter).Title__c, titleBefore);
        	
        	// Filters: update filter
        	con.filterUpdate();
        	
        	// Filters: delete filter
        	con.filterDelete();
        	system.assertEquals(con.filters.size(), 1);
        	system.assertEquals(con.filters.get(con.selectedFilter).Default__c, true);
        	
        	// CSV Upload as a Blob
        	//con.fileBody = Blob.valueOf('123;10.05.2012;5011;4234243;111;Test;true');
        	//con.uploadCSV();
        	
        	// CSV text parser
        	con.csvUserInput = '123;10.05.2012;5011;4234243;111;Test;true';
        	con.processCSVText();
        	//String s = EncodingUtil.convertToHex(con.fileBody);
			//Blob b = SCPortal.HexToUTF(s);
			
			con.getCSVFileId();
			con.getReadVendorForHeader();
			
			for(Integer i = 1; i <= (SCPortal.maxNumberOfFilters + 1); i++)
			{
				Map<ID, SCSearchFilter__c> filtersTMP = new Map<ID, SCSearchFilter__c>();
				filtersTMP.putAll(con.filters);
				
				con.filters.putAll(filtersTMP);
			}
			
			con.createNewFilter();
			con.filterDelete();
        }
        
        Test.stopTest();  
    }
    
   /* 
    * Testing exceptions
    */ 
    static testMethod void testNegative1() 
    {
        // Initializing controller
        SCPortal con = new SCPortal();
    	
    	// Starting the test
    	Test.startTest();
    	
    	con.docDevname = null;
    	con.getCSVFileId();
    	con.readVendor();
    	
    	//con.currentUser = null;
    	con.filterObjectType = null;
    	con.filterGroupId = null;
    	con.readFilters();
    	
    	for(Id i : con.filters.keyset())
    	{
    		con.filters.get(i).Id = '1234567890';
    		break;
    	}
    	con.getLoadFilters();
    	
    	con.filters = null;
    	con.switchFilter();
    	
    	con.filterObjectType = null;
    	con.filterGroupId = null;
    	con.checkDefaultFilter();
    	
    	con.standardFilterTitle = null;
    	con.createDefaultFilter();
    	
		SCSearchFilter__c newFilter = new SCSearchFilter__c(Default__c  = true,
															User__c     = UserInfo.getUserId(),
															Title__c    = 'Test',
															Object__c   = 'Obj',
															Group__c    = 'Group',
															Filter__c   = '');
		insert newFilter;
		con.filters = new Map<ID, SCSearchFilter__c>();
		con.filters.put(newFilter.id, newFilter);
    	con.createNewFilter();
    	for(Integer i = 0; i <= (SCPortal.maxNumberOfFilters + 1); i++)
    	{
    		con.filters.put(newFilter.id, newFilter);
    	}
    	con.createNewFilter();
    	
    	con.selectedFilter = null; 
    	con.filterRename();
    	con.selectedFilter = null; 
    	con.filterUpdate();
    	con.filterDelete();
    	con.selectedFilter = null;
    	con.filters = null; 
    	con.filterDelete();
    	
    	con.delimiter = '';
    	con.parseCSV('"test01"	11.12.14	5011	503405151	111	Test Beschreibung 1	ja',false);
    	con.delimiter = '\t';
    	con.parseCSV('"test01"	"11.12.14"	"5011"	503405151	111	Test Beschreibung 1	ja\t"test01	11.12.14	5011	503405151	111	Test Beschreibung 1	ja',false);
    	con.parseCSV('"test01	11.12.14	5011	503405151	111	Test Beschreibung 1	ja\ttest01"	11.12.14	5011	503405151	111	Test Beschreibung 1	ja',false);
    	con.parseCSV('test01"	"11.12.14"	5011"	503405151"	111"	Test Beschreibung 1	ja"\ttest01"	11.12.14	5011"	503405151"	111"	Test Beschreibung 1	ja"',false);
    	con.parseCSV('"test01	11.12.14	5011	503405151	111	Test Beschreibung 1	ja"\ttest01"	11.12.14	5011"	503405151"	111"	Test Beschreibung 1	ja"',false);
    	con.parseCSV('test01	11.12.14	5011	503405151	111	Test Beschreibung 1	ja',true);
    	con.parseCSV('',false);
    	
    	Test.stopTest();  
    }
}
