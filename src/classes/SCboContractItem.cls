/*
 * @(#)SCboContractItem.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * on the base of SCboOrderItem
 * @version $Revision$, $Date$
 */
public class SCboContractItem
{
    public SCContractItem__c contractItem { get; private set; }

    public SCboContractItem()
    {
        this.contractItem = new SCContractItem__c();
    }
    
    public SCboContractItem(SCContractItem__c contractItem)
    {
        this.contractItem = contractItem;
//remove TK: this.historyData = '';
    }
    
    public SCboContractItem(Id contractItemId)
    {
        this.contractItem = [select Id, ID2__c,
                                 MaintenanceDuration__c,
                                 MaintenanceInterval__c,
                                 MaintenanceLastDate__c,
                                 MaintenanceNextDate__c,
                                 MaintenancePrice__c,
                                 MaintenancePriceCalc__c,
                                 MaintenanceDiscount__c,
                                 ValidFrom__c,
                                 ValidTo__c,
                                 util_Valid__c,
                                 InstalledBase__r.SerialNo__c, 
                                 InstalledBase__r.SerialNoException__c, 
                                 InstalledBase__r.InstallationDate__c, 
                                 InstalledBase__r.GuaranteeUntil__c, 
                                 InstalledBase__r.CommissioningDate__c, 
                                 InstalledBase__r.CommissioningStatus__c, 
                                 InstalledBase__r.ConstructionWeek__c, 
                                 InstalledBase__r.ConstructionYear__c, 
                                 InstalledBase__r.Name, 
                                 InstalledBase__r.Id, 
                                 InstalledBase__r.ID2__c, 
                                 InstalledBase__r.IdExt__c, 
                                 InstalledBase__r.Article__c, 
                                 InstalledBase__r.ArticleEAN__c, 
                                 InstalledBase__r.ArticleName__c, 
                                 InstalledBase__r.ArticleNo__c, 
                                 InstalledBase__r.Brand__c, 
                                 InstalledBase__r.Brand__r.Name, 
                                 InstalledBase__r.System__c, 
                                 InstalledBase__r.System__r.Name, 
                                 InstalledBase__r.ProductModel__c, 
                                 InstalledBase__r.ProductModel__r.Name, 
                                 InstalledBase__r.ProductModel__r.Group__c, 
                                 InstalledBase__r.ProductUnitClass__c, 
                                 InstalledBase__r.ProductUnitType__c, 
                                 InstalledBase__r.ProductGroup__c, 
                                 InstalledBase__r.ProductPower__c, 
                                 InstalledBase__r.ProductEnergy__c, 
                                 InstalledBase__r.Type__c, 
                                 InstalledBase__r.Status__c, 
                                 InstalledBase__r.SafetyAction__c, 
                                 InstalledBase__r.Safety1__c, 
                                 InstalledBase__r.Safety2__c, 
                                 InstalledBase__r.SafetyNote__c, 
                                 InstalledBase__r.SafetyStatus__c, 
                                 InstalledBase__r.InstalledBaseLocation__c, 
                                 InstalledBase__r.InstalledBaseLocation__r.ID2__c, 
                                 InstalledBase__r.InstalledBaseLocation__r.GeoX__c, 
                                 InstalledBase__r.InstalledBaseLocation__r.GeoY__c, 
                                 InstalledBase__r.InstalledBaseLocation__r.GeoApprox__c, 
                                 InstalledBase__r.InstalledBaseLocation__r.LocName__c, 
                                 InstalledBase__r.InstalledBaseLocation__r.Address__c, 
                                 InstalledBase__r.InstalledBaseLocation__r.FlatNo__c, 
                                 InstalledBase__r.InstalledBaseLocation__r.Floor__c, 
                                 InstalledBase__r.InstalledBaseLocation__r.Street__c, 
                                 InstalledBase__r.InstalledBaseLocation__r.HouseNo__c, 
                                 InstalledBase__r.InstalledBaseLocation__r.Extension__c, 
                                 InstalledBase__r.InstalledBaseLocation__r.PostalCode__c,
                                 InstalledBase__r.InstalledBaseLocation__r.City__c, 
                                 InstalledBase__r.InstalledBaseLocation__r.District__c, 
                                 InstalledBase__r.InstalledBaseLocation__r.County__c, 
                                 InstalledBase__r.InstalledBaseLocation__r.CountryState__c, 
                                 InstalledBase__r.InstalledBaseLocation__r.Country__c  
                          from SCContractItem__c 
                          where Id = :contractItemId];
    }

    public void save()
    {
        try
        {
            //#### Update contract item ###
            upsert contractItem;
        }
        catch (DmlException e)
        {
            throw new SCfwOrderException('Unable to save contract item ' + 
                e.getMessage());
        }
    }    

    /**
     * getAssignedContractItems
     * ========================
     *
     * Retrieves all contracts assigned to the installed base of this contract item.
     *
     * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
     * @version $Revision$, $Date$
     */
    public List<SCContractItem__c> getAssignedContract()
    {
        List<SCContractItem__c> contracts = 
            [Select Id, Name,
         Contract__r.util_templatedetails__c,
         Contract__r.util_earliest_termination_date__c,
         Contract__r.util_details__c,
         Contract__r.util_days_until_expiration__c,
         Contract__r.util_active__c,
         Contract__r.util_RevenueTotal__c,
         Contract__r.util_RevenueInsurance__c,
         Contract__r.util_RevenueCustomer__c,
         Contract__r.util_RevenueContract__c,
         Contract__r.util_Profitability__c,
         Contract__r.util_ProfitabilityRanking__c,
         Contract__r.util_Profit__c,
         Contract__r.util_MaxRenewals__c,
         Contract__r.util_MaintenanceDuration__c,
         Contract__r.util_CostsTravel__c,
         Contract__r.util_CostsTotal__c,
         Contract__r.util_CostsParts__c,
         Contract__r.util_CostsLabour__c,
         Contract__r.TerminationType__c,
         Contract__r.TerminationReceivedDate__c,
         Contract__r.TerminationReason__c,
         Contract__r.TerminationPeriod__c,
         Contract__r.TerminationExtraordinary__c,
         Contract__r.TerminationDescription__c,
         Contract__r.TerminationDate__c,
         Contract__r.Template__c,
         Contract__r.TemplateName__c,
         Contract__r.SystemModstamp,
         Contract__r.Status__c,
         Contract__r.StartDependsOnWarranty__c,
         Contract__r.StartDelayDays__c,
         Contract__r.StartDate__c,
         Contract__r.Runtime__c,
         Contract__r.RuntimeMax__c,
         Contract__r.RenewalReason__c,
         Contract__r.RecordTypeId,
         Contract__r.PreviousContract__c,
         Contract__r.OwnerId,
         Contract__r.OrderType__c,
         Contract__r.OrderTypeFirstVisit__c,
         
         Contract__r.OfferDescription__c,
         Contract__r.OfferDate__c,
         Contract__r.Name,
         Contract__r.MaintenanceVisitTime__c,
         Contract__r.MaintenanceVisitTimeSelection__c,
         Contract__r.MaintenanceVisitPeriod__c,
         Contract__r.MaintenanceVisitPeriodSelection__c,
         Contract__r.MaintenanceVisitDay__c,
         Contract__r.MaintenanceVisitDaySelection__c,
         Contract__r.MaintenanceInterval__c,
         Contract__r.MaintenanceFirstDate__c,
         Contract__r.MaintenanceDuration__c,
         Contract__r.MaintenanceCreationShiftFactor__c,
         Contract__r.MaintenanceCreationLeadtime__c,
         Contract__r.MaintenanceBeforeDue__c,
         Contract__r.MaintenanceAfterDue__c,
         Contract__r.LastModifiedDate,
         Contract__r.LastModifiedById,
         Contract__r.LastActivityDate,
         Contract__r.IsDeleted,
         Contract__r.InvoicingType__c,
         Contract__r.InvoicingSubtype__c,
         Contract__r.Internal__c,
         Contract__r.InsuranceValidateOnline__c,
         Contract__r.InsuranceSalesPrice__c,
         Contract__r.InsuranceProductCode__c,
         Contract__r.InsurancePolicyTypeName__c,
         Contract__r.InsurancePolicySelector__c,
         Contract__r.InsurancePaymentType__c,
         Contract__r.InsuranceOnRiskDate__c,
         Contract__r.InsuranceMailCode__c,
         Contract__r.InsuranceLastChecked__c,
         Contract__r.InsuranceExcess__c,
         Contract__r.InsuranceCompany__c,
         Contract__r.InsuranceCleanup__c,
         Contract__r.InsuranceAreaCode__c,
         Contract__r.Info__c,
         Contract__r.IdOld__c,
         Contract__r.IdExt__c,
         Contract__r.Id,
         Contract__r.ID2__c,
         Contract__r.Frame__c,
         Contract__r.Followup2__c,
         Contract__r.Followup1__c,
         Contract__r.EndDate__c,
         Contract__r.EnableScheduleIfPayed__c,
         Contract__r.EnableOrderCreation__c,
         Contract__r.EnableMultiLocations__c,
         Contract__r.EnableAutoScheduling__c,
         Contract__r.Description__c,
         Contract__r.DescriptionSpecialTerms__c,
         Contract__r.DescriptionInternal__c,
         Contract__r.DescriptionEngineer__c,
         Contract__r.DepartmentTurnover__c,
         Contract__r.DepartmentResponsible__c,
         Contract__r.DepartmentCurrent__c,
         Contract__r.CustomerPriority__c,
         Contract__r.CurrencyIsoCode,
         Contract__r.CreatedDate,
         Contract__r.CreatedById,
         Contract__r.Country__c,
         Contract__r.Contact__c,
         Contract__r.ConclusionDate__c,
         Contract__r.BulkId__c,
         Contract__r.Brand__c,
         Contract__r.BillingStatus__c,
         Contract__r.BillingMethod__c,
         Contract__r.BillingInterval__c,
         Contract__r.AutoExtend__c,
         Contract__r.Account__c
       from SCContractItem__c 
              where InstalledBase__c = :contractItem.InstalledBase__c 
           order by MaintenanceLastDate__c desc];

        System.debug('#### getAssignedContractItems(): contractItems -> ' + contracts);
        
        return contracts;
    } // getAssignedContract

    private static testmethod void test()
    {
        SCInstalledBase__c ib = new SCInstalledBase__c();
        insert ib;
    
        SCContract__c contr = new SCContract__c();
        insert contr;
        
        SCContractItem__c contrItem = new SCContractItem__c(contract__c = contr.id, installedbase__c = ib.id);
        insert contrItem;
        
        
        SCboContractItem c1 = new SCboContractItem();
        
        SCboContractItem c2 = new SCboContractItem(contrItem.id);
 
        List<SCContractItem__c> conlist = c2.getAssignedContract();
        
        SCboContractItem c3 = new  SCboContractItem(contrItem );
        c3.save();
    
    }
    
    

}
