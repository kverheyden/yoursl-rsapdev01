public with sharing class SCProductModelTriggerHandler {
	public static final String DEVELOPERNAME_FIELDNAME = 'PictureDocDevName__c';
	public static final String IDTEXT_FIELDNAME = 'Picture__c';

	public class SCProductModelBUHandler extends TriggerHandlerBase {
		public override void mainEntry(TriggerParameters tp) {
			setIdTextByDeveloperName(tp.newList);
		}
	}

	public class SCProductModelBIHandler extends TriggerHandlerBase {
		public override void mainEntry(TriggerParameters tp) {
			setIdTextByDeveloperName(tp.newList);
		}
	}

	public static void setIdTextByDeveloperName(List<SCProductModel__c> productModels) {
		Set<String> documentDeveloperNames = new Set<String>();
		for (SCProductModel__c pm : productModels) {
			String devName = (String)pm.get(DEVELOPERNAME_FIELDNAME);
			if (!String.isBlank(devName))
				documentDeveloperNames.add(devName);
		}
		if (documentDeveloperNames.isEmpty())
			return;

		List<Document> documents = [SELECT Id, DeveloperName FROM Document WHERE DeveloperName IN: documentDeveloperNames];
		if (documents.isEmpty())
			return;
		
		Map<String, Id> developerNamesWithId = new Map<String, Id>();
		for (Document d : documents)
			developerNamesWithId.put(d.DeveloperName, d.Id);


		for (SCProductModel__c pm : productModels) {
			String devName = (String)pm.get(DEVELOPERNAME_FIELDNAME);
			Id documentId = developerNamesWithId.get(devName);
			pm.put(IDTEXT_FIELDNAME, documentId);
		}
	}
}
