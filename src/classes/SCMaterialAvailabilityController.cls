/*
 * @(#)SCMaterialAvailabilityController.cls 
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author Alexei Geiger <ageiger@gms-online.de>
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de> corrections of too many script statement exception 200001
 *                                                  and refactoring and optimizing
 * @version $Revision$, $Date$
 */

/*
 * This class searches an article in the technician stocks
 */

public with sharing class SCMaterialAvailabilityController extends SCOrderComponentController
{
    public static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
    
    
    //Search input data
    public String articleName { get; set; }
    public String articleId { get; set; }
    public SCTool__c dateRequired { get; set; }
    public SCResourceAssignment__c businessUnit { get; set; }
    public SCStockItem__c stockItemInput { get; set; }
    public SCStock__c stock { get; set; }
    public Boolean searchingReplaceableSparePart { get; set; }
    public List<SCMaterialAvailabilityData.SCResultData> resultData { get; set; }
    
    //Result of searching container
    // public List<SCStockItem__c> stockItems{ get; set; }
    // public List<SCResourceAssignment__c> resourceAssignments{ get; set; }
    public List<SpareParts> spareParts {get; set;}
    
    //Help variables
    public String articleSearchAutoStart { get; set; }
    public Boolean isArticleTableRender { get; set; }
    private List<Id> allArticleIds;
    public Boolean isValidArticleId { get; set; }
    
    //Search article dialog variables
    public String returnArticleName { get; set; }
    public String currentResourceId { get; set; }

    //Feedback search articles
    public List<ArticleExt> articlesExt { get; set; }
    
    public String errMsgExtern { get; private set; }
    public String callback { get; set; }
    public String callbackComplete { get; set; }
    
    //Component parameters
    public Boolean isDisplaySearchField1
    {
        get;
        set
        {
            isDisplaySearchField1 = value;
        }
    }
    public Boolean isDisplaySearchField2
    {
        get;
        set
        {
            isDisplaySearchField2 = value;
        }
    }
    public Boolean isExternCheck
    {
        get
        {
            String value = appSettings.MATERIALAVAILABILITY__c;
            return ((null != value) && value.equals('2'));
        }
        set;
    }
    public List<Id> articleIds
    {
        get;
        set
        {
            articleIds = new List<Id>();
            if( value != null )
            {
                articleIds.addAll( value );
            }
        }
    }
    public List<Id> stockIds
    {
        get;
        set
        {
            stockIds = new List<Id>();
            if( value != null )
            {
                stockIds.addAll( value );
            }
        }
        
    }
    public List<Id> plantIds
    {
        get;
        set
        {
            plantIds = new List<Id>();
            if( value != null )
            {
                plantIds.addAll( value );
            }
        }
        
    }

    public List<Id> resourceIds
    {
        get;
        set
        {
            resourceIds = new List<Id>();
            if( value != null )
            {
                resourceIds.addAll( value );
            }
        }
        
    }

    //Initialise the variables
    public SCMaterialAvailabilityController()
    {
        System.debug('### SCMaterialAvailabilityController Constructor:');
        this.businessUnit = new SCResourceAssignment__c();
        this.stockItemInput = new SCStockItem__c();
        //this.stockItems = new List<SCStockItem__c>();
        //this.resourceAssignments = new List<SCResourceAssignment__c>();
        this.stock = new SCStock__c();
        this.articleSearchAutoStart = '0';
        this.dateRequired = new SCTool__c();
        this.dateRequired.Start__c = date.TODAY();
        this.articleId = '';
        this.currentResourceId = '0';
        this.isValidArticleId = true;
        this.allArticleIds = new List<Id>();
        this.spareParts = new List<SpareParts>();
        this.searchingReplaceableSparePart = true;
        
        this.callback = ApexPages.currentPage().getParameters().get('callback');
        this.callbackComplete = ((callback == null || callback == '') ? '' : 'window.opener.' + callback + '(deliveryDate);');
        
    }
    
    
    /* 
    * Helper class that contains all searching data.
    * This data will showing in the result table.
    */
    class SpareParts
    {
        private SCStockItem__c stockItem;
        private SCResourceAssignment__c resourceAssignment;
        private Boolean isColorMark;
        private String replacesArticle;
        
        public SpareParts(SCStockItem__c stockItem, SCResourceAssignment__c resourceAssignment, List<String> replacesArticle, Boolean isColorMark){
            this.stockItem = stockItem;
            this.resourceAssignment = resourceAssignment;
            this.isColorMark = isColorMark;
            if( replacesArticle != null || replacesArticle.size() > 0 )
            {
                Boolean isFirstNumber = true;
                for( String article : replacesArticle )
                {
                    if( isFirstNumber )
                    {
                        this.replacesArticle = article;
                        isFirstNumber = false;
                    }
                    else
                    {
                        this.replacesArticle += ', '+article;
                    }
                }
            }
        }
        public SCStockItem__c getStockItem(){
            return this.stockItem;
        }
        public SCResourceAssignment__c getResourceAssignment(){
            return this.resourceAssignment;
        }
        public Boolean getIsColorMark()
        {
            return this.isColorMark;
        }
        public String getReplacesArticle()
        {
            return this.replacesArticle;
        }
        
    }
    
    /*
    *   StockItemExt extends the StockItem object
    */
    class StockItemExt
    {
        private SCStockItem__c stockItem;
        private Id belongToArticle;
        
        public StockItemExt( SCStockItem__c stockItem, Id belongToArticle )
        {
            this.stockItem = stockItem;
            this.belongToArticle = belongToArticle;
        }
        
        public SCStockItem__c getStockItem()
        {
            return this.stockItem;
        }       
        public Id getBelongToArticle()
        {
            return this.belongToArticle;
        }
    }
    
    /*
    *   ArticleExt extends the SCboOrderLine object
    */
    class ArticleExt
    {
        SCArticle__c article;
        Decimal qty;
        Boolean isSelected;
        
        public ArticleExt( SCArticle__c article, Decimal qty, Boolean isSelected)
        {
            this.article = article;
            this.qty = qty;
            this.isSelected = isSelected;
        }
        
        public SCArticle__c getArticle()
        {
            return this.article;
        }
        public Decimal getQty()
        {
            return this.qty;
        }
        public Boolean getIsSelected()
        {
            return this.isSelected;
        }
        public void setIsSelected( Boolean isSelected )
        {
            this.isSelected = isSelected;
        }
    }
    
    /*
    * This method search technical stocks for a spare part.
    *
    * @return SpareParts object filled with the result of searching
    */
    public PageReference onSearchArticle()
    {
        checkMaterialAvailability(false);
        return null;
    } // onSearchArticle
    
    /*
    * This method checks the material availability with an extern system (e.g. SAP).
    */
    public PageReference onSearchArticleExt()
    {
        checkMaterialAvailability(true);
        return null;
    } // onSearchArticleExt
    
    /*
    * This method checks the material availability.
    */
    private void checkMaterialAvailability(Boolean extern)
    {
        // Article replacement map
        Map<ID,ID> articleReplacementMap = SCbtcStockOptimization.createReplacementMap();
    
        // Map of article ids to the list of the article name that are replaced by the key article
        Map<Id, List<String>> replacedArticleMap = getMapArticleIdToNamesOfArticleReplacedByIt();
        
        System.debug('####articleName: ' + this.articleName);
        System.debug('####articlesExt: ' + this.articlesExt);

        this.resultData = null;
        this.spareParts.clear();
        this.isArticleTableRender = false;

        // deteminers the article ids that ought to be worked out
        this.articleIds = completeArticleIds();
        System.debug('####input articleIds: ' + this.articleIds);
        

        if ( this.articleIds.size() > 0 )
        {
            System.debug('####this.articleIds: ' + this.articleIds);
            if ( this.searchingReplaceableSparePart )
            {
                // get all article ids also the replacees ( that are replacing some input articles)
                this.allArticleIds = getReplacementArticleList(this.articleIds, articleReplacementMap);
                this.allArticleIds = removeDuplicateIds( this.allArticleIds );
            }
            else
            {
                this.allArticleIds = this.articleIds;
            }
            
            if (!extern)
            {
                // Get all stock items having the articles
                List<SCStockItem__c> stockItems = getStockItems(this.allArticleIds,
                                                   stockItemInput.Stock__c, stock.Plant__c);
                System.debug('####StockItems: '+ stockItems);
                
                if ( stockItems.size() > 0 )
                {
                    // Sorts the stock items in the following way
                    // stock item with an input article
                    //     stock item with a replacee of the previous article
                    //         stock item with the replacee of the previous article 
                    //             stock item with the replacee of the previous article
                    //  ...
                    // There are rendundant subtrees possible
                    List<StockItemExt> sortedStockItemList = sortStockItem(stockItems, this.articleIds );
                    
                    // Read resource assignments
                    List<SCResourceAssignment__c> resourceAssignments = 
                                        getResourceAssignments(dateRequired.Start__c, stockItems,
                                                               this.resourceIds);
                    
                    //Prepare result data table
                    Boolean isColorMark = false;
                    Boolean init = true;
                    Id artId;
                    for( StockItemExt item : sortedStockItemList )
                    {
                        // filter the stock items for the possible input stock or plant
                        // if no input stock or plant the stock items for all stocks and plant are to
                        // be worked out.
                        if( ( item.getStockItem().Stock__c == stockItemInput.Stock__c  
                                || stockItemInput.Stock__c == null ) 
                            &&
                            ( item.getStockItem().Stock__r.Plant__c == stock.Plant__c  
                                || stock.Plant__c == null ))
                        {
                            for( SCResourceAssignment__c resource: resourceAssignments )
                            {
                                if( item.getStockItem().Stock__c == resource.Stock__c 
                                    && 
                                    ( this.businessUnit.Department__c == null 
                                        || resource.Department__c == this.businessUnit.Department__c ) )
                                {
                                    if( init )
                                    {
                                        init = false;
                                        artId = item.getBelongToArticle();
                                    }
                                    if( artId != item.getBelongToArticle() )
                                    {
                                        isColorMark = isColorMark ? false : true;
                                        artId = item.getBelongToArticle();
                                    }
                                    
                                    System.debug('####ArticleNo: ' + item.getStockItem().Article__r.Name + ', Stock: ' + item.getStockItem().Stock__r.Name + ', Technician: ' +resource.Resource__r.FirstName__c);
                                    List<String> repArticles = new List<String>();
                                    if( replacedArticleMap.containsKey(item.getStockItem().Article__c) )
                                    {
                                        repArticles = replacedArticleMap.get( item.getStockItem().Article__c );
                                    }
                                    this.spareParts.add( new SpareParts( item.getStockItem(), resource, repArticles, isColorMark ) );
                                    this.isArticleTableRender = true;
                                }
                            }
                        }
                    }
                }
            } // if (!extern)
            else
            {
                SCMaterialAvailabilityData.SCServiceData serviceData = new SCMaterialAvailabilityData.SCServiceData();
                
                String orderId = ApexPages.currentPage().getParameters().get('oid');
                String brand = ApexPages.currentPage().getParameters().get('b');
                if (SCBase.isSet(orderId))
                {
                    System.debug('#### checkMaterialAvailability(): orderId -> ' + orderId);
                    try
                    {
                        serviceData.accNumber = [Select AccountNumber__c from SCOrderRole__c 
                                                  where Order__c = :orderId 
                                                    and OrderRole__c = :SCfwConstants.DOMVAL_ORDERROLE_AG].AccountNumber__c;

                        SCOrder__c order = [Select Name, Country__c, Brand__r.Name 
                                              from SCOrder__c where Id = :orderId];
                        serviceData.orderNo = order.Name;
                        serviceData.country = order.Country__c;
                        serviceData.brand = order.Brand__r.Name;
                        
                    }
                    catch (Exception e)
                    {
                    }
                }
                if (SCBase.isSet(brand))
                {
                    serviceData.brand = brand;
                }
                serviceData.deliveryDate = dateRequired.Start__c;
                serviceData.articles = new List<SCMaterialAvailabilityData.SCArticleData>();
                Decimal qty = null;
                for (SCArticle__c art :[SELECT Id, Name from SCArticle__c where Id in :this.allArticleIds])
                {
                    qty = getQty(art.Id);
                    serviceData.articles.add(new SCMaterialAvailabilityData.SCArticleData(art.Name, (null == qty) ? 1 : qty.intValue()));
                }
                
                SCMaterialAvailabilityData.SCResult result = SCMaterialAvailability.call(serviceData);
                if (result.success)
                {
                    resultData = result.results;
                    errMsgExtern = null;
                }
                else
                {
                    errMsgExtern = result.errorMsg;
                }
            } // else [if (!extern)]
        }
    } // checkMaterialAvailability
        
    public Boolean getHasResults()
    {
        return ((null != resultData) && (resultData.size() > 0));
    }
    
    /**
     * In case the function is called from onSearchArticle only the article id of the article name from 
     * the input field is set to be used.
     *
     * In case the function is called from onSearchArticleEx the list of article from the input field are
     * is taken
     *
     * @return see the info above
     */
    private List<ID> completeArticleIds()
    {
        List<ID> ret = new List<ID>();
        if( this.articlesExt != null )
        {
            for( ArticleExt articleExt : this.articlesExt )
            {
                if( articleExt.getIsSelected() )
                {
                    ret.add( articleExt.getArticle().Id );
                }
            }
            
        }
        
        if ( this.articleName != null && this.articleName.trim() != '' )
        {
            List<SCArticle__c> articleIds = [SELECT Id from SCArticle__c where Name =: this.articleName.trim()];
            //Display 'Search article' dialog if article nummber not available ????
            if( articleIds.size() > 0 )
            {
                ret.add( articleIds.get(0).Id );
                this.isValidArticleId = true;
            }
            else
            {
                this.isValidArticleId = false;
            }
            
        }
        System.debug('####isValidArticleId: '+this.isValidArticleId);
        return ret;
    }
    
    /**
     * Reads stock items having the articles from the list.
     *
     * @param articleIdList
     *
     * @return the list of stock items
     */
    private List<SCStockItem__c> getStockItems(List<Id> articleIdList,
                                               ID stock, ID plant)
    {
    
        List<SCStockItem__c> ret = null;
        if(stock == null && plant == null)
        {
            ret = [SELECT Article__c, Article__r.Name, Article__r.ArticleNameCalc__c, 
                                        Article__r.ReplacedBy__c, Article__r.ReplacedBy__r.Name, 
                                        Article__r.LockType__c, Qty__c, Stock__c, Stock__r.Name, 
                                        Stock__r.Plant__c, Stock__r.Plant__r.Name 
                                        FROM SCStockItem__c where Article__c in : articleIdList and Qty__c > 0];
        }
        else if (stock != null)
        {
            ret = [SELECT Article__c, Article__r.Name, Article__r.ArticleNameCalc__c, 
                                        Article__r.ReplacedBy__c, Article__r.ReplacedBy__r.Name, 
                                        Article__r.LockType__c, Qty__c, Stock__c, Stock__r.Name, 
                                        Stock__r.Plant__c, Stock__r.Plant__r.Name 
                                        FROM SCStockItem__c 
                                        where Article__c in : articleIdList 
                                        and Qty__c > 0
                                        and Stock__c = : stock];
        }
        else if(plant != null)
        {
            ret = [SELECT Article__c, Article__r.Name, Article__r.ArticleNameCalc__c, 
                                        Article__r.ReplacedBy__c, Article__r.ReplacedBy__r.Name, 
                                        Article__r.LockType__c, Qty__c, Stock__c, Stock__r.Name, 
                                        Stock__r.Plant__c, Stock__r.Plant__r.Name 
                                        FROM SCStockItem__c 
                                        where Article__c in : articleIdList 
                                        and Qty__c > 0
                                        and Stock__r.Plant__c = : plant];
        }
        return ret;                    
    }
    
    /**
     * read resource assignments
     * 
     * @param dateRequired
     * @param stockItems used to determine the stocks that are used for determine the resource assignments
     *
     * @return the list of resource assignments
     */
    private List<SCResourceAssignment__c> getResourceAssignments(Date dateRequired, 
                                                                 List<SCStockItem__c> stockItems,
                                                                 List<ID> resourceIds)
    {
        List<SCResourceAssignment__c> ret = null;
        if(stockItems.size() > 0 )
        {
            Set<ID> stocks = new Set<ID>();
            for(SCStockItem__c stockItem: stockItems)
            {
                stocks.add(stockItem.Stock__c);
            }
            if(resourceIds == null || resourceIds.size() == 0)
            {
                ret = [SELECT Resource__r.Name, Resource__r.FirstName__c, 
                        Resource__r.LastName__c, Resource__r.Mobile__c, Stock__c, Department__c, 
                        Resource__r.DefaultDepartment__r.Name, Street__c, 
                        PostalCode__c, HouseNo__c, City__c FROM SCResourceAssignment__c 
                        where ValidFrom__c <= :dateRequired and ValidTo__c >= :dateRequired
                        and Stock__c in : stocks];
            }
            else if (resourceIds != null && resourceIds.size() > 0)
            {
                ret = [SELECT Resource__r.Name, Resource__r.FirstName__c, 
                        Resource__r.LastName__c, Resource__r.Mobile__c, Stock__c, Department__c, 
                        Resource__r.DefaultDepartment__r.Name, Street__c, 
                        PostalCode__c, HouseNo__c, City__c FROM SCResourceAssignment__c 
                        where ValidFrom__c <= :dateRequired and ValidTo__c >= :dateRequired
                        and Stock__c in : stocks
                        and Resource__c in : resourceIds];
            }         
        }
        return ret;
    }

    /*
    * This method builds a Map where key is the article id and value is a list of replaces article ids.
    *
    * @return Map of article ids
    */
    private Map<Id, List<Id>> getReplacesArticleIds( List<SCStockItem__c> stockItems )
    {
        Map<Id, List<String>> replacesIds = new Map<Id, List<String>>();
        
        for( SCStockItem__c stockItem : stockItems )
        {
            Id articleId = stockItem.Article__c;
            for( SCStockItem__c stItem : stockItems )
            {
                if( stItem.Article__r.ReplacedBy__c == articleId )
                {
                    if( replacesIds.containsKey( articleId ) )
                    {
                        Boolean isNewArticle = true;
                        for( String articleName : replacesIds.get( articleId ) )
                        {
                            if( articleName == stItem.Article__r.Name )
                            {
                                isNewArticle = false;
                            }
                        }
                        if ( isNewArticle )
                        {
                            replacesIds.get( articleId ).add( stItem.Article__r.Name );
                        }
                    }
                    else
                    {
                        List<String> idName = new List<String>();
                        idName.add( stItem.Article__r.Name );
                        replacesIds.put( articleId, idName);
                    }
                }
            }
        }
        System.debug('####replacesIds: '+replacesIds);
        return replacesIds;
    }

    
    /**
     * Sorts the stock items in the following way
     * stock item with an input article
     *  stock item with a replacee of the previous article
     *      stock item with the replacee of the previous article 
     *         stock item with the replacee of the previous article
     *         ...
     *  The subtrees can be repeated
     */
    private List<StockItemExt> sortStockItem( List<SCStockItem__c> stockItems, List<Id> articleIds )
    {
        
        List<StockItemExt> sortedStockItemList = new List<StockItemExt>();
        Id replacedBy = null;

        if( stockItems.size() == 0 )
        {
            return sortedStockItemList;
        }

        // loop over all articles
        for( Id articleId : articleIds )
        {
            Id id = articleId;
            Map<Id, Boolean> itemMap = new Map<Id, Boolean>();
            // set the break condition variable for the inner loop
            Boolean isNext = true;
            while( isNext )
            {
                // to go over article and their possible replacees
                // the subtree for the article is created
                // loop over all stock items ( beloning also to different stocks)
                for( SCStockItem__c stockItem : stockItems )
                {
                    // if we find the stock item if the certain article id from the input list (without replacee)
                    if( id == stockItem.Article__c )
                    {
                        sortedStockItemList.add( new StockItemExt(stockItem, articleId) );
                        itemMap.put( stockItem.Article__c, true );
                        if( stockItem.Article__r.ReplacedBy__c != null )
                        {
                            replacedBy = stockItem.Article__r.ReplacedBy__c;
                        }
                    }
                }
                if( replacedBy == null || itemMap.containsKey( replacedBy ) )
                {
                    isNext = false;
                }
                id = replacedBy;
                replacedBy = null;
            }
        }
        
        System.debug('####sortedStockItemList: ' + sortedStockItemList);
        return sortedStockItemList;
    }
    
    /*
    * Remove any duplicate Ids
    */
    private List<Id> removeDuplicateIds( List<Id> ids )
    {
        Map<Id, Boolean> idContainer = new Map<Id, Boolean>();
        List<Id> clearedList = new List<Id>();
        
        for( Id id : ids )
        {
            if( !idContainer.containsKey( id ))
            {
                clearedList.add( id );
                idContainer.put( id, true );
            }
        }
        System.debug('####clearedArticleIdList: ' + clearedList);
        return clearedList;
    }

    /*
    *   This method create a list of ArticleExt objects
    *   
    *   @return  ArticleExt list. The list contains articles that are Spare Part or Product type.
    */
    public void onArticleExt()
    {
        String paramIds;
        List<Id> articleIds;
        String paramQtys;
        List<String> articleQtys;
        //semicolon separated articles String
        paramIds = ApexPages.currentPage().getParameters().get('aids');
        if (null != paramIds)
        {
            articleIds = paramIds.split(',');
        }
        paramQtys = ApexPages.currentPage().getParameters().get('qtys');
        if (null != paramQtys)
        {
            articleQtys = paramQtys.split(',');
        }
        
        if ((null != articleIds) && (articleIds.size() > 0))
        {
            //Build SOQL query
            List<SCArticle__c> articleInfos = [SELECT Id, Name, ArticleNameCalc__c FROM SCArticle__c 
                                where id in: articleIds];
            
            Map<Id, SCArticle__c> articleInfosMap = new Map<Id, SCArticle__c>();
            for( SCArticle__c articleInfo : articleInfos )
            {
                articleInfosMap.put( articleInfo.Id, articleInfo );
            }
    
            this.articlesExt = new List<ArticleExt>();
            Integer i = 0;
            for( Id articleId : articleIds )
            {
                this.articlesExt.add( new ArticleExt( articleInfosMap.get(articleId),
                                                      decimal.valueOf(articleQtys.get(i)),
                                                      true ));
                i++;
            }
            System.debug('####articlesExt: ' + this.articlesExt);
    
            if( this.articlesExt.size() > 0)
            {
                onSearchArticle();
            }
        }
    }

    /*
     * Retrieves the quantity for an article.
     *   
     * @return  the quantity, if the article was found, or null.
     */
    public Decimal getQty(Id articleId)
    {
        if (null != this.articlesExt)
        {
            for (ArticleExt artExt :this.articlesExt)
            {
                if (artExt.article.Id == articleId)
                {
                    return artExt.qty;
                }
            }
        }
        return null;
    } // getQty
    
    public List<ID> getReplacementArticleList(List<ID> articleIdList, Map<ID, ID> replacementMap)
    {
        List<ID> ret = new List<ID>();
        ret.addAll(articleIdList);
        for(ID articleId: articleIdList)
        {

            List<ID> listLoop = getReplacementArticleList(articleId, replacementMap);
            ret.addAll(listLoop);
        }
        return ret;    
    }

    public  List<ID> getReplacementArticleList(ID articleIdFirst, Map<ID, ID> replacementMap)
    {
        List<ID> ret = new List<ID>();
        if(!replacementMap.containsKey(articleIdFirst))
        {
            // there is no replacement
            return ret;
        }
        ID prev = articleIdFirst;
        ID next = replacementMap.get(prev);
        Map<ID,ID> cycleMap = new Map<ID,ID>();
        while(true)
        {
            if(next == null)
            {
                return ret;
            }
            else
            {
                cycleMap.put(prev, next);
                ret.add(next);
                prev = next;
                next = replacementMap.get(prev);
                if(cycleMap.containsKey(next))
                {
                    // there is a cycle
                    System.debug('####.....There is a cycle of replacement articles: cycleMap: ' + cycleMap 
                    + ', articleIdFirst: ' + articleIdFirst + ', prev: ' + prev + ', next: ' + next);
                    return ret;
                }
            }
        }    
        return ret;
    }

    /**
     * Gets the map of the article to the list of names of articles that are to be replaced by 
     * the article from the key of map
     *
     * @returns the map described above
     */
    public  Map<Id, List<String>> getMapArticleIdToNamesOfArticleReplacedByIt()
    {
        System.debug('### get map article id to names of article replaced by it');
        Map<Id, List<String>> ret = new Map<Id, List<String>>();
        List<SCArticle__c> articleList = [Select Id, Name, ReplacedBy__c from SCArticle__c where ReplacedBy__c <> null order by ReplacedBy__c];
        SCArticle__c articlePrev = new SCArticle__c();
        List<String> currentList = null;
        for(SCArticle__c article: articleList)
        {
            if(articlePrev.ReplacedBy__c == null 
                || article.ReplacedBy__c != articlePrev.ReplacedBy__c)
            {
                // the beginning of the group
                currentList = new List<String>();
                ret.put(article.ReplacedBy__c, currentList);
            }
            if(currentList != null)
            {
                currentList.add(article.Name);
            }
        }
        return ret;
    }

    /**
     * Gets a list of possible delivery dates, which can be copied to the order.
     */
    public List<SelectOption> getDeliveryDates()
    {
        Date minDate = null;
        Date maxDate = null;
        
        if (null != resultData)
        {
            for (SCMaterialAvailabilityData.SCResultData result :resultData)
            {
                if (null != result.deliveryDate)
                {
                    if ((null == minDate) || (result.deliveryDate < minDate))
                    {
                        minDate = result.deliveryDate;
                    }
                    if ((null == maxDate) || (result.deliveryDate > maxDate))
                    {
                        maxDate = result.deliveryDate;
                    }
                }
            }
        }
        
        List<SelectOption> deliveryDates = new List<SelectOption>();
        if (null != minDate)
        {
            DateTime minDateTime = Datetime.newInstance(minDate.year(), minDate.month(), minDate.day());
            deliveryDates.add(new SelectOption(String.valueOf(minDateTime.getTime()), System.Label.SC_app_DelivDateEarliest + ' (' + minDate.format() + ')'));
        }
        if (null != maxDate)
        {
            DateTime maxDateTIme = Datetime.newInstance(maxDate.year(), maxDate.month(), maxDate.day());
            deliveryDates.add(new SelectOption(String.valueOf(maxDateTime.getTime()), System.Label.SC_app_DelivDateLatest + ' (' + maxDate.format() + ')'));
        }
        return deliveryDates;
    } // getDeliveryDates
}
