/**
* @author		Austen Buennemann
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				a.buennemann@yoursl.de
*
* @description	Test for CCAppAccountSyncStatus
*
* @date			09.09.2014
*
* Timeline:
* Name               DateTime                  Description
* Austen Buennemann  09.09.2014 13:53          Class created
*/

@isTest
public with sharing class CCAppAccountSyncStatusTest {

    static testMethod void myUnitTest() {
    	
    	User sapUser = new User();
        sapUser.FirstName 	= 'sapapi (foo)';
        sapUser.LastName	= 'sapapi (foo)';
        sapUser.Email		= 'sapapi@cc-eag.de';
        sapUser.Username	= 'sapapi@cc-eag.de';
        sapUser.ProfileId	= '00eD0000001lNP0';
        sapUser.ID2__c		= 'sapapi@cc-eag.de';
        sapUser.Alias		= 'sapapi';
        sapUser.TimeZoneSidKey = 'Europe/Berlin';
        sapUser.LocaleSidKey = 'de_DE_EURO';
        sapUser.EmailEncodingKey = 'ISO-8859-1';
        sapUser.LanguageLocaleKey = 'en_US';
        insert sapUser;
    	
    	Account  account = new Account();
        account.LastName__c = 'Max';
        account.FirstName__c = 'Muster';
        account.CeNumber__c = 'CE1234';
        account.OwnerId = sapUser.Id;
        insert account;         
    	
    	Request__c req 					= new Request__c();
		req.OutletCompanyName__c		= 'Muster GmbH';
		req.Account__c 			        = account.Id;		
		req.CeNumber__c				    = 'CE1234';
		insert req;
		
		account.Request__c = req.Id;
    	update account;
    	
    	Set<Id> accset= new Set<Id>();
    	accset.add(account.Id);
    	
    	CCAppAccountSyncStatus appchk = new CCAppAccountSyncStatus();
    	Map<Id, Account> accSyncMap = appchk.checkFromAccountRole(accset);
    	   	   	
    	User tmpUser = new User();
        tmpUser.FirstName 	= 'Hans';
        tmpUser.LastName	= 'Muster';
        tmpUser.Email		= 'test@cc-eag.de';
        tmpUser.Username	= 'test@cc-eag.de';
        tmpUser.ProfileId	= '00eD0000001lNP0';
        tmpUser.ID2__c		= 'DE123456@cc-eag.de';
        tmpUser.Alias		= 'JT';
        tmpUser.TimeZoneSidKey = 'Europe/Berlin';
        tmpUser.LocaleSidKey = 'de_DE_EURO';
        tmpUser.EmailEncodingKey = 'ISO-8859-1';
        tmpUser.LanguageLocaleKey = 'en_US';
        insert tmpUser;
        
        CCAccountRole__c tmpCCAR = new CCAccountRole__c();
        tmpCCAR.MasterAccount__c	= account.Id;
        tmpCCAR.AccountRole__c		= 'ZR';
        tmpCCAR.Status__c			= 'Active';
        tmpCCAR.EmployeeDeNumber__c	= 'DE123456';
        insert tmpCCAR;
        
        account.OwnerId = sapUser.Id;
        update account;
        
        List<Account> accounts = new List<Account>();
        accounts.add(account);
        
        appchk.setAccountOwner(accounts);
                
    }

}
