/*
 * @(#)SCStockItemExtensionTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 * <p>
 * This class contains unit tests for displaying stock items.
 * <p>
 * A complete testing environment is created first. Remember that all data
 * created in a Unit test is not commited to the database!!
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */

@isTest
private class SCStockItemExtensionTest
{
    /**
     * Test the extension class for displaying stockitems for a stock.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    static testMethod void testDisplayStockItems()
    {
        System.debug('########################################################');
        System.debug('#### Starting Test:  testDisplayStockItems()');
        System.debug('########################################################');

        // create all neccessary datas
        Id plantId = SCHelperTestClass4.createTestPlant();
        List<SCStock__c> stocks = SCHelperTestClass4.createTestStocks(plantId);
        List<SCArticle__c> articleList = SCHelperTestClass4.createTestArticles();
        SCHelperTestClass4.createTestStockItem(stocks.get(0).Id, articleList.get(1).Id);
        List<SCStockProfile__c> profiles = SCHelperTestClass4.createTestStockProfiles();
        SCHelperTestClass4.createTestStockProfileAssocs(stocks.get(0).Id, stocks.get(1).Id,
                                                      profiles.get(0).Id, profiles.get(1).Id);
        
        Test.startTest();
        
        SCStockItemExtension stockItemExt = 
                     new SCStockItemExtension(new ApexPages.StandardController(stocks.get(0)));
        
        // test the methodes only for code coverage
        List<SelectOption> typeList = stockItemExt.getViewList();
        
        stockItemExt.view = '1';
        stockItemExt.fillLIst();
        System.assertEquals(null, stockItemExt.getStockItemsList());
        System.assertEquals(null, stockItemExt.getMatMoveList());
        System.assertEquals(null, stockItemExt.setController);
        
        stockItemExt.view = '2';
        stockItemExt.fillLIst();
        System.assertNotEquals(null, stockItemExt.getStockItemsList());
        System.assertEquals(null, stockItemExt.getMatMoveList());
        
        stockItemExt.view = '3';
        stockItemExt.fillLIst();
        System.assertNotEquals(null, stockItemExt.getStockItemsList());
        System.assertEquals(null, stockItemExt.getMatMoveList());
        
        stockItemExt.view = '4';
        stockItemExt.fillLIst();
        System.assertNotEquals(null, stockItemExt.getStockItemsList());
        System.assertEquals(null, stockItemExt.getMatMoveList());
        
        stockItemExt.view = '5';
        stockItemExt.fillLIst();
        System.assertEquals(null, stockItemExt.getStockItemsList());
        System.assertNotEquals(null, stockItemExt.getMatMoveList());
        
        stockItemExt.view = '6';
        stockItemExt.fillLIst();
        System.assertEquals(null, stockItemExt.getStockItemsList());
        System.assertNotEquals(null, stockItemExt.getMatMoveList());
        
        stockItemExt.view = '7';
        stockItemExt.fillLIst();
        System.assertEquals(null, stockItemExt.getStockItemsList());
        System.assertNotEquals(null, stockItemExt.getMatMoveList());
        
        stockItemExt.view = '8';
        stockItemExt.fillLIst();
        System.assertEquals(null, stockItemExt.getStockItemsList());
        System.assertNotEquals(null, stockItemExt.getMatMoveList());
        
        stockItemExt.view = '9';
        stockItemExt.fillLIst();
        System.assertEquals(null, stockItemExt.getStockItemsList());
        System.assertNotEquals(null, stockItemExt.getMatMoveList());
        
        stockItemExt.view = profiles.get(0).Id;
        stockItemExt.fillLIst();
        System.assertNotEquals(null, stockItemExt.getStockItemsList());
        System.assertEquals(null, stockItemExt.getMatMoveList());
        
        stockItemExt.articleId = articleList.get(1).Id;
        stockItemExt.readArticleMatMoves();

        Test.stopTest();
    } // testDisplayStockItems
} // SCStockItemExtensionTest
