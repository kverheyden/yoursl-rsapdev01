/*
 * @(#)SCfwPageControllerBaseTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Basic Tests
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCfwPageControllerBaseTest 
{
    static testMethod void constructorTest() 
    {
        SCfwComponentControllerBase con = new SCfwComponentControllerBase();
        SCfwPageControllerBase pcon = new SCfwPageControllerBase();
        pcon.setComponentController(con);
        Map<String, SCfwComponentControllerBase> m = pcon.getComponentControllerMap();
        pcon.setComponentControllerMap('test', con);
        pcon.OnUpdateData('ctx', null);
        
        System.assertEquals(con, pcon.getComponentController());
        System.assertEquals(pcon, pcon.getThis());
    }

    /**
     * Save an order via the BO w/o having all account for 
     * the defined roles already.
     */
/*  from SCBOOrderTest
    static testMethod void saveWORoleAccount1() 
    {
        
        SCOrder__c localOrder = SCHelperTestClass4.createOrder(true);
        SCOrderRole__c orderRole = 
            SCHelperTestClass4.orderRolePerson(localOrder, 
                                             SCfwConstants.DOMVAL_ORDERROLE_LE,
                                             true);

    
        Test.startTest();

        SCboOrder.OrderRoleObj orderRoleObj = new SCboorder.OrderRoleObj(orderRole);               
        SCboOrder boOrder = new SCboOrder(localOrder);
        
        boOrder.save();
        
        Test.stopTest();
//// ###### not completed!!!
        System.assert(true);
    }
*/
    
    /**
     * Test the if the image text for an empty role is return as empty string
     */
/*  from SCBOOrderTest
    static testMethod void saveEmptyRoleImageText()
    {
        SCOrder__c localOrder = SCHelperTestClass4.createOrder(true);
        
        Test.startTest();

        SCboOrder.OrderRoleObj orderRoleObj = new SCboorder.OrderRoleObj();               
        SCboOrder boOrder = new SCboOrder(localOrder);
        
        String txt = orderRoleObj.imageText;

        Test.stopTest();
        
        System.assertEquals('', txt);
    }
*/
    /**
     * Test the if the locked status "locked" image text is set properly
     */
/*  from SCBOOrderTest
    static testMethod void saveRiskClassImageText()
    {
        SCOrder__c localOrder = SCHelperTestClass4.createOrder(true);
        SCOrderRole__c orderRole = 
            SCHelperTestClass4.orderRolePerson(localOrder, 
                                             SCfwConstants.DOMVAL_ORDERROLE_LE,
                                             true);

        // FIXME use constants!
        orderRole.RiskClass__c = '51001';
        
        Test.startTest();

        SCboOrder.OrderRoleObj orderRoleObj = new SCboorder.OrderRoleObj(orderRole);               
        SCboOrder boOrder = new SCboOrder(localOrder);

        String txt = orderRoleObj.imageText;

        Test.stopTest();
        
        //System.assert(txt.contains('locked'));
    }
*/
    
    /**
     * Test the if the locked status "locked" image text is set properly
     */
/*  from SCBOOrderTest
    static testMethod void saveLockImageTextLocked()
    {
        SCOrder__c localOrder = SCHelperTestClass4.createOrder(true);
        SCOrderRole__c orderRole = 
            SCHelperTestClass4.orderRolePerson(localOrder, 
                                             SCfwConstants.DOMVAL_ORDERROLE_LE,
                                             true);

        orderRole.LockType__c = SCfwConstants.DOMVAL_LOCKTYP_LOCKED;
        
        Test.startTest();

        SCboOrder.OrderRoleObj orderRoleObj = new SCboorder.OrderRoleObj(orderRole);               
        SCboOrder boOrder = new SCboOrder(localOrder);

        String txt = orderRoleObj.imageText;

        Test.stopTest();
        
        //System.assert(txt.contains('locked'));
    }
*/    

    
    /**
     * Test the if the locked status "cash" image text is set properly
     */
/*  from SCBOOrderTest
    static testMethod void saveLockImageTextCash()
    {
        SCOrder__c localOrder = SCHelperTestClass4.createOrder(true);
        SCOrderRole__c orderRole = 
            SCHelperTestClass4.orderRolePerson(localOrder, 
                                             SCfwConstants.DOMVAL_ORDERROLE_LE,
                                             true);

        orderRole.LockType__c = SCfwConstants.DOMVAL_LOCKTYP_CASH;

        Test.startTest();

        SCboOrder.OrderRoleObj orderRoleObj = new SCboorder.OrderRoleObj(orderRole);               
        SCboOrder boOrder = new SCboOrder(localOrder);

        String txt = orderRoleObj.imageText;        

        Test.stopTest();
        
        //System.assert(txt.contains('cash'));
    }
*/

    /**
     * Test the if the VIP level 'platin' image text is set properly
     */
/*  from SCBOOrderTest
    static testMethod void saveVIPImageTextPlatin()
    {
        String level = 'platin';
        
        SCOrder__c localOrder = SCHelperTestClass4.createOrder(true);
        SCOrderRole__c orderRole = 
            SCHelperTestClass4.orderRolePerson(localOrder, 
                                             SCfwConstants.DOMVAL_ORDERROLE_LE,
                                             true);

        orderRole.VipLevel__c = level;

        Test.startTest();

        SCboOrder.OrderRoleObj orderRoleObj = new SCboorder.OrderRoleObj(orderRole);               
        SCboOrder boOrder = new SCboOrder(localOrder);
        
        String txt = orderRoleObj.imageText;

        Test.stopTest();
        
        //System.assert(txt.contains(level));
    }
*/

    /**
     * Test the if the VIP level 'gold' image text is set properly
     */
/*  from SCBOOrderTest
    static testMethod void saveVIPImageTextGold()
    {
        String level = 'gold';
        SCOrder__c localOrder = SCHelperTestClass4.createOrder(true);
        SCOrderRole__c orderRole = 
            SCHelperTestClass4.orderRolePerson(localOrder, 
                                             SCfwConstants.DOMVAL_ORDERROLE_LE,
                                             true);

        orderRole.VipLevel__c = level;

        Test.startTest();

        SCboOrder.OrderRoleObj orderRoleObj = new SCboorder.OrderRoleObj(orderRole);               
        SCboOrder boOrder = new SCboOrder(localOrder);

        String txt = orderRoleObj.imageText;        

        Test.stopTest();
        
        //System.assert(txt.contains(level));
    }
*/
    /**
     * Test the if the VIP level 'gold' image text is set properly
     */
/*  from SCBOOrderTest
    static testMethod void saveVIPImageTextSilver()
    {
        String level = 'silver';
        
        SCOrder__c localOrder = SCHelperTestClass4.createOrder(true);
        SCOrderRole__c orderRole = 
            SCHelperTestClass4.orderRolePerson(localOrder, 
                                             SCfwConstants.DOMVAL_ORDERROLE_LE,
                                             true);

        orderRole.VipLevel__c = level;

        Test.startTest();

        SCboOrder.OrderRoleObj orderRoleObj = new SCboorder.OrderRoleObj(orderRole);               
        SCboOrder boOrder = new SCboOrder(localOrder);

        String txt = orderRoleObj.imageText;        

        Test.stopTest();
        
        //System.assert(txt.contains(level));
    }
*/
    /**
     * Test the ...
     */
/*  from SCBOOrderTest
    static testMethod void testImageNameEmptyRole()
    {
        String name = 'blank';
        
        SCOrder__c localOrder = SCHelperTestClass4.createOrder(true);

        Test.startTest();


        SCboOrder.OrderRoleObj orderRoleObj = new SCboorder.OrderRoleObj();                              
        SCboOrder boOrder = new SCboOrder(localOrder);

        String imageName = orderRoleObj.image;        

        Test.stopTest();
        
        System.assert(imageName.contains(name));
    }


    static testMethod void testImageNameVIPSilver()
    {
        String name = '00';
        String level = 'silver';

        SCOrder__c localOrder = SCHelperTestClass4.createOrder(true);

        SCOrderRole__c orderRole = 
            SCHelperTestClass4.orderRolePerson(localOrder, 
                                             SCfwConstants.DOMVAL_ORDERROLE_LE,
                                             true);

        orderRole.VipLevel__c = level;

        Test.startTest();

        SCboOrder.OrderRoleObj orderRoleObj = new SCboorder.OrderRoleObj(orderRole);
                             
        SCboOrder boOrder = new SCboOrder(localOrder);

        String imageName = orderRoleObj.image;        

        Test.stopTest();
        
        System.assert(imageName.contains(name));
    }

    static testMethod void testImageNameLockTypeCash()
    {
        String name = '00';

        SCOrder__c localOrder = SCHelperTestClass4.createOrder(true);

        SCOrderRole__c orderRole = 
            SCHelperTestClass4.orderRolePerson(localOrder, 
                                             SCfwConstants.DOMVAL_ORDERROLE_LE,
                                             true);

        orderRole.LockType__c = SCfwConstants.DOMVAL_LOCKTYP_CASH;

        Test.startTest();

        SCboOrder.OrderRoleObj orderRoleObj = new SCboorder.OrderRoleObj(orderRole);
                            
        SCboOrder boOrder = new SCboOrder(localOrder);

        String imageName = orderRoleObj.image;        

        Test.stopTest();
        
        System.assert(imageName.contains(name));
    }

    static testMethod void testImageNameRiskClass()
    {
        String name = '00';

        SCOrder__c localOrder = SCHelperTestClass4.createOrder(true);

        SCOrderRole__c orderRole = 
            SCHelperTestClass4.orderRolePerson(localOrder, 
                                             SCfwConstants.DOMVAL_ORDERROLE_LE,
                                             true);

        // FIXME use constant instead
        orderRole.RiskClass__c = '51001';

        Test.startTest();

        SCboOrder.OrderRoleObj orderRoleObj = new SCboorder.OrderRoleObj(orderRole);
                             
        SCboOrder boOrder = new SCboOrder(localOrder);

        String imageName = orderRoleObj.image;        

        Test.stopTest();
        
        //System.assert(imageName.contains(name));
    }
*/
}
