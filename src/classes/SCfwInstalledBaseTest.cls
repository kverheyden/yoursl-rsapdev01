/*
 * @(#)SCfwInstalledBaseTest.cls SCCloud    dh 20.09.2010
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author Dietrich Herdt <dherdt@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCfwInstalledBaseTest
{
    private static SCfwInstalledBase fwInstalledBase;
    
    /**
     * SCfwInstalledBase under positiv test 1
     */       
    static testMethod void testSCfwInstalledBasePositiv1()
    {
        SCHelperTestClass.createOrderTestSet(true);
        
        //DH: createArticle is includet in createInstalledBase
        //SCHelperTestClass.createArticle(true);
        SCHelperTestClass.createInstalledBase(true);
        
        fwInstalledBase = new SCfwInstalledBase(); 
        fwInstalledBase.addInstalledBaseRole('TestRole');
              
        fwInstalledBase.save();

        SCfwInstalledBase.getInstalledBaseByOwner(SCHelperTestClass.account.Id);
        SCfwInstalledBase.getInstalledBaseLocationByOwner(SCHelperTestClass.account.Id);
    }

    /**
     * SCfwInstalledBase under positiv test 2
     */       
    static testMethod void testSCfwInstalledBasePositiv2()
    {
        SCHelperTestClass.createOrderTestSet(true);
        
        //DH: createArticle is includet in createInstalledBase
        //SCHelperTestClass.createArticle(true);
        SCHelperTestClass.createInstalledBase(true);
        
        //fwInstalledBase = new SCfwInstalledBase(SCHelperTestClass.installedBase.get(0));
        fwInstalledBase = new SCfwInstalledBase(SCHelperTestClass.installedBaseSingle);
        fwInstalledBase.addInstalledBaseRole('TestRole');

        fwInstalledBase = new SCfwInstalledBase(SCHelperTestClass.account.Id, SCHelperTestClass.location.Id);
        //fwInstalledBase.addDefaultRoles();
        //fwInstalledBase.hasRole('TestRole');      
        fwInstalledBase.save();

        SCfwInstalledBase.getInstalledBaseByOwner(SCHelperTestClass.account.Id);
        SCfwInstalledBase.getInstalledBaseLocationByOwner(SCHelperTestClass.account.Id);
    }

    /**
     * SCfwInstalledBase under positiv test 3
     */       
    static testMethod void testSCfwInstalledBasePositiv3()
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createInstalledBase(true);
        
        fwInstalledBase = new SCfwInstalledBase(SCHelperTestClass.account.Id, SCHelperTestClass.location.Id);  
        fwInstalledBase.save();

        SCfwInstalledBase.getInstalledBaseByOwner(SCHelperTestClass.account.Id);
        SCfwInstalledBase.getInstalledBaseLocationByOwner(SCHelperTestClass.account.Id);
    }
            
    /**
     * SCfwInstalledBase under negativ test
     */          
    static testMethod void testSCfwInstalledBaseNegativ()
    {
        /*
        SCHelperTestClass.createOrderTestSet(true);
        
        SCHelperTestClass.createArticle(true);
        SCHelperTestClass.createInstalledBase(true);
        */    
    }   
            
    /**
     * Test the search for installed base location ba account id and geo coordinates.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */          
    static testMethod void testSearchForLocation()
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createInstalledBase(true);
        
        List<SCInstalledBaseLocation__c> locs = SCfwInstalledBase.getInstBaseLocByGeoCoord(
                                                                         SCHelperTestClass.account.Id, 
                                                                         SCHelperTestClass.account.GeoX__c, 
                                                                         SCHelperTestClass.account.GeoY__c);
        System.assertEquals(1, locs.size());
    } // testSearchForLocation
}
