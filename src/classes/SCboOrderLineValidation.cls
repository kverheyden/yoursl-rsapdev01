/*
 * @(#)SCboOrderLineValidation.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * www.gms-online.de 
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of
 * GMS Development GmbH. ("Confidential Information").  You shall not disclose 
 * any confidential information and shall use it only in accordance with the 
 * terms of the license agreement you aggreed with GMS.
 */
 
/**
 *
 * @author Martin Assmann <massmann@gms-online.de>
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCboOrderLineValidation
{
    private static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();

    /**
     * Validates a business object from the type order line.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static Boolean validate(SCboOrderLine boOrderLine)
    {
        return validate(boOrderLine, SCboOrderValidation.ALL);
    }
    public static Boolean validate(SCboOrderLine boOrderLine, Integer mode)
    {
        Boolean ret = true;
        
        System.debug('#### validate(): boOrderLine -> ' + boOrderLine);
        if ((mode & SCboOrderValidation.ORDERLINE_DATA) == SCboOrderValidation.ORDERLINE_DATA)
        {
            if (!SCBase.isSet(boOrderLine.orderLine.Order__c))
            {
                throw new SCfwException(2500);
            }
            if (!SCBase.isSet(boOrderLine.orderLine.Article__c))
            {
                throw new SCfwException(2501);
            }
            if (null == boOrderLine.orderLine.Qty__c)
            {
                throw new SCfwException(2502);
            }
            if (!SCBase.isSet(boOrderLine.orderLine.Type__c))
            {
                throw new SCfwException(2503);
            }
            if (!SCBase.isSet(boOrderLine.orderLine.MaterialStatus__c))
            {
                throw new SCfwException(2504);
            }
            if (!SCBase.isSet(boOrderLine.orderLine.PriceType__c))
            {
                throw new SCfwException(2505);
            }
            if (null == boOrderLine.orderLine.PositionPrice__c)
            {
                throw new SCfwException(2506);
            }
            if (null == boOrderLine.orderLine.Price__c)
            {
                throw new SCfwException(2507);
            }
            if (null == boOrderLine.orderLine.UnitPrice__c)
            {
                throw new SCfwException(2508);
            }
            if (null == boOrderLine.orderLine.UnitNetPrice__c)
            {
                throw new SCfwException(2509);
            }
            if (null == boOrderLine.orderLine.Tax__c)
            {
                throw new SCfwException(2510);
            }
            if (null == boOrderLine.orderLine.TaxRate__c)
            {
                throw new SCfwException(2511);
            }
            if (!SCBase.isSet(boOrderLine.orderLine.InvoicingType__c))
            {
                throw new SCfwException(2512);
            }
            /* CCEAG
            if (!SCBase.isSet(boOrderLine.orderLine.InvoicingSubType__c))
            {
                throw new SCfwException(2513);
            }
            */
            if (null == boOrderLine.orderLine.ContractRelated__c)
            {
                throw new SCfwException(2514);
            }
            if (null == boOrderLine.orderLine.ListPrice__c)
            {
                throw new SCfwException(2516);
            }
            
            if (SCBase.isSet(appSettings.VALIDATE_PICKLIST_VALUES__c) && 
                appSettings.VALIDATE_PICKLIST_VALUES__c.equals('1'))
            {
                if (SCBase.isSet(boOrderLine.orderLine.AutoPos__c) && 
                    !SCBase.isPicklistValue(SCOrderLine__c.AutoPos__c, boOrderLine.orderLine.AutoPos__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderLine.orderLine.AutoPos__c, 'SCOrderLine__c.AutoPos__c'));
                }
                
                if (SCBase.isSet(boOrderLine.orderLine.Country__c) && 
                    !SCBase.isPicklistValue(SCOrderLine__c.Country__c, boOrderLine.orderLine.Country__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderLine.orderLine.Country__c, 'SCOrderLine__c.Country__c'));
                }
                
                if (SCBase.isSet(boOrderLine.orderLine.InvoicingType__c) && 
                    !SCBase.isPicklistValue(SCOrderLine__c.InvoicingType__c, boOrderLine.orderLine.InvoicingType__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderLine.orderLine.InvoicingType__c, 'SCOrderLine__c.InvoicingType__c'));
                }
                
                if (SCBase.isSet(boOrderLine.orderLine.InvoicingSubtype__c) && 
                    !SCBase.isPicklistValue(SCOrderLine__c.InvoicingSubtype__c, boOrderLine.orderLine.InvoicingSubtype__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderLine.orderLine.InvoicingSubtype__c, 'SCOrderLine__c.InvoicingSubtype__c'));
                }
                
                if (SCBase.isSet(boOrderLine.orderLine.MaterialStatus__c) && 
                    !SCBase.isPicklistValue(SCOrderLine__c.MaterialStatus__c, boOrderLine.orderLine.MaterialStatus__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderLine.orderLine.MaterialStatus__c, 'SCOrderLine__c.MaterialStatus__c'));
                }
                
                if (SCBase.isSet(boOrderLine.orderLine.PriceType__c) && 
                    !SCBase.isPicklistValue(SCOrderLine__c.PriceType__c, boOrderLine.orderLine.PriceType__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderLine.orderLine.PriceType__c, 'SCOrderLine__c.PriceType__c'));
                }
                
                if (SCBase.isSet(boOrderLine.orderLine.Type__c) && 
                    !SCBase.isPicklistValue(SCOrderLine__c.Type__c, boOrderLine.orderLine.Type__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderLine.orderLine.Type__c, 'SCOrderLine__c.Type__c'));
                }
            } // if (SCBase.isSet(appSettings.VALIDATE_PICKLIST_VALUES__c) && ...
        } // if ((mode & SCboOrderValidation.ORDERLINE_DATA) == SCboOrderValidation.ORDERLINE_DATA)
        
        return ret;
    } // validate
} // SCboOrderLineValidation
