/*
 * @(#)CCWCOrderCreate.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * The class creates in SAP an work order based on the sepcified Clockport order.
 * The SAP system sends asynchronously the response (see CCWCOrderCreateResponse)
 * 
 * To start the callout:
 *   CCWCOrderCreate.callout(String oid, boolean async, boolean testMode)
 *
 * Methods to override: 
 *      fillAndSendERPData(...)
 *      fillAndSendERPData(...)
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
public without sharing class CCWCOrderCreate extends SCInterfaceExportBase
{

    public CCWCOrderCreate()
    {
    }
    public CCWCOrderCreate(String interfaceName, String interfaceHandler, String refType, String subclassName)
    {
        super(interfaceName, interfaceHandler, refType, subclassName);
    }
    public CCWCOrderCreate(String interfaceName, String interfaceHandler, String refType)
    {
        //super(interfaceName, interfaceHandler, refType, subclassName);
    }

    /**
     * Prepare to make the webservice callout synchrounously or asynchronously (future)
     *
     * ### Set the interface name, its interface handler and a refType for head id in an interface object!
     *
     * @param oid   order id
     * @param async     true: use a future method to call 
     *                  false: do a synchrounous callout
     * @param testMode true: emulate call
     */
    public static String callout(String oid, boolean async, boolean testMode)
    {
        String interfaceName = 'SAP_ORDER_CREATE';
        String interfaceHandler = 'CCWCOrderCreate';
        String refType = 'SCOrder__c';
        CCWCOrderCreate oc = new CCWCOrderCreate(interfaceName, interfaceHandler, refType);
        return oc.calloutCore(oid, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCOrderCreate');
    } // callout   

    /**
     * Reads an order to send
     *
     * @param orderId
     * @param testMode if true no call is made, because in the test mode the salesforce framework prevent it.
     *
     */
    public override void readSalesforceData(String orderID, Map<String, Object> retValue, Boolean testMode)
    {
        List<SCOrder__c> ol =  [Select ID, Name, IdExt__c, Type__c, DepartmentCurrent__r.Name, Description__c, 
                               CustomerPrefStart__c, PriceList__r.ID2__c, PlannerGroup__c, 
                               CompanyCode__c, SalesArea__c, Division__c, DistributionChannel__c, SalesOffice__c, 
                               isWorkshopOrder__c, SalesGroup__c,ERPLongtext__c,
                               (Select Installedbase__r.ProductModel__r.Name, Installedbase__r.IdExt__c, 
                                ErrorText__c, ErrorSymptom1__c, ErrorSymptom2__c,
                                IBProductModel__c, IBProductModel__r.Name from OrderItem__r),
                               (select ID, OrderRole__c, AccountNumber__c, Account__r.AccountNumber  from OrderRole__r where Orderrole__c in ('50301', '50303') order by OrderroleNumeric__c asc)
                                from SCOrder__c where ID = : orderId and ERPStatusOrderCreate__c in ('none', 'error')]; 
                               
        if(ol.size() > 0)
        {
            retValue.put(ROOT, ol[0]);
            debug('order: ' + retValue);
        }
        else
        {	
        	String msg = '';
            setReferenceID(orderID);           
            List<SCOrder__c> ol2 = [Select ERPStatusOrderCreate__c from SCOrder__c where id = :orderID];
            if(ol2.size() > 0)
            {
            		msg += 'The order create callout for the order with id: ' + orderId + ' was cancelled!';
                    msg += '\n The interface has the state (ERPStatusOrderCreate__c: [' + ol2[0].ERPStatusOrderCreate__c + ']).';
                    msg += ' The allowed states are [none] or [error].'; 
                    
                    if (ol2[0].ERPStatusOrderCreate__c == 'pending')
                    {
                    	msg += '\n The status \'pending\' indicates that there was already an outgoing order create call, thus wait for the SAP answer. \n No further action required';
                    }
            }
            else
            {
                msg = 'There is no order with id: ' + orderId + '!';
            }
            debug(msg);
            throw new SCfwException(msg);
        }
    }

    /**
     * Fills the send structure and sends the call to SAP
     * 
     * @param dataMap with the order under the key ROOT
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure
     *           message = data sent to ERP
     *           message_v1 = endpoint 
     *           message_v2 = a return value of web service 
     *           message_v3 = a mandatory fields error
     *
     */
    public override Boolean fillAndSendERPData(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs)
    {
        Boolean retValue = true;
        SCOrder__c order = (SCOrder__c)dataMap.get(ROOT);
        // instantiate the client of the web service
        piCceagDeSfdcCSOrderCreate.CustomerServiceOrderCreate_OutPort ws = new piCceagDeSfdcCSOrderCreate.CustomerServiceOrderCreate_OutPort();

        // sets the basic authorization for the web service
        ws.inputHttpHeaders_x = u.getBasicAuth();

        // sets the endpoint of the web service
        ws.endpoint_x  = u.getEndpoint('OrderCreate');
        debug('endpoint: ' + ws.endpoint_x);
        rs.message_v1 = ws.endpoint_x;
        ws.timeout_x = u.getTimeOut();
        
        try
        {
            // instantiate a message header
            piCceagDeSfdcCSOrderCreate.BusinessDocumentMessageHeader messageHeader = new piCceagDeSfdcCSOrderCreate.BusinessDocumentMessageHeader();
            setMessageHeader(messageHeader, order, messageID, u, testMode);
            
            // instantiate the body of the call
            piCceagDeSfdcCSOrderCreate.CustomerServiceOrder_element customerServiceOrder = new piCceagDeSfdcCSOrderCreate.CustomerServiceOrder_element();
            
            // set the data to the body
            // rs.message_v3 = 
            setCustomerServiceOrder(customerServiceOrder, order, u, testMode);

            String jsonInputMessageHeader = JSON.serialize(messageHeader);
            String fromJSONMapMessageHeader = getDataFromJSON(jsonInputMessageHeader);
            debug('from json Message Header: ' + fromJSONMapMessageHeader);

            String jsonInputOrder = JSON.serialize(customerServiceOrder);
            String fromJSONMapOrder = getDataFromJSON(jsonInputOrder);
            debug('from json Order: ' + fromJSONMapOrder);
            
            rs.message = '\n\nMessageHeader: ' + fromJSONMapMessageHeader + '\n\nmessageData: ' + fromJSONMapOrder ;
            debug('rs.message: ' + rs.message);
            // check if there are missing mandatory fields
            if(rs.message_v3 == null || rs.message_v3 == '')
            {
                debug('message_v3 ok');
                // All mandatory fields are filled
                // callout
                if(!testMode)
                {
                    debug('real mode');
                    // It is not the test from the test class
                    // go
                    rs.setCounter(1);
                    ws.CustomerServiceOrderCreate_Out(messageHeader, customerServiceOrder);
                    rs.Message_v2 = 'void';
                }
            }
            debug('after sending');
        }
        catch(Exception e) 
        {
            throw e;
        }
        return retValue;
    }
    
    /**
     * Sets the ERPStatusOrderCreate in the root object to 'pending'
     * 
     * @param dataMap of obects to dispatch
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure with error messages
     * @param error true if some error has been occured
     */
    public override void setERPStatus(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs, Boolean error)
    {
        if(dataMap != null)
        {
            SCOrder__c order = (SCOrder__c)dataMap.get(ROOT);
            if(order != null)
            {
                if(error)
                {
                    order.ERPStatusOrderCreate__c = 'error';
                }
                else
                {   
                    order.ERPStatusOrderCreate__c = 'pending';
                }   
                update order;
                debug('order after update: ' + order);
            }
        }       
    }
    
    /**
     * sets data into the message header
     * messageID, referenceID = order.Name, CreationDateTime, SenderBusinessSystemID, RecipientBusinessSystemID
     *
     * @param mh message header 
     * @param order 
     * @param messageID MS: milliseconds R: random, e.g.    MS:1351254139745-R:0.7486318721270884
     * @param u CCWSUtil instance
     * @param testMode used to prevent call out by calls from CCWCOrderCreateTest class
     *
     */
    public void  setMessageHeader(piCceagDeSfdcCSOrderCreate.BusinessDocumentMessageHeader mh, 
                                         SCOrder__c order, String messageID, CCWSUtil u, Boolean testMode)
    {
        mh.MessageID = messageID;
//      mh.MessageUUID;
        mh.ReferenceID = order.Name;
//      mh.ReferenceUUID;
        mh.CreationDateTime = u.getFormatedCreationDateTime();
        if(testMode)
        {
            mh.TestDataIndicator = 'Test';
        }    
        mh.SenderBusinessSystemID = u.getSenderBusinessSystemID();
        mh.RecipientBusinessSystemID = u.getRecipientBusinessSystemID ();
        debug('Message Header: ' + mh);
    }

    /**
     * sets the call out order structure with data form an order
     *
     * @param cso callout order sturcture
     * @param order order with input data
     * @param u instance of the util class CCWSUtil
     * @param testMode used to prevent call out by calls from CCWCOrderCreateTest class
     *
     */
    public String setCustomerServiceOrder(piCceagDeSfdcCSOrderCreate.CustomerServiceOrder_element cso, 
                                        SCOrder__c order, CCWSUtil u, Boolean testMode)
    {
        CCSettings__c ccset = CCSettings__c.getInstance();
    
        String retValue = '';
        String step = '';
        try
        {
            step = 'OrderType';
            cso.OrderType = u.getSAPOrderType(order.Type__c); 
            retValue = checkMandatory(retValue, cso.OrderType, 'Order Type');                
            
            //### GMSNA: sollte richtig sein - mit CC klären, ob fest oder dynamisch (abhängig von der Regionalen Niederlassung)
            step = 'MaintenancePlanningPlant = order.DepartmentCurrent__r.Name';
            cso.MaintenancePlanningPlant = order.DepartmentCurrent__r.Name;
            retValue = checkMandatory(retValue, cso.MaintenancePlanningPlant, 'Department Current');                
            
            
            // 27.11.2012 KU fixed text
            cso.ShortText = 'Clockport Order'; // order.Description__c;
            
            // 27.11.2012 KU do not pass this information
            if(ccset.SAPOrderShortText__c != null)
            {
                cso.ShortText = ccset.SAPOrderShortText__c;
            }
            
            // 28.11.2012 KU planner group wird benötigt            
            cso.PlannerGroup = u.getPlannerGroup(order.PlannerGroup__c);
            

            //### GMSNA: sollte richtig sein - mit CC klären, ob fest oder dynamisch (abhängig von der Regionalen Niederlassung)
            cso.MainWorkcenter = u.getWorkCenter();
            retValue = checkMandatory(retValue, cso.MainWorkcenter, 'User Work Center ');                
            
            //### GMSNA: sollte richtig sein - mit CC klären, ob fest oder dynamisch (abhängig von der Regionalen Niederlassung)
            // ### cso.MaintenancePlanningPlant = wird auf die IB.Plant gesetzt
            cso.MaintenancePlanningPlant = order.DepartmentCurrent__r.Name;
            cso.WorkCenterPlant          = order.DepartmentCurrent__r.Name;

            cso.BasicStartDate = u.formatDate(order.CustomerPrefStart__c);
            retValue = checkMandatory(retValue, cso.BasicStartDate , 'Customer Prefered Start Date');                
            
            // GMSGB 28.08.13
            // If the order is an SAP Workshop order (SAP Vendor Portal), it is possible that it has no 
            // Equipment, only a product number.
            // in such a case we have to leave the IDExt empty and set the prduct model from the 
            // order item
            if(order.Type__c == SCfwConstants.DOMVAL_ORDERTYPE_INHOUSE) // ZC15
            {
            	if( order.OrderItem__r[0].InstalledBase__c != null)
            	{
            		step = 'EquipmentNumber = order.OrderItem__r[0].InstalledBase__r.IdExt__c';
		            cso.EquipmentNumber = order.OrderItem__r[0].InstalledBase__r.IdExt__c;
		            		
		            step = 'Assembly = order.OrderItem__r[0].Installedbase__r.ProductModel__r.Name';
		            cso.Assembly = order.OrderItem__r[0].Installedbase__r.ProductModel__r.Name;
		            
            	}
            	else // Workshop order for product model without specific EQ
            	{
            		step = 'EquipmentNumber = null';
		            cso.EquipmentNumber = null;
		            	
		            step = 'Assembly = order.OrderItem__r[0].IBProductModel__c.Name';
		            cso.Assembly = order.OrderItem__r[0].IBProductModel__r.Name;
            	}
            }
            else
            {           
	            step = 'EquipmentNumber = order.OrderItem__r[0].InstalledBase__r.IdExt__c';
	            cso.EquipmentNumber = order.OrderItem__r[0].InstalledBase__r.IdExt__c;
	            	
	            step = 'Assembly = order.OrderItem__r[0].Installedbase__r.ProductModel__r.Name';
	            cso.Assembly = order.OrderItem__r[0].Installedbase__r.ProductModel__r.Name;
            }

            
            step = 'PriceList = order.PriceList__r.ID2__c';
            cso.PriceList = order.PriceList__r.ID2__c;
            
            cso.ReferenceNumber = order.Name;
            
            //--<Sales data>----------------------------------------
            step = 'SalesData';
            cso.SalesData = new piCceagDeSfdcCSOrderCreate.SalesData();

            // 1. sales data - company code is set in PI
            
            // 2. sales area 
            cso.SalesData.SalesOrg = order.SalesArea__c;
            retValue = checkMandatory(retValue, cso.SalesData.SalesOrg, 'Sales Area');                
            
            // 3. sales distribution channel 
            cso.SalesData.DistributionChannel = order.DistributionChannel__c;
            retValue = checkMandatory(retValue, cso.SalesData.DistributionChannel, 'Distribution Channel');                

            // 4. sales division
            step = 'SalesData.SalesDivision = order.Division__c';
            cso.SalesData.SalesDivision = order.Division__c;
            retValue = checkMandatory(retValue, cso.SalesData.SalesDivision, 'Sales Division');                

            // 5. sales office - is optional
            step = 'SalesData.SalesOffice = order.SalesOffice__c';
            cso.SalesData.SalesOffice = order.SalesOffice__c;
            cso.SalesData.SalesGroup = order.SalesGroup__c;
            
            //--<Partner roles>----------------------------------------
            // For testing, Orders by the WorkshopOrder have no order roles
            String irNo = getInvoiceRecipient(order);
            
            if(order.isWorkshopOrder__c != true && irNo != null) 
            {            
                cso.Partners = new piCceagDeSfdcCSOrderCreate.Partners_element();
                cso.Partners.Partner = new piCceagDeSfdcCSOrderCreate.Partner[2];
                cso.Partners.Partner[0] = new piCceagDeSfdcCSOrderCreate.Partner();

                
                    
                step = 'Setting an invoice recipient';
                cso.Partners.Partner[0].PartnerFunction = 'AG';
                cso.Partners.Partner[0].PartnerNumber = getInvoiceRecipient(order);
                retValue = checkMandatory(retValue, cso.Partners.Partner[0].PartnerNumber, 'order.OrderRole__r[IR].Account__r.AccountNumber');                
    
                step = 'Setting a service recipient';
                cso.Partners.Partner[1] = new piCceagDeSfdcCSOrderCreate.Partner();
                cso.Partners.Partner[1].PartnerFunction = 'WE';
    
                cso.Partners.Partner[1].PartnerNumber = order.OrderRole__r[0].Account__r.AccountNumber;
                retValue = checkMandatory(retValue, cso.Partners.Partner[1].PartnerNumber, 'order.OrderRole__r[SR].Account__r.AccountNumber');                
            }
            else
            {// create empty elements
                cso.Partners = new piCceagDeSfdcCSOrderCreate.Partners_element();
                cso.Partners.Partner = new List<piCceagDeSfdcCSOrderCreate.Partner>();
                //cso.Partners.Partner[0] = new piCceagDeSfdcCSOrderCreate.Partner();
                //cso.Partners.Partner[1] = new piCceagDeSfdcCSOrderCreate.Partner();
            }
            
            //--<Text>---------------------------------------------------
            cso.NotificationData = new piCceagDeSfdcCSOrderCreate.NotificationData_element();
            
            step = 'setting Long Text';
            
            //### pass the string 1:1 / SAP is responsible to split the lines 
            cso.NotificationData.LongText = new String[1];
            cso.NotificationData.LongText[0] = order.OrderItem__r[0].ErrorText__c;

            // 23.01.13 Fill the longtext into the order.longtext
            //List<SCOrder__c> updateOrderList = [Select ERPLongtext__c from SCOrder__c where id = :order.ID];
            //if(updateOrderList.size() > 0)
            //{
            //    SCOrder__c uo = updateOrderList[0]; 
            //    uo.ERPLongtext__c = order.OrderItem__r[0].ErrorText__c;
            //    update uo;
            //}

            /*
            // warum nicht einfach so?
            // cso.NotificationData.LongText = u.getArray(50, order.OrderItem__r[0].ErrorText__c);
            String []  errorTextArr = u.getArray(50, order.OrderItem__r[0].ErrorText__c);
            cso.NotificationData.LongText = new String[errorTextArr.size()];
            Integer i = 0;
            for(String line: errorTextArr)
            {
                cso.NotificationData.LongText[i] = line;
                i++;
            }
            */

            //--<Symptom codes>------------------------------------------
            step = 'NotificationData.CodeGroup = order.OrderItem__r[0].ErrorSymptom...';
            cso.NotificationData.CodeGroup = order.OrderItem__r[0].ErrorSymptom1__c;
            cso.NotificationData.CodeID    = order.OrderItem__r[0].ErrorSymptom2__c;
        }
        catch(Exception e)
        {
            String prevMsg = e.getMessage();
            String newMsg = 'setCustomerServiceOrder: ' + step + ' ' + prevMsg;
            String stack = SCfwException.getExceptionInfo(e);
            newMsg += stack;
            debug(newMsg);
            e.setMessage(newMsg);
            throw e;
        }
        return retValue;    
    }

    /**
     * gets the account number of the invoice recipient. If there is not such the service recipient is returned instead
     *
     * @param order the order tree with data
     *
     * @return the account number of the invoice recipient
     */
    public String getInvoiceRecipient(SCOrder__c order)
    {
        String retValue = null;     
        if(order.OrderRole__r.size() > 0)
        {
            retValue = order.OrderRole__r[0].Account__r.AccountNumber;
            if(order.OrderRole__r[1] != null)
            {
                retValue = order.OrderRole__r[1].Account__r.AccountNumber;
            }
        }
        else
        {
            // Commented for testing the Werkstattapp
            //String msg = 'Missing order roles by order: ' + order.Name;
            //debug(msg);
            //throw new SCfwException(msg);         
        }    
        return retValue;
    }
    
    /*
    public String getAccountNoForRole(SCOrder__c order, String role)
    {
    
    getAccountNoForRole(order, DOMVAL_ORDERROLE_LE)
    
DOMVAL_ORDERROLE_LE = '50301';
DOMVAL_ORDERROLE_AG = '50302';
DOMVAL_ORDERROLE_RE = '50303';
DOMVAL_ORDERROLE_RG = '50304';    
    
        for(order.OrderRole__r)
        {
        
    
        '50301', '50303'
            
        }
    }
    */    
        
    
    
    
    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }
}
