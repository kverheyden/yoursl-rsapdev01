/*
 * @(#)SCInstalledBaseTreeController.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  
 */
 
/**
 * Controller handling the installed base hierarchy tree.
 * Based on an example from the SF team.
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCInstalledBaseTreeController
{

    /**
     * Standard constructor
     */ 
    public SCInstalledBaseTreeController()
    {
        
    }
    
    /**
     * The actual Salesforce-Account-ID passed to component
     */ 
    public String accountId{
        get;
        set {
            accountId = value;
            System.debug('#### accountId: ' + value);
        }
    }
    
    /**
     * Container class for the installed base and location objects
     */ 
    public Class ReadLocation
    {
        public SCInstalledBaseLocation__c installedBaseLocation { get; set; }
        public List<SCInstalledBase__c> installedBase { get; set; }
        
        readLocation(SCinstalledBaseLocation__c installedBaseLocation, List<SCInstalledBase__c> installedBase)
        {
            this.installedBaseLocation = installedBaseLocation;
            this.installedBase = installedBase;
        }
    }
    
    /**
     * This list have all locations and the first level of these installed bases
     *
     * @return the list with object
     */ 
    public List<ReadLocation> getReadLocation()
    {
        List<ReadLocation> rl = new List<ReadLocation>();
        
        // GMSSU 22.08.2012 PMS 33526
        // Reading all inst. base roles for this account
        List<aggregateResult> roles = [ Select InstalledBaseLocation__c, InstalledBaseLocation__r.Id locId, InstalledBaseLocation__r.LocName__c locName
                                               From SCInstalledBaseRole__c 
                                               Where Account__c = :accountId 
                                               Group By InstalledBaseLocation__c, InstalledBaseLocation__r.Id, InstalledBaseLocation__r.LocName__c, Account__c 
                                               Limit 100 ];
        
        List<Id> iblIds = new List<Id>();
        for (AggregateResult ar : roles)
        {
            iblIds.add(String.valueOf(ar.get('locId')));
        }        
        Map<Id, SCInstalledBaseLocation__c> locationNames = new Map<ID, SCInstalledBaseLocation__c>([Select Id, Name 
                                                                                                     From SCInstalledBaseLocation__c 
                                                                                                     Where id IN :iblIds]);
        
        // Preparing the list
        List<SCInstalledBaseLocation__c> locations = new List<SCInstalledBaseLocation__c>();
               
        // GMSSU 22.08.2012 PMS 33526
        for (AggregateResult ar : roles)
        {      
            String lId   = String.valueOf(ar.get('locId'));
            String lName = String.valueOf(ar.get('locName'));
            String lNum = locationNames.get(lId).Name;
        
            SCInstalledBaseLocation__c tmpLoc = new SCInstalledBaseLocation__c( Id = lId, Description__c = lNum, LocName__c = lName );
            locations.add(tmpLoc);
        }
        
        // Reading all bases                                           
        List<SCInstalledBase__c> instBases = [ Select   Id, Name, 
                                                        Article__c, Article__r.Name, 
                                                        PurchaseDate__c,
                                                        Article__r.ArticleNameCalc__c,
                                                        Article__r.Class__c, 
                                                        SerialNo__c, ComBoxStatus__c, 
                                                        Status__c, PurchasePrice__c,
                                                        Room__c, InstalledBaseLocation__c,
                                                        Type__c, ConstructionYear__c,
                                                        Brand__r.Name, ContractRef__c, ContractStartDate__c, 
                                                        DeliveryDate__c, InstallationDate__c, 
                                                        HandoverDate__c, Sort__c, 
                                                        ProductGroup__c, System__r.Name, 
                                                        ProductModel__c, ProductModel__r.Group__c, 
                                                        ProductModel__r.Name, ProductModel__r.ComBoxType__c, 
                                                        ProductModel__r.ProductNameCalc__c, 
                                                        ProductUnitClass__c, ProductUnitType__c, ProductEnergy__c,
                                                        ProductSkill__c, ProductNameCalc__c,
                                                        InstalledBaseLocation__r.LocName__c, 
                                                        InstalledBaseLocation__r.Street__c, 
                                                        InstalledBaseLocation__r.HouseNo__c, 
                                                        InstalledBaseLocation__r.Extension__c, 
                                                        InstalledBaseLocation__r.FlatNo__c, 
                                                        InstalledBaseLocation__r.Floor__c, 
                                                        InstalledBaseLocation__r.CountryState__c, 
                                                        InstalledBaseLocation__r.City__c, 
                                                        InstalledBaseLocation__r.District__c, 
                                                        InstalledBaseLocation__r.County__c, 
                                                        InstalledBaseLocation__r.Country__c, 
                                                        InstalledBaseLocation__r.PostalCode__c, 
                                                        InstalledBaseLocation__r.GeoX__c,
                                                        InstalledBaseLocation__r.GeoY__c,
                                                        InstalledBaseLocation__r.GeoApprox__c, 
                                                        InstalledBaseLocation__r.Status__c,
                                                        (Select Id From Installed_Base__r),
                                                        IdExt__c, Description__c, PriceList__c, PriceList__r.Name, ManufacturerSerialNo__c, 
                                                        util_StockPlant__c, toLabel(TechnicalObjType__c)
                                               From SCInstalledBase__c 
                                               Where InstalledBaseLocation__c IN : locations
                                               AND Parent__c = null
                                               Order By System__c ];
                                              
        for(SCInstalledBaseLocation__c l : locations)
        {
            List<SCInstalledBase__c> tmpInstBaseList = new List<SCInstalledBase__c>();
            
            for(SCInstalledBase__c ib : instBases)
            {
                if(ib.InstalledBaseLocation__c == l.Id)
                {
                    tmpInstBaseList.add(ib);
                }
            }
            
            rl.add( new ReadLocation(l, tmpInstBaseList) );
        }

        
        System.debug('#### rl: ' + rl);
        
        return rl;
    }   

}
