public class SubtradechannelProductTriggerCLS 
{
    /**
     * @author thomas richter <thomas@meformobile.com>
     */
	public void setCloudCrazeProductReference(SubtradechannelProduct__c[] stcProducts) 
    {
        // collect all references products
        List<ID> prodIds = new List<ID>();
        for (SubtradechannelProduct__c stcProduct : stcProducts) {
            prodIds.add(stcProduct.Product2__c);
        }
        // get all referenced products
        List<Product2> products = [SELECT Id, ID2__c FROM Product2 WHERE Id in :prodIds];
        Map<ID,String> productIdsToMatNr = new Map<ID,String>();
        for (Product2 product : products) {
            productIdsToMatNr.put(product.Id, product.ID2__c);
        }

        // get corresponding cloudcraze product
		List<ccrz__E_Product__c> ccProducts = [SELECT Id, ccrz__ProductId__c FROM ccrz__E_Product__c WHERE ccrz__ProductId__c IN :productIdsToMatNr.values()];
        Map<String,ID> matNrToCCProductId = new Map<String,ID>();
        for (ccrz__E_Product__c ccProduct : ccProducts) {
            matNrToCCProductId.put(ccProduct.ccrz__ProductId__c, ccProduct.Id);
        }
        
        // now update subtradechannelproduct records
        for (SubtradechannelProduct__c stcProduct : stcProducts) {
            ID prodId = stcProduct.Product2__c;
            String matNr = productIdsToMatNr.get(prodId);
            ID ccProdId = matNrToCCProductId.get(matNr);
            stcProduct.ccrz_product__c = ccProdId;
        }
    }
}
