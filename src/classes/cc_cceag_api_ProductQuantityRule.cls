global with sharing class cc_cceag_api_ProductQuantityRule extends ccrz.cc_api_ProductQuantityRule {
    private Map<String, Object> storeSettings;
    
    global cc_cceag_api_ProductQuantityRule(){}

    public override void setStorefrontSettings(Map<String,Object> settings) {
        this.storeSettings = settings;
    }   

    public override Map<String, Map<String,Object>> getRules(List<String> skus) {
        Map<String,Map<String,Object>> retMap = new Map<String,Map<String,Object>>();
        List<ccrz__E_Product__c> prods = [SELECT Id, ccrz__SKU__c, NumberOfSalesUnitsPerPallet__c FROM ccrz__E_Product__c WHERE ccrz__SKU__c =: skus];
        for (ccrz__E_Product__c prod: prods) {
            Integer intVal = Integer.valueOf(prod.NumberOfSalesUnitsPerPallet__c);
            retMap.put(prod.ccrz__SKU__c, new Map<String,Object>{PARM_INCREMENT=>1, PARM_SKIP_INCREMENT=>intVal, PARM_MISC_DETAILS=>''});
        }
        return retMap;
    }

}
