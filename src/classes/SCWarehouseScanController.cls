/*
 * @(#)SCWarehouseScanController.cls
 *
 * Copyright 2014 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Implement a custom lookup to allow a highly sophisticated article search.
 *
 * @author Georg Birkenheuer <gbirkenheuer@gms-online.de>
 * @version $Revision$, $Date$
 */

public with sharing class SCWarehouseScanController 
{

	User user = null;
	SCVendorUser__c vendorUser = null;
	SCInstalledBase__c equipment = null;
    /**
     * Default constructor
     */
    public SCWarehouseScanController()
    {
    	//Select User and Vendor
    	CCWSUtil util = new CCWSUtil();
    	user = util.getUser();
    	vendorUser = [select id,name,Vendor__c from SCVendorUser__c where User__c =: user.id] ;
    	//vendorUser.
    	
        System.debug('##### SCWarehouseScanController(): vendorUser -> ' + vendorUser.name);
       
    }
    
    
    public SCInstalledBase__c getEquipment(String eqNumber)
    {
		SCInstalledBase__c equipment = [select id,name,id2__c,idExt__c,Brand__c, 	ManufacturerSerialNo__c,	
										Plant__c, Stock__r.Name, 	ShortText__c,	SerialNo__c
										from SCInstalledBase__c where 	IdExt__c =: eqNumber ];
		this.equipment = equipment;
		return equipment;	
	}
    
    @RemoteAction
    public static SCInstalledBase__c getEquipmentDetails(String eqNumber)
    {
    	System.debug('#### suche: ' + eqNumber);
		SCInstalledBase__c equipment = new SCInstalledBase__c();
		
		try
		{
			equipment = [select id,name,id2__c,idExt__c,Brand__c, Brand__r.Name, ManufacturerSerialNo__c, ProductNameCalc__c,
								Plant__c, Stock__r.Name, 	ShortText__c,	SerialNo__c
						 from SCInstalledBase__c 
						 where 	IdExt__c =: eqNumber
						 Limit 1 ];
		}
		catch(Exception e)
		{
			System.debug('#### err: ' + e.getMessage());
		}
					 
		return equipment;	
	}
    
    
    
}
