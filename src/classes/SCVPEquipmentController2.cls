/*
 * @(#)SCVPEquipmentController.cls
 * 
 * Copyright 2014 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author Mirco Stein
*/

public with sharing class SCVPEquipmentController2 extends SCPortal	
{


	
	
	
//######################################  INNER CLASSES  ##################################################

//###################################### GETTER & SETTER ##################################################

 
   /*
    * Equipment Type values
    * @return list of select options
  	*/
    public List<SelectOption> getEquipmentType()
    {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = SCInstalledBase__c.Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        SelectOption noneOption = new SelectOption('', 'Alle');
        options.add(noneOption);
        for(Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getValue(), f.getLabel()));
        }
        return options;
	} 
 
   /*
    * Equipment Status values
    * @return list of select options
  	*/
    public List<SelectOption> getEquipmentStatus()
    {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = SCInstalledBase__c.Status__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        SelectOption noneOption = new SelectOption('', 'Alle');
        options.add(noneOption);
        for(Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getValue(), f.getLabel()));
        }
        return options;
	}

   /*
    * This list is used on the visualforce page.
    * @return List of the equipments
  	*/	
	public List<SelectOption> getStocks() 
	{

	 	fetchStocks();	

	 	
	 	stockOptions = new List<SelectOption>();
	 	
	 	for(SCStock__c stock : stocklist)
	 	{
	 		stockOptions.add(new SelectOption(stock.id,stock.name));	
	 	}	

	 	return stockOptions;
	}
 
 

   
   /*
    * This list is used on the visualforce page.
    * @return List of the equipments
  	*/		

	public List<SCInstalledBase__c> getEquipment()
	{
		if(setCon != null)
		{
			try
			{
	 			return (List<SCInstalledBase__c>) setCon.getRecords();
			}
			catch(Exception e) 
			{
				addPageMessage(e, Label.SC_msg_VP_OrdersQueryError);
			}			
		}

		return null;
	}

        

	
//####################################  GLOBAL VARIABLES  #################################################
	
	private List<SCInstalledBase__c> 	g_EquipmentList 		= new List<SCInstalledBase__c>();
	public 	List<SCStock__c> 			stockList 				= new List<SCStock__c>();
	
//######################################   ATTRIBUTES    ##################################################
	
	
	public List<SelectOption> 			stockOptions			{ get; set; }	
	public String 						objectName 	 		 	{ get; set; }	
	public List<String> 				selectedStatusValues 	{ get; set; }
	public List<String> 				selectedTypeValues   	{ get; set; }
	public String						selectedVendorId    	{ get; set; }	
	public SCOrderExternalAssignment__c	selectedVendor 			{ get; set; }
		
	// Variables used for the list sorting
	public String  						sortField 				{ get; set; }
	public String  						sortOrder 				{ get; set; }
	public String  						sortFieldTmp = '';
	public Boolean 						reloadForSort 			{ get; set; }	
 
	// Need this flag to be able to query equipments again
	public Boolean 						filterQuery 			{ get; set; }
	
	public String						chosenStockId    		{ get; set; }
	public String						filterStockId    		{ get; set; }

    // Helper object used in the custom filter form
    public SCInstalledBase__c 			helperEqip 				{ get; set; }
    public String 						helperbrand				{ get; set; }
    public String 						helperMatNr				{ get; set; }
    public String 						helperMatSD				{ get; set; }
    
	// Order Edit Mode Flag
	public Boolean equipEdit { get; set; }    

	// Selected Equip ID
	public String selectedEquipID { get; set; }

//######################################  CONSTRUCTOR(S) ##################################################

	public SCVPEquipmentController2()
	{

		// Order edit mode ist disabled by default
		equipEdit = false;
		selectedEquipID = '';

		filterObjectType = 'SCInstalledBase__c';
		filterGroupId    = '004'; // Vendor Portal
		sortOrder = 'DESC';
		helperbrand = '';
		helperMatNr = '';
		helperMatSD = '';
		selectedVendorId = '';
		filterQuery = false;
		isVendor = false;

		fetchStocks();
		


		System.debug('###selectedTypeValues: ' + selectedVendor);
		System.debug('###selectedTypeValues: ' + vendor);		
		
		selectedStatusValues = new List<String>{''};
		selectedTypeValues = new List<String>{''};
		helperEqip = new SCInstalledBase__c();		
		
		// First of all read current user details
		readUser();
		// Then reading the vendor
		readVendor();		

		
		if(vendor!=null)
		{
			selectedVendorId = vendor.id;	
		}

		if(selectedVendor == null)
		{
			selectedVendor = new SCOrderExternalAssignment__c();	
		}
		
		// Now reading search filters for current user and object
		readFilters();						
	}


//######################################   MAIN METHODS  ##################################################

   /*
    * Initialiting Set-Controller
    * @return set of the order external assignments
  	*/
    public ApexPages.StandardSetController setCon 
    {
        get 
        {

        	System.debug('###setCon - chosenStockId: ' + chosenStockId);
        	System.debug('###setCon - selectedVendor: ' + selectedVendor);

        	
        	getStocks();
			try
			{
	        	if(!stocklist.isempty())
	        	{ 	   	
	            	if(!filterQuery)
	            	{
		            	if(sortFieldTmp != sortField)
		            	{
		            		sortOrder = 'DESC';
		            	}
		            	else
		            	{
		            		if(sortOrder == 'DESC')
		            		{
		            			sortOrder = 'ASC';
		            		}
		            		else
		            		{
		            			sortOrder = 'DESC';
		            		}
		            	}
	            	}
				
					// Building a query 
					// Order fields
					sortField = 'Status__c';
					
					if(chosenStockId == null)
					{
						if(!stocklist.isEmpty())
						{
							chosenStockId = stocklist[0].id;	
						}	
					}
	
					String query = 'SELECT ';
	
					for(Schema.FieldSetMember f : this.getFields()) 
					{
	            		query += f.getFieldPath() + ', ';
	        		}
	        		
					query += 'id, Name ';
					
					query += 'FROM SCInstalledBase__c ';
					
					
					// Status
					if(!selectedStatusValues.isEmpty() && String.isNotBlank(selectedStatusValues[0]))
					{	
						System.debug('###selectedStatusValues: ' + selectedStatusValues);
						query += checkWhereOrAnd() + ' Status__c = :selectedStatusValues ';
					}
	
					
					// Type
					if(!selectedTypeValues.isEmpty() && String.isNotBlank(selectedTypeValues[0]))
					{
						System.debug('###selectedTypeValues: ' + selectedTypeValues);
						query += checkWhereOrAnd() + ' Type__c = :selectedTypeValues ';
					}
				
					// Brand
					if(helperEqip.brand__c != null)
					{
						System.debug('###Brand: ' + helperEqip.brand__c);
	
						query += checkWhereOrAnd() + ' Brand__c =' + '\'' + helperEqip.brand__c + '\'';
					}				
					
					// MaterialNr
					if(helperMatNr != null && helperMatNr != '')
					{
						System.debug('###MatNr: ' + helperMatNr);
						query += checkWhereOrAnd() + ' ProductModel__r.ProductNameCalc__c = :helperMatNr ';
					}					
			
					// Material Short Description
					if(helperMatSD != null && helperMatSD != '')
					{
						System.debug('###MatSD: ' + helperMatSD);
						query += checkWhereOrAnd() + ' ProductModel__r.name = :helperMatSD ';
					}	
			
					if(chosenStockId != null && chosenStockId != '')
					{
						query +=  checkWhereOrAnd() + ' stock__c = ' + '\'' + chosenStockId + '\'';	
					}

					
	       			// Sort entries
					query += ' Order By ' + sortField + ' ' + sortOrder + ' NULLS LAST Limit 1000';				
					System.debug('###query: ' + query); 	
					setCon = new ApexPages.StandardSetController(Database.getQueryLocator(query)); 	
					whereOrAnd = '';
					

	        	}
			}
			catch(QueryException e)
			{
				addPageMessage(e, Label.SC_msg_VP_DatabaseQueryError);
			}
                  
            return setCon;
            
        }
        set;
    }

 
   /*
    * Initialiting Set-Controller
    * @return set of equipments
  	*/
    public List<SCStock__c> fetchStocks()
    {
		System.debug('###SCVPEquipmentController - fetchStocks');
		stockList.clear();

		if(isVendor)
		{
			System.debug('###SCVPEquipmentController - fetchStocks - VENDOR');	
			stockList = [	Select id, Name, Name__c, Vendor__c, Plant__c, Plant__r.name, TotalArticles__c, TotalArticleCount__c, Replenishment__c, Type__c 
			                From SCStock__c
			                Where Vendor__c = :vendor.id Limit 1000];				
		}
		if(!isVendor && (selectedVendor != null && selectedVendor.Vendor__c != null))
		{
			System.debug('###SCVPEquipmentController - fetchStocks - NOT VENDOR');
			System.debug('###SCVPEquipmentController - fetchStocks - selectedVendor: ' + selectedVendor);		
			stockList = [	Select id, Name, Name__c, Vendor__c, Plant__c, Plant__r.name, TotalArticles__c, TotalArticleCount__c, Replenishment__c, Type__c 
			                From SCStock__c
			                Where Vendor__c = :selectedVendor.Vendor__c Limit 1000];			
		}
		
		System.debug('###SCVPEquipmentController - fetchStocks - stockList: ' + stockList);	                    
	   return stockList; 		                    
	}


//######################################  HELPER METHODS ################################################## 
   
   /*
    * Standard Fields for this Controller via FieldSet
    */
    public List<Schema.FieldSetMember> getFields() 
    {
	    return SObjectType.SCInstalledBase__c.FieldSets.EquipmentPage.getFields();
    }
   
   /*
    * Assign selected filter values to the variables
    */
    public override void assignFilter(SCSearchFilter__c filter)
    {
        System.debug('###SCVPEquipmentController - assignFilter');
        
        helperEqip = new SCInstalledBase__c();		
        
        if(filter.Filter__c != null)
        {
            Map<String,String> mapOfFilters = (Map<String,String>)JSON.deserialize(filter.Filter__c, Map<String,String>.class);
			System.debug('###assignFilter - mapOfFilters: ' + mapOfFilters);
           
            if(mapOfFilters.get('Status__c') != null)
            {               
                selectedStatusValues = (List<String>)JSON.deserialize(mapOfFilters.get('Status__c'),List<String>.class);
                System.debug('###assignFilter - selectedStatusValues: ' + selectedStatusValues);
            }                           
            if(mapOfFilters.get('Type__c') != null)
            {
                selectedTypeValues = (List<String>)JSON.deserialize(mapOfFilters.get('Type__c'),List<String>.class);
                System.debug('###assignFilter - selectedTypeValues: ' + selectedTypeValues);
            }          
           
            if(mapOfFilters.get('helperEqip.SerialNo__c') != null)
            {
                
                helperEqip.SerialNo__c = mapOfFilters.get('helperEqip.SerialNo__c');
                System.debug('###assignFilter - helperEqip: ' + helperEqip);
            }  
            
            
            if(mapOfFilters.get('helperEqip.brand__c') != null)
            {               
                helperEqip.brand__c = mapOfFilters.get('helperEqip.brand__c');
            }            
            if(mapOfFilters.get('helperMatSD') != null)
            {               
                helperMatSD = mapOfFilters.get('helperMatSD');
            } 
            if(mapOfFilters.get('helperMatNr') != null)
            {               
                helperMatNr = mapOfFilters.get('helperMatNr');
            } 
            if(mapOfFilters.get('helperEqip.ManufacturerSerialNo__c') != null)
            {               
                helperEqip.ManufacturerSerialNo__c = mapOfFilters.get('helperEqip.ManufacturerSerialNo__c');
            }                        
 
            // Selected Vendor
            if(!isVendor)
            {
                if(mapOfFilters.get('selectedVendor.vendor__c') != null)
                {
                    selectedVendor.vendor__c = String.valueOf(mapOfFilters.get('selectedVendor.vendor__c'));
                }
            } 

            if(mapOfFilters.get('Name') != null)
            {
                objectName = mapOfFilters.get('Name');
            }
 
        }
         
        selectedFilter = filter.Id;
    }
    
    
/*
    * Generates a JSON string from all filter fields.
    * This string will be saved to the Filter__c field of the SCSearchFilter_c object
    * @return JSON string
    */
    public override String generateJSONFilter()
    {
       	
       	System.debug('###SCVPEquipmentController - generateJSONFilter');
       	System.debug('###SCVPEquipmentController - generateJSONFilter - objectName: ' + objectName);
       	
        Map<String,String> params = new Map<String, String>();
        params.put('Name',objectName);      

        params.put('Type__c',  JSON.serialize(selectedTypeValues));
        params.put('Status__c',JSON.serialize(selectedStatusValues));

        if(helperEqip.SerialNo__c != null)
        {
            params.put('helperEqip.SerialNo__c',String.valueOf(helperEqip.SerialNo__c));
        }
    	
    	// Selected Vendor
    	if(!isVendor && selectedVendor.Vendor__c != null)
    	{
    		params.put('selectedVendor.vendor__c',String.valueOf(selectedVendor.Vendor__c));
    	}
      
    

        if(helperEqip.brand__c != null)
        {
            params.put('helperEqip.brand__c',String.valueOf(helperEqip.brand__c));
        }
        if(helperMatSD != '')
        {
            params.put('helperMatSD',String.valueOf(helperMatSD));
        }
        if(helperMatNr != '')
        {
            params.put('helperMatNr',String.valueOf(helperMatNr));
        }
        if(helperEqip.ManufacturerSerialNo__c != null)
        {
            params.put('helperEqip.ManufacturerSerialNo__c',String.valueOf(helperEqip.ManufacturerSerialNo__c));
        }

        
        return JSON.serialize(params);
    }    
    
}
