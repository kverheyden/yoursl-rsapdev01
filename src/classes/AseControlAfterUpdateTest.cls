/*
 * @(#)AseControlAfterUpdateTest.cls SCCloud    ma 20.07.2011
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author MA
 * @version $Revision$, $Date$
 */
@isTest
private class AseControlAfterUpdateTest 
{
    /**
     * Class under test
     */
        
     static testMethod void testTrigger1()
    {
        SCAseControl__c aseControl = new SCAseControl__c();
        
      
        
        try 
        {
            insert aseControl;
            
            aseControl.Activity__c = 'U';
            update aseControl;
        }
        catch(Exception e) 
        {   
            System.assert(e instanceof System.DMLException);
        }
    } 
}
