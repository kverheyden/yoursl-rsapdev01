@isTest
private class DeveloperNameIdReferenceHandlerTest {
	static ArticleGroup__c ag;
	static Document d;

	static void setupDocument() {
		Folder f = [SELECT Id FROM Folder LIMIT 1];
		d = new Document(FolderId = f.Id, Name = 'TestDoc.pdf', DeveloperName = 'TestDoc', Body = Blob.valueOf('1234'));
		insert d;
	}

	static void setupArticleGroup() {
		ag = new ArticleGroup__c (
			Group__c = 'Fanta Erdbeere',
			KavRelevant__c = false,
			KindOfPackage__c = 'PET',
			MeasuringUnit__c = 'L',
			Name = 'Fanta Erdbeere 1L PET',
			NoAnnualOrderDiscount__c = false,
			NoCateringBottleDiscount__c = false,
			NoCentralBillingDiscount__c = false,
			NoSponsoringDiscount__c = false,
			Packaging_size__c = '1',
			Status__c = 'Active',
			SubGroup__c = 'Standard',
			PictureDocDevName__c = 'TestDoc'
		);

		insert ag;
	}

	@isTest 
	static void testDeveloperNameIdReferenceHandler() {
		setupDocument();
		setupArticleGroup();
		DeveloperNameIdReferenceHandler h = 
			new DeveloperNameIdReferenceHandler(ArticleGroup__c.SObjectType, ArticleGroup__c.PictureDocDevName__c, ArticleGroup__c.Picture__c);
		h.setIdTextByDeveloperName(new List<Account>());
		update ag;
		System.assertEquals(null, ag.Picture__c);
		h.setIdTextByDeveloperName(new List<ArticleGroup__c>{ag});
		update ag;
		System.assertEquals(d.Id, ag.Picture__c);
	}
	
	@isTest 
	static void testDeveloperNameIdReferenceHandlerWithoutDocuments() {
		setupArticleGroup();
		DeveloperNameIdReferenceHandler h = new DeveloperNameIdReferenceHandler(Account.SObjectType, Account.FirstName__c, Account.LastName__c);
		h.setIdTextByDeveloperName(new List<ArticleGroup__c>());
		update ag;
		System.assertEquals(null, ag.Picture__c);
	}
}
