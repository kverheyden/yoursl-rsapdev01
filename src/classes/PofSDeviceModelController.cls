/**********************************************************************
Name:  PofSDeviceModelController() 
======================================================
Purpose:                                                            
This class serves as the data management controller of PofS_DeviceModel for the PofS Editor view.                                              
======================================================
History                                                            
-------                                                            
Date  		AUTHOR				DETAIL 
01/14/2013	Jan Mensfeld
04/15/2014  Oliver Preuschl 	Update for Initial Content List Support
05/06/2014  Oliver Preuschl 	Removed support for Initial Content List
06/26/2014  Jan Mensfeld        Added Initial Content List to Device Model table
***********************************************************************/

public class PofSDeviceModelController {
		
	//Non-Static Variables-----------------------------------------------------------------------------	
	public Id GV_PofSId { get; set; }
	public Id GV_CurrentPofSDeviceModelId{ get; set; }
	private List< PofSDeviceModel__c > GL_PofSDeviceModels;
	private PofSDeviceModel__c GO_PofSDeviceLookup;
	
	//getter & setter methods------------------------------------------------------------------------	
	public List< PofSDeviceModel__c > getGL_PofSDeviceModels(){
		System.debug( 'Entering getGL_PofSDeviceModels: ' );

		if( ( GV_PofSId != null ) ){
			GL_PofSDeviceModels = [ SELECT Id, Name, 
						        		DeviceModel__r.Name, 
						        		DeviceModel__r.AvailableBrands__c, 
						        		DeviceModel__r.Brand__c,
                                   		InitialContentList__r.Name
						        	FROM PofSDeviceModel__c 
						        	WHERE PictureOfSuccess__c =: GV_PofSId ];
		}else{
			GL_PofSDeviceModels = new List<PofSDeviceModel__c>();
		}
		System.debug( 'PDM: ' + GL_PofSDeviceModels );
		System.debug( 'Exiting getGL_PofSDeviceModels: ' );
		return GL_PofSDeviceModels;
	}

	public void setGL_PofSDeviceModels( List< PofSDeviceModel__c > PL_PofsDeviceModel ){
		System.debug( 'Entering setGL_PofSDeviceModels: ' + PL_PofsDeviceModel );

		GL_PofSDeviceModels = PL_PofsDeviceModel;

		System.debug( 'Exiting setGL_PofSDeviceModels: ' );
	}
	
	public PofSDeviceModel__c getGO_PofSDeviceLookup(){
		System.debug( 'Entering getGO_PofSDeviceLookup: ' );

		if( GO_PofSDeviceLookup == null ){
			GO_PofSDeviceLookup = new PofSDeviceModel__c();
		}

		System.debug( 'Exiting getGO_PofSDeviceLookup: ' );
		return GO_PofSDeviceLookup;
	}

	public void setGO_PofSDeviceLookup( PofSDeviceModel__c PO_PofSDeviceLookup ){
		System.debug( 'Entering setGO_PofSDeviceLookup: ' + PO_PofSDeviceLookup );

		GO_PofSDeviceLookup = PO_PofSDeviceLookup;

		System.debug( 'Exiting setGO_PofSDeviceLookup: ' );
	}

	//Constructors-----------------------------------------------------------------------------

	/*******************************************************************
	 Purpose: 		Controller, nothing to do
	 ********************************************************************/	
    public PofSDeviceModelController() {
    	
    }

    //Action Methods---------------------------------------------------------------------------

	/*******************************************************************
	 Purpose: 		Create and save a new PofSDeviceModel for the current PofS and the chosen DeviceModel
	 ********************************************************************/	
	public void savePofSDeviceModel() {
		System.debug( 'Entering savePofSDeviceModel: ' );

		PofSDeviceModel__c LO_NewPofSDeviceModel = new PofSDeviceModel__c();
		LO_NewPofSDeviceModel.PictureOfSuccess__c = GV_PofSId;
		
		Integer LV_NumberOfExistingDeviceModels = [ SELECT COUNT() FROM PofSDeviceModel__c WHERE DeviceModel__c =  : getGO_PofSDeviceLookup().DeviceModel__c AND PictureOfSuccess__c = : LO_NewPofSDeviceModel.PictureOfSuccess__c ];
		if( LV_NumberOfExistingDeviceModels == 0 ){
			LO_NewPofSDeviceModel.DeviceModel__c = getGO_PofSDeviceLookup().DeviceModel__c;
            //LO_NewPofSDeviceModel.InitialContentList__c = getGO_PofSDeviceLookup().InitialContentList__c;
			insert LO_NewPofSDeviceModel;
		}
		getGO_PofSDeviceLookup().DeviceModel__c = null;
        //getGO_PofSDeviceLookup().InitialContentList__c = null;

		System.debug( 'Exiting savePofSDeviceModel: ' );
	}

	/*******************************************************************
	 Purpose: 		Delete the specified PofSDeviceModel
	 ********************************************************************/	
	public void deletePofSDeviceModel(){
		System.debug( 'Entering deletePofSDeviceModel: ' );

		try{
			PofSDeviceModel__c LO_DeletePofSDeviceModel = [ SELECT Id FROM PofSDeviceModel__c WHERE Id =: GV_CurrentPofSDeviceModelId ];
			delete( LO_DeletePofSDeviceModel );
		}catch( Exception LO_Exception ){

		}
		System.debug( 'Exiting deletePofSDeviceModel: ' );
	}	
}
