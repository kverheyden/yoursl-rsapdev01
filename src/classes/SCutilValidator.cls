/*
 * @(#)SCutilValidator.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
* Validation class
*
* @author Eugen Tiessen <etiessen@gms-online.de>
* @version $Revision$, $Date$
*/

public class SCutilValidator {

	
	/**
	 * Validate E-Mail with SF validator
	 * Consumes 2 DML operations
	 *
	 * @param email the E-Mail addres to check
	 * @return true if valid, false if not valid
	 */
	public static Boolean checkEMail(String email)
	{
		try
    	{
    		SCTool__c tool = new SCTool__c(EMail__c = email);
    		upsert tool;
    		system.debug('Email is valid: ' + tool.EMail__c);
    		delete tool;
    	}
    	catch(Exception e)
    	{
    		system.debug('Email not valid: ' + email);
    		return false;
    	}
    	return true;
	}
}
