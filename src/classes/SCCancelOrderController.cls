/*
 * @(#)SCCancelOrderController.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This controller gets ID and cancels the order.
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCCancelOrderController
{
    public String oid;
    public String orderStatusLabel { get; set; }
    public String orderTypeLabel { get; set; }
    public Boolean cancellable { get; set; }
    public String orderStatusId { get; set; }
    public String selcancelreason { get; set; }
    public String cancelInfo{ get; set; }
    private SelectOption noneOption = new SelectOption('', System.Label.SC_flt_None);
    public SCOrder__c order { get; set; }

    public Boolean showAppointments{get;set;}
    public List<SCAppointment__c> appoints{get;set;}
    public String baseUrl{get;set;}
        
    public User currentUser
    {
        get
        {
            if (currentUser == null)
            {
                currentUser = [SELECT Id, ERPWorkCenterPlant__c FROM User WHERE Id = :UserInfo.getUserId()].get(0);
            }
            return currentUser;
        }
        private set;
    }
    public Boolean testcase { get; set; }
    
    /**
     * Standart constructor
     */
    public SCCancelOrderController(ApexPages.StandardController controller) 
    {
    	
    	showAppointments = false;
    	appoints = new List<SCAppointment__c>();    
    	baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
    	
        testcase = false;
        
        // Gets the order ID
        oid = controller.getId();

        // Create a new instance of scorder_c
        order = new SCOrder__c();
        
        // Check if order can be cancelled
        if(oid != null && oid != '')
        {
            //ET: 09.09.2013
            order =  
            [
            	SELECT 
            		Status__c, 
            		Type__c, 
            		DepartmentCurrent__r.Name 
            	FROM 
            		SCOrder__c 
            	WHERE 
            		Id = :oid
            ];
            
            order.CancelReason__c = null;
            order.Info__c = null;
            orderStatusId = order.Status__c;
            //orderStatusId = [ Select Status__c From SCOrder__c Where Id = :oid ].Status__c;
            orderTypeLabel = order.Type__c;
            //orderTypeLabel = [ Select Type__c From SCOrder__c Where Id = :oid ].Type__c;

			orderStatusLabel = [ Select toLabel(Status__c) From SCOrder__c Where Id = :oid ].Status__c;
            
            System.debug('#### orderTypeLabel: ' + orderTypeLabel);
            /** 23.09.2014 GMSSW PMS 38805: "Auftrag stornieren" werksübergreifend freischalten
            if(	order.DepartmentCurrent__c != null && 
            	order.DepartmentCurrent__r.Name != currentUser.ERPWorkCenterPlant__c)
            {
            	ApexPages.Message msg; 
             	msg = new ApexPages.Message(ApexPages.Severity.WARNING, 'Der Auftrag gehört zu einer anderen Organisationseinheit (werksfremd).');
             	ApexPages.addMessage(msg);
             	cancellable = false;
            }
            
            else
            **/ 
            if( (orderStatusId == SCfwConstants.DOMVAL_ORDERSTATUS_OPEN || 
                orderStatusId == SCfwConstants.DOMVAL_ORDERSTATUS_PLANNED || 
                orderStatusId == SCfwConstants.DOMVAL_ORDERSTATUS_PLANNED_FOR_PRECHECK || 
                orderStatusId == SCfwConstants.DOMVAL_ORDERSTATUS_RELEASED_FOR_PROCESSING) &&
                orderTypeLabel != SCfwConstants.DOMVAL_ORDERTYPE_SALESORDER )
            {
                cancellable = true;
                
                // Added for CCE.
                // Orders with external assignemtns are not allowed to be cancelled.
                if(hasExternalAssignmelts())
            	{
            		ApexPages.Message msg; 
                 	msg = new ApexPages.Message(ApexPages.Severity.WARNING, 'Aufträge mit externer Auftragsvergabe dürfen nicht storniert werden.');
                 	ApexPages.addMessage(msg);
                 	cancellable = false;
            	}
            	
            	//gmssw check all the appopintment is closed
            	if(!isAllAppointmentsClosed())
            	{
            		ApexPages.Message msg; 
                 	msg = new ApexPages.Message(ApexPages.Severity.WARNING, System.Label.SC_msg_NotClosedAppointExists);
                 	ApexPages.addMessage(msg);
                 	cancellable = false;
                 	
                 	//'The following Appointment are not completed';
            	    showAppointments = true;            		
            	}
            	
            	
            }
            else
            {
                 ApexPages.Message msg; 
                 msg = new ApexPages.Message(ApexPages.Severity.WARNING, System.Label.SC_msg_OrderNotCancellable + '/Type');
                 ApexPages.addMessage(msg);
                 cancellable = false;
            }
            if(hasExternalAssignmelts())
            {
            	
            }
        }
    }
    /**
    * Orders with external assignemtes are not allowed to be cancelled. 
    **/
    private boolean hasExternalAssignmelts()
    {
    	List<SCOrderExternalAssignment__c> externalAssingments = [ Select Name, Order__c, Status__c
    							FROM SCOrderExternalAssignment__c
    							where Status__c	= 'assigned'
    							and ERPStatusRem__c != 'ok'
								and Order__c =: oid];
								
		if(externalAssingments.size() > 0)
		{
			// Orders with external assignemts are not allowed to be cancelled.
			return true; 
		}
		else
		{
			return false;
		}
    }
        
    /**
     * Close order cancellation
     *
     * @return    Redirect to the order page
     */
    public PageReference closeCancellation() 
    {             
        PageReference pageRef;
        pageRef = new PageReference('/' + oid);
        return pageRef.setRedirect(true);
    }
   
    
    /**
     * Cancel method
     *
     * @return    If all mandatory field are ok -> calls the cancel function and redirects to the order page
     */
    public PageReference cancelOrder() 
    {    
        System.debug('#### order.CancelReason__c: ' + order.CancelReason__c);
        System.debug('#### order.CancelReason__c: ' + order.CancelReason__c);
        System.debug('#### oid: ' + oid);
    
        if(order.CancelReason__c != null && order.CancelReason__c != '' && oid != '' && oid != null && order.Info__c != null && order.Info__c != '')
        {           
            // If Order is Open or Planned or Released for Processing
            System.debug('#### cancellable: ' + cancellable);
            if(cancellable != null && cancellable == true)
            {
                SCboOrder.cancelOrder(oid, order.CancelReason__c, order.Info__c);
                String oType = [select Type__C from SCOrder__c where id = :oid].type__c;
                
                // If order type = '5705' or '5711' we need to remove IBL reference from IBs
                System.debug('#### order.type__c: ' + order.type__c);
                if(oType != null && (oType == '5705' || oType == SCfwConstants.DOMVAL_ORDERTYPE_REPLACEMENT))
                {
                    // Reading order items
                    System.debug('#### Reading order items');
                    List<SCOrderItem__c> oItems = new List<SCOrderItem__c>();
                    
                    oItems = [
                    	SELECT 
                    		Id, 
                    		Name, 
                    		InstalledBase__c
                    	FROM 
                    		SCOrderItem__c
                        WHERE 
                        	Order__c = :oid 
                        	AND
                        	
                        	(
                        		Order__r.Type__c != :SCfwConstants.DOMVAL_ORDERTYPE_REPLACEMENT
                        		OR
                        		(
                        			Order__r.Type__c = :SCfwConstants.DOMVAL_ORDERTYPE_REPLACEMENT
                        			AND
                        			RecordType.DeveloperName = 'EquipmentNew'
                        		)
                        	)
					];
                    
        
                    // Now collecting ids of installed bases
                    List<Id> ibToUpdate = new List<Id>();
                    if(!oItems.isEmpty())
                    {
                        for (SCOrderItem__c item :oItems)
                        {
                            ibToUpdate.add(item.InstalledBase__c);
                        }
                    }
                    
                    // Reading installed bases
                    List<SCInstalledBase__c> ibs = [ select id, name, installedbaselocation__c, Parent__c 
                                                     from SCInstalledBase__c Where id IN :ibToUpdate ];
                                                     
                    System.debug('#### oItems: ' + oItems.size());
                    System.debug('#### ibToUpdate: ' + ibToUpdate.size());
                    System.debug('#### ibs: ' + ibs.size());
                                                     
                    if(!ibs.isEmpty())
                    {
                        // Removing IBL und parent reference from IB
                        for(SCInstalledBase__c ib : ibs)
                        {
                            ib.InstalledBaseLocation__c = null;
                            ib.Parent__c = null;
                            
                            //reset reservedutil field. equipment is now available for new orders
                            ib.ReservedUntil__c = null;
                        }
                        
                        try
                        {
                            update ibs;
                        }
                        catch(Exception e)
                        {
                            System.debug('#### Cannot update installed bases: ' + e.getMessage());
                        }
                    }
                }
                
                // Redirecting to cancelled order
                System.debug('#### Redirecting to cancelled order.');
                PageReference page = new PageReference('/' + oid);
                
                //###### CCE ###### -->
                if(!testcase)
                {
                    // Call the SAP web service 
                    CCWCOrderClose.cancel(oid, true, false);
                }
                //###### CCE ###### <--
                
                return page;
            }
        }
        return null;
    }
    
    public boolean isAllAppointmentsClosed()
    {    	
    	    //GMSSW PMS 38766 18.09.2014 get all appoinements in this order, if one of them not in status(5507,5508,5506)
    	    // then prevent user from close this order
        	appoints.clear();
        	appoints = [
        	             Select id,
        	                    Order__c,
        	             		Order__r.id,
        	             		Employee__c,
        	             		Start__c,
        	             		Name,
        	             		End__c,
        	             		Order__r.name, 
        	                    Class__c, 
        	                    Assignment__c, 
        	                    Assignment__r.id,
        	                    Assignment__r.status__c,
        	                    Assignment__r.Name
        	                    From SCAppointment__c
        	                    WHERE Order__r.id = : oid      	
        	                    AND Assignment__r.status__c not in :SCfwConstants.DOMVAL_ORDERASSIGNMENTSTATUS_FOR_ORDERCLOSE.split(',')
        	            ];
        	if(appoints.size()>0)
        	{
        		return false;
        	}
        	return true;
    }    
}
