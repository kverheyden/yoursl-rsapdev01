/**
* @author		Austen Buennemann
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				a.buennemann@yoursl.de
*
* @description	Handler for CCWCFaxService
*
* @date			16.09.2014
*
* Timeline:
* Name               DateTime                  Description
* Austen Buennemann  18.09.2014 12:20          Class created
*/

public with sharing class CCWCFaxServiceHandler {
	

	public void readAttachment(Attachment [] document){ 
		Set<Id> accIds = new Set<Id>();
		for(Attachment att : document){
			if(att != null && att.Name != null && att.Name == 'SalesOrderSummary'){
				accIds.add(att.ParentId);
			}
		}
		
		if(!accIds.isEmpty()){
			Map<Id, Account> accMap = new Map<Id, Account>([Select Id, Name, GFGHPreferedOrderFax__c, GFGHPreferredOrderTransactionMethod__c From Account Where Id =: accIds]);
						
			for(Attachment att : document){
				if(att != null && att.Name != null && att.ParentId != null 
					&& att.Name == 'SalesOrderSummary' 
					&& accMap.containsKey(att.ParentId) 
					&& accMap.get(att.ParentId).GFGHPreferredOrderTransactionMethod__c.toLowerCase() == 'fax'
					&& !String.isBlank(accMap.get(att.ParentId).GFGHPreferedOrderFax__c)
					&& att.Body != null){
						String doc = EncodingUtil.base64Encode(att.Body);
						String acc = JSON.serialize(accMap.get(att.ParentId));
						boolean testMode = false;
						CCWCFaxService.executecallout(acc, doc, testMode);
				}
			}	
		}	
	}
}
