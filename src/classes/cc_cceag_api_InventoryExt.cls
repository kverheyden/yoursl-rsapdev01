global with sharing class cc_cceag_api_InventoryExt extends ccrz.cc_api_InventoryExtension {
	public cc_cceag_api_InventoryExt() {
		
	}

	global override Map<Id,ccrz__E_ProductInventoryItem__c> getAvailabilityQtyMsg(List<Id> products) {
		Map<Id,ccrz__E_ProductInventoryItem__c> retData = new Map<Id,ccrz__E_ProductInventoryItem__c>();
		ccrz.cc_RemoteActionContext ctx = ccrz.cc_CallContext.remoteContext;
		if (ctx == null) 
			ctx = new ccrz.cc_RemoteActionContext();
		if (String.isBlank(ctx.portalUserId))
            ctx.portalUserId = UserInfo.getUserId();
        if (String.isBlank(ctx.storefront))
            ctx.storefront = ccrz.cc_util_Storefront.getStoreName();
		ccrz__E_Cart__c activeCart = cc_cceag_dao_Cart.retrieveActiveCart(ctx.portalUserId, ctx.storefront);
		if (activeCart != null) {
			String plantId = activeCart.ccrz__ShipTo__r.ccrz__MailStop__c;
			if (plantId != null) {
				List<ATPStockQuantity__c> stocks = [SELECT ccrz_product__c, ToleratedQuantity1__c, ToleratedQuantity2__c, ToleratedQuantity3__c, ToleratedQuantity4__c, ToleratedQuantity5__c, 
														DateDay1__c, DateDay2__c, DateDay3__c, DateDay4__c, DateDay5__c
													FROM 
														ATPStockQuantity__c 
													WHERE 
														SCPlant__r.Name =: plantId AND ccrz_product__c =: products ];
				for (ATPStockQuantity__c stock: stocks) {
					if (activeCart.ccrz__RequestDate__c == stock.DateDay1__c)
						retData.put(stock.ccrz_product__c, new ccrz__E_ProductInventoryItem__c(ccrz__QtyAvailable__c=stock.ToleratedQuantity1__c));
					else if (activeCart.ccrz__RequestDate__c == stock.DateDay2__c)
						retData.put(stock.ccrz_product__c, new ccrz__E_ProductInventoryItem__c(ccrz__QtyAvailable__c=stock.ToleratedQuantity2__c));
					else if (activeCart.ccrz__RequestDate__c == stock.DateDay3__c)
						retData.put(stock.ccrz_product__c, new ccrz__E_ProductInventoryItem__c(ccrz__QtyAvailable__c=stock.ToleratedQuantity3__c));
					else if (activeCart.ccrz__RequestDate__c == stock.DateDay4__c)
						retData.put(stock.ccrz_product__c, new ccrz__E_ProductInventoryItem__c(ccrz__QtyAvailable__c=stock.ToleratedQuantity4__c));
					else if (activeCart.ccrz__RequestDate__c == stock.DateDay5__c)
						retData.put(stock.ccrz_product__c, new ccrz__E_ProductInventoryItem__c(ccrz__QtyAvailable__c=stock.ToleratedQuantity5__c));
					else
						retData.put(stock.ccrz_product__c, new ccrz__E_ProductInventoryItem__c(ccrz__QtyAvailable__c=100000));
				}
			}
			else {
				for (Id prod: products)
					retData.put(prod, new ccrz__E_ProductInventoryItem__c(ccrz__QtyAvailable__c=1));
			}
		}
		else {
			for (Id prod: products)
				retData.put(prod, new ccrz__E_ProductInventoryItem__c(ccrz__QtyAvailable__c=1));
		}
		return retData;
	}
}
