/*
 * @(#)CCWSUtil.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * The class exports an Salesforce order into SAP
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
public without sharing class CCWSUtil 
{
    public CCSettings__c ccSettings = CCSettings__c.getInstance();
    public Map<String, String> orderTypeSF2SAPmap = new Map<String, String>();
    public Map<String, String> errSympt1SF2SAPmap = new Map<String, String>();
    public Map<String, String> errSympt2SF2SAPmap = new Map<String, String>();
    public Map<String, String> materialMovementSF2SAPmap = new Map<String, String>();
    public User user;
    public String workCenter;
        
    public CCWSUtil()
    {
        initOrderTypeSF2SAPMap();
        user = getUser();
    }

    public String getMessageID()
    {
        DateTime dt = DateTime.now();
        Long ms = dt.getTime();
        Double dRandom = Math.random();
        System.debug('###dRandom: ' + dRandom);
        DateTime now = DateTime.now();
        String timestamp = now.formatGmt('yyyyMMddHHmmssSSS');
        Integer nRandom = (Integer) ((double)dRandom * 1000000);
        String strRandom = rect6(nRandom);
        String ret = 'CP' + timestamp + '-' + strRandom;
        System.debug('###GetMessageID messageId: ' + ret);
        return ret;        
    }
    


    //--<Date and time formatting helper functions>----------------------------
    // ####
   /* used by classes (12.12.12):     
    * CCWCArchiveDocumentInsert.cls
    * CCWCMaterialInventoryCreate.cls
    * CCWCMaterialMovementCreate.cls
    * CCWCMaterialPackageReceived.cls
    * CCWCMaterialReservationCreate.cls
    * CCWCOrderClose.cls
    * CCWCOrderCreate.cls
    * CCWCOrderEquipmentUpdate.cls
    * CCWCOrderExternalOperationAdd.cls
    * CCWCOrderExternalOperationRem.cls
    * CCWCReprocessingRequest.cls
    * CCWSUtilTest.cls
    * SCSoapUIController.cls
    * SCSoapUIController2.cls
    */
   /*
    *  Returns the current timestamp in the web sevice UTC Format: 2012-10-23T13:49:22.1234567Z      
    * GLOBAL_DateTime  [0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}(.[0-9]{1,7})?Z "
    */
    public String getFormatedCreationDateTime()
    {
        return formatDateTime(DateTime.now());
    }

   /*
    * Returns a string in the web sevice UTC Format: 2012-10-23T13:49:22.1234567Z      
    * GLOBAL_DateTime  [0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}(.[0-9]{1,7})?Z
    * @param dt the date time to format or null
    * @return a formatted string in GMT
    */
    public String formatDateTime(DateTime dt)
    {
        if(dt != null)
        {                
            return dt.formatGmt('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
        }    
        return null;
    }


   /* used by classes (12.12.12):  
   * CCWCOrderClose.cls (3 matches)
   *
   */
   /*
    * Returns a string in the web sevice UTC Format: 2012-10-23      
    * GLOBAL_Date  [0-9]{4}-[0-9]{2}-[0-9]{2}
    * @param d the date to format or null
    * @return a formatted string in GMT
    */
    public String formatToDate(DateTime d)
    {
        if(d != null)
        {
            return d.formatGmt('yyyy-MM-dd');
        }    
        return null;
    }

    public String formatToTime(DateTime dt)
    {
        if(dt != null)
        {
            return dt.formatGmt('HH:mm:ss');
        }    
        return null;
    }

    public String formatToGmtTimeZ(DateTime dt)
    {
        if(dt != null)
        {
            return dt.formatGmt('HH:mm:ss') + 'Z';
        }    
        return null;
    }


    //### remove
   /* used by classes (12.12.12):   
    * CCWCOrderCreate.cls
    */
    public String formatDate(Date d)
    {
        return formatToDate(d);
    }
    
    //### remove
   /* used by classes (12.12.12):   
   * only CCWSUtilTest.cls
   */ 
    public String getFormatedGMTCreationDateTime()
    {
        return getFormatedCreationDateTime();
        /*
        DateTime now = DateTime.now();
        String retValue = now.formatGmt('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
        System.debug('###getGMTCreationDateTime: ' + retValue);
        return retValue;
        */
    }
 
 
    public String rect(String digit)
    {
        String retValue = digit;
        if(digit.length() == 1)
        {
            retValue = '0' + digit;
        }
        return retValue;
    }

    public String rect4(Integer i)
    {
        String retValue = '' + i;
        if(i < 10)
        {
            retValue = '000' + i;
        }
        else if (i < 100)
        {
            retValue = '00' + i;
        }
        else if(i < 1000)
        {
            retValue = '0' + i;
        }
        return retValue;
    }

    public String rect6(Integer i)
    {
        String retValue = '' + i;
        if(i < 10)
        {
            retValue = '00000' + i;
        }
        else if (i < 100)
        {
            retValue = '0000' + i;
        }
        else if(i < 1000)
        {
            retValue = '000' + i;
        }
        else if(i < 10000)
        {
            retValue = '00' + i;
        }
        else if(i < 100000)
        {
            retValue = '0' + i;
        }
        return retValue;
    }



    public String getSenderBusinessSystemID()
    {
         String retValue = ccSettings.SAPDispSenderSystemID__c;
         System.debug('###getSenderBusinessSystemID: ' + retValue);
         return retValue;
    }

    public String getRecipientBusinessSystemID()
    {
         String retValue = ccSettings.SAPDispRecipientSystemID__c;
         System.debug('###getRecipientBusinessSystemID : ' + retValue);
         return retValue;
    }

    // default values for username and password are hard coded
    public Map<String, String> getBasicAuth()
    {
        Map<String, String> retValue =  new Map<String, String>(); 

        String username = ccSettings.SAPDispBasicAuthUsername__c;
        if(username ==  null || username == '')
        {        
            throw new SCfwException('The user name for SAP Web Service in CCSetting.SAPDispBasicAuthUsername is missing.');
        }    
        String password = ccSettings.SAPDispBasicAuthPassword__c;
        if(password == null || password == '')
        {
            throw new SCfwException('The password for SAP Web Service in CCSetting.SAPDispBasicAuthPassword is missing.');
        }    

        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        retValue.put('Authorization', authorizationHeader);

        return retValue;
    }

   /*
    * Create the endpoint string for the web service base don the custom setting object CCSettings__c 
    * and the interface transaction. Use this endpoint string in the web services client implementation.
    * @param trans     transaction name (see code for details) 
    * @return concatenated string SAPDispServer__c and the transaction endpoint.
    */
    public String getEndpoint(String trans)
    {
        String retValue = null;
        String server = ccSettings.SAPDispServer__c;
        if(server == null || server == '')
        {
            throw new SCfwException('The server setting in CCSetting.SAPDispServer is missing.');
        }       
        
        String request; 
        if(trans == 'OrderCreate')
        {
            request = ccSettings.SAPDispEndpointOrderCreate__c;
        }
        else if(trans == 'OrderClose')
        {
            request = ccSettings.SAPDispEndpointOrderClose__c;
        }
        else if(trans == 'OrderEquipmentUpdate')
        {
            request = ccSettings.SAPDispEndpointOrderEquipmentUpdate__c;
        }
        else if(trans == 'MaterialMovementCreate')
        {
            request = ccSettings.SAPDispEndpointMaterialMovementCreate__c;
        }
        else if(trans == 'MaterialReservationCreate')
        {
            request = ccSettings.SAPDispEndpointMaterialReservationCreate__c;
        }
        else if(trans == 'OrderExternalOperationAdd')
        {
            request = ccSettings.SAPDispEndpointOrderExternalOperationAdd__c;
        }
        else if(trans == 'OrderExternalOperationRemove')
        {
            request = ccSettings.SAPDispEndpointOrderExternalOperationRem__c;
        }
        else if(trans == 'MaterialInventoryCreate')
        {
            request = ccSettings.SAPDispEndpointMaterialInventoryCreate__c;
        }
        else if(trans == 'ArchiveDocumentInsert')
        {
            request = ccSettings.SAPDispEndpointArchiveDocumentInsert__c;
        }
        else if(trans == 'MaterialPackageReceived')
        {
            request = ccSettings.SAPDispEndpointMaterialPackageReceived__c;
        }
        else if(trans == 'CCWCReprocessingRequest')
        {
            request = ccSettings.SAPDispReprocessingIDOC__c;
        }
        else if(trans == 'AssetInventoryDataRequest')
        {
            request = ccSettings.SAPDispEndpointAssetInventory__c;
        }
        
        //ECOM EDL Cloudcraze
        else if(trans == 'SalesOrderRequestResponse')
        {
            request = ccSettings.SAPDispEndpointSalesOrderRequestResponse__c;
        }
        
        //ECOM YOURSL Webservices
       	else if(trans == 'CustomerRegistration')
        {
            request = ccSettings.SAPDispEndpointCustomerRegistration__c;
        }         
                       
        //FSFA YOURSL Webservices
        else if(trans == 'NotizenOut')
        {
            request = ccSettings.SAPDispEndpointNotizenOut__c;
        }
        else if(trans == 'CustomerCreate')
        {
            request = ccSettings.SAPDispEndpointCustomerCreate__c;
        }
        else if(trans == 'SalesOrder')
        {
            request = ccSettings.SAPDispEndpointSalesOrder__c;
        } 
        else if(trans == 'FaxService')
        {
            request = ccSettings.SAPDispEndpointFaxService__c;
        } 
        
        if(request == null || request == '')
        {
            throw new SCfwException('The server setting in CCSetting.SAPDispServer is missing for ' + trans);
        }       
        retValue = server + request;
        return retValue;
    }

	public String getGenericResponseHookName()
	{
		return ccSettings.GenericResponseHookClassName__c;
	}
    /**
     *  Gets timeout 
     */
    public Integer getTimeOut()
    {
        Integer retValue = null;
        retValue = (Integer)ccSettings.SAPCalloutTimeOut__c;
        if(retValue == null)
        {
            retValue = 90000; // 90 seconds 90000 milliseconds  
        }
        return retValue;    
    }     
   /*
    * 
    * GMSGB added new Coke "Umbau" OrderTypes (internal ZC02-2 ZC02-8) (15.02.2014)
    * Coke will get only an 5701 for all ZC02 Tyes, even if ClockPort contains a more fine graind 
    * service type.
    */
    public void initOrderTypeSF2SAPMap()
    {
        orderTypeSF2SAPmap.put('5701', 'ZC02');
        orderTypeSF2SAPmap.put('5731', 'ZC02');           
        orderTypeSF2SAPmap.put('5732', 'ZC02');                
        orderTypeSF2SAPmap.put('5733', 'ZC02');                
        orderTypeSF2SAPmap.put('5734', 'ZC02');                
        orderTypeSF2SAPmap.put('5735', 'ZC02');                
        orderTypeSF2SAPmap.put('5736', 'ZC02');                
        orderTypeSF2SAPmap.put('5737', 'ZC02'); 
        orderTypeSF2SAPmap.put('5702', 'ZC15');
        orderTypeSF2SAPmap.put('5704', 'ZC09');
        orderTypeSF2SAPmap.put('5705', 'ZC04');
        orderTypeSF2SAPmap.put('5738', 'ZC04');
        orderTypeSF2SAPmap.put('5707', 'ZC01');
        orderTypeSF2SAPmap.put('5711', 'ZC16');
               
        
    }
 
    
    /*
     * Converts the Clockport order type into the SAP order type
     */
    public String getSAPOrderType(String sfOrderType)
    {
        String retValue = sfOrderType;
        if(sfOrderType != null && sfOrderType != '')
        {
            if(orderTypeSF2SAPmap.containsKey(sfOrderType))
            {
                retValue = orderTypeSF2SAPmap.get(sfOrderType);
            }
        }
        else
        {
            throw new SCfwException('The src order type is null or empty.');
        }    
        return retValue;
    }

    public void initMaterialMovementTypeSF2SAPMap()
    {
        // TODO:
        // MaterialMovement
        materialMovementSF2SAPmap.put('5204', 'Verbrauch');
        materialMovementSF2SAPmap.put('5215', 'Abgangsbuchung');
        materialMovementSF2SAPmap.put('5229', 'Konvertierung Zugang');
        // Material Reservation
        materialMovementSF2SAPmap.put('5222', 'Nachversorgung');
        materialMovementSF2SAPmap.put('5202', 'Materialanforderung');



    } 

    public String getSAPMaterialMovementType(String mtype)
    {
    
        if(mtype == '5202')  // Material Reservation 
        {
            return '311';    // Transfer reservation
        } 
        if(mtype == '5204')  // Consumption  
        {
            return '261';    // Goods Issue against Order (consumption)
        } 
        if(mtype == '5215')  // Transfer out
        {
            return '311';   // Transfer Posting
        }
        if(mtype == '5222')  // Replenishment
        {
            return '311';   // Transfer Posting
        }
        if(mtype == '5227')  // Replenishment
        {
            return '315';   // Transfer Posting
        }
        
        /* Status 14.11.2012
        SAP     CP
        -       DOMVAL_MATMOVETYP_MATREQUEST_CUST = '5201';    (reserved) Material Reservation - send to customer
        311     DOMVAL_MATMOVETYP_MATREQUEST_EMPL = '5202';    Material Reservation - send to engineer 
        261     DOMVAL_MATMOVETYP_CONSUMPTION = '5204';        Consumption
        -       DOMVAL_MATMOVETYP_DELIVERY = '5207';           Delivery  
        -       DOMVAL_MATMOVETYP_NEW = '5208';                Return - faulty new part
        -       DOMVAL_MATMOVETYP_SCRAP = '5209';              Return - scrap  
        -       DOMVAL_MATMOVETYP_TRANSFER_IN = '5214';        Transfer - in
        311     DOMVAL_MATMOVETYP_TRANSFER_OUT = '5215';       Transfer - out (must have the receiving stock)
        -       DOMVAL_MATMOVETYP_RETURN_OLD = '5216';         Return - used
        -       DOMVAL_MATMOVETYP_RETURN_NEW = '5217';         Return - new
        -       DOMVAL_MATMOVETYP_CONSUMPTION_UNDO = '5218';   (reserved)
        -       DOMVAL_MATMOVETYP_CORRECTION_IN = '5219';      (internal only)
        -       DOMVAL_MATMOVETYP_CORRECTION_OUT = '5220';     (internal only)
        311     DOMVAL_MATMOVETYP_REPLENISHMENT = '5222';      Material Replenishment (based on stock profiles)  
        -       DOMVAL_MATMOVETYP_INVENTORY_IN = '5225';       Inventory (absolut value) 
        -       DOMVAL_MATMOVETYP_INVENTORY_OUT = '5226';      (reserved)
        -       DOMVAL_MATMOVETYP_RESETSTOCKBALANCE = '5228';  (reserved) 
        -       DOMVAL_MATMOVETYP_CONVERSION_IN = '5229';      
        -       DOMVAL_MATMOVETYP_CONVERSION_OUT = '5230';
        315     DOMVAL_MATMOVETYP_RECEIPT        = '5227';      Delivery receipt confirmation   
        */
        return mtype;
    }
    
    public String getSAPUnitOfMeasure(String munit)
    {
    /*
        SELECT unit__c, count(id) FROM SCArticle__c group by unit__c order by count(id) desc
            1   EA  28066
            2   M   1028
            3   SET 87
            4   PAK 21
            5   KG  13
            6   ZDO 13
            7   KAN 12
            8   ROL 6
            9   L   5
            10  KAR 3
            11  ZCE 3
            12  UND 3
            13  BOT 1
            14  BAG 1
            15  ST  1
            16  201 1
            17  M2  1
            18  PAA 1

        */
        
        if(munit == '207' || munit == 'M')
        {
            return 'M';
        }
        if(munit == 'SET')
        {
            return 'SET';
        }
        if(munit == 'PAK')
        {
            return 'PAK';
        }
        if(munit == 'KG')
        {
            return 'KG';
        }
        if(munit == 'ZDO')
        {
            return 'ZDO';
        }
        if(munit == 'KAN')
        {
            return 'KAN';
        }
        if(munit == 'ROL')
        {
            return 'ROL';
        }
        if(munit == 'L')
        {
            return 'L';
        }
        if(munit == 'KAR')
        {
            return 'KAR';
        }
        if(munit == 'ZCE')
        {
            return 'ZCE';
        }
        if(munit == 'UND')
        {
            return 'UND';
        }
        if(munit == 'BOT')
        {
            return 'BOT';
        }
        if(munit == 'BAG')
        {
            return 'BAG';
        }
        if(munit == 'ST')
        {
            return 'ST';
        }
        if(munit == 'M2')
        {
            return 'M2';
        }
        if(munit == 'PAA')
        {
            return 'PAA';
        }
        return 'EA';
    }    


    public User getUser()
    {
        User retValue = null;
        ID userId = UserInfo.getUserId();
        if(userId != null)
        {
            List<User> ul = [select Name, ERPPlannerGroup__c, ERPWorkCenter__c, ERPPurchaseGroup__c, 
            	ERPPurchaseOrg__c, DefaultBusinessUnit__c, ERPWorkCenterPlant__c  
            	from User where id = : userId];
            if(ul.size() > 0)
            {
                retValue = ul[0];
            }
        }    
        return retValue;
    }  
    
    public String getBusinessUnit()
    {
        String retValue = null;
        if(user != null)
        {
            retValue = user.DefaultBusinessUnit__c;
        }
        return retValue;
    }  
    
    public String getWorkCenter()
    {
        String retValue = null;
        if(user != null)
        {
            retValue = user.ERPWorkCenter__c;
        }
        return retValue;
    }

    public String getPlannerGroup(String value)
    {
        if(value != null)
        {
            return value;
        }
        if(user != null && user.ERPPlannerGroup__c != null)
        {
            return user.ERPPlannerGroup__c;
        }
        return '';
    }

    
    public String getPurchaseGroup()
    {
        String retValue = null;
        if(user != null)
        {
            retValue = user.ERPPurchaseGroup__c;
        }
        return retValue;
    }

    public String getPurchaseOrg()
    {
        String retValue = null;
        if(user != null)
        {
            retValue = user.ERPPurchaseOrg__c;
        }
        return retValue;
    }


    //#### check if this can replaced by String.Split !!!
    // used by :
    // CCWCOrderCreate.cls (2 matches)
    public String [] getArray(Integer itemLength, String toSplit)
    {
        if(itemLength == 0)
        {
            throw new SCfwException('Divident = 0');
        }
        String [] retValue = null;
        if(toSplit != null && toSplit != '')
        {
            Integer inLength = toSplit.length();
            debug('inLength: ' + inLength);
            Integer dimension = inLength/itemLength;
            debug('dimension for update: ' + dimension);
            Integer rest = Math.mod(inLength, itemLength);
            debug('rest: ' + rest);
            if(rest > 0)
            {
                dimension++;
            }
            debug('dimension after update: ' + dimension);
            retValue = new String[dimension];
            for(Integer i  = 0; i < dimension - 1; i++)
            {
                String loopPart = toSplit.substring(i * itemLength, (i + 1) * itemLength);
                debug('loop Part: ' + loopPart);
                retValue[i] = loopPart;
            }
            String restPart = toSplit.substring((dimension-1) * itemLength);
            debug('restPart: ' + restPart);
            retValue[dimension -1] = restPart;
        }
        return retValue;
    }
    
    public String mapOrderStatusToSAP(String orderStatus)
    {
        // TODO: define
        String retValue = orderStatus  + ' please define the mapping';
        return retValue;
    }
    
    public Boolean isDeleted(String orderStauts)
    {
        Boolean retValue = false;
        if(orderStauts == '5507')
        {
            retValue = true;
        }
        return retValue;
    }

    public String mapActivityType(String activityType, Boolean distance)
    {
        // TODO: define
        String retValue = activityType;
        if( activityType == '7503') 
        {
            //  Arbeitszeit 
            retValue = '100700';
        }
        else if(activityType == '7505' && !distance)
        {
            // Fahrtzeit    
            retValue = '100701';
        }
        else if(activityType == '7533')
        {
            //  QA Test 
            retValue = '100702';
        }
        else if(activityType == '7525')
        {
            //  Schulung Kunde  
            retValue = '100703';
        }
        else if(activityType == '7534')
        {
            // Equipment Audit  
            retValue = '100704';
        }
        else if(activityType == '7527')
        {
            //  Inventur    
            retValue = '100705';
        }
        else if(activityType == '7505' && distance)
        {
            // Fahrtzeit mailage
            retValue = '100706';
        }
        else if(activityType == '7535')
        {
            //  Sanitation  
            retValue = '100707';
        }   
        return retValue;
    }


    /**
    * This method translates the OrderType piclist to SCOrder__C.Type__c
    *
    *
    * The mapping table is as follows:
    * ZC02 --> 5701
    * ZC04 --> 5705
    * ZC04-2 --> 5738
    * ZC09 --> 5704
    * ZC01 --> 5707
    * ZC16 --> 5711
    * ZC15 --> 5702
    * GMSGB added new Coke OrderTypes ZC02-2 ZC02-8 (15.02.2014)
    * ZC02-2 --> 5731            
    * ZC02-3 --> 5732                 
    * ZC02-4 --> 5733                 
    * ZC02-5 --> 5734                 
    * ZC02-6 --> 5735                 
    * ZC02-7 --> 5736                 
    * ZC02-8 --> 5737                 
    *
    */
    public static String translateSAPOrderTypeToClockPort(String cceType)
    {
        String clockportType = null;
        if(cceType == 'ZC02')
        {
            clockportType = '5701';
        }
        else if(cceType == 'ZC04')
        {
            clockportType = '5705';
        }
        /*else if(cceType == 'ZC04-2')
        {
            clockportType = '5738';
        }*/
        else if(cceType == 'ZC09')
        {
            clockportType = '5704';
        }
        else if(cceType == 'ZC01')
        {
            clockportType = '5707';
        }
        else if(cceType == 'ZC16')
        {
            clockportType = '5711';
        }
        else if(cceType == 'ZC15')
        {
            clockportType = '5702';
        }
        else if(cceType == 'ZC02-2')
        {
            clockportType = '5731';
        }
        else if(cceType == 'ZC02-3')
        {
            clockportType = '5732';
        }
        else if(cceType == 'ZC02-4')
        {
            clockportType = '5733';
        }
        else if(cceType == 'ZC02-5')
        {
            clockportType = '5734';
        }
        else if(cceType == 'ZC02-6')
        {
            clockportType = '5735';
        }
        else if(cceType == 'ZC02-7')
        {
            clockportType = '5736';
        }
        else if(cceType == 'ZC02-8')
        {
            clockportType = '5737';
        }      
        else
        {
            debug('OrderType is not set!');         
        }
        if(clockportType == null)
        {
            String msg = 'There is no ClockPort Order Type for SAP Order Type:' + cceType;
            debug(msg);
        }       
        return clockportType;
    } 
    
    public Date toDate(DateTime dt)
    {
        return dt.date();
    }
 
    public String getSapArticleUnit(String articleUnit)
    {
        String retValue = articleUnit;
        return retValue;
    }
 
 
    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }
 
    public static boolean isValid(String value) 
    {
        if(value != null)
        {
            return value.trim().length() > 0;
        }
        return false;
    }
    
    
   /*
    * Selects the required plants based on the SCPlant__c.name field. If required missing plants are created.
    * @param itemnames the plant names (e.g. 1234)
    * @param ifname optional context name used for filling the info field
    * @return a map of the required items
    */
    public static Map<String,SCPlant__c> selectOrCreatePlants(List<String> itemnames, String ifname)
    {
        // fill the map - the key is the name field
        Map<String, SCPlant__c> mapItems = new Map<String, SCPlant__c>();
        if(itemnames.size() > 0)
        {
            // read all existing items based on the name field
            List<SCPlant__c> items = [SELECT ID, Name FROM SCPlant__c WHERE Name in: itemnames];
            
            for(SCPlant__c i : items)
            {
                mapItems.put(i.Name, i);
            }
    
            // create missing plants        
            List<SCPlant__c> items2create = new List<SCPlant__c>();
            for(String key : itemnames)
            {
                if(CCWSUtil.isValid(key) && !mapItems.containsKey(key))
                {
                    SCPlant__c item = new SCPlant__c();
                    item.Name    = key;
                    item.ID2__c  = key; 
                    item.Info__c = '[' + key + '] created by interface ' + ifname != null ? ifname : '';
                    items2create.add(item);
                    mapItems.put(item.Name, item);
                }
            } 
            if(items2create.size() > 0)
            {
                insert(items2create);
            }
        }
        return mapItems;
    }
    
   /*
    * Selects the required stocks based on the SCStock__c.util_stockselector__c field. 
    * If required missing stocks are created automatically.
    * @param itemnames the stock names (e.g. 1234/5678) in the format <plant>-<stock>
    * @param ifname optional context name used for filling the info field
    * @return a map of the required items
    */
    public static Map<String,SCStock__c> selectOrCreateStocks(List<String> itemNames,  Map<String,SCPlant__c> plants, String ifname)
    {
        Map<String,SCStock__c> mapItems = new Map<String,SCStock__c>();
       
        List<SCStock__c> stocks = [SELECT ID, Name, util_stockselector__c FROM SCStock__c WHERE util_stockselector__c in :itemNames];
        
        for(SCStock__c st : stocks)
        {
            mapItems.put(st.util_stockselector__c, st);
        }
        
        // create missing stocks
        List<SCStock__c> items2create = new List<SCStock__c>();
        for(String key : itemNames)
        {
            // the key is a string with <plant>-<stock>
            if(CCWSUtil.IsValid(key) && !mapItems.containsKey(key))
            {
                // extract the stock / plant from the string plant-stock (only correct if plant or stock do not contain '-')
                integer pos = key.indexOf('-');
                if(pos > 0)
                {
                    String stockName = key.substring(pos + 1); 
                    String plantName = key.substring(0, pos);
                    if(stockName != null && stockName != '')
                    {    
                        SCStock__c item = new SCStock__c();
                        item.Name     = stockName;
                        item.ID2__c   = key; // <plant>-<stock>
                        if(plants.get(plantName) != null)
                        {
                            item.Plant__c = plants.get(plantName).id;
                            item.ValuationType__c = true;
                            item.Info__c = '[' + key + '] created by interface ' + ifname != null ? ifname : '';
                            items2create.add(item);
                            mapItems.put(key, item);
                        }
                    }    
                }
             }
        }
        if(items2create.size() > 0)
        {
            insert(items2create);
        }
        return mapItems;
    }
   
    public static Map<String, SCPlant__c> selectOrCreatePlant(String plant, String ifname)
    {
        List<String> itemNames = new List<String>();
        itemNames.add(plant);
        return selectOrCreatePlants(itemNames, ifname);
    }

    public static SCStock__c selectOrCreateStock(String plant, String stock, String ifname)
    {
        Map<String, SCPlant__c> mapPlants = selectOrCreatePlant(plant, ifname);
        
        List<String> itemNames = new List<String>();
        String stockselector = getStockSelector(plant, stock);
        itemNames.add(stockselector);
        Map<String, SCStock__c> mapStocks = selectOrCreateStocks(itemNames, mapPlants, ifname);
        return mapStocks.get(stockselector);
    }
    
    public static String getStockSelector(String plant, String stock)
    {
        return plant + '-' + stock;
    }
    
    
   
   /*
    * Selects the required pricelists based on the SCPriceList__c.ID2__c and country fields. 
    * If required missing items are created automatically.
    * @param itemnames the pricelist names (e.g. 'DE124')
    * @param ifname optional context name used for filling the info field
    * @return a map of the required items
    */ 
    public static Map<String,SCPriceList__c> selectOrCreatePriceLists(List<String> itemNames, String defaultCountry, String ifname)
    {
        Map<String,SCPriceList__c>  mapItems = new Map<String,SCPriceList__c>();
        if(itemNames.size() > 0)
        {
            List<SCPriceList__c> pricelists = [select id, Name, Info__c, Country__c from SCPriceList__c where ID2__c in: itemNames and country__c = :defaultCountry];
            for(SCPriceList__c pli : pricelists)
            {
                mapItems.put(pli.Name,pli);
            }
            
            // create missing records
            List<SCPriceList__c> items2create = new List<SCPriceList__c>();
            for(String key : itemNames)
            {
                if(CCWSUtil.IsValid(key) && !mapItems.containsKey(key))
                {
                    // create price list
                    SCPriceList__c item =  new SCPriceList__c(); 
                    item.Name       = key;
                    item.ID2__c     = key; 
                    item.Info__c    = '[' + key + '] created by interface ' + ifname != null ? ifname : '';
                    item.Country__c = defaultCountry;
                    
                    items2create.add(item);
                    mapItems.put(item.Name,item);
                }
            } 
            if(items2create.size() > 0)
            {
                insert(items2create);
            }
        }
        return mapItems;      
    }
    
   /* DMML 2x
    * Selects the required product models based on the SCProductModel.Name field.
    * If required missing items are created automatically.
    * @param itemnames the product model numbers names
    * @param ifname optional context name used for filling the info field
    * @return a map of the required items
    */ 
    public static Map<String, SCProductModel__c> selectOrCreateProductModel(List<String> itemNames, String ifname)
    {
        Map<String, SCProductModel__c> mapItems = new Map<String, SCProductModel__c>();   
        if(itemNames.size() > 0)
        {
            List<SCProductModel__c> items = [SELECT ID, Name, UnitClass__c, UnitType__c, Group__c, Power__c, ProductSkill__c
                                              FROM SCProductModel__c where Name in :itemNames];
            for(SCProductModel__c item : items)
            {
                mapItems.put(item.Name, item);
            }
            
            // create missing products        
            List<SCProductModel__c> items2create = new List<SCProductModel__c>();
            for(String key: itemNames)
            {
                if(CCWSUtil.isValid(key) && !mapItems.containsKey(key))
                {
                    SCProductModel__c item = CCWSUtil.createProductModel(key);
                    item.Text_en__c = '[' + key + '] created by interface ' + (ifname != null ? ifname : '');
                    item.Text_de__c = '[' + key + '] angelegt von Schnittstelle ' + (ifname != null ? ifname : '');
                    items2create.add(item);
                    mapItems.put(key, item);
                }
            } 
            if(items2create.size() > 0)
            {
                insert(items2create);
            }
        }                      
        return mapItems; 
    }    
    
   /*
    * Helper function - fills standard fields into the product model object. 
    * Can be used to create the product model later by inserting the object.
    */
    public static SCProductModel__c createProductModel(String no)
    {
      SCProductModel__c item = new SCProductModel__c();
      
      item.Name = no;
      item.ID2__c = no;
      item.Group__c = no;     
      
      item.Text_de__c = '(sapif)'; 
      item.Text_en__c = item.Text_de__c; // ### todo - check if required
      item.UnitClass__c = SCfwConstants.DOMVAL_PRODUCTUNITCLASS_STANDARD; 
      item.UnitType__c = SCfwConstants.DOMVAL_PRODUCTUNITTYPE_DEFAULT;

      // all other fields are not deliverey by the interface 
        
      return item;
    }    
    
    
   /* DMML 2x
    * Selects the required articles based on the SCArticle.Name field.
    * If required missing items are created automatically.
    * @param itemnames the article names
    * @param productMap optional product map (if set the article is created with the class/type "product")
    *                   of null the article is created with the class/type "article"
    * @param ifname optional context name used for filling the info field
    * @return a map of the required items
    */ 
    public static Map<String,SCArticle__c> selectOrCreateArticles(List<String> itemNames, Map<String, SCProductModel__c> productMap, String ifname)
    {
        Map<String,SCArticle__c> mapItems = new Map<String,SCArticle__c>();   
        if(itemNames.size() > 0)
        {
            List<SCArticle__c> items = [SELECT ID, Name, EANCode__c, Height__c, Width__c, Weight__c FROM SCArticle__c where Name in: itemNames];
            for(SCArticle__c item : items)
            {
                mapItems.put(item.Name, item);
            }
                    
            // create missing articles        
            List<SCArticle__c> items2create = new List<SCArticle__c>();
            for(String key : itemNames)
            {
                if(CCWSUtil.isValid(key) && !mapItems.containsKey(key))
                {
                    // create article
                    SCArticle__c item =  CCWSUtil.createArticle(key);
                    if(productMap != null)
                    {
                        SCProductModel__c prod = productMap.get(key);
                        if(prod != null)
                        {
                            item.ProductModel__c = prod.id;
                            item.Class__c = '14801';    // products
                            item.Type__c  = '30001';    // products
                        }
                    }
                    items2create.add(item);
                    mapItems.put(item.Name, item);
                }
            } 
            if(items2create.size() > 0)
            {
                insert(items2create);
            }
        }
        return mapItems; 
    }
    
   /*
    * Helper function - fills standard fields into the article object. 
    * Can be used to create the product model later by inserting the object.
    */
    public static SCArticle__c createArticle(String articleno)
    {
      SCArticle__c art = new SCArticle__c();
      art.Name   = articleno;
      art.ID2__c = articleno;
      art.ERPRelevant__c = true;
      art.Orderable__c = true;
      art.Stockable__c = true;
      art.External__c = false;
      art.ValuationType__c = true;
      art.Unit__c = '201';                         // ST
      art.AvailabilityType__c = '50401';           // 'Item immediately available';
      art.Class__c = SCfwConstants.DOMVAL_ARTICLECLASS_SPAREPART; 
      art.Type__c = SCfwConstants.DOMVAL_ARTICLETYP_NO_TYP; // sparepart
      art.LockType__c = '50201';                   // active
      art.TaxType__c = '50601';                    // full tax
      
      art.Text_de__c = '(' + articleno + ' sapif) de';   
      art.Text_en__c = '(' + articleno + ' sapif) en';
      
      return art;
    }

    /**
     *  Gets ExternalOperationMaterialGroup.
     *  Used by outgoing web service CCWCOrderExternalOperationAdd 
     *  to set the Material Group in an external operation element. 
     *  It is set if there is no Vendor Contract.
     */
    public String getExternalOperationMaterialGroup()
    {
        return ccSettings.ExternalOperationMaterialGroup__c;        
    }   

    
}
