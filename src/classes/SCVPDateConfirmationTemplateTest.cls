/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SCVPDateConfirmationTemplateTest {
/*
    static testMethod void SCVPDateConfirmationTemplateControllerTest() 
    {
    	SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet(true);
    	
    	// Vendor
        SCVendor__c vendor = new SCVEndor__c(Name 			= '0123456789',
        							     	 Name1__c 		= 'My test vendor',
        							   		 Country__c 	= 'DE',
        							   		 PostalCode__c 	= '33100',
        							   		 City__c		= 'Paderborn',
        							   		 Street__c		= 'Bahnhofstrt',
        							   		 LockType__c	= '5001',
        							   		 Status__c		= 'Active');
		insert vendor;
		

    	
    	
    	SCOrderExternalAssignment__c extAssignment = new SCOrderExternalAssignment__c(Order__c  		= SCHelperTestClass.order.id,
        																		  	  Vendor__c 		= vendor.id,
        																			  Status__c 		= 'assigned',
        																			  WishedDate__c 	= Date.today(),
        																			  PlannedDate__c	= Date.today());
		insert extAssignment;
    	
    	
    	
    	
    	
    	SCVPDateConfirmationTemplateController handler = new SCVPDateConfirmationTemplateController();
 		
 		handler.g_exAssigmentID = extAssignment.id;
 		
 		handler.init();
 		handler.getEquips();
    
    	 
    }
    */
}
