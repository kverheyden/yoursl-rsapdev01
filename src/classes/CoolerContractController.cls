/*
 * Author Duc Nguyen Tien (ngdawid@gmail.com/ d.nguyentien@yoursl.de)
 * 
 * Components:
 * CoolerContractController.cls
 * CoolerContractControllerTest.cls
 * srLeihvertrag_Cooler_Muster_hdm_090714.vfp
 * CDEOrder__c.Button.Generate_PDF_Contract
 *
 * Manual Task
 * - Add the Generate_PDF_Contract button to CDEOrder__c object layouts
 * 
 */
/*
{!requireScript("/soap/ajax/20.0/connection.js")} 
{!requireScript("/soap/ajax/20.0/apex.js")} 

var retStr; 
retStr = sforce.apex.execute("RequestContractController", "generateAndSaveContract", {orderID:'{! CdeOrder__c.Id}'}); 

alert('The method returned: ' + retStr); 

document.location = '/{!Account.Id}';
*/

global with sharing class CoolerContractController{
    public static final String ORDER_PARAMETER_LABEL = 'id';
    public static final String ATTACHMENT_PARAMETER_LABEL = 'attachmentID';
    
    public String AttachmentId { get; private set; }
    public CdeOrder__c CdeOrder { get; private set; }
    public String CurrentDate { get; private set; }
    public Map<String, SalesCenter> salesCenterMapping {get;set;}
    public User cdeOrderOwner {get;set;}
    public Boolean signatureExists {get;set;}
    
    public CoolerContractController(ApexPages.StandardController stdCont) {   
        CurrentDate = System.now().format('dd.MM.yyyy');
        
        CdeOrder = (CdeOrder__c) stdCont.getRecord();
        attachmentID = ApexPages.currentPage().getParameters().get(ATTACHMENT_PARAMETER_LABEL);
        system.debug('CdeOrder '+CdeOrder);
        salesCenterMapping = new Map<String, SalesCenter>();
        salesCenterMapping.put('0358',new SalesCenter('0358', 'Verkaufsgebiet Nord','Hemelinger Bahnhofstr. 20-24', '28309','Bremen'));
        salesCenterMapping.put('0359',new SalesCenter('0359', 'Verkaufsgebiet Ost','Alte Nauener Chaussee 4', '14621','Schönwalde-Glien'));
        salesCenterMapping.put('0360',new SalesCenter('0360', 'Verkaufsgebiet Nordwest','Paschenbergstr. 30', '45699','Herten'));
        salesCenterMapping.put('0363',new SalesCenter('0363', 'Verkaufsgebiet Südwest','Sindlinger Weg 1', '65835','Liederbach'));
        salesCenterMapping.put('0364',new SalesCenter('0364', 'Verkaufsgebiet Rheinland','Mathias-Brüggen-Str. 74', '50827','Köln'));
        salesCenterMapping.put('0366',new SalesCenter('0366', 'Verkaufsgebiet Baden-Württemberg','Im Kleinen Bruch 11', '76149','Karlsruhe'));
        salesCenterMapping.put('0367',new SalesCenter('0367', 'Verkaufsgebiet Bayern','Industriestr. 8', '82256','Fürstenfeldbruck'));
        salesCenterMapping.put(null,new SalesCenter('', '','', '',''));
        salesCenterMapping.put('',new SalesCenter('', '','', '',''));
        
         
        List<User> users = [Select FirstName, Lastname, SalesOffice__c from User where ID =: CdeOrder.OwnerID];
        if(!users.isEmpty()){
            cdeOrderOwner = users.get(0);
        }
        system.debug(cdeOrderOwner);
        if(cdeOrderOwner != null){
            if(!salesCenterMapping.containsKey(cdeOrderOwner.SalesOffice__c))
               salesCenterMapping.put(cdeOrderOwner.SalesOffice__c ,new SalesCenter(cdeOrderOwner .SalesOffice__c , '','', '',''));
        }else{
            system.debug('owner null');
            cdeOrderOwner = new User();
        }
        if(attachmentID == null || attachmentID == ''){
        	List<Attachment> atts = [Select ID, Name from Attachment where ParentID = :CdeOrder.ID and Name like '%Signature%CoolerContract%' order by CreatedDate desc];
        	if(!atts.isEmpty()){
            	signatureExists = true;
            	attachmentID = atts.get(0).Id;
        	}else{
            	signatureExists = false;
        	}
        }else{
            signatureExists = true;
        }
    }
    
    public class SalesCenter{
        public String code {get;set;}
        public String name {get;set;}
        public String street {get;set;}
        public String postalCode {get;set;}
        public String city {get;set;}
        public SalesCenter(String cd, String n, String s, String p, String c){
            code = cd;
            name = n;
            street = s;
            postalCode = p;
            city = c;
        }
    }
    
}
