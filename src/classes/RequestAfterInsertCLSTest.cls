/**
* @author		Andre Mergen
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
*
* @description	UnitTest of RequestAfterInsertCLS
*
* @date			01.07.2014
*
* Timeline:
* Name               DateTime                 Description
* Andre Mergen       01.07.2014 09:00         Class created
* Austen Buennemann  03.09.2014 09:59         ckeck and comments
*/

@isTest
private class RequestAfterInsertCLSTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
		String recTypte_create; 
		String recTypte_update;
		recTypte_create = [Select Id From RecordType Where SobjectType = 'Request__c' AND DeveloperName = 'NewCustomerRegistration' LIMIT 1].Id; 
		recTypte_update = [Select Id From RecordType Where SobjectType = 'Request__c' AND DeveloperName = 'CustomerMasterDataUpdate' LIMIT 1].Id;
		String recTypeDefault = [Select Id From RecordType Where SobjectType = 'Request__c' LIMIT 1].Id;
		                                    
        SalesAppSettings__c settingSalesApp = new SalesAppSettings__c();
        settingSalesApp.Name = 'RequestMasterDataUpdateRecordTypeId';
        settingSalesApp.Value__c = recTypte_update;
        insert settingSalesApp;                
        
        system.debug('Request');
        // create Request
        Request__c tmpReq = new Request__c();
		tmpReq.RecordTypeId = String.isEmpty(recTypte_create) ? recTypeDefault : recTypte_create;
        insert tmpReq;
        
        system.debug('Account');
        // create Account
        Account tmpAcc = new Account(); 
        string tmpReqIdStr = tmpReq.Id;
        tmpAcc.Name	= 'Test Account';
        tmpAcc.BillingCountry__c = 'DE';
        tmpAcc.RequestID__c = tmpReqIdStr.left(15);
        insert tmpAcc;
        
        system.debug('Request Change');
        // create Request
        Request__c tmpReq2 = new Request__c();
        tmpReq2.RelatedAccount__c = tmpAcc.Id;
        tmpReq2.ChangedFields__c = 'MainBrandCola__c';
        tmpReq2.MainBrandCola__c = 'COCA-COLA';
        tmpReq2.DeletedFieldsCSV__c = 'MainBrandBeer__c';
        tmpReq2.MainBrandBeer__c = '';
		tmpReq2.RecordTypeId = String.isEmpty(recTypte_update) ? recTypeDefault : recTypte_update;
        insert tmpReq2;
    }
}
