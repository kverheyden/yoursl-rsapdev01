/**
 * @(#)SCboMaterialMovementTest
 * Copyright 2012 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

 /**
 * @author Sebastian Schrage
 * @version $Revision$, $Date$
 */
@isTest
private class SCboMaterialMovementTest
{
    static testMethod void CodeCoverageA() 
    {
        SCHelperTestClass3.createCustomSettings('DE',true);
        SCHelperTestClass.createDomsForOrderCreation();
        
        SCArticle__c article = new SCArticle__c();
        Database.insert(article);
        
        SCPlant__c plant = new SCPlant__c();
        Database.insert(plant);
        
        SCStock__c stock = new SCStock__c(Plant__c = plant.Id);
        Database.insert(stock);
        
        
        Test.startTest();
        
        
        SCboMaterialMovement boMatMove = new SCboMaterialMovement(new SCfwDomain('DOM_MATERIALMOVEMENT_TYPE'));
        boMatMove.createMatMove(stock.Id, article.Id, 'New', 1, SCfwConstants.DOMVAL_MATMOVETYP_CONSUMPTION, SCfwConstants.DOMVAL_MATSTAT_NOMATERIAL, Date.Today(), stock.Id, null, null, false);
        boMatMove.insertMatMoves();
        
        boMatMove.matMovesForInsert = new List<SCMaterialMovement__c>{new SCMaterialMovement__c()};
        boMatMove.insertMatMoves();
        
        boMatMove.bookMaterialWithAllowNegativ(stock.Id, null, true);
        boMatMove.bookMaterialWithAllowNegativ(stock.Id, null, false);
        
        Test.stopTest();
        
        
    }
}
