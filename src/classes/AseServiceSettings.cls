/**
 * AseServiceSettings.cls   mt  28.09.2009
 * 
 * Copyright (c) 2009 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 /**
 * AseServiceSettings includes some settings which 
 * can set at runtime  
 * 
 * @author mt
 * @version $Revision$, $Date$
 */
 public class AseServiceSettings 
{
    /* flag if we are in testing mode. */
    public static boolean isTest = false;
    
}
