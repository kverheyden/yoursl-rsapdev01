/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class piCceagDeSfdc_SalesOrderTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        piCceagDeSfdc_SalesOrder testSO = new piCceagDeSfdc_SalesOrder();
        piCceagDeSfdc_SalesOrder.ORDER_CFGS_PART_OF_element testOrderCFGS	= new piCceagDeSfdc_SalesOrder.ORDER_CFGS_PART_OF_element();
        piCceagDeSfdc_SalesOrder.ORDER_ITEMS_IN_element ORDER_ITEMS_IN_element = new piCceagDeSfdc_SalesOrder.ORDER_ITEMS_IN_element();
        piCceagDeSfdc_SalesOrder.ORDER_CCARD_EX_element ORDER_CCARD_EX_element = new piCceagDeSfdc_SalesOrder.ORDER_CCARD_EX_element();
        piCceagDeSfdc_SalesOrder.ORDER_CONDITIONS_IN_element ORDER_CONDITIONS_IN_element= new piCceagDeSfdc_SalesOrder.ORDER_CONDITIONS_IN_element();
        piCceagDeSfdc_SalesOrder.BAPIPARTNR BAPIPARTNR = new piCceagDeSfdc_SalesOrder.BAPIPARTNR();
        piCceagDeSfdc_SalesOrder.BAPICUPRT BAPICUPRT =	new piCceagDeSfdc_SalesOrder.BAPICUPRT();
        piCceagDeSfdc_SalesOrder.BAPISDHEAD BAPISDHEAD = new piCceagDeSfdc_SalesOrder.BAPISDHEAD();
        piCceagDeSfdc_SalesOrder.BAPISCHDL BAPISCHDL = 	new piCceagDeSfdc_SalesOrder.BAPISCHDL();
        piCceagDeSfdc_SalesOrder.BAPICUBLB BAPICUBLB =	new piCceagDeSfdc_SalesOrder.BAPICUBLB();
        piCceagDeSfdc_SalesOrder.BAPIITEMEX BAPIITEMEX = new piCceagDeSfdc_SalesOrder.BAPIITEMEX();
        piCceagDeSfdc_SalesOrder.RETURN_element RETURN_element = new piCceagDeSfdc_SalesOrder.RETURN_element();
        piCceagDeSfdc_SalesOrder.BAPIITEMIN BAPIITEMIN = new piCceagDeSfdc_SalesOrder.BAPIITEMIN();
        piCceagDeSfdc_SalesOrder.ORDER_CFGS_INST_element ORDER_CFGS_INST_element = new piCceagDeSfdc_SalesOrder.ORDER_CFGS_INST_element();
        piCceagDeSfdc_SalesOrder.MESSAGETABLE_element MESSAGETABLE_element = new piCceagDeSfdc_SalesOrder.MESSAGETABLE_element();
        piCceagDeSfdc_SalesOrder.BAPIPARNR BAPIPARNR = new piCceagDeSfdc_SalesOrder.BAPIPARNR();
        piCceagDeSfdc_SalesOrder.BAPIRETURN BAPIRETURN = new piCceagDeSfdc_SalesOrder.BAPIRETURN();
        piCceagDeSfdc_SalesOrder.ORDER_TEXT_element ORDER_TEXT_element = new piCceagDeSfdc_SalesOrder.ORDER_TEXT_element();
        piCceagDeSfdc_SalesOrder.ORDER_CFGS_BLOB_element ORDER_CFGS_BLOB_element = new piCceagDeSfdc_SalesOrder.ORDER_CFGS_BLOB_element();
        piCceagDeSfdc_SalesOrder.CC_SalesOrderCreateOrder_element CC_SalesOrderCreateOrder_element = new piCceagDeSfdc_SalesOrder.CC_SalesOrderCreateOrder_element();
        piCceagDeSfdc_SalesOrder.BAPISDTEXT BAPISDTEXT = new piCceagDeSfdc_SalesOrder.BAPISDTEXT();
        piCceagDeSfdc_SalesOrder.EXTENSIONIN_element EXTENSIONIN_element = new piCceagDeSfdc_SalesOrder.EXTENSIONIN_element();
        piCceagDeSfdc_SalesOrder.BAPIINCOMP BAPIINCOMP = new piCceagDeSfdc_SalesOrder.BAPIINCOMP();
        piCceagDeSfdc_SalesOrder.BAPISDHD1 BAPISDHD1 = new piCceagDeSfdc_SalesOrder.BAPISDHD1();
        piCceagDeSfdc_SalesOrder.ORDER_CFGS_REF_element ORDER_CFGS_REF_element = new piCceagDeSfdc_SalesOrder.ORDER_CFGS_REF_element();
        piCceagDeSfdc_SalesOrder.BAPICUVAL BAPICUVAL = new piCceagDeSfdc_SalesOrder.BAPICUVAL();
        piCceagDeSfdc_SalesOrder.BAPISDHEDU BAPISDHEDU = new piCceagDeSfdc_SalesOrder.BAPISDHEDU();
        piCceagDeSfdc_SalesOrder.BAPIADDR1 BAPIADDR1 = new piCceagDeSfdc_SalesOrder.BAPIADDR1();
        piCceagDeSfdc_SalesOrder.BAPIRET2 BAPIRET2 = new piCceagDeSfdc_SalesOrder.BAPIRET2();
        piCceagDeSfdc_SalesOrder.BAPICUCFG BAPICUCFG = new piCceagDeSfdc_SalesOrder.BAPICUCFG();
        piCceagDeSfdc_SalesOrder.PARTNERADDRESSES_element PARTNERADDRESSES_element = new piCceagDeSfdc_SalesOrder.PARTNERADDRESSES_element();
        piCceagDeSfdc_SalesOrder.ORDER_PARTNERS_element ORDER_PARTNERS_element = new piCceagDeSfdc_SalesOrder.ORDER_PARTNERS_element();
        piCceagDeSfdc_SalesOrder.ORDER_SCHEDULE_EX_element ORDER_SCHEDULE_EX_element = new piCceagDeSfdc_SalesOrder.ORDER_SCHEDULE_EX_element();
        piCceagDeSfdc_SalesOrder.BAPISOLDTO BAPISOLDTO = new piCceagDeSfdc_SalesOrder.BAPISOLDTO();
        piCceagDeSfdc_SalesOrder.ORDER_ITEMS_OUT_element ORDER_ITEMS_OUT_element = new piCceagDeSfdc_SalesOrder.ORDER_ITEMS_OUT_element();
        piCceagDeSfdc_SalesOrder.BAPICUINS BAPICUINS = new piCceagDeSfdc_SalesOrder.BAPICUINS();
        piCceagDeSfdc_SalesOrder.CC_SalesOrderCreateOrderResponse_element CC_SalesOrderCreateOrderResponse_element = new piCceagDeSfdc_SalesOrder.CC_SalesOrderCreateOrderResponse_element();
        piCceagDeSfdc_SalesOrder.BAPICCARD BAPICCARD = new piCceagDeSfdc_SalesOrder.BAPICCARD();
        piCceagDeSfdc_SalesOrder.ORDER_SCHEDULE_IN_element ORDER_SCHEDULE_IN_element = new piCceagDeSfdc_SalesOrder.ORDER_SCHEDULE_IN_element();
        piCceagDeSfdc_SalesOrder.CC_SalesOrderSimulateOrder_element CC_SalesOrderSimulateOrder_element = new piCceagDeSfdc_SalesOrder.CC_SalesOrderSimulateOrder_element();
        piCceagDeSfdc_SalesOrder.CC_SalesOrderGetRequestDate_element CC_SalesOrderGetRequestDate_element = new piCceagDeSfdc_SalesOrder.CC_SalesOrderGetRequestDate_element();
        piCceagDeSfdc_SalesOrder.CC_SalesOrderSimulateOrderResponse_element CC_SalesOrderSimulateOrderResponse_element = new piCceagDeSfdc_SalesOrder.CC_SalesOrderSimulateOrderResponse_element();
        piCceagDeSfdc_SalesOrder.ORDER_SCHEDULES_IN_element ORDER_SCHEDULES_IN_element = new piCceagDeSfdc_SalesOrder.ORDER_SCHEDULES_IN_element();
        piCceagDeSfdc_SalesOrder.BAPICOND BAPICOND = new piCceagDeSfdc_SalesOrder.BAPICOND();
        piCceagDeSfdc_SalesOrder.BAPIPAREX BAPIPAREX = new piCceagDeSfdc_SalesOrder.BAPIPAREX();
        piCceagDeSfdc_SalesOrder.BAPICCARD_EX BAPICCARD_EX = new piCceagDeSfdc_SalesOrder.BAPICCARD_EX();
        piCceagDeSfdc_SalesOrder.CC_SalesOrderGetRequestDateResponse_element CC_SalesOrderGetRequestDateResponse_element 	= new piCceagDeSfdc_SalesOrder.CC_SalesOrderGetRequestDateResponse_element();
        piCceagDeSfdc_SalesOrder.BAPISDITM BAPISDITM = new piCceagDeSfdc_SalesOrder.BAPISDITM();
        piCceagDeSfdc_SalesOrder.BAPIPAYER BAPIPAYER = new piCceagDeSfdc_SalesOrder.BAPIPAYER();
        piCceagDeSfdc_SalesOrder.ORDER_CFGS_VALUE_element ORDER_CFGS_VALUE_element = new piCceagDeSfdc_SalesOrder.ORDER_CFGS_VALUE_element();
        piCceagDeSfdc_SalesOrder.ORDER_CONDITION_EX_element ORDER_CONDITION_EX_element = new piCceagDeSfdc_SalesOrder.ORDER_CONDITION_EX_element();
        piCceagDeSfdc_SalesOrder.ORDER_CCARD_element ORDER_CCARD_element = new piCceagDeSfdc_SalesOrder.ORDER_CCARD_element();
        piCceagDeSfdc_SalesOrder.BAPISHIPTO BAPISHIPTO = new piCceagDeSfdc_SalesOrder.BAPISHIPTO();
        piCceagDeSfdc_SalesOrder.ORDER_INCOMPLETE_element ORDER_INCOMPLETE_element = new piCceagDeSfdc_SalesOrder.ORDER_INCOMPLETE_element();
        piCceagDeSfdc_SalesOrder.CCWSSalesOrderRequestResponse_OutPort CCWSSalesOrderRequestResponse_OutPort = new piCceagDeSfdc_SalesOrder.CCWSSalesOrderRequestResponse_OutPort();
        
        
    }
}
