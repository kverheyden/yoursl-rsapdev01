/*
 * @(#)SCOffroadOrder.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information 
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/*
 *
 * @author Georg Birkenheuer <gbirkenheuer@gms-online.de>
 * @version $Revision$, $Date$
 *
 * Call:
 * createOrder(String serialNumber, String resourceName, Datetime start, Integer duration, String orderType)
 * Test case:
 * SCOffroadOrder.createOrder('35221931', 'DE002513', datetime.now(), 15, '5701');
 */
public with sharing class SCOffroadOrder {


    public static String[] validate(String installedBaseSerialNo, String resourceID, String orderType,
                                    String errorSymptom1, String errorSymptom2, Datetime start, Integer duration)
    {
        /* TODO:
         *  - Check if 'start' lies within given constraints
         *  - Check if 'duration' lies within given constraints       
         */
         
        // Check if oderType is '5701' (ZC02-Reparatur) 
        if (orderType != '5701')
        {
            return new String [] { 'EINVALID', 'Only Orders with Type ZC02-Reparatur may be created "OffRoute".' };
        }
         
        // Check if InstalledBase with given serialNo exists:
        List<SCInstalledBase__c> ibs = [SELECT Id, Name, SerialNo__c
                                        FROM SCInstalledBase__c 
                                        WHERE SerialNo__c = :installedBaseSerialNo];
        
        if(ibs.size() == 0) // Abort validation in case we don't find an IB with matching serialNo
        {
            return new String [] { 'EINVALID', 'Cannot find Equipment with SerialNo: ' + installedBaseSerialNo };
        }
        
        List<SCResource__c> resources = [Select Id FROM SCResource__c 
                                         WHERE Id = :resourceID];
        
        if(resources.size() == 0) // Abort validation in case we don't find an Enginner with matching ID
        {
            return new String [] { 'EINVALID', 'Cannot find Engineer with ID: ' + resourceID };
        } 
        
        // TODO:
        // - Check if errorSymptom1 and errorSymptom2 are valid ErrorSymptoms
        
        return new String [] { 'ENOTRANS', 'TODO: implement validation' };
    }
    
    /*
     * Create an instance of SCboOrder from an OffroadOrder call
     */
    public static String createOrder(String serialNumber, String resourceName,  Datetime start, Integer duration, String orderType)
    {
        // reate Order
        SCboOrder newboOrder = null;
        SCOrder__c newOrder = new SCOrder__c();
        
        SCResource__c resource = null;
        // select technican
        resource = selectResource(resourceName);
        // select equipment 
        SCInstalledBase__c installedBase =  selectEquipment(serialNumber);
        
        // select customer roles
        //SCInstalledBaseRole__c equser = selectInstalledBaseRole(installedBase.InstalledBaseLocation__c, 'User');
        //SCInstalledBaseRole__c eqowner = selectInstalledBaseRole(installedBase.InstalledBaseLocation__c, 'Owner');

        // the setting order as in the document
        //this.order.ID = CreateOrderFromExternalMessage.PlainFields.OrderID;
        newOrder.ERPOrderNo__c  = null;
        newOrder.ERPLongtext__c = 'Offroad created order by ' + resource.Name + ' for equipment ' + installedBase.Name +'.';     
        newOrder.ID2__c         = null;
        newOrder.IdExt__c       = null;
        newOrder.ID3__c         = null; 
        
         
        
        // search technician and select his/her workcenter
        if(resource.Employee__r != null && resource.Employee__r.ERPWorkcenter__c != null)
        {
            newOrder.cce_Workcenter__c = resource.Employee__r.ERPWorkcenter__c;
        }
        else
        {
            newOrder.cce_Workcenter__c = null;
        }
        // search technician and select his/her plant
        if(resource.DefaultDepartment__c != null)
        {
            newOrder.DepartmentResponsible__c = resource.DefaultDepartment__c;
            newOrder.DepartmentCurrent__c = resource.DefaultDepartment__c;
        }
        else 
        {
            newOrder.DepartmentResponsible__c = null;
            newOrder.DepartmentCurrent__c = null;
        }
        
        newOrder.DepartmentTurnover__c = null;
        
        newOrder.Description__c = 'Offroad created order by ' + resource.Name + ' for equipment ' + installedBase.Name +'.';
        
        // set start date
        if(start != null)
        {
            newOrder.CustomerPrefStart__c = start.date();
        }
        else
        {
            String msg = 'StartDate is empty';
            debug(msg);
            newOrder.CustomerPrefStart__c = null;
        }   
        
        // set duration
        if(duration != null)
        {
            newOrder.Duration__c = duration;
        }
        else
        {
            String msg = 'duration is empty, set 60 min as default';
            debug(msg);
            newOrder.Duration__c = 60;
        }  
        
        // select sales area of the technician.
        if(resource.Employee__r != null && resource.Employee__r.ERPSalesArea__c != null)
        {   
            newOrder.SalesArea__c = resource.Employee__r.ERPSalesArea__c;
        }
        else
        {
            String msg = 'SalesArea is empty';
            debug(msg);
            newOrder.SalesArea__c = null;
        }   
        
        if(resource.Employee__r != null && resource.Employee__r.ERPDistributionChannel__c != null)
        {     
            newOrder.DistributionChannel__c = resource.Employee__r.ERPDistributionChannel__c;
        }
        else
        {
            String msg = 'DistributionChannel is empty';
            debug(msg);
            newOrder.DistributionChannel__c = null;
        }        
        
        if(resource.Employee__r != null && resource.Employee__r.ERPDivision__c != null)
        {  
            newOrder.Division__c = resource.Employee__r.ERPDivision__c;
        }
        else
        {
            String msg = 'Division is empty';
            debug(msg);
            newOrder.Division__c = null;
        }    
        
        newOrder.SalesOffice__c = null;
        

        newOrder.IdOld__c = null;
        newOrder.AdditionalEmployee__c = false;
        newOrder.Brand__c = null;
        newOrder.BrandName__c = null;
        newOrder.Country__c = 'DE';



        if(orderType == null)
        {
            newOrder.Type__c = '5701';
            String msg = 'Order Type is not set. As default is used ClockPort OrderType = 5701 (ZC02-Reparatur) is set';
            debug(msg);
        }
        else
        {   // (ZC02-Reparatur)    || ( ZC01-Wartung)     || (ZC09-Rückholung)
            if(orderType == '5701' || orderType == '5707' || orderType == '5704')
            {
                newOrder.Type__c = orderType;
            }
            else
            {
                newOrder.Type__c = '5701';
                String msg = 'Wrong Order Type is set. As default is used ClockPort OrderType = 5701 (ZC02-Reparatur) is set';
                debug(msg);
            }
        }
        
        newOrder.InvoicingType__c = null;
        newOrder.InvoicingSubtype__c = null;
        
        newOrder.ClosedByInfo__c = null;
        newOrder.Closed__c = null;

        newOrder.CustomerPrefEnd__c = null;
        newOrder.CustomerPriority__c = null;
        
        // Object for application settings
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        String defCustomerTimeWindow = appSettings.DEFAULT_CUSTOMERTIMEWINDOW__c;
        newOrder.CustomerTimewindow__c = defCustomerTimeWindow; //'12404';    // AM till 13:00 = 12404, All day = 12401

        newOrder.BulkId__c = null;
        newOrder.DistanceZone__c = '901';
        newOrder.EstimatedPrice__c = null;
        //newOrder.EstimationType__c = null; without an assignment a default value is taken
        //newOrder.FailureType__c = null;
        newOrder.FailureSince__c = null;
        newOrder.InsurancePolicy__c = null;
        newOrder.InvoicingNo__c = null;
        newOrder.InvoicingReleased__c = false;
        newOrder.InvoicingDate__c = null;
        //newOrder.InvoicingStatus__c = null; without an assignment a default value is taken
        //newOrder.MaterialStatus__c = null;
        newOrder.Followup1__c = null;
        newOrder.Followup2__c = null;
        //newOrder.Standby__c = null; automatic default
        newOrder.PaymentType__c = null;  // Contract
        
        newOrder.Price__c = null;
        newOrder.Status__c = '5501';// Open


        newOrder.InvoicingText__c = null;
        
        newOrder.Channel__c = '5305'; // Is this correct? DOMVAL_CHANNEL_IMPORT_FROM_EXTERN ???
        //newOrder.ValidationStatus__c = null; an automatic default
        newOrder.Info__c = null;

        newboOrder = new SCboOrder(newOrder);
        
        if(installedBase != null)
        {           
            // a installed base has been found
            newboOrder.addOrderItem(installedBase);
            copyInstalledBaseFieldsIntoOrderItem(newboOrder, installedBase);
        } 
        
        createOrderRoles(newboOrder, installedBase);
        

        String msg = 'save';
        debug(msg);
        //  save alltogether
        newboOrder.save();
        debug('newboOrder: ' + newboOrder + ' Name ' + newboOrder.order.Name + ' ID: ' + newboOrder.order.Id);
        
        // TODO Create Appointment
        // TODO Create Assigment
                
        return 'success';
    }// createOrder

    /*
    * Selects the neccessary information about the resource.
    * The technician is identified by its Name (e.g. DE002513)
    *
    */
    private static SCResource__c selectResource(String resourceName)
    {
        SCResource__c resource = null;
        // select technican
        List<SCResource__c> resources = [select Id, Name, DefaultDepartment__c, Employee__c,
            Employee__r.ERPWorkcenter__c,
            Employee__r.ERPSalesArea__c,
            Employee__r.ERPDistributionChannel__c,
            Employee__r.ERPDivision__c
            from SCResource__c where Name =: resourceName];
         
         if(resources.size() > 0)
         {
            resource = resources[0];
         }
         else
         {
            
         }
         return resource;
    }
    
    /*
    * Selects the Installed Base roles.
    * The technician is identified by its Name (e.g. DE002513)
    *
    */
    private static SCInstalledBaseRole__c selectInstalledBaseRole(String ibLocationId, String roleName)
    {
        SCInstalledBaseRole__c role = null;
    
        List<SCInstalledBaseRole__c> roles = [SELECT AccountAddress__c, Account__c ,Id,InstalledBaseLocation__c,LocationAddress__c,Name,
            Role__c FROM SCInstalledBaseRole__c 
            where InstalledBaseLocation__c =: ibLocationId
            and Role__c =: roleName];
            
         if(roles.size() == 1)
         {
            role = roles[0];
         }
         else if(roles.size() > 1)
         {
            debug('More than one role found for InstalledBaseLocation' + ibLocationId + ' and Role: ' +  role);
            role = roles[0];
         }
         return role;
    }
    

    
    
    
    /**
     * Creates order roles LE and RE based on the installedbase shipTo and soldTo accounts
     *
     * @param boOrder the new order
     * @param equipment
     *
     */
    private static void createOrderRoles(SCboOrder boOrder, SCInstalledBase__c equipment)
    {
        
        // select service recipient account based on shipToParty
        if(equipment.shipTo__c != null)
        {  
            boOrder.setRole(equipment.shipTo__c, SCfwConstants.DOMVAL_ORDERROLE_LE, false);
        }        

        if(equipment.soldTo__c != null)
        {  
            boOrder.setRole(equipment.soldTo__c, SCfwConstants.DOMVAL_ORDERROLE_RE, false);
        }        
        debug('boOrder.allRole Default:  ' + boOrder.allRole);
        
        debug('boOrder.allRole:' + boOrder.allRole);
    }// createOrderRoles
    
   /**
     * Finds an installed base using the serial number.
     * 
     */
    private static SCInstalledBase__c selectEquipment(String serialNumber)
    { 

        SCInstalledBase__c retValue;
        List<SCInstalledBase__c> ibs = [Select Id, Name, PriceList__c, ProductSkill__c, ProductPower__c, Brand__r.Name, 
            Article__c, ProductGroup__c, ProductUnitType__c, ProductUnitClass__c, 
            AcquisitionCurrency__c, AcquisitionValue__c, AuditHint__c, AuditLast__c, BookValueCurrency__c, BookValue__c, 
            Brand__c, Building__c, Description__c, Floor__c, IdExt__c, IdInt__c, InstallationDate__c, 
            LocationName__c, Phone__c, ProductModel__c, Room__c, SerialNo__c, Stock__c,  Plant__c, 
            cce_equipmenttypefilter__c, ShipTo__c, SoldTo__c,
            InstalledBaseLocation__c, 
            InstalledBaseLocation__r.ID2__c, 
            InstalledBaseLocation__r.GeoX__c, 
            InstalledBaseLocation__r.GeoY__c, 
            InstalledBaseLocation__r.GeoApprox__c, 
            InstalledBaseLocation__r.LocName__c, 
            InstalledBaseLocation__r.Address__c, 
            InstalledBaseLocation__r.FlatNo__c, 
            InstalledBaseLocation__r.Floor__c, 
            InstalledBaseLocation__r.Street__c, 
            InstalledBaseLocation__r.HouseNo__c, 
            InstalledBaseLocation__r.Extension__c, 
            InstalledBaseLocation__r.PostalCode__c,
            InstalledBaseLocation__r.City__c, 
            InstalledBaseLocation__r.District__c, 
            InstalledBaseLocation__r.County__c, 
            InstalledBaseLocation__r.CountryState__c, 
            InstalledBaseLocation__r.Country__c, 
            InstalledBaseLocation__r.Status__c
            from SCInstalledBase__c 
            where SerialNo__c = :serialNumber];
        
        if(ibs.size() == 1)
        {
            debug('installed base found');
            retValue = ibs[0];
            debug('installed base: ' + retValue);                
        }
        else if(ibs.size() > 1)
        {
            debug('installed bases found, more than once');
            retValue = ibs[0];
            debug('installed base: ' + retValue);
 
        }
        else
        {
            debug('installed base not found');
            return null;
        }
       return retValue;
    }   

    /**
    * copies fields of an installed base into an order item
    */
    private static void copyInstalledBaseFieldsIntoOrderItem(SCboOrder boOrder, SCInstalledBase__c installedBase)
    {
        if(boOrder.boOrderItems.size() > 0)
        {
            SCOrderItem__c orderItem = boOrder.boOrderItems[0].orderItem;
            orderItem.IBAcquisitionCurrency__c =  installedBase.AcquisitionCurrency__c; 
            orderItem.IBAcquisitionValue__c =     installedBase.AcquisitionValue__c;    
            orderItem.IBAuditHint__c =            installedBase.AuditHint__c;           
            orderItem.IBAuditLast__c =            installedBase.AuditLast__c;           
            orderItem.IBBookValueCurrency__c =    installedBase.BookValueCurrency__c;   
            orderItem.IBBookValue__c =            installedBase.BookValue__c;           
            orderItem.IBBrand__c =                installedBase.Brand__c;               
            orderItem.IBBuilding__c =             installedBase.Building__c;            
            orderItem.IBDescription__c =          installedBase.Description__c;         
            orderItem.IBFloor__c =                installedBase.Floor__c;               
            orderItem.IBIdExt__c =                installedBase.IdExt__c;               
            orderItem.IBIdInt__c =                installedBase.IdInt__c;               
            orderItem.IBInstallationDate__c =     installedBase.InstallationDate__c;    
            orderItem.IBLocationName__c =         installedBase.LocationName__c;        
            orderItem.IBPhone__c =                installedBase.Phone__c;               
            orderItem.IBProductModel__c =         installedBase.ProductModel__c;        
            orderItem.IBRoom__c =                 installedBase.Room__c;                
            orderItem.IBSerialNo__c =             installedBase.SerialNo__c;

            // Now setting the record type
            List<RecordType> recs = [Select Id, DeveloperName From RecordType
            Where SobjectType = 'SCOrderItem__c'
            And (DeveloperName = 'Equipment' 
            OR DeveloperName = 'SubEquipment')]; 
                 
            Map<String, RecordType> recordTypes = new Map<String, RecordType>();
            
            for(RecordType r : recs)
            {
                recordTypes.put(r.DeveloperName,r);
            }
                
            if(installedBase.cce_equipmenttypefilter__c == 'EQ')
            {
                orderItem.RecordType = recordTypes.get('Equipment');
                orderItem.RecordTypeId = recordTypes.get('Equipment').Id;
            }
            if(installedBase.cce_equipmenttypefilter__c == 'SU')
            {
                orderItem.RecordType = recordTypes.get('SubEquipment');
                orderItem.RecordTypeId = recordTypes.get('SubEquipment').Id;
            }
                 
        }           
    }                                                      
    
    /**
    * Used for Debug messages.
    */
    private static void debug(String msg)
    {
        System.debug('###...' +  msg);        
    }

}
