/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed 
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PofSDeviceModelControllerTest {

    static testMethod void testPofSDeviceModelController() {
    	
    	PictureOfSuccess__c pofs = new PictureOfSuccess__c();
		pofs.Bezeichnung__c	= 'Test Pof';
		pofs.ValidFrom__c =	Date.today();
		pofs.ValidUntil__c = Date.today();
		pofs.RedWeightingActivation__c = 25;
		pofs.RedWeightingArea__c = 25;
		pofs.RedWeightingAssortment__c = 25;
		pofs.RedWeightingEquipment__c = 25;
		pofs.Subtradechannel__c	= '121';
    	insert pofs;
    	
    	PofSDeviceModel__c pofsDeviceModel = new PofSDeviceModel__c();
    	pofsDeviceModel.PictureOfSuccess__c = pofs.Id;
    	insert pofsDeviceModel;
    	
        PofSDeviceModelController controller = new PofSDeviceModelController();
       	controller.GV_PofSId = pofs.Id;
       	controller.GV_CurrentPofSDeviceModelId = pofsDeviceModel.Id;
       	
       	List<PofSDeviceModel__c> pofsDeviceModelList = controller.getGL_PofSDeviceModels();
       	
       	System.assertEquals(pofsDeviceModelList[0].Id, pofsDeviceModel.Id);       	
       	System.assertEquals(controller.getGO_PofSDeviceLookup(), new PofSDeviceModel__c());
       	
       	controller.setGO_PofSDeviceLookup(pofsDeviceModel);
       	System.assertEquals(controller.getGO_PofSDeviceLookup(), pofsDeviceModel);
       	
       	controller.savePofSDeviceModel();
       	controller.deletePofSDeviceModel();
    }
}
