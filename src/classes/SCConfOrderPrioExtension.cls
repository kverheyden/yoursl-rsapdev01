/*
 * @(#)SCConfOrderPrioExtension.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Implements a simple configuration function for creating
 * SCConfOrderPrio items (used for determining the earliest date). 
 * @author Norbert Armbruster <narmbruster@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCConfOrderPrioExtension
{
    // used to access the current default country of the user
    static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();

    // context information for returing to the calling page
    ApexPages.StandardSetController con;
    public String returl {get; set;}
    
    // Variable filled by command links 
    public String countrySelected {get; set;}  
    // The current country of the user (for information only)
    public String countryCurrent {get; set;}  

    Map<String, Integer> mapResult;
    
    List<ResultCount> countryConfigItems;
    public List<ResultCount> getCountryConfig()
    {
        // always reload the data (actions might change the configuration)
        
        countryConfigItems = new List<ResultCount>();
        mapResult = new Map<String, Integer>();
        
        // Count the number of records per country
        String query = 'select country__c, count(id) cnt from SCConfOrderPrio__c group by country__c order by country__c';
        SObject[] queryresult = Database.query(query);
        for(SObject item : queryresult)
        {
            mapResult.put(String.valueOf(item.get(SCfwConstants.NamespacePrefix + 'country__c')), Integer.valueOf(item.get('cnt')));
        }
        
        // interate each country and set the record count
        Schema.DescribeFieldResult fieldResult = SCConfOrderPrio__c.Country__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry f : ple)
        {
            ResultCount value = new ResultCount();
            value.countryLabel = f.getLabel();
            value.countryValue = f.getValue();
            value.count   = 0;
            // is there a record count?
            Integer cnt = mapResult.get(value.countryValue);
            if(cnt != null)
            {
                value.count = cnt;
            }
            countryConfigItems.add(value);
        }
        return countryConfigItems;
    }
    
   /*
    * Returns the number of configuration items
    */
    public Integer countryConfigItemsCount()
    {
        if(countryConfigItems != null)
        {
            return countryConfigItems.size();
        }
        return 0;
    }

    
    /**
     * Returns the number of the selected items - used for 
     */
    public integer getSelectedCount() 
    {
        return con.getSelected().size();
    }

    /**
     * Contstructor
     */
    public SCConfOrderPrioExtension(ApexPages.StandardSetController controller)
    {
        // we store the return URL for redirecting to the same view
        returl = ApexPages.currentPage().getParameters().get('retURL');
        con = controller;
        con.setPageSize(100);
        // we store the current country
        countryCurrent = appSettings.DEFAULT_COUNTRY__c;
        
        countryConfigItems = new List<ResultCount>();
        mapResult = new Map<String, Integer>();
        
    }



    /**
     * Called to create standard items for the selected country
     */
    public PageReference onCreate()
    {
        if(countrySelected != null)
        {
            SCConfOrderPrioLoader.initialize(countrySelected);
        }
        return null;
    }

    /**
     * Called from within the management list to delete all items of a country. 
     */
    public PageReference onDelete()
    {
        if(countrySelected != null)
        {
            delete [select id from SCConfOrderPrio__c where country__c = :countrySelected];
        }    
        return null;
    }

    /**
     * Called to delete the selected items from the main browser
     */
    public PageReference onDeleteSelected()
    {
        // just delete the selected items
        List<SCConfOrderPrio__c> selectedItems = (List<SCConfOrderPrio__c>)con.getSelected();

        // there are always items selected (button is only renered if so)
        delete selectedItems;        

        // return to calling page
        return onCancel();
    }

    /**
     * Returns so the calling page
     */
    public PageReference onCancel()
    {
        PageReference p = new PageReference(returl);
        p.setRedirect(true);
        return p;
    }
    

   /*
    * Internal helper class for displaying the country details
    */
    public class ResultCount
    {
        public String countryLabel {get; set;}
        public String countryValue {get; set;}
        public Integer count {get; set;}
    }
    
 
   /*
    * A simple test method to get the test coverage.
    */
    public static testMethod void testConfig() 
    {
        String country = 'testonly2';
    
        Test.StartTest();

        // create an empty list (nothing selected)
        List<SCConfOrderPrio__c> items = new List<SCConfOrderPrio__c>();
        ApexPages.StandardSetController setCon = new ApexPages.StandardSetController(items);
        SCConfOrderPrioExtension ext = new SCConfOrderPrioExtension(setCon);

        System.AssertEquals(0, ext.getSelectedCount());

        // set the current country
        ext.CountrySelected = country;
        System.AssertEquals(country, ext.CountrySelected);

        ext.onCreate();
        Integer count = [select count() from SCConfOrderPrio__c where country__c = :country];
        System.AssertEquals(true, count > 0);
        
        // read the configuration for our test country
        List<ResultCount> result = ext.getCountryConfig();
        System.AssertEquals(true, ext.countryConfigItems.size() > 0);
        
        // delete the current items of the country
        ext.onDelete();
        count = [select count() from SCConfOrderPrio__c where country__c = :country];
        System.AssertEquals(0, count);
 
        // Dummy test       
        ext.returl = '/dummy';
        ext.onCancel();
        ext.onDeleteSelected();
        
        Test.StopTest();
   } 
}
