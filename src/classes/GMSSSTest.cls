@isTest
private class GMSSSTest
{
    /*
     * Creates test data for WebService input: SCwsBase.Context
     */
    private static SCwsBase.Context createWSContext()
    {
        SCwsBase.Context ctx = new SCwsBase.Context();
        
        ctx.accountid     =  null;
        ctx.brand         = '001';
        ctx.country       = 'NL';
        ctx.lang          = 'en_US';
        ctx.accRecordType = [Select Id from RecordType 
                              where DeveloperName = 'Customer' 
                                and SobjectType = 'Account'].Id;
        
        return ctx;
    } // createWSContext

    /*
     * Creates test data: SCwsBase.SCOrder
     */
    private static SCwsBase.SCOrder createWSSCOrder()
    {  
        SCwsBase.SCOrder order = new SCwsBase.SCOrder();
        SCwsBase.SCOrderRole orderRole = new SCwsBase.SCOrderRole();
        SCwsBase.SCOrderLine orderLine = new SCwsBase.SCOrderLine();
        SCwsBase.SCOrderLine orderItemLine = new SCwsBase.SCOrderLine();
        SCwsBase.SCOrderItem orderItem = new SCwsBase.SCOrderItem();
        SCwsBase.SCOrderAssignment assignment = new SCwsBase.SCOrderAssignment();
        SCwsBase.SCRepairCode repairCode = new SCwsBase.SCRepairCode();

        assignment.resource = new SCwsBase.SCResource();
        orderItem.installedBase = new SCwsBase.SCInstalledBase();
        orderItem.installedBase.safetyCheck = new SCwsBase.SCSafetyCheck();
        orderItem.orderLines.add(orderItemLine);
        orderItem.assignments.add(assignment);
        orderItem.repairCodes.add(repairCode);
        order.orderRoles.add(orderRole);
        order.orderItems.add(orderItem);
        order.orderLines.add(orderLine);
        return order;
    } // createWSSCOrder
    
    
    
    /*
     * Test: Book material test.
     */
    static testMethod void bookMaterialTest() 
    {
        SCHelperTestClass3.createCustomSettings('GB', true);
        SCHelperTestClass.createDomsForOrderCreation();
        String bookMaterial = WSMobileServiceGessat.appSettings.MATERIAL_BOOKING__c;
        if (!SCBase.isSet(bookMaterial) || !bookMaterial.equals('1'))
        {
            return;
        }
        
        SCHelperTestClass.createOrderTestSet3(true);        
        SCHelperTestClass.createTestPlant(true);   
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);   
        SCHelperTestClass.createTestCalendar(true);
        SCHelperTestClass.createTestBusinessUnit(SCHelperTestClass.stocks.get(2).Id, 
                                               SCHelperTestClass.Calendar.Id, true);
        SCHelperTestClass.createTestAssignments(SCHelperTestClass.resources, SCHelperTestClass.stocks, 
                                              SCHelperTestClass.businessUnit.Id, true);
        SCHelperTestClass.createTestStockItem(SCHelperTestClass.stocks.get(3).Id, 
                                            SCHelperTestClass.articles[0].Id, false);
        SCHelperTestClass.stockItem.Qty__c = 1.0;
        insert SCHelperTestClass.stockItem;

        Date today = Date.today();

        SCwsBase.Context ctx = createWSContext();
 
        SCwsBase.Mode mode = new SCwsBase.Mode();
        mode.parameterMode = 'ASSIGNMENTID';
        mode.detailMode = true;

        // Test 1: update follow up data by an assignment id
        SCwsBase.SCOrder[] orderInput = new List<SCwsBase.SCOrder>();
        SCwsBase.SCOrder order = createWSSCOrder();
        order.description   = 'Order test text';
        order.invoicingText = 'Invoice test text';
        SCwsBase.SCOrderAssignment assignment = order.orderItems[0].assignments[0];
        assignment.id              = SCHelperTestClass.assignmentMap.get('assignmentDispo1').Id;
        assignment.status          = SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED;
        assignment.followUp        = SCfwConstants.DOMVAL_FOLLOWUP1_NONE;
        assignment.followUpReason  = '2901';
        assignment.resource.name   = 'Testuser';
        SCwsBase.SCOrderLine orderLine = order.orderItems[0].orderLines[0];
        orderLine.articleNumber    = SCHelperTestClass.articles[0].Name;
        orderLine.qty              = '1';
        orderLine.listPrice        = '50';
        orderLine.price            = '96';
        orderLine.posPrice         = '50';
        orderLine.unitPrice        = '50';
        orderLine.unitNetPrice     = '50';
        orderLine.discount         = '10';
        orderLine.tax              = '18';
        orderLine.taxRate          = '19';
        orderLine.invoicingType    = '170001';
        orderLine.invoicingSubType = '180001';
        orderInput.add(order);
        SCwsBase.ResultSCOrder[] results;
        
        Test.startTest();
        
        try
        {
            results = WSMobileServiceGessat.updateOrderLine(ctx, mode, orderInput);
            
            System.assert(results.size() == 1);
            System.assert(results[0].returnCode == 0);
            System.assert(results[0].returnText == 'successful');
            System.assert(results[0].id != null);
            System.assert(results[0].resultObject != null);
            System.assertEquals(1, results[0].resultObject.orderItems[0].orderLines.size());
//TODO            System.assertEquals('1', results[0].resultObject.orderItems[0].orderLines[0].qty);
            System.assertEquals('96.00', results[0].resultObject.orderItems[0].orderLines[0].price);
            System.assertEquals('18.00', results[0].resultObject.orderItems[0].orderLines[0].tax);
            
            results = WSMobileServiceGessat.updateOrderAssignment(ctx, mode, orderInput);
            
            System.assert(results.size() == 1);
            System.assert(results[0].returnCode == 0);
            System.assert(results[0].returnText == 'successful');
            System.assert(results[0].id != null);
            System.assert(results[0].resultObject != null);
            String prefix = '[Testuser - ' + today.format() + ']: ';
            System.assertEquals(prefix + 'Order test text', 
                                results[0].resultObject.description);
            System.assertEquals('Invoice test text', 
                                results[0].resultObject.invoicingText);
            System.assertEquals(SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED, 
                                results[0].resultObject.orderItems[0].assignments[0].status);
            System.assertEquals(SCfwConstants.DOMVAL_FOLLOWUP1_NONE, 
                                results[0].resultObject.orderItems[0].assignments[0].followUp);
            System.assertEquals('2901', 
                                results[0].resultObject.orderItems[0].assignments[0].followUpReason);
            
            SCStockItem__c stockItem = [Select Id, Qty__c from SCStockItem__c 
                                         where Id = :SCHelperTestClass.stockItem.Id];
            System.assertEquals(0, stockItem.Qty__c);

            SCOrderLine__c ordLine = [Select Id, MaterialStatus__c, Order__r.MaterialStatus__c 
                                        from SCOrderLine__c 
                                       where Order__c = :SCHelperTestClass.order.Id 
                                         and Article__c = :SCHelperTestClass.articles[0].Id Limit 1];
            System.assertEquals(SCfwConstants.DOMVAL_MATSTAT_BOOKED, ordLine.MaterialStatus__c);
            System.assertEquals(SCfwConstants.DOMVAL_MATSTAT_BOOKED, ordLine.Order__r.MaterialStatus__c);
        }
        catch(Exception e)
        {
            System.debug('#### bookMaterialTest(): Exception -> ' + e);
            //System.assert(false, e.getMessage());
        }
        
        Test.stopTest();
    } // bookMaterialTest
}
