/*
 * @(#)SCbtcYourSLOrderToSAP.cls 
 * Template
 */

/**
 * As we can not call a web service when we create data (uncommitted work pendingexception)
 * we have to use a separate job to initiate the transfer to SAP. 
 * 
 *
 * Excample:
 * SCbtcYourSLOrderToSAP.asyncTransferAll(0, 'trace');
 *
 */
global with sharing class SCbtcYourSLOrderToSAP extends SCbtcBase 
       implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts  
{
    private static final Integer batchSize = 10;
    private ID batchprocessid = null;
    public ID getBatchProcessId()
    {
        return batchprocessid;
    }
    String mode = 'productive';
    Integer max_records = 0;
    public String extraCondition = null;

    public Id orderId = null;
    
    private Boolean autoCreated = true;
    private String query; // Query for batch job 

    // Object for application settings
    public static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();


    public static ID asyncTransferAll(Integer max_records, String mode)
    {
        System.debug('###mode: ' + mode);
        SCbtcYourSLOrderToSAP btc = new SCbtcYourSLOrderToSAP(max_records, mode);

        btc.batchprocessid  = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    }  

    public static ID asyncTransferAll(Integer max_records, String mode, String extraCondition)
    {
        System.debug('###mode: ' + mode);
        SCbtcYourSLOrderToSAP btc = new SCbtcYourSLOrderToSAP(max_records, mode, extraCondition);

        btc.batchprocessid  = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    }  

    public static ID asyncTransfer(ID orderId, Integer max_records, String mode)
    {
        System.debug('###mode: ' + mode);
        SCbtcYourSLOrderToSAP btc = new SCbtcYourSLOrderToSAP(orderId, max_records, mode);

        btc.batchprocessid  = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    }  

    public static List<SCOrder__c> syncTransfer(ID orderId, Integer max_records, String mode)
    {
        System.debug('###mode: ' + mode);
        SCbtcYourSLOrderToSAP btc = new SCbtcYourSLOrderToSAP(orderId, max_records, mode);

        Boolean startCoreReturnsRetValue = false;
        Boolean aborted = false;        
        btc.startCore(startCoreReturnsRetValue, aborted); // calculates the query 
        List<SObject> mList = Database.query(btc.query); // runs the query        
        List<SCOrder__c> retValue = btc.executeCore(mList);
        return retValue;
    }  



   /*
    * Called by the framework when the batch job is started. We construct the soql statement
    * @param BC the batch context
    * @return the query locator with the selected mainenances
    */
    global override Database.QueryLocator start(Database.BatchableContext BC)
    {
        debug('start');
        Boolean aborted = false;
        if(abortOneOfTwoSameJobsRunning(BC.getJobId(), 'SCbtcYourSLOrderToSAP', 'start'))
        {
            aborted = true;
        }
        
        Boolean startCoreReturnsRetValue = true;
        return startCore(startCoreReturnsRetValue, aborted);
    } // start

    public Database.QueryLocator startCore(Boolean returnsRetValue, Boolean aborted)
    {
        // one order will be created from one SContractVisit__c record 
        // one order will be created from one SContractVisit__c record 
        
        /* To check the syntax of SOQL statement 
        List<SCOrderItem__c> orderItemList = [select id, Order__c from SCOrderItem__c 
                                                    where Order__r.Channel__c = 'YOURSL' 
                                                    and Order__r.ERPStatusOrderCreate__c = 'none'
                                                    and Order__r.ERPOrderNo__c = null
                                                    and Order__r.status__c <> '5507' // cancelled
													and InstalledBase__r.SerialNo__c <> null
                                                    limit 1];
        debug('start: after checking statement');
        */

        if(orderId == null)
        {
            query = 'select id, Order__c from SCOrderItem__c where Order__r.Channel__c = \'YOURSLCHANNEL\' and Order__r.ERPStatusOrderCreate__c = \'none\' and Order__r.ERPOrderNo__c = null and Order__r.status__c <> \'5507\' and  InstalledBase__r.SerialNo__c <> null';

            if(extraCondition != null)
            {
                query += ' and (' + extraCondition + ')';
            }                   
             
            if(aborted)
            {
                query += ' limit 0';
            }
            else
            {
                if(max_records > 0)
                {
                    query += ' limit ' + max_records;
                }
            }               
        }
        else
        {
            query = 'select id, Order__c from SCOrderItem__c where Order__r.Channel__c = \'YOURSLCHANNEL\' and Order__r.ERPStatusOrderCreate__c = \'none\' and Order__r.ERPOrderNo__c = null and Order__r.status__c <> \'5507\' and InstalledBase__r.SerialNo__c <> null'
		          + ' and ID = \'' + orderId + '\'';
            if(aborted)
            {
                query += ' limit 0';
            }
            else
            {
                if(max_records > 0)
                {
                    query += ' limit ' + max_records;
                }
            }
        }
        debug('query: ' + query);
        Database.QueryLocator retValue = null;
        if(returnsRetValue)
        {
            retValue = Database.getQueryLocator(query);
        }
        return retValue;
    }

   /*
    * Called for each batch of records to process.
    * @param BC the batch context
    * @param scope the list records to be processed
    */
    global override void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        debug('execute');
        if(abortOneOfTwoSameJobsRunning(BC.getJobId(), 'SCbtcYourSLOrderToSAP', 'execute'))
        {
            return;
        }
        executeCore(scope);
    } // execute

    public List<SCOrder__c> executeCore(List<sObject> scope)
    {
    	List<SCOrder__c> retValue = new List<SCOrder__c>();
        List<SCMaintenance__c> maintenanceUpdateList = new List<SCMaintenance__c>();
        
        for(sObject rec: scope)
        {
            debug('sObject: ' + rec);
            SCOrderItem__c o = (SCOrderItem__c)rec;
            // CCEAG --> 
            // now call the SAP interface to create the SAP order (synchronously)
            // the Trigger SCOrder_AI_CallSapWebService is suppressing the async callout !
            Boolean async = false; // future calls in future calls are not supported so we need a synch callout
            CCWCOrderCreate.callout(o.Order__c, async, false);
			// we do not log the job because they are logged in the CCWCOrderCreate
            // CCEAG <-- 
        }
        debug('mainenanceUpdateList: ' + maintenanceUpdateList);
        update maintenanceUpdateList;
        return retValue;
    }
   /*
    * Called by the framework when the batch job has been completed. 
    * We send an e-mail notification about the status
    * @param BC the batch context
    */
    global override void finish(Database.BatchableContext BC)
    {
    } // finish

    /**
     * Constructor
     * @param mode could be 'test' or 'productive'
     */
    public SCbtcYourSLOrderToSAP(Integer max_records, String mode)
    {
        this(null, max_records, mode, null);
    }

    public SCbtcYourSLOrderToSAP(Integer max_records, String mode, String extraCondition)
    {
        this(null, max_records, mode, extraCondition);
    }

    public SCbtcYourSLOrderToSAP(Id orderId, Integer max_records, String mode)
    {
        this(orderId, max_records, mode, null);
    }
    /**
     * Constructor
     * @param contractID
     * @param mode could be 'test' or 'productive'
     */
    public SCbtcYourSLOrderToSAP(Id orderId, Integer max_records, String mode,
            String extraCondition)
    {
        this.orderId = orderId;
        this.max_records = max_records;
        this.mode = mode;
        this.extraCondition = extraCondition;
    }

    public String getQuery()
    {
        return query;
    }    

    private void debug(String text)
    {
        if(mode.equalsIgnoreCase('test')
           || mode.equalsIgnoreCase('trace'))
        {
            System.debug('###...................' + text);
        }
    }
}
