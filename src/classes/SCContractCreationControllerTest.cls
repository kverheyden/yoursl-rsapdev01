/*
 * @(#)SCContractCreationControllerTest.cls
 *
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Testclass for the "Contract creation Wizard"
 * 
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCContractCreationControllerTest
{

    private static SCContractCreationController ccc;
    static SCApplicationSettings__c globalAppSetting = SCApplicationSettings__c.getInstance();
    
    static testMethod void contractCreationControllerPositiv1()
    {
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.createInstalledBaseLocation(true);
        SCHelperTestClass.createInstalledBase(true);
        //SCHelperTestClass.createProductModel(true);
        
        SCHelperTestClass2.createContractTemplates(true);       
        
        // SCHelperTestClass2.createContract('Contract', SCHelperTestClass2.insuranceTemplate.Id, SCHelperTestClass.account.Id, SCHelperTestClass.installedBaseSingle.Id, true);
        // ApexPages.currentPage().getParameters().put('id', SCHelperTestClass2.contracts.get(0).Id);
        
        ApexPages.currentPage().getParameters().put('aid', SCHelperTestClass.account.Id);
        ApexPages.currentPage().getParameters().put('ref', SCHelperTestClass.account.Id);
       
        
        ccc = new SCContractCreationController();
        
        ccc.selectedRecordType = [Select Id from RecordType where SobjectType = :(SCfwConstants.NamespacePrefix + 'SCContract__c') LIMIT 1].Id;
        ccc.selectedTemplate = SCHelperTestClass2.insuranceTemplate.Id;
        ccc.getCanCreateContacts();
        ccc.loadNewAccount();
        ccc.loadNewAccountOwner();
        ccc.loadNewAccountIR();
        ccc.saveMaintenanceOrder();
        ccc.showDetails();
        ccc.hideDetails();
        ccc.reloadContacts();
        ccc.showContacts();
        ccc.hideContacts();
        ccc.createContact();
        ccc.selectedContact = ccc.contact.id;
        //ccc.holdContactData();
        ccc.getContacts();
        ccc.setStatus();
        ccc.getRecordType();
        ccc.getTemplates();
        ccc.getRecordTypeDescription();
        ccc.onLoadTemplate();
        ccc.getIBL();
        
        ccc.newSelectedItems = String.valueOf(SCHelperTestClass.installedBaseSingle.Id);
        ccc.getContractItems();
        
        ccc.selAccountId = SCHelperTestClass.account.Id;
        ccc.selAccountOwnerId = SCHelperTestClass.account.Id;
        ccc.selAccountIRId = SCHelperTestClass.account.Id;
        ccc.getAccountIds();
        
        // Save new contract
        ccc.prices = '1111,99-' + String.valueOf(SCHelperTestClass.installedBaseSingle.Id);
        ccc.durations = ccc.prices = '10-' + String.valueOf(SCHelperTestClass.installedBaseSingle.Id);
        ccc.newSelectedLocations = String.valueOf(SCHelperTestClass.location.Id);
        ccc.createMaintenanceOrder = true;
        ccc.saveContract();

        ccc.cancel();
        
        // Wizard steps
        ccc.step1();
        ccc.step2();
        ccc.step3();
        ccc.step4();
        
    }
    
    
    static testMethod void testCodeCoverageA()
    {
        try { SCHelperTestClass3.createCustomSettings('DE',true); } catch(Exception e) {}
        try { SCHelperTestClass.createDomsForOrderCreation(); } catch(Exception e) {}
        
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        
        
        Test.startTest();
        
        
        try { SCContractCreationController.TemplateModule tmp = new SCContractCreationController.TemplateModule(true, true, null); } catch(Exception e) {}
        
        SCContractCreationController obj = new SCContractCreationController();
        
        try { obj.getTotalPrice(); } catch(Exception e) {}
        try { obj.changeNumberOfSteps(); } catch(Exception e) {}
        try { obj.reloadIBL(); } catch(Exception e) {}
        try { obj.reloadIBLPage(); } catch(Exception e) {}
        try { obj.getInstalledBasePriceBasedOnModule(-1, null, 0); } catch(Exception e) {}
        try { obj.getInstalledBasePriceBasedOnModule(1000, null, 0); } catch(Exception e) {}
        
        
        Test.stopTest();
    }
    
}
