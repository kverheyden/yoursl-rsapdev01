global class fsfaAlertBatchScheduler implements Schedulable {
	
	
	global void execute(SchedulableContext sc) 
	{
		fsfaAlertBatch batch = new fsfaAlertBatch();
		Id batchId = Database.executeBatch(batch);
	}

}
