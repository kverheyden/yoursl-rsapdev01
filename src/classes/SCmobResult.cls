/*
 * @(#)SCmobResult.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Simple container-class for serializing results before sending it
 * over the wire to the caller of a WebService. 
 * 
 * @author mautenrieth@gms-online.de
*/ 
public class SCmobResult
{

    /**
     * Status of the operation: success ('E000'), failure ('E101', 'E1002', ...), 
     */
    public String status {get; set;} 
    
    /**
     * Usable to send a user-friendly description of an error-status
     */
    public String message {get; set;}
    
    /**
     * Usable to piggyback additional data onto the Result.
     */
    public List<Object> data {get; set;}
        
    /**
     * Every Result should have at least the 'status' field set.
     */
    public SCmobResult(String status)
    {
        this.status = status;
        data = new List<Object>();
    }
    
    /**
     * Simple default constructor which initializes 'status' to success 'E000'
     */
    public SCmobResult()
    {
        this('E000');
    }
}
