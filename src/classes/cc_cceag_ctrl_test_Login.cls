/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public with sharing class cc_cceag_ctrl_test_Login {
	
	public static User getGuestUser() {
		User guest = null;
		List<User> guests = [SELECT FirstName, LastName FROM User WHERE UserType = 'Guest' and isActive=true];
     	if(guests != null && guests.size() > 0) {
     		guest = guests[0];
     	}
     	return guest;
 	}
	
	static testMethod void myUnitTest() {
	 	
	 	cc_cceag_ctrl_Login login = new cc_cceag_ctrl_Login();
	 	
	 	login.username = 'StandardTest@Test.com';
	 	login.password = 'TestPass';
	 	
	 	PageReference pRef = login.login();
	 	system.assert(pRef == null);
	 	
	 	pRef = login.forwardToCokeID();
	 	String landing = pRef.getParameters().get('pageKey');
	 	system.assert(landing == 'Landing');
	 	
        ecomSettings__c setting = new ecomSettings__c(Name='ECOMCokeIDLoginURL', value__c='test');
	 	insert setting;
	 	
	 	User u = getGuestUser(); 
	     
	     System.runAs(u){
	     	pRef = login.forwardToCokeID();
	     }
	     system.assert(pRef != null);
	     
    }
	
}
