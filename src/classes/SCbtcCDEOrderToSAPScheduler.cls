/**********************************************************************
 
Name: SCbtcCDEOrderToSAPScheduler

======================================================

Purpose: 

Initiate the transfer of SCOrders to SAP (Only SCOrders that were generated out of CDEOrders).
Used template: SCbtcYourSLOrderToSAP

======================================================

History 

------- 

Date 			AUTHOR 				DETAIL

08/12/2014 		Oliver Preuschl 	INITIAL DEVELOPMENT

***********************************************************************/

global class SCbtcCDEOrderToSAPScheduler implements Schedulable {
	global void execute( SchedulableContext LO_Context ) {
		SCbtcCDEOrderToSAP.asyncTransferAll( 0, 'trace' );
	}
}
