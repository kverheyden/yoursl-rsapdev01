/**
* @author		Austen Buennemann
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				a.buennemann@yoursl.de
*
* @description	Unit Test for CCWCCustomerRegistrationHandlerTest
*
* @date			18.11.2014
*
* Timeline:
* Name               DateTime                  Description
* Austen Buennemann  18.11.2014 16:38          Class created
*/

@isTest
private class CCWCCustomerRegistrationHandlerTest {

    static testMethod void WrapperTest() {
        System.assertNotEquals(null,new piCceagDeSfdc_CustomerRegistration.SFDCResponse_element());
        System.assertNotEquals(null,new piCceagDeSfdc_CustomerRegistration.CC_CustomerRegistration());
        System.assertNotEquals(null,new piCceagDeSfdc_CustomerRegistration.HTTPS_Port());        
    }    
    
    static testMethod void myUnitTest() {
        CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        appSettings = SCApplicationSettings__c.getInstance();
        
        CCSettings__c ccSettings = new CCSettings__c();
        ccSettings.SAPDispBasicAuthUsername__c = 'user';
        ccSettings.SAPDispBasicAuthPassword__c = 'passsword';
        ccSettings.SAPDispEndpointCustomerRegistration__c = 'CustomerRegistration';
        ccSettings.SAPDispServer__c = 'Server';
        insert ccSettings;
        
        Account soldTo = new Account();
        soldTo.Name = 'SoldTo';
        insert soldTo;
        
        Account shipTo = new Account();
        shipTo.Name = 'ShipTo';
        insert shipTo;
        
        // add WE role
        CCAccountRole__c roleWE = new CCAccountRole__c();
        roleWE.AccountRole__c = 'WE';
        roleWE.SlaveAccount__c = shipTo.Id;
        roleWE.MasterAccount__c = soldTo.Id;
        insert roleWE;
        
        // add AG role
        CCAccountRole__c roleAG = new CCAccountRole__c();
        roleAG.AccountRole__c = 'AG';
        roleAG.SlaveAccount__c = soldTo.Id;
        roleAG.MasterAccount__c = soldTo.Id;
        insert roleAG;
        
        List<CokeIDRequest__c> reqs = new List<CokeIDRequest__c>();
        
        // add Coke ID Requests
        CokeIDRequest__c req = new CokeIDRequest__c();
        req.Account__c = soldTo.Id;
        req.CokeIdUserId__c = '1234';
        req.Status__c = 'new';
		insert req;
		req.Status__c = 'created';
		update req;
		reqs.add(req);
        
        CokeIDRequest__c req2 = new CokeIDRequest__c();
        req2.Account__c = shipTo.Id;
        req2.CokeIdUserId__c = '1234';
        req2.Status__c = 'new';
		insert req2;     
		req2.Status__c = 'created';
		update req2;
		reqs.add(req2);
		
		CCWCCustomerRegistrationHandler send = new CCWCCustomerRegistrationHandler();
		send.prepareRegistration(reqs);  		
		send.callout(req.Id, false);
    }
}
