/**
* @author		Development (AB)
* 				H&W Consult GmbH
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				http://www.hundw.com
*
* @description	REST Webservice for ProductList
*
* @date			18.12.2013
*
* /services/apexrest/ProductList 
*/

@RestResource(urlMapping='/ProductList/*')
global with sharing class CCWCRestProductList {
	
	global class Products {
		public ProductEntry ProductEntry;
		public Products(ProductEntry ProductEntry){
			this.ProductEntry = ProductEntry;
		}
	}
	
    global class ProductEntry {
		public List<PricebookEntryDetail> PricebookEntryDetail;    	
		public String ProductNumber; 
		public String ProductName; 		
		public List<Listing> Listing;
        public ProductEntry(List<PricebookEntryDetail> PricebookEntryDetail, String ProductNumber, String ProductName, List<Listing> Listing) {
            this.PricebookEntryDetail = PricebookEntryDetail;        	
            this.ProductNumber = ProductNumber;            
            this.ProductName = ProductName;
            this.Listing = Listing;
        }
    }
    
    global class Listing {
		public String PricebookName; 

        public Listing(String PricebookName) {
            this.PricebookName = PricebookName;
        }
    }    

    global class PricebookEntryDetail {
		public String ValidTo;     	
		public String ValidFrom;
		public String PricebookName; 
		public String ListingIndicator;		
		public String LastModifiedDate;
		public String DeliveryType; 		
		public String ID2;
		public String AccountName;
		public String AccountID2;

        public PricebookEntryDetail(String ValidTo, String ValidFrom, String PricebookName, String ListingIndicator, 
        											String LastModifiedDate, String DeliveryType, String ID2, String AccountName, String AccountID2) {
                  
			this.ValidTo = ValidTo;     	
			this.ValidFrom = ValidFrom;
			this.PricebookName = PricebookName; 
			this.ListingIndicator = ListingIndicator;		
			this.LastModifiedDate = LastModifiedDate;   
			this.DeliveryType = DeliveryType; 		
			this.ID2 = ID2;
			this.AccountName = AccountName;
			this.AccountID2 = AccountID2;
        }
    }    

    @HttpGet
    global static List<Products> doGet() { 
    	
    	RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String Id2 = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        String rolelabel = Label.CCWCRestProductList_AccountRole;
        String statuslabel = Label.CCWCRestProductList_Status;
    	
    	Account account = [Select Id, Name, ID2__c, Pricebook__c From Account WHERE ID2__c =: Id2 LIMIT 1];
    	List<CCAccountRole__c> accountroles = [Select SlaveAccountPricebookId__c, SlaveAccount__c, MasterAccount__c, Status__c, AccountRole__c From CCAccountRole__c 
    											WHERE AccountRole__c =: rolelabel AND Status__c =: statuslabel AND MasterAccount__c =: account.Id];
    	Set<Id> pricebookIDs = new Set<Id>();
    	Set<Id> relaccountIDs = new Set<Id>();
    	for (CCAccountRole__c role : accountroles){
    		pricebookIDs.add(role.SlaveAccountPricebookId__c);
    		relaccountIDs.add(role.SlaveAccount__c);
    		if(account != null && account.Pricebook__c != null){
    			pricebookIDs.add(account.Pricebook__c);
    			relaccountIDs.add(account.Id);
    		}
    	}
    			
		List<PricebookEntryDetail__c> pricebookentry = [Select ValidUntil__c, ValidFrom__c, Product2__c, Pricebook2__c, ListingIndicator__c, 
												LastModifiedDate, ID2__c, DeliveryType__c From PricebookEntryDetail__c  
												Where Pricebook2__c =: pricebookIDs];											
    	
    	Set<Id> productIDs = new Set<Id>();
    	for (PricebookEntryDetail__c entry : pricebookentry){
    		productIDs.add(entry.Product2__c);
    	}
    	
    	List<Product2> products2 = [Select ProductCode, Name, LastModifiedDate, Id From Product2 WHERE Id =: productIDs];

    	Map<Id, Pricebook2> bookmap = new Map<Id, Pricebook2>([Select Id, Name, ID2__c From Pricebook2 WHERE Id =: pricebookIDs]);
    	Map<Id, Account> accmap = new Map<Id, Account>();
    	List<Account> accounts = [Select Pricebook__c, Name, Id , ID2__c From Account WHERE Pricebook__c =: pricebookIDs AND Id =: relaccountIDs];
    	for(Account acc : accounts){
    		accmap.put(acc.Pricebook__c, acc);
    	}
    	
        List<Products> products = new List<Products>();  
        List<PricebookEntryDetail> pricebooks; 
        PricebookEntryDetail PricebookEntryDetail;
        List<Listing> listings;
        Listing listing;
        ProductEntry ProductEntry;
        Products product;
		Pricebook2 pbook;    
	    Account relaccount;        
	   	// add product	
	    for (Product2 product2 : products2){
	    	if(product2 != null){     
		        Set<String> chkID = new Set<String>();
		        pricebooks = new List<PricebookEntryDetail>();
		        listings = new  List<Listing>();
		        for (PricebookEntryDetail__c entry : pricebookentry){
		        	if(entry != null){
		        		// add pricebook list
		        		pbook = bookmap.get(entry.Pricebook2__c);
		        		relaccount = accmap.get(entry.Pricebook2__c);
		        		if(chkID.contains(relaccount.Id + '|' +  pbook.Id) == false && product2.Id == entry.Product2__c){
			        		PricebookEntryDetail = new PricebookEntryDetail(string.valueOf(entry.ValidUntil__c), string.valueOf(entry.ValidFrom__c), pbook.Name, 
			        									entry.ListingIndicator__c, string.valueOf(entry.LastModifiedDate), entry.DeliveryType__c, entry.ID2__c, relaccount.Name, relaccount.ID2__c);
							pricebooks.add(PricebookEntryDetail);
							
							listing = new Listing(bookmap.get(entry.Pricebook2__c).Name);
							listings.add(listing);
	    					chkID.add(relaccount.Id + '|' +  pbook.Id);							
		        		}
		        	}
		        }
		        // add product items
		        if(account != null && account.Pricebook__c != null){
    				pbook = bookmap.get(account.Pricebook__c);
    			}
    			//add listing items
		        ProductEntry = new ProductEntry(pricebooks, product2.ProductCode, product2.Name, listings);
	    	}
	        product = new Products(ProductEntry);   
	        products.add(product);
    	}
         return products;
    }
}
