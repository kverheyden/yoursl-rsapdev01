/*
 * @(#)SCMapViewMarker.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Class to collect google marker information
 *
 * @author Eugen Tiessen <etiessen@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCMapViewMarker {
        
    /**
     * Static color and icon constants
     */
    public static final String ORDER_ICON = 'books';
    public static final String ORDER_ICON_COLOR = '#FFFF00';
    public static final String ENGINEER_ICON = 'wc-male';
    public static final String ENGINEER_ICON_COLOR = '#00AAAA';
    public static final String APPOINTMENT_PROPOSAL_ICON = 'A';
    public static final String APPOINTMENT_PROPOSAL_ICON_COLOR = '#FF9900';
    public static final String DEFAULT_COLOR = '#FF0000';
    public static final String CONTEXT_COLOR = '#00FF00';
    
    /**
    * Static marker type constants
    */
    public static final String OBJECT_TYPE_ENGINEER = 'ENGINEER';
    public static final String OBJECT_TYPE_TOUR_ENGINEER = 'TOUR_ENGINEER';
    public static final String OBJECT_TYPE_ORDER = 'ORDER';
    public static final String OBJECT_TYPE_ORDER_COMPLETED = 'ORDER_COMPLETED';
    public static final String OBJECT_TYPE_TOUR_ORDER = 'TOUR_ORDER';
	
	/**
    * Static drag n drop validation constants
    */   
    public static final List<String> OBJECT_TYPE_ENGINEER_VALID_DROPS = new List<String>
    {
    	OBJECT_TYPE_ORDER,
    	OBJECT_TYPE_TOUR_ORDER
    };
    public static final List<String> OBJECT_TYPE_TOUR_ENGINEER_VALID_DROPS = new List<String>
    {
    	OBJECT_TYPE_ORDER,
    	OBJECT_TYPE_TOUR_ORDER
    };
    public static final List<String> OBJECT_TYPE_ORDER_VALID_DROPS = new List<String>
    {
    	OBJECT_TYPE_ENGINEER
    };
    public static final List<String> OBJECT_TYPE_TOUR_ORDER_VALID_DROPS = new List<String>
    {
    	OBJECT_TYPE_ENGINEER,
    	OBJECT_TYPE_ORDER,
    	OBJECT_TYPE_TOUR_ORDER
    };
    public static final List<String> OBJECT_TYPE_NO_VALID_DROPS = new List<String>();
    
    /**
    * Static context menu items for drag n drop constants
    * String Array [eventName,label]
    */
    public static final String[] MENU_ITEM_DRAGDROP_SEPARATOR = new String[] {'separator',''};
    
    public static final String[] MENU_ITEM_DRAGDROP_DISPATCH_ENGINEER_ORDER = new String[] { 'dispatch_engineer_order',Label.SC_btn_Dispatch};
    
    public static final String[] MENU_ITEM_DRAGDROP_DISPATCH_ORDER_ENGINEER = new String[] { 'dispatch_order_engineer',Label.SC_btn_Dispatch};
    
    
    
    public static final String[] MENU_ITEM_DRAGDROP_APPOINTMENTS_ENGINEER_ORDER = new String[] { 'appointments_engineer_order',Label.SC_btn_GetAppointments};
    
    public static final String[] MENU_ITEM_DRAGDROP_APPOINTMENTS_ORDER_ENGINEER = new String[] { 'appointments_order_engineer',Label.SC_btn_GetAppointments};
    
    
    public static final String[] MENU_ITEM_DRAGDROP_ENGINEER_ASSIGN_TO_ORDER = new String[] { 'engineer_add_to_order',Label.SC_btn_AssignEngineer};
    
    public static final String[] MENU_ITEM_DRAGDROP_TOUR_ORDER_REASSIGN_TO_ENGINEER = new String[] { 'tour_order_reassign_to_engineer',Label.SC_btn_ReassignToThisEngineer};
    //IDEA concat label (clientside) with order name e.g. "Before ORD-0002490254"
    //maybe with a placeholder like "%1"
    
    
    public static final String[] MENU_ITEM_DRAGDROP_ORDER_ASSIGN_BEFORE = new String[] { 'order_before',Label.SC_btn_Before};
    
    public static final String[] MENU_ITEM_DRAGDROP_ORDER_ASSIGN_AFTER = new String[] { 'order_after',Label.SC_btn_After};
    
    public static final String[] MENU_ITEM_DRAGDROP_ORDER_ASSIGN_TO_TOUR = new String[] { 'order_assign_to_tour',Label.SC_btn_AssignToThisTour};
    
    public static final String[] MENU_ITEM_DRAGDROP_ORDER_ASSIGN_TO_TOUR_ORDER = new String[] { 'order_assign_to_tour_order',Label.SC_btn_AssignToThisTour};
    
    public static final String[] MENU_ITEM_DRAGDROP_TOUR_ORDER_REASSIGN_BEFORE = new String[] { 'tour_order_reassign_before',Label.SC_btn_Before};
    
    public static final String[] MENU_ITEM_DRAGDROP_TOUR_ORDER_REASSIGN_AFTER = new String[] { 'tour_order_reassign_after',Label.SC_btn_After};
    
    public static final String[] MENU_ITEM_DRAGDROP_TOUR_ORDER_REASSIGN_TO_TOUR = new String[] { 'tour_order_reassign_to_tour',Label.SC_btn_ReassignToThisTour};
    
    public static final String[] MENU_ITEM_DRAGDROP_TOUR_ORDER_REASSIGN_TO_TOUR_ORDER = new String[] { 'tour_order_reassign_to_tour_order',Label.SC_btn_ReassignToThisTour};
    /*public static final String[] MENU_ITEM_DRAGDROP_ = new String[] { '',''};
    public static final String[] MENU_ITEM_DRAGDROP_ = new String[] { '',''};
    public static final String[] MENU_ITEM_DRAGDROP_ = new String[] { '',''};
    */
    
    /**
    * Static context menu entries for drag n drop constants
    */
    public static final List<String[]> MENU_ITEM_DRAGDROP_GROUP_ENGINEER_ORDER = new List<String[]>
    {
    	MENU_ITEM_DRAGDROP_DISPATCH_ENGINEER_ORDER,
    	MENU_ITEM_DRAGDROP_APPOINTMENTS_ENGINEER_ORDER
    };
    public static final List<String[]> MENU_ITEM_DRAGDROP_GROUP_ORDER_ENGINEER = new List<String[]>
    {
    	MENU_ITEM_DRAGDROP_DISPATCH_ORDER_ENGINEER,
    	MENU_ITEM_DRAGDROP_APPOINTMENTS_ORDER_ENGINEER
    };
    
    public static final List<String[]> MENU_ITEM_DRAGDROP_GROUP_ENGINEER_TOUR_ORDER = new List<String[]>
    {
    	//MENU_ITEM_DRAGDROP_ENGINEER_ASSIGN_TO_ORDER
    	MENU_ITEM_DRAGDROP_DISPATCH_ENGINEER_ORDER,
    	MENU_ITEM_DRAGDROP_APPOINTMENTS_ENGINEER_ORDER
    };
    public static final List<String[]> MENU_ITEM_DRAGDROP_GROUP_TOUR_ORDER_ENGINEER = new List<String[]>
    {
    	MENU_ITEM_DRAGDROP_TOUR_ORDER_REASSIGN_TO_ENGINEER
    };
    public static final List<String[]> MENU_ITEM_DRAGDROP_GROUP_ORDER_TOUR_ORDER = new List<String[]>
    {
    	//will be used later
    	//MENU_ITEM_DRAGDROP_ORDER_ASSIGN_BEFORE,
    	//MENU_ITEM_DRAGDROP_ORDER_ASSIGN_AFTER,
    	MENU_ITEM_DRAGDROP_ORDER_ASSIGN_TO_TOUR_ORDER
    };
    public static final List<String[]> MENU_ITEM_DRAGDROP_GROUP_ORDER_TOUR_ENGINEER = new List<String[]>
    {
    	MENU_ITEM_DRAGDROP_ORDER_ASSIGN_TO_TOUR
    };
    public static final List<String[]> MENU_ITEM_DRAGDROP_GROUP_TOUR_ORDER_TOUR_ORDER = new List<String[]>
    {
    	//will be used later
    	//MENU_ITEM_DRAGDROP_TOUR_ORDER_REASSIGN_BEFORE,
    	//MENU_ITEM_DRAGDROP_TOUR_ORDER_REASSIGN_AFTER,
    	MENU_ITEM_DRAGDROP_TOUR_ORDER_REASSIGN_TO_TOUR_ORDER
    };
    public static final List<String[]> MENU_ITEM_DRAGDROP_GROUP_TOUR_ORDER_TOUR_ENGINEER = new List<String[]>
    {
    	MENU_ITEM_DRAGDROP_TOUR_ORDER_REASSIGN_TO_TOUR
    };
    
    /**
    * Static context menu items constants
    * String Array [eventName,label]
    */
    public static final String[] MENU_ITEM_SEPARATOR = new String[] {'separator',''};
   	
    public static final String[] MENU_ITEM_GET_APPOINTMENTS = new String[] { 'get_appointments',Label.SC_btn_GetAppointments};
    
    public static final String[] MENU_ITEM_CANCEL_APPOINTMENT = new String[] { 'cancel_appointment','Cancel Appointment'};//Label.SC_btn_CancelAppointment};
    
    public static final String[] MENU_ITEM_CANCEL_APPOINTMENTS = new String[] { 'cancel_appointments',Label.SC_btn_CancelAppointments};
    
    public static final String[] MENU_ITEM_AUTO_DISPATCH = new String[] { 'auto_dispatch',Label.SC_btn_AutoDispatch};
    
    public static final String[] MENU_ITEM_SHOW_PLANNINGBOARD = new String[] { 'show_planningboard',Label.SC_btn_ShowInPlanningboard};
    
    public static final String[] MENU_ITEM_SHOW_PLANNINGBOARD_ORDER = new String[] { 'show_planningboard_order',Label.SC_btn_ShowInPlanningboard};
    
    public static final String[] MENU_ITEM_SHOW_PLANNINGBOARD_ENGINEER = new String[] { 'show_planningboard_engineer',Label.SC_btn_ShowInPlanningboard};
    
    public static final String[] MENU_ITEM_SHOW_ORDER = new String[] { 'show_order',Label.SC_btn_ShowOrder};
    
    public static final String[] MENU_ITEM_SHOW_ENGINEER = new String[] {'show_engineer',Label.SC_btn_ShowEngineer};
    
    public static final String[] MENU_ITEM_SHOW_AS_MIDPOINT = new String[] {'show_as_midpoint',Label.SC_btn_ShowAsMidpoint};
    
    public static final String[] MENU_ITEM_SHOW_STREET_VIEW = new String[] {'show_street_view',Label.SC_btn_StreetView};
    
    public static final String[] MENU_ITEM_SHOW_TOUR = new String[] {'show_tour',Label.SC_btn_ShowTour};
    
    /**
    * Static context menu entries constants
    */
    public static final List<String[]> MENU_ITEM_GROUP_OPEN_ORDER = new List<String[]>
    {
    	MENU_ITEM_SHOW_AS_MIDPOINT,
    	MENU_ITEM_SHOW_STREET_VIEW,
    	MENU_ITEM_SHOW_ORDER,
    	MENU_ITEM_SHOW_PLANNINGBOARD_ORDER,
    	MENU_ITEM_SEPARATOR,
    	MENU_ITEM_GET_APPOINTMENTS,
    	MENU_ITEM_AUTO_DISPATCH
    	
    };
    public static final List<String[]> MENU_ITEM_GROUP_DISPATCHED_ORDER = new List<String[]>
    {
    	MENU_ITEM_SHOW_AS_MIDPOINT,
    	MENU_ITEM_SHOW_TOUR,
    	MENU_ITEM_SHOW_STREET_VIEW,
    	MENU_ITEM_SHOW_ORDER,
    	MENU_ITEM_SHOW_PLANNINGBOARD_ORDER,
    	MENU_ITEM_SEPARATOR,
    	//MENU_ITEM_CANCEL_APPOINTMENTS
    	MENU_ITEM_CANCEL_APPOINTMENT
    };
    public static final List<String[]> MENU_ITEM_GROUP_ENGINEER = new List<String[]>
    {
    	MENU_ITEM_SHOW_AS_MIDPOINT,
    	MENU_ITEM_SHOW_STREET_VIEW,
    	MENU_ITEM_SHOW_ENGINEER,
    	MENU_ITEM_SHOW_PLANNINGBOARD_ENGINEER
    };
    
    public static final List<String[]> MENU_ITEM_GROUP_TOUR_ENGINEER = new List<String[]>
    {
    	MENU_ITEM_SHOW_AS_MIDPOINT,
    	MENU_ITEM_SHOW_TOUR,
    	MENU_ITEM_SHOW_STREET_VIEW,
    	MENU_ITEM_SHOW_ENGINEER,
    	MENU_ITEM_SHOW_PLANNINGBOARD_ENGINEER
    	
    };
    
    /**
     * Static hex values constant
     */
    static final String[] hexCode = new String[]{'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
    
    static Integer lastColor = 0;
    
     
    
    public Point point {get; set;}
    public String content {get; set;}
    public String icon {get; set;}
    public String color {get; set;}
    public Integer index {get; set;}
    public Boolean highlighting {get; set;}
    public String textIcon {get;set;}
    public List<String[]> contextMenuItems {get;set;}
    
    public List<String> validDrops {get;private set;}
    public Map<String,List<String[]>> dynamicContextMenuMap {get; private set;}
    public Boolean draggable {get; private set;}
    public String tourDate {get; set;}
    
    public Id objectId {get;set;}
    public String objectName {get; set;}
    public Id objectTourResourceId {get; set;}
    public Id objectTourAppointmentId {get; set;}
    public String objectTourResourceName {get; set;}
    public String objectType 
    {
    	get; 
    	set
    	{
    		this.dynamicContextMenuMap = new Map<String,List<String[]>>();
    		
    		if(value == OBJECT_TYPE_ENGINEER)
    		{
    			this.draggable = true;
    			this.validDrops = OBJECT_TYPE_ENGINEER_VALID_DROPS;
    			
    			this.dynamicContextMenuMap.put(OBJECT_TYPE_ORDER, MENU_ITEM_DRAGDROP_GROUP_ORDER_ENGINEER);
    			this.dynamicContextMenuMap.put(OBJECT_TYPE_TOUR_ORDER, MENU_ITEM_DRAGDROP_GROUP_TOUR_ORDER_ENGINEER);
    		}
    		else if(value == OBJECT_TYPE_TOUR_ENGINEER)
    		{
    			this.draggable = false;
    			this.validDrops = OBJECT_TYPE_TOUR_ENGINEER_VALID_DROPS;
    			
    			this.dynamicContextMenuMap.put(OBJECT_TYPE_ORDER, MENU_ITEM_DRAGDROP_GROUP_ORDER_TOUR_ENGINEER);
    			this.dynamicContextMenuMap.put(OBJECT_TYPE_TOUR_ORDER, MENU_ITEM_DRAGDROP_GROUP_TOUR_ORDER_TOUR_ENGINEER);
    			
    		}
    		else if(value == OBJECT_TYPE_ORDER)
    		{
    			this.draggable = true;
    			this.validDrops = OBJECT_TYPE_ORDER_VALID_DROPS;
    			
    			this.dynamicContextMenuMap.put(OBJECT_TYPE_ENGINEER, MENU_ITEM_DRAGDROP_GROUP_ENGINEER_ORDER);
     			
    		}
    		else if(value == OBJECT_TYPE_TOUR_ORDER)
    		{
    			this.draggable = true;
    			this.validDrops = OBJECT_TYPE_TOUR_ORDER_VALID_DROPS;
    			
    			this.dynamicContextMenuMap.put(OBJECT_TYPE_ENGINEER, MENU_ITEM_DRAGDROP_GROUP_ENGINEER_TOUR_ORDER);
    			this.dynamicContextMenuMap.put(OBJECT_TYPE_ORDER, MENU_ITEM_DRAGDROP_GROUP_ORDER_TOUR_ORDER);
    			this.dynamicContextMenuMap.put(OBJECT_TYPE_TOUR_ORDER, MENU_ITEM_DRAGDROP_GROUP_TOUR_ORDER_TOUR_ORDER);
    		}
    		else if(value == OBJECT_TYPE_ORDER_COMPLETED)
    		{
    			this.draggable = false;
    			this.validDrops = OBJECT_TYPE_TOUR_ORDER_VALID_DROPS;
    			
    		}
    		else
    		{
    			this.draggable = false;
    			this.validDrops = OBJECT_TYPE_NO_VALID_DROPS;
    			
    		}
    		this.objectType = value;
    	}
    }
    
    
    
    public SCMapViewMarker(Point pnt, String cont){
        this.point=pnt;
        this.content=cont;
        this.color=DEFAULT_COLOR;
        this.objectType = 'OBJECT';
        //this.icon=icon;
        
    }
    
    public SCMapViewMarker(Point pnt, String cont, String icon, String color){
        this.point=pnt;
        this.content=cont;
        this.color=color;
        this.icon=icon;
        this.objectType = 'OBJECT';
    }
    
    public SCMapViewMarker(Double lat,Double lng, String cont, String icon, String color){
        this.point=new Point(lat, lng);
        this.content=cont;
        this.color=color;
        this.icon=icon;
        this.objectType = 'OBJECT';
    }
    
    public SCMapViewMarker(Double lat,Double lng, String cont, String icon, Boolean autoColor){
        this.point=new Point(lat, lng);
        this.content=cont;
        this.color=getNextColor();
        this.icon=icon;
        this.objectType = 'OBJECT';
    }
    
    private String getNextColor(){
        lastColor += 13;
        String myColor='#';
        for(Integer i=0; i<3; i++ )
        {
            
            Integer state = Math.mod(lastColor + i,3);
            if(state == 0)
            {
                myColor += '00';
                continue;
            }
            else if(state == 1)
            {
                //if(Math.mod(lastColor + i,6)==1)
                if(i == 1)
                {
                    myColor += '7F';
                }
                else
                {
                    myColor += 'FF';
                }
                
                continue;
            }
                
            //linear part
            //myColor += hexCode[(Integer)(Math.mod( lastColor,16)) ];
            
            //generic part
            //myColor += hexCode[(Integer)(Math.random()*16)];
            //myColor += hexCode[(Integer)(Math.random()*16)];
            myColor += hexCode[(lastColor & 240) >> 4];
            myColor += hexCode[lastColor & 15];
        }
        return myColor;
        //return '#' + ((Integer)(Math.random()*100)) + ((Integer)(Math.random()*100)) + ((Integer)(Math.random()*100));
        //return '#' + (36+(Integer)(lastColor*Math.random()) & 63) + (36+(Integer)(lastColor*Math.random()) & 63) + (36+(Integer)(lastColor*Math.random()) & 63);
        //return '#'+Math.mod(99,lastColor)+Math.mod(99,lastColor)+Math.mod(99,lastColor);
    }
    
    public class Point{
        /*public Double Qa {get; set;}
        public Double Ra {get; set;}
        */
        public Double Y {get; set;}
        public Double X {get; set;}
        
        public Point(Double lat, Double lng){
            //latitude
            //Qa=lat;
            Y=lat;
            //longitude
            //Ra=lng;
            X=lng;
        }
        
        public String asString()
        {
            //return Qa + ',' + Ra;
            return Y + ',' + X;
            
        }
    
    }

    public class LocationPoint{
        public Point location {get;set;}
        
        public LocationPoint(Point loc){
            location=loc;
        }
    }   

    
}
