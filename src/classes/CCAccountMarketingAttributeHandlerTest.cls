/**
* @author		Austen Buennemann
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
*				a.buennemann@yoursl.de
*
* @description	Unit Test for AccountRelationOfRequestChields
*
* @date			15.08.2014
*
* Timeline:
* Name               DateTime                  Version         Description
* Austen Buennemann  15.08.2014 11:36          *1.0*           created the class
*/

@isTest 
public with sharing class CCAccountMarketingAttributeHandlerTest {
	
	 static testMethod void myUnitTest() {
		
		// create MarketingAttribute
		MarketingAttribute__c tmpMA1 = new MarketingAttribute__c();
		tmpMA1.UniqueKey__c = 'tmpMA1';
		insert tmpMA1;		
 
        // create Account
        Account tmpAcc = new Account(); 
        tmpAcc.Name	= 'Test Account';
        tmpAcc.ID2__c = '12345';
        tmpAcc.BillingPostalCode = '33106'; 
        tmpAcc.BillingCountry__c = 'DE';
        tmpAcc.CurrencyIsoCode = 'EUR';
        tmpAcc.BillingStreet = 'Karl-Schurz Str.';
        tmpAcc.BillingHouseNo__c = '29';
        tmpAcc.BillingCity = 'Padebrorn';
        insert tmpAcc;
        
        //create AccountMarketingAttribute__c
		AccountMarketingAttribute__c att = new AccountMarketingAttribute__c();
		att.UniqueKey__c = '##Y2A1##Y2A1003';
		att.ToDelete__c = false;
		att.MarketingAttribute__c = tmpMA1.Id;
		att.Account__c = tmpAcc.Id;
		insert att;
		
		//update AccountMarketingAttribute__c
		AccountMarketingAttribute__c attUpdate = [Select Id From AccountMarketingAttribute__c Where Id =: att.Id];
		attUpdate.ToDelete__c = true;
		update attUpdate;
		
    	List<AccountMarketingAttribute__c> attribs = new List<AccountMarketingAttribute__c>();    	
    	CCAccountMarketingAttributeHandler handler = new CCAccountMarketingAttributeHandler();
    	attribs.add(att);
    	handler.deleteMarketingAttribute(attribs);		
		
	}

}
