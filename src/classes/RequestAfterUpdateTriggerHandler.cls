public with sharing class RequestAfterUpdateTriggerHandler extends TriggerHandlerBase{
	
	public override void mainEntry(TriggerParameters tp) {
		CCWCCustomerCreateHandler customerCreateHandler = new CCWCCustomerCreateHandler();
		List<Request__c> requestsToSend = new List<Request__c>();
		for (SObject o : tp.newList) {
			Request__c request = (Request__c)o;
			if (request.Send_to_SAP__c)
				requestsToSend.add(request);
		}
		if (!requestsToSend.isEmpty())
			customerCreateHandler.sendCallout(requestsToSend, false, false);
	}
}
