/*
* @(#)SCVPProductsControllerTest.cls
* 
* Copyright 2014 by GMS Development GmbH, Karl-Schurz-Strasse 29, 
* DE-33100 Paderborn. All rights reserved.
*
* This software is the confidential and proprietary information
* of GMS Development GmbH. ("Confidential Information").  You
* shall not disclose such Confidential Information and shall use
* it only in accordance with the terms of the license agreement
* you entered into with GMS.
* 
* # Test class for 'SCVPProductsController.cls'.
*
* @author Marc Sälzler <msaelzler@gms-online.de>
*
* @history  
* 2014-11-07 GMSmae created
* 2014-11-20 GMSmae added another test method, to cover the opposites. (reached 100%)
* 
* @review
*
*/

@isTest
public class SCVPProductsControllerTest
{
    public static SCVendor__c t_vendor
    {
        get
        {
            if(t_vendor == null)
            {
                t_vendor  = new SCVendor__c(
                    Name            = '0123456789',
                    Name1__c        = 'My test t_vendor',
                    Country__c      = 'DE',
                    PostalCode__c   = '33100',
                    City__c         = 'Paderborn',
                    Street__c       = 'Bahnhofstrt',
                    LockType__c     = '5001',
                    Status__c       = 'Active'
                );
                
                insert t_vendor;
                
                System.assert(t_vendor.Id != null);
            }
            
            return t_vendor;
        }
        
        set;
    }
    
    public static Brand__c t_brand
    {
        get
        {
            if(t_brand == null)
            {
                t_brand  = new Brand__c(
                    Name = 'GMS Test 1',
                    ID2__c = 'GMS_Test' + String.valueOf(DateTime.Now())
                );
                
                insert t_brand;
                
                System.assert(t_brand.Id != null);
            }
            
            return t_brand;
        }
        
        set;
    }
    
    public static SCPlant__c t_plant
    {
        get
        {
            if (t_plant == null)
            {
                t_plant = new SCPlant__c(
                    Name = '9100',
                    Info__c = 'Test plant'
                );
                
                insert t_plant;
                
                System.assert(t_plant.Id != null);
            }
            
            return t_plant;
        }
        
        set;
    }
    
    public static SCStock__c t_stock
    {
        get
        {
            if (t_stock == null)
            {
                t_stock = new SCStock__c(
                    Name        = '9101',
                    Info__c     = 'Test stock 1',
                    Plant__c    = t_plant.Id,
                    Vendor__c   = t_vendor.Id,
                    Type__c     = 'EQReadyToMarket'
                );
                
                insert t_stock;
                
                System.assert(t_stock.Id != null);
            }
            
            return t_stock;
        }
        
        set;
    }
        
    
    public static SCProductModel__c[] t_prodModels
    {
        get
        {
            if(t_prodModels == null)
            {
                t_prodModels = new SCProductModel__c[]{};
                
                for(Integer i = 0; i < 25; i++)
                {
                    SCProductModel__c tmp_prodModel = new SCProductModel__c(
                        Brand__c        = t_brand.Id,
                        Name            = 'VAM climaVAIR [' + i + ']',
                        Country__c      = 'NL',
                        UnitClass__c    = SCfwConstants.DOMVAL_PRODUCTUNITCLASS_STANDARD,
                        UnitType__c     = SCfwConstants.DOMVAL_PRODUCTUNITTYPE_DEFAULT,
                        Group__c        = String.valueOf(i),
                        Power__c        = '550'
                    );
                    
                    t_prodModels.add(tmp_prodModel);
                }
                
                insert t_prodModels;
                
                System.assert(!t_prodModels.isEmpty());
            }
            
            return t_prodModels;
        }
        
        set;
    }
    
    public static SCArticle__c t_article
    {
        get
        {
            if(t_article == null)
            {
                t_article = new SCArticle__c(
                    Name        = 'TEST-000251',
                    Text_en__c  = 'Unit Test Article',
                    TaxType__c  = SCfwConstants.DOMVAL_TAXARTICLE_FULL,
                    Class__c    = SCfwConstants.DOMVAL_ARTICLECLASS_PRODUCT,
                    
                    AvailabilityType__c = SCfwConstants.DOMVAL_MATERIALDISPO_A
                );
                
                insert t_article;
                
                System.assert(t_article.Id != null);
            }
            
            return t_article;
        }
        
        set;
    } 
    
    public static SCInstalledBaseLocation__c t_location
    {
        get
        {
            if(t_location == null)
            {
                t_location = new SCInstalledBaseLocation__c(
                    BuildingDate__c = (Date.today() - 1000),
                    Street__c = 'Moormanweg',
                    HouseNo__c = '6',
                    PostalCode__c = '9831 NK',
                    City__c = 'Aduard',
                    Country__c = 'NL',
                    GeoX__c = 6.454447,
                    GeoY__c = 53.253582,
                    LocName__c = 'Test Location',
                    Status__c = 'Active'
                );
                
                insert t_location;
                
                System.assert(t_location.Id != null);
            }
            
            return t_location;
        }
        
        set;
    }
    
    
    public static SCInstalledBase__c GetInstalledBase()
    {
        return (new SCInstalledBase__c(
            Article__c      = t_article.Id,
            Stock__c        = t_stock.Id,
            SerialNo__c     = '00305005',
            Status__c       = 'active',
            Type__c         = 'Appliance',
            ProductSkill__c = 'PS1',
            
            InstalledBaseLocation__c    = t_location.Id,
            InstalledBaseLocation__r    = t_location
        ));
    }
    
    /**
    * Test method to cover the usual process.
    * 
    */
    @isTest
    public static void SCVPProductsControllerTest()
    {
        Profile t_profile = [
            SELECT
                Id
            FROM
                Profile
            WHERE
                Name IN ('Systemadministrator', 'System Administrator')
            LIMIT 1
        ];
        
        System.assert(t_profile != null);
        System.assert(t_profile.Id != null);
        
        
        User t_user = new User(
            Alias = 'GMSTest1', 
            FirstName = 'GMS',
            LastName = 'Testuser 1', 
            Username = 'gmstest1@gms-online.de', 
            Email = 'gmstest1@gms-online.de', 
            CommunityNickname = 'gmstest1', 
            Street = 'Karl Schurz Str.',
            PostalCode = '33100',
            City = 'Paderborn',
            Country = 'DE',
            GEOX__c = -0.10345,
            GEOY__c = 51.49250,
            TimeZoneSidKey = 'Europe/Berlin', 
            LocaleSidKey = 'de_DE_EURO', 
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            
            ProfileId = t_profile.Id
        );
        
        insert t_user;
        
        System.assert(t_user.Id != null);
        
        
        SCVendorUser__c vendorUser = new SCVendorUser__c(
            User__c   = t_user.Id,
            Vendor__c = t_vendor.id
        );
        
        insert vendorUser;
        
        System.assert(vendorUser.Id != null);
        
        
        SCInstalledBase__c[] t_installedBases = new SCInstalledBase__c[]{};
        
        for (SCProductModel__c tmp_prodModel : t_prodModels)
        {
            SCInstalledBase__c tmp_installedBase = GetInstalledBase();
            
            tmp_installedBase.ProductModel__c = tmp_prodModel.Id;
            tmp_installedBase.Brand__c        = tmp_prodModel.Brand__c;
            tmp_installedBase.ProductGroup__c = tmp_prodModel.Group__c;
            tmp_installedBase.ProductPower__c = tmp_prodModel.Power__c;
            
            tmp_installedBase.ProductUnitClass__c         = tmp_prodModel.UnitClass__c;
            tmp_installedBase.ProductUnitType__c          = tmp_prodModel.UnitType__c;
            
            t_installedBases.add(tmp_installedBase);
        }
        
        insert t_installedBases;
        
        System.assert(!t_installedBases.isEmpty());
        System.assert(t_installedBases.size() > 1);
        
        
        Test.startTest();
        
        System.runAs(t_user) 
        {
            Test.setCurrentPage(Page.SCVPProducts);
            
            SCVPProductsController t_SCVPProductsController = new SCVPProductsController();
            
            System.assert(!t_SCVPProductsController.x_StockListForPage.isEmpty());
            
            t_SCVPProductsController.x_StockListForPage.get(0).showEquipmentForThisStock();
            
            
            t_SCVPProductsController.RefreshEquipment();
            
            
            t_SCVPProductsController.filterQuery = true;
            
            t_SCVPProductsController.RefreshEquipment();
            
            System.assert(t_SCVPProductsController.x_PageCounter == 0);
            
            System.assert(t_SCVPProductsController.getIntReturnListSize() > 0);
            
            System.Debug('#t_SCVPProductsController.getIntReturnListSize(): ' + t_SCVPProductsController.getIntReturnListSize());
            
            
            t_SCVPProductsController.Next();
            
            System.assert(t_SCVPProductsController.x_PageCounter == 1);
            
            
            t_SCVPProductsController.Prev();
            
            System.assert(t_SCVPProductsController.x_PageCounter == 0);
            
            
            t_SCVPProductsController.LastPage();
            
            System.assert(t_SCVPProductsController.x_PageCounter == t_SCVPProductsController.getIntReturnListSize() - 1);
            
            
            t_SCVPProductsController.FirstPage();
            
            System.assert(t_SCVPProductsController.x_PageCounter == 0);
            
            
            t_SCVPProductsController.x_PageCounterInput = 1;
            
            t_SCVPProductsController.setPage();
            
            System.assert(t_SCVPProductsController.x_PageCounter == 0);
            
            
            t_SCVPProductsController.isVendor = false;
            t_SCVPProductsController.filterQuery = true;
            
            t_SCVPProductsController.RefreshEquipment();
            
            
            t_SCVPProductsController.assignFilter(
                new SCSearchFilter__c(
                    Filter__c = '{' +
                        '"Name":null,' +
                        '"x_FilterHelper":\"' + t_brand.Id + '\",' +
                        '"x_FilterHelperVendor":\"' + t_vendor.Id + '\",' +
                        '"FilterMatNrFilter":null,' +
                        '"MatKurztextFilter":null' +
                    '}'
                )
            );
            
            String t_jsonFilter = t_SCVPProductsController.generateJSONFilter();
            
            
            t_SCVPProductsController.isVendor = false;
            t_SCVPProductsController.filterQuery = true;
            
            t_SCVPProductsController.RefreshEquipment();
            
            t_SCVPProductsController.switchFilterForProducts();
        }
        
        Test.stopTest();
    }
    
    /**
    * Test method to cover the opposites of the processes.
    *
    */
    @isTest
    public static void SCVPProductsControllerTest2()
    {
        Profile t_profile = [
            SELECT
                Id
            FROM
                Profile
            WHERE
                Name IN ('Systemadministrator', 'System Administrator')
            LIMIT 1
        ];
        
        System.assert(t_profile != null);
        System.assert(t_profile.Id != null);
        
        
        User t_user = new User(
            Alias = 'GMSTest1', 
            FirstName = 'GMS',
            LastName = 'Testuser 1', 
            Username = 'gmstest1@gms-online.de', 
            Email = 'gmstest1@gms-online.de', 
            CommunityNickname = 'gmstest1', 
            Street = 'Karl Schurz Str.',
            PostalCode = '33100',
            City = 'Paderborn',
            Country = 'DE',
            GEOX__c = -0.10345,
            GEOY__c = 51.49250,
            TimeZoneSidKey = 'Europe/Berlin', 
            LocaleSidKey = 'de_DE_EURO', 
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            
            ProfileId = t_profile.Id
        );
        
        insert t_user;
        
        System.assert(t_user.Id != null);
        
        t_stock.Vendor__c = null;
        
        update t_stock;
        
        SCStock__c tmp_stock = t_stock;
        
        Test.startTest();
        
        System.runAs(t_user)
        {
            Test.setCurrentPage(Page.SCVPProducts);
            
            SCVPProductsController t_SCVPProductsController = new SCVPProductsController();
            
            t_SCVPProductsController.getIntReturnListSize();
            
            t_SCVPProductsController.RefreshEquipment();
            
            t_SCVPProductsController.x_FilterHelperVendor.Vendor__c = t_vendor.Id;
            
            t_SCVPProductsController.FilterMatNrFilter = '1';
            t_SCVPProductsController.MatKurztextFilter = '1';
            
            t_SCVPProductsController.ShowEquipment(tmp_stock);
            t_SCVPProductsController.fillStocks();
            
                        t_SCVPProductsController.assignFilter(
                new SCSearchFilter__c(
                    Filter__c = '{' +
                        '"Name":null,' +
                        '"FilterMatNrFilter":1,' +
                        '"MatKurztextFilter":1' +
                    '}'
                )
            );
            
            String t_jsonFilter = t_SCVPProductsController.generateJSONFilter();
            
            t_SCVPProductsController.x_PageCounterInput = 2;
            
            t_SCVPProductsController.setPage();
            
            t_SCVPProductsController.gethasPrevious();
            
             t_SCVPProductsController.gethasNext();
        }
        
        Test.stopTest();
    }
}
