/*
 * @(#)SCTriggerDomainRefTest.cls
 *  
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
@isTest
private class SCTriggerDomainRefTest
{
    static testMethod void testTrigger()
    {
        SCDomainRef__c a = new SCDomainRef__c(Name = 'DomA', ID2__c = '100');
        SCDomainRef__c b = new SCDomainRef__c(Name = 'DomB', ID2__c = '200', ID__c = 300);
        SCDomainRef__c c = new SCDomainRef__c(Name = 'DomC', ID2__c = 'testC');
        SCDomainRef__c d = new SCDomainRef__c(Name = 'DomD', ID2__c = 'testD', ID__c = 400);
        
        
        Database.insert(new List<SCDomainRef__c>{a,b,c,d});
        
        System.assertEquals(4, [SELECT count() FROM SCDomainRef__c]);
        System.assertEquals(4, [SELECT count()
                                  FROM SCDomainRef__c
                                 WHERE (Id = :a.Id AND Name = 'DomA' AND ID2__c = '100' AND ID__c = 100)
                                    OR (Id = :b.Id AND Name = 'DomB' AND ID2__c = '200' AND ID__c = 200)
                                    OR (Id = :c.Id AND Name = 'DomC' AND ID2__c = 'testC' AND ID__c = null)
                                    OR (Id = :d.Id AND Name = 'DomD' AND ID2__c = 'testD' AND ID__c = 400) ]);
        
        
        a.ID2__c = 'testA';
        b.ID2__c = '500';
        c.ID2__c = '600';
        d.ID2__c = 'testE';
        
        Database.update(new List<SCDomainRef__c>{a,b,c,d});
        
        System.assertEquals(4, [SELECT count() FROM SCDomainRef__c]);
        System.assertEquals(4, [SELECT count()
                                  FROM SCDomainRef__c
                                 WHERE (Id = :a.Id AND Name = 'DomA' AND ID2__c = 'testA' AND ID__c = 100)
                                    OR (Id = :b.Id AND Name = 'DomB' AND ID2__c = '500' AND ID__c = 500)
                                    OR (Id = :c.Id AND Name = 'DomC' AND ID2__c = '600' AND ID__c = 600)
                                    OR (Id = :d.Id AND Name = 'DomD' AND ID2__c = 'testE' AND ID__c = 400) ]);
    }
}
