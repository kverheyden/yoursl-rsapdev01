/**********************************************************************
Name:  SalesOrderSummaryBatch
======================================================
Purpose:                                                            
Runs every hour and sends out an PDF file with sales orders and sales order items if Account.GFGHOrderDeadline__c is exceeded.                                                    
======================================================
History                                                            
-------                                                            
Date  	AUTHOR	DETAIL 
06/11/2014 Jan Mensfeld  
***********************************************************************/


global with sharing class SalesOrderSummaryBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {
	
	global String SessionId { get; set; }
	
	private static String getCommaSeparatedIdListForQueries(Set<Id> ids) {
		return '\'' + String.join(new List<Id>(ids), '\',\'') + '\'';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {		
		String salesOrderQuery = 'SELECT Supplier__c FROM SalesOrder__c' + 
			' WHERE SendToSupplierDate__c != NULL AND SendToSupplierDate__c < ' + System.now().format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'') +
			' AND Status__c = \'Active\'' +
			' AND RecordType.Name = \'Indirect Order\'' + 
			' AND Id IN (SELECT SalesOrder__c FROM SalesOrderItem__c)';
		System.debug('SALES ORDER QUERY: ' + salesOrderQuery);
		
		List<SalesOrder__c> IndirectOrders = Database.query(salesOrderQuery);
		
		Set<Id> supplierIds = new Set<Id>();		
		for (SalesOrder__c so : IndirectOrders) 
			supplierIds.add(so.Supplier__c);
		return Database.getQueryLocator([SELECT Id FROM Account WHERE Id IN: supplierIds]);
	}
	
	global void execute(Database.BatchableContext bc, List<Account> scope) {
		System.debug('EXECUTE SALES ORDER BATCH JOB');
		
		Set<Id> supplierIds = new Set<Id>();		
		for (Account acc : scope) {
			supplierIds.add(acc.Id);
		}
		
		System.debug(UserInfo.getSessionId());
		CCWCPDFCreator.createSalesOrderSummariesCallout(supplierIds, SessionId);	
		
		
		String salesOrderQuery = 'SELECT Supplier__c, Status__c FROM SalesOrder__c' + 
			' WHERE SendToSupplierDate__c != NULL AND SendToSupplierDate__c < ' + System.now().format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'') +
			' AND Status__c = \'Active\'' +
			' AND RecordType.Name = \'Indirect Order\'' + 
			' AND Id IN (SELECT SalesOrder__c FROM SalesOrderItem__c)';
		System.debug('SALES ORDER QUERY: ' + salesOrderQuery);
		List<SalesOrder__c> IndirectOrders = Database.query(salesOrderQuery);
		
		for (SalesOrder__c so : IndirectOrders) {
			if (so.Status__c != 'Sent to supplier')
				so.Status__c = 'Sent to supplier';
		}
		update IndirectOrders;
	}
	
	global void finish(Database.BatchableContext bc) {
		System.debug('Batch Process Complete');
	}
}
