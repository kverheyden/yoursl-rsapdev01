/**
 * AseCalloutSCResource.cls mt  28.09.2009
 * 
 * Copyright (c) 2009 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 /**
 * AseCalloutSCResource represents SCResource 
 * 
 * @author mt
 * @version $Revision$, $Date$
 */
global class AseCalloutSCResource extends AseCalloutSCBaseClass {

    /*
     * @see AseCalloutSCBaseClass#getASEType()
     */
    public Integer getASEType()
    {
        return AseCalloutConstants.ASE_TYPE_RESOURCE;
    }
    
}
