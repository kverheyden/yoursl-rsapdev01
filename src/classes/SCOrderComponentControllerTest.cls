/*
 * @(#)SCOrderComponentControllerTest.cls
 *
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author Dietrich Herdt <dherdt@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCOrderComponentControllerTest
{
    /**
     * orderComponentController under positiv test 1
     */
    static testMethod void orderComponentControllerPositiv1()
    {
        SCHelperTestClass.createOrderTestSet(true);

        Test.startTest();
        
        SCOrderTabComponentController base = new SCOrderTabComponentController();
        Id appointmentId = SCHelperTestClass.appointmentsSingle1[0].Id ;          
        ApexPages.currentPage().getParameters().put('aid', appointmentId);
        base.pageController = new SCProcessOrderController();

        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.createOrderRoles(true);
        base.processCustomerParamAid = SCHelperTestClass.account.Id;
        
        System.assert(base.boOrder != null);
        System.assert(base.aid != null);
        System.assert(base.deletedOrderItemIds != null);
        
        SCboOrderItem oi = new SCboOrderItem();
        oi = base.currentOrderItem;
        base.currentOrderItem = oi;
        
        base.getCurrentResourceId();
        Test.stopTest();
    }
       
}
