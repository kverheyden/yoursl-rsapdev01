/*
 * @(#)SCOrderTabComponentControllerTest.cls
 *  
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @version $Revision$, $Date$
 */
@isTest
private class SCOrderTabComponentControllerTest 
{
    
    static testMethod void constructor() 
    {
        SCOrderTabComponentController base = new SCOrderTabComponentController();
        base.appSettings.OrderRoleMode__c = 'SRCA';
    }


    static testMethod void changeRole() 
    {
        // insert a test order (the insert trigger fires)
        SCHelperTestClass.createOrderTestSet(true);

        Test.startTest();
        
        SCOrderTabComponentController base = new SCOrderTabComponentController();
        Id appointmentId = SCHelperTestClass.appointmentsSingle1[0].Id ;          
        ApexPages.currentPage().getParameters().put('aid', appointmentId);
        base.pageController = new SCProcessOrderController();

        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.createOrderRoles(true);
        base.processCustomerParamAid = SCHelperTestClass.account.Id;
        base.changeRole();
        Test.stopTest();
    }

    static testMethod void delRole() 
    {
        SCHelperTestClass.createOrderTestSet(true);

        Test.startTest();
        
        SCOrderTabComponentController base = new SCOrderTabComponentController();
        Id appointmentId = SCHelperTestClass.appointmentsSingle1[0].Id ;          
        ApexPages.currentPage().getParameters().put('aid', appointmentId);
        base.pageController = new SCProcessOrderController();

        base.appSettings.OrderRoleMode__c = 'SRCA';
        List<SCfwOrderControllerBase.OrderRoleObj> roleObjList = new List<SCfwOrderControllerBase.OrderRoleObj>();
        base.delRole();
        Test.stopTest();
   }

    static testMethod void processCustomer()
    {
        // insert a test order (the insert trigger fires)
        SCHelperTestClass.createOrderTestSet(true);

        Test.startTest();
        
        SCOrderTabComponentController base = new SCOrderTabComponentController();
        Id appointmentId = SCHelperTestClass.appointmentsSingle1[0].Id ;          
        ApexPages.currentPage().getParameters().put('aid', appointmentId);
        base.pageController = new SCProcessOrderController();

        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.createOrderRoles(true);
        base.processCustomerParamAid = SCHelperTestClass.account.Id;
        base.processCustomerParamType = ' ';
        base.processCustomer();
        base.getRoleAccountIds();
        
        base.roleObjList = null;
        System.assert(base.roleObjList != null );
        Test.stopTest();
    }
}
