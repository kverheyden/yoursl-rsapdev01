/*
 * @(#)SCbtcStockReplenishmentTest.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCbtcStockReplenishmentTest
{
    static testMethod void testEntries_1()
    {
        SCApplicationSettings__c applicationSettings = SCApplicationSettings__c.getInstance();
        System.debug('###....testEntries_1 before startTest, applicationSettings1 : ' + applicationSettings);
        
        Test.StartTest();
        SCApplicationSettings__c applicationSettings2 = SCApplicationSettings__c.getInstance();
        System.debug('###....testEntries_1 after startTest, applicationSettings2 : ' + applicationSettings2);

        ID stockId = SCbtcStockReplenishmentTestHelper.prepareTestData_A();
        
        ID jobID = SCbtcStockReplenishment.asyncReplenishAllForPlantName('9100', 1, 'trace');
        //System.abortJob(jobID);
        Test.StopTest();
    }

    static testMethod void testEntries_2()
    {
        Test.StartTest();
        ID stockId = SCbtcStockReplenishmentTestHelper.prepareTestData_A();
        
        ID jobID = SCbtcStockReplenishment.asyncReplenishStock(stockId, 'trace');
        //System.abortJob(jobID);
        Test.StopTest();
    }
    
    static testMethod void testEntries_3()
    {
        Test.StartTest();
        ID stockId = SCbtcStockReplenishmentTestHelper.prepareTestData_A();
        
        ID jobID = SCbtcStockReplenishment.asyncReplenishStockDeterminedByName('9101', 'trace');
        //System.abortJob(jobID);
        Test.StopTest();
    }

    static testMethod void testEntries_4()
    {
        Test.StartTest();
        ID stockId = SCbtcStockReplenishmentTestHelper.prepareTestData_A();
        List<String> stockIdList = new List<String>();
        stockIdList.add(stockId);
        ID jobID = SCbtcStockReplenishment.asyncReplenishStockList(stockIdList);
        //System.abortJob(jobID);
        Test.StopTest();
    }

    static testMethod void testEntries_5()
    {
        Test.StartTest();
        ID stockId = SCbtcStockReplenishmentTestHelper.prepareTestData_A();
        List<String> stockIdList = new List<String>();
        stockIdList.add(stockId);
        SCbtcStockReplenishment.syncReplenishStockList(stockIdList);
        Test.StopTest();
    }



    static testMethod void testCreateReplenishment_A()
    {
        Test.StartTest();
        ID stockId = SCbtcStockReplenishmentTestHelper.prepareTestData_A();
        runJobToTest(stockId);
        checkResults_A(stockId);        
        Test.StopTest();
    }

    static void checkResults_A(ID stockId)
    {
        SCMaterialReplenishment__c r = SCbtcStockReplenishmentTestHelper.readReplenishment(stockId);
        debug('replenishment: ' + r);
        
        // Article Name to a material movement
        Map<String, SCMaterialMovement__c> mm = SCbtcStockReplenishmentTestHelper.getMapArticleToMaterialMovement(stockId, r.Id);    

        // check material movements
        debug('CheckArticle SB001'); 
//           rule,  name,  unit, wUnit,weight,vol,   repl,   price,priceUnit,premanent,  lockType, minQty, qty,movementQty
//  pseudorule,  name,  unit, wUnit,weight,vol,   repl,   price,priceUnit,premanent,       lockType,minQty, qty,movementQty
//      cI('R0', 'SB001','201','KGM',     1,  1,   null,      10,        1,     true,       '50201',     5,   1,          2,        plantId, 
//      stockId, priceListId); // replenishment 2
        System.assertEquals(mm.get('SB001').Qty__c, 2);

        debug('CheckArticle S0001'); 
//      cI('S4', 'S0001','201','KGM',     1,  1,   null,      10,        1,     true,       '50201',     5,   1,          1,        plantId, 
//      stockId, priceListId); // replenishment 3
        System.assertEquals(mm.get('S0001').Qty__c, 3);

        debug('CheckArticle S0002'); 
//      cI('S5', 'S0002','201','KGM',     1,  1,   null,      10,        1,     true,       '50203',     5,   1,          3,        plantId,
//      stockId, priceListId); // replenishment 0 because locked, without locking it would by 1
        // if Qty = 0 then no material movement is created
        System.assertEquals(mm.get('S0002'), null);

        debug('CheckArticle S0008'); 
        // supersession wiht replenishment = 6
//      cI('E5', 'S0008','201', null,     1,  1,   null,      10,        1,     true,       '50201',     5,   1,          1,        plantId,
//      stockId, priceListId); // replenishment 3 + 2 + 1 = 6 because added 2 from S0008 and 1 from S0009
        System.assertEquals(mm.get('S0008').Qty__c, 6);
        
        debug('CheckArticle S0009'); 
//      cI('E6', 'S0009','201', null,     1,  1,'S0008',      10,        1,     true,       '50201',     5,   1,          2,        plantId,
//      stockId, priceListId); // replenishment 0 because replaced, else it would by 2
        // if Qty = 0 then no material movement is created
        System.assertEquals(mm.get('S0009'), null); 


        debug('CheckArticle S0010'); 
//      cI('E7', 'S0010','201', null,     1,  1,'S0009',      10,        1,     true,       '50201',     5,   1,          3,        plantId,
//      stockId, priceListId); // replenishment 0 because replaced, else it would be 1
        // if Qty = 0 then no material movement is created
        System.assertEquals(mm.get('S0010'), null);

        debug('CheckArticle S0004'); 
        // supersession with replenishment = 0
//      cI('F5', 'S0004','201', null,     1,  1,   null,      10,        1,     true,       '50203',     5,   1,          1,        plantId,
//      stockId, priceListId); // replenishment 3 + 2 + 1 = 6 aber while locked replenishment = 0
        // if Qty = 0 then no material movement is created
        System.assertEquals(mm.get('S0004'), null);
        
        debug('CheckArticle S0005'); 
//      cI('F6', 'S0005','201', null,     1,  1,'S0004',      10,        1,     true,       '50205',     5,   1,          2,        plantId,
//      stockId, priceListId); // replenishment 0, because replaced and locked, else it would be 1
        // if Qty = 0 then no material movement is created
        System.assertEquals(mm.get('S0005'), null);
        
        debug('CheckArticle S0006'); 
//      cI('F7', 'S0006','201', null,     1,  1,'S0005',      10,        1,     true,       '50205',     5,   1,          3,        plantId,
//      stockId, priceListId); // replenishment 0, because replaced and locked, else it would be 1
        // if Qty = 0 then no material movement is created
        System.assertEquals(mm.get('S0006'), null);
        

        debug('End of material movement cases.'); 


    }


    static void runJobToTest(ID stockId)
    {
        SCbtcStockReplenishment.syncReplenishStock(stockId);
    }


    private static void debug(String text)
    {
        System.debug('###...................' + text);
    }


}
