/*
* @(#)SCVPExtAssignmtReversalExtensionTest.cls
* 
* Copyright 2014 by GMS Development GmbH, Karl-Schurz-Strasse 29, 
* DE-33100 Paderborn. All rights reserved.
*
* 
* 
* # Test class for 'SCVPExtAssignmtReversalExtension'.
*
* @author Marc Sälzler <msaelzler@gms-online.de>
*
* @history  
* 2014-11-25 GMSmae created
* 
* @review
*
*/

@isTest
public class SCVPExtAssignmtReversalExtensionTest
{
    @isTest
    public static void SCVPExtAssignmtReversalExtensionTest()
    {
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet(true);
        
        SCVendor__c vendor = new SCVEndor__c(
                                                Name           = '0123456789',
                                                Name1__c       = 'My test vendor',
                                                Country__c     = 'DE',
                                                PostalCode__c  = '33100',
                                                City__c        = 'Paderborn',
                                                Street__c      = 'Bahnhofstrt',
                                                LockType__c    = '5001',
                                                Status__c      = 'Active'
                                            );
                                            
        insert vendor;
        
        
        SCOrderExternalAssignment__c extAssignment = new SCOrderExternalAssignment__c(
                                                                                        Order__c = SCHelperTestClass.order.id,
                                                                                        Vendor__c         = vendor.id,
                                                                                        Status__c         = 'assigned',
                                                                                        WishedDate__c     = Date.today()
                                                                                    );
        
        insert extAssignment;
        
        
        Test.startTest();
        
        PageReference t_pageRef = Page.SCVPExtAssignmtReversal;
        
        Test.setCurrentPage(t_pageRef);
        
        ApexPages.StandardController t_stdCtrl;
        
        t_stdCtrl = new ApexPages.StandardController(new SCOrderExternalAssignment__c());
        
        SCVPExtAssignmtReversalExtension t_scVPExtAssignmtReversalExtension;
        
        try
        {
            t_scVPExtAssignmtReversalExtension = new SCVPExtAssignmtReversalExtension(t_stdCtrl);
        }
        catch(Exception e)
        {
            
        }
        
        System.assert(t_scVPExtAssignmtReversalExtension.a_errorText != '',  'Expected an error text. String is empty.');
        
        t_stdCtrl = new ApexPages.StandardController(extAssignment);
        
        t_pageRef.getParameters().put('id', extAssignment.Id);
        
        Test.setCurrentPage(t_pageRef);
        
        t_scVPExtAssignmtReversalExtension = new SCVPExtAssignmtReversalExtension(t_stdCtrl);
        
        String t_errorText = t_scVPExtAssignmtReversalExtension.a_errorText;
        String t_errorTextExtAssignmt = t_scVPExtAssignmtReversalExtension.a_errorTextExtAssignmt;
        Boolean t_success = t_scVPExtAssignmtReversalExtension.a_success;
        
        // Text too long.
        extAssignment.ExternalReversalReason__c = 'testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest';
        
        try
        {
            t_scVPExtAssignmtReversalExtension.SaveProcess();
        }
        catch(Exception e)
        {
            
        }
        
        System.assert(t_scVPExtAssignmtReversalExtension.a_errorTextExtAssignmt != '',  'Expected an error text. String is empty.');
        
        extAssignment.ExternalReversalReason__c = 'Test';
        
        t_scVPExtAssignmtReversalExtension.SaveProcess();
        
        System.assertEquals(true, t_scVPExtAssignmtReversalExtension.a_success);
        
        Test.stopTest();
    }
}
