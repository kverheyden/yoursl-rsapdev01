@isTest (SeeAllData = true)
private class SCboInventoryTest
{
    private static SCboInventory boInventory;
    
    static testMethod void boInventoryPositiv1() 
    {      
        SCHelperTestClass.createOrderTestSet(true);
        ApexPages.currentPage().getParameters().put('sid', SCHelperTestClass.stocks[0].id);
        boInventory = new SCboInventory();
        
        Test.startTest();
        
        // With plant name
        List<ID> inventories = SCboInventory.createInventoryForPlant(SCHelperTestClass.plant.name, 'createInventoryForPlant1', '2013', true, Date.today());
               
        // Read by ID
        SCInventory__c inv1 = boInventory.readById(String.valueOf(inventories[0]));
        
        // Read all ny id
        List<String> ids = new List<String>();
        for(Id i : inventories)
        {
            ids.add(String.valueOf(i));
        }
        List<SCInventory__c> inv2 = boInventory.readAllById(ids);
                
        // Cancel function
        String isCancelled = boInventory.cancel(String.valueOf(inventories[0]));
        
        Test.stopTest();
    }
    
    static testMethod void boInventoryPositiv2() 
    {
        SCHelperTestClass.createOrderTestSet(true);
        ApexPages.currentPage().getParameters().put('sid', SCHelperTestClass.stocks[0].id);
        boInventory = new SCboInventory();
        
        Test.startTest();
    
        // Only create function (stock id: strings)
        List<String> stockIds = new List<String>();
        for(SCStock__c s : SCHelperTestClass.stocks)
        {
            stockIds.add(String.valueOf(s.id));
        }
        List<ID> inventories2 = SCboInventory.createInventory(stockIds, 'createInventory 2', '2013', false, Date.today());
    
        Test.stopTest();
    }
    
    static testMethod void boInventoryPositiv3() 
    {
        SCHelperTestClass.createOrderTestSet(true);
        ApexPages.currentPage().getParameters().put('sid', SCHelperTestClass.stocks[0].id);
        boInventory = new SCboInventory();
        
        Test.startTest();
    
        // Only create function (stock id: strings)
        List<ID> stockIds2 = new List<String>();
        for(SCStock__c s : SCHelperTestClass.stocks)
        {
            stockIds2.add(s.id);
        }
        // Only create function (stock id: ids)
        List<ID> inventories3 = SCboInventory.createInventory(stockIds2, 'createInventory 3', '2013', false, Date.today());
    
        Test.stopTest();
    }
    
    static testMethod void boInventoryPositiv4() 
    {
        SCHelperTestClass.createOrderTestSet(true);
        ApexPages.currentPage().getParameters().put('sid', SCHelperTestClass.stocks[0].id);
        boInventory = new SCboInventory();
        
        Test.startTest();
            
        // create Inventory For Stock With Name (stock name)
        Id invId = SCboInventory.createInventoryForStockWithName(SCHelperTestClass.stocks[0].name, 'createInventory 4', '2013', false, Date.today());
    
        Test.stopTest();
    }
    
    static testMethod void boInventoryPositiv5() 
    {
        SCHelperTestClass.createOrderTestSet(true);
        ApexPages.currentPage().getParameters().put('sid', SCHelperTestClass.stocks[0].id);
        boInventory = new SCboInventory();
        
        Test.startTest();

        SCHelperTestClass.createTestStockItem(SCHelperTestClass.stocks[0].id, SCHelperTestClass.article.id, true);
            
        // create Inventory For Stock With Name (stock name)
        Id invId = SCboInventory.createInventory(SCHelperTestClass.stocks[0].id, 'createInventory 5', '2013', false, Date.today());
        Id invId2 = SCboInventory.createInventory(SCHelperTestClass.stocks[0].id, 'createInventory 5', '2013', false, Date.today());
    
        // Close function
        String isClosed = boInventory.close(invId,false, Date.today());
    
        Test.stopTest();
    }
}
