global without sharing class cc_cceag_ctrl_Landing {
    @RemoteAction
    global static ccrz.cc_RemoteActionResult fetchOutlets(ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_RemoteActionResult result = cc_cceag_util_Utils.initAction(ctx);
        try {
            String userId = processUserId(ctx.portalUserId);
            String accountId = cc_cceag_dao_Account.fetchAccountId(userId);
            List<cc_bean_Outlet> outlets = retrieveShipTos(ctx, accountId);
            if (outlets.isEmpty()) result.messages.add(cc_cceag_util_Utils.buildErrorMessage(null, 'ERROR_MSG_NO_OUTLETS', null));
            else {
                cc_cceag_bean_HeaderData headerData = new cc_cceag_bean_HeaderData(outlets);
                if (!String.isBlank(ctx.currentCartID))
                    headerData.cartData = retrieveCart(ctx);
                headerData.deliveryData = fetchAvailableDeliveryDates(ctx, outlets);
                if (headerData.cartData == null) {
                    headerData.selectedOutlet = outlets.get(0).outlet;
                    ccrz__E_Cart__c newCart = createCart(userId, ctx.storefront, outlets.get(0), accountId);
                    if (headerData.deliveryData != null) {
                        Map<String,Object> outDelivery = (Map<String,Object>) headerData.deliveryData.get(headerData.selectedOutlet);
                        newCart.ccrz__RequestDate__c = (Date) outDelivery.remove('rawDate');
                    }
                    upsertCart(newCart, outlets.get(0), null);
                    headerData.cartData = retrieveCart(ctx);
                    headerData.isNew = true;
                }
                else {
                    if (headerData.cartData.shippingAddress != null && headerData.cartData.shippingAddress.partnerId != null)
                        headerData.selectedOutlet = headerData.cartData.shippingAddress.partnerId;
                    if (headerData.selectedOutlet == null || headerData.cartData.requestedDate == null) {
                        if (headerData.selectedOutlet == null) {
                            headerData.selectedOutlet = outlets.get(0).outlet;
                            if (outlets.get(0).shipTerms == 'Abholung')
                                headerData.cartData.shippingMethod = 'Pickup';
                            else
                                headerData.cartData.shippingMethod = 'Delivery';
                        }
                        ccrz__E_Cart__c cart = processCartUpdate(headerData, outlets);
                    }
                    else {
                        Map<String, Object> dateData = (Map<String, Object>) headerData.deliveryData.get(headerData.selectedOutlet);
                        if (dateData != null) {
                            Date nextAvail = (Date) dateData.get('rawDate');
                            if (nextAvail > headerData.cartData.requestedDate) processCartUpdate(headerData, outlets);
                        }
                    }
                    if (headerData.cartData.requestedDate != null) {
                        DateTime dt = DateTime.newInstance(headerData.cartData.requestedDate.year(), headerData.cartData.requestedDate.month(), headerData.cartData.requestedDate.day());
                        headerData.displayDate = dt.format('dd M yyyy');
                    }
                    headerData.isNew = false;
                }
                result.data = headerData;
                result.success = true;
                System.debug(System.LoggingLevel.INFO, headerData);
            }
        } catch(Exception e) {
            System.debug(System.LoggingLevel.ERROR, e.getMessage());
            System.debug(System.LoggingLevel.ERROR, e.getStackTraceString());
            result.messages.add(cc_cceag_util_Utils.buildExceptionMessage(e));
        }
        return result;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult selectOutlet(ccrz.cc_RemoteActionContext ctx, cc_bean_Outlet outlet, cc_cceag_bean_Cart cartData) {
        ccrz.cc_RemoteActionResult result = cc_cceag_util_Utils.initAction(ctx);
        try {
            String userId = (String.isBlank(ctx.portalUserId)) ? UserInfo.getUserId() : ctx.portalUserId;
            ccrz__E_Cart__c cart = cc_cceag_dao_Cart.retrieveCart(cartData.sfid);
            cart = upsertCart(cart, outlet, cartData);
            result.data = cart.ccrz__EncryptedId__c;
            result.success = true;
        } catch(Exception e) {
            System.debug(System.LoggingLevel.ERROR, e.getMessage());
            System.debug(System.LoggingLevel.ERROR, e.getStackTraceString());
            result.messages.add(cc_cceag_util_Utils.buildExceptionMessage(e));
        }
        return result;
    }

    @RemoteAction
    global static cc_cceag_bean_Cart retrieveCart(ccrz.cc_RemoteActionContext ctx) {
        String portalUserId = processUserId(ctx.portalUserId);
        ccrz__E_Cart__c activeCart = cc_cceag_dao_Cart.retrieveActiveCart(portalUserId, ctx.storefront);
        if (activeCart != null)
            return new cc_cceag_bean_Cart(activeCart);
        else
            return null;
    }

    @RemoteAction
    global static List<cc_bean_Outlet> retrieveShipTos(ccrz.cc_RemoteActionContext ctx, String accountId) {
        List<CCAccountRole__c> relatedAccounts = cc_cceag_dao_Account.fetchRelatedAccounts(accountId, 'WE');
        List<cc_bean_Outlet> outlets = new List<cc_bean_Outlet>();
        for (CCAccountRole__c rel: relatedAccounts) {
            outlets.add(new cc_bean_Outlet(rel.SlaveAccount__r));
        }
        return outlets;
    }

    @RemoteAction
    global static Map<String, Object> fetchAvailableDeliveryDates(ccrz.cc_RemoteActionContext ctx, List<cc_bean_Outlet> outlets) {
        String portalUserId = processUserId(ctx.portalUserId);
        ccrz.cc_api_DeliveryDate deliveryDateApi = new cc_cceag_api_DeliveryDate();
        Map<String, Object> inputData = new Map<String, Object>();
        inputData.put('shipTos', outlets);
        return deliveryDateApi.getDeliveryDates(inputData);
    }

    private static ccrz__E_Cart__c processCartUpdate(cc_cceag_bean_HeaderData headerData, List<cc_bean_Outlet> outlets) {
        ccrz__E_Cart__c cart = cc_cceag_dao_Cart.retrieveCart(headerData.cartData.sfid);
        Map<String,Object> outDelivery = (Map<String,Object>) headerData.deliveryData.get(headerData.selectedOutlet);
        headerData.cartData.requestedDate = (Date) outDelivery.remove('rawDate');
        cart.ccrz__RequestDate__c = headerData.cartData.requestedDate;
        cc_bean_Outlet foundOutlet = outlets.get(0);
        for (cc_bean_Outlet outlet: outlets) {
            if (outlet.outlet == headerData.selectedOutlet) {
                foundOutlet = outlet;
                break;
            }
        }
        cart = upsertCart(cart, foundOutlet, headerData.cartData);
        return cart;
    }

    private static ccrz__E_Cart__c createCart(String userId, String storeName, cc_bean_Outlet outlet, String accountId) {
        User userData = cc_cceag_dao_Account.fetchUser(userId);
        ccrz__E_Cart__c cartHeader             = new ccrz__E_Cart__c();
        cartHeader.ccrz__CartType__c           = 'Cart';
        cartHeader.ccrz__CartStatus__c         = 'Open';
        cartHeader.ccrz__User__c               = userId;
        cartHeader.ccrz__Contact__c            = userData.ContactId;
        cartHeader.ccrz__Name__c               = 'Shopping Cart';
        cartHeader.OwnerId               = userId;
        cartHeader.ccrz__AnonymousID__c        = false;
        cartHeader.ccrz__ActiveCart__c         = true;
        cartHeader.ccrz__Storefront__c         = storeName;
        cartHeader.ccrz__Account__c = accountId;
        cartHeader.ccrz__SessionID__c = UserInfo.getSessionId();
        cartHeader.ccrz__BuyerFirstName__c = userData.Contact.FirstName;
        cartHeader.ccrz__BuyerLastName__c = userData.Contact.LastName;
        cartHeader.ccrz__BuyerEmail__c = userData.Contact.Email;
        if (outlet.shipTerms == 'Abholung')
            cartHeader.ccrz__ShipMethod__c = 'Pickup';
        else
            cartHeader.ccrz__ShipMethod__c = 'Delivery';
        insert cartHeader;
        return cartHeader;
    }

    private static ccrz__E_Cart__c upsertCart(ccrz__E_Cart__c cart, cc_bean_Outlet outlet, cc_cceag_bean_Cart cartData) {
        ccrz__E_ContactAddr__c defAddr = outlet.convertToAddress();
        defAddr.Id = cart.ccrz__ShipTo__c;
        List<ccrz__E_ContactAddr__c> addresses = new List<ccrz__E_ContactAddr__c>();
        ccrz__E_ContactAddr__c billTo = defAddr;
        ccrz__E_ContactAddr__c shipTo = defAddr;
        if (cart.ccrz__BillTo__c == null) {
            List<CCAccountRole__c> billAccounts = cc_cceag_dao_Account.fetchRelatedAccounts(cart.ccrz__Account__c, 'RG');
            if (billAccounts != null && billAccounts.size() > 0) {
                cc_bean_Outlet billOutlet = new cc_bean_Outlet(billAccounts.get(0).SlaveAccount__r);
                billTo = billOutlet.convertToAddress();
                addresses.add(billTo);
            }
        }
        addresses.add(shipTo);
        if (cartData != null) {
            if (cartData.requestedDateYear != null) {
                Date requestedDate = Date.newInstance(Integer.valueOf(cartData.requestedDateYear), Integer.valueOf(cartData.requestedDateMonth), Integer.valueOf(cartData.requestedDateDay));
                cart.ccrz__RequestDate__c = requestedDate;
            }
            //cart.ccrz__EffectiveAccountID__c = outlet.sfid;
            cart.ccrz__EffectiveAccountID__c = null;
            cart.ccrz__ShipMethod__c = cartData.shippingMethod;
            if (cartData.shippingMethod == 'Pickup') {
                String plantId = outlet.plant;
                SCPlant__c plant = null;
                plant = cc_cceag_dao_Account.fetchPlant(plantId);
                if (plant != null) {
                    ccrz.cc_bean_MockContactAddress plantAddr = new ccrz.cc_bean_MockContactAddress();
                    plantAddr.address1 = plant.Street__c;
                    plantAddr.city = plant.City__c;
                    plantAddr.country = outlet.countryCode;
                    plantAddr.countryCode = outlet.countryCode;
                    plantAddr.companyName = plant.Name__c;
                    plantAddr.postalCode = plant.ZipCode__c;
                    plantAddr.partnerId = outlet.outlet;
                    plantAddr.mailStop = plantId;
                    createAddress(shipTo, plantAddr);
                } else createAddress(shipTo, outlet);
            }
            else
                createAddress(shipTo, outlet);
            SCAccountInfo__c info = cc_cceag_dao_Account.fetchSCAccountInfo(cart.ccrz__Account__c);
            cart.ccrz__ContractId__c = (info == null) ? 'true' : 'false';
        }
        upsert addresses;
        if (cart.ccrz__BillTo__c == null)
            cart.ccrz__BillTo__c = billTo.Id;
        if (cart.ccrz__ShipTo__c == null)
            cart.ccrz__ShipTo__c = shipTo.Id;
        upsert cart;
        return cart;
    }

    private static String processUserId(String portalUserId) {
        if (String.isBlank(portalUserId))portalUserId = UserInfo.getUserId();
        return portalUserId;
    }

    private static ccrz__E_ContactAddr__c createAddress(ccrz__E_ContactAddr__c addr, cc_bean_Outlet outlet) {
        ccrz__E_ContactAddr__c newAddr = outlet.convertToAddress();
        if (addr != null)
            newAddr.Id = addr.Id;
        return addr;
    }

    private static ccrz__E_ContactAddr__c createAddress(ccrz__E_ContactAddr__c addr, ccrz.cc_bean_MockContactAddress addressBean) {
        if (addr == null) addr = new ccrz__E_ContactAddr__c();
        addr.ccrz__AddressFirstline__c = addressBean.address1;
        addr.ccrz__City__c = addressBean.city;
        addr.ccrz__Country__c = addressBean.country;
        addr.ccrz__CountryISOCode__c = addressBean.countryCode;
        addr.ccrz__CompanyName__c = addressBean.companyName;
        addr.ccrz__PostalCode__c = addressBean.postalCode;
        addr.ccrz__StateISOCode__c = addressBean.stateCode;
        addr.ccrz__State__c = addressBean.state;
        addr.ccrz__Partner_Id__c = addressBean.partnerId;       
        return addr;
    }

    public with sharing class cc_cceag_bean_HeaderData {
        public cc_cceag_bean_Cart cartData { get; set; }
        public List<cc_bean_Outlet> outlets { get; set; }
        public String selectedOutlet { get; set; }
        public Map<String, Object> deliveryData { get; set; }
        public String displayDate { get; set; }
        public boolean isNew { get; set; }

        public cc_cceag_bean_HeaderData(List<cc_bean_Outlet> outlets) {
            this.outlets = outlets;
        }
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult getProductTeaserProducts(ccrz.cc_RemoteActionContext ctx)
    {
        Map<String, Object> dataSet = new Map<String,Object>();
        
        string portalUserId = cc_cceag_dao_User.getFlowUserWithContext(ctx);
        
        ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
        result.inputContext = ctx;
        result.success = false;     
        
        String accountId = cc_cceag_dao_Account.fetchAccountId(portalUserId);

        Object productList = cc_cceag_dao_Account.getPromotedProductsForAccount(accountId);
        dataSet.put('products', productList);
        
        result.data = dataSet;
        result.success = true;
        
        return result;
    }
    
    @RemoteAction
    global static ccrz.cc_RemoteActionResult fetchWeatherForecast (ccrz.cc_RemoteActionContext ctx, Id accountId)
    {
        ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
        result.inputContext = ctx;
        result.success = false;
        
        result.data = loadWeatherForecast(accountId);
        result.success = true;
        
        return result;
    }

    private static String loadWeatherForecast (Id accountId)
    {
        Account account = [SELECT Id, GeoY__c, GeoX__c FROM Account WHERE Id=:accountId];
        
        String endPoint = 'http://lwf.meteogroup.de/index.php?' +
            'customer=cocacola&' +
            'hash=C91B9E28-92AB-46D7-9D78-535AB607248D&' +
            'locsearchtype=latlon&' +
            'lat='+account.GeoY__c+'&' +
            'lon='+account.GeoX__c;
        
        Httprequest request = new Httprequest();
        request.setEndpoint(endPoint);
        request.setMethod('GET');
        
        Http http = new Http();
        Httpresponse response = http.send(request);
        
        return response.getBody();
    }
}
