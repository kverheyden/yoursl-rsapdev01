/*
 * @(#)SCOrderCancelAppointmentsController.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This controller cancels selected Appointments (Assignments) of the order.
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCOrderCancelAppointmentsController
{  
    public String orderId; 
    public List<SCAppointment__c> selectedItems;
    public List<SCAppointment__c> selectedItemsOutput { get; set; }
    public String canNotCancel { get; set; }
    public Boolean cancelTransferred { get; set; }
    public Boolean showTransferredCheckbox { get; set; }
    public Boolean showCancelButton { get; set; }
    public Boolean onlyTransferred { get; set; }

    public SCOrderCancelAppointmentsController(ApexPages.StandardSetController controller) 
    {
        orderId = ApexPages.currentPage().getParameters().get('id');
        
        canNotCancel = '';
        
        showTransferredCheckbox = false;
        showCancelButton = true;
        onlyTransferred = false;
        
        selectedItems = (List<SCAppointment__c>) controller.getSelected();
        
        selectedItemsOutput = [ Select Assignment__r.Status__c, AssignmentStatus__c, Employee__c, Mobile__c, Phone__c, Name, Start__c, End__c 
                                From SCAppointment__c 
                                Where Id IN :selectedItems
                                Order By Assignment__r.Status__c desc];
                                
        // - Check, if the chechbox for cancelling transferred assignments must be shown.
        //   (by default it is hidden)
        showTransferredCheckbox = false;
        Integer countCancelledApps = 0;      
        Integer countTransferredApps = 0;   
        
        for(SCAppointment__c s :selectedItemsOutput)
        {
            if(s.Assignment__r.Status__c == '5503')
            {
                showTransferredCheckbox = true;
            }
            
            // Count only cancelled, completed and completed offline
            if(s.Assignment__r.Status__c == '5505' || s.Assignment__r.Status__c == '5506' || s.Assignment__r.Status__c == '5507' )
            {
                countCancelledApps++;
            }
            
            // Count only transferred
            if(s.Assignment__r.Status__c == '5503')
            {
                countTransferredApps++;
            }
        }
        
        // - Check, if there are only cancelled appointments
        if(countCancelledApps == selectedItemsOutput.size() && selectedItemsOutput.size() > 0)
        {
            showCancelButton = false;
        }
        
        // - Check, if there are only transferred appointments
        if(countTransferredApps == selectedItemsOutput.size() && selectedItemsOutput.size() > 0)
        {
            onlyTransferred = true;
        }
    }
    
    public SCOrderCancelAppointmentsController()
    {
    }
    
    /**
     * Is called when the button for enabling cancellation of assignments is clicked and only refresh the page.
     *
     * @return    Reference to the page
     */
    public PageReference handleCancelTransferred()
    {
        return null;
    }
    
    /**
     * Calcels selected order appointments and redirects user back to the order page
     *
     * @return    Reference to the order page
     */
    public PageReference cancelAppointments()
    {
        System.debug('#### cancelAppointments(): selectedItems -> ' + selectedItems);
        if(selectedItems != null && selectedItems.size() > 0)
        {
            // Starting to cancel
            // only open, planned, planned for precheck, released for processing, accepted
            // check this before calling this method
            // if(SCfConstants.DOMVAL_ORDERSTATUS_NOT_COMPLETED.contains(ord.Status__c))
        
            // Get the assignments depending on the selected appointments
            List<SCAppointment__c> selectedAssignmentsTmp = [ Select toLabel(AssignmentStatus__c), Start__c, End__c, Assignment__r.Status__c, Assignment__c, Employee__c, Mobile__c
                                                              From SCAppointment__c 
                                                              Where Id IN :selectedItems ];
            
            // Generating new Info
            String info = '';
            for(SCAppointment__c a: selectedAssignmentsTmp)
            {
                if(cancelTransferred == false && a.Assignment__r.status__c == '5503')
                {

                }
                else
                {
                    info += SCBase.formatInterval(a.start__c, a.end__c) + ' [' + a.AssignmentStatus__c + ']\n     ' + a.employee__c + ' ';
                    
                    if(a.Mobile__c != null)
                    {
                        info += a.Mobile__c;
                    }
                    
                    info += '\n';
                }
            }
            
            List<Id> selectedAssignments = new List<Id>();
            
            // Pack assignments Ids in the list               
            if(selectedAssignmentsTmp != null && selectedAssignmentsTmp.size() > 0)
            {
                for(SCAppointment__c a : selectedAssignmentsTmp)
                {
                    selectedAssignments.add(a.Assignment__c);
                }
            }
            
            List <SCAssignment__c> assignments = new List <SCAssignment__c>();
            
            List<String> appStatus;
            
            if(cancelTransferred == true)
            {
                appStatus = new List<String>{'5501', '5502', '5503', '5509', '5510'};
            }
            else
            {
                appStatus = new List<String>{'5501', '5502', '5509', '5510'};
            }
            
            // Cancel the assignments (a trigger forwards the cancell status to the ASE)
            if(selectedAssignments != null && selectedAssignments.size() > 0)
            {
                assignments = [ Select Id, Status__c 
                                From SCAssignment__c 
                                Where Order__c = :orderId 
                                AND Status__c in :appStatus
                                AND Id IN :selectedAssignments ];
            }
    
            if(assignments != null && assignments.size() > 0)
            {
                for(SCAssignment__c a: assignments)
                {
                    a.Status__c = SCfwConstants.DOMVAL_ORDERSTATUS_CANCELLED;
                }
                update assignments;
                
                System.debug('#### updated');
            }
            // cancel / update the order
            SCboOrder.CalcStateResult result = SCboOrder.calculateState(SCfwConstants.DOMVAL_ORDERSTATUS_OPEN, orderId);
            SCOrder__c ord = [select id, country__c from SCOrder__c where id = :orderId];
            ord.Status__c = result.nextState;
            ord.Followup1__c = null;
            ord.Followup2__c = null;  
            //From VA: ord.NotificationStatus__c = null;
            update ord;
            if(assignments != null && assignments.size() > 0)
            {
                if(info != null)
                {
                    SCboOrderLog.createLog(ord.Id, '11737', '', info);
                }
            }
            
            /* ### PMS 36296 not required any more here
            SCutilMessage  msg = new SCutilMessage();
            msg.onOrderCancel(ord);
            msg.send();       
            */
               
            PageReference pageRef = new PageReference('/' + orderId);
            pageRef.setRedirect(true);
            
            return pageRef.setRedirect(true);  
        }
        else
        {
            PageReference pageRef = new PageReference('/' + orderId);
            pageRef.setRedirect(true);
            
            return pageRef.setRedirect(true); 
        }
    }
    
    /**
     * Returns the user back to order page
     *
     * @return    Reference to the order page
     */
    public PageReference gotoOrder()
    {        
        PageReference pageRef = new PageReference('/' + orderId);
        pageRef.setRedirect(true);
        
        return pageRef.setRedirect(true);
    }
    
}
