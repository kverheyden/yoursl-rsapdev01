/*
 * @(#)SCHoverController.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Implements the controller for simple hovers that can be used to 
 * display context sensitive information. The controller evalates
 * the mode and oid field and reads the corresponding data.
 *
 * @author Norbert Armbruster  <narmbruster@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCHoverController 
{
    public List<HoverData> items{get;set;}
    public String itemHeader{get;set;}
    public String itemRemark{get;set;}

    public ID oid{get; set; }    
    public String mode{get; set;}    

    /*
     * Constructor retrieves the parameter oid and mode from the page 
     * and calls the ReadData method to access the database.
     */
    public SCHoverController()
    {
        items = new List<HoverData>();

        String oid  = ApexPages.currentPage().getParameters().get('oid');
        mode = ApexPages.currentPage().getParameters().get('mode');
        String value = ApexPages.currentPage().getParameters().get('value');
           
        ReadData(oid, mode, value);
    }

    /*
     * Constructor retrieves the parameter oid and mode as input 
     * and calls the ReadData method to access the database.
     */
    public SCHoverController(String oid, String mode)
    {
        items = new List<HoverData>();

        ReadData(oid, mode, null);
    }

    /*
     * Constructor retrieves the parameter oid and mode as input 
     * and calls the ReadData method to access the database.
     */
    public SCHoverController(String oid, String mode, String value)
    {
        items = new List<HoverData>();

        ReadData(oid, mode, value);
    }
    
    /*
     * Internal helper function that reads the data and prepares the output  
     * by filling the items object.
     * @param oid   The object id (depends on the mode)
     * @param mode  The following modes are supported:
     *              ACCOUNT  - reads the account lock flags 
     *              CASES    - reads the current pending cases   
     *              CONTRACT - reads contract details 
     * @param value optional value for reading required data
     */
    private void ReadData(String oid, String mode, String value)
    {
        // read the account alert flags 
        if (mode == 'ACCOUNT')
        {
             Account account = [select tolabel(locktype__c), tolabel(riskclass__c), description from account where id = :oid];
    
             // fill the header and a detail description
             itemHeader = System.Label.SC_app_HoverAccount;
             itemRemark = account.description;

             items.add(new HoverData(Schema.SObjectType.SCOrderRole__c.fields.LockType__c.getLabel(), 
                                     account.locktype__c, ''));
             items.add(new HoverData(Schema.SObjectType.SCOrderRole__c.fields.RiskClass__c.getLabel(), 
                                     account.riskclass__c, ''));
        } // if (mode == 'ACCOUNT')
        // read the vip level and contract informations of the account        
        else if (mode == 'CONTRACT')
        {
             Account account = [select tolabel(viplevel__c), description from account where id = :oid];
    
             // fill the header and a detail description
             itemHeader = System.Label.SC_app_HoverContract;
             itemRemark = account.description;

             items.add(new HoverData(Schema.SObjectType.SCOrderRole__c.fields.VipLevel__c.getLabel(), 
                                     account.viplevel__c, ''));
        } 
        // read infos for contracts defines by the value parameter
        else if (mode == 'CONTRACTREF')
        {
            // fill the header and a detail description
            itemHeader = Schema.SObjectType.SCContract__c.labelPlural;
            // add an empty line if necessary
            List<String> addInfos = new List<String>();
            addInfos.add('');
            addInfos.add('');
            addInfos.add('');
            items.add(new HoverData('', '', '', addInfos));
            
            addInfos = new List<String>();
            addInfos.add(Schema.SObjectType.SCContract__c.fields.StartDate__c.label);
            addInfos.add(Schema.SObjectType.SCContract__c.fields.EndDate__c.label);
            addInfos.add(Schema.SObjectType.SCContract__c.fields.TemplateName__c.label);
            // add the title only if there are data
            items.add(new HoverData(Schema.SObjectType.SCContract__c.fields.Name.label, 
                                    Schema.SObjectType.SCContract__c.fields.IdExt__c.label, '', addInfos));

            if (null != value)
            {
                List<String> contrNames = value.split(',');
                for (SCContract__c contr :[select Id, Name, IdExt__c, StartDate__c, EndDate__c, Template__r.TemplateName__c 
                                             from SCContract__c where Name in :contrNames])
                {
                    addInfos = new List<String>();
                    addInfos.add(String.valueOf(contr.StartDate__c));
                    addInfos.add(String.valueOf(contr.EndDate__c));
                    addInfos.add(contr.Template__r.TemplateName__c);
        
                    items.add(new HoverData(contr.Name, contr.IdExt__c, contr.Id, addInfos));
                }
            }
        } 
        // read the pending cases of the account        
        else if (mode == 'CASES')
        {
            itemHeader = System.Label.SC_flt_NotCompleted; // System.Label.SC_app_HoverCases;
        
            // Count the number of cases
            String query1 = 'select status, count(id) cnt from case where accountid = \'' + String.escapeSingleQuotes(oid) + '\' group by status';
            SObject[] queryresult1 = Database.query(query1);
            
            if (queryresult1.size() > 0)
            {
                // add the title only if there are data
                items.add(new HoverData(System.Label.SC_app_HoverCases + ' ' + System.Label.SC_app_Status, 'Count', ''));
            } // if (queryresult1.size() > 0)
            for (SObject item :queryresult1)    
            {            
                // store the data in the return object            
                items.add(new HoverData(String.valueOf(item.get('status')), 
                                        String.valueOf(item.get('cnt')), ''));
            } // for (SObject item :queryresult1)    
        
            String statList = '';
            for (String status : SCfwConstants.DOMVAL_ORDERSTATUS_PENDING_ORDERS.split(','))
            {
                statList += '\'' + status + '\''; 
                statList += ',';
            }
            statList = statList.substring(0,statList.length() - 1);
        
            // get infos for open orders
            String query2 = 'select Order__c, Order__r.Id, Order__r.Name, Order__r.CreatedDate from SCOrderRole__c ' + 
                            'where Account__c = \'' + oid + '\' and OrderRole__c = \'' + 
                            SCfwConstants.DOMVAL_ORDERROLE_LE + '\' and ' + 
                            ' Order__r.Status__c  IN (' + statList + ')';
            SCOrderRole__c[] orderRoles = Database.query(query2);
            
            
            //------------ pms 33162 gmssu 03.06.2012 --------
            // Reading Appointments from selected orders
            Set<Id> oIds = new Set<Id>();
            for (SCOrderRole__c role :orderRoles) 
            {
                oIds.add(role.Order__r.Id);
            }
            List<SCOrder__c> orders = [ Select Id, (Select Id, Name, Start__c From Appointments__r LIMIT 1) From SCOrder__c Where Id IN : oIds ];
            Map<Id, String> appDate = new Map<Id, String>();
            for(SCOrder__c a : orders)
            {
                if(a.Appointments__r != null && !a.Appointments__r.isEmpty())
                    appDate.put(a.id, String.valueOf(a.Appointments__r[0].Start__c));
                else
                    appDate.put(a.id, '');
            }
            //------------------------------------------------
            
            
            if (orderRoles.size() > 0)
            {
                List<String> strLabel = new List<String>();
                if (queryresult1.size() > 0)
                {
                    // add an empty line if necessary
                    items.add(new HoverData('', '', '',strLabel));
                } // if (queryresult1.size() > 0)
                
                // add the title only if there are data
                strLabel.add(System.Label.SC_app_Scheduled);
                
                items.add(new HoverData(System.Label.SC_app_OrderTab, 
                                        System.Label.SC_app_Date, 
                                        '',
                                        strLabel));
            } // if (orderRoles.size() > 0)
            for (SCOrderRole__c role :orderRoles)    
            {            
                // store the data in the return object            
                List<String> addInfos = new List<String>();
                if(appDate.containsKey(role.Order__r.Id))
                    addInfos.add(appDate.get(role.Order__r.Id));
                else
                    addInfos.add(' ');
                
                // the last value addInfos got the apppointment start date
                items.add(new HoverData(String.valueOf(role.Order__r.Name), 
                                        String.valueOf(role.Order__r.CreatedDate),
                                        String.valueOf(role.Order__c),
                                        addInfos));
            } // for (SObject item :queryresult2)    
        } // else if (mode == 'CASES')
    }

    /*
     * Retrieves the number of columns depending on the addInfos list in the items.
     */
    public Integer getColumns()
    {
        System.debug('#### getColumns(): columns -> ' + (((null != items) && (items.size() > 0)) ? 3 + (items[0].addInfos.size() * 2) : 3));
        return (((null != items) && (items.size() > 0)) ? 3 + (items[0].addInfos.size() * 2) : 3);
    }

    /*
     * Retrieves the number of additional columns depending on the addInfos list in the items.
     */
    public Integer getAddColumns()
    {
        return (((null != items) && (items.size() > 0)) ? items[0].addInfos.size() : 0);
    }

    /*
     * Retrieves the width for the complete hover area.
     */
    public String getWidth()
    {
        Integer width = 300 + (getAddColumns() * 65);
        
        String hoverWidth = String.valueOf(width) + 'px';
        return hoverWidth;
    }

    /*
     * Retrieves the offset for the arrow of the hover area.
     */
    public String getArrowOffset()
    {
        Integer offset = 309 + (getAddColumns() * 65);
        
        String arrowOffset = String.valueOf(offset) + 'px';
        return arrowOffset;
    }

    /*
     * Retrieves the widths for all columns.
     */
    public String getColumnsWidth()
    {
        Integer columns = getColumns();
        Integer count = (columns / 2) + 1;
        Integer width = (100 / count) - 4;
        Integer lastWidth = 100 - 4 - (2 * width);
        
        String columnsWidth = '';
        for (Integer i=0; i<count; i++)
        {
            columnsWidth += String.valueOf(width) + '%,4%,';
        }
        columnsWidth = columnsWidth.subString(0, columnsWidth.length() - 4);
        System.debug('#### getColumnsWidth(): columnsWidth -> ' + columnsWidth);
        return columnsWidth;
    }

    /*
     * Retrieves the classes for all columns.
     */
    public String getColumnClasses()
    {
        Integer columns = getColumns();
        Integer count = (columns / 2) + 1;
        
        String columnClasses = 'columnClass1,normalColumn,';
        for (Integer i=0; i<count-1; i++)
        {
            columnClasses += 'normalColumn,normalColumn,';
        }
        columnClasses = columnClasses.subString(0, columnClasses.length() - 14);
        return columnClasses;
    }
    
    // Data Container
    public class HoverData
    {
        public String label {get;set;}
        public String info {get;set;}
        public String entryid {get;set;}
        public Boolean selected {get;set;}
        public List<String> addInfos {get; set;}
        
        HoverData(String label, String info, String entryid)
        {
            this.label = label;
            this.info  = info;
            this.entryid = entryid;
            this.selected = false;
            this.addInfos = new List<String>();
        }

        HoverData(String label, String info, String entryid, List<String> addInfos)
        {
            this.label = label;
            this.info  = info;
            this.entryid = entryid;
            this.selected = false;
            this.addInfos = addInfos;
        }
    }    
    
}
