@isTest
public class CdeOrderInstallationOrderControllerTest {
    
    testmethod static public void testCdeOrderInstallationOrderController(){
        SCHelperTestClass3.createCustomSettings('XX', true); // Taken from SCbOrderTest.cls
        
        SCInstalledBaseLocation__c location = new SCInstalledBaseLocation__c(ID2__c = '123');
        insert location;
        
        SCInstalledBaseLocationContact__c contact = new SCInstalledBaseLocationContact__c(InstalledBaseLocation__c = location.ID);
        insert contact;
        
        Brand__c brand = new Brand__c(name = 'Brand');

        SCProductModel__c product = new SCProductModel__c(name = 'product', Brand__c=brand.ID);
        insert product;
        
        SCInstalledBase__c ib = new SCInstalledBase__c(ProductModel__c = product.ID, InstalledBaseLocation__c = location.ID);
        insert ib;
        
        SCOrder__c order = new SCOrder__c();
        insert order;
        
        SCOrderItem__c item = new SCOrderItem__C(InstalledBase__c = ib.ID, Order__c = order.ID,ID2__c= '345', IBProductModel__c= product.id);
        insert item;
        
        PageReference pageRef = new PageReference('/apex/CdeOrderInstallationOrder?id'+order.ID);
		Test.setCurrentPage(pageRef);
        ApexPages.standardController Stdcontroller = new ApexPages.standardController(order);
        CdeOrderInstallationOrderController controller = new CdeOrderInstallationOrderController(Stdcontroller);

    }
}
