/*
 * @(#)SCbtcGeocodeAccount.cls
 * 
 * Copyright 2011 by GMS Development GmbH 
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Batch job to geocode accounts
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
global class SCbtcGeocodeAccount extends SCbtcGeocodeBase 
       implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts
{
    private ID batchprocessid = null;
    String country = 'DE';
    Integer max_cycles = 0;
    String email = null;
    
    // the counter of the calls to the execute function
    // by the countries with double web call for geo coding 
    // such as NL, UK this counter is used to skip the even
    // record to allow the odd record the making of the second web call
    Integer callCnt = 0;
    // The limit of out calls. Its value is read from ApplicationSettings
    Integer limitOfOutCalls = 100;
    String emailData = '';

    // Subject of the email
    global String jobinfo;    
    global String summary;
    
        
    // The address validation results (temporary)
    public AvsResult addrResult {get; set;}
    public AvsResult geocodingResult {get; set;}
    private AvsAddress checkAddress = null;
    private AvsAddress checkAddress2 = null; // because geocode sets GeoX and GeoY to 0 in the input data

    List<String> accountIdList = null;    // accountIdList = null, normal batch call
    String msg = '';
    /**
     * if the contract id is null the batch job for all contract visit dates is started
     * if the contract id is not null the batch job for the given contract is started
     */
    private Id accountId = null;

    // Object for application settings
    private static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();

    // the intern account object
    public Account account { get; private set; }

    
   /**
    * API entry for starting the batch job for contracts
    * @param processingMode    for geocoding records with GeoStatus__c <> 'Invalid'
    *                          ALL     geocdes all records (also the already geocoded)
    *                          SIM_ALL     like ALL but only simulation
    *
    *                          for geocoding records with GeoStatus__c = null
    *                          ALLNEW  geocodes only new records not yet geocoded (geox = 0 and geoy =0). 
    *                          LAST_TWO_YEARS geocodes only records created in the last two years
    *                          SIM_ALLNEW  like ALLNEW but only simulation
    *                          SIM_LAST_TWO_YEARS like LAST_TWO_YEARS but only simulation
    *                          
    *                          for geocoding records with GeoStatus__c = 'Invalid'
    *                          ALL_INVALID   
    *                          LAST_TWO_YEARS_INVALID geocodes only records created in the last two years
    *                          SIM_ALL_INVALID  like ALL_INVALID but only simulation
    *                          SIM_LAST_TWO_YEARS_INVALID like LAST_TWO_YEARS but only simulation
    *
    *                        
    * @param country    the country code (e.g. DE, GB)
    * @param email      the email of the operator to get the message after finishing the job
    * @param max_cycles 
    * @param traceMode  if = 'trace' or 'test' the traces are written into a log file 
    */
    WebService static ID asyncGeocodeAll(String processingMode, String country, String email, Integer max_cycles, String traceMode)
    {
        if(!isAllowed(processingMode))
        {
            SCfwException e = new SCfwException(true, 'The processing mode \'' + processingMode + '\' is not allowed!');
            throw e;
        }
        System.debug('###traceMode: ' + traceMode);
        SCbtcGeocodeAccount btc = new SCbtcGeocodeAccount(processingMode, country, email, max_cycles, traceMode);

        // only one account in one call to execute
        btc.batchprocessid  = btc.executeBatch(1);
        return btc.batchprocessid;
    } // asyncGeocodeAll

    /**
     * checks whether the processing mode is allowed.
     * It returns true if it is allowed, else it returns false
     */
    private static Boolean isAllowed(String processingMode)
    {
       String modes = 'ALL, SIM_ALL, ALL_NEW, LAST_TWO_YEARS, SIM_ALLNEW, SIM_LAST_TWO_YEARS,'
                   + 'ALL_INVALID, LAST_TWO_YEARS_INVALID, SIM_ALL_INVALID, SIM_LAST_TWO_YEARS_INVALID';
       List<String> modeList = modes.split(',');
       Boolean ret = SCfwCollectionUtils.isContained(processingMode, modeList);
       return ret;
    }



    /**
     * Geocode asynchronically one account found on the base of account id.
     * If the account id is not null the batch job for the given account is started.
     * @param accountId
     * @param traceMode    'trace' or 'test' cause the trace to a log file
     */
    public static ID asyncGeocodeAccount(Id accountId, String traceMode)
    {
        String country = 'XX'; // dummy only - will be determined from the account
        if(accountId != null)
        {
            List<Account> cList = [Select BillingCountry__c from Account where Id = :accountId];
            if(cList.size() > 0)
            {
                country = cList[0].BillingCountry__c;
                Integer max_cycles = 1;
                String processingMode = null;
                String email = null;
                SCbtcGeocodeAccount btc = new SCbtcGeocodeAccount(accountId, processingMode, country, email, max_cycles, traceMode);
                // only one order in one call to execute
                btc.batchprocessid = btc.executeBatch(1);
                return btc.batchprocessid;
            }    
        }
        return null;
    } // asyncGeocodeAccount


    /**
     * geocodes asynchronically on the base of list of account
     * @param account list
     */
    WebService static ID asyncGeocodeAccounts(List<String> accountIdList)
    {
        if(accountIdList == null || accountIdList.size() == 0)
        {
            return null;
        }   
        SCbtcGeocodeAccount btc = new SCbtcGeocodeAccount(accountIdList, 'trace');
        // only one order in one call to execute
        btc.batchprocessid  = btc.executeBatch(1);
        return btc.batchprocessid;
    } // asyncGeocodeAccount

 
    /**
     * Synchronically calls geocoding of the account list
     *
     * @param account list
     */ 
    Webservice static void syncGeocodeAccounts(List<String> accountIdList)
    {
        if(accountIdList == null || accountIdList.size() == 0)
        {
            return;
        }   
        System.debug('###......accountIdList: ' + accountIdList);
        Integer max_cycles = 1;
        SCbtcGeocodeAccount btc = new SCbtcGeocodeAccount(accountIdList, 'test');        
            
        List<Account> accountList = [select ID, BillingCity, BillingCountry__c, BillingCounty__c,  BillingCountryState__c,
                                            BillingDistrict__c, BillingExtension__c, BillingFlatNo__c,
                                            BillingHouseNo__c, BillingPostalCode,  BillingStreet,
                                            GeoX__c, GeoY__c, GeoApprox__c, GeoStatus__c
                                            from Account 
                                            where 
//                                            GeoStatus__c = null
//                                            and (GeoX__c = 0 or GeoX__c = null )and (GeoY__c = 0 or GeoY__c = null) and
                                            Id in :accountIdList];
        
        List<SCInterfaceLog__c> interfaceLogList = new List<SCInterfaceLog__c>();
        for(Account acc: accountList)
        {
            // validate a account
            btc.debug('account id: ' + acc.Id);
            Boolean sync = true;
            btc.country = acc.BillingCountry__c;
            if(btc.country != null)
            {
                btc.country = btc.country.toLowerCase();
            }

            btc.geocode(acc, sync, interfaceLogList);
        }
        upsert accountList;
        insert interfaceLogList;
    }
    
    /**
     * Synchronically calls geocoding of the account
     */ 
    Webservice static void syncGeocodeAccount(String accountId)
    {
        if(accountId == null || accountId == '')
        {
            return;
        }   
        System.debug('###......accountId: ' + accountId);
        Integer max_cycles = 1;
        String processingMode =  null;
        String email = null;
            
        List<Account> accountList = [select ID, BillingCity, BillingCountry__c, BillingCounty__c, BillingCountryState__c,
                                            BillingDistrict__c, BillingExtension__c, BillingFlatNo__c,
                                            BillingHouseNo__c, BillingPostalCode,  BillingStreet,
                                            GeoX__c, GeoY__c, GeoApprox__c, GeoStatus__c
                                            from Account 
                                            where Id = :accountId];
        

        for(Account acc: accountList)
        {
            // geocode an account
            String countryLoc = acc.BillingCountry__c;
            
            SCbtcGeocodeAccount btc = new SCbtcGeocodeAccount(accountId, processingMode, countryLoc, email, max_cycles, 'test');        
            btc.debug('account id: ' + acc.Id);
            Boolean sync = true;
            btc.geocode(acc, sync, null);
        }
        upsert accountList;
    }

    /**
    * Constructors.
    * The parameter names are explicitly written.
    */
    public SCbtcGeocodeAccount(String processingMode, String country, String email, Integer max_cycles, String traceMode)
    {
        this(null, processingMode, country, email, max_cycles, traceMode);
    }

    public SCbtcGeocodeAccount(List<String> accountIdList, String traceMode)
    {
        // in case of this call the country is not regarded
        this(null, 'ALL', 'XX', null, 0, traceMode);
        this.accountIdList = accountIdList;
    }


    /**
     * Constructor 
     * @param accountId set for working out the specified account
     * @param processingMode
     * @param country
     * @param email
     * @param max_cycles
     * @param traceMode
     */
    public SCbtcGeocodeAccount(Id accountId, String processingMode, String country, String email, Integer max_cycles, String traceMode)
    {    
        this.accountId = accountId;
        if(country != null)
        {
            country = country.toLowerCase();
        }    
        this.country = country;
        this.processingMode = processingMode;
        if(this.processingMode != null)
        {
            this.processingMode = this.processingMode.toUpperCase();
        }
        this.email = email;
        this.max_cycles = max_cycles;
        this.traceMode = traceMode;
        System.debug('###traceMode: ' + traceMode);
        debug('traceMode: ' + traceMode);
        Decimal decLimitOfOutCalls = appSettings.BATCH_GEOCODE_WEB_CALLS_DAILY_MAXIMUM__c;
        if(decLimitOfOutCalls != null)
        {
            limitOfOutCalls = decLimitOfOutCalls.intValue();
        }
    }

   /*
    * Called by the framework when the batch job is started. We construct the soql statement
    * for reading the resource assignments.
    * @param BC the batch context
    * @return the query locator 
    */
    global override Database.QueryLocator start(Database.BatchableContext BC)
    {
        debug('start');
        /* To check the syntax of SOQL statement */
        List<Account> accountList = [select ID, BillingCity, BillingCountry__c, BillingCounty__c, BillingCountryState__c,
                                            BillingDistrict__c, BillingExtension__c, BillingFlatNo__c,
                                            BillingHouseNo__c, BillingPostalCode,  BillingStreet,
                                            GeoX__c, GeoY__c, GeoApprox__c, GeoStatus__c
                                            from Account 
                                            where  
                                            BillingCountry__c = :country
                                            and GeoCode__c = true 
                                            and GeoStatus__c = null
                                            and (GeoX__c = 0 or GeoX__c = null )and (GeoY__c = 0 or GeoY__c = null)
                                            limit 1 ];
        debug('start: after checking statement');
        /**/
        String query = null;
        if(accountId == null)
        {
            query = ' select ID, BillingCity, BillingCountry__c, BillingCounty__c, BillingCountryState__c, '
                + ' BillingDistrict__c, BillingExtension__c, BillingFlatNo__c, '
                + ' BillingHouseNo__c, BillingPostalCode,  BillingStreet, '
                + ' GeoX__c, GeoY__c, GeoApprox__c, GeoStatus__c '
                + ' from Account '
                + ' where  '
                + ' BillingCountry__c = \'' + country + '\'';
            if(processingMode != null 
                && (processingMode.equalsIgnoreCase('ALL')
                    || processingMode.equalsIgnoreCase('SIM_ALL')))
            {
                query += ' and GeoStatus__c <> \'Invalid\' ';
            }    
 
            if(processingMode != null 
                && (processingMode.equalsIgnoreCase('ALL_INVALID')
                    || processingMode.equalsIgnoreCase('SIM_ALL_INVALID')
                    || processingMode.equalsIgnoreCase('LAST_TWO_YEARS')
                    || processingMode.equalsIgnoreCase('SIM_LAST_TWO_YEARS')))
            {
                query += ' and GeoStatus__c = \'Invalid\' ';
            }    

            if(processingMode != null 
                && (processingMode.equalsIgnoreCase('LAST_TWO_YEARS')
                    || processingMode.equalsIgnoreCase('SIM_LAST_TWO_YEARS')))
            {
                query += ' and GeoCode__c = true ';
                query += ' GeoStatus__c = null ';
         /* 
             Mö 26.10.11 temp. x/y nicht prüfen, da ~1500 Sätze nur mit x oder y gefüllt sind:
                 + ' and (GeoX__c = 0 or GeoX__c = null )and (GeoY__c = 0 or GeoY__c = null) '; 
         */

            }    
                
            if(processingMode != null 
                && (processingMode.equalsIgnoreCase('ALLNEW')
                    || processingMode.equalsIgnoreCase('SIM_ALLNEW')
                    || processingMode.equalsIgnoreCase('SIM_LAST_TWO_YEARS')))
            {            
                query += ' and GeoStatus__c = null '
                      + ' and (GeoX__c = 0 or GeoX__c = null )and (GeoY__c = 0 or GeoY__c = null) ';
            }    
            query += ' order by id ';
            
            if(max_cycles > 0 )
            {
                query += ' limit ' + max_cycles;
            }
        }
        else
        {
            // specified account
            query = ' select ID, BillingCity, BillingCountry__c, BillingCounty__c, BillingCountryState__c, '
                + ' BillingDistrict__c, BillingExtension__c, BillingFlatNo__c, '
                + ' BillingHouseNo__c, BillingPostalCode,  BillingStreet, '
                + ' GeoX__c, GeoY__c, GeoApprox__c, GeoStatus__c '
                + ' from Account '
                + ' where  '
                + ' BillingCountry__c = \'' + country + '\''
                + ' and id = \'' + accountId + '\'';
                if(max_cycles > 0 )
                {
                    query += ' limit ' + max_cycles;
                }
        }        
        if(accountIdList != null)
        {
            debug('accountIdList: ' + accountIdList);
            return Database.getQueryLocator([select ID, BillingCity, BillingCountry__c, BillingCounty__c,
                                            BillingCountryState__c,
                                            BillingDistrict__c, BillingExtension__c, BillingFlatNo__c,
                                            BillingHouseNo__c, BillingPostalCode,  BillingStreet,
                                            GeoX__c, GeoY__c, GeoApprox__c
                                            from Account 
                                            where  
//                                            GeoStatus__c = null
//                                            and (GeoX__c = 0 or GeoX__c = null )and (GeoY__c = 0 or GeoY__c = null) and
                                            Id in :accountIdList]);
        }
        debug('query: ' + query);
        return Database.getQueryLocator(query);
    } // start
   /*
    * Called for each batch of records to process.
    * @param BC the batch context
    * @param scope the list records to be processed
    */
    global override void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        debug('execute');
        breakJobByExhaustedDailyCallouts(BC, limitOfOutCalls, callCnt);

        Integer size = scope.size();
        if(size > 1)
        {
            System.debug('###The size ought to be 1 but it is: ' + size);
             throw new SCfwException('Batch for geo coding executed with scope =' + size + 
             '. It ought to be executed with the size 1');
        }
        Boolean sync = false;
        for(sObject res: scope)
        {
            debug('sObject: ' + res);
            Account workAccount = (Account)res;
            country = workAccount.BillingCountry__c;
            if(country != null)
            {
                country = country.toLowerCase();
            }
            geocode(workAccount, sync, null);
        }// for

        //Bulkify_Apex_Methods_Using_Collections_In_Methods
        //update account;
    } // execute

   /*
    * Called by the framework when the batch job has been completed. 
    * We send an e-mail notification about the status
    * @param BC the batch context
    */
    global override void finish(Database.BatchableContext BC)
    {
        // Get the ID of the AsyncApexJob representing this batch job and  
        // query the AsyncApexJob object to retrieve the current job's information.  
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,  CreatedDate, TotalJobItems, CreatedBy.Email  from AsyncApexJob where Id =:BC.getJobId()];

        // Send an email to the Apex job's submitter notifying of job completion.  
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        String[] toAddresses;
        if(email == null)
        {
            toAddresses = new String[] {a.CreatedBy.Email};
        }
        else
        {
            toAddresses = new String[] {email};
        }
        
        mail.setToAddresses(toAddresses);
        jobinfo = 'Job SCbtcGeocodeAccount at ' + a.CreatedDate;
        mail.setSubject(jobinfo + ' with status[' + a.Status + ']');
        
        String text = 'The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures. geocoded items\n\n';
//        text += 'id|name|Resource|Employee|Country|CountryState|County|PostalCode|City|District|Street|HouseNo|Extension|geoY|geoX| ---> |geoY|geoX|Country|CountryState|County|PostalCode|City|District|Street|HouseNo|Extension\n';
//        text += summary;        
        String fieldSeparator = '|';
        text += '\n\n' + getGridTitle(emailTitlePrefix, fieldSeparator, null);
        text += emailData;

        mail.setPlainTextBody(text);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
    } // finish

    public static final String STEP_BEGIN = 'BEGIN';
    public static final String STEP_DBL_FIRST = 'DBL_FIRST';
    public static final String STEP_DBL_SECOND = 'DBL_SECOND';
    public static final String STEP_SINGLE = 'SINGLE';
    /**
    * Geocodes an account
    *
    * @param workAccount    the account to geocode
    * @param sync           true if the call is synchron
    * @param interfaceLogList the interface log list, used only by syncron calls, for batch job always null
    *
    */
    public void geocode(Account workAccount, Boolean sync, List<SCInterfaceLog__c> interfaceLogList)
    {
        // increase the counter of outgoing web calls
        debug('geocode');
        callCnt++;
        
        DateTime start = Datetime.now();
        Boolean thrownException = false;
        Boolean secondCallMade = false;
        String resultInfo = '';
        Integer count = 1;
        String data = '';
        String step = STEP_BEGIN;
        try
        {
            if(isDoubleCall(country))
            {
                debug('double country: ' + country);
                // we make geo coding for an every odd record from batch
                // because we need two outgoing web calls.
                // The first web call we do by odd number of records.
                // The second web call we do by even number of records
                // for the result we got by the previous call by the odd number.
                if(isOdd(callCnt) || sync)
                {
                    step = STEP_DBL_FIRST;

                    // checking
                    debug('odd or sync');
                    // get the list or moniker
                    account = (Account)workAccount;
                    SCAddressValidation validation = new SCAddressValidation();
                    checkAddress = getAddr(account);
                    debug('account: ' + account);
                    debug('check address: ' + checkAddress);
                    checkAddress2 = getAddr(account);

                    // prevent from taking addrResult of the first account of previous pair of accounts
                    addrResult = null;
                    geocodingResult = null;
                    addrResult = validation.check(checkAddress2);
                    if(addrResult.status == AvsResult.STATUS_ERROR)
                    {
                    	String error = addrResult.statusInfo;
                    	throw new SCfwException(error);
                    }
                    if(addrResult.status == AvsResult.STATUS_EMPTY)
                    {
                        debug('the address has been not found: ' + checkAddress2);
                    }
                    debug('addrResult: ' + addrResult);
                    if(addrResult.items != null && addrResult.items.size() > 0)
                    {
                        debug('the first call: addrResult.items[0] : ' + addrResult.items[0]);
                    }
                }
                
                if(isEven(callCnt) || sync)
                {
                    step = STEP_DBL_SECOND;
                    
                    // geocoding
                    debug('even or sync');
                    // get the geocoding
                    SCAddressValidation validation = new SCAddressValidation();
                    if(addrResult != null && addrResult.items != null && addrResult.items.size() > 0
                        && addrResult.status != AvsResult.STATUS_EMPTY)
                    {
                        Integer indexOfAddr = getIndexQAS(checkAddress2, addrResult);
                        AvsAddress a = addrResult.items[indexOfAddr];
                        debug('the second call: addrResult.items[' + indexOfAddr + '] : ' + addrResult.items[indexOfAddr]); 
                        
                        geocodingResult = validation.geocode(a);
                        debug('geocodingResult: ' + geocodingResult);
                        debug('geocodingResult.size(): ' + geocodingResult.items.size());
                        resultInfo = setGeoCode(sync, isDoubleCall(country), checkAddress, addrResult, geocodingResult, account);
                        secondCallMade = true;    
                    }
                    else
                    {
	                    if(addrResult.status == AvsResult.STATUS_ERROR)
	                    {
	                    	String error = addrResult.statusInfo;
	                    	throw new SCfwException(error);
	                    }
                        resultInfo = 'The address could not be found. ';
                    }    
                }
            }
            else
            {
                step = STEP_SINGLE;
                
                // an only geocoding call
                account = (Account)workAccount;
                SCAddressValidation validation = new SCAddressValidation();
                checkAddress = getAddr(account);
                debug('account: ' + account);
                debug('check address: ' + checkAddress);
                
                checkAddress2 = getAddr(account);
                geocodingResult = validation.geocode(checkAddress2); // geocode sets GeoX and GeoY to 0
                if(geocodingResult.status == AvsResult.STATUS_ERROR)
                {
                	String error = geocodingResult.statusInfo;
                	throw new SCfwException(error);
                }
                debug('geocodingResult: ' + geocodingResult);
                addrResult = null;
                // getIndex is called implicitly by setGeoCode 
                resultInfo = setGeoCode(sync, isDoubleCall(country), checkAddress, addrResult, 
                    geocodingResult, account);
            }
            
            // copying of geo coordinates to the locations
            // having the same address
            geocodeInstalledBaseLocations(account);
        }
        catch(Exception e)
        {
            thrownException = true;
            resultInfo += 'Exception: ' + e.getMessage();
            String prevMode = traceMode;
            traceMode = 'test';
            debug('resultInfo:' + resultInfo);
            traceMode = prevMode;
            count = 0;
        }
        finally
        {
            /**
            E000 Success
            E001 Success with Info
            E100 Service not reachable
            E101 Service processing failed
            */
            String resultCode = 'E000';
            if(thrownException
               || resultInfo.startsWith('Invalid'))
            {
                resultCode = 'E101';
            }
            // protocoll only the events that something happend
            if(!isDoubleCall(country) 
                || isDoubleCall(country) && isEven(callCnt)
                || thrownException)
            {
                debug('last check address: ' + checkAddress);
                String dataFieldSeparator = ',';
                Id accId = null;
                if(account != null)
                {
                    accId = account.Id;
                }
                data = getData(checkAddress, addrResult, geocodingResult, accId, dataFieldSeparator, null);
                String emailFieldSeparator = '|';
                emailData += '\n' + getEmailData(checkAddress, addrResult, geocodingResult, 
                    accId, emailFieldSeparator, null, 'step: ' + step + '; ' + resultInfo);
                if(account !=  null)
                {
                    logBatchInternal(interfaceLogList, 'GEOCODE_ACCOUNT', 'SCbtcGeocodeAccount',
                                account.Id, null, resultCode, 
                                resultInfo, data, start, count); 
                }
                else
                {
                    logBatchInternal(interfaceLogList, 'GEOCODE_ACCOUNT', 'SCbtcGeocodeAccount',
                                null, null, resultCode, 
                                resultInfo, data, start, count); 
                }                
            }                    
        }     
    } // geocode

    /**
    * Gets an address from an account
    */
    public AvsAddress getAddr(Account acc)
    {
        AvsAddress a = new AvsAddress();
        
        a.country = acc.BillingCountry__c;
        a.countrystate = acc.BillingCountryState__c;
        a.county = acc.BillingCounty__c;
        a.postalcode = acc.BillingPostalCode;
        a.city = acc.BillingCity;
        a.district = acc.BillingDistrict__c;
        a.street = acc.BillingStreet;
        a.housenumber = acc.BillingHouseNo__c;
        a.GeoX = acc.GeoX__c;
        a.GeoY = acc.GeoY__c;
        a.GeoApprox = acc.GeoApprox__c;
        
        return a;
    } // getAddr
    
    /**
    * Set coordinaten into an account.
    * @param sync true if the synchron call, else false
    * @param doubleCallOut true if on the reason of country we need to have two web calls
    * @param checkAddress the addresss to geocode
    * @param addrResult the result of check call
    * @param geocodingResult the result of geocode call
    * @param the account, we now working out
    * @return the text for the SCInterfaceLog.ResultInfo__c
    */
    public String setGeoCode(Boolean sync, Boolean doubleCallOut, AvsAddress checkAddress, AvsResult addrResult, 
        AvsResult geocodingResult, Account account)
    {
        SCbtcGeocodeBase.GeoCodeHolder gcHolder = new SCbtcGeocodeBase.GeoCodeHolder();
        String ret = prepareSetGeoCode(sync, doubleCallOut, checkAddress, addrResult, 
                                        geocodingResult, gcHolder);
        if(!sync || sync)
        {
            if(processingMode == null 
                || !processingMode.startsWith('SIM'))
            {
                account.GeoStatus__c = gcHolder.geoStatus;
                if(gcHolder.geoApprox != null)
                {
                    account.GeoApprox__c = gcHolder.geoApprox;
                }    
                account.GeoX__c = gcHolder.geoX;
                account.GeoY__c = gcHolder.geoY;
                update account; //Bulkify_Apex_Methods_Using_Collections_In_Methods
            }    
        }    
        return ret;
    }//setGeoCode

    /**
    *    Geocodes the installed base location dependent on the account.
    *    @param a, an account
    */
    public void  geocodeInstalledBaseLocations(Account a)
    {
        List<SCInstalledBaseLocation__c> readList= [SELECT Id, Address__c, BuildingAge__c, BuildingDate__c, City__c, Country__c, 
                            County__c, District__c, Extension__c, FlatNo__c, Floor__c, GeoY__c, GeoX__c, GeoStatus__c, 
                            GeoApprox__c, HouseNo__c, ID2__c, Name, LocName__c, OwnerId, PostalCode__c, 
                            CountryState__c, Status__c, Street__c 
            FROM SCInstalledBaseLocation__c 
            where id in (select InstalledBaseLocation__c   
                        from SCInstalledBaseRole__c
                        where Account__c = :a.Id)];
        debug('InstalledBaseLocation readList: ' + readList);
        List<SCInstalledBaseLocation__c> changedList = new List<SCInstalledBaseLocation__c>();
        for(SCInstalledBaseLocation__c IBLoc: readList)
        {
            debug('account: ' + a);
            debug('location: ' + IBLoc);
            if(isTheSameAddress(a, IBLoc))
            {
                debug('The same address');
                changedList.add(IBLoc);
                IBLoc.GeoX__c = account.GeoX__c;
                IBLoc.GeoY__c = account.GeoY__c;
                IBLoc.GeoStatus__c = account.GeoStatus__c;
            }
        }
        if(changedList.size() > 0)
        {
            debug('update installedBaseLocations: ' + changedList);
            update changedList;
        }

    }
    /**
    * Determines whether the account the installed base location has as the same address as the account
    * @param a, the account
    * @param i, the installed base location
    */
    public Boolean isTheSameAddress(Account a, SCInstalledBaseLocation__c i)
    {
        Boolean ret = false;
        /* let it remain for the test purposes
        if(!isEqual(a.BillingCountry__c, i.Country__c))
        {
            debug('different countries a: ' + a.BillingCountry__c + ', i: ' + i.Country__c);
        }
        if(!isEqual(a.BillingCountryState__c, i.CountryState__c))
        {
            debug('different country states a: ' + a.BillingCountryState__c + ', i: ' + i.CountryState__c);
        }
        if(!isEqual(a.BillingCounty__c, i.County__c))
        {
            debug('different counties states a: ' + a.BillingCounty__c + ', i: ' + i.County__c);
        }
        if(!isEqual(a.BillingPostalCode, i.PostalCode__c))
        {
            debug('different postal codes a: ' + a.BillingPostalCode + ', i: ' + i.PostalCode__c);
        }
        if(!isEqual(a.BillingCity, i.City__c))
        {
            debug('different cities a: ' + a.BillingCity + ', i: ' + i.City__c);
        }
        if(!isEqual(a.BillingDistrict__c, i.District__c))
        {
            debug('different districts a: ' + a.BillingDistrict__c + ', i: ' + i.District__c);
        }
        if(!isEqual(a.BillingStreet, i.Street__c))
        {
            debug('different streets a: ' + a.BillingStreet + ', i: ' + i.Street__c);
        }
        if(!isEqual(a.BillingHouseNo__c, i.HouseNo__c))
        {
            debug('different house no a: ' + a.BillingHouseNo__c + ', i: ' + i.HouseNo__c);
        }
        */
        if(isEqual(a.BillingCountry__c, i.Country__c)
            && isEqual(a.BillingCountryState__c, i.CountryState__c)
            && isEqual(a.BillingCounty__c, i.County__c)
            && isEqual(a.BillingPostalCode, i.PostalCode__c)
            && isEqual(a.BillingCity, i.City__c)
            && isEqual(a.BillingDistrict__c, i.District__c)
            && isEqual(a.BillingStreet, i.Street__c)
            && isEqual(a.BillingHouseNo__c, i.HouseNo__c))
        {
            ret = true;
        }    
        return ret;    
    }
    
    /**
    * Determines whether the account field is equal the the counterpart field of the installed base
    */
    public Boolean isEqual(String a, String b)
    {
        Boolean ret = false;
        if(a == null)
        {
            a = '';
        }
        else
        {
            a = a.trim();
            if(a == '-')
            {
                a = '';
            }
        }
        if(b == null)
        {
            b = '';
        }
        else
        {
            b = b.trim();
            if(b == '-')
            {
                b = '';
            }
        }
        if(a.equalsIgnoreCase(b))
        {
            ret = true;
        }
        return ret;
    }
    

}
