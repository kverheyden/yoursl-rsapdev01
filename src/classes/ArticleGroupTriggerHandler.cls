public with sharing class ArticleGroupTriggerHandler{

	private static final SObjectField DEVELOPERNAME_FIELD = ArticleGroup__c.PictureDocDevName__c;
	private static final SObjectField IDTEXT_FIELD = ArticleGroup__c.Picture__c;
	private static final DeveloperNameIdReferenceHandler pictureReferenceHandler = new DeveloperNameIdReferenceHandler(ArticleGroup__c.SObjectType, DEVELOPERNAME_FIELD, IDTEXT_FIELD);


	public with sharing class ArticleGroupBIHandler extends TriggerHandlerBase {
		public override void mainEntry(TriggerParameters tp) {
			pictureReferenceHandler.setIdTextByDeveloperName(tp.oldMap, tp.newMap);
		}
	}

	public with sharing class ArticleGroupBUHandler extends TriggerHandlerBase {
		public override void mainEntry(TriggerParameters tp) {
			pictureReferenceHandler.setIdTextByDeveloperName(tp.oldMap, tp.newMap);
		}
	}
/*
	public static List<SObject> getObjectsWithChangedFields(SObjectField soField, TriggerParameters tp) {
		List<SObject> changedObjects = new List<SObject>();
		for (SObject objectNew : tp.newList) {
			SObject objectOld = tp.oldMap.get(objectNew.Id);
			if (objectOld.get(soField) != objectNew.get(soField)) 
				changedObjects.add(objectNew);
		}
		return changedObjects;
	}
*/
}
