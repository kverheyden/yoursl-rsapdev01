/*
 * @(#)SCOrderInboxControllerTest.cls
 *
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author Dietrich Herdt <dherdt@gms-online.de>
 * @author Sebastian Schrage  <sschrage@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCOrderInboxControllerTest
{
    private static SCOrderInboxController orderInboxController;

    /**
     * SCOrderInboxController under positiv test 1
     */
    static testMethod void orderInboxControllerPositiv1()
    {
        SCHelperTestClass.createTestUsers(true);
        // set test user from SCHelperTestClass --> nlburgt
        User testUser = SCHelperTestClass.users.get(6);
    
        System.runAs (testUser)
        {       
        /*CCE
            SCHelperTestClass.createOrderTestSet(true);
            Test.startTest();
            
            orderInboxController = new SCOrderInboxController();
            
            // 99 = next 14 days
            orderInboxController.curViewInterval = '99';
            // select all status of appointment
            orderInboxController.curViewStatus = '0';          

            //DOMVAL_ORDERSTATUS_PLANNED_FOR_PRECHECK = '5509';
            //DOMVAL_ORDERSTATUS_RELEASED_FOR_PROCESSING = '5510';
            
            orderInboxController.refreshList();
            // select appointment with status PLANNED_FOR_PRECHECK (5509)
            
            orderInboxController.selectedApp = orderInboxController.appObjects.get(1).appointment.Id;
            
            // select appointment
            orderInboxController.selectItem();
            // assign selected appointment --> DOMVAL_ORDERSTATUS_RELEASED_FOR_PROCESSING
            orderInboxController.assignAppointment();
            System.assertEquals(orderInboxController.curAppObj.status, 
                                Integer.valueOf(SCfwConstants.DOMVAL_ORDERSTATUS_RELEASED_FOR_PROCESSING));
            // accept selected appointment --> DOMVAL_ORDERSTATUS_ACCEPTED
            orderInboxController.acceptAppointment();
            System.assertEquals(orderInboxController.curAppObj.status, 
                                Integer.valueOf(SCfwConstants.DOMVAL_ORDERSTATUS_ACCEPTED));
            // accept selected appointment --> DOMVAL_ORDERSTATUS_PLANNED_FOR_PRECHECK
            orderInboxController.rejectAppointment();
            System.assertEquals(orderInboxController.curAppObj.status, 
                                SCOrderInboxController.STAT_NONE);

            Test.stopTest();
            */
        }
    }
    
    /**
     * SCOrderInboxController under positiv test 2
     */
     /* GMSNA 24.05.2013 disabled - to be checked
    static testMethod void orderInboxControllerPositiv2()
    {
        SCHelperTestClass.createTestUsers(true);
        // set test user from SCHelperTestClass --> nlburgt
        User testUser = SCHelperTestClass.users.get(6);
        
        System.runAs (testUser)
        {       
            SCHelperTestClass.createOrderTestSet(true);
            Test.startTest();
        
            orderInboxController = new SCOrderInboxController();

            // call all methods with null test
            // orderInboxController.locationMap;
            
            SCOrderInboxController.LocationObject locationTestObject = 
                    new SCOrderInboxController.LocationObject(2);
            
            List<SelectOption> testViewIntervalList = orderInboxController.viewInterval;

            // if (null == viewStatus)
            List<SelectOption> testViewStatusList = orderInboxController.viewStatus;
            
            // varios status calls
            orderInboxController.curViewStatus = '0';
            orderInboxController.refreshList();
            
            orderInboxController.selectedApp = orderInboxController.appObjects.get(1).appointment.Id;
            orderInboxController.selectItem();
            orderInboxController.refreshList();
            orderInboxController.curViewStatus = SCfwConstants.DOMVAL_ORDERSTATUS_PLANNED_FOR_PRECHECK;
            orderInboxController.refreshList();
            orderInboxController.curViewStatus = SCfwConstants.DOMVAL_ORDERSTATUS_RELEASED_FOR_PROCESSING;
            orderInboxController.refreshList();
            orderInboxController.curViewStatus = SCfwConstants.DOMVAL_ORDERSTATUS_ACCEPTED;
            orderInboxController.refreshList();
            orderInboxController.curViewStatus = SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED;
            orderInboxController.refreshList();
            orderInboxController.curViewStatus = SCfwConstants.DOMVAL_ORDERSTATUS_NOT_COMPLETED;
            orderInboxController.refreshList();
            System.assertNotEquals(null, orderInboxController.appObjects);

            Test.stopTest();
        }
    }  */      
    
    /**
     * SCOrderInboxController under positiv test 3.1
     */
    static testMethod void orderInboxControllerPositiv3_1()
    {
        SCHelperTestClass.createTestUsers(true);
        // set test user from SCHelperTestClass --> nlburgt
        User testUser = SCHelperTestClass.users.get(6);
        
        System.runAs (testUser)
        {       
            SCHelperTestClass.createOrderTestSet(true);
            Test.startTest();
        
            orderInboxController = new SCOrderInboxController();

            // call all methods with null test
            // varios interval calls
            orderInboxController.curViewInterval = '00';
            orderInboxController.refreshList();
            orderInboxController.curViewInterval = '01';
            orderInboxController.refreshList();
            orderInboxController.curViewInterval = '02';
            orderInboxController.refreshList();
            orderInboxController.curViewInterval = '10';
            orderInboxController.refreshList();
            orderInboxController.curViewInterval = '11';
            orderInboxController.refreshList();
            orderInboxController.curViewInterval = '12';
            orderInboxController.refreshList();
            System.assertNotEquals(null, orderInboxController.appObjects);
        
            Test.stopTest();
        }
    }        
    
    /**
     * SCOrderInboxController under positiv test 3.2
     */
    static testMethod void orderInboxControllerPositiv3_2()
    {
        SCHelperTestClass.createTestUsers(true);
        // set test user from SCHelperTestClass --> nlburgt
        User testUser = SCHelperTestClass.users.get(6);
        
        System.runAs (testUser)
        {       
            SCHelperTestClass.createOrderTestSet(true);
            Test.startTest();
        
            orderInboxController = new SCOrderInboxController();

            // call all methods with null test
            // varios interval calls
            orderInboxController.curViewInterval = '20';
            orderInboxController.refreshList();
            orderInboxController.curViewInterval = '30';                                    
            orderInboxController.refreshList();
            orderInboxController.curViewInterval = '31';                                    
            orderInboxController.refreshList();
            orderInboxController.curViewInterval = '32';                                    
            orderInboxController.refreshList();
            orderInboxController.curViewInterval = '33';                                    
            orderInboxController.refreshList();
            System.assertNotEquals(null, orderInboxController.appObjects);
        
            Test.stopTest();
        }
    }
    
    /**
     * @author: Sebastian Schrage <sschrage@gms-online.de>
     */
    static testMethod void orderInboxControllerPageCalls()
    {
        SCHelperTestClass.createTestUsers(true);
        // set test user from SCHelperTestClass --> nlburgt
        User testUser = SCHelperTestClass.users.get(6);
        
        System.runAs (testUser)
        {
            SCHelperTestClass.createOrderTestSet(true);
            Test.startTest();
            
            orderInboxController = new SCOrderInboxController();
            System.assertEquals(orderInboxController.getDifferentRescource(),'false');
            System.assertEquals(orderInboxController.getRescourceId(),null);
            // System.assertEquals(orderInboxController.getAllowedToChooseTheResource(),false);
            orderInboxController.getAllResourceList();
            orderInboxController.getSelectedResource();
            orderInboxController.setSelectedResource('ALL');
            
            try { orderInboxController.setSelectedResource('ALL'); }
            catch (Exception e) {}
            
            orderInboxController.ChangeResource();
            System.assertEquals(1, orderInboxController.pseudoList.size());
            
            Test.stopTest();
        }
    }
    
    
    /**
     * @author: Sebastian Schrage <sschrage@gms-online.de>
     */
    static testMethod void orderInboxControllerCodeCoverage1() { UPorderInboxControllerCodeCoverage(null,null); }
    static testMethod void orderInboxControllerCodeCoverage2() { UPorderInboxControllerCodeCoverage(0,null); }
    static testMethod void orderInboxControllerCodeCoverage3() { UPorderInboxControllerCodeCoverage(1,null); }
    static testMethod void orderInboxControllerCodeCoverage4() { UPorderInboxControllerCodeCoverage(2,'DE'); }
    
    private static void UPorderInboxControllerCodeCoverage(Integer mode, String country)
    {
        List<SCApplicationSettings__c> listAppSet = new List<SCApplicationSettings__c>();
        for (SCApplicationSettings__c appSetting : [SELECT Id, CUSTOMERPROPERTY__c, DEFAULT_COUNTRY__c, ORDERCREATION_MINDURATION__c,
                                                           RESOURCE_CHANGE_ALLOWED__c
                                                      FROM SCApplicationSettings__c])
        {
            appSetting.CUSTOMERPROPERTY__c = '0';
            appSetting.DEFAULT_COUNTRY__c = country;
            appSetting.RESOURCE_CHANGE_ALLOWED__c = mode;
            appSetting.ORDERCREATION_MINDURATION__c = 15;
            listAppSet.add(appSetting);
        }
        Database.update(listAppSet);
        
        orderInboxController = new SCOrderInboxController();
        
        Boolean tmp = orderInboxController.getAllowedToChooseTheResource();
        
        orderInboxController.setSelectedResource('test');
        orderInboxController.setSelectedResource(UserInfo.getUserId());
        
        Id tmpId = orderInboxController.globalSelectedResourceId;
        orderInboxController.globalSelectedResourceId = UserInfo.getUserId();
        if (mode > 0)          { System.assertEquals(UserInfo.getUserId(), orderInboxController.getRescourceId()); }
        else                   { System.assertEquals(null, orderInboxController.getRescourceId()); }
        orderInboxController.globalSelectedResourceId = tmpId;
    }
}
