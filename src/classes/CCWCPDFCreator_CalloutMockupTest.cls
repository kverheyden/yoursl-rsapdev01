@isTest                        
global class CCWCPDFCreator_CalloutMockupTest implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/pdf');
        Blob test = Blob.valueOf('test');
        
        res.setBody(EncodingUtil.base64Encode(test));
        res.setStatusCode(200);
        return res;
    }
}
