global class AlertBatchScheduler implements Schedulable { 
	
	
	global void execute(SchedulableContext sc) 
	{
		AlertBatch batch = new AlertBatch();
		Id batchId = Database.executeBatch(batch);
	}

}
