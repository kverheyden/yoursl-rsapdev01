@isTest
public class CoolerContractControllerTest {
    testmethod static public void CoolerContractController_WithoutAttachmentParameter(){
        Profile p = [select id from profile where name='System Administrator']; 
        User testUser = new User(alias = 'sta23', email='admin@testorg.com', 
            emailencodingkey='UTF-8', lastname='Testing123', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, 
            timezonesidkey='America/Los_Angeles', username='admin@cceag.de', isActive=true, ID2__c = '12345678', SalesOffice__c = '0363');
            
    	System.runAs(testUser) {
            Account acc = new Account(Name = 'Test Acc');
            insert acc;
            
            CDEOrder__c order = new CDEOrder__c(OwnerID = testUser.ID);
            insert order;
            
            SCProductModel__c product = new SCProductModel__c(
                Name          =  'Cooler',
                ID2__c        =  'D036900_4712'
            );
            insert product;
            
            CDEOrderItem__c item = new CDEOrderItem__c(
                CDEOrder__c = order.ID,
                ProductModel__c = product.ID,
                Quantity__c = 10
            );
            insert item;
            
            
            
            Test.startTest();
    		
            
            Pagereference LO_Page = Page.srLeihvertrag_Cooler_Muster_hdm_090714;
            Test.setCurrentPage( LO_Page );
    		
            
            
            CoolerContractController ctrl = new CoolerContractController(new ApexPages.StandardController(order));
    
            Test.stopTest();
    	}
    }
    testmethod static public void CoolerContractController_WithAttachmentParameter(){
        Profile p = [select id from profile where name='System Administrator']; 
        User testUser = new User(alias = 'sta23', email='admin@testorg.com', 
            emailencodingkey='UTF-8', lastname='Testing123', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, 
            timezonesidkey='America/Los_Angeles', username='admin@cceag.de', isActive=true, ID2__c = '12345678', SalesOffice__c = '0363');
            
    	System.runAs(testUser) {
            Account acc = new Account(Name = 'Test Acc');
            insert acc;
            
            CDEOrder__c order = new CDEOrder__c(OwnerID = testUser.ID);
            insert order;
            
            SCProductModel__c product = new SCProductModel__c(
                Name          =  'Cooler',
                ID2__c        =  'D036900_4712'
            );
            insert product;
            
            CDEOrderItem__c item = new CDEOrderItem__c(
                CDEOrder__c = order.ID,
                ProductModel__c = product.ID,
                Quantity__c = 10
            );
            insert item;
            
            Attachment att = new Attachment(Name = 'Signature CoolerContract');
            att.parentID = acc.ID;
            att.Body = Blob.valueOf('test');
            insert att;
            
            Test.startTest();
    		
            
            Pagereference LO_Page = Page.srLeihvertrag_Cooler_Muster_hdm_090714;
            LO_Page.getParameters().put(CoolerContractController.ATTACHMENT_PARAMETER_LABEL,att.ID);
            Test.setCurrentPage( LO_Page );
    		
            
            
            CoolerContractController ctrl = new CoolerContractController(new ApexPages.StandardController(order));
    
            Test.stopTest();
    	}
    }
}
