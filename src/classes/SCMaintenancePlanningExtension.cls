/*
 * @(#)SCMaintenancePlanningExtension.cls
 *
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCMaintenancePlanningExtension extends SCContractSearchController
{
    private final SCContractSearchController c;
    private final SCContract__c contracts;
    public  List<ResultObj> results { get; set; }
    public  List<ResultObj> selectedContracts { get; set; }
    public Integer runningJobs { get; set; }
    private SCPlanningSessionExtension planningSession;
    public String processId { get; set; }
    public String processName { get; set; }
    public String processStatus { get; set; }
    Public String newSelectedEmployeeString = '';
    public Integer numberOfAssignedIngeneer;
    public String selectedItems { get; set; }
    
    
    public Boolean showStep1 { get; set; }
    public Boolean showStep2 { get; set; }
    public Boolean showStep3 { get; set; }
    public Boolean showStep4 { get; set; }
    public Boolean showCapacitySettings { get; set; }
    
    public Integer stepNumber { get; set; }
    public Integer stepsTotal { get; set; }
    public List<CapacitySettings> cSettings { get; set; }
    
    public String selEngineerCapacityType { get; set; }
    public String selTeamCapacityType { get; set; }
        
    private static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance(); 
    
    public String engineerSelected { get; set; }
    public String engineerExcluded { get; set; }
    
    public LIST<SCResource__c> resource;
    public LIST<AggregateResult> resourceAggr;
    public List<SCContract__c> selectedOrders;
    public List<SelectOption> engineerOptions { get; set; }
    
    public String newSelectedEmployee { get; set; }
    public static Boolean isTest = false;
    public String severity { get; set; }
    public String messageText { get; set; }
    
    public Set<Id> rids;
    public List<SCBusinessUnit__c> deps { get; set; }
    
    public SCContractSchedule__c contractSchedule;
    public List<SCContractScheduleItem__c> scheduleItems;
    
    
    public Integer directionsSM { get; set; }
    public Integer directionsCM { get; set; }
    
    public Integer numberOfSMContracts { get; set; }
    public Integer numberOfCMContracts { get; set; }
        
    public Integer numberOfSMOrders { get; set; }
    public Integer numberOfCMOrders { get; set; }
        
    public Double capacitySMOrders { get; set; }
    public Double capacityCMOrders { get; set; }
    
    public Integer numberOfSMEngineers { get; set; }
    public Integer numberOfCMEngineers { get; set; }
    
    public Integer workHours { get; set; }
    
    public Map<Id, Boolean> isSelected { get; set; }
    
    
    public SCMaintenancePlanningExtension(SCContractSearchController controller)
    {            
        LIST<SCResource__c> resource = new LIST<SCResource__c>();
        LIST<AggregateResult> resourceAggr = new LIST<AggregateResult>();
        List<ResultObj> selectedContracts =  new List<ResultObj>();
        List<ResultObj> results =  new List<ResultObj>();
        List<SCContract__c> selectedOrders = new List<SCContract__c>();
        
        this.selectedDepartments = controller.selectedDepartments;
        this.setControllerMaintenancePlanning = controller.setControllerMaintenancePlanning;
        
        this.results = resultObjs;
        this.c = controller;
        showStep1 = true;
        showStep2 = false;
        showStep3 = false;
        showStep4 = false;
        showCapacitySettings = false; 
        
        stepNumber = 1;
        stepsTotal = 3;
        
        cSettings = new List<CapacitySettings>();
        cSettings.add(new CapacitySettings(System.Label.SC_app_WeekdayMonday, 100, 10, 100));
        cSettings.add(new CapacitySettings(System.Label.SC_app_WeekdayTuesday, 100, 10, 100));
        cSettings.add(new CapacitySettings(System.Label.SC_app_WeekdayWednesday, 100, 10, 100));
        cSettings.add(new CapacitySettings(System.Label.SC_app_WeekdayThursday, 100, 10, 100));
        cSettings.add(new CapacitySettings(System.Label.SC_app_WeekdayFriday, 100, 10, 100));
        cSettings.add(new CapacitySettings(System.Label.SC_app_WeekdaySaturday, 100, 10, 100));
        cSettings.add(new CapacitySettings(System.Label.SC_app_WeekdaySunday, 100, 10, 100)); 
        
        engineerSelected = '';   
        engineerExcluded = '';   
        
        planningSession = new SCPlanningSessionExtension(isTest);
        severity = 'info';
        
        numberOfAssignedIngeneer = 0;
        rids = new Set<Id>();
        deps = new List<SCBusinessUnit__c>();
        
        directionsSM = 15;
        directionsCM = 5;
        
        workHours = 8;
        
    }

    public PageReference getReloadSetController()
    {
        if(SCBase.isSet(selectedItems))
        {
            // First getting selected items ids
            List<String> selectedContractVisits = selectedItems.split(',');
            List<SCContractVisit__c> allVisits = getContractVisits();
            
            // Refreshing the set controller
            String mpQuery = '';
            mpQuery += ' Select Id, Name, Contract__c, Contract__r.Account__c, Contract__r.BulkId__c, Order__c, Order__r.Duration__c, ';
            mpQuery += ' Order__r.BulkId__c, Sort__c, InstalledBaseLocation__c, AutoCreated__c, ';
            mpQuery += ' InstalledBaseLocation__r.GeoX__c, InstalledBaseLocation__r.GeoY__c, InstalledBaseLocation__r.GeoStatus__c, InstalledBaseLocation__r.Id, ';
            mpQuery += '( Select ContractItem__r.Id, ContractItem__r.InstalledBase__r.ProductModel__c, ContractItem__r.InstalledBase__r.ProductModel__r.ProductNameCalc__c ';
            mpQuery += '  From ContractVisitItems__r Order By CreatedDate asc ) ';
            mpQuery += ' From SCContractVisit__c ';
            mpQuery += ' Where Id IN :allVisits ';
            mpQuery += ' AND Id NOT IN :selectedContractVisits ';
            
            // Collecting data for removing contract schedule items
            List<SCContract__c> selectedContracts = [ select id 
                                                      from sccontract__c 
                                                      where id IN (select contract__c 
                                                                   from sccontractvisit__c 
                                                                   where id IN : selectedContractVisits) ];
            
            setControllerMaintenancePlanning = new ApexPages.StandardSetController(Database.getQueryLocator(mpQuery));
            
            List<SCContractscheduleitem__c> itemsToDelete = [ select id 
                                                              from sccontractscheduleitem__c 
                                                              where ContractSchedule__c = :contractSchedule.id
                                                              and Contract__c in :selectedContracts  ];
            
            // Now deleting the items
            try
            {
                delete itemsToDelete;
            }
            catch(Exception e)
            {
                System.debug('#### Can not delete items: ' + e);
            }
            
            calculateCapacityAndNumberOfMaintenance();
            
        }
        
        return null;
    }
    
    // --- Data for the output on the page only ---      
    public ApexPages.StandardSetController getSetConOutput()
    {
        ApexPages.StandardSetController s = setControllerMaintenancePlanning;
        s.setPageSize(50);
        
        return s;
    }

    public List<SCCOntractVisit__c> getContractVisitsOutput() 
    {
        List <SCContractVisit__c> cv = (List<SCCOntractVisit__c>) getSetConOutput().getRecords();
        
        return cv;
    }
    // ---
    
    // --- Data for creating contr. sched. object with items ---
    // Initializing a standard controller with results from the contract search
    public ApexPages.StandardSetController getSetCon()
    {
        setControllerMaintenancePlanning.setPageSize(setControllerMaintenancePlanning.getResultSize());
        ApexPages.StandardSetController s = setControllerMaintenancePlanning;
        
        return s;
    } 
        
    // Creating a list with all records from thr standars controller
    public List<SCCOntractVisit__c> getContractVisits() 
    {
        List <SCContractVisit__c> cv = (List<SCCOntractVisit__c>) getSetCon().getRecords();
        // getSetCon().setPageSize(5);
        
        return cv;
    }
    // ---
    
    // Creating a new contract schedule object and items
    private void createContractScheduleObject()
    {
        
        if(getSetCon().getResultSize() != 0)
        {
        
            // First creating a parent object - contract schedule
            contractSchedule = new SCContractSchedule__c();
            
            try
            {
                insert contractSchedule;
            }
            catch(Exception e)
            {
                System.debug('#### Can not create a new SCContractSchedule__c object ' + e);
            }
            
            // Now creating the items depending od the controller records
            List<SCContractScheduleItem__c> scheduleItems = new List<SCContractScheduleItem__c>();
            
            for(SCContractVisit__c v :getContractVisits())
            {
                SCContractScheduleItem__c item = new SCContractScheduleItem__c();
                
                item.ContractSchedule__c = contractSchedule.id;
                item.Order__c = v.Order__c;
                
                if(SCBase.isSet(v.Contract__r.BulkId__c))
                {
                    item.MaintenanceType__c = 'Collective';
                    item.BulkId__c = v.Contract__r.BulkId__c;
                }
                else
                {
                    item.MaintenanceType__c = 'Single';
                }
                
                item.Contract__c = v.Contract__c;
                //item.Status__c = 'Active';
                item.Sort__c = v.Sort__c;
                
                scheduleItems.add(item);
            }
            
            try
            {
                insert scheduleItems;
            }
            catch(Exception e)
            {
                System.debug('#### Can not create a new SCContractScheduleItem__c object ' + e);
            }
        }
    }

    public void init()
    {
        
        if(dateNextMaintenance2.Start__c == null)
            dateNextMaintenance2.Start__c = Date.today();
            
        if(dateNextMaintenance2.End__c == null)
            dateNextMaintenance2.End__c = Date.today() + 7;
            

        if(selectedDepartments != null && !selectedDepartments.isEmpty())
        {
            resourceAggr = [ Select Resource__c, Resource__r.Employee__c, Resource__r.FirstName_txt__c, Resource__r.LastName_txt__c
                             From  SCResourceAssignment__c
                             Where ValidFrom__c <= today
                             AND   ValidTo__c >= today
                             AND   EnableScheduling__c = true
                             AND   Department__c IN :selectedDepartments
                             AND   Department__r.Operational__c = true
                             Group By Resource__c, Resource__r.Employee__c, Resource__r.FirstName_txt__c, Resource__r.LastName_txt__c ];
                             
            rids = new Set<Id>();
                             
            for(AggregateResult r : resourceAggr)
            {                
                rids.add((ID)r.get('Resource__c'));
            }
                             
            resource = new List<SCResource__c>();
            
            resource = [ Select Id, Employee__c, LastName_txt__c, FirstName_txt__c, Alias__c
                         From   SCResource__c 
                         Where  Id IN :rids
                         Order By LastName_txt__c ];
                         
            deps = [ Select Id, Name From SCBusinessUnit__c Where Id IN :selectedDepartments Order By Name ];
        }
        else
        {
            resource = null;
            deps = null;
        } 
        
        // Preparing a contract schedule object with items
        if(contractSchedule == null)
        {
            createContractScheduleObject();
        }
        
    }
     
    //-------------------------------------- Capacity & Number of SM / CM -----------------------------------------------
    
    public void calculateCapacityAndNumberOfMaintenance()
    {       
        numberOfSMContracts = 0;    
        numberOfCMContracts = 0;    
        
        numberOfSMOrders = 0;
        numberOfCMOrders = 0;
        
        capacitySMOrders = 0;
        capacityCMOrders = 0;
        
        numberOfSMEngineers = 0;
        numberOfCMEngineers = 0;
        
        Integer allDurationsSM = 0; // Minutes
        Integer allDurationsCM = 0; // Minutes
        
        Set<Id> contractsSM = new Set<Id>();
        Set<Id> ordersSM    = new Set<Id>();
        
        Set<Id> contractsCM = new Set<Id>();
        Set<Id> ordersCM    = new Set<Id>();
        
        // Iterating the list of contract search results
        for(SCContractVisit__c v :getContractVisits())
        {            
            // SM/CM Contracts
            if(v.Contract__r.BulkId__c == null) 
                contractsSM.add(v.Contract__r.Id);
            else
                contractsCM.add(v.Contract__r.Id);
                
            // SM/CM Orders
            if(v.Order__r.BulkId__c == null) 
                ordersSM.add(v.Order__r.Id);
            else
                ordersSM.add(v.Order__r.Id);
                
            // SM/CM Order Capacity
            if(v.Order__r.BulkId__c == null && v.Order__r.Duration__c != null) 
                allDurationsSM += Integer.valueOf(v.Order__r.Duration__c); 
            
            if(v.Order__r.BulkId__c != null && v.Order__r.Duration__c != null)
                allDurationsCM += Integer.valueOf(v.Order__r.Duration__c); 
        } 
        
        // Calculating capacity
        // total work time for all orders + total trip time
        // (number of orders * average duration) + (number of orders * trip time)
        if(directionsSM == null || directionsSM <= 0)
            directionsSM = 15;
            
        if(directionsCM == null || directionsCM <= 0)
            directionsCM = 5;
        
        if(ordersSM.size() > 0)
            capacitySMOrders = (ordersSM.size() * (allDurationsSM / ordersSM.size())) + (ordersSM.size() * directionsSM );
        if(ordersSM.size() == 0)
            capacitySMOrders = 0;
        
        if(ordersCM.size() > 0)
            capacityCMOrders = (ordersCM.size() * (allDurationsCM / ordersCM.size())) + (ordersCM.size() * directionsCM );
        if(ordersCM.size() == 0)
            capacityCMOrders = 0;
        
        // Getting all SM/CS orders and contracts
        numberOfSMContracts = contractsSM.size();
        numberOfCMContracts = contractsCM.size();
        numberOfSMOrders = ordersSM.size();
        numberOfCMOrders = ordersCM.size();
        
        // Calculating the number of required engineers
        
        Integer averageEngineerCapacity = 0;
        Integer averageEngineerCapacityGroup = 0;
        
        for(Integer i = 0; i<7; i++)
        {
            averageEngineerCapacity += cSettings[i].cPercent;
        }
        
        for(Integer i = 0; i<7; i++)
        {
            averageEngineerCapacityGroup += cSettings[i].cGroup;
        }
        
        if(averageEngineerCapacityGroup < averageEngineerCapacity)
            averageEngineerCapacity = averageEngineerCapacityGroup;
        
        averageEngineerCapacity = averageEngineerCapacity / 7;
        
        if(workHours == null || workHours <= 0)
            workHours = 8;
        
        if(capacitySMOrders == 0)
        {
            numberOfSMEngineers = 0;
        }
        else
        {
            Decimal tmpDecimalSM = (Decimal.valueOf(capacitySMOrders)/60.0) / ((Decimal.valueOf(workHours) * Decimal.valueOf(averageEngineerCapacity)) / 100.0);
            numberOfSMEngineers = Integer.valueOf(tmpDecimalSM.round(System.RoundingMode.UP));
        }
        
        if(capacityCMOrders == 0)
        {
            numberOfCMEngineers = 0;
        }
        else
        {
            Decimal tmpDecimalCM = (Decimal.valueOf(capacityCMOrders)/60.0) / ((Decimal.valueOf(workHours) * Decimal.valueOf(averageEngineerCapacity)) / 100.0);
            numberOfCMEngineers = Integer.valueOf(tmpDecimalCM.round(System.RoundingMode.UP));
        }

    }
    
       
    public Integer getNumberOfCollectiveMaintenace()
    {
        Integer i = 0;
        
        for(SCContractVisit__c v :getContractVisits())
        {
            if(v.Contract__r.BulkId__c != null) 
            { i++; }
        }
        
        return i;
    }
    
    public Double getNumberOfCollectiveMaintenaceCapacity()
    {
        Double i = 0;
        
        for(SCContractVisit__c v :getContractVisits())
        {
            if(v.Contract__r.BulkId__c != null && v.Order__r.Duration__c != null) 
            { 
                i += v.Order__r.Duration__c; 
            }
        }
        
        return (i/60).round();
    }
    
    public Integer getNumberOfStandardMaintenace()
    {
        return getContractVisits().size() - getNumberOfCollectiveMaintenace();
    }
    
    public Double getNumberOfStandardMaintenaceCapacity()
    {
        Double i = 0;
        
        for(SCContractVisit__c v :getContractVisits())
        {
            if(v.Contract__r.BulkId__c == null && v.Order__r.Duration__c != null) 
            { 
                i += v.Order__r.Duration__c; 
            }
        }
        
        return (i/60).round();
    }
    
    
    //-----------------------------------------------------------------------------------------------------------------
    
    
    public List<SelectOption> getEngineerCapacityType()
    {   
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('weekday',System.Label.SC_app_Weekday));
        options.add(new SelectOption('global',System.Label.SC_app_Global));
        
        return options;
    }
    
    public List<SelectOption> getTeamCapacityType()
    {   
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('weekday',System.Label.SC_app_Weekday));
        options.add(new SelectOption('global',System.Label.SC_app_Global));
        
        return options;
    }
    
    
    public PageReference showSettings()
    {
        showCapacitySettings = true;
        return null;
    }
    
    public PageReference hideSettings()
    {
        showCapacitySettings = false;
        return null;
    }
    
    public PageReference gotoStep1()
    {
        showStep1 = true;
        showStep2 = false;
        showStep3 = false;
        showStep4 = false;
        stepNumber = 1;
        
        return null;
    }
    
    public PageReference gotoStep2()
    {
        showStep1 = false;
        showStep2 = true;
        showStep3 = false;
        showStep4 = false;
        stepNumber = 2;
        
        // Now we must calculate number of SM/CM orders and contracts and capacity
        calculateCapacityAndNumberOfMaintenance();

        return null;        
    }
    
    public PageReference gotoStep3()
    {
        showStep1 = false;
        showStep2 = false;
        showStep3 = true;
        showStep4 = false;
        stepNumber = 3;        
        
        return null;
    }
    
    
    public Integer getNumberOfSelectedEngineer()
    {   
        return numberOfAssignedIngeneer - getNumberOfExcludedEngineer();
    }
    
    public Integer getNumberOfExcludedEngineer()
    {
        System.debug('#### engineerExcluded: ' + engineerExcluded);
        
        if(engineerExcluded.trim() != '')
        {
            List<String> ids = engineerExcluded.split(',');
            return ids.size();
        }
        else
        {
            return 0;
        }
    }
    
    public class CapacitySettings
    {
        public String  cDay { get; set;}
        public Integer cPercent { get; set;}
        public Integer cOrder { get; set;}
        public Integer cGroup { get; set;}
        
        public CapacitySettings(String cDay, Integer cPercent, Integer cOrder, Integer cGroup)
        {
            this.cDay = cDay;
            this.cPercent = cPercent;
            this.cOrder = cOrder;
            this.cGroup = cGroup;
        }
    }
        
    public PageReference reloadEngineer()
    {   
        // Wee need to clear this lists, because we are load a new department
        engineerSelected = '';
        engineerExcluded = '';
        numberOfAssignedIngeneer = 0;
        
        // Loading selected department
        init();
        getAssignedEngineer();
        return null;
    }
    
    public List<SelectOption> getAssignedEngineer() 
    { 
        
        System.debug('#### newSelectedEmployee: ' + newSelectedEmployee);
        
        // First checking, wether a new employee was added manually
        if(newSelectedEmployee != null && newSelectedEmployee != '')
        {
            if(!rids.contains(newSelectedEmployee))
            {
                List<SCResource__c> manuallyAddedEmployee = [ Select Id, LastName_txt__c, FirstName_txt__c, Alias__c
                                                              From SCResource__c
                                                              Where Id = :newSelectedEmployee ];
                                                              
                if(manuallyAddedEmployee.size() > 0)
                {
                    resource.add(manuallyAddedEmployee[0]);
                    
                    if(newSelectedEmployeeString.trim() == '')
                    {
                        newSelectedEmployeeString = newSelectedEmployee;
                    }
                    else
                    {
                        newSelectedEmployeeString += ',' + newSelectedEmployee;
                    }
                    
                    newSelectedEmployee = null;
                }
                
            }
        }

        List<SelectOption> options = new List<SelectOption>();
        
        // Now fill out the list with employees from selected departments
        if(resource != null && resource.size() > 0)
        {   
            for(SCResource__c r :resource)
            {
                options.add(new SelectOption(r.Id, r.LastName_txt__c + ' ' + r.FirstName_txt__c + ' (' + r.Alias__c + ')'));
            }
            
            numberOfAssignedIngeneer = options.size();
            
            //return options;
        }
        else
        {
            //return null;
        }
        return options;
    }
    
    public PageReference addEngineer()
    {
        return null;
    }
    
    public PageReference onSaveProcess()
    {
        // Contract Schedule       
        contractSchedule.EngineerCapacityPercentMonday__c     = cSettings[0].cPercent;
        contractSchedule.EngineerCapacityPercentTuesday__c    = cSettings[1].cPercent;
        contractSchedule.EngineerCapacityPercentWednesday__c  = cSettings[2].cPercent;
        contractSchedule.EngineerCapacityPercentThursday__c   = cSettings[3].cPercent;
        contractSchedule.EngineerCapacityPercentFriday__c     = cSettings[4].cPercent;
        contractSchedule.EngineerCapacityPercentSaturday__c   = cSettings[5].cPercent;
        contractSchedule.EngineerCapacityPercentSunday__c     = cSettings[6].cPercent;
        
        contractSchedule.EngineerCapacityOrdersMonday__c      = cSettings[0].cOrder;
        contractSchedule.EngineerCapacityOrdersTuesday__c     = cSettings[1].cOrder;
        contractSchedule.EngineerCapacityOrdersWednesday__c   = cSettings[2].cOrder;
        contractSchedule.EngineerCapacityOrdersThursday__c    = cSettings[3].cOrder;
        contractSchedule.EngineerCapacityOrdersFriday__c      = cSettings[4].cOrder;
        contractSchedule.EngineerCapacityOrdersSaturday__c    = cSettings[5].cOrder;
        contractSchedule.EngineerCapacityOrdersSunday__c      = cSettings[6].cOrder;
        
        contractSchedule.TeamCapacityPercentMonday__c         = cSettings[0].cGroup;
        contractSchedule.TeamCapacityPercentTuesday__c        = cSettings[1].cGroup;
        contractSchedule.TeamCapacityPercentWednesday__c      = cSettings[2].cGroup;
        contractSchedule.TeamCapacityPercentThursday__c       = cSettings[3].cGroup;
        contractSchedule.TeamCapacityPercentFriday__c         = cSettings[4].cGroup;
        contractSchedule.TeamCapacityPercentSaturday__c       = cSettings[5].cGroup;
        contractSchedule.TeamCapacityPercentSunday__c         = cSettings[6].cGroup;
        
        String departments = '';
        
        if(!selectedDepartments.isEmpty())
        {   
            for(SCBusinessUnit__c d : [ Select Name From SCBusinessUnit__c Where Id IN :selectedDepartments ])
            {
                departments += d.Name + ',';
            }
        }

        contractSchedule.BusinessUnits__c           = departments;
        contractSchedule.EngineerCapacityType__c    = selEngineerCapacityType;
        contractSchedule.TeamCapacityType__c        = selTeamCapacityType;
        contractSchedule.EngineerSelected__c        = newSelectedEmployeeString;
        contractSchedule.EngineerExcluded__c        = engineerExcluded;
        contractSchedule.ProcessStatus__c           = 'Running';
        
        if(thresholdValueSM == null || thresholdValueSM.trim() == ''){ thresholdValueSM = '0'; }
        if(thresholdValueCM == null || thresholdValueCM.trim() == ''){ thresholdValueCM = '0'; }
        
        contractSchedule.ThresholdCM__c  = Double.valueOf(Integer.valueOf(thresholdValueCM));
        contractSchedule.ThresholdSM__c  = Double.valueOf(Integer.valueOf(thresholdValueSM));
        
        if(dateNextMaintenance2.Start__c != null)
        {
            contractSchedule.CustomerPrefStart__c = dateNextMaintenance2.Start__c;
        }
        if(dateNextMaintenance2.End__c != null)
        {
            contractSchedule.CustomerPrefEnd__c   = dateNextMaintenance2.End__c;
        }
        
        contractSchedule.PlannedCapacity__c  = numberOfSMOrders + ';' + capacitySMOrders + ';' + numberOfSMEngineers + ';' + numberOfCMOrders + ';';
        contractSchedule.PlannedCapacity__c += capacityCMOrders + ';' + numberOfCMEngineers + ';' + workHours;
        
        try 
        {
            upsert contractSchedule;
            
            System.debug('#### Contract schedule updated');
            
            processId = contractSchedule.Id;
            
            List<SCContractSchedule__c> cs = [ Select Name, ProcessStatus__c From SCContractSchedule__c Where Id = : contractSchedule.id Limit 1 ];
            
            processName = cs[0].Name;
            processStatus = cs[0].ProcessStatus__c;
            
        } 
        catch (DmlException e)
        {
            System.debug('#### Contract Schedule UPSERT Failed: ' + e.getTypeName() + ' - ' + e.getCause() + ': ' + e.getMessage());
            
            return null;
        }  
       
        return gotoStep3();
    
    }
    
    public PageReference onSchedule()
    {
        SCConfigOverviewController conf = new SCConfigOverviewController();
        
        System.debug('#### callout start');
        
        List<String> command = new List<String>();
        String s = 'maintenanceService';
        
        command.add(s);

        conf.callout(command);       
        
        SCConfigOverviewController co = new SCConfigOverviewController(); 
        
        co.controlOperation(command);
    
        System.debug('#### callout end');
        
        //PageReference ref = new PageReference('/' + processId);
        PageReference ref = new PageReference('/apex/SCMaintenancePlanningResult?id=' + processId);

        ref.setRedirect(true);
        return ref;
    }
    
    public PageReference onCancel()
    {
        // First we must delete an contract schedule object with items
        try
        {
            delete contractSchedule;
        }
        catch(Exception e)
        {
            System.debug('#### Can not delete contract schedule object: ' + e);
        }
        
        // Now redirecting user to the contract search page
        //return Page.SCContractSearch;
        PageReference ref = new Pagereference('/apex/SCContractSearch');
        ref.setRedirect(true);
        return ref;
    }
    

}
