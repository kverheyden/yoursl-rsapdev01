/**
* @author Jan Mensfeld
* @date 30.10.2014
* @description Class to create the right dispatcher and dispatching the trigger event(s) to the appropriate 
* event handler(s).
*				
*/
public with sharing class TriggerFactory{
	
	public class TriggerException extends Exception {}
	
	 /** 
	* @author Jan Mensfeld
	* @date 30.10.2014
	* @description Creates the appropriate dispatcher and dispatches the trigger event to the dispatcher's event handler method.
	* @param Schema.sObjectType Object type to process (SObject.sObjectType)
	*/
	public static void createTriggerDispatcher(Schema.SObjectType soType) {
		if (!triggerEnabled(soType))
			return;
        ITriggerDispatcher dispatcher = getTriggerDispatcher(soType);
        if (dispatcher == null)
            throw new TriggerException('No Trigger dispatcher registered for Object Type: ' + soType);
        execute(dispatcher);
    }
	
     /** 
	* @author Jan Mensfeld
	* @date 30.10.2014
	* @description Check if the trigger for an SObject is disabled. 
	*				(Convention for TriggerSettings__c.Name: <SObjectType>Trigger)
	* @param Schema.sObjectType Object type for checking if the Trigger is disabled
	* @return Returns false if the Trigger is disabled in CustomSetting otherwise true
	*/
	public static Boolean triggerEnabled(Schema.SObjectType soType) {
		String triggerName = getOriginalTypeName(soType) + 'Trigger';
		TriggerSettings__c ts = TriggerSettings__c.getValues(triggerName);
		if (ts == null || ts.Active__c)
			return true;
		return false;
	}
	
	private static ITriggerDispatcher getTriggerDispatcher(Schema.SObjectType soType) {
    	String dispatcherTypeName = getDispatcherTypeName(soType);
		Type obType = Type.forName(dispatcherTypeName);
		ITriggerDispatcher dispatcher = (obType == null) ? null : (ITriggerDispatcher)obType.newInstance();
    
		return dispatcher;
    }
	
	private static String getDispatcherTypeName(Schema.SObjectType soType) {
		return getOriginalTypeName(soType) + 'TriggerDispatcher';
	}
	
	private static String getOriginalTypeName(Schema.SObjectType soType) {
		String originalTypeName = soType.getDescribe().getName();
    	if (originalTypeName.endsWith('__c')) {
    		Integer index = originalTypeName.indexOf('__c');
    		return originalTypeName.substring(0, index);
    	}	
		return originalTypeName;
	}
	
	private static void execute(ITriggerDispatcher dispatcher) {
		TriggerParameters tp = new TriggerParameters(
									Trigger.old, Trigger.new, 
									Trigger.oldMap, Trigger.newMap,
									Trigger.isBefore, Trigger.isAfter, 
									Trigger.isDelete, 
									Trigger.isInsert, 
									Trigger.isUpdate, 
									Trigger.isUnDelete, 
									Trigger.isExecuting
									);
        
        if (Trigger.isBefore) {
            if (Trigger.isDelete)
                dispatcher.beforeDelete(tp);
            else if (Trigger.isInsert)
                dispatcher.beforeInsert(tp);
            else if (Trigger.isUpdate)
                dispatcher.beforeUpdate(tp);         
        }
        else
        {
            if (Trigger.isDelete)
                dispatcher.afterDelete(tp);
            else if (Trigger.isInsert)
                dispatcher.afterInsert(tp);
            else if (Trigger.isUpdate)
                dispatcher.afterUpdate(tp);
        }
        dispatcher.executed();
	}
}
