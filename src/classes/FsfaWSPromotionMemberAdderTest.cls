@isTest
private class FsfaWSPromotionMemberAdderTest {
	static testMethod void FsfaWSPromotionMemberAdderUnitTest(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
      	User u 					= new User();
      	u.Alias 				= 'standt';
      	u.Email					= 'standarduser@testorg.com'; 
      	u.EmailEncodingKey		= 'UTF-8';
      	u.LastName				= 'Testing';
      	u.LanguageLocaleKey		= 'en_US'; 
      	u.LocaleSidKey			= 'en_US';
      	u.ProfileId 			= p.Id; 
      	u.TimeZoneSidKey		= 'America/Los_Angeles';
      	u.UserName				= 'yoursl_standarduser@cceag.com';
        
      	System.runAs(u){
            Promotion__c oTestPromotion		= new Promotion__c();
            oTestPromotion.Name				= 'Testpromotion';
            oTestPromotion.PromotionID__c	= '54321';
            insert oTestPromotion;
            
            Account acc = new Account(Name = 'TestAcc',ID2__c = '123');
            insert acc;
            
            Account acc2 = new Account(Name = 'TestAcc2', ID2__c = '456');
            insert acc2;
            
            
            
            //Case with invalid promotion ID
            FsfaWSPromotionMemberAdder.WsPromotionMemberAdder(acc.ID2__c, 'invalidPromotionID');
            List<PromotionMember__c> assert1 = [Select ID from PromotionMember__c where Account__c = :acc.ID];
            system.assertEquals(0, assert1.size());
            
            //Case where two promotion is found under the same ID
            Promotion__c oTestPromotion2		= new Promotion__c();
            oTestPromotion2.Name				= 'Testpromotion2';
            oTestPromotion2.PromotionID__c	= '54321';
            insert oTestPromotion2;
            FsfaWSPromotionMemberAdder.WsPromotionMemberAdder(acc.ID2__c, '54321');
            delete oTestPromotion2;
            
            List<PromotionMember__c> assert2 = [Select ID from PromotionMember__c where Account__c = :acc.ID];
            system.assertEquals(0, assert2.size());
            
            //Case where member id is empty
            FsfaWSPromotionMemberAdder.WsPromotionMemberAdder('', '54321');
            List<PromotionMember__c> assert3 = [Select ID from PromotionMember__c where Promotion__c = :oTestPromotion.ID];
            system.assertEquals(0, assert3.size());
            FsfaWSPromotionMemberAdder.WsPromotionMemberAdder(';', '54321');
            List<PromotionMember__c> assert4 = [Select ID from PromotionMember__c where Promotion__c = :oTestPromotion.ID];
            system.assertEquals(0, assert4.size());
            
            
            //Case where there's more than 2000 member ids
            String temp = '';
            List<String> tempList = new List<String>();
            for(integer i=0;i< 2001;i++){
                tempList.add(String.valueOf(i));
            }
            temp = String.join(tempList,';');
            FsfaWSPromotionMemberAdder.WsPromotionMemberAdder(temp, '54321');
            List<PromotionMember__c> assert5 = [Select ID from PromotionMember__c where Promotion__c = :oTestPromotion.ID];
            system.assertEquals(0, assert5.size());
            
            // Proper test
            FsfaWSPromotionMemberAdder.WsPromotionMemberAdder(acc.ID2__c+';'+acc2.ID2__c, '54321');
            List<PromotionMember__c> accMember = [Select ID from PromotionMember__c where Promotion__c =:oTestPromotion.ID and Account__c = :acc.ID];
            system.assertEquals(1, accMember.size());
            List<PromotionMember__c> accMember2 = [Select ID from PromotionMember__c where Promotion__c =:oTestPromotion.ID and Account__c = :acc2.ID];
            system.assertEquals(1, accMember2.size());
        }
    }
}
