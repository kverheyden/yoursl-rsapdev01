@isTest
public with sharing class SCCokeOpenIDUserRegistrationHandlerTest 
{

	public static testmethod void CokeIdTest()
	{
		Profile t_profile = [ SELECT Id FROM Profile WHERE Name IN ('Systemadministrator', 'System Administrator') LIMIT 1];

		        System.assert(t_profile != null);
		        System.assert(t_profile.Id != null);

        		User t_user = new User(
	            Alias = 'GMSTest1', 
	            FirstName = 'GMS',
	            LastName = 'Testuser 1', 
	            Username = 'gmstest1@gms-online.de', 
	            Email = 'gmstest1@gms-online.de', 
	            CommunityNickname = 'gmstest1', 
	            Street = 'Karl Schurz Str.',
	            PostalCode = '33100',
	            City = 'Paderborn',
	            Country = 'DE',
	            GEOX__c = -0.10345,
	            GEOY__c = 51.49250,
	            TimeZoneSidKey = 'Europe/Berlin', 
	            LocaleSidKey = 'de_DE_EURO', 
	            EmailEncodingKey = 'UTF-8',
	            LanguageLocaleKey = 'en_US',
	            
	            ProfileId = t_profile.Id
        		);
        
        		insert t_user;

		SCCokeOpenIDUserRegistrationHandler testHandler = new SCCokeOpenIDUserRegistrationHandler();
		
		String identifier 	= '';
        String firstName	= 'GMS';
        String lastName		= 'Testuser 1';
        String fullName 	= '';
        String email 		= 'gmstest1@gms-online.de';
        String link 		= '';
        String userName 	= 'gmstest1@gms-online.de';
        String locale 		= '';
        String provider 	= '';
        String siteLoginUrl = '';
        Map<String, String> attributeMap = new Map<String, String>(); 

		Auth.UserData IdData = new Auth.UserData( identifier,firstName,lastName,fullName,email,link,userName,locale,provider,siteLoginUrl,attributeMap);

		String portalID = '';
		
		testHandler.createUser(null, IdData);
		testHandler.updateUser(t_user.id, null, IdData);
		testHandler.setGeneralUserData(t_user, IdData);
		
	}

}
