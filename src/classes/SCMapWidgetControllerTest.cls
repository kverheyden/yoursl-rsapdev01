/**
 * @(#)SCMapWidgetControllerTest
 * Copyright 2012 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

 /**
 * @author Eugen Tiessen <etiessen@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCMapWidgetControllerTest 
{

    static testMethod void myUnitTest() 
    {
        SCHelperTestClass3.createCustomSettings('DE',true);
        
        Test.startTest();
        SCMapWidgetController mwc = new SCMapWidgetController();
        mwc.lat = '10';
        mwc.lng = '50';
        mwc.sizeX = '100';
        mwc.sizeY = '100';
        mwc.getSignedStreetViewImageUrl();
        Test.stopTest(); 
    }
}
