/*
 * @(#)CCWCOrderCreateTest.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 /**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Note !!!
// ========
//@isTest(SeeAllData=true) not allowed by tests creating an order. Because a trigger SCOrder_AI_CallSapWebService calls the Web Service
//====================================
// CCWCOrderCreate if IFEnableTriggerOrderCreate__c of CCSetting is greater than 0. By production and qa data it is the case.
// Calling of Web services while testing causes an exception that aborts the test. So we will not get the test running over the 75 procent of 
// the code
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@isTest
private class CCWCOrderCreateTest
{
	private static Boolean seeAllData = CCWCTestBase.isSeeingAllData();

    static testMethod void getTimeOutDefault()
    {
        Test.StartTest();
        CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
        	ccSettings = CCSettings__c.getInstance();
        }
        Boolean defaultTimeout = true;
        CCWCTestBase.setCCSettings(ccSettings, seeAllData, defaultTimeout);
		
        CCWSUtil u = new CCWSUtil();
		Integer timeOut = u.getTimeOut();
		System.assertEquals(90000, timeOut);
        Test.StopTest();
    }	

    static testMethod void positiveTestCase1()
    {
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
        	appSettings = SCApplicationSettings__c.getInstance();
        }
		CCWCTestBase.setAppSettingsForOrder(appSettings, seeAllData);
        CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
        	ccSettings = CCSettings__c.getInstance();
        }
        CCWCTestBase.setCCSettings(ccSettings, seeAllData);
		
        CCWSUtil u = new CCWSUtil();
		Integer timeOut = u.getTimeOut();
		System.assertEquals(30000, timeOut);
        
        CCWCTestBase.createDomsForOrderCreation(seeAllData);
        Boolean doUpsert = true;
        SCOrder__c order = CCWCTestBase.createTestOrder(appSettings, doUpsert);
        
		
        // call service
        Boolean async = false;
        Boolean testMode = true;
        Test.StartTest();
        String requestMessageID = CCWCOrderCreate.callout(order.Id, async, testMode);       
        
        // make assertion after web call
        List<SCOrder__c> orderList1 = [select id, name, ERPStatusOrderCreate__c from SCOrder__c where id = : order.Id];
        System.assertEquals(orderList1.size(), 1);
        System.assertEquals('pending', orderList1[0].ERPStatusOrderCreate__c); 
        
       
        // fill response structure
        CCWSGenericResponse.GenericServiceResponseMessageClass gr = new CCWSGenericResponse.GenericServiceResponseMessageClass();
        String messageId = u.getMessageID();
        String messageUUID = u.getMessageID();
        String operation = 'CreateOrder';
        String externalID = orderList1[0].Name;
        debug('externalID: ' + externalID);

        CCWCTestBase.prepareWCGenericResponse(gr, messageId, messageUUID, requestMessageID, operation, externalID);
        // add reference        
        String idType = 'Order Number';
        String sapOrderNumber = 'SAP-OrderNo-1';
        String referenceID = sapOrderNumber;
        CCWCTestBase.addReferenceItem (gr, idType, externalID, referenceId);

        // add log
        String typeId = 'TypeID';
        Integer severityCode = 1; 
        String msg = 'Order created';
        
        CCWCTestBase.addLogEntry (gr, typeId, externalID, severityCode, msg);       
        // call the response service
        CCWSGenericResponse.transmit(gr);
        Test.StopTest();
        // make assertions 
        // read Order
        List<SCOrder__c> orderList2 = [select id, name, ERPStatusOrderCreate__c, ERPOrderNo__c, ERPResultDate__c from SCOrder__c 
        					where id = : order.Id];
        System.assertEquals(orderList2.size(), 1);
        System.assertEquals('ok', orderList2[0].ERPStatusOrderCreate__c); 
        System.assertEquals(sapOrderNumber, orderList2[0].ERPOrderNo__c);
        Date today = Date.today();
        System.assertEquals(today, orderList2[0].ERPResultDate__c.date()); 
        
    }

    static testMethod void negativeTestCase1()
    {
        CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
        	appSettings = SCApplicationSettings__c.getInstance();
        }
		CCWCTestBase.setAppSettingsForOrder(appSettings, seeAllData);
        CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
        	ccSettings = CCSettings__c.getInstance();
        }
        CCWCTestBase.setCCSettings(ccSettings, seeAllData);
        
        CCWCTestBase.createDomsForOrderCreation(seeAllData);
        Boolean doUpsert = true;
        SCOrder__c order = CCWCTestBase.createTestOrder(appSettings, doUpsert);
        
		
        // call service
        Boolean async = false;
        Boolean testMode = true;
        Test.StartTest();
        String requestMessageID = CCWCOrderCreate.callout(order.Id, async, testMode);       
        
        // make assertion after web call
        List<SCOrder__c> orderList1 = [select id, name, ERPStatusOrderCreate__c from SCOrder__c where id = : order.Id];
        System.assertEquals(orderList1.size(), 1);
        System.assertEquals('pending', orderList1[0].ERPStatusOrderCreate__c); 
        
       
        // fill response structure
        CCWSGenericResponse.GenericServiceResponseMessageClass gr = new CCWSGenericResponse.GenericServiceResponseMessageClass();
        String messageId = u.getMessageID();
        String messageUUID = u.getMessageID();
        String operation = 'CreateOrder';
        String externalID = orderList1[0].Name;
        debug('externalID: ' + externalID);

        CCWCTestBase.prepareWCGenericResponse(gr, messageId, messageUUID, requestMessageID, operation, externalID);
        // add reference        
        String idType = 'Order Number';
        String sapOrderNumber = 'SAP-OrderNo-1';
        String referenceID = sapOrderNumber;
        CCWCTestBase.addReferenceItem (gr, idType, externalID, referenceId);

        // add log with an error
        String typeId = 'TypeID';
        Integer severityCode = 3; 
        String msg = 'Order not created';
        
        CCWCTestBase.addLogEntry (gr, typeId, externalID, severityCode, msg);       
        // call the response service
        CCWSGenericResponse.transmit(gr);
        Test.StopTest();
        // make assertions 
        // read Order
        List<SCOrder__c> orderList2 = [select id, name, ERPStatusOrderCreate__c, ERPOrderNo__c, ERPResultDate__c from SCOrder__c 
        					where id = : order.Id];
        System.assertEquals(orderList2.size(), 1);
        System.assertEquals('error', orderList2[0].ERPStatusOrderCreate__c); 
    }

	

/*
    static testMethod void musterTest()
    {
        Test.StartTest();
        //Test.setMock(WebServiceMock.class, new CCWCOrderCreateTestMock()); 
        Test.StopTest();
    }   
*/
    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }

}
