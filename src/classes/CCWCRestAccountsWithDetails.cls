/**********************************************************************
Name: CCWCRestAccountsWithDetails()
======================================================
Purpose: 
Represents a web service. Gets list of accounts by Id or by list of Ids.  
======================================================
History 
------- 
Date AUTHOR DETAIL 
01/22/2014 Jan Mensfeld INITIAL DEVELOPMENT
***********************************************************************/

@RestResource(urlMapping='/AccountsWithDetails/*')
global without sharing class CCWCRestAccountsWithDetails {
	
	global with sharing class QueryWithSharing {
		public List<SObject> query(String query) {
			return Database.query(query);
		}
	}
	
	public static final String FIELDSET_NAME = 'SalesAppQueryAccountWithDetailsFields';
	public static final String MANAGER_FIELDSET_NAME = 'ManagerAppQueryAccountWithDetailsFields';
	private static final String TO_LABEL = 'toLabel({0})';
	
	private static final String PARAMETER_ACCOUNT_IDS = 'accountIds';
	private static final String PARAMETER_OFFSET_ID = 'offsetId';
	private static final String PARAMETER_TIMESTAMP = 'timestamp';
		
	private static final String SUBSELECT_ATTACHMENT = ', ( SELECT Id, Description, Name, BodyLength, ContentType, LastModifiedDate FROM Attachments LIMIT 1)';	
	
	private static final String ACCOUNT_NO_UPDATES = 'Account with Id {0} has no changes.';
	private static final String ACCOUNT_NOT_FOUND = 'Account with Id {0} was not found.';
	
	private static final String ACCOUNT_OBJECTNAME = 'Account';
	private static final String ACCOUNT_FIELD_NAME = 'Name';
	private static final String ACCOUNT_FIELD_SUBTRADECHANNEL = 'Subtradechannel__c'; 
	private static final String ACCOUNT_FIELD_CUSTOMER_STATUS = 'CustomerStatus__c';
	private static final String ACCOUNT_FIELD_TYPE = 'Type';
	
	private static final String RED_SURVEY_RESULT_OBJECTNAME = 'RedSurveyResult__c';
	private static final String SC_INSTALLED_BASE_OBJECTNAME = 'SCInstalledBase__c';	
	private static final String MARKETTING_ATTRIBUTE_OBJECTNAME = 'MarketingAttribute__c';
	
	private static final String TASK_OBJECTNAME = 'Task';
	private static final String TASK_FIELD_IS_CLOSED = 'IsClosed';
	private static final String OBJECT_FIELD_LASTMODIFIED_DATE = 'LastModifiedDate';
	
	private static final String ACCOUNT_ROLE_FILTER = 'ZV';
	
	private static String sLimit = getValueFromSalesAppSetting('AccountsWithDetailsQueryLimit');
	private static Map<String, String> mapSubtradeChannel = getSubtradeChannelMap();
	
	private static String getValueFromSalesAppSetting(String name) { 
		try {
			SalesAppSettings__c objSalesAppSetting = SalesAppSettings__c.getValues(name);
			return objSalesAppSetting.Value__c;
		} catch (Exception e) {
			return String.format('No entry in SalesAppSettings__c for {0}', new String[] {name});
		}
	}
		
	public static List<Schema.FieldSetMember> getFieldSetMembers(String sObjectName) {
		String fieldsetName = (sObjectName == 'Account' && isUserManager()) ? MANAGER_FIELDSET_NAME : FIELDSET_NAME;
		Map<String, Schema.SObjectType> mapGlobalDescribe = Schema.getGlobalDescribe(); 
	    Schema.SObjectType objSObjectType = mapGlobalDescribe.get(sObjectName);
	    
		Schema.DescribeSObjectResult objDescribeSObjectResult = objSObjectType.getDescribe();
	    Schema.FieldSet objFieldSet = objDescribeSObjectResult.FieldSets.getMap().get(fieldsetName);

		if (objFieldSet != null)		
			return objFieldSet.getFields();
		return null;
	}
	
	private static Boolean isUserManager() {
		User u = [SELECT SalesAppUserType__c FROM User WHERE Id =: UserInfo.getUserId()];
		return u.SalesAppUserType__c == 'Manager';
	}
	
	private static List<String> getQueryFieldsFromObject(String sObjectName) {
		List<String> fieldList = new List<String>();
		List<Schema.FieldSetMember> fieldSetMemberList = getFieldSetMembers(sObjectName);
		if (fieldSetMemberList == null)
			return fieldList;
		for (Schema.FieldSetMember f : fieldSetMemberList) {
			if (f.getType() == Schema.DisplayType.Picklist && 
				!( (sObjectName == 'Account') && (f.getFieldPath() == ACCOUNT_FIELD_SUBTRADECHANNEL || f.getFieldPath() == ACCOUNT_FIELD_TYPE || f.getFieldPath() == ACCOUNT_FIELD_CUSTOMER_STATUS) )
			)			
				fieldList.add(formatToLabel(f.getFieldPath()));				
			else
				fieldList.add(f.getFieldPath());
		}
		if (sObjectName == ACCOUNT_OBJECTNAME)
			fieldList.add(ACCOUNT_FIELD_NAME);
		if (sObjectName == TASK_OBJECTNAME)
			fieldList.add(TASK_FIELD_IS_CLOSED);
		fieldList.add(OBJECT_FIELD_LASTMODIFIED_DATE);
		return fieldList;
	}
	
 	@TestVisible
	private static AccountWithDetails createAccountWithError(Exception e){
		AccountWithDetails a = new AccountWithDetails();
		a.Message = e.getTypeName() + ' ' + e.getMessage();
		//a.ExceptionOccurred = e;
		a.HasException = true;
		return a;
	} 
		
	/*
	@TestVisible
	private static AccountWithDetails createAccountWithError(String sMessage){
		AccountWithDetails a = new AccountWithDetails();
		a.Message = sMessage;
		return a;
	} 
	*/
	
	private static String getParameterValueFromUrlRequest(String sUrlParameter) {
		try {
			RestRequest objRequest = RestContext.Request;
			return objRequest.params.get(sUrlParameter);			
		}
		catch(TypeException e) {
			System.debug(e.getTypeName() + ': ' + e.getMessage());
			return null;
		}
	}
		
	private static Set<Id> getAccountIdSetFromString(String sCommaSeparatedAccountIds) { 
		Set<Id> accountIds = new Set<Id>();
		if(sCommaSeparatedAccountIds == null || sCommaSeparatedAccountIds == '')
			return accountIds;
		
		List<String> listAccountIds = sCommaSeparatedAccountIds.split(',');
		
		for (String accId : listAccountIds) {
			try {
				accountIds.add(Id.valueOf(accId));
			} catch(Exception e) {
				System.debug(e.getTypeName() + ' ' + e.getMessage() + ' ' + e.getStackTraceString());
			}
		}
		return accountIds;
	}
	
	@TestVisible
	private static Datetime getDateTimeFromTimeStamp(String sTimeStamp){
		try {
			return Datetime.valueOf(sTimeStamp.replace('T', ' '));
		} catch(TypeException e) {
			System.debug(e.getMessage());
			return null;
		}
	}
	
	private static List<Task> getAccountTasks(Set<Id> accountIds) {
		String sTaskQuery = 'SELECT ' + String.join(getQueryFieldsFromObject(TASK_OBJECTNAME), ', ') + SUBSELECT_ATTACHMENT + ' FROM ' + TASK_OBJECTNAME + 
			' WHERE AccountId IN (' + getCommaSeparatedIdListForQueries(accountIds) + ')' +
			' ORDER BY CreatedDate DESC';
		//System.debug('TASK QUERY: ' + sTaskQuery);
		return Database.query(sTaskQuery);
	}
	
	private static Map<Id, AccountMarketingAttribute__c> queryAccountMarketingAttributes(Set<Id> accountIds) {
		return new Map<Id, AccountMarketingAttribute__c> ([
			SELECT Account__c, MarketingAttribute__c FROM AccountMarketingAttribute__c WHERE Account__c IN: accountIds
		]); 
	}
	
	private static Map<Id, MarketingAttribute__c> queryMarketingAttributes(Set<Id> marketingAttributeIds) {
		/* Change to use of field set */
		return new Map<Id, MarketingAttribute__c> ([
			SELECT toLabel(Attribute__c), toLabel(Value__c) FROM MarketingAttribute__c WHERE Id IN: MarketingAttributeIds
		]);
	}
	
	@TestVisible
	private static void fillTempMaps(Map<Id, AccountMarketingAttribute__c> accMarkAttr, Map<Id, Id> ama_acc, Map<Id, Id> ama_ma) {
		for (Id amaId : accMarkAttr.keySet()) {
			AccountMarketingAttribute__c ama = accMarkAttr.get(amaId);
			ama_acc.put(ama.Id, ama.Account__c);
			ama_ma.put(ama.Id, ama.MarketingAttribute__c);
		}
	}
	
	private static List<MarketingAttribute__c> getFilteredMarketingAttributes(Map<Id, MarketingAttribute__c> marketingAttributeMap, Map<Id, Id> ama_acc, Map<Id, Id> ama_ma, Id accountId) {
		List<MarketingAttribute__c> marketingAttributeList = new List<MarketingAttribute__c>();
		for(Id amaId : ama_acc.keySet()) {
			Id accId = ama_acc.get(amaId); 
			if(accId == accountId) {
				Id maId = ama_ma.get(amaId);
				marketingAttributeList.add(marketingAttributeMap.get(maId));
			}
		}
		return marketingAttributeList;
	}
		
	public static List<RedSurveyResult__c> getAccountRedSurveyResult(Set<Id> accountIds) {
		String sRedSurveyResultQuery = 'SELECT ' + String.join(getQueryFieldsFromObject(RED_SURVEY_RESULT_OBJECTNAME), ', ') + SUBSELECT_ATTACHMENT + ' FROM ' + RED_SURVEY_RESULT_OBJECTNAME +
		' WHERE Account__c IN (' + getCommaSeparatedIdListForQueries(accountIds) + ')' +
		' ORDER BY CreatedDate DESC';
		//System.debug('RED SURVEY RESULT QUERY: ' + sRedSurveyResultQuery);
		return new QueryWithSharing().query(sRedSurveyResultQuery);
	}
	
	private static List<AccountFeedWithUserName> getAccountFeedsWithUsernames(Set<Id> accountIds) {		
		String sAccountFeedQuery = 'SELECT Id, CreatedById, ContentType, CreatedDate, Body, Type, ContentSize, Title, ParentId, ContentFileName FROM AccountFeed ' +
			' WHERE Type != \'TrackedChange\' AND ParentId IN (' + getCommaSeparatedIdListForQueries(accountIds) + ') ORDER BY CreatedDate DESC' ;
		List<AccountFeed> feeds = Database.query(sAccountFeedQuery);
		
		List<AccountFeedWithUserName> listAccFeedsUserName = new List<AccountFeedWithUserName>();
		
		Set<Id> CreatedByIds = new Set<Id>();
		
		// Collect CreatedByIds of Account Feed
		for (AccountFeed af: feeds)
			CreatedByIds.add(af.CreatedById);
		
		// Map user names
		List<User> listAccountFeedUser = [SELECT Id, Name FROM User WHERE Id IN : CreatedByIds];
		Map<Id, String> mapIdUserName = new Map <Id, String>();
		for (User u : listAccountFeedUser)
			mapIdUserName.put(u.Id, u.Name);
		
		// Create AccountFeedWithUserName
		for (AccountFeed accFeed: feeds) {
			AccountFeedWithUserName a = new AccountFeedWithUserName();
			a.AccountFeed = accFeed;
			a.UserName = mapIdUserName.get(accFeed.CreatedById);
			listAccFeedsUserName.add(a);
		}
		if (listAccFeedsUserName.size() == 0)
			return null;
		return listAccFeedsUserName;
	}
	private static String getCommaSeparatedIdListForQueries(Set<Id> ids) {
		return '\'' + String.join(new List<Id>(ids), '\',\'') + '\'';
	}
	
	private static List<Task> getFilteredTasks(List<Task> listAccountTasks, Id accountId, Id ownerId) {
		List<Task> listOpenTask = new List<Task>();
		List<Task> listClosedTask = new List<Task>();
		List<Task> listFilteredTask = new List<Task>();
		
		for(Task t : listAccountTasks) {
			if (t.AccountId != accountId || t.OwnerId != ownerId)
				continue;
			if (!t.IsClosed)
				listOpenTask.add(t);
			if (listClosedTask.size() < 10 && t.IsClosed)
				listClosedTask.add(t);
		}
		listFilteredTask.addAll(listOpenTask);
		listFilteredTask.addAll(listClosedTask);
		return listFilteredTask;
	}
	
	private static List<RedSurveyResult__c> getFilteredRedSurveyResults(List<RedSurveyResult__c> listAccountRedSurveys, Id accountId) {
		List<RedSurveyResult__c> l = new List<RedSurveyResult__c>();

		for(RedSurveyResult__c rr: listAccountRedSurveys) {
			if (rr.Account__c == accountId && l.size() < 6)                
            	l.add(rr);
		}
		return l;
	}
	
	private static List<AccountFeedWithUserName> getFilteredAccountFeeds(List<AccountFeedWithUserName> listAccountFeeds, Id accountId) {
		if (listAccountFeeds == null) 
			return null;
		
		List<AccountFeedWithUserName> l = new List<AccountFeedWithUserName>();
		for (AccountFeedWithUserName af: listAccountFeeds) { 
			if (af.AccountFeed.ParentId == accountId && l.size() < 20)
				l.add(af);
		}
		return l;
	}
	
	@TestVisible
	private static List<CCAccountRole__c> getFilteredAccountRoles(List<CCAccountRole__c> listAccountRoles, Id accountId) {
		if (listAccountRoles == null)
			return null;
		List<CCAccountRole__c> listAccRoles = new List<CCAccountRole__c>();
		for (CCAccountRole__c ar : listAccountRoles)
			if (ar.MasterAccount__c == accountId)
				listAccRoles.add(ar);
		return listAccRoles;
	}
	
	private static List<SlaveAccountWithPricebook> getFilteredSlaveAccountsWithPricebook(Map<Id, Account> mapSlaveAccounts, Map<Id, Pricebook2> mapPricebookIdPricebook, List<CCAccountRole__c> listAccRoles) {
		List<SlaveAccountWithPricebook> listSlaveAccWithPriceBoook = new List<SlaveAccountWithPricebook>();
		for(CCAccountRole__c ar : listAccRoles) {
			SlaveAccountWithPricebook sawp = new SlaveAccountWithPricebook();
			sawp.SlaveAccount = mapSlaveAccounts.get(ar.SlaveAccount__c);
			sawp.Pricebook = mapPricebookIdPricebook.get(sawp.SlaveAccount.Pricebook__c);
			listSlaveAccWithPriceBoook.add(sawp);
		}	
		return listSlaveAccWithPriceBoook;
	}
	
	private static Boolean parameterHasAccountAndTimestamp() {
		try {
			return getParameterValueFromUrlRequest(PARAMETER_ACCOUNT_IDS) != null &&
				   getParameterValueFromUrlRequest(PARAMETER_TIMESTAMP) != null;					
		}
		catch(TypeException e) {
			return false;
		}
	}
	
	private static String formatToLabel(String fieldName) {
		return String.format(TO_LABEL, new String[] {fieldName});
	}
	
	private static Map<Id, Boolean> getAccountIdsMap() {
		String query;
		String accountIds = getParameterValueFromUrlRequest(PARAMETER_ACCOUNT_IDS);
		String timestamp = getParameterValueFromUrlRequest(PARAMETER_TIMESTAMP);
		if (accountIds == null)
			return null;
		Map<Id, Boolean> mapAccountIds = new Map<Id, Boolean>();
		List<String> splittedParameter = new List<String> (accountIds.split(','));
		Set<Id> accountIdSet = new Set<Id> ();
		for (String s : splittedParameter) {
			try {
				accountIdSet.add(Id.valueOf(s));
			} catch (Exception e) {
				System.debug(e.getTypeName() + ': ' + e.getMessage() + ' ' + e.getStacktraceString());
			}
		}		
		if (accountIdSet.size() > 0 ) {
			query = 'SELECT Id FROM Account WHERE Id IN (' + getCommaSeparatedIdListForQueries(accountIdSet) + ') AND LastModifiedDate > ' + timestamp + ' ALL ROWS';
			//System.debug('JM Debug: ' + query);
			List<Account> accounts = new QueryWithSharing().query(query);//Database.query(query);
			for (Account a : accounts) 
				mapAccountIds.put(a.Id, true);
			
			query = 'SELECT Account__c FROM SalesDetails__c WHERE Account__c IN (' + getCommaSeparatedIdListForQueries(accountIdSet) + ') AND LastModifiedDate > ' + timestamp + ' ALL ROWS';
			//System.debug('JM Debug: ' + query);
			List<SalesDetails__c> salesDetails = new QueryWithSharing().query(query);//Database.query(query);
			for (SalesDetails__c sd : salesDetails) 
				mapAccountIds.put(sd.Account__c, true);	
			
			query = 'SELECT Account__c FROM RedSurveyResult__c WHERE Account__c IN (' + getCommaSeparatedIdListForQueries(accountIdSet) + ') AND LastModifiedDate > ' + timestamp + ' ALL ROWS';
			//System.debug('JM Debug: ' + query);
			List<RedSurveyResult__c> redSurveyResults = new QueryWithSharing().query(query);//Database.query(query);
			for (RedSurveyResult__c rsr : redSurveyResults) 
				mapAccountIds.put(rsr.Account__c, true);		
			
			query = 'SELECT Account__c FROM SCAccountInfo__c WHERE Account__c IN (' + getCommaSeparatedIdListForQueries(accountIdSet) + ') AND LastModifiedDate > ' + timestamp + ' ALL ROWS';
			//System.debug('JM Debug: ' + query);
			List<SCAccountInfo__c> scAccountInfos = new QueryWithSharing().query(query);//Database.query(query);
			for (SCAccountInfo__c ai : scAccountInfos)
				mapAccountIds.put(ai.Account__c, true);
			
			query = 'SELECT Account__c FROM PromotionMember__c WHERE Account__c IN (' + getCommaSeparatedIdListForQueries(accountIdSet) + ') AND LastModifiedDate > ' + timestamp + ' ALL ROWS';
			//System.debug('JM Debug: ' + query);
			List<PromotionMember__c> promotionMembers = new QueryWithSharing().query(query);//Database.query(query);
			for (PromotionMember__c pm : promotionMembers)
				mapAccountIds.put(pm.Account__c, true);
			
			query = 'SELECT AccountId FROM Task WHERE AccountId IN (' + getCommaSeparatedIdListForQueries(accountIdSet) + ') AND LastModifiedDate > ' + timestamp + ' ALL ROWS';
			//System.debug('JM Debug: ' + query);
			List<Task> tasks = new QueryWithSharing().query(query);//Database.query(query);
			for(Task t : tasks)
				mapAccountIds.put(t.AccountId, true);
			
			query = 'SELECT AccountId FROM Contact WHERE AccountId IN (' + getCommaSeparatedIdListForQueries(accountIdSet) + ') AND LastModifiedDate > ' + timestamp + ' ALL ROWS';
			//System.debug('JM Debug: ' + query);
			List<Contact> contacts = new QueryWithSharing().query(query);//Database.query(query);
			for (Contact c : contacts) 
				mapAccountIds.put(c.AccountId, true);	
			
			query = 'SELECT Account__c FROM BankAccount__c WHERE Account__c IN (' + getCommaSeparatedIdListForQueries(accountIdSet) + ') AND LastModifiedDate > ' + timestamp + ' ALL ROWS';
			//System.debug('JM Debug: ' + query);
			List<BankAccount__c> bankAccounts = new QueryWithSharing().query(query);//Database.query(query);
			for (BankAccount__c ba : bankAccounts) 
				mapAccountIds.put(ba.Account__c, true);
			
			query = 'SELECT ParentId FROM AccountFeed WHERE ParentId IN (' + getCommaSeparatedIdListForQueries(accountIdSet) + ') AND LastModifiedDate > ' + timestamp + ' ALL ROWS';
			List<AccountFeed> accountFeeds = new QueryWithSharing().query(query);//Database.query(query);
			for (AccountFeed af : accountFeeds) 
				mapAccountIds.put(af.ParentId, true);
			
			query = 'SELECT ShipTo__c FROM SCInstalledBase__c WHERE ShipTo__c IN (' + getCommaSeparatedIdListForQueries(accountIdSet) + ') AND LastModifiedDate > ' + timestamp + ' ALL ROWS';
			List<SCInstalledBase__c> installedBases = new QueryWithSharing().query(query);
			for (SCInstalledBase__c ib : installedBases)
				mapAccountIds.put(ib.ShipTo__c, true);
			
		}
		for (Id i: accountIdSet) {
			if (mapAccountIds.get(i) == null)
				mapAccountIds.put(i, false);
		}
		//System.debug('JM Debug: ' + query);
		return mapAccountIds;
	}
	
	private static List<Schema.PicklistEntry> getPicklistEntries(String sFieldName){                
        Schema.sObjectType objectType = Account.getSObjectType();
        Schema.DescribeSObjectResult objectDescribe = objectType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = objectDescribe.fields.getMap();
        List<Schema.PicklistEntry> picklistEntries = fieldMap.get(sFieldName).getDescribe().getPickListValues();        
        return picklistEntries;
    }
	
	private static Map<String, String> getSubtradeChannelMap() {
		List<Schema.PicklistEntry> subtradeChannelEntries = getPicklistEntries('Subtradechannel__c');
		Map<String, String> subtradeChannelMap = new Map<String, String>();
		if (subtradeChannelEntries == null)
			return subtradeChannelMap;
		for(Schema.PicklistEntry pe : subtradeChannelEntries)
			subtradeChannelMap.put(pe.Value, pe.Label);
		return subtradeChannelMap;
	}
	
	global class AccountFeedWithUserName {
		public AccountFeed AccountFeed { get; set; }
		public String UserName { get; set; }
		
		public AccountFeedWithUserName () {
			AccountFeed = null;
			UserName = null;
		}
		
		public AccountFeedWithUserName(AccountFeed accFeed, String userName) {
			AccountFeed = accFeed;
			UserName = userName;
		}
	}
	
	global class SlaveAccountWithPricebook {
		public Account SlaveAccount { get; set; }
		public Pricebook2 Pricebook { get; set; }
	}
		
	global class AccountWithDetails{
		public Account Account { get; set; }		
		public Pricebook2 Pricebook { get; set; }
		public List<CCAccountRole__c> AccountRoles { get; set; }
		public List<SlaveAccountWithPricebook> SlaveAccountsWithPricebook { get; set; }
		public List<SCInstalledBase__c> InstalledBases { get; set; }
		public List<Task> Tasks { get; set; }
		public List<RedSurveyResult__c> RedSurveyResults { get; set; }
		public List<AccountFeedWithUserName> AccountFeeds { get; set; }
		public List<MarketingAttribute__c> MarketingAttributes { get; set; }
		public String Message { get; set; }
		
		public Boolean HasException { get; set; }
		public Boolean AccountIsUpToDate { get; set; }
		public Boolean AccountExists { get; set; }
		
		public AccountWithDetails() {
			AccountIsUpToDate = false;
			AccountExists = true;
			HasException = false;
		}
	}

	@HttpGet
	global static void doGet(){
		//Datetime requestStart = System.now();
		RestServiceLogs restLog = new RestServiceLogs();
		Map<Id, Boolean> mapAccountId_IsUpdated = new Map<Id, Boolean>();
		Boolean parameterHasAccountAndTimestamp = parameterHasAccountAndTimestamp();
		if(parameterHasAccountAndTimestamp)
			mapAccountId_IsUpdated = getAccountIdsMap();
		String sCommaSeparatedAccountIds = getParameterValueFromUrlRequest(PARAMETER_ACCOUNT_IDS);
		Set<String> setParameterIds = new Set<String>();
		if (sCommaSeparatedAccountIds != null)
			setParameterIds.addAll(sCommaSeparatedAccountIds.split(','));		
		
		Set<Id> accountIds = getAccountIdSetFromString(sCommaSeparatedAccountIds);
		
		System.debug('JM Debug: sCommaSeparatedAccountIds = ' + sCommaSeparatedAccountIds + ' | accountIds = ' + accountIds); 
		String sOffsetId = getParameterValueFromUrlRequest(PARAMETER_OFFSET_ID);
		
		Map<Id, Account> mapAccount = new Map<Id, Account>();
		AccountWithDetails objResult;
		List<AccountWithDetails> listResult = new List<AccountWithDetails>();
		if (accountIds.size() == 0 && setParameterIds.size() > 0) {
			for(String s : setParameterIds) {
				objResult = new AccountWithDetails();
				objResult.Message = 'Can\'t find Account with ID ' + s;
				objResult.AccountExists = false;
				listResult.add(objResult);	
			}
			RestContext.response.addHeader('Content-Type', 'application/json');
	        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(listResult));
			restLog.insertLog('Success');
			return;
		}
		
		String sQuery = 'SELECT ' + String.join(getQueryFieldsFromObject('Account'), ', ') + 
							', (SELECT ' + String.join(getQueryFieldsFromObject('BankAccount__c'), ', ') + ' FROM Bank_details__r) ' +	
							', (SELECT ' + String.join(getQueryFieldsFromObject('SCAccountInfo__c'), ', ') + ' FROM AccountInfo__r LIMIT 1) ' +	
							', (SELECT ' + String.join(getQueryFieldsFromObject('SalesDetails__c'), ', ') + ' FROM Sales_Details__r WHERE IsDeleted = false ORDER BY CreatedDate DESC LIMIT 6) ' +
							', (SELECT ' + String.join(getQueryFieldsFromObject('PromotionMember__c'), ', ') + ' FROM Promotion_members__r) ' +
							//', (SELECT ' + String.join(getQueryFieldsFromObject('Contact'), ', ') + ' FROM Contacts WHERE IsPrimarySalesContact__c = true) ' +
						'FROM Account ';
		
		if ((accountIds != null && accountIds.size() > 0) ||
			(sOffsetId != null && sOffsetId != ''))
			sQuery += 'WHERE ';
		
		if (accountIds != null && accountIds.size() > 0) {
			String commaSepIds = getCommaSeparatedIdListForQueries(accountIds);	
			sQuery += '(Id IN (' + getCommaSeparatedIdListForQueries(accountIds) + ')) ';	
		}
		if (sOffsetId != null && sOffsetId != '') {
			if (!sQuery.endsWith('WHERE '))
				sQuery += ' AND ';
			sQuery += '(Id > \'' + sOffsetId + '\')';
		}
		
		sQuery += ' ORDER BY Id LIMIT ' + sLimit + ' ALL ROWS';
		try{
			List<Account> listAccount = new QueryWithSharing().query(sQuery);//Database.query(sQuery);
			mapAccount = new Map<Id, Account>(listAccount);
			/*
			if(listAccount.size() == 0) {
				listResult.add(createAccountWithError('Found ' + listAccount.size() + ' Accounts where Id in (' + getCommaSeparatedIdListForQueries(accountIds) + ')'));
			}
			*/
			Map<Id, Id> mapAccountIdPricebookId = new Map<Id, Id>();			
			for (Account a : listAccount) {
				accountIds.add(a.Id);
				mapAccountIdPricebookId.put(a.Id, a.Pricebook__c);
			}
			
			List<CCAccountRole__c> listAccountRoles = [SELECT Id, CustomerNumberAtSlaveAccount__c, SlaveAccount__c, MasterAccount__c FROM CCAccountRole__c WHERE MasterAccount__c IN : accountIds AND AccountRole__c = : ACCOUNT_ROLE_FILTER ];
			Set<Id> setSlaveAccountIds = new Set<Id>();
			
			for (CCAccountRole__c ar : listAccountRoles) {
				setSlaveAccountIds.add(ar.SlaveAccount__c);
			}
			List<Account> listSlaveAccounts = [SELECT Name, Name2__c, BillingAddress__c, Pricebook__c, AccountNumber FROM Account WHERE Id IN : setSlaveAccountIds];
			Map<Id, Account> mapSlaveAccounts = new Map<Id, Account> ([SELECT Id, Name, Name2__c, BillingAddress__c, IsDeleted, Pricebook__c, AccountNumber FROM Account WHERE Id IN : setSlaveAccountIds ALL ROWS]);
			for(Account a : mapSlaveAccounts.values())
				mapAccountIdPricebookId.put(a.Id, a.Pricebook__c);
			
			Map<Id, Pricebook2> mapPricebookIdPricebook = new Map<Id, Pricebook2>([SELECT Name, Id, 
				(SELECT LastModifiedDate, OwnerId, toLabel(DeliveryType__c), toLabel(ListingIndicator__c), ValidFrom__c, ValidUntil__c, Product2__c FROM Pricebook_Entry_Details__r) 
				FROM Pricebook2 WHERE Id IN : mapAccountIdPricebookId.values()]);
			
			Map<Id, CCAccountRole__c> mapAccountIdAccountRole = new Map<Id, CCAccountRole__c>();
			Map<Id, Id> mapMasterAccountSlaveAccount = new Map<Id, Id>();
			for (CCAccountRole__c ar : listAccountRoles) {
				mapAccountIdAccountRole.put(ar.MasterAccount__c, ar);
				mapMasterAccountSlaveAccount.put(ar.MasterAccount__c, ar.SlaveAccount__c);
			}
			
			Map<Id, SCInstalledBaseRole__c> mapAccountId_InstalledBaseRole = new Map<Id, SCInstalledBaseRole__c>();
			Map<Id, Id> mapInstalledBaseRoleId_InstalledBaseLocationId = new Map<Id, Id>();
			Set<Id> installedBaseRoleIds = new Set<Id>();
			Set<Id> installedBaseLocationIds = new Set<Id>();		
			List<SCInstalledBaseRole__c> listInstalledBaseRole = [SELECT Id, InstalledBaseLocation__c, Account__c FROM SCInstalledBaseRole__c WHERE Account__c IN : accountIds];
			
			for (SCInstalledBaseRole__c r : listInstalledBaseRole) {
				mapAccountId_InstalledBaseRole.put(r.Account__c, r);
				mapInstalledBaseRoleId_InstalledBaseLocationId.put(r.Id, r.InstalledBaseLocation__c);
				installedBaseRoleIds.add(r.Id);
				installedBaseLocationIds.add(r.InstalledBaseLocation__c);
			}
						
			String sInstalledBaseLocationQuery = 'SELECT Id, (SELECT ' + String.join(getQueryFieldsFromObject(SC_INSTALLED_BASE_OBJECTNAME), ', ') + 
				' FROM InstalledBaseLocation__r WHERE SerialNo__c != NULL) FROM SCInstalledBaseLocation__c WHERE Id IN (' + getCommaSeparatedIdListForQueries(installedBaseLocationIds) + ')';
			//System.debug('SC INSTALLED BASE LOCATION QUERY: ' + sInstalledBaseLocationQuery);
			List<SCInstalledBaseLocation__c> listInstalledBaseLocation = Database.query(sInstalledBaseLocationQuery);
			Map<Id, SCInstalledBaseLocation__c> mapInstalledBaseLocationId_InstalledBaseLocation = new Map<Id, SCInstalledBaseLocation__c>();
			for(SCInstalledBaseLocation__c l : listInstalledBaseLocation) {
				mapInstalledBaseLocationId_InstalledBaseLocation.put(l.Id, l);
			}
					
			List<Task> listAccountTasks = getAccountTasks(accountIds);
			List<RedSurveyResult__c> listAccountRedSurveyResults = getAccountRedSurveyResult(accountIds);
			List<AccountFeedWithUserName> listAccountFeedsWithUsernames = getAccountFeedsWithUsernames(accountIds);
	
			/* AccountMarketingAttributest */
			Map<Id, Id> ama_acc = new Map<Id, Id>();
			Map<Id, Id> ama_ma = new Map<Id, Id>();
			fillTempMaps(queryAccountMarketingAttributes(accountIds), ama_acc, ama_ma);
			Map<Id, MarketingAttribute__c> marketingAttributeMap = queryMarketingAttributes(new Set<Id>(ama_ma.values()));
			
			for (Account a : mapAccount.values()) {
				objResult = new AccountWithDetails();
				if (a.IsDeleted) {
					objResult.Message = 'Account with ID ' + a.Id + ' is deleted.';
					listResult.add(objResult);	 
					continue;
				}
				if (parameterHasAccountAndTimestamp && !mapAccountId_IsUpdated.get(a.Id)) {
					objResult.Message = 'Account with ID ' + a.Id + ' is already up to date.';
					objResult.AccountIsUpToDate = true;
					listResult.add(objResult);	 
					continue;
				}
				
				objResult.Account = a;
				
				objResult.MarketingAttributes = getFilteredMarketingAttributes(marketingAttributeMap, ama_acc, ama_ma, a.Id);
				objResult.Tasks = getFilteredTasks(listAccountTasks, a.Id, a.OwnerId);
				objResult.RedSurveyResults = getFilteredRedSurveyResults(listAccountRedSurveyResults, a.Id);
				objResult.AccountFeeds = getFilteredAccountFeeds(listAccountFeedsWithUsernames, a.Id);
				objResult.AccountRoles = getFilteredAccountRoles(listAccountRoles, a.Id);
				objResult.SlaveAccountsWithPricebook = getFilteredSlaveAccountsWithPricebook(mapSlaveAccounts, mapPricebookIdPricebook, objResult.AccountRoles);
				objResult.Pricebook = mapPricebookIdPricebook.get(mapAccountIdPricebookId.get(a.Id));
				SCInstalledBaseRole__c role = mapAccountId_InstalledBaseRole.get(a.Id);
				if(role != null){
					Id locId = mapInstalledBaseRoleId_InstalledBaseLocationId.get(role.Id);
					SCInstalledBaseLocation__c location = mapInstalledBaseLocationId_InstalledBaseLocation.get(locId);
					objResult.InstalledBases = location.InstalledBaseLocation__r;
				}
				listResult.add(objResult);
			}
			for (String s : setParameterIds) {
				if(s.length() >= 15 && mapAccount.keySet().contains(s.subString(0, 15)))
					continue;
				objResult = new AccountWithDetails();
				objResult.Message = 'Can\'t find Account with ID ' + s;
				objResult.AccountExists = false;
				listResult.add(objResult);	
			}
		}
		catch(Exception e) {
			listResult.add(createAccountWithError(e));
			listResult[0].Message += '\n' + sQuery;
		}
		RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(listResult));
		restLog.insertLog('Success');
	}		
}
