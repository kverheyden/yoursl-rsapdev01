@isTest
public class cc_cceag_test_cokeid_settings 
{
	static testMethod void testSettings()
    {
        ecomSettings__c settingLogout = new ecomSettings__c(Name='ECOMCokeIDLogoutURL',value__c='logout');
        insert settingLogout;
        ecomSettings__c settingPassword = new ecomSettings__c(Name='ECOMCokeIDChangePasswordURL',value__c='changepwd');
        insert settingPassword; 
        ecomSettings__c settingAccount = new ecomSettings__c(Name='ECOMCokeIDAccountSettingsURL',value__c='accountsettings');
        insert settingAccount;
        
        Test.startTest();
        cc_cceag_cokeid_settings i = new cc_cceag_cokeid_settings();
        
        System.assertEquals(settingLogout.value__c, i.cokeIdLogoutUrl);
        System.assertEquals(settingPassword.value__c, i.cokeIdChangePasswordUrl);
        System.assertEquals(settingAccount.value__c, i.cokeIdAccountSettingsUrl);
        Test.stopTest();
    }
}
