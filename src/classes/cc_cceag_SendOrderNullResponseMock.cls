global with sharing class cc_cceag_SendOrderNullResponseMock implements WebServiceMock  {
	global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
           	system.debug('request name = ' + requestName);
           	system.debug('soap action  = ' + soapAction);
           	system.debug('endpoint  = ' + endpoint);
	   		response.put('response_x', null); 
   }
}
