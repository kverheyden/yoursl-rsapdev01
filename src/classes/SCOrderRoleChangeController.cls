/*
 * @(#)SCOrderRoleChangeController.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.   
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This controller changes the orde role.
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCOrderRoleChangeController
{
    public SCOrderRole__c role { get; set; }
    public String selAccountIRId { get; set; }
    public String selAccountIRName { get; set; }
    private Id orderRoleId = null;
    private Id actualRoleAccount = null;
    public Id oid { get; set; }
    public String errorMessage { get; set; }
    public Boolean isNewAccountSelected { get; set; }
    
    // Constructor
    public SCOrderRoleChangeController()
    {  
        
        errorMessage = '';
        isNewAccountSelected = false;
        
        if (ApexPages.currentPage().getParameters().containsKey('oid'))
        {
            oid = ApexPages.currentPage().getParameters().get('oid');
            
            // Trying to read the order role IR for the selected order
            try{
                orderRoleId = [ Select Id From SCOrderRole__c Where OrderRole__c = '50303' AND Order__c = :oid ].Id;
                actualRoleAccount = [ Select Account__r.Id From SCOrderRole__c Where OrderRole__c = '50303' AND Order__c = :oid].Account__r.Id;
                String lname = [ Select Name1__c From SCOrderRole__c Where OrderRole__c = '50303' AND Order__c = :oid ].Name1__c;
                String fname = [ Select Name2__c From SCOrderRole__c Where OrderRole__c = '50303' AND Order__c = :oid ].Name2__c;
                if(lname == null){ lname = ''; }
                if(fname == null){ fname = ''; }
                selAccountIRName = fname + ' ' + lname;
                errorMessage = '';
            }
            catch( Exception ex )
            {
                System.debug('#### There is no role IR for this order!');
                errorMessage = Label.SC_msg_NoInvoiceRecipient;
            }
        }
        else{
            errorMessage = Label.SC_app_SelectAtLeastOneOrder;
        }
        
    }    

    // Reloads the IR after selecting the new IR-Account
    public PageReference loadNewAccountIR()
    {
        // Checking whether new account was selected
        if(actualRoleAccount != selAccountIRId)
        {
            isNewAccountSelected = true;
            errorMessage = '';
        }
        else
        {
            isNewAccountSelected = false;
            errorMessage = Label.SC_msg_SameAccount;
        }
        
        return null;
    }
    
    public PageReference changeIR()
    {
        SCOrderRole__c roleToChange = new SCOrderRole__c();
        Account selectedAccount = new Account();

        if(orderRoleId != null)
        {
            roleToChange = [ Select Id, Name, Account__c, Name1__c, Name2__c, VipLevel__c,Type__c,  AccountType__c,TaxType__c,TaxCode2__c,TaxCode1__c,
                                    TaxCode0__c,SpecialCustomer__c,SearchCode__c,Salutation__c,RiskClass__c,Phone2__c,Phone__c,PersonTitle__c,
                                    HomePhone__c,Contact__c,MobilePhone__c,LockType__c,LocaleSidKey__c,GeoY__c,GeoX__c,GeoApprox__c,Email__c,
                                    DistanceZone__c,CurrencyIsoCode,Street__c,PostalCode__c,PoBox__c,PoBoxPostCode__c,
                                    PoBoxCity__c,HouseNo__c,Floor__c,FlatNo__c,Extension__c,District__c,County__c,Country__c,
                                    CountryState__c,City__c,AccountNumber__c 
                             From   SCOrderRole__c
                             Where  Id = :orderRoleId ];
        }
        
        if(selAccountIRId != null && selAccountIRId != '')
        {
            selectedAccount = [ Select VipLevel__c, Type, TaxType__c, TaxCode2__c, TaxCode1__c, TaxCode0__c, SpecialCustomer__c, SearchCode__c, 
                                       Salutation__c, RiskClass__c, Phone2__c, Phone,  PersonTitle__c, PersonHomePhone__c, PersonContactId__c, Mobile__c, 
                                       LockType__c, LocaleSidKey__c, LastName__c, IsPersonAccount__c, GeoY__c, GeoX__c,                
                                       GeoStatus__c, GeoApprox__c, FirstName__c, Email__c, DistanceZone__c, CurrencyIsoCode, BillingStreet, 
                                       BillingPostalCode, BillingPOBox__c, BillingPOBoxPostCode__c, BillingPOBoxCity__c, BillingHouseNo__c, 
                                       BillingFloor__c, BillingFlatNo__c, BillingExtension__c, BillingDistrict__c,    
                                       BillingCounty__c, BillingCountry__c, BillingCountryState__c, BillingCity, BillingAddress__c, AccountNumber
                                From   Account 
                                Where  Id = :selAccountIRId];
        }

        // Do this only if order has the IR role
        if(orderRoleId != null)
        {
            roleToChange.Account__c       = selAccountIRId;           
            roleToChange.Name1__c                        = selectedAccount.LastName__c;
            roleToChange.Name2__c                        =  selectedAccount.FirstName__c;
            roleToChange.VipLevel__c                     = selectedAccount.VipLevel__c;           
            roleToChange.Type__c                         = selectedAccount.Type;
            roleToChange.AccountType__c                  = selectedAccount.Type;    
            roleToChange.TaxType__c                      = selectedAccount.TaxType__c;
            roleToChange.TaxCode2__c                     = selectedAccount.TaxCode2__c;             
            roleToChange.TaxCode1__c                     = selectedAccount.TaxCode1__c;             
            roleToChange.TaxCode0__c                     = selectedAccount.TaxCode0__c;             
            roleToChange.SpecialCustomer__c              = selectedAccount.SpecialCustomer__c;  
            roleToChange.SearchCode__c                   = selectedAccount.SearchCode__c;           
            roleToChange.Salutation__c                   = selectedAccount.Salutation__c;          
            roleToChange.RiskClass__c                    = selectedAccount.RiskClass__c;            
            roleToChange.Phone2__c                       = selectedAccount.Phone2__c;               
            roleToChange.Phone__c                        = selectedAccount.Phone;                   
            roleToChange.PersonTitle__c                  = selectedAccount.PersonTitle__c;             
            roleToChange.HomePhone__c                    = selectedAccount.PersonHomePhone__c;         
            roleToChange.Contact__c                      = selectedAccount.PersonContactId__c;         
            roleToChange.MobilePhone__c                  = selectedAccount.Mobile__c;               
            roleToChange.LockType__c                     = selectedAccount.LockType__c;             
            roleToChange.LocaleSidKey__c                 = selectedAccount.LocaleSidKey__c;         
            roleToChange.GeoY__c                         = selectedAccount.GeoY__c;                 
            roleToChange.GeoX__c                         = selectedAccount.GeoX__c;                 
            roleToChange.GeoApprox__c                    = selectedAccount.GeoApprox__c;            
            roleToChange.Email__c                        = selectedAccount.Email__c;                
            roleToChange.DistanceZone__c                 = selectedAccount.DistanceZone__c;         
            roleToChange.CurrencyIsoCode                 = selectedAccount.CurrencyIsoCode;         
            roleToChange.Street__c                       = selectedAccount.BillingStreet;        
            roleToChange.PostalCode__c                   = selectedAccount.BillingPostalCode;    
            roleToChange.PoBox__c                        = selectedAccount.BillingPOBox__c;         
            roleToChange.PoBoxPostCode__c                = selectedAccount.BillingPOBoxPostCode__c;
            roleToChange.PoBoxCity__c                    = selectedAccount.BillingPOBoxCity__c;     
            roleToChange.HouseNo__c                      = selectedAccount.BillingHouseNo__c;       
            roleToChange.Floor__c                        = selectedAccount.BillingFloor__c;         
            roleToChange.FlatNo__c                       = selectedAccount.BillingFlatNo__c;        
            roleToChange.Extension__c                    = selectedAccount.BillingExtension__c;     
            roleToChange.District__c                     = selectedAccount.BillingDistrict__c;  
            roleToChange.County__c                       = selectedAccount.BillingCounty__c;        
            roleToChange.Country__c                      = selectedAccount.BillingCountry__c;       
            roleToChange.CountryState__c                 = selectedAccount.BillingCountryState__c; 
            roleToChange.City__c                         = selectedAccount.BillingCity;      
            roleToChange.AccountNumber__c                = selectedAccount.AccountNumber;   
            
            
            
            
            
            try{
                upsert roleToChange;
                
                // Redirecting back to the order page
                PageReference pageRef;
                pageRef = new PageReference('/' + oid);
                return pageRef.setRedirect(true);
                
            }
            catch(Exception ex)
            {
                System.debug('#### upsert error: ' + ex);
            }
        }
        
        return null;
    }
    
    public PageReference goBack()
    {
        if(oid != null)
        {
            PageReference pageRef;
            pageRef = new PageReference('/' + String.valueOf(oid));
            return pageRef.setRedirect(true);
        }
        else
        {
            return null;
        }
    }
    
    /**
     * Get Account Ids.
     *
     * @return    comma separated Account id string
     *
     * @author Alexei Geiger <ageiger@gms-online.de>
     * @version $Revision$, $Date$
     */
    public String getAccountIds()
    {
        String accountIds = null;

        if( selAccountIRId != null )
        {
          accountIds = addIdString( accountIds, (String)selAccountIRId);
        }

        System.debug('###accountIds: ' + accountIds);
        return accountIds;
    }
    private String addIdString( String str, String id )
    {
        if( str != null )
        {
            str += ',' + id;
        }
        else
        {
            str = id;
        }
        return str;
    }

}
