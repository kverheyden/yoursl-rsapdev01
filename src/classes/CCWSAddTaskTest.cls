/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest //(SeeAllData = true) 
private class CCWSAddTaskTest {

    
    static testMethod void myUnitTest() {
        
        Profile p = [select id from profile where name='System Administrator']; 
        User testUser = new User(alias = 'sta23', email='admin@testorg.com', 
            emailencodingkey='UTF-8', lastname='Testing123', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, 
            timezonesidkey='America/Los_Angeles', username='admin@cceag.de', isActive=true, ID2__c = '12345678');
            
    	System.runAs(testUser) {
	    Account testAccount = new Account();
	 
	    RecordType accountRecType= [SELECT Id, Name, SobjectType FROM RecordType WHERE SobjectType = 'Account' AND Name = 'Customer' LIMIT 1];
	    testAccount.RecordTypeId = accountRecType.Id;
	    
	    testAccount.Name = 'Max Mustermann';
	    testAccount.BillingCountry__c = 'DE';
	    testAccount.ID2__c = '12345678';
	    
	    insert testAccount;
	    testAccount = [SELECT Name, Id, ID2__c FROM Account WHERE Name = 'Max Mustermann' AND BillingCountry__c = 'DE' AND ID2__c = '12345678' LIMIT 1];
	    
	    CCWSAddTask.addTask(testUser.ID2__c, testAccount.ID2__c, 'Call', 'Test Description', System.today());
	    
	    Task ourTask = [Select Subject, OwnerId From Task WHERE WhatId =: testAccount.Id AND OwnerId =: testUser.Id LIMIT 1];
	    
	    //+++++++++++++ special case for exceptions: null accounts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	    
	     Account testAccount2 = new Account();
	     
	     testAccount2.RecordTypeId = accountRecType.Id;
	    
	    testAccount2.Name = 'Max Müller';
	    testAccount2.BillingCountry__c = 'DE';
	    //testAccount2.ID2__c = null;
	    
	    insert testAccount2;
	    testAccount2 = [SELECT Name, Id, ID2__c FROM Account WHERE Name = 'Max Müller' AND BillingCountry__c = 'DE' LIMIT 1];
	    
	    CCWSAddTask.addTask(testUser.ID2__c, testAccount2.ID2__c, 'Call', 'Test Description', System.today());
	    
	    //Task ourTask2 = [Select Subject, OwnerId From Task WHERE WhatId =: testAccount2.Id AND OwnerId =: testUser.Id LIMIT 1];
	    
	    //+++++++++++++ special case for exceptions: null user ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	    
	   User testUser2 = [SELECT Id, Name, isActive, ID2__c FROM User WHERE isActive = true AND ID2__c =: null LIMIT 1];
	   CCWSAddTask.addTask(testUser2.ID2__c, testAccount.ID2__c, 'Call', 'Test Description', System.today());
	   
	   //+++++++++++++ special case for exceptions: user with bad or void id +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	   CCWSAddTask.addTask('###', testAccount.ID2__c, 'Call', 'Test Description', System.today()); 
	   
	   
	   	CCWSAddTask.addTask(testUser.ID2__c, '###', 'Call', 'Test Description', System.today());
	     
            
        //Case with wrong user ID
        CCWSAddChatterFeedItemToAccount.addFeed('123', testAccount.ID2__c, 'Test Discribtion', 'test Title');
        //Case with wrong account id
        CCWSAddChatterFeedItemToAccount.addFeed(testUser.ID2__c, 'zxc', 'Test Discribtion', 'test Title');
		CCWSAddChatterFeedItemToAccount.addFeed(testUser.ID2__c, testAccount.ID2__c, 'Test Discribtion', 'test Title');
        }
    }
    
    
}
