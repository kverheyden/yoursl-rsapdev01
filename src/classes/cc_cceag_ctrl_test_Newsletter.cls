@isTest(SeeAllData=true)
private class cc_cceag_ctrl_test_Newsletter {

    /**
     * Default profile name
     */
    private static String communityProfileName = 'CloudCraze Customer Community User';


    /**
     * Returns the profile for the given name
     * @return Profile
     */
    public static Profile getProfile (String profileName) {

        Profile profile = [
            SELECT
                Id
            FROM
                Profile
            WHERE
            Name = :profileName
        ];

        return profile;
    }

    /**
     * Creates a user with its requried fields
     */
    private static User createTestUser (Boolean hasPromotionNewsletter, Boolean hasProductNewsletter)
    {
        Account account = new Account(
            Name = 'Account'
        );
        insert account;
           
        Contact contact = new Contact(
            AccountId = account.Id,
            LastName = 'Lastname',
            promotionnewsletter__c = hasPromotionNewsletter,
            productnewsletter__c = hasProductNewsletter
        );
        insert contact;
        
        Profile profile = getProfile(communityProfileName);

        User user = new User(
            Username = 'user@example.org',
            LastName = 'Lastname',
            Email = 'user@example.org',
            Alias = 'alias',
            CommunityNickname = 'nickname',
            TimeZoneSidKey = 'Europe/Berlin',
            LocaleSidKey = 'de',
            EmailEncodingKey = 'UTF-8',
            ProfileId = profile.Id,
            LanguageLocaleKey = 'de',
            ContactId = contact.Id
        );

        return user;
    }

    
    public static testMethod void testFetchDataReturnsUserContactData ()
    {
        Boolean hasPromotionNewsletter = true;
        Boolean hasProductNewsletter = true;

        User testUser = createTestUser(hasPromotionNewsletter, hasProductNewsletter);

        System.runas(testUser) {
    
            ccrz.cc_RemoteActionContext context = new ccrz.cc_RemoteActionContext();
            
            ccrz.cc_RemoteActionResult result = cc_cceag_ctrl_Newsletter.fetchData(context);
    
            System.assertEquals(true, result.success);
    
            Contact contact = (Contact) result.data;
            System.assertEquals(hasPromotionNewsletter, contact.promotionnewsletter__c);
            System.assertEquals(hasProductNewsletter, contact.productnewsletter__c);
        }
    }

    
    public static testMethod void testSaveDataUpdatesUserContactData ()
    {
        Boolean hasPromotionNewsletter = false;
        Boolean hasProductNewsletter = false;

        User testUser = createTestUser(hasPromotionNewsletter, hasProductNewsletter);

        System.runas(testUser) {
    
            ccrz.cc_RemoteActionContext context = new ccrz.cc_RemoteActionContext();
            
            ccrz.cc_RemoteActionResult result = cc_cceag_ctrl_Newsletter.saveData(context, '1', '1');
    
            System.assertEquals(true, result.success);
            
            User user = [
                SELECT
                    Contact.promotionnewsletter__c,
                    Contact.productnewsletter__c
                FROM
                    User
                WHERE
                    Id = :testUser.Id
            ];
            
            Contact contact = user.Contact;
            System.assertEquals(true, contact.promotionnewsletter__c);
            System.assertEquals(true, contact.productnewsletter__c);
        }
    }

    
    public static testMethod void testHandleException ()
    {
        ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
        NullPointerException thrownException = new NullPointerException();
        
        cc_cceag_ctrl_Newsletter.handleException(result, thrownException);
               
        List<ccrz.cc_bean_Message> messages = (List<ccrz.cc_bean_Message>) result.messages;
        System.assertEquals(1, messages.size());
        ccrz.cc_bean_Message message = messages.get(0);
        System.assertEquals(ccrz.cc_bean_Message.MessageType.CUSTOM, message.type);
        System.assertEquals('messagingSection-Error', message.classToAppend);
        System.assertEquals(ccrz.cc_bean_Message.MessageSeverity.ERROR, message.severity);
    }
}
