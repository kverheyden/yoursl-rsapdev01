global with sharing class cc_cceag_ctrl_CartAlternatives {
	public cc_cceag_ctrl_CartAlternatives() {
		
	}

	@RemoteAction
	global static Map<String, List<cc_cceag_bean_Product>> fetchAlternateItems(ccrz.cc_RemoteActionContext ctx, List<String> prodIds, Map<String, String> prodLineItems) {
		ccrz.cc_CallContext.initRemoteContext(ctx);
		Map<String, List<cc_cceag_bean_Product>> retData = new Map<String, List<cc_cceag_bean_Product>>();
		List<ccrz__E_RelatedProduct__c> relProds = cc_cceag_dao_Cart.getRelatedProducts(prodIds, new List<String> { 'CrossSell' });
		List<String> relIds = new List<String>();
		for (ccrz__E_RelatedProduct__c relProd: relProds)
			relIds.add(relProd.ccrz__RelatedProduct__c);

		List<ccrz__E_ProductMedia__c> pmList = [Select ccrz__AltMessage__c, ccrz__filePath__c, ccrz__EndDate__c, Id, ccrz__Product__c, ccrz__Product__r.Id, ccrz__Product__r.Name, ccrz__Sequence__c, ccrz__StartDate__c, ccrz__URI__c, ccrz__MediaType__c, ccrz__ProductMediaSource__c, ccrz__StaticResourceName__c, (SELECT Id, Name, ParentId FROM Attachments) from ccrz__E_ProductMedia__c where ccrz__Product__r.Id =: relIds and ccrz__MediaType__c = 'Product Search Image' and ccrz__Enabled__c = true];
		Map<String, String> mediaMap = new Map<String, String>();
		for (ccrz__E_ProductMedia__c pm: pmList) {
			if ('Attachment' == pm.ccrz__ProductMediaSource__c) {
				List<Attachment> attachmentList = pm.Attachments;
				for (Attachment a: attachmentList) {
					mediaMap.put(pm.ccrz__Product__r.Id, 'servlet/servlet.FileDownload?file='+a.id);
				}
			}
		}
		ccrz.cc_api_InventoryExtension inventoryApi = new cc_cceag_api_InventoryExt();
		Map<Id,ccrz__E_ProductInventoryItem__c> inventoryMap = inventoryApi.getAvailabilityQtyMsg(relIds);
		
		for (ccrz__E_RelatedProduct__c relProd: relProds) {
			ccrz__E_ProductInventoryItem__c invData = inventoryMap.get(relProd.ccrz__RelatedProduct__c);
			if (invData != null && invData.ccrz__QtyAvailable__c > 0) {
				if (!retData.containsKey(relProd.ccrz__Product__c))
					retData.put(relProd.ccrz__Product__c, new List<cc_cceag_bean_Product>());
				List<cc_cceag_bean_Product> prodList = retData.get(relProd.ccrz__Product__c);
				cc_cceag_bean_Product prodBean = new cc_cceag_bean_Product();
				prodBean.id = relProd.ccrz__RelatedProduct__c;
				prodBean.sku = relProd.ccrz__RelatedProduct__r.ccrz__Sku__c;
				prodBean.name = relProd.ccrz__RelatedProduct__r.Name;
				prodBean.shortDesc = relProd.ccrz__RelatedProduct__r.ccrz__ShortDesc__c;
				prodBean.imageURL = mediaMap.get(relProd.ccrz__RelatedProduct__c);
				prodList.add(prodBean);
			}
		}
		//query account group
		//query pricelist items
		//retrieve items, and run intersect with the rel prods 
		return retData;
	}

	@RemoteAction
	global static ccrz.cc_RemoteActionResult postProc(ccrz.cc_RemoteActionContext ctx) {
		ccrz.cc_CallContext.initRemoteContext(ctx);
		ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
		result.inputContext = ctx;
		result.success = false; 
		try {
			ccrz__E_Cart__c cartData = [SELECT Id, ccrz__ValidationStatus__c FROM ccrz__E_Cart__c WHERE ccrz__EncryptedId__c =: ctx.currentCartID LIMIT 1];
			List<ccrz__E_CartItem__c> cartItems = [SELECT Id, ccrz__Price__c, ccrz__SubAmount__c FROM ccrz__E_CartItem__c WHERE ccrz__Cart__r.ccrz__EncryptedId__c =: ctx.currentCartID];
			cartData.ccrz__ValidationStatus__c = 'CartAuthUserValidated';
			update cartData;
			for (ccrz__E_CartItem__c cartItem: cartItems) {
				cartItem.ccrz__Price__c = 0;
				cartItem.ccrz__SubAmount__c = 0;
			}
			update cartItems;
			result.success = true;
		} catch(Exception e) {
			System.debug(System.LoggingLevel.ERROR, e.getMessage());
            System.debug(System.LoggingLevel.ERROR, e.getStackTraceString());
            ccrz.cc_bean_Message msg = new ccrz.cc_bean_Message();
			msg.type = ccrz.cc_bean_Message.MessageType.CUSTOM;
			msg.classToAppend = 'messagingSection-Error';
			msg.message = e.getStackTraceString();
			msg.severity = ccrz.cc_bean_Message.MessageSeverity.ERROR;
			result.messages.add(msg);
        }
		return result;
	}

	@RemoteAction
	global static boolean replaceItem(String storeName, String portalUserId, String cartId, String oldItem, String newItem, Integer qty) {
		List<String> prodIds = new List<String> { oldItem, newItem };
		List<ccrz__E_CartItem__c> cartItems = [SELECT ccrz__Product__c,ccrz__Quantity__c,ccrz__AvailabilityMessage__c FROM ccrz__E_CartItem__c WHERE ccrz__Product__c =: prodIds AND ccrz__Cart__r.ccrz__EncryptedId__c =: cartId];
		if (cartItems.size() > 1) {
			ccrz__E_CartItem__c existingNewEntry = null;
			ccrz__E_CartItem__c oldItemEntry = null;
			if (cartItems.get(0).ccrz__Product__c == newItem) {
				existingNewEntry = cartItems.get(0);
				oldItemEntry = cartItems.get(1);
			}
			else {
				existingNewEntry = cartItems.get(1);
				oldItemEntry = cartItems.get(0);
			}
			existingNewEntry.ccrz__Quantity__c = existingNewEntry.ccrz__Quantity__c + qty;
			existingNewEntry.ccrz__AvailabilityMessage__c = oldItemEntry.ccrz__Product__c;
			update existingNewEntry;
			delete oldItemEntry;
		}
		else {
			for (ccrz__E_CartItem__c cartItem: cartItems) {
				cartItem.ccrz__AvailabilityMessage__c = cartItem.ccrz__Product__c;
				cartItem.ccrz__Product__c = newItem;
				cartItem.ccrz__Quantity__c = qty;
			}
			update cartItems;
		}
		return true;
	}

	@RemoteAction
	global static ccrz.cc_RemoteActionResult sendMail(ccrz.cc_RemoteActionContext ctx, String emailAddress) {
		ccrz.cc_CallContext.initRemoteContext(ctx);
		ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
		result.inputContext = ctx;
		result.success = false; 
		try {
			ccrz__E_Cart__c cart = [SELECT Id FROM ccrz__E_Cart__c WHERE ccrz__EncryptedId__c =: ctx.currentCartId LIMIT 1];
			PageReference emailRef = Page.cc_cceag_CartEMail;
            emailRef.getParameters().put('cartId', cart.Id);
            emailRef.getHeaders().put('cartId', cart.Id);
            Blob content = null;
            if (Test.isRunningTest())
            	content = Blob.valueOf('<html><head></head><body>hello</body></html>');
            else
            	content = emailRef.getContent();
            String[] toAddresses = new String[] { emailAddress };

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setSubject('Warenkorb');
	        mail.setToAddresses(toAddresses);
            mail.setSenderDisplayName('Webstore');
            mail.setSaveAsActivity(false);
            mail.setHTMLBody(content.toString());
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        	result.success = true;
		}
		catch(Exception e) {
			System.debug(System.LoggingLevel.ERROR, e.getMessage());
            System.debug(System.LoggingLevel.ERROR, e.getStackTraceString());
            ccrz.cc_bean_Message msg = new ccrz.cc_bean_Message();
			msg.type = ccrz.cc_bean_Message.MessageType.CUSTOM;
			msg.classToAppend = 'messagingSection-Error';
			msg.message = e.getStackTraceString();
			msg.severity = ccrz.cc_bean_Message.MessageSeverity.ERROR;
			result.messages.add(msg);
        }
		return result;
	}

}
