/*
 * @(#)CCWCArchiveDocumentInsert.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * The class exports an Salesforce order close information into SAP
 * 
 * The entry method is: 
 *      callout(attachmentId, async, testMode);
 *      calloutOid(oid, async, testMode);
 *
 * Methods to override: 
 *      fillAndSendERPData(...)
 *      fillAndSendERPData(...)
 *
 * Test:
 * String attachmentId = '00PM0000000roldMAA'; 
 * boolean async = true;
 * boolean testMode = false;
 * with attachment
 * CCWCArchiveDocumentInsert.callout(attachmentId, async, testMode);
 * with order
 * CCWCArchiveDocumentInsert.calloutOid(orderID, async, testMode)
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @author Georg Birkenheuer <gbirkenheuerk@gms-online.de>
 */
global without sharing class CCWCArchiveDocumentInsert extends SCInterfaceExportBase 
{
    public static String MODE_FOR_ORDER = 'FOR_ORDER';
    public static String MODE_FOR_ORDER_ITEM = 'FOR_ORDER_ITEM';
    public static String MODE_FOR_INVENTORY = 'FOR_INVENTORY';

    public static String KEY_ATTACHMENT = 'KEY_ATTACHMENT';
    public static String KEY_ORDER_ITEM = 'ORDER_ITEM';
    public static String KEY_ORDER_ROLE = 'ORDER_ROLE';
    public static String KEY_ASSIGNMENT = 'ASSIGNMENT'; 
    public static String KEY_INVENTORY = 'INVENTORY';

    public static String KEY_OBJECT_NAME = 'OBJECT_NAME';   
    
    public CCWCArchiveDocumentInsert()
    {
    }
    public CCWCArchiveDocumentInsert(String interfaceName, String interfaceHandler, String refType, String subclassName)
    {
        super(interfaceName, interfaceHandler, refType, subclassName);
    }
    // If there is a four parameter constructor, Salesforce expects three parameter constructor
    public CCWCArchiveDocumentInsert(String interfaceName, String interfaceHandler, String refType)
    {
        super(interfaceName, interfaceHandler, refType, 'Dummy');
    }

    /**
     * Prepare to make the webservice callout synchrounously or asynchronously (future)
     *
     * The default mode is  CCWCArchiveDocumentInsert.MODE_FOR_ORDER
     *
     * @param oid   attachment id
     * @param async     true: use a future method to call 
     *                  false: do a synchrounous callout
     * @param testMode true: emulate call
     */
    public static String callout(String attachmentId, boolean async, boolean testMode)
    {
        String interfaceName = 'SAP_ARCHIVE_DOCUMENT_INSERT';
        String interfaceHandler = 'CCWCArchiveDocumentInsert';
        String refType = null;
        CCWCArchiveDocumentInsert oc = new CCWCArchiveDocumentInsert(interfaceName, interfaceHandler, refType);
        return oc.calloutCore(attachmentId, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCArchiveDocumentInsert', null, null, 
            MODE_FOR_ORDER);
    } // callout   
    
    /*
     * Prepare to make the webservice callout synchrounously or asynchronously (future)
     *
     * The default mode is CCWCArchiveDocumentInsert.MODE_FOR_ORDER
     *
     * @param oid   attachment orderID
     * @param async     true: use a future method to call 
     *                  false: do a synchrounous callout
     * @param testMode true: emulate call
     */
    public static String calloutOid(String oid, boolean async, boolean testMode)
    {
    if(String.isNotBlank(oid))
        {
            List<Attachment> docs = [ Select Id 
                                      From Attachment 
                                      Where ParentId = :oid 
                                      Limit 1000 ];

            if(!docs.isEmpty())
            {
                Integer count = 0;
                
                for(Attachment a : docs)
                {
                    try
                    {
                        CCWCArchiveDocumentInsert.callout(a.id, true, false);
                        count++;
                    }
                    catch(Exception e)
                    {
                        System.debug('#### Cannot do callout CCWCArchiveDocumentInsert: ' + e.getMessage());
                    }
                }
                
                return Label.SC_app_ArchiveOk + ' ' + count;
            }
            else
            {
                return Label.SC_app_NoAttachments;
            }
        }
        else
        {
            return Label.SC_app_WrongOrderId + ': ' + oid;
        }
    } // callout  
    
    /*
     * Callable mehtod from the CP GUI via Java Script
     * Used by: SCbtnArchiveInventoryDocument
     * Mode MODE_FOR_INVENTORY
     * 
     * @param invID  id of the Inventory 
     * 
     */
    WebService static String wsInvCallout(String invID)
    {
    if(String.isNotBlank(invID))
        {
            List<Attachment> docs = [ Select Id 
                                      From Attachment 
                                      Where ParentId = :invID 
                                      Limit 1000 ];

            if(!docs.isEmpty())
            {
                Integer count = 0;
                
                for(Attachment a : docs)
                {
                    try
                    {
                        CCWCArchiveDocumentInsert.callout(a.id, MODE_FOR_INVENTORY, true, false);
                        count++;
                    }
                    catch(Exception e)
                    {
                        System.debug('#### Cannot do callout CCWCArchiveDocumentInsert: ' + e.getMessage());
                    }
                }
                
                return Label.SC_app_ArchiveOk + ' ' + count;
            }
            else
            {
                return Label.SC_app_NoAttachments;
            }
        }
        else
        {
            return 'Wrong inventory ID: ' + invID;
        }
    } // callout     
    

    /**
     * Prepare to make the webservice callout synchrounously or asynchronously (future)
     *
     *
     * @param attachmentid   attachment id
     * @param mode  FOR_ORDER, FOR_ORDER_ITEM, FOR_INVENTORY 
     *              use constants:
     *              CCWCArchiveDocumentInsert.MODE_FOR_ORDER
     *              CCWCArchiveDocumentInsert.MODE_FOR_ORDERMODE_FOR_ORDER_ITEM
     *              CCWCArchiveDocumentInsert.MODE_FOR_INVENTORY
     * 
     * @param async     true: use a future method to call 
     *                  false: do a synchrounous callout
     * @param testMode true: emulate call
     */
    public static String callout(String attachmentId, String mode, boolean async, boolean testMode)
    {
//        Sebastian bitte zuerst eine Klasse holen und dann aktualisieren         
//        if (!testMode)
//        {
            String plantName = null;
            String stockName = null;
            String interfaceName = 'SAP_ARCHIVE_DOCUMENT_INSERT';
            String interfaceHandler = 'CCWCArchiveDocumentInsert';
            String refType;
            if(mode == MODE_FOR_ORDER || mode == MODE_FOR_ORDER_ITEM)
            {
                refType = 'SCOrder__c';
            }
            else if(mode == MODE_FOR_INVENTORY)
            {
                refType = 'SCInventory__c';
            }
            CCWCArchiveDocumentInsert oc = new CCWCArchiveDocumentInsert(interfaceName, interfaceHandler, refType);
            return oc.calloutCore(attachmentId, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCArchiveDocumentInsert',
                           plantName, stockName, mode);
//        }
//        else
//        {
//            //TODO: test  
//        }
//        return '';
    } // callout   



    /**
     * Reads attachment and header objects to send
     * @param objectId in this case without meaning
     * @param testMode if true no call is made, because in the test mode the salesforce framework prevent it.
     *
     */
    public override void readSalesforceData(String objectID, Map<String, Object> retValue, Boolean testMode)
    {
        retValue.put(KEY_OBJECT_NAME, objectID);
        List<Attachment> attachmentList = [Select Body,BodyLength,ContentType,CreatedById,CreatedDate,Description,Id,
                        IsDeleted,IsPrivate,LastModifiedById,LastModifiedDate,Name,OwnerId,ParentId,SystemModstamp  
                        from Attachment where id = : objectID]; 

        if(attachmentList.size() > 0)
        {
            //Blob body = attachmentList[0].Body;   
            retValue.put(KEY_ATTACHMENT, attachmentList[0]);
            ID parentId = attachmentList[0].ParentId;
            if(mode == MODE_FOR_ORDER || mode == MODE_FOR_ORDER_ITEM)
            {
                List<SCOrderItem__c> orderItemList = null;
                if(mode == MODE_FOR_ORDER)
                {
                    orderItemList = [select id, Order__c, Order__r.Name, Order__r.ERPOrderNo__c, 
                                                        Order__r.DepartmentCurrent__r.Name,
                                                        InstalledBase__r.IDExt__c, InstalledBase__r.SerialNo__c
                                                        from SCOrderItem__c where Order__r.id = : parentId];
                    if(orderItemList.size() == 0)
                    {
                        String msg = 'There is no order item for an order attachment with id: ' + objectID;
                        debug(msg);
                        throw new SCfwException(msg);
                    }
                    
                    //retValue.put(KEY_OBJECT_NAME, orderItemList[0].Order__r.Name);
                }
                else if(mode == MODE_FOR_ORDER_ITEM)
                {                                                   
                    orderItemList = [select id, Order__c, Order__r.Name, Order__r.ERPOrderNo__c, 
                                                        Order__r.DepartmentCurrent__r.Name,
                                                        InstalledBase__r.IDExt__c, InstalledBase__r.SerialNo__c
                                                        from SCOrderItem__c where id = : parentId];
                    if(orderItemList.size() == 0)
                    {
                        String msg = 'There is no order item for an order attachment with id: ' + objectID;
                        debug(msg);
                        throw new SCfwException(msg);
                    }                                               
                    //retValue.put(KEY_OBJECT_NAME, orderItemList[0].Name);
                }
                retValue.put(ROOT, orderItemList[0]);
                
                ID orderId = orderItemList[0].Order__c;
                setReferenceID(orderId);
                setReferenceType('SCOrder__c');
                
                List<SCOrderRole__c> orderRoleList = [Select AccountNumber__c from SCOrderRole__c 
                                    where Order__r.ID = : orderId and OrderRole__c = '50301'];
                if(orderRoleList.size() == 0)
                {
                    String msg = 'There is no service recipment for an order attachment with id: ' + objectID;
                    debug(msg);
                    throw new SCfwException(msg);
                }                                           
                retValue.put(KEY_ORDER_ROLE, orderRoleList[0]);
                
                List<SCAssignment__c> assignmentList = [Select CreatedDate, Employee__c, Resource__r.ID2__c from SCAssignment__c 
                                            where Order__c = :orderId];
                if(assignmentList.size() == 0)
                {
                    String msg = 'There is no assignment for an order attachment with id: ' + objectID;
                    debug(msg);
                    //throw new SCfwException(msg);
                }                                           
                else
                {
                    retValue.put(KEY_ASSIGNMENT, assignmentList[0]);
                }          
            }
            else if(mode == MODE_FOR_INVENTORY)
            {
                List<SCInventory__c> invList = [Select Name, ERPInventoryCountNumber__c, Stock__r.Plant__r.Name from SCInventory__c where id = : parentId];
                if(invList.size() == 0)
                {
                    String msg = 'There is no inventory for an inventory attachment with id: ' + objectID;
                    debug(msg);
                    throw new SCfwException(msg);
                }
                //retValue.put(KEY_OBJECT_NAME, invList[0].Name);
                retValue.put(ROOT, invList[0]);
                setReferenceID(parentId);
                setReferenceType('SCInventory__c');
            }
            else
            {
                throw new SCfwException('Handling for the mode : ' + mode + ' has not been defined!');
            }           
        }
        else
        {
            String msg = 'There is no attachment with the id: ' + objectID;
            debug(msg);
            throw new SCfwException(msg);
        }
        debug('readSalesForceData: ' + retValue);

    }

    /**
     * Fills the send structure and sends the call to SAP
     * 
     * @param dataMap with the order under the key ROOT
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure
     *           message = data sent to ERP
     *           message_v1 = endpoint 
     *           message_v2 = a return value of web service 
     *           message_v3 = a mandatory fields error
     *
     */
    public override Boolean fillAndSendERPData(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs)
    {
        Boolean retValue = true; // Sebastian warum wurde es auf false gesetzt
        // instantiate the client of the web service
        piCceagDeSfdcCSArchiveDocumentInsert.HTTPS_Port ws = new piCceagDeSfdcCSArchiveDocumentInsert.HTTPS_Port();
        // instantiate a message header
        piCceagDeSfdcCSArchiveDocumentInsert.BusinessDocumentMessageHeader messageHeader = new piCceagDeSfdcCSArchiveDocumentInsert.BusinessDocumentMessageHeader();

        // sets the basic authorization for the web service
        ws.inputHttpHeaders_x = u.getBasicAuth();

        // sets the endpoint of the web service
        ws.endpoint_x  = u.getEndpoint('ArchiveDocumentInsert');
        debug('endpoint: ' + ws.endpoint_x);
        rs.message_v1 = ws.endpoint_x;
        ws.timeout_x = u.getTimeOut();

        SCOrderItem__c orderItem = null;
        Attachment attachment = null;
        SCOrderRole__c orderRole = null;
        SCAssignment__c assignment = null;
        SCInventory__c inventory = null;
        try
        {
            if(mode == MODE_FOR_ORDER || mode == MODE_FOR_ORDER_ITEM)
            {
                orderItem = (SCOrderItem__c)dataMap.get(ROOT);
                attachment = (Attachment)dataMap.get(KEY_ATTACHMENT);
                orderRole = (SCOrderRole__c)dataMap.get(KEY_ORDER_ROLE);
                if(dataMap.containsKey(KEY_ASSIGNMENT))
                {
                    assignment = (SCAssignment__c)dataMap.get(KEY_ASSIGNMENT);
                }    
            }
            else
            {
                inventory = (SCInventory__c)dataMap.get(ROOT);
                attachment = (Attachment)dataMap.get(KEY_ATTACHMENT);
            }
            // instantiate the body of the call
            piCceagDeSfdcCSArchiveDocumentInsert.ArchiveDocument_element archiveDocumentElement = setArchiveDocument(orderItem, 
                                        attachment, orderRole, assignment, inventory, u, testMode);
            if(archiveDocumentElement != null)
            {
                debug('archiveDocumentElement: ' + archiveDocumentElement);
                String clockPortNameOfObject = (String)dataMap.get(KEY_OBJECT_NAME);
                setMessageHeader(messageHeader, clockPortNameOfObject, messageID, u, testMode);
                
    
                String jsonInputMessageHeader = JSON.serialize(messageHeader);
                String fromJSONMapMessageHeader = getDataFromJSON(jsonInputMessageHeader);
                debug('from json Message Header: ' + fromJSONMapMessageHeader);
    
                String jsonInputArchiveDocument = JSON.serialize(archiveDocumentElement);
                String fromJsonInputArchiveDocument = getDataFromJSON(jsonInputArchiveDocument);
                debug('from json material array: ' + fromJsonInputArchiveDocument);
                
                rs.message = '\n\nMessageHeader: ' + fromJSONMapMessageHeader + ',\n\nmessageData: ' + fromJsonInputArchiveDocument;
                // check if there are missing mandatory fields
                if(rs.message_v3 == null || rs.message_v3 == '')
                {
                    // All mandatory fields are filled
                    // callout
                    if(!testMode)
                    {
                        // It is not the test from the test class
                        // go
                        rs.setCounter(1);
                        ws.ArchiveDocumentCreate_Out(messageHeader, archiveDocumentElement);
                        rs.Message_v2 = 'void';
                    }
                }
            }
            else
            {
                retValue = false;
                // the interface log must not be written
            }
        }
        catch(Exception e)
        {
            throw e;
        }
        return retValue;
    }
    
    /**
     * sets data into the message header
     * messageID, referenceID = order.Name, CreationDateTime, SenderBusinessSystemID, RecipientBusinessSystemID
     *
     * @param mh message header 
     * @param order 
     * @param messageID MS: milliseconds R: random, e.g.    MS:1351254139745-R:0.7486318721270884
     * @param u CCWSUtil instance
     * @param testMode used to prevent call out by calls from CCWCOrderCloseTest class
     *
     */
    public void  setMessageHeader(piCceagDeSfdcCSArchiveDocumentInsert.BusinessDocumentMessageHeader mh, 
                                         String clockPortNameOfObject, String messageID, CCWSUtil u, Boolean testMode)
    {
        mh.MessageID = messageID;
//      mh.MessageUUID;
        mh.ReferenceID = clockPortNameOfObject;
//      mh.ReferenceUUID;
        mh.CreationDateTime = u.getFormatedCreationDateTime();
        if(testMode)
        {
            mh.TestDataIndicator = 'Test';
        }    
        mh.SenderBusinessSystemID = u.getSenderBusinessSystemID();
        mh.RecipientBusinessSystemID = u.getRecipientBusinessSystemID ();
        debug('Message Header: ' + mh);
    }

    /**
     * sets the call out order structure with data form an order
     *
     * @param mml list of material movements
     * @param u instance of the util class CCWSUtil
     * @param testMode used to prevent call out by calls from CCWCOrderCloseTest class
     *
     * @return ArchiveDocument_element[] 
     *
     */
    public piCceagDeSfdcCSArchiveDocumentInsert.ArchiveDocument_element setArchiveDocument(SCOrderItem__c orderItem,  
                                            Attachment attachment, SCOrderRole__c orderRole, SCAssignment__c assignment,
                                            SCInventory__c inventory, CCWSUtil u, Boolean testMode)
    {
        piCceagDeSfdcCSArchiveDocumentInsert.ArchiveDocument_element retValue = new piCceagDeSfdcCSArchiveDocumentInsert.ArchiveDocument_element();
        String step = '';
        try
        {
            if(mode == MODE_FOR_ORDER || mode == MODE_FOR_ORDER_ITEM)
            {
                if(orderItem != null)
                {
                    retValue.DocumentType = 'OR'; 
                    retValue.DocumentNumber = orderItem.Order__r.ERPOrderNo__c;
                    retValue.PrintoutDate = attachment.CreatedDate.date();
                    retValue.PrintoutTime =  u.formatToTime(attachment.CreatedDate) + 'Z'; //HH:MI:SSZ
                    retValue.CustomerNumber = orderRole.AccountNumber__c;
                    if(assignment != null)
                    {
                        retValue.SignerName = assignment.Employee__c;
                    }
                    retValue.EquipmentNumber = orderItem.InstalledBase__r.IDExt__c;
                    retValue.SerialNumber = orderItem.InstalledBase__r.SerialNo__c;
                    retValue.WorkCenter =  u.getWorkCenter();   //assignment.Resource__r.ID2__c;
                                                                //SCOrder__c.ClosedByInfo__c ?;
                    retValue.CreationDate = attachment.CreatedDate.date();
                    retValue.CreationTime = u.formatToTime(attachment.CreatedDate) + 'Z';
                    String attID = attachment.ID;//orderItem.Order__r.Name;
                    if(attID.length() > 15)
                    {
                        retValue.ReferenceNumber = attID.substring(0,15);
                    }
                    else
                    {
                        retValue.ReferenceNumber = attID;
                    }
                    
                    retValue.Plant = orderItem.Order__r.DepartmentCurrent__r.Name;
                }
            }
            else
            {
                debug('setArchiveElement Inventory');
                // Inventory
                retValue.DocumentType = 'IC';
                retValue.DocumentNumber = inventory.Name;//ERPInventoryCountNumber__c;
                retValue.PrintoutDate = attachment.CreatedDate.date();
                retValue.PrintoutTime = u.formatToTime(attachment.CreatedDate) + 'Z';
                retValue.WorkCenter = u.getWorkCenter();
                retValue.Plant = inventory.Stock__r.Plant__r.Name; 
                retValue.CreationDate = attachment.CreatedDate.date();
                retValue.CreationTime = u.formatToTime(attachment.CreatedDate) + 'Z';              
                String attID = attachment.ID;//inventory.Name;
                if(attID.length() > 15)
                {
                    retValue.ReferenceNumber = attID.substring(0,15);
                }
                else
                {
                    retValue.ReferenceNumber = attID;
                }
                    
            }
            retValue.DocumentMIMECode = 'application/pdf';    
            retValue.DocumentContent = EncodingUtil.base64Encode(attachment.Body);
        }
        catch(Exception e)
        {
            String prevMsg = e.getMessage();
            String newMsg = 'setArchiveDocument: ' + step + ' ' + prevMsg;
            debug(newMsg);
            e.setMessage(newMsg);
            throw e;
        }
        return retValue;    
    }

    /**
     * Sets the ERPStatus in the root object list to 'pending'
     * 
     * @param dataMap of obects to dispatch
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure with error messages
     * @param error true if some error has been occured
     */
    public override void setERPStatus(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs, Boolean error)
    {
        if(mode == MODE_FOR_ORDER_ITEM)
        {
            SCOrderItem__c orderItem = (SCOrderItem__c)dataMap.get(ROOT);
            if(error)
            {
                orderItem.ERPStatusArchiveDocumentInsert__c = 'error';
            }
            else
            {
                orderItem.ERPStatusArchiveDocumentInsert__c = 'pending';
            }
            orderItem.ERPResultDate__c = DateTime.now();     
            update orderItem;
            updateOrder(orderItem.Order__c, orderItem.ERPStatusArchiveDocumentInsert__c);
            
        }
        else if(mode == MODE_FOR_ORDER)
        {
            
            SCOrderItem__c orderItem = (SCOrderItem__c)dataMap.get(ROOT);
            String status = null;
            if(error)
            {
                status = 'error';
            }
            else
            {
                status = 'pending';
            }    
            updateOrder(orderItem.Order__c, status);
        }
        else if(mode == MODE_FOR_INVENTORY)
        {
            SCInventory__c inv = (SCInventory__c)dataMap.get(ROOT);
            if(error)
            {
                inv.ERPStatusArchiveDocumentInsert__c = 'error';
            }
            else
            {
                inv.ERPStatusArchiveDocumentInsert__c = 'pending';
            }    
            inv.ERPResultDate__c = DateTime.now();   
            update inv;
        }


    }

    public void updateOrder(ID orderID, String status)
    {
        if(orderID != null)
        {
            List<SCOrder__c> orderList = [Select ID from SCOrder__c where id = :orderID];
            if(orderList != null && orderList.size() > 0)
            {
                for(SCOrder__c o: orderList)
                {
                    if(status != null)
                    {
                        o.ERPStatusArchiveDocumentInsert__c = status;
                        o.ERPResultDate__c = DateTime.now();
                    }       
                }
                update orderList;
            }
        }       
    }


    /**
     * gets the account number of the invoice recipient. If there is not such the service recipient is returned instead
     *
     * @param order the order tree with data
     *
     * @return the account number of the invoice recipient
     */
    public String getInvoiceRecipient(SCOrder__c order)
    {
        String retValue = order.OrderRole__r[0].Account__r.AccountNumber;
        if(order.OrderRole__r[1] != null)
        {
            retValue = order.OrderRole__r[1].Account__r.AccountNumber;
        }
        return retValue;
    }
    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }
}
