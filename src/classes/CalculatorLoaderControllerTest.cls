@isTest(SeeAllData=true)
private class CalculatorLoaderControllerTest {
  static testMethod void testCalculatorLoaderController() {
    Folder f = [SELECT Id FROM Folder WHERE Type = 'Document' LIMIT 1];
    System.assert(f != null);
    
    Document d = new Document();
    d.Name = 'Testclass Calculator Combined Menu ';
    d.DeveloperName = 'TestclassCalculatorCombinedMenu';
    d.Body = Blob.valueOf('d body');
    d.FolderId = f.Id;
    d.ContentType='text/html';
    insert d;
    
    
    Test.setCurrentPageReference(new PageReference('Page.CokeConnectCalculatorLoader'));
    System.currentPageReference().getParameters().put('name', 'TestclassCalculatorCombinedMenu');

    
    CalculatorLoaderController calculator = new CalculatorLoaderController();
    
    System.assert(calculator.getLoadCalculator() != null);
    System.assert(calculator.getCalculatorId('TestclassCalculatorCombinedMenu') != null);
    
    System.assert(calculator.getCalculatorCombinedMenuId() != null);
    System.assert(calculator.getCalculatorPITAId() != null);
    System.assert(calculator.getCalculatorIPPCooledId() != null);
    System.assert(calculator.getCalculatorIPPUncooledId() != null);
    
    
  }
}
