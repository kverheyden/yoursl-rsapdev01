/*
 * @(#)SCAddressValidation.cls 
 * 
 * Copyright 2010 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Base class for front-end related part of the address validation service. 
 * The class uses the actual back-end webservices classes to call for the 
 * address validation and geocoding service and offers methods to access
 * the results.
 *
 * Usage:
 *   SCAddressValidation validation = new SCAddressValidation();
 *   List<AvsAddress> results = validation.check('de', 
 *                                  'Schulstraße 10 33102 Paderborn');
 *
 *  The service SCAddressServiceImplGMS() is not supported any more
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
public virtual class SCAddressValidation 
{
    /**
     * Check the query directly and return the result set
     * 
     * @param country country to search in
     * @param query search for the address string
     * @return the validated addresses and status information 
     */
    public AvsResult check(String country, String query)
    {
        // use the info field for the complete query string (containing city, zipcode, ...)
        AvsAddress addr = new AvsAddress();
        addr.country = country;
        addr.city = query;
        // do the validation
        return execute(addr);
    }

    /**
     * Check the query directly and return the result set
     * 
     * @param Address object containing the query
     * @return the validated addresses and status information 
     */
    public AvsResult check(AvsAddress addr)
    {
        // do the validation
        return execute(addr);
    }


    /**
     * Determines the longitude and latitude for an address.
     * Only required if the external address validation service 
     * does not deliver the geocodes in the addres validateion.
     * 
     * @param addr address object with all needed data
     * @return the address with the longitude and latitude
     *         if successful the status is AvsResult.STATUS_EXACT
     *         if not successful the status is AvsResult.STATUS_BLANK
     */
    public AvsResult geocode(AvsAddress addr)
    {
        AvsResult result = new AvsResult();
        
        // Instantiate the geocoding implementation (web service access)
        SCAddressService service = getInstance(addr.country);
        if(service != null)
        {
            // the address object contains the data in separate fields
            result = service.geocode(addr);
        }
        else
        {   
            // return a warning message
            result = new AvsResult();
            result.status = AvsResult.STATUS_ERROR;
            result.statusInfo = 'no address validation service available for country: ' + addr.country;
        }
        return result;

    }


    
    /*
     * Executes the validation and calls the external web services
     * @param addr  the comlete address (country, postcode or city, street and house number)
     *              or an search string (country + info) info containing the postcode, city, street in one string
     */
    private AvsResult execute(AvsAddress addr)
    {
        AvsResult result;
        System.debug('\n....................addr.country: ' + addr.country); 
        // Instantiate the address validation implementation (web service access)
        SCAddressService service = getInstance(addr.country);
        if(service != null)
        {
            // the address object contains the data in separate fields
            result = service.check(addr);
        }
        else
        {   
            // return a warning message
            result = new AvsResult();
            result.status = AvsResult.STATUS_ERROR;
            result.statusInfo = 'no address validation service available for country: ' + addr.country;
        }
        return result;
    }

    /*
     * Instantiates the address validation implementation
     * @param   country - the country identifier
     * @return  An instance of the address validation component
     *          (default is the GMS address validation in test environments)
    */
    private SCAddressService getInstance(String country)
    {
        //PMS 36555/GMS: Google Implementierung aktivieren 
        // GMSNA: Bitte immer Google implementierung aktivieren. 
        /*if ((country == 'de') || (country == 'lu'))
        {
            // Test only: return new SCAddressServiceImplGMS();
            //internatinal address service 
            return new SCAddressServiceImplDE();
        }    
        else if (country == 'nl')
        {
            return new SCAddressServiceImplNL2();
        }
        else if(isQAS(country))
        {
            return new SCAddressServiceImplQAS();
        }*/
        // Default is Google Maps Integration
        System.debug('SCAddressValidation.getInstance info: using default address validation for: ' + country);
        return new SCAddressServiceImplDE();
    } // getInstance
    
    /**
    * determines on behalf of country whether the QAS service ought to be called.
    */
    boolean isQAS(String country)
    {
        boolean ret = false;
        if(country == null)
        {
            System.debug('\n.....................................................country is null');
        }
        if(country.equalsIgnoreCase('gb')
         || country.equalsIgnoreCase('uk')
         || country.equalsIgnoreCase('en') )
        {
            ret = true;
        } 
        return ret;
    }   
}
