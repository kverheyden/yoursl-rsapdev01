@isTest
private class cc_cceag_test_email_ctrl_Cart {
	
	@isTest static void test_method_one() {
		Account soldTo = new Account(Name='SoldTo', AccountNumber='12354');
        insert soldTo;
		ccrz__E_Cart__c cart = cc_cceag_test_TestUtils.createCart(soldTo, true);
		buildCartItems(cart.Id);
		Test.setCurrentPageReference(Page.cc_cceag_CartEMail);
		System.currentPageReference().getParameters().put('cartId', cart.Id);
		Test.startTest();
		cc_cceag_email_ctrl_Cart ctrl = new cc_cceag_email_ctrl_Cart();
		cc_cceag_bean_Cart cartBean = ctrl.currentCart;
		System.assert(cartBean != null);
		Test.stopTest();
	}
	
	@isTest static void test_method_two() {
		// Implement test code
	}

	private static void buildCartItems(Id cartId) {
		ccrz__E_Product__c p1 = new ccrz__E_Product__c(Name='Product1', ccrz__Sku__c='sku1', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12);
		ccrz__E_Product__c p2 = new ccrz__E_Product__c(Name='Product2', ccrz__Sku__c='sku2', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12);
		ccrz__E_Product__c p3 = new ccrz__E_Product__c(Name='Product3', ccrz__Sku__c='sku3', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12);
		ccrz__E_Product__c p4 = new ccrz__E_Product__c(Name='Product4', ccrz__Sku__c='sku4', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12);
		
		List<ccrz__E_Product__c> ps = new List<ccrz__E_Product__c>{
			p1,p2,p3,p4
		};
		insert ps;
		
		List<ccrz__E_CartItem__c> cartItems = new List<ccrz__E_CartItem__c> {
			new ccrz__E_CartItem__c(ccrz__Quantity__c=1, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku1'), ccrz__cart__c=cartId, ccrz__price__c=100, ccrz__SubAmount__c=100.0, ccrz__cartItemType__c='Major'),
			new ccrz__E_CartItem__c(ccrz__Quantity__c=1, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku2'), ccrz__cart__c=cartId, ccrz__price__c=100, ccrz__SubAmount__c=100.0, ccrz__cartItemType__c='Major'),
			new ccrz__E_CartItem__c(ccrz__Quantity__c=1, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku3'), ccrz__cart__c=cartId, ccrz__price__c=100, ccrz__SubAmount__c=100.0, ccrz__cartItemType__c='Major')
		};
		insert cartItems;
	}
	
}
