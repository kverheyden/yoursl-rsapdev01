/*
 * @(#)SCOrderProtocolControllerTest.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * @author Sebastian Schrage
 */
@isTest
private class SCOrderProtocolControllerTest
{
    static testMethod void CodeCoverageA()
    {
    	SCHelperTestClass3.createCustomSettings('DE',true);
        SCHelperTestClass3.appSettings.DISABLETRIGGER__c = true;
        SCHelperTestClass3.appSettings.DISABLETRIGGER_ORDERAFTERUPDATE__c = true;
        
        SCHelperTestClass.createTestUsers(true);
        //SCHelperTestClass.createTestResources(SCHelperTestClass.users, true);
        
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet3(true);
        
        SCHelperTestClass2.createTimeReports(SCHelperTestClass.resources, false);
        SCHelperTestClass.createArticle(true);
    	

        SCOrder__c order = SCHelperTestClass.order;
        
        //Database.insert(order);
        
        Database.insert(new List<SCOrderLog__c>{
                    new SCOrderLog__c(CreatedDate__c = DateTime.now(),
                                      Info__c        = 'Test 1',
                                      SCOrderId__c   = order.Id,
                                      SubType__c     = '5501',
                                      Type__c        = '11701',
                                      Username__c    = UserInfo.getUserName()),
                    new SCOrderLog__c(CreatedDate__c = DateTime.now(),
                                      Info__c        = 'Test 2',
                                      SCOrderId__c   = order.Id,
                                      SubType__c     = '5501',
                                      Type__c        = '11701',
                                      Username__c    = UserInfo.getUserName()),
                    new SCOrderLog__c(CreatedDate__c = DateTime.now(),
                                      Info__c        = 'Test 3',
                                      SCOrderId__c   = order.Id,
                                      SubType__c     = '5501',
                                      Type__c        = '11701',
                                      Username__c    = UserInfo.getUserName()),
                    new SCOrderLog__c(CreatedDate__c = DateTime.now(),
                                      Info__c        = 'Test 4',
                                      SCOrderId__c   = order.Id,
                                      SubType__c     = '5501',
                                      Type__c        = '11701',
                                      Username__c    = UserInfo.getUserName()),
                    new SCOrderLog__c(CreatedDate__c = DateTime.now(),
                                      Info__c        = 'Test 5',
                                      SCOrderId__c   = order.Id,
                                      SubType__c     = '5501',
                                      Type__c        = '11701',
                                      Username__c    = 'GMSTestA')
                });
        
        Note testNote = new Note(parentId = order.Id);
        testNote.Body = 'Test text';
        testNote.Title = 'Test title';
        insert testNote;
        
        Attachment att = new Attachment();
		att.ParentId = order.Id;
 	    att.Name = 'Test Attachment for Parent';  
    	att.Body = Blob.valueOf('Test Data');  
        insert att;  
        
        system.debug('##### test note: ' + testNote);
        
        order.Status__c = '5507';
        update order;
        
        Test.startTest();
        
        SCOrder__c orderAttachments = [Select Name,(Select Id, CreatedDate, CreatedById, LastModifiedDate, CreatedBy.Name From Attachments order by CreatedDate desc) From SCOrder__c s WHERE id = :order.id limit 1];
        
        system.debug('#### notes and attachments: ' + orderAttachments.getSObjects('Attachments'));    
        system.debug('#### notes and attachments: ' + orderAttachments.Attachments);    
        
        SCOrderProtocolController obj = new SCOrderProtocolController();
        obj.getNotePrefix();
        obj.oid = order.Id;
        
        SCOrderProtocolController.History h = new SCOrderProtocolController.History();
        h.createdDate = System.now();
        
        h.getValueOfDate();
        obj.getTranslation('testPrefix', 'testValue');
        
        Test.stopTest();
    }
}
