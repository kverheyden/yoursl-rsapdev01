/* Duc Nguyen Tine
 * This test is heavily relying on the configuration of org.
 * there must exists a Group SalesAppAdmins which is set to as an author for Sales Folder Library
 */
@isTest(seeAllData=true)
private class ContentAlertBatchTest {
	static testMethod void ContentAlertBatch_UnitTest() {
        //User u = [Select ID from User where Name = 'David Nguyentien'];
        User u = new User();
        u.FirstName ='First1';
        u.LastName = 'Tester';
        u.Alias = 'mtst';
        u.Email = 'mtester1@company.com';
        u.Username = u.Email;
        u.CommunityNickname = u.Alias;
        u.ProfileId = [select Id from Profile
                       where Name = 'System Administrator' OR Name = 'Systemadministrator'][0].Id;
        u.TimeZoneSidKey = 'America/Chicago';
        u.LocaleSidKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        u.LanguageLocaleKey = 'en_US';
        
        insert u;
        
        List<Group> groups = [Select ID from Group where DeveloperName = 'SalesAppAdmins'];
        system.assertEquals(1, groups.size(), 'Group SalesAppAdmins could not be found on this org. It gives access to publish contents to library.');
        
        GroupMember mem = new GroupMember();
        mem.GroupId = groups.get(0).id;
        mem.UserOrGroupId = u.iD;
        insert mem;
        
        System.runAs(u){
            List<ContentWorkspace> workspaces = [Select ID from ContentWorkspace where Name in  ('fSFA Vertriebsdokumente','Sales Folder')];
            system.assertEquals(1, workspaces.size());
        	ContentWorkspace cw = new ContentWorkspace();
            
        	RecordType rt = [Select ID from RecordType where SobjectType = 'ContentVersion' and DeveloperName = 'Sales_Folder'];
            
            String sDocName = 'PofS';
            Blob b = Blob.valueOf('Content');
            ContentVersion objContentVersion= new ContentVersion(Title=sDocName );
            objContentVersion.RecordTypeId = rt.ID;
        	objContentVersion.FirstPublishLocationId = workspaces.get(0).id;
            objContentVersion.VersionData = b;
            objContentVersion.PathOnClient = sDocName;
            objContentVersion.Valid_until__c = Date.today().addDays(14);  
            insert objContentVersion;
        
            ContentAlertBatch batch = new ContentAlertBatch();
            Database.executeBatch(batch);
        }
	}
}
