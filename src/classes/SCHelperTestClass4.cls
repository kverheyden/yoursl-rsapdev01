/*
 * @(#)SCHelperTestClass4.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This class is used for the creation of data for unit tests.
 * However it counts to the Apex limit since we can't set the @isTest
 * annotation which would need a private class!
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */

public class SCHelperTestClass4
{
    public static List<SCInstalledBase__c> installedBase { get; private set; }
    public static Account account { get; private set; }
    public static List<SCAccountRole__c> allAccountRole { get; private set; }
    
    public static SCArticle__c article { get; private set; }    
    public static SCInstalledBaseLocation__c location { get; private set; }
    
    private static SCApplicationSettings__c appSettings
    {
        get
        {
            if (appSettings == null)
            {
                try { SCHelperTestClass3.createCustomSettings('de',true); }
                catch (Exception e) { }
            }
            return appSettings;
        }
        private set;
    }
    
    static {
        installedBase = new List<SCInstalledBase__c>();
        allAccountRole = new List<SCAccountRole__c>();
    }
    
    /**
     * Create a new account for testing purposes using default values.
     */
    public static void createAccount() 
    {
        String priceListName = 'Standard';

        if (null == appSettings)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        System.debug('defaults -> ' + appSettings);

        if (appSettings.DEFAULT_PRICELIST__c <> null && 
            appSettings.DEFAULT_PRICELIST__c.length() > 0)
        {
            priceListName = appSettings.DEFAULT_PRICELIST__c;
        }
        
        SCPricelist__c priceList;
        
        try
        {
            priceList = [select Id, Name from SCPriceList__c where Name = :priceListName];
        }
        catch (Exception e)
        {
            priceList = new SCPriceList__c(Name = priceListName);
            insert priceList;
        }

        SCHelperTestClass.createAccountObject('Customer', true);
        account = SCHelperTestClass.account;

/*
        account = new Account(Name='Unit Test', 
                              Type = SCfwConstants.DOMVAL_ACCOUNT_TYPE_CUSTOMER,
                              DefaultPriceList__c = priceList.Id,
                              DefaultPriceList__r = priceList,
                              TaxType__c = SCfwConstants.DOMVAL_TAXACCOUNT_TAX,
                              BillingStreet = 'Karl-Schurz-Straße 29',
                              BillingPostalCode = '33100',
                              BillingCity = 'Paderborn',
                              BillingCountry__c = 'DE');
                              //ShippingStreet = 'Bahnhofstraße 15',
                              //ShippingPostalCode = '33100',
                              //ShippingCity = 'Paderborn',
                              //ShippingCountry__c = 'DE'
                              //);

        insert account;
*/
        allAccountRole.add(
            new SCAccountRole__c(MasterAccount__c = account.Id,
                                 SlaveAccount__c = account.Id,
                                 AccountRole__c = '50303'));
                                 
        allAccountRole.add(
            new SCAccountRole__c(MasterAccount__c = account.Id,
                                 SlaveAccount__c = account.Id,
                                 AccountRole__c = '50304'));
                                 
        
        insert allAccountRole;
    }
    
    /**
     * Create a new person account for testing purposes using default values.
     */
    public static Account createPersonAccount()
    {
        return createPersonAccount(true);
    }
    
    /**
     * Create a new person account for testing purposes using default values.
     */
    public static Account createPersonAccount(Boolean autoInsert) 
    {
        String priceListName = 'Standard';

        if (null == appSettings)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        if (appSettings.DEFAULT_PRICELIST__c <> null && 
            appSettings.DEFAULT_PRICELIST__c.length() > 0)
        {
            priceListName = appSettings.DEFAULT_PRICELIST__c;
        }
        
        SCPricelist__c priceList;
        
        try
        {
            priceList = [select Id, Name from SCPriceList__c where Name = :priceListName];
        }
        catch (Exception e)
        {
            priceList = new SCPriceList__c(Name = priceListName);
            
            if (autoInsert == true)
            {
                insert priceList;
            }
        }

        RecordType rt;
        
        try
        {
            rt = [select id,Name from RecordType 
                     where sobjecttype= 'Account'
                     limit 1];
        }
        catch (Exception e)
        {
            System.debug(LoggingLevel.ERROR, 'Unable to get a valid PersonType!');
        }

        Account pAccount = new Account(
                              FirstName__c='Person',
                              LastName__c='Unit Test', 
                              Type = SCfwConstants.DOMVAL_ACCOUNT_TYPE_CUSTOMER,
                              DefaultPriceList__r = priceList,
                              TaxType__c = SCfwConstants.DOMVAL_TAXACCOUNT_TAX,
                              BillingStreet = 'Bahnhofstraße 15',
                              BillingPostalCode = '33100',
                              BillingCity = 'Paderborn',
                              BillingCountry__c = 'DE',
                              //ShippingStreet = 'Bahnhofstraße 15',
                              //ShippingPostalCode = '33100',
                              //ShippingCity = 'Paderborn',
                              //ShippingCountry__c = 'DE',
                              RecordTypeId = rt.Id
                              );

        if (priceList.Id <> null) 
        {
            pAccount.DefaultPriceList__c = priceList.Id;
        }   

        if (autoInsert == true)
        {
            insert pAccount;
        }
        
        return pAccount;
        
    }
    
    
    /**
     * Create an article for testing purposes
     */
    public static void createArticle()
    {
        article = new SCArticle__c(
                                    Name = 'TEST-000251',
                                    ID2__c = '00-TEST-000251',
                                    Text_en__c = 'Unit Test Article',
                                    TaxType__c = SCfwConstants.DOMVAL_TAXARTICLE_FULL,
                                    Class__c = SCfwConstants.DOMVAL_ARTICLECLASS_PRODUCT,
                                    AvailabilityType__c = SCfwConstants.DOMVAL_MATERIALDISPO_A);

        insert article;
    }


    public static void createInstalledBaseLocation()
    {
        location = new SCInstalledBaseLocation__c(
                BuildingDate__c = (Date.today() - 1000),
                City__c = 'Paderborn',
                Country__c = 'DE',
                GeoX__c = 8.799476,
                GeoY__c = 51.726569,
                ID2__c = 'TEST-LOCATION',
                Status__c = 'Active',
                Street__c = 'Karl-Schurz-Straße 29'
            );
            
            insert location;
    
    }

    
    /**
     * Create a new installed base for a given account.
     */
    public static void createInstalledBase()
    {
        // create a new article if we don't already have one
        if (article == null)
        {
            createArticle();
        }
    
        article = [select Id,Name from SCArticle__c 
                    where Name = 'TEST-000251' limit 1];

        // create a new account if we don't already have one
        // to avoid nasty error messages
        if (account == null)
        {
            createAccount();
        }



        SCInstalledBase__c ib = new SCInstalledBase__c(
                                            Article__c = article.Id,
                                            Article__r = article,
                                            SerialNo__c = 'AFDSC1235-678',
                                            Status__c = 'active',
                                            InstalledBaseLocation__c = location.Id,
                                            InstalledBaseLocation__r = location,
                                            Type__c = 'Appliance',
                                            ProductUnitClass__c = SCfwConstants.DOMVAL_PRODUCTUNITCLASS_STANDARD,
                                            ProductUnitType__c = SCfwConstants.DOMVAL_PRODUCTUNITTYPE_DEFAULT
                                    );
        installedBase.add(ib);
        
        insert installedBase;
    }
    
    /**
     * Create a tax record for testing purposes
     */
    public static void createTaxRecord()
    {
        List<SCConditionTax__c> taxes = new List<SCConditionTax__c>();

        taxes.add(new SCConditionTax__c(
                Country__c = 'NL',
                TaxRate__c = 7.0,
                TaxAccount__c = SCfwConstants.DOMVAL_TAXACCOUNT_TAX,
                TaxArticle__c = SCfwConstants.DOMVAL_TAXARTICLE_FULL,
                ValidFrom__c = Date.today()
            ));

        taxes.add(new SCConditionTax__c(
                Country__c = 'NL',
                TaxRate__c = 3.0,
                TaxAccount__c = SCfwConstants.DOMVAL_TAXACCOUNT_TAX,
                TaxArticle__c = SCfwConstants.DOMVAL_TAXARTICLE_HALF,
                ValidFrom__c = Date.today()
            ));
        insert taxes;
    }

    /**
     * createTestArticles
     * ==================
     *
     * Create articles for testing.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static List<SCArticle__c> createTestArticles()
    {
        List<SCArticle__c> articles = new List<SCArticle__c>();

        SCArticle__c article1 = new SCArticle__c(Name = 'A-{0000000001}',
                              Text_en__c = 'Test article 1',
                              EANCode__c = 'EAN-001', 
                              Class__c = SCfwConstants.DOMVAL_ARTICLECLASS_SPAREPART,
                              Type__c = SCfwConstants.DOMVAL_ARTICLETYP_NO_TYP,
                              Unit__c = SCfwConstants.DOMVAL_UNIT_PIECE,
                              AvailabilityType__c = SCfwConstants.DOMVAL_MATERIALDISPO_A,
                              TaxType__c = SCfwConstants.DOMVAL_TAXARTICLE_FULL,
                              PriceChangesEnabled__c = true,
                              ReturnType__c = '13901',
                              LockType__c = '50201' 
                             );
        insert article1;
        System.debug('#### createTestArticles(): article1.Id -> ' + article1.Id);
        articles.add(article1);

        articles.add(new SCArticle__c(Name = 'A-{0000000002}',
                              Text_en__c = 'Test article 2',
                              EANCode__c = 'EAN-002', 
                              Class__c = SCfwConstants.DOMVAL_ARTICLECLASS_SPAREPART,
                              Type__c = SCfwConstants.DOMVAL_ARTICLETYP_NO_TYP,
                              Unit__c = SCfwConstants.DOMVAL_UNIT_PIECE,
                              AvailabilityType__c = SCfwConstants.DOMVAL_MATERIALDISPO_A,
                              TaxType__c = SCfwConstants.DOMVAL_TAXARTICLE_FULL,
                              PriceChangesEnabled__c = true,
                              ReturnType__c = '13901', 
                              ReplacedBy__c = article1.Id,
                              LockType__c = '50201'  
                             ));

        articles.add(new SCArticle__c(Name = 'A-{0000000003}',
                              Text_en__c = 'Test article 3',
                              EANCode__c = 'EAN-003', 
                              Class__c = SCfwConstants.DOMVAL_ARTICLECLASS_SPAREPART,
                              Type__c = SCfwConstants.DOMVAL_ARTICLETYP_NO_TYP,
                              Unit__c = SCfwConstants.DOMVAL_UNIT_PIECE,
                              AvailabilityType__c = SCfwConstants.DOMVAL_MATERIALDISPO_A,
                              TaxType__c = SCfwConstants.DOMVAL_TAXARTICLE_FULL,
                              PriceChangesEnabled__c = true,
                              ReturnType__c = '13901', 
                              ReplacedBy__c = article1.Id,
                              LockType__c = '50201'  
                             ));

        articles.add(new SCArticle__c(Name = 'A-{0000000004}',
                              Text_en__c = 'Test article 4',
                              EANCode__c = 'EAN-004', 
                              Class__c = SCfwConstants.DOMVAL_ARTICLECLASS_SPAREPART,
                              Type__c = SCfwConstants.DOMVAL_ARTICLETYP_NO_TYP,
                              Unit__c = SCfwConstants.DOMVAL_UNIT_PIECE,
                              AvailabilityType__c = SCfwConstants.DOMVAL_MATERIALDISPO_A,
                              TaxType__c = SCfwConstants.DOMVAL_TAXARTICLE_FULL,
                              PriceChangesEnabled__c = true,
                              ReturnType__c = '13901',
                              LockType__c = '50201'  
                             ));

        articles.add(new SCArticle__c(Name = 'A-{0000000005}',
                              Text_en__c = 'Test article 5',
                              EANCode__c = 'EAN-005', 
                              Class__c = SCfwConstants.DOMVAL_ARTICLECLASS_SPAREPART,
                              Type__c = SCfwConstants.DOMVAL_ARTICLETYP_NO_TYP,
                              Unit__c = SCfwConstants.DOMVAL_UNIT_PIECE,
                              AvailabilityType__c = SCfwConstants.DOMVAL_MATERIALDISPO_A,
                              TaxType__c = SCfwConstants.DOMVAL_TAXARTICLE_FULL,
                              PriceChangesEnabled__c = true,
                              ReturnType__c = '13901',
                              LockType__c = '50201'  
                             ));

        articles.add(new SCArticle__c(Name = 'A-{0000000006}',
                              Text_en__c = 'Test article 6',
                              EANCode__c = 'EAN-006', 
                              Class__c = SCfwConstants.DOMVAL_ARTICLECLASS_SPAREPART,
                              Type__c = SCfwConstants.DOMVAL_ARTICLETYP_NO_TYP,
                              Unit__c = SCfwConstants.DOMVAL_UNIT_PIECE,
                              AvailabilityType__c = SCfwConstants.DOMVAL_MATERIALDISPO_A,
                              TaxType__c = SCfwConstants.DOMVAL_TAXARTICLE_FULL,
                              PriceChangesEnabled__c = true,
                              ReturnType__c = '13901',
                              LockType__c = '50201'  
                             ));

        articles.add(new SCArticle__c(Name = 'A-{0000000007}',
                              Text_en__c = 'Test article 7',
                              EANCode__c = 'EAN-007', 
                              Class__c = SCfwConstants.DOMVAL_ARTICLECLASS_SPAREPART,
                              Type__c = SCfwConstants.DOMVAL_ARTICLETYP_NO_TYP,
                              Unit__c = SCfwConstants.DOMVAL_UNIT_PIECE,
                              AvailabilityType__c = SCfwConstants.DOMVAL_MATERIALDISPO_A,
                              TaxType__c = SCfwConstants.DOMVAL_TAXARTICLE_FULL,
                              PriceChangesEnabled__c = true,
                              ReturnType__c = '13901',
                              LockType__c = '50201'  
                             ));
        upsert articles;
        return articles;
    } // createTestArticles

    /**
     * createTestPlant
     * ===============
     *
     * Create a plant for testing.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static Id createTestPlant()
    {
        SCPlant__c plant = new SCPlant__c(Name = '9100', info__c = 'Test plant');
        insert plant;
        return plant.Id;
    } // createTestPlant

    /**
     * createTestStocks
     * ================
     *
     * Create stocks for testing.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static List<SCStock__c> createTestStocks(Id plantId)
    {
        List<SCStock__c> stocks = new List<SCStock__c>();

        stocks.add(new SCStock__c(Name = '9101', info__c = 'Test stock 1', Plant__c = plantId));
        stocks.add(new SCStock__c(Name = '9102', info__c = 'Test stock 2', Plant__c = plantId));
        stocks.add(new SCStock__c(Name = '9103', info__c = 'Test stock 3', Plant__c = plantId));
        stocks.add(new SCStock__c(Name = '9104', info__c = 'Test stock 4', Plant__c = plantId));
        stocks.add(new SCStock__c(Name = '9105', info__c = 'Test stock 5', Plant__c = plantId));
        stocks.add(new SCStock__c(Name = '9106', info__c = 'Test stock 6', Plant__c = plantId));

        insert stocks;
        return stocks;
    } // createTestStocks

    /**
     * createTestStockOrigins
     * ======================
     *
     * Create stocks for testing.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static List<SCStockOrigin__c> createTestStockOrigins(List<SCStock__c> stockList)
    {
        List<SCStockOrigin__c> origins = new List<SCStockOrigin__c>();

        Integer i = 1;
        for (SCStock__c stock :stockList)
        {
            origins.add(new SCStockOrigin__c(Stock__c = stock.Id, 
                                             Description__c = 'Stock Origin ' + i, 
                                             Sort__c = 999 - stockList.size() + i, 
                                             Default__c = (1 == i)));
            i++;
        } // for (SCStock__c stock :stockList)

        insert origins;
        return origins;
    } // createTestStockOrigins

    /**
     * createTestStockItem
     * ===================
     *
     * Create stock items for testing.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static SCStockItem__c createTestStockItem(Id stockId, Id articleId)
    {
        SCStockItem__c item = new SCStockItem__c(Stock__c = stockId, 
                                                 Article__c = articleId,
                                                 Qty__c = 0.0);
        insert item;
        return item;
    } // createTestStockItems

    /**
     * createTestStockItems
     * ====================
     *
     * Create stock items for testing the article import
     * in stock profiles.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static List<SCStockItem__c> createTestStockItems(List<SCArticle__c> articleList, 
                                                            Id stockId1, Id stockId2)
    {
        List<SCStockItem__c> items = new List<SCStockItem__c>();

        items.add(new SCStockItem__c(Stock__c = stockId1, 
                                     Article__c = articleList.get(0).Id,
                                     Qty__c = 0.0, 
                                     MinQty__c = 0.0, MaxQty__c = 0.0, 
                                     ReplenishmentQty__c = 0.0));
        items.add(new SCStockItem__c(Stock__c = stockId1, 
                                     Article__c = articleList.get(1).Id,
                                     Qty__c = 0.0));
        items.add(new SCStockItem__c(Stock__c = stockId2, 
                                     Article__c = articleList.get(2).ID,
                                     Qty__c = 0.0, 
                                     MinQty__c = 0.0, MaxQty__c = 0.0, 
                                     ReplenishmentQty__c = 0.0));
        items.add(new SCStockItem__c(Stock__c = stockId2, 
                                     Article__c = articleList.get(3).Id,
                                     Qty__c = 0.0));

        insert items;
        return items;
    } // createTestStockItems

    /**
     * createTestStockItemsForReplenishment
     * ====================================
     *
     * Create stock items for testing the article import
     * in stock profiles.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static List<SCStockItem__c> createTestStockItemsForReplenishment(
                                                            List<SCArticle__c> articleList, 
                                                            List<SCStock__c> stockList)
    {
        List<SCStockItem__c> items = new List<SCStockItem__c>();

        items.add(new SCStockItem__c(Stock__c = stockList.get(0).Id, 
                                     Article__c = articleList.get(0).Id,
                                     Qty__c = 1.0, 
                                     MinQty__c = 2.0, MaxQty__c = 4.0, 
                                     ReplenishmentQty__c = 1.0));
        items.add(new SCStockItem__c(Stock__c = stockList.get(0).Id, 
                                     Article__c = articleList.get(1).Id,
                                     Qty__c = 1.0, 
                                     MinQty__c = 2.0, MaxQty__c = 3.0, 
                                     ReplenishmentQty__c = 1.0));
        items.add(new SCStockItem__c(Stock__c = stockList.get(0).Id, 
                                     Article__c = articleList.get(2).Id,
                                     Qty__c = 1.0, 
                                     MinQty__c = 3.0, MaxQty__c = 5.0, 
                                     ReplenishmentQty__c = 2.0));

        items.add(new SCStockItem__c(Stock__c = stockList.get(1).Id, 
                                     Article__c = articleList.get(3).Id,
                                     Qty__c = 1.0, 
                                     MinQty__c = 2.0, MaxQty__c = 3.0, 
                                     ReplenishmentQty__c = 1.0));
        items.add(new SCStockItem__c(Stock__c = stockList.get(1).Id, 
                                     Article__c = articleList.get(4).Id,
                                     Qty__c = 0.0, 
                                     MinQty__c = 1.0, MaxQty__c = 2.0, 
                                     ReplenishmentQty__c = 1.0));
        items.add(new SCStockItem__c(Stock__c = stockList.get(1).Id, 
                                     Article__c = articleList.get(5).Id,
                                     Qty__c = 0.0, 
                                     MinQty__c = 3.0, MaxQty__c = 5.0, 
                                     ReplenishmentQty__c = 2.0));

        insert items;
        return items;
    } // createTestStockItemsForReplenishment

    /**
     * createTestStockProfiles
     * =======================
     *
     * Create stock profiles for testing.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static List<SCStockProfile__c> createTestStockProfiles()
    {
        List<SCStockProfile__c> profiles = new List<SCStockProfile__c>();

        profiles.add(new SCStockProfile__c(Name = 'STP_T001', 
                                           info__c = 'Test stock profile1'));
        profiles.add(new SCStockProfile__c(Name = 'STP_T002', 
                                           info__c = 'Test stock profile2'));

        insert profiles;
        return profiles;
    } // createTestStockProfiles

    /**
     * createTestStockProfileItems
     * ===========================
     *
     * Create stock profile items for testing.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static List<SCStockProfileItem__c> createTestStockProfileItems(List<SCArticle__c> articleList, 
                                                                          Id profileId)
    {
        List<SCStockProfileItem__c> items = new List<SCStockProfileItem__c>();

        items.add(new SCStockProfileItem__c(StockProfile__c = profileId, 
                                            Article__c = articleList.get(0).Id, 
                                            MinQty__c = 3.0, MaxQty__c = 5.0, 
                                            ReplenishmentQty__c = 3.0));
        items.add(new SCStockProfileItem__c(StockProfile__c = profileId, 
                                            Article__c = articleList.get(1).Id, 
                                            MinQty__c = 3.0, MaxQty__c = 6.0, 
                                            ReplenishmentQty__c = 3.0));
        items.add(new SCStockProfileItem__c(StockProfile__c = profileId, 
                                            Article__c = articleList.get(2).Id));
        items.add(new SCStockProfileItem__c(StockProfile__c = profileId, 
                                            Article__c = articleList.get(4).Id, 
                                            MinQty__c = 1.0, MaxQty__c = 3.0, 
                                            ReplenishmentQty__c = 1.0));
        items.add(new SCStockProfileItem__c(StockProfile__c = profileId, 
                                            Article__c = articleList.get(5).Id));
                                                               
        insert items;
        return items;
    } // createTestStockProfileItems

    /**
     * createTestStockProfileAssocs
     * ============================
     *
     * Create stock profile assocs for testing.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static List<SCStockProfileAssoc__c> createTestStockProfileAssocs(Id stockId1, 
                                                                            Id stockId2, 
                                                                            Id profileId1,
                                                                            Id profileId2)
    {
        List<SCStockProfileAssoc__c> assocs = new List<SCStockProfileAssoc__c>();

        assocs.add(new SCStockProfileAssoc__c(Stock__c = stockId1, StockProfile__c = profileId1));
        assocs.add(new SCStockProfileAssoc__c(Stock__c = stockId2, StockProfile__c = profileId1));
        assocs.add(new SCStockProfileAssoc__c(Stock__c = stockId1, StockProfile__c = profileId2));

        insert assocs;
        return assocs;
    } // createTestStockProfileAssocs

    /**
     * createTestUsers
     * ===============
     *
     * Create users.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static List<User> createTestUsers()
    {
        Profile pro = [select Id from Profile where Name in ('Systemadministrator','System Administrator') limit 1];
        
        List<User> users = new List<User>();

        users.add(new User(Alias = 'GMSTest1', 
                           FirstName = 'GMS', LastName = 'Testuser 1', 
                           Username = 'gmstest1@gms-online.de', 
                           Email = 'gmstest1@gms-online.de', 
                           CommunityNickname = 'gmstest1', 
                           Street = 'Karl Schurz Str.',
                           PostalCode = '33100',
                           City = 'Paderborn',
                           Country = 'DE',
                           GEOX__c = -0.10345,
                           GEOY__c = 51.49250,
                           TimeZoneSidKey = 'Europe/Berlin', 
                           LocaleSidKey = 'de_DE_EURO', 
                           EmailEncodingKey = 'UTF-8', ProfileId = pro.Id, 
                           LanguageLocaleKey = 'en_US'));
        users.add(new User(Alias = 'GMSTest2', 
                           FirstName = 'GMS', LastName = 'Testuser 2', 
                           Username = 'gmstest2@gms-online.de', 
                           Email = 'gmstest2@gms-online.de', 
                           CommunityNickname = 'gmstest2', 
                           Street = 'Karl Schurz Str.',
                           PostalCode = '33100',
                           City = 'Paderborn',
                           Country = 'DE',
                           GEOX__c = -0.10345,
                           GEOY__c = 51.49250,
                           TimeZoneSidKey = 'Europe/Berlin', 
                           LocaleSidKey = 'de_DE_EURO', 
                           EmailEncodingKey = 'UTF-8', ProfileId = pro.Id, 
                           LanguageLocaleKey = 'en_US'));
        users.add(new User(Alias = 'GMSTest3', 
                           FirstName = 'GMS', LastName = 'Testuser 3', 
                           Username = 'gmstest3@gms-online.de', 
                           Email = 'gmstest3@gms-online.de', 
                           CommunityNickname = 'gmstest3', 
                           Street = 'Karl Schurz Str.',
                           PostalCode = '33100',
                           City = 'Paderborn',
                           Country = 'DE',
                           GEOX__c = -0.10345,
                           GEOY__c = 51.49250,
                           TimeZoneSidKey = 'Europe/Berlin', 
                           LocaleSidKey = 'de_DE_EURO', 
                           EmailEncodingKey = 'UTF-8', ProfileId = pro.Id, 
                           LanguageLocaleKey = 'en_US'));

        insert users;
        return users;
    } // createTestUsers

    /**
     * createTestResources
     * ===================
     *
     * Create resources for the given users.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static List<SCResource__c> createTestResources(List<User> userList)
    {
        return createTestResources(userList, null, null);
    }
    
    public static List<SCResource__c> createTestResources(List<User> userList, Id calId, Id buId)
    {
        List<SCResource__c> resources = new List<SCResource__c>();

        for (User user :userList)
        {
            resources.add(new SCResource__c(Employee__c = user.Id,
                                            Employee__r = user,
                                            alias_txt__c = user.Alias, 
                                            FirstName_txt__c = user.FirstName, 
                                            LastName_txt__c = user.LastName, 
                                            //Scheduleable__c = true,
                                            EnableScheduling__c = true,
                                            Type__c = '2601',
                                            Description__c = user.FirstName + user.LastName, 
                                            DefaultCalendar__c = calId, 
                                            DefaultDepartment__c = buId, 
                                            TimeZoneSidKey__c = user.TimeZoneSidKey));
        } // for (User user :userList)

        insert resources;
        return resources;
    } // createTestResources

    /**
     * createTestCalendar
     * ==================
     *
     * Create a test calendar.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static Id createTestCalendar()
    {
        SCCalendar__c cal = new SCCalendar__c(Name = 'GMS Test Cal');
        insert cal;
        return cal.Id;
    } // createTestCalendar

    /**
     * createTestBusinessUnit
     * ======================
     *
     * Create a business unit.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static Id createTestBusinessUnit(Id stockId, Id calId)
    {
        SCBusinessUnit__c unit = new SCBusinessUnit__c(Name = 'GMS Test Dev', 
                                                       info__c = 'GMS test Dev', 
                                                       Stock__c = stockID, 
                                                       Calendar__c = calId, 
                                                       Type__c = '5901'
                                                      );
        insert unit;
        return unit.Id;
    } // createTestBusinessUnit

    /**
     * createTestWorkTimeProfile
     * =========================
     *
     * Create a test work time profile.
     *
     * @param doUpsert upsert to DB if <code>true</code>
     */
    public static Id createTestWorkTimeProfile()
    {
        SCWorkTimeProfile__c workTimeProfile = new SCWorkTimeProfile__c(Name = 'GMS Test Work Time Profile', 
                                                   Description__c = 'GMS Test Work Time Profile', 
                                                   Weeks__c = 2);
        upsert workTimeProfile;
        return workTimeProfile.ID;
    } // createTestWorkTimeProfile

    /**
     * createTestAssignments
     * =====================
     *
     * Create resource assignments for the given resources.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static List<SCResourceAssignment__c> createTestAssignments(List<SCResource__c> resoureList, 
                                                                      List<SCStock__c> stockList, 
                                                                      Id unitId)
    {
        Id wtID = createTestWorkTimeProfile();
        List<SCResourceAssignment__c> assignments = new List<SCResourceAssignment__c>();

        Date now = Date.today();
        Integer i = 0;
        for (SCResource__c resource :resoureList)
        {
            Id stockId = (stockList.size() > i) ? stockList.get(i).Id : null;
            assignments.add(new SCResourceAssignment__c(Resource__c = resource.Id, 
                                                        Stock__c = stockId, 
                                                        Department__c = unitId, 
                                                        ValidFrom__C = now, 
                                                        ValidTo__c = now.addDays(7), 
                                                        Calendar__c = resource.DefaultCalendar__c, 
                                                        WorktimeProfile__c = wtID, 
                                                        Street__c = resource.Employee__r.Street, 
                                                        HouseNo__c = '1', // default value, because user object does not have this field
                                                        PostalCode__c = resource.Employee__r.PostalCode, 
                                                        City__c = resource.Employee__r.City, 
                                                        GeoY__c = resource.Employee__r.GeoX__c, 
                                                        GeoX__c = resource.Employee__r.GeoY__c, 
                                                        Country__c = resource.Employee__r.Country));
            i++;
        } // for (SCResource__c resource :resoureList)

        insert assignments;
        return assignments;
    } // createTestAssignments
    
    
    
    /**
     * Create an minimally filled order object
     *
     */
    public static SCOrder__c createOrder()
    {
        return createOrder(false);
    }
    
    /**
     * Create an minimally filled order object
     *
     */
    public static SCOrder__c createOrder(Boolean autoInsert)
    {
        if (null == appSettings)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        
        // At least an empty order should be there
        SCOrder__c order = new SCOrder__c();
        
        // set default values that are global
        order.InvoicingStatus__c = SCfwConstants.DOMVAL_INVOICINGSTATUS_OPEN;
        order.Status__c = SCfwConstants.DOMVAL_ORDERSTATUS_OPEN;

        // set default values that depend on different levels
        order.InvoicingType__c = appSettings.DEFAULT_INVOICINGTYPE__c;
        order.InvoicingSubType__c = appSettings.DEFAULT_INVOICINGSUBTYPE__c;
        order.PaymentType__c = appSettings.DEFAULT_PAYMENTTYPE__c;
        order.Type__c = appSettings.DEFAULT_ORDERTYPE__c;
        
        if (autoInsert == true)
        {
            insert order;
        }
        
        return order;           
    }
     
    /**
     * Create an order role object w/o a corresponding account
     *
     * @param order Order that this role belongs to
     * @param role Role for this order
     * @param autoInsert insert the record if <code>true</false>
     */
    public static SCOrderRole__c orderRolePerson(SCOrder__c order, 
                                                 String role, 
                                                 Boolean autoInsert)
    {  
        SCOrderRole__c orderRole = new SCOrderRole__c
             (
                 Order__c = order.Id,
                 OrderRole__c = role,
                 Account__r = account,
                 AccountNumber__c = '123456',
                 City__c = 'Leopoldshöhe',
                 Country__c = 'DE',
                 HouseNo__c = '15',
                 PostalCode__c = '33818',
                 Street__c = 'Südstrasse',
                 LocaleSidKey__c = 'de_DE',
                 GeoY__c = 52.036588,
                 GeoX__c = 8.693304,
                 Salutation__c = 'Herr',
                 Name = 'Person Test User'
            );


            if (autoInsert == true)
            {
                insert orderRole;
            }
            
            return orderRole;
    }
}
