/*
 * @(#)SCContractOrderSchedule.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
global with sharing class SCbtcContractOrderSchedule extends SCbtcBase
       implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts
{
    private ID batchprocessid = null;
    public ID getBatchProcessId()
    {
        return batchprocessid;
    }
    private String mode = 'productive';
    String country = 'DE';
    String department = null;

    Integer max_cycles = 0;    
    List<String> visitIdList = null;    // visitIdList = null, normal batch call
                                        // visitIdList != null, orders are scheduled only for the list
                                        // of visits    

    // Object for application settings
    private static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();

    /**
     * peramenente Schedule Engine Session ID used only by Appex Batch for 
     * dispatching orders
     */
    private String scheduleEngineSessionID = 'gwin-3';

    /**
     * The length of the interval beginning from today for dispatching the orders
     */ 
    private Integer schedulingDays = 7;
    
    /**
     * if the order id is null the batch job for all scheduled contract visit dates  is started
     * if the order id is not null the batch job for the given the scheduled contract visit date is started
     */
    private Id orderId = null;

    /**
     * Entry point for starting the apex batch dispatching all orders having the 
     * SCContractVisit__c.Status__c = 'schedule'
     * @param mode could be 'trace', 'test' or 'productive'
     */
    public static ID scheduleAll(String country, Integer max_cycles, String mode)
    {
        String department =  null;
        SCbtcContractOrderSchedule btc = new SCbtcContractOrderSchedule(country, department, max_cycles, mode);
        btc.batchprocessid = btc.executeBatch(1);
        return btc.batchprocessid;
    } // makeOrderAppointments

    /**
     * Entry point for starting the apex batch dispatching all orders having the 
     * SCContractVisit__c.Status__c = 'schedule'
     * @param mode could be 'trace', 'test' or 'productive'
     */
    public static ID scheduleDepartmentAll(String country, String department, Integer max_cycles, String mode)
    {
        SCbtcContractOrderSchedule btc = new SCbtcContractOrderSchedule(country, department, max_cycles, mode);
        btc.batchprocessid = btc.executeBatch(1);
        return btc.batchprocessid;
    } // makeOrderAppointments


    /**
     * Determine whether the orders can be scheduled
     * @return "OK" if visits can be created
     *         else an error string 
     */
    Webservice static String canSchedule(List<String> visitIdList)
    {
        String country = 'DE';
        if(visitIdList != null && visitIdList.size() > 0)
        {
            Id contractId = [Select Contract__r.Id from SCContractVisit__c where id = :visitIdList[0]].Contract__r.Id;
            if(contractId != null)
            {
                SCContract__c c = [Select Country__c from SCContract__c where Id = :contractId];
                country = c.Country__c;
            }
            System.debug('###......contractId: ' + contractId);
            List<RecordType> recordTypeIdList = [Select Id from RecordType where DeveloperName in ('Contract', 'Insurance') and SobjectType = :(SCfwConstants.NamespacePrefix + 'SCContract__c')];
            System.debug('recordTypeIdList: ' + recordTypeIdList);
            System.debug('contractId: ' + contractId);
            List<SCContractVisit__c> contractVisitList = [select Id
                        from SCContractVisit__c
                        where  
                        Contract__r.RecordTypeId in :recordTypeIdList
                        and id in :visitIdList 
                        and Contract__r.Id = :contractID
                        and Contract__r.Country__c = :country
                        and Order__c <> null and Status__c = 'schedule'
                        and (Contract__r.Status__c = 'Created' 
                        or Contract__r.Status__c = 'Active' 
                        or Contract__r.Status__c = 'Suspended')];
            if(contractVisitList.size() > 0)
            {
                System.debug('###....OK');
                return 'OK';
            }
        }   
        System.debug('###.....msg: ' + System.Label.SC_msg_ContractScheduleOrder);
        return System.Label.SC_msg_ContractScheduleOrder;       
   } // canDispatch  


    /**
     * Synchronically calls the scheduling of the orders for the list of contract visits.
     * Orders are created if following condition von contract visits are satisfied
     * id in :visitIdList  
     * and Order__c <> null and Status__c = 'schedule'
     *
     */ 
    Webservice static void syncSchedule(List<String> visitIdList)
    {
        syncScheduleEx(visitIdList, null);
    }
    Webservice static List<String> syncScheduleEx(List<String> visitIdList, id reqEmplId)
    {
        System.debug('###......visitIdList: ' + visitIdList);
        System.debug('###......reqEmplId: ' + reqEmplId);
        List<String> retMsgs = new List<String>();
        if(visitIdList == null || visitIdList.size() == 0)
        {
            return null;
        }   
        Id contractId = [Select Contract__r.Id from SCContractVisit__c where id = :visitIdList[0]].Contract__r.Id;
        System.debug('###......contractId: ' + contractId);
        String withoutMeaningCountry = 'DE';
        Integer max_cycles = 1;
        String department = null;
        SCbtcContractOrderSchedule btc = new SCbtcContractOrderSchedule(contractId, withoutMeaningCountry, department, max_cycles, 'test');        

        List<SCContractVisit__c> visitList = [select ID, Contract__c, Order__c, Order__r.Name, 
                                            Order__r.Id, DueDate__c
                                            from SCContractVisit__c 
                                            where
                                            id in :visitIdList  
                                            and Order__c <> null and Status__c = 'schedule'
                                            order by StartDate__c, EndDate__c];
        boolean force = true;  
        // forcing for the scheduling.
        // without checking whether the due day is in between the scope 
        // today and today + scheduling days                                          
        List<SCContractVisit__c> visits = new List<SCContractVisit__c>();
        String errMsg = null;
        for(SCContractVisit__c cv: visitList)
        {
            // create an order tree
            btc.debug('contract id: ' + contractId + ', contract visit id: ' + cv.Id);
            errMsg = btc.dispatchOrder(cv, reqEmplId, visits, force);
            if (null != errMsg)
            {
                retMsgs.add(cv.Order__r.Name + ': ' + errMsg);
            }
        }

        for (SCContractVisit__c visit :visits)
        {
            visit.Status__c = 'scheduled';
        }
        update visits;
        
        return (retMsgs.size() > 0) ? retMsgs: null;
    }

    Webservice static ID asyncSchedule(List<String> visitIdList)
    {
        if(visitIdList == null || visitIdList.size() == 0)
        {
            return null;
        }   
        SCbtcContractOrderSchedule btc = new SCbtcContractOrderSchedule(visitIdList, 'trace');
        btc.batchprocessid = btc.executeBatch(1);
        return btc.batchprocessid;
    }

    /**
     * Entry point for starting the apex batch dispatching an order having the 
     * SCContractVisit__c.Status__c = 'schedule' and SCContractVisit__c.Order__c = :orderId
     * @param orderId
     * @param mode could be 'test' or 'productive'
     */
    public static ID makeOrderAppointments(Id orderId, String country, Integer max_cycles, String mode)
    {
        String department = null;
        SCbtcContractOrderSchedule btc = new SCbtcContractOrderSchedule(orderId, country, department, max_cycles, mode);
        btc.batchprocessid = btc.executeBatch(1);
        return btc.batchprocessid;
    } // makeOrderAppointments

    /**
     * Entry point for starting the apex batch dispatching an order having the 
     * SCContractVisit__c.Status__c = 'schedule' and SCContractVisit__c.Order__c.Name = :orderName
     * @param orderName
     * @param mode could be 'test' or 'productive'
     */
    public static ID makeOrderAppointments(String orderName, String country, Integer max_cycles, String mode)
    {
        Id orderId = null;
        System.debug('###mode: ' + mode);
        System.debug('###orderName: ' + orderName);
        if(orderName != null)
        {
            List<SCOrder__c> orderList = [Select Id from SCOrder__c where Name = :orderName];
            if(orderList.size() > 0)
            {
                orderId = orderList[0].Id;
                System.debug('###orderId: ' + orderId);
                String department = null;
                SCbtcContractOrderSchedule btc = new SCbtcContractOrderSchedule(orderId, country, department, max_cycles, mode);
                btc.batchprocessid = btc.executeBatch(1);
                return btc.batchprocessid;
            }
        }
        return null;
    } // makeOrderAppointments

    /**
     * constructor for scheduling only the list of contract visits
     */
    public SCbtcContractOrderSchedule(List<String> visitIdList, String mode)
    {
        this(null, 'XX', null, 0, mode);
        this.visitIdList = visitIdList;
    }


    /**
     * Constructor
     * @param mode could be 'test' or 'productive'
     */
    public SCbtcContractOrderSchedule(String country, Integer max_cycles, String mode)
    {
        this(null, country, null, max_cycles, mode);
    }

    /**
     * Constructor
     * @param mode could be 'test' or 'productive'
     */
    public SCbtcContractOrderSchedule(String country, String department, Integer max_cycles, String mode)
    {
        this(null, country, department, max_cycles, mode);
    }

    /**
     * Constructor
     * @param orderID
     * @param mode could be 'test' or 'productive'
     */
    public SCbtcContractOrderSchedule(Id orderId, String country, String department, Integer max_cycles, String mode)
    {
        this.orderId = orderId;
        this.country = country;
        this.department = department;
        this.max_cycles = max_cycles;
        this.mode = mode;
        Decimal schedulingDaysDec = appSettings.CONTRACT_SCHEDULING_DAYS__c;
        if(schedulingDaysDec  != null)
        {
            schedulingDays = appSettings.CONTRACT_SCHEDULING_DAYS__c.intValue();
        }
        else
        {
            debug('could not find CONTRACT_SCHEDULING_DAYS__c !');
        }
        debug('scheduling days: ' + schedulingDays); 
    }

   /*
    * Called by the framework when the batch job has been completed. 
    * We send an e-mail notification about the status
    * @param BC the batch context
    */
    global override void finish(Database.BatchableContext BC)
    {
    } // finish

    /**
     * Dispatches an order SCContractVisit__c.Order__c
     */
    private void dispatchOrder(SCContractVisit__c cv, Boolean force)
    {
        dispatchOrder(cv, null, null, force);
    }
    private String dispatchOrder(SCContractVisit__c cv, Id reqEmplId, List<SCContractVisit__c> visits, Boolean force)
    {
        debug('dispatchOrder');
        debug('contractVisit: ' + cv);
        String errMsg = '';
        String retMsg = null;
        SCboOrder boOrder = null;
        debug('orderID: ' + cv.Order__r.Id);
        
        DateTime start = Datetime.now();
        Boolean thrownException = false;
        String resultInfo = '';
        Integer count = 0;
        String step = '';
        SCOrder__c order = null;
        try
        {
            step = 'Check whether the due day is in scope';
            if(isInScope(cv) || force)
            {
                step = 'Read order.';
                debug(step);
                order = [select id, (Select id, InstalledBase__r.InstalledBaseLocation__r.GeoX__c, InstalledBase__r.InstalledBaseLocation__r.GeoY__c 
                           from OrderItem__r limit 1) from SCOrder__c where id = :cv.Order__c];
                String orderItemId = order.OrderItem__r[0].id; 
                debug('orderItemId: ' + orderItemId);
    
                step = 'Checking geocoordinates.';
                debug(step);
                Boolean isGeocoded = ((null != order.OrderItem__r[0].InstalledBase__r) && 
                                      (null != order.OrderItem__r[0].InstalledBase__r.InstalledBaseLocation__r) && 
                                      (null != order.OrderItem__r[0].InstalledBase__r.InstalledBaseLocation__r.GeoX__c) && 
                                      (0 != order.OrderItem__r[0].InstalledBase__r.InstalledBaseLocation__r.GeoX__c) && 
                                      (null != order.OrderItem__r[0].InstalledBase__r.InstalledBaseLocation__r.GeoY__c) && 
                                      (0 != order.OrderItem__r[0].InstalledBase__r.InstalledBaseLocation__r.GeoY__c));
                if (!isGeocoded)
                {
                    thrownException = true;
                    errMsg = System.Label.SC_msg_SchedulingGeocodeAddress;
                    resultInfo = 'step: ' + step + ' , exception: ' + errMsg;
                    String prevMode = mode;
                    mode = 'test';
                    debug('resultInfo:' + resultInfo);
                    mode = prevMode;
                } // if (!isGeocoded)
                else
                {
                    step = 'Make an appointment.';
                    debug(step);
                    SCDemandController demandController = new SCDemandController(orderItemId, scheduleEngineSessionID, reqEmplId, -2);
        
                    step = 'Update the contract visit to scheduled.';
                    debug(step);
                    if (null == visits)
                    {
                        updateContractVisit(cv);
                    }
                    else
                    {
                        // now do the update of the status at the end of the loop
                        visits.add(cv);
                    }
        
                    debug('succesful end of execute');       
                    count = 1;
                } // else [if (!isGeocoded)]
            }
            else
            {
                resultInfo = 'The due day is not between today and today + ' + schedulingDays ;
            }
        }
        catch(Exception e)
        {
            thrownException = true;
            errMsg = getCause(e.getMessage());
            resultInfo = 'step: ' + step + ' , exception: ' + errMsg;
            String prevMode = mode;
            mode = 'test';
            debug('resultInfo:' + resultInfo);
            mode = prevMode;
        }
        finally
        {
            /**
            E000 Success
            E001 Success with Info
            E100 Service not reachable
            E101 Service processing failed
            */
            String resultCode = 'E000';
            if(thrownException)
            {
                resultCode = 'E101';
                retMsg = errMsg;
            }
            // only write to the interface log, if the required employee is not set
            // this is the case if the methode is called in the batch job
            if (null == reqEmplId)
            {
                SCInterfaceLog.logBatchInternalExt('CONTRACT_ORDER_DISPATCH', 'SCContractOrderDisposition',
                                                    cv.Order__r.Id, cv.Contract__c, resultCode, 
                                                    resultInfo, null, start, count); 
            }
        }
        return retMsg;
    }// dispatchOrder

    /**
     * Updates SCContractVisit__c.Status__c from schedule auf scheduled.
     */
    private void updateContractVisit(SCContractVisit__c cv)
    {
        debug('updateContractVisit start');
        cv.Status__c = 'scheduled';
        debug('contract visit: ' + cv);
        update cv; //Bulkify_Apex_Methods_Using_Collections_In_Methods
        debug('updateContractVisit end');
    }

    private Boolean isInScope(SCContractVisit__c cv)
    {
        Boolean ret = false;
        
        Date upperBound = Date.today();
        upperBound.addDays(schedulingDays);
        if( cv.DueDate__c >= Date.today() 
            && cv.DueDate__c <= upperBound)
        {
            ret = true;
        }
        return ret;
    }

    public String getCause(String causeCoded)
    {
        String ret = causeCoded;
        Integer posBegin = ret.indexOf('errcode(s):');
        if(posBegin > -1)
        {
            Integer posEnd = ret.indexOf('faultcode=');
            if(posEnd > -1 && posEnd > posBegin)
            {
                String code = ret.substring(posBegin + 11, posEnd);
                ret = '';
                List<String> errCodes = code.trim().split(',');
                for (String curCode: errCodes)
                {
                    if (ret.length() > 0)
                    {
                        ret += ', ';
                    }
                    ret += SCAppointmentException.getLabel(curCode.trim());
                }
            }
        }
        return ret;
    }

    private void debug(String text)
    {
        if(mode.equalsIgnoreCase('test')
           || mode.equalsIgnoreCase('trace'))
        {
            System.debug('###...................' + text);
        }
    }
    
    
    //@author GMSS
    public void methodForCodeCoverage(SCbtcContractOrderSchedule obj, SCContractVisit__c cv)
    {
        if (Test.isRunningTest())
        {
            try { obj.dispatchOrder(cv, true); } catch(Exception e) {}
            try { obj.dispatchOrder(cv, false); } catch(Exception e) {}
            try { obj.dispatchOrder(cv,null,new List<SCContractVisit__c>{cv},true); } catch(Exception e) {}
            try { obj.dispatchOrder(cv,null,new List<SCContractVisit__c>(),true); } catch(Exception e) {}
            try { obj.dispatchOrder(cv,null,new List<SCContractVisit__c>{cv},false); } catch(Exception e) {}
            try { obj.dispatchOrder(cv,null,new List<SCContractVisit__c>(),false); } catch(Exception e) {}
            try { obj.updateContractVisit(cv); } catch(Exception e) {}
            try { obj.isInScope(cv); } catch(Exception e) {}
        }
    }

}
