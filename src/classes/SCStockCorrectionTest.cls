/*
 * @(#)SCStockCorrectionTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 * <p>
 * This class contains unit tests for validating the behaviour of manual
 * correction of stocks.
 * <p>
 * A complete testing environment is created first. Remember that all data
 * created in a Unit test is not commited to the database!!
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */

@isTest
private class SCStockCorrectionTest 
{
    /**
     * createTestArticles
     * ==================
     *
     * Create articles for testing.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    static void createTestArticles()
    {
        {
            List<SCArticle__c> listArt1 = [select Id, ArticleNameCalc__c, AvailabilityType__c,
                                                  Class__c, Name, PriceChangesEnabled__c, ReturnType__c,
                                                  TaxType__c, Text_en__c, Unit__c
                                             from SCArticle__c
                                            where Name = 'A-{0000000001}'];
            
            if ((listArt1 != null) && (listArt1.size() > 0))
            {
                SCArticle__c art1;
                
                if (listArt1.size() > 1)
                {
                    art1 = listArt1.remove(0);
                    Database.delete(listArt1);
                }
                else
                {
                    art1 = listArt1.get(0);
                }
                
                art1.Text_en__c = 'Test article 1';
                art1.Class__c = String.valueOf(SCfwConstants.DOMVAL_ARTICLECLASS_SPAREPART);
                art1.Type__c = String.valueOf(SCfwConstants.DOMVAL_ARTICLETYP_NO_TYP);
                art1.Unit__c = String.valueOf(SCfwConstants.DOMVAL_UNIT_PIECE);
                art1.AvailabilityType__c = String.valueOf(SCfwConstants.DOMVAL_MATERIALDISPO_A);
                art1.TaxType__c = String.valueOf(SCfwConstants.DOMVAL_TAXARTICLE_FULL);
                art1.PriceChangesEnabled__c = true;
                art1.ReturnType__c = '13901';
                Database.update(art1);
            }
            else
            {
                SCArticle__c art1 = 
                    new SCArticle__c(Name = 'A-{0000000001}',
                                     Text_en__c = 'Test article 1',
                                     Class__c = String.valueOf(SCfwConstants.DOMVAL_ARTICLECLASS_SPAREPART),
                                     Type__c = String.valueOf(SCfwConstants.DOMVAL_ARTICLETYP_NO_TYP),
                                     Unit__c = String.valueOf(SCfwConstants.DOMVAL_UNIT_PIECE),
                                     AvailabilityType__c = String.valueOf(SCfwConstants.DOMVAL_MATERIALDISPO_A),
                                     TaxType__c = String.valueOf(SCfwConstants.DOMVAL_TAXARTICLE_FULL),
                                     PriceChangesEnabled__c = true,
                                     ReturnType__c = '13901');
                Database.insert(art1);
            }
        }
        
        
        {
            List<SCArticle__c> listArt2 = [select Id, ArticleNameCalc__c, AvailabilityType__c,
                                                  Class__c, Name, PriceChangesEnabled__c, ReturnType__c,
                                                  TaxType__c, Text_en__c, Unit__c
                                             from SCArticle__c
                                            where Name = 'A-{0000000002}'];
            
            if ((listArt2 != null) && (listArt2.size() > 0))
            {
                SCArticle__c art2;
                
                if (listArt2.size() > 1)
                {
                    art2 = listArt2.remove(0);
                    Database.delete(listArt2);
                }
                else
                {
                    art2 = listArt2.get(0);
                }
                
                art2.Text_en__c = 'Test article 2';
                art2.Class__c = String.valueOf(SCfwConstants.DOMVAL_ARTICLECLASS_SPAREPART);
                art2.Type__c = String.valueOf(SCfwConstants.DOMVAL_ARTICLETYP_NO_TYP);
                art2.Unit__c = String.valueOf(SCfwConstants.DOMVAL_UNIT_PIECE);
                art2.AvailabilityType__c = String.valueOf(SCfwConstants.DOMVAL_MATERIALDISPO_A);
                art2.TaxType__c = String.valueOf(SCfwConstants.DOMVAL_TAXARTICLE_FULL);
                art2.PriceChangesEnabled__c = true;
                art2.ReturnType__c = '13901';
                Database.update(art2);
            }
            else
            {
                SCArticle__c art2 = 
                    new SCArticle__c(Name = 'A-{0000000002}',
                                     Text_en__c = 'Test article 2',
                                     Class__c = String.valueOf(SCfwConstants.DOMVAL_ARTICLECLASS_SPAREPART),
                                     Type__c = String.valueOf(SCfwConstants.DOMVAL_ARTICLETYP_NO_TYP),
                                     Unit__c = String.valueOf(SCfwConstants.DOMVAL_UNIT_PIECE),
                                     AvailabilityType__c = String.valueOf(SCfwConstants.DOMVAL_MATERIALDISPO_A),
                                     TaxType__c = String.valueOf(SCfwConstants.DOMVAL_TAXARTICLE_FULL),
                                     PriceChangesEnabled__c = true,
                                     ReturnType__c = '13901');
                Database.insert(art2);
            }
        }
        
        
        {
            List<SCArticle__c> listArt3 = [select Id, ArticleNameCalc__c, AvailabilityType__c,
                                                  Class__c, Name, PriceChangesEnabled__c, ReturnType__c,
                                                  TaxType__c, Text_en__c, Unit__c
                                             from SCArticle__c
                                            where Name = 'A-{0000000003}'];
            
            if ((listArt3 != null) && (listArt3.size() > 0))
            {
                SCArticle__c art3;
                
                if (listArt3.size() > 1)
                {
                    art3 = listArt3.remove(0);
                    Database.delete(listArt3);
                }
                else
                {
                    art3 = listArt3.get(0);
                }
                
                art3.Text_en__c = 'Test article 3';
                art3.Class__c = String.valueOf(SCfwConstants.DOMVAL_ARTICLECLASS_SPAREPART);
                art3.Type__c = String.valueOf(SCfwConstants.DOMVAL_ARTICLETYP_NO_TYP);
                art3.Unit__c = String.valueOf(SCfwConstants.DOMVAL_UNIT_PIECE);
                art3.AvailabilityType__c = String.valueOf(SCfwConstants.DOMVAL_MATERIALDISPO_A);
                art3.TaxType__c = String.valueOf(SCfwConstants.DOMVAL_TAXARTICLE_FULL);
                art3.PriceChangesEnabled__c = true;
                art3.ReturnType__c = '13901';
                Database.update(art3);
            }
            else
            {
                SCArticle__c art3 = 
                    new SCArticle__c(Name = 'A-{0000000003}',
                                     Text_en__c = 'Test article 3',
                                     Class__c = String.valueOf(SCfwConstants.DOMVAL_ARTICLECLASS_SPAREPART),
                                     Type__c = String.valueOf(SCfwConstants.DOMVAL_ARTICLETYP_NO_TYP),
                                     Unit__c = String.valueOf(SCfwConstants.DOMVAL_UNIT_PIECE),
                                     AvailabilityType__c = String.valueOf(SCfwConstants.DOMVAL_MATERIALDISPO_A),
                                     TaxType__c = String.valueOf(SCfwConstants.DOMVAL_TAXARTICLE_FULL),
                                     PriceChangesEnabled__c = true,
                                     ReturnType__c = '13901');
                Database.insert(art3);
            }
        }
    } // createTestArticles

    /**
     * createTestPlant
     * ===============
     *
     * Create a plant for testing.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    static void createTestPlant()
    {
        List<SCPlant__c> listPlant = [select Id, Info__c, Name from SCPlant__c where Name = '9100'];
        
        if ((listPlant != null) && (listPlant.size() > 0))
        {
            SCPlant__c plant;
            
            if (listPlant.size() > 1)
            {
                plant = listPlant.remove(0);
                //Database.delete([select Id from SCStock__c where Plant__r in :listPlant]);
                Database.delete(listPlant);
            }
            else
            {
                plant = listPlant.get(0);
            }
            
            if (plant.Info__c != 'Test plant')
            {
                plant.Info__c = 'Test plant';
                Database.update(plant);
            }
        }
        else
        {
            SCPlant__c plant = new SCPlant__c(Name = '9100', Info__c = 'Test plant');
            Database.insert(plant);
        }
    } // createTestPlant

    /**
     * createTestStocks
     * ================
     *
     * Create stocks for testing.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    static void createTestStocks()
    {
        SCPlant__c plant = [select Id, Info__c, Name from SCPlant__c where Name = '9100'];
        
        
        {
            List<SCStock__c> listStock1 = [select Id, Info__c, Name, Plant__c from SCStock__c where Name = '9101'];
            
            if ((listStock1 != null) && (listStock1.size() > 0))
            {
                SCStock__c stock1;
                
                if (listStock1.size() > 1)
                {
                    stock1 = listStock1.remove(0);
                    Database.delete(listStock1);
                }
                else
                {
                    stock1 = listStock1.get(0);
                }
                
                if ((stock1.Info__c != 'Test stock 1') || (stock1.Plant__c != plant.Id))
                {
                    stock1.Info__c = 'Test stock 1';
                    stock1.Plant__c = plant.Id;
                    Database.update(stock1);
                }
            }
            else
            {
                SCStock__c stock1 = new SCStock__c(Name = '9101', Info__c = 'Test stock 1', Plant__c = plant.Id);
                Database.insert(stock1);
            }
        }
        
        
        {
            List<SCStock__c> listStock2 = [select Id, Info__c, Name, Plant__c from SCStock__c where Name = '9102'];
            
            if ((listStock2 != null) && (listStock2.size() > 0))
            {
                SCStock__c stock2;
                
                if (listStock2.size() > 1)
                {
                    stock2 = listStock2.remove(0);
                    Database.delete(listStock2);
                }
                else
                {
                    stock2 = listStock2.get(0);
                }
                
                if ((stock2.Info__c != 'Test stock 2') || (stock2.Plant__c != plant.Id))
                {
                    stock2.Info__c = 'Test stock 2';
                    stock2.Plant__c = plant.Id;
                    Database.update(stock2);
                }
            }
            else
            {
                SCStock__c stock2 = new SCStock__c(Name = '9102', Info__c = 'Test stock 2', Plant__c = plant.Id);
                Database.insert(stock2);
            }
        }
    } // createTestStocks


    /**
     * testStockCorrection
     * ===================
     *
     * Test the manual stock correction with following steps:
     * - create all neccessary datas
     * - increase the quantity of an article which is not in the stock 1
     * - increase the quantity of an article which is in the stock 1
     * - decrease the quantity of an article which is not in the stock 1
     * - decrease the quantity of an article which is in the stock 1
     *
     * - increase the quantity of an article which is not in the stock 2
     * - increase the quantity of an article which is in the stock 2
     * - decrease the quantity of an article which is not in the stock 2
     * - decrease the quantity of an article which is in the stock 2
     *
     * - validate the quantities of all entries in all stocks
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    static testMethod void testStockCorrection()
    {
        //SCHelperTestClass.createDomainNonTransData();
        //SCHelperTestClass.createDomMatType();
        //SCHelperTestClass3.createCustomSettings('de',true);
        
        // create all neccessary datas
        createTestPlant();
        createTestStocks();
        createTestArticles();
        
        SCStock__c stock1 = [select Id, Name, Info__c from SCStock__c where Name = '9101'];
        SCStock__c stock2 = [select Id, Name, Info__c from SCStock__c where Name = '9102'];

        SCArticle__c article1 = [select Id, Name, ArticleNameCalc__c from SCArticle__c where Name = 'A-{0000000001}'];
        SCArticle__c article2 = [select Id, Name, ArticleNameCalc__c from SCArticle__c where Name = 'A-{0000000002}'];
        SCArticle__c article3 = [select Id, Name, ArticleNameCalc__c from SCArticle__c where Name = 'A-{0000000003}'];
        
        Test.startTest();
        
        SCMatMoveBaseExtension matMoveExt = new SCMatMoveBaseExtension();
        
        // increase the quantity of an article which is not in the stock 1
        matMoveExt.createMatMove(stock1, article1.Id, 2, SCfwConstants.DOMVAL_MATMOVETYP_CORRECTION_IN);
        matMoveExt.bookMaterial(stock1);
        // increase the quantity of an article which is in the stock 1
        matMoveExt.createMatMove(stock1, article1.Id, 3, SCfwConstants.DOMVAL_MATMOVETYP_CORRECTION_IN);
        matMoveExt.bookMaterial(stock1);
        
        // decrease the quantity of an article which is not in the stock 1
        matMoveExt.createMatMove(stock1, article2.Id, 2, SCfwConstants.DOMVAL_MATMOVETYP_CORRECTION_OUT);
        matMoveExt.bookMaterial(stock1);
        // decrease the quantity of an article which is in the stock 1
        matMoveExt.createMatMove(stock1, article1.Id, 1, SCfwConstants.DOMVAL_MATMOVETYP_CORRECTION_OUT);
        matMoveExt.bookMaterial(stock1);
        
        // increase the quantity of an article which is not in the stock 2
        matMoveExt.createMatMove(stock2, article2.Id, 5, SCfwConstants.DOMVAL_MATMOVETYP_CORRECTION_IN);
        matMoveExt.bookMaterial(stock2);
        // increase the quantity of an article which is in the stock 2
        matMoveExt.createMatMove(stock2, article2.Id, 2, SCfwConstants.DOMVAL_MATMOVETYP_CORRECTION_IN);
        matMoveExt.bookMaterial(stock2);
        
        // decrease the quantity of an article which is not in the stock 2
        matMoveExt.createMatMove(stock2, article3.Id, 1, SCfwConstants.DOMVAL_MATMOVETYP_CORRECTION_OUT);
        matMoveExt.bookMaterial(stock2);
        // decrease the quantity of an article which is in the stock 2
        matMoveExt.createMatMove(stock2, article2.Id, 1, SCfwConstants.DOMVAL_MATMOVETYP_CORRECTION_OUT);
        matMoveExt.bookMaterial(stock2);
        
        // testing the exception case for this methode
        matMoveExt.createMatMove(stock2, '01pT000000055DA', 1, SCfwConstants.DOMVAL_MATMOVETYP_CORRECTION_OUT);

        Test.stopTest();
        
        // validate the quantities of all entries in all stocks
        SCStockItem__c stockItem = [select Qty__c from SCStockItem__c where Stock__c = :stock1.Id and Article__c = :article1.Id];
        System.assertEquals(4,stockItem.Qty__c);
        List<SCStockItem__c> stockItems = [select Qty__c from SCStockItem__c where Stock__c = :stock1.Id and Article__c = :article2.Id];
        System.assertEquals(1,stockItems.size());
        stockItem = [select Qty__c from SCStockItem__c where Stock__c = :stock2.Id and Article__c = :article2.Id];
        System.assertEquals(6,stockItem.Qty__c);
        stockItems = [select Qty__c from SCStockItem__c where Stock__c = :stock2.Id and Article__c = :article3.Id];
        System.assertEquals(1,stockItems.size());
    } // testStockCorrection
} // SCStockCorrectionTest
