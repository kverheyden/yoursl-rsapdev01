/*
 * @(#)WSInventory.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This class implements the web services to manipulate the inventory: cancel or close.
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
global without sharing class WSInventory
{
   /*
    * Cancells the inventory
    * @param   oid   id of the SCInventory object to cancel
    * @return  'OK'  if the inventory was sucessfully cancelled or 'ERROR' text if not
    */
    WebService static String cancelInventory(String oid)
    {
        SCboInventory boInventory = new SCboInventory();
        
        return boInventory.cancel(oid);
    }
    
   /*
    * Closes the inventory
    * @param   oid              id of the SCInventory object to close
    * @param   updateStockQty   this parameter controlls the update of the stock quantity
    * @param   inventoryDate    this parameter controlls the update of the inventory date
    * @return  'OK'             if the inventory was sucessfully cancelled or 'ERROR' text if not
    */
    WebService static String closeInventory(String oid, Boolean updateStockQty, Date inventoryDate)
    {
        SCboInventory boInventory = new SCboInventory();
        
        return boInventory.close(oid, updateStockQty, inventoryDate);
    }
}
