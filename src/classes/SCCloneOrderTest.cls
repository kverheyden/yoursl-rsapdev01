/*
 * @(#)SCCloneOrderTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Alexei Geiger <ageiger@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCCloneOrderTest
{
    public static testMethod void cloneOrderTestPositiv() 
    {
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.createOrder(true);
        SCHelperTestClass.createOrderRoles(true);
        SCHelperTestClass.createOrderItem(true);
        SCHelperTestClass2.createQualificationOrder(SCHelperTestClass.order.Id, SCHelperTestClass.orderItem.Id);
        
        Test.startTest();
        String clonedOrderId = SCCloneOrder.cloneOrder((String)SCHelperTestClass.order.id);
        Test.stopTest();
        
        System.assert(clonedOrderId != (String)SCHelperTestClass.order.id);
        SCOrderQualification__c quali = [Select Qualification__c, Order__c, OrderItem__c 
                                           from SCOrderQualification__c 
                                          where Order__c = :clonedOrderId];
        System.assertNotEquals(quali.OrderItem__c, SCHelperTestClass.orderItem.id);
    }
    
    public static testMethod void cloneOrderTestNegativ1() 
    {
        SCHelperTestClass.createAccountObject('Customer', true);
        string year = '2008';
        string month = '10';
        string day = '5';
        string hour = '12';
        string minute = '20';
        string second = '20';
        string stringDate = year + '-' + month
         + '-' + day + ' ' + hour + ':' + 
        minute +  ':' + second;
        Datetime curDate = datetime.valueOf(stringDate);        

        SCHelperTestClass.createOrder(curDate);
        SCHelperTestClass.createOrderRoles(true);
        SCHelperTestClass.createOrderItem(true);
        
        String msg = SCCloneOrder.cloneOrder((String)SCHelperTestClass.order.id );
        System.assertNotEquals( 'cloneIsNotAllowed_date', msg );
    }

    public static testMethod void cloneOrderTestNegativ2() 
    {
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.createOrder(true);
        SCHelperTestClass.createOrderRoles(true);
        SCHelperTestClass.createOrderItem(true);
        
        // change the order type to sales order
        SCOrder__c ord = new SCOrder__c(Id = SCHelperTestClass.order.Id, 
                                        Type__c = SCfwConstants.DOMVAL_ORDERTYPE_SALESORDER);
        update ord;
        
        String msg = SCCloneOrder.cloneOrder((String)SCHelperTestClass.order.id );
        System.assert( 'cloneIsNotAllowed_type' == msg );
    }
}
