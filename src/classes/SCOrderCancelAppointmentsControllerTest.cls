/*
 * @(#)SCOrderCancelAppointmentsControllerTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Test class checks the controler functions.
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCOrderCancelAppointmentsControllerTest
{
    private static SCOrderCancelAppointmentsController ocac;

    static testMethod void orderCancelAppointmentsControllerPositiv1() 
    {
        SCboOrder.ignoreTrigger = true;
        SCHelperTestClass.createOrderTestSet(true);
        
        List<SCAppointment__c> appointmentsList = SCHelperTestClass.appointments;
        
        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(appointmentsList);
        controller.setSelected(appointmentsList);
        
        ApexPages.currentPage().getParameters().put('id', SCHelperTestClass.order.Id);
        
        ocac = new SCOrderCancelAppointmentsController(controller);
        
        ocac.cancelAppointments();
        System.assertNotEquals(null, ocac.gotoOrder());
        System.assertEquals(false, ocac.onlyTransferred);
        System.assertEquals(true, ocac.showCancelButton);
    }

    static testMethod void orderCancelAppointmentsControllerPositiv2() 
    {
        SCboOrder.ignoreTrigger = true;
        SCHelperTestClass.createOrderTestSet3(true);
        SCHelperTestClass.assignmentMap.get('assignmentDispo1').Status__c = SCfwConstants.DOMVAL_ORDERSTATUS_ACCEPTED;
        update SCHelperTestClass.assignmentMap.get('assignmentDispo1');
        
        List<SCAppointment__c> appointmentsList = SCHelperTestClass.appointments;
        
        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(appointmentsList);
        controller.setSelected(appointmentsList);
        
        ApexPages.currentPage().getParameters().put('id', SCHelperTestClass.order.Id);
        
        ocac = new SCOrderCancelAppointmentsController(controller);
        
        ocac.cancelTransferred = true;
        ocac.handleCancelTransferred();
        ocac.cancelAppointments();
        System.assertNotEquals(null, ocac.gotoOrder());
        System.assertEquals(true, ocac.onlyTransferred);
        System.assertEquals(true, ocac.showCancelButton);
    }

    static testMethod void orderCancelAppointmentsControllerPositiv3() 
    {
        SCboOrder.ignoreTrigger = true;
        SCHelperTestClass.createOrderTestSet3(true);
        SCHelperTestClass.assignmentMap.get('assignmentDispo1').Status__c = SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED;
        update SCHelperTestClass.assignmentMap.get('assignmentDispo1');
        
        List<SCAppointment__c> appointmentsList = SCHelperTestClass.appointments;
        
        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(appointmentsList);
        controller.setSelected(appointmentsList);
        
        ApexPages.currentPage().getParameters().put('id', SCHelperTestClass.order.Id);
        
        ocac = new SCOrderCancelAppointmentsController(controller);
        
        ocac.cancelAppointments();
        System.assertNotEquals(null, ocac.gotoOrder());
        System.assertEquals(false, ocac.onlyTransferred);
        System.assertEquals(false, ocac.showCancelButton);
    }

    static testMethod void orderCancelAppointmentsControllerPositiv4() 
    {
        SCboOrder.ignoreTrigger = true;
        SCHelperTestClass.createOrderTestSet3(true);
        
        List<SCAppointment__c> appointmentsList = SCHelperTestClass.appointments;
        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(appointmentsList);
        
        ApexPages.currentPage().getParameters().put('id', SCHelperTestClass.order.Id);
        
        ocac = new SCOrderCancelAppointmentsController();
        ocac = new SCOrderCancelAppointmentsController(controller);
        
        System.assertNotEquals(null, ocac.cancelAppointments());
    }
}
