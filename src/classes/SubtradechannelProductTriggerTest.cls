@isTest
public class SubtradechannelProductTriggerTest 
{
    /**
     * @author thomas richter <thomas@meformobile.com>
     */
    static testMethod void testTrigger()
    {
        // trigger should look on product and populate corresponding cc product based on MatNr/SKU (Product2.ID2__c = ccrz__E_Product__c.ccrz__ProductId__c)
        
        Product2 product = new Product2(ID2__c='MatNr',Name='MyProduct');
        insert product;
        ccrz__E_Product__c ccProduct = new ccrz__E_Product__c(ccrz__ProductId__c='MatNr',Name='CCMyProduct', ccrz__SKU__c='MatNr',NumberOfSalesUnitsPerPallet__c=1);
        insert ccProduct;
        Subtradechannel__c stc = new Subtradechannel__c(Name='021',UniqueKey__c='021', Subtradechannel__c='021');
        insert stc;
        
        Test.startTest();
		
		List<SubtradechannelProduct__c> liststcProduct = new List<SubtradechannelProduct__c>();
        SubtradechannelProduct__c stcProduct = new SubtradechannelProduct__c();
        stcProduct.Product2__c = product.Id;
        stcProduct.Subtradechannel__c = stc.Id;

		insert stcProduct;      
		
		//test of SubtradechannelProductTriggerCLS 
		liststcProduct.add(stcProduct);
		SubtradechannelProductTriggerCLS scpt = new SubtradechannelProductTriggerCLS();
		scpt.setCloudCrazeProductReference(liststcProduct); 
        
        stcProduct = [SELECT Id,ccrz_product__c FROM SubtradechannelProduct__c WHERE Id=:stcProduct.Id];
        System.assertEquals(ccProduct.Id, stcProduct.ccrz_product__c, 'ccrz_product__c should reference the corresponding cloudcraze product');
        
        Test.stopTest();
    }
}
