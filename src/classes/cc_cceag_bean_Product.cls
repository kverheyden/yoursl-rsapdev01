global with sharing class cc_cceag_bean_Product {
	public String id {get; set;}
	public String name {get; set;}
	public String sku {get; set;}
	public String UnitOfMeasure { get; set; }
	public String ProductStatus { get; set; }
	public String ProductType { get; set; }
	public String shortDesc { get; set; }
	public String imageUrl { get; set; }
	public Map<String,Object> quantityRules{get;set;}
	
	public cc_cceag_bean_Product() {
		
	}
}
