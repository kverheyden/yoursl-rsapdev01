/**
 * @(#)SCboStockItemDeltaOverview.cls
 * 
 * Copyright 2014 by GMS Development GmbH, Karl-Schurz-Strasse 29, 
 * DE-33100 Paderborn. All rights reserved.
 * 
 * Class for update / upsert delta items 
 *
 * @author Eugen Tiessen <etiessen@gms-online.de>
 */
public class SCboStockItemDeltaOverview 
{
	/**
	 * Wrapper for Trigger events (insert, update)
	 * Fills all fields with the values of the stock items (Stockname, Plantname, Articlename, Valuationtype)
	 * Set the timestamp for the source (SAP, CMS, CP)
	 *
	 * @param	sidoList	new values to be stored
	 * @param	sidoOldList	old values
	 */
	public static void BeforeUpsert(List<SCStockItemDeltaOverview__c> sidoList, List<SCStockItemDeltaOverview__c> sidoOldList)
	{
		Set<ID> stockItemIDs = new Set<ID>();
	    Set<String> ID2s = new Set<String>();
	      
	    //Collect stockitem ids and id2s
	    for(SCStockItemDeltaOverview__c sido : sidoList)
		{
			//Collect stockitems ids by id
			if(sido.StockItemId__c != null)
			{
				stockItemIDs.add(sido.StockItemId__c);
				
			}
			//Collect stockitems ids by id2
			else if(sido.ID2__c != null)
			{
				ID2s.add(sido.ID2__c);
			}
			
		}
		
		Map<String, SCStockItem__c> stockItemsByID;
		Map<String, SCStockItem__c> stockItemsByID2 = new Map<String, SCStockItem__c>();
		
		//Read stockitems by ids or id2s	
		if(stockItemIDs.size() > 0 || ID2s.size() > 0)
		{
			stockItemsByID = new Map<String, SCStockItem__c>
			(
				[
					SELECT
						Id,
						util_StockItemSelector__c,
						Qty__c,
						Stock__r.Name,
						Plant__c,
						Article__r.Name,
						ValuationType__c,
						ID2__c
						
					FROM
						SCStockItem__c
					WHERE
						Id = :stockItemIDs
						OR
						util_StockItemSelector__c = :ID2s
				]
			);
			
			
			//Create a stockitem map by id2 as key
			for(SCStockItem__c item: stockItemsByID.values())
			{
				if(item.ID2__c != null && item.ID2__c.length() > 0)
				{
					stockItemsByID2.put(item.ID2__c, item);
				}
				
			}
		}
		
				
		//Add the timestamp for the relevant function
		for(SCStockItemDeltaOverview__c sido : sidoList)
		{
			if(sido.ID2__c == null && sido.StockItemId__c != null && stockItemsByID != null)
			{
				SCStockItem__c item = stockItemsByID.get(sido.StockItem__c);
				if(item != null)
				{
					system.debug('#### sido before id2 change:' + sido);			
					sido.ID2__c = item.util_StockItemSelector__c;
					system.debug('#### sido after id2 change:' + sido);	
				}
				
			}
			
			if(sido.ID2__c != null && sido.StockItemId__c == null && stockItemsByID2 != null)
			{
				SCStockItem__c item = stockItemsByID2.get(sido.id2__c);
				if(item != null)
				{
					system.debug('#### sido before stockitemid change:' + sido);			
					sido.StockItemID__c = item.Id;
					sido.StockItem__c = item.Id;
					
					system.debug('#### sido after stockitemid change:' + sido);
				}
				
			}
			
			//add additional fields (stock, plant, article, valuationtype) from stock item
			if(stockItemsByID != null && stockItemsByID.size() > 0)
			{
				SCStockItem__c item = stockItemsByID.get(sido.StockItem__c);
				if(item != null)
				{
					sido.QtyCP__c = item.Qty__c;
					if(sido.Source__c == 'CMS')
					{
						sido.Stock__c 			= item.Stock__r.Name;
						sido.Plant__c 			= item.Plant__c;
						sido.Article__c 		= item.Article__r.Name;
						sido.ValuationType__c 	= item.ValuationType__c;
						sido.ID2__c = item.util_StockItemSelector__c;
					}
					
				}			
			}
			
			
			if(sido.QtyCMS__c != null && sido.Source__c == 'CMS')
			{
				sido.LastModifiedDateByCMS__c = System.NOW();
			}
			if(sido.QtyCP__c != null)
			{
				sido.LastModifiedDateByCP__c = System.NOW();
			}
			if(sido.QtySAPUnrestricted__c != null && sido.Source__c == 'SAP')
			{
				sido.LastModifiedDateBySAP__c = System.NOW();
			}
			else if(sido.QtySAPInTransit__c != null && sido.Source__c == 'SAP')
			{
				sido.LastModifiedDateBySAP__c = System.NOW();
			}
			
			system.debug('#### sido:' + sido);		
		}
		 
		
	}
}
