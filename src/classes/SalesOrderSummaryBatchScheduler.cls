global class SalesOrderSummaryBatchScheduler implements Schedulable, Database.stateful {
	
	global String SessionId { get; set; }
	
	global SalesOrderSummaryBatchScheduler(String userSessionId) {
		SessionId = userSessionId;
	}
	
	global void execute(SchedulableContext sc) 
	{
		SalesOrderSummaryBatch batch = new SalesOrderSummaryBatch();
		batch.SessionId = SessionId;
		System.debug(UserInfo.getSessionId());
		Id batchId = Database.executeBatch(batch, 1);
	}
}
