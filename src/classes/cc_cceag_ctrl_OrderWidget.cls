global with sharing class cc_cceag_ctrl_OrderWidget {
	public cc_cceag_ctrl_OrderWidget() {
		
	}

	@RemoteAction
	global static ccrz.cc_RemoteActionResult fetchOrders(ccrz.cc_RemoteActionContext ctx, string formdata) {
		ccrz.cc_CallContext.initRemoteContext(ctx);
		ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
		result.inputContext = ctx;
		result.success = false; 
		try {
			Map<String, Object> searchFormMap = (Map<String, Object>) JSON.deserializeUntyped(formdata);
			string orderStatus = (String) searchFormMap.get('searchOrderStatus');
			String userId = (String.isBlank(ctx.portalUserId)) ? UserInfo.getUserId() : ctx.portalUserId;
			String whereClause = ' ';
			if(string.isNotBlank(orderStatus)) {
				whereClause += 'Where ccrz__OrderStatus__c = \'' + orderStatus + '\'';
			}
			String sortClause = ' order by Name desc';
			List<ccrz__E_Order__c> orders = cc_cceag_dao_Cart.getOrders(whereClause, sortClause);
			List<ccrz.cc_bean_MockOrder> orderList = new List<ccrz.cc_bean_MockOrder>();
			for (ccrz__E_Order__c order : orders) {
				ccrz.cc_bean_MockOrder orderBean = new ccrz.cc_bean_MockOrder();
				orderBean.sfid = order.Id;
				orderBean.name = order.Name;
				orderBean.orderDate = order.ccrz__OrderDate__c;
				if (orderBean.orderDate != null)
					orderBean.orderDateStr = orderBean.orderDate.format();
				orderBean.encryptedId = order.ccrz__EncryptedId__c;
				orderBean.status = order.ccrz__OrderStatus__c;
				orderBean.totalAmount = order.ccrz__TotalAmount__c;
				orderBean.externalOrderId = order.ccrz__OrderId__c;
				orderBean.poNumber = order.ccrz__PONumber__c;
				orderBean.cartId = order.ccrz__OriginatedCart__c;
				try{ orderBean.requestDate = (order.ccrz__RequestDate__c != null)?order.ccrz__RequestDate__c.format():null; }catch(SObjectException ignored){/*ignored*/}
				orderList.add(orderBean);
			}
			result.data = orderList;
			result.success = true;
		} catch(Exception e) {
			System.debug(System.LoggingLevel.ERROR, e.getMessage());
            System.debug(System.LoggingLevel.ERROR, e.getStackTraceString());
            ccrz.cc_bean_Message msg = new ccrz.cc_bean_Message();
			msg.type = ccrz.cc_bean_Message.MessageType.CUSTOM;
			msg.classToAppend = 'messagingSection-Error';
			msg.message = e.getStackTraceString();
			msg.severity = ccrz.cc_bean_Message.MessageSeverity.ERROR;
			result.messages.add(msg);
        }
		return result;
	}

	@RemoteAction
	global static ccrz.cc_RemoteActionResult reorder(ccrz.cc_RemoteActionContext ctx, String orderId) {
		ccrz.cc_CallContext.initRemoteContext(ctx);
		ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
		result.inputContext = ctx;
		result.success = false; 
		try {
			String userId = (String.isBlank(ctx.portalUserId)) ? UserInfo.getUserId() : ctx.portalUserId;
			
			if (!String.isBlank(ctx.currentCartID))
				result.data = createCartFromOrder(orderId, ctx.currentCartID);
			else
				result.data = cloneCart(orderId);
			result.success = true;
		} catch(Exception e) {
			System.debug(System.LoggingLevel.ERROR, e.getMessage());
            System.debug(System.LoggingLevel.ERROR, e.getStackTraceString());
            ccrz.cc_bean_Message msg = new ccrz.cc_bean_Message();
			msg.type = ccrz.cc_bean_Message.MessageType.CUSTOM;
			msg.classToAppend = 'messagingSection-Error';
			msg.message = e.getStackTraceString() + ' ' + e.getMessage();
			msg.severity = ccrz.cc_bean_Message.MessageSeverity.ERROR;
			result.messages.add(msg);
        }
		return result;
	}

	public static String createCartFromOrder(string ordId, String cartEncId) {
		ccrz__E_Order__c originalOrder = cc_cceag_dao_Cart.getOrderByEncId(ordId);
		ccrz__E_Cart__c newCart = cc_cceag_dao_Cart.retrieveCartByEncryptedId(cartEncId, true);
		Map<Id, ccrz__E_CartItem__c> existingItemMap = new Map<Id, ccrz__E_CartItem__c> ();
		for (ccrz__E_CartItem__c oldItem: newCart.ccrz__E_CartItems__r)
			existingItemMap.put(oldItem.ccrz__Product__c, oldItem);

		List<ccrz__E_CartItem__c> cartItemsToInsert = new List<ccrz__E_CartItem__c>();
		for (ccrz__E_OrderItem__c item : originalOrder.ccrz__E_OrderItems__r) {
			if (existingItemMap.containsKey(item.ccrz__Product__c)) {
				ccrz__E_CartItem__c oldItem = existingItemMap.get(item.ccrz__Product__c);
				oldItem.ccrz__Quantity__c = oldItem.ccrz__Quantity__c + item.ccrz__Quantity__c;
				cartItemsToInsert.add(oldItem);
			}
			else {
				ccrz__E_CartItem__c cartItem = new ccrz__E_CartItem__c();
				cartItem.ccrz__Product__c = item.ccrz__Product__c;
				//cartItem.ccrz__cartItemType__c = item.ccrz__cartItemType__c;
				cartItem.ccrz__Quantity__c = item.ccrz__Quantity__c;
				cartItem.ccrz__ProductType__c = item.ccrz__ProductType__c;
				cartItem.ccrz__UnitOfMeasure__c = item.ccrz__UnitOfMeasure__c;
				cartItem.ccrz__StoreId__c = item.ccrz__StoreId__c;
				cartItem.ccrz__Price__c = 0;
				cartItem.ccrz__Cart__c = newCart.Id;
				cartItemsToInsert.add(cartItem);
			}
		}
		upsert cartItemsToInsert;
		return newCart.ccrz__EncryptedId__c;
	}

	public static String cloneCart(string ordId) {
		ccrz__E_Order__c originalOrder = cc_cceag_dao_Cart.getOrderByEncId(ordId);
		if (originalOrder.ccrz__OriginatedCart__c != null) {
			ccrz__E_Cart__c oldCart = cc_cceag_dao_Cart.retrieveCartWithItems(originalOrder.ccrz__OriginatedCart__c);
			ccrz__E_Cart__c newCart = new ccrz__E_Cart__c();
			newCart.ccrz__Name__c = 'clone-'+originalOrder.Name;
			newCart.ccrz__EncryptedId__c = null;
			newCart.ccrz__cartType__c = 'Cart';
			newCart.ccrz__storefront__c = originalOrder.ccrz__storefront__c;
			newCart.ccrz__CartStatus__c = 'Open';
			newCart.ccrz__cartID__c = 'newCart'+String.valueOf(Crypto.getRandomInteger());
			newCart.ccrz__SessionID__c = userInfo.getSessionId();
			newCart.ccrz__RequestDate__c = originalOrder.ccrz__RequestDate__c;
	        newCart.ccrz__ShipMethod__c = originalOrder.ccrz__ShipMethod__c;
	        newCart.ccrz__EffectiveAccountID__c = originalOrder.ccrz__EffectiveAccountID__c;
	        newCart.ccrz__ActiveCart__c = true;
	        newCart.ccrz__User__c = originalOrder.ccrz__User__c;
	        newCart.ccrz__Contact__c = originalOrder.ccrz__Contact__c;
	        newCart.ccrz__ContractId__c = originalOrder.ccrz__ContractId__c;

	        List<ccrz__E_ContactAddr__c> addrs = new List<ccrz__E_ContactAddr__c>();
	        ccrz__E_ContactAddr__c newShip = originalOrder.ccrz__ShipTo__r.clone(false, true, false, false);
	        newShip.ccrz__ShippingComments__c = null;
	        ccrz__E_ContactAddr__c newBill = originalOrder.ccrz__BillTo__r.clone(false, true, false, false);
	        addrs.add(newBill);
	        addrs.add(newShip);
	        insert addrs;
	        newCart.ccrz__BillTo__c = newBill.Id;
	        newCart.ccrz__ShipTo__c = newShip.Id;

			insert newCart;
			List<ccrz__E_CartItem__c> cartItemsToInsert = new List<ccrz__E_CartItem__c>();
			for (ccrz__E_OrderItem__c item : originalOrder.ccrz__E_OrderItems__r) {
				ccrz__E_CartItem__c cartItem = new ccrz__E_CartItem__c();
				cartItem.ccrz__Product__c = item.ccrz__Product__c;
				cartItem.ccrz__cartItemType__c = 'Major';
				cartItem.ccrz__Quantity__c = item.ccrz__Quantity__c;
				cartItem.ccrz__ProductType__c = item.ccrz__ProductType__c;
				cartItem.ccrz__UnitOfMeasure__c = item.ccrz__UnitOfMeasure__c;
				cartItem.ccrz__StoreId__c = item.ccrz__StoreId__c;
				cartItem.ccrz__Price__c = 0;
				cartItem.ccrz__Cart__c = newCart.Id;
				cartItemsToInsert.add(cartItem);
			}
			insert cartItemsToInsert;
			ccrz__E_Cart__c cartBean = cc_cceag_dao_Cart.retrieveCart(newCart.id);
			return cartBean.ccrz__EncryptedId__c;
		}
		else
			return null;
	}
	
	public with sharing class cc_cceag_bean_SelectOption {
		public String value 	{get; set;}
		public String label 	{get; set;}
		public cc_cceag_bean_SelectOption(string value, string label) {
			this.value = value;
			this.label = label;
		}
	}

	public string getJsonOrderStatus() {
		List<cc_cceag_ctrl_OrderWidget.cc_cceag_bean_SelectOption> options = new List<cc_cceag_ctrl_OrderWidget.cc_cceag_bean_SelectOption>();
		List <Schema.PicklistEntry> ples = ccrz__E_Order__c.ccrz__OrderStatus__c.getDescribe().getPicklistValues();
		for (Schema.PicklistEntry e : ples) {
			options.add(new cc_cceag_ctrl_OrderWidget.cc_cceag_bean_SelectOption(e.getValue(), e.getLabel()));
		}
		return JSON.serialize(options);
	}

}
