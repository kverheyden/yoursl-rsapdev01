/*
 * @(#)SCbtcBase.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * The batch job to create contract visit records and their contract visit item records
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
global virtual class SCbtcBase 
    implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts
{
    public static Id jobIdToStop;

    public ID executeBatch()
    {
        ID ret = null;
        if(canStartApexJob())
        {
            ret = Database.executeBatch(this);
        }
        else
        {
            System.debug('####The job can not be started because currently 5 another jobs are running!');
        }
        return ret;
    }
    
    public ID executeBatch(Integer scope)
    {
        ID ret = null;
        if(canStartApexJob())
        {
            ret = Database.executeBatch(this, scope);
        }
        else
        {
            System.debug('####The job can not be started because currently 5 another jobs are running!');
        }
        return ret;
    }

    /**
     * In the time only 5 Apex Jobs can run concurrently.
     * This function determines whether it is possible to start another job
     * without taking the risk that the new job will be aborted.
     *
     * @return true if the job can be started.
     */
    Webservice static Boolean canStartApexJob()
    {
        Boolean ret = false;
        List<String> statusList = new List<String>();
        statusList.add('Queued');
        statusList.add('Preparing');
        statusList.add('Processing');
        List<AsyncApexJob> jobList = [Select Id from AsyncApexJob where JobType = 'BatchApex' and Status in :statusList];
        if(jobList.size() < 5)
        {
            ret = true;
        }
        System.debug('createdDate: ' + ret);
        return ret;        
    }

    /**
     * Determines whether the job with the class name for the given country 
     * 
     * @param	country, e.g. DE
     * @param	className, e.g.SCbtcClearingPostProcess
     * @return	'1' if the job is running, else '0'
     *
     */
    Webservice static String isJobRunningForCountry(String country, String className)
    {
        String retValue = '0';
        List<AsyncApexJob> jobList = [Select Id, CreatedDate, JobItemsProcessed, CreatedBy.Name, CreatedById,
                                      CreatedBy.Email, ApexClass.Name
                                     from AsyncApexJob where 
                                     JobType = 'BatchApex' and ApexClass.Name = :className 
                                     and status not in ('Completed', 'Aborted', 'Failed') 
                                     order by CreatedDate ASC, JobItemsProcessed DESC, Id ASC];
        System.debug('###........jobList: ' + jobList);
        List<AsyncApexJob> countryJobList = new List<AsyncApexJob>();
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        if(jobList.size() > 1)
        {
            for(AsyncApexJob asj: jobList)
            {
                SCApplicationSettings__c loopAppSettings =  SCApplicationSettings__c.getInstance(asj.CreatedByID);
                String jobCountryLoop = loopAppSettings.DEFAULT_COUNTRY__c;
                System.debug('### ...Apex Batch Job User name: ' + asj.CreatedBy.Name + ', country: ' + jobCountryLoop);
                if(jobCountryLoop != null && jobCountryLoop.trim() != ''
                   && jobCountryLoop == country)
                {
					retValue = '1';
					break;
                }
            }
        }    
        return retValue;
    }

    /**
     * Determines whether the job with the class name for the country of the caller is running 
     * 
     * @param	className, e.g.SCbtcClearingPostProcess
     * @return	'1' if the job is running, else '0'
     *
     */
    Webservice static String isJobRunningForCallerCountry(String className)
    {
        String retValue = '0';
        List<AsyncApexJob> jobList = [Select Id, CreatedDate, JobItemsProcessed, CreatedBy.Name, CreatedById,
                                      CreatedBy.Email, ApexClass.Name
                                     from AsyncApexJob where 
                                     JobType = 'BatchApex' and ApexClass.Name = :className 
                                     and status not in ('Completed', 'Aborted', 'Failed') 
                                     order by CreatedDate ASC, JobItemsProcessed DESC, Id ASC];
        System.debug('###........jobList: ' + jobList);
        List<AsyncApexJob> countryJobList = new List<AsyncApexJob>();
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        String callerCountry = appSettings.DEFAULT_COUNTRY__c;
        if(jobList.size() > 1)
        {
            for(AsyncApexJob asj: jobList)
            {
                SCApplicationSettings__c loopAppSettings =  SCApplicationSettings__c.getInstance(asj.CreatedByID);
                String jobCountryLoop = loopAppSettings.DEFAULT_COUNTRY__c;
                System.debug('### ...Apex Batch Job User name: ' + asj.CreatedBy.Name + ', country: ' + jobCountryLoop);
                if(jobCountryLoop != null && jobCountryLoop.trim() != ''
                   && jobCountryLoop == callerCountry)
                {
					retValue = '1';
					break;
                }
            }
        }    
        return retValue;
    }

    /**
     * Aborts the job that has  is another running job of the same class.
     * 
     * The completed, aborted and failed jobs are not regarded.
     * 
     * returns true if the current job is to be aborted, else false 
     *
     */
    public Boolean abortOneOfTwoSameJobsRunning(Id currentJobId, String className, String callerMethod)
    {
        Boolean ret = false;
        if(currentJobId != null)
        {
            DateTime createdDateCurr = getCreatedDate(currentJobId);
            System.debug('We are in the current job: ' + currentJobId + ' started at : ' + createdDateCurr);
            
            List<AsyncApexJob> jobList = [Select Id, CreatedDate, JobItemsProcessed, CreatedBy.Name, CreatedById,
                                          CreatedBy.Email, ApexClass.Name
                                         from AsyncApexJob where 
                                         JobType = 'BatchApex' and ApexClass.Name = :className 
                                         and status not in ('Completed', 'Aborted', 'Failed') 
                                         order by CreatedDate ASC, JobItemsProcessed DESC, Id ASC];
            System.debug('###........jobList: ' + jobList);
            System.debug('CallerMethod: ' + callerMethod);
            List<AsyncApexJob> countryJobList = new List<AsyncApexJob>();
            SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
            String currentJobCountry = appSettings.DEFAULT_COUNTRY__c;
            if(jobList.size() > 1)
            {
                Id survivor = null;
                DateTime minCreatedDate = null;
                List<ID> setupOwnerIdList = new List<ID>();
                for(AsyncApexJob asj: jobList)
                {
                    SCApplicationSettings__c loopAppSettings =  SCApplicationSettings__c.getInstance(asj.CreatedByID);
                    String jobCountryLoop = loopAppSettings.DEFAULT_COUNTRY__c;
                    System.debug('### ...Apex Batch Job User name: ' + asj.CreatedBy.Name + ', country: ' + jobCountryLoop);
                    if(survivor == null)
                    {
                        if(jobCountryLoop != null && jobCountryLoop.trim() != ''
                          && jobCountryLoop == currentJobCountry)
                        {
                            survivor = asj.Id;
                            System.debug('survivor Id: ' + survivor);
                            minCreatedDate = asj.CreatedDate;
                        }
                    }
                    if(jobCountryLoop == currentJobCountry || jobCountryLoop == null || jobCountryLoop.trim() == '' )
                    {
                        countryJobList.add(asj);
                    }    
                }
                                                                 
                // find a survivor and abort the rest
                abortAllButMinimum(survivor, minCreatedDate, countryJobList, currentJobCountry);
                if(survivor != currentJobId)
                {
                    //currentJob has been aborted
                    ret = true;
                }
            }    
        }
        return ret;
    }


    /**
     * Finds the earliest created date of the job
     */
    public static DateTime minimumDate(List<AsyncApexJob> jobList)
    {
        DateTime ret =  null;
        if(jobList.size() > 0)
        {
            for(AsyncApexJob aaj: jobList)
            {
                if(ret == null || ret > aaj.CreatedDate)
                {
                    ret = aaj.CreatedDate;
                }
            }
        }    
        return ret;
    }

    /**
     * The first job in the result is the job that ought to be preserved.
     * regarding JobItemsProcessed
     * Records are  CreatedDate ASC, JobItemsProcessed DESC, Id ASC
     */
    public static Id getSurvivorId(List<AsyncApexJob> jobList)
    {
        Id ret =  null;
        if(jobList.size() > 0)
        {
            for(AsyncApexJob aaj: jobList)
            {
                ret = jobList[0].id;
            }    
        }    
        return ret;
    }

    /**
     * Aborts all jobs but the job that was started first.
     */
    public static void abortAllButMinimum(Id minimumId, DateTime minimumDate, List<AsyncApexJob> jobList,
                                         String currentJobCountry)
    {
        System.debug('the job : ' + minimumId + ' started at: ' + minimumDate + ' remains running.');      
        if(jobList.size() > 0)
        {
            AsyncApexJob surviverJob = null;
            List<AsyncApexJob> abortedJobs = new List<AsyncApexJob>();
            for(AsyncApexJob aaj: jobList)
            {
                if(minimumId != aaj.id)
                {
                    System.debug('the job : ' + aaj.id + ' started at: ' + aaj.CreatedDate  +  ' has been aborted.'); 
                    abortedJobs.add(aaj);     
                    System.abortJob(aaj.id);
                }
                else
                {
                    surviverJob = aaj;
                }
            }
            // send Mails to the users that started the aborted jobs
            if(abortedJobs.size() > 0)
            {
                for(AsyncApexJob aj: abortedJobs)
                {
                    // Send an email to the Apex job's submitter notifying of job completion.  
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    
                    String[] toAddresses = new String[] {aj.CreatedBy.Email};
                    
                    mail.setToAddresses(toAddresses);
                    mail.setSubject('Country: ' + currentJobCountry + '. The Apex Batch Job ' + aj.ApexClass.Name + ' started at ' + aj.CreatedDate + ' has been aborted');
                    
    
                    String text = null;
                    SCApplicationSettings__c loopAppSettings =  SCApplicationSettings__c.getInstance(aj.CreatedByID);
                    String jobCountryLoop = loopAppSettings.DEFAULT_COUNTRY__c;
                    System.debug('###aj: ' + aj);
                    System.debug('###aj.ApexClass: ' + aj.ApexClass);
                    System.debug('###aj.ApexClass.Name: ' + aj.ApexClass.Name);
                    System.debug('###aj.CreatedBy.Name: ' + aj.CreatedBy.Name);
                    System.debug('###surviverJob: ' + surviverJob);
                    System.debug('###surviverJob.CreatedBy: ' + surviverJob.CreatedBy);
                    System.debug('###surviverJob.CreatedBy.Name: ' + surviverJob.CreatedBy.Name);
                    
                    if(jobCountryLoop != null && jobCountryLoop.trim() != '')
                    {
                        text = 'The batch Apex job ' + aj.ApexClass.Name + ' started at ' + aj.CreatedDate
                                    + ' by the user ' + aj.CreatedBy.Name
                                    + ' has been aborted because the batch job ' + aj.ApexClass.Name
                                    + ' started at ' + minimumDate + ' by the user ' + surviverJob.CreatedBy.Name
                                    + ' is still running for the country ' + currentJobCountry;
                    }
                    else
                    {
                        text = 'The batch Apex job ' + aj.ApexClass.Name + ' started at ' + aj.CreatedDate
                                    + ' by the user ' + aj.CreatedBy.Name
                                    + ' has been aborted because the profile of this user has no set DEFAULT_COUNTRY '
                                    + ' in the Application Settings!';
                    }
                    mail.setPlainTextBody(text);
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});                    
               }
            }
        }    
    }

   /*
    * Called by the framework when the batch job is started. We construct the soql statement
    * for reading the resource assignments.
    * @param BC the batch context
    * @return the query locator with the selected material movements (up to 50 mio records possible)
    */
    global virtual Database.QueryLocator start(Database.BatchableContext BC)
    {
        return null;
    }
   /*
    * Called for each batch of records to process.
    * @param BC the batch context
    * @param scope the list records to be processed
    */
    global virtual void execute(Database.BatchableContext BC, List<sObject> scope)
    {
    }
   /*
    * Called by the framework when the batch job has been completed. 
    * We send an e-mail notification about the status
    * @param BC the batch context
    */
    global virtual void finish(Database.BatchableContext BC)
    {
    }

    /**
     *    Gets the creation date of an job
     */
    public static DateTime getCreatedDate(Id jobId)
    {
        DateTime ret = null;
        List<AsyncApexJob> jobList = [Select CreatedDate from AsyncApexJob where Id = :jobId];
        if(jobList.size() > 0)
        {
            ret = jobList[0].CreatedDate;
        }
        return ret;
    }

}
