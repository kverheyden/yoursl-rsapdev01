@isTest
private class SolutionTriggerMethodsTest {
	static testMethod void testSolutionTrigger() {
		// TO DO: implement unit test
		Solution testSolution = new Solution();
		testSolution.SolutionName = 'My solution';
		testSolution.SolutionNote = 'Note...';
		//testSolution.SolutionNumber = '12345';
		insert testSolution;
		
		Solution insertedSolution = [SELECT SolutionName, SolutionNote, AssignedCategories__c FROM Solution LIMIT 1];
		
		System.debug('JM Debug:' + insertedSolution);
		System.debug('JM Debug:' + insertedSolution.AssignedCategories__c);
		System.assertEquals(insertedSolution.AssignedCategories__c, null);
		
	}
}
