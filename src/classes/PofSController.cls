/**********************************************************************

Name: PofSController
 
======================================================

Purpose: 

This class serves as the data management controller for the PofS edit view.

======================================================

History 

------- 

Date 			AUTHOR 				DETAIL

11/05/2013 		Oliver Preuschl 	INITIAL DEVELOPMENT
01/07/2013		Jan Mensfeld		Added List of PofSDeviceModel

***********************************************************************/

public class PofSController{

	//Non-Static Variables-----------------------------------------------------------------------------
	ApexPages.StandardController GO_Controller;
    public PofSDocumentController GO_DocumentController;
    public String GV_ImageId;
	public String GV_NewImageName;
	
	public PictureOfSuccess__c GO_Image;
    public PofSSettings__c GO_PofSSettings;
    public List<PofSDeviceModel__c> GO_PofSDeviceModels;
        
	//getter & setter methods------------------------------------------------------------------------	
	public PofSDocumentController getGO_DocumentController(){
		return GO_DocumentController;
	}
	
	public void setGO_DocumentController( PofSDocumentController PO_Value){
		GO_DocumentController = PO_Value;
	}
	
	public String getGV_ImageId(){
		return GV_ImageId;
	}
		
	public void setGV_ImageId( String PO_Value){
		GV_ImageId = PO_Value;
	}
	
	public String getGV_NewImageName(){
		return GV_NewImageName;
	}
	
	public void setGV_NewImageName( String PO_Value){
		GV_NewImageName = PO_Value;
	}
	
	public PictureOfSuccess__c getGO_Image(){
		return GO_Image;
	}
	
	public void setGO_Image( PictureOfSuccess__c PO_Value){
		GO_Image = PO_Value;
	}

	public PofSSettings__c getGO_PofSSettings(){
			if( GO_PofSSettings == null ){
                GO_PofSSettings = PofSSettings__c.getInstance( 'Default' );
            }
		return GO_PofSSettings;
	}
	
	public void setGO_PofSSettings( PofSSettings__c PO_Value){
		GO_PofSSettings = PO_Value;
	}	

	public List<PofSDeviceModel__c> getGO_PofSDeviceModels() {
		return GO_PofSDeviceModels;
	}
	
	//Constructors-----------------------------------------------------------------------------
    public PofSController( ApexPages.StandardController PO_Controller ){
		System.debug( 'Entering PofSController: ' );
		
		GO_Controller = PO_Controller;
		GO_Image = ( PictureOfSuccess__c )PO_Controller.getRecord();
		try{
        	GV_ImageId = GO_Image.Picture__c;
        }catch( Exception LO_Exception ){

        }
        GO_PofSDeviceModels = 
        	[SELECT Id, Name, 
        		DeviceModel__r.Name, 
        		DeviceModel__r.AvailableBrands__c, 
        		DeviceModel__r.Brand__c
        	 FROM PofSDeviceModel__c 
        	 WHERE PictureOfSuccess__c =: GO_Image.Id];
        GO_DocumentController = new PofSDocumentController();
		System.debug( 'Exiting PofSController: ' );
    }
    
    //Action Methods---------------------------------------------------------------------------
	
	/*******************************************************************
	 Purpose: 		Dummy Function
	 ********************************************************************/
	public void Dummy(){
		System.debug( 'Entering Dummy: ' );
		
        if( GV_ImageId != '000000000000000' ){
			GO_Image.Picture__c = GV_ImageId;
		}
		
		System.debug( 'Exiting Dummy: ' );
    }
    
	/*******************************************************************
	 Purpose: 		Refresh the list of background images after an image has been uploaded
	 ********************************************************************/
    public void refreshBackgrounds(){
		System.debug( 'Entering refreshBackgrounds: ' );
		
		if( GV_NewImageName != '' ){
	        GO_DocumentController.refreshGL_Backgrounds();
			GV_ImageId = GO_DocumentController.getBackgroundByName( GV_NewImageName );
			System.debug( 'NewImageId: ' + GV_ImageId );
			if( GV_ImageId != '000000000000000' ){
	        	GO_Image.Picture__c = GV_ImageId;
			}
		}
		
		System.debug( 'Exiting refreshBackgrounds: ' );
    }	

	/*******************************************************************
	 Purpose: 		Activate/Deactivate the PofS, check if the image size is correct
	 ********************************************************************/	
    public void toggleActivation(){
		System.debug( 'Entering toggleActivation: ' );
		
		Decimal LV_ImageWidth = getImageWidth();
		Decimal LV_ImageHeight = getImageHeight();

        if( !GO_Image.Active__c ){
        	if( ( LV_ImageWidth == getGO_PofSSettings().ImageWidthValidation__c ) && ( LV_ImageHeight == getGO_PofSSettings().ImageHeightValidation__c ) ){
        		GO_Image.Active__c = true;
        		GO_Image.Image_Width__c = LV_ImageWidth;
        		GO_Image.Image_Height__c = LV_ImageHeight;
        		Try{
        			update( GO_Image );
				}catch( Exception LO_Exception ){
					ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, LO_Exception.getMessage() ) );
					GO_Image.Active__c = false;
				}
        	}else{
        		ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, 'The PofS could not be activated because the image size does not match the required size (' + GO_PofSSettings.ImageWidthValidation__c + ' X ' + GO_PofSSettings.ImageHeightValidation__c + ')' ) );
       		}
        }else{
        	GO_Image.Active__c = false;
        	GO_Image.Image_Width__c = LV_ImageWidth;
        	GO_Image.Image_Height__c = LV_ImageHeight;
        	update( GO_Image );
        }
		
		System.debug( 'Exiting toggleActivation: ' );
    }

	/*******************************************************************
	 Purpose: 		Get the width of the image, This has to be done in real time because the size is calculated in an underlying component
	 ********************************************************************/	
    public Decimal getImageWidth(){
    	System.debug( 'Entering getGV_ImageWidth: ' );

		Decimal LV_ImageWidth = [ SELECT Image_Width__c FROM PictureOfSuccess__c WHERE ( Id =: getGO_Image().Id ) ].Image_Width__c;

		System.debug( 'Exiting getGV_ImageWidth: ' );
		return LV_ImageWidth;
	}

	/*******************************************************************
	 Purpose: 		Get the height of the image, This has to be done in real time because the size is calculated in an underlying component
	 ********************************************************************/	
	public Decimal getImageHeight(){
		System.debug( 'Entering getGV_ImageHeight: ' );

		Decimal LV_ImageHeight = [ SELECT Image_Height__c FROM PictureOfSuccess__c WHERE ( Id =: getGO_Image().Id ) ].Image_Height__c;

		System.debug( 'Exiting getGV_ImageHeight: ' );
		return LV_ImageHeight;
	}


}
