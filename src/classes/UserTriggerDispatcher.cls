public with sharing class UserTriggerDispatcher extends TriggerDispatcherBase {

	public static Boolean isAfterInsertProcessing = false;
	public static Boolean isAfterUpdateProcessing = false;

	public override void afterInsert(TriggerParameters tp) {
		if (!isAfterInsertProcessing) {
			isAfterInsertProcessing = true;
			execute(new UserTriggerHandler.UserAIHandler(), tp, TriggerParameters.TriggerEvent.afterInsert);
			isAfterInsertProcessing = false;
		} else 
			execute(null, tp, TriggerParameters.TriggerEvent.afterInsert);
	}

	public override void afterUpdate(TriggerParameters tp) {
		if (!isAfterUpdateProcessing) {
			isAfterUpdateProcessing = true;
			execute(new UserTriggerHandler.UserAUHandler(), tp, TriggerParameters.TriggerEvent.afterUpdate);
			isAfterUpdateProcessing = false;
		} else 
			execute(null, tp, TriggerParameters.TriggerEvent.afterUpdate);
	}
}
