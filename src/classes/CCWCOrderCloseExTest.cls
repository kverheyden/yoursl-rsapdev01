/* 
 * @(#)CCWCOrderCloseExTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * www.gms-online.de 
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of
 * GMS Development GmbH. ("Confidential Information").  You shall not disclose 
 * any confidential information and shall use it only in accordance with the 
 * terms of the license agreement you aggreed with GMS.
 */
 
/**
 * @author SS <sschrage@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class CCWCOrderCloseExTest
{
    static SCOrder__c upInsertNecessaryData(Boolean closeOrder)
    {
        Integer i=0;
        
        
        //CustomSettings
        Database.insert(new CCSettings__c(IFEnableTriggerOrderCreate__c = 1));
        Database.insert(new SCApplicationSettings__c(DISABLETRIGGER__c = false));
        
        
        //Brands
        Brand__c brand = new Brand__c(Competitor__c = false,
                                      ID2__c        = 'TestBrand',
                                      Name          = 'Test');
        Database.insert(new List<Brand__c> {brand});
        
        
        //Articles
        List<SCArticle__c> listArticle = new List<SCArticle__c>();
        listArticle.add(new SCArticle__c(Name = 'art0', ID2__c = 'art0', LockType__c = SCfwConstants.DOMVAL_ARTLOCKTYP_NO));
        listArticle.add(new SCArticle__c(Name = 'art1', ID2__c = 'art1', LockType__c = SCfwConstants.DOMVAL_ARTLOCKTYP_NO));
        listArticle.add(new SCArticle__c(Name = 'art2', ID2__c = 'art2', LockType__c = SCfwConstants.DOMVAL_ARTLOCKTYP_NO));
        listArticle.add(new SCArticle__c(Name = 'art3', ID2__c = 'art3', LockType__c = SCfwConstants.DOMVAL_ARTLOCKTYP_NO));
        listArticle.add(new SCArticle__c(Name = 'art4', ID2__c = 'art4', LockType__c = SCfwConstants.DOMVAL_ARTLOCKTYP_NO));
        listArticle.add(new SCArticle__c(Name = 'art5', ID2__c = 'art5', LockType__c = SCfwConstants.DOMVAL_ARTLOCKTYP_NO));
        listArticle.add(new SCArticle__c(Name = 'art6', ID2__c = 'art6', LockType__c = SCfwConstants.DOMVAL_ARTLOCKTYP_NO));
        listArticle.add(new SCArticle__c(Name = 'art7', ID2__c = 'art7', LockType__c = SCfwConstants.DOMVAL_ARTLOCKTYP_NO));
        listArticle.add(new SCArticle__c(Name = 'art8', ID2__c = 'art8', LockType__c = SCfwConstants.DOMVAL_ARTLOCKTYP_NO));
        listArticle.add(new SCArticle__c(Name = 'art9', ID2__c = 'art9', LockType__c = SCfwConstants.DOMVAL_ARTLOCKTYP_NO));
        Database.insert(listArticle);
        
        
        //Plants
        SCPlant__c plant = new SCPlant__c(Info__c = 'Test Werk Info',
                                          Name    = 'Test Werk');        
        Database.insert(new List<SCPlant__c>{plant});
        
        
        //Stocks
        SCStock__c stock = new SCStock__c(Info__c          = 'Test Lager Info',
                                          Name             = '6401',
                                          Plant__c         = plant.Id);
        Database.insert(new List<SCStock__c>{stock});
        
        
        //StockItems
        List<SCStockItem__c> listStockItem = new List<SCStockItem__c>();
        i=0;
        for (SCArticle__c art : listArticle)
        {
            i++;
            listStockItem.add(new SCStockItem__c(Article__c = art.Id, Qty__c = i, Stock__c = stock.Id));
        }
        Database.insert(listStockItem);
        
        
        //ProductModel
        SCProductModel__c prodModel = new SCProductModel__c(Brand__c           = brand.Id,
                                                            Name               = 'ProduktModel',
                                                            Text_en__c         = 'ProduktModel English');
        Database.insert(prodModel);
        
        
        //InstalledBase
        SCInstalledBase__c instBase = new SCInstalledBase__c(Brand__c        = brand.Id,
                                                             IdInt__c        = 'IdInt1',
                                                             IdExt__c        = 'IdExt1',
                                                             Stock__c        = stock.Id,
                                                             ProductModel__c = prodModel.Id,
                                                             SerialNo__c     = '1234567890');
        Database.insert(new List<SCInstalledBase__c> {instBase});
        
        
        //Calendar
        SCCalendar__c calendar = new SCCalendar__c(Country__c = 'DE',
                                                   Description__c = 'Germany standard',
                                                   Name = 'Germany standard');
        Database.insert(calendar);
        
        
        //BusinessUnit
        SCBusinessUnit__c businessUnit = new SCBusinessUnit__c(Calendar__c   = calendar.Id,
                                                               GeoX__c       = 123.456789,
                                                               GeoY__c       =  98.765432,
                                                               Name          = 'test',
                                                               Stock__c      = stock.Id,
                                                               StockWSRFR__c = stock.Id,
                                                               StockWSRFS__c = stock.Id,
                                                               StockWSRTM__c = stock.Id,
                                                               Type__c       = '5905');
        Database.insert(businessUnit);
        
        
        //Resource
        SCResource__c resource = new SCResource__c(Alias_txt__c         = 'test',
                                                   Employee__c          = UserInfo.getUserId(),
                                                   Name                 = UserInfo.getUserName(),
                                                   Type__c              = '2601');
        Database.insert(resource);
        
        
        //ResourceAssignments
        SCResourceAssignment__c resAssign = new SCResourceAssignment__c(Country__c    = 'DE',
                                                                        Department__c = businessUnit.Id,
                                                                        Resource__c   = resource.Id,
                                                                        ValidTo__c    = Date.today(),
                                                                        ValidFrom__c  = Date.today());
        Database.insert(resAssign);
        
        //Order
        SCOrder__c order = new SCOrder__c(Closed__c          = DateTime.now().addDays(-2),
                                          Followup1__c       = SCfwConstants.DOMVAL_FOLLOWUP1_NONE,
                                          Followup2__c       = SCfwConstants.DOMVAL_FOLLOWUP2_NONE,
                                          ID2__c             = 'Order_ClosedYesterday',
                                          isWorkshopOrder__c = true,
                                          Status__c          = SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL,
                                          Type__c            = SCfwConstants.DOMVAL_ORDERTYPE_INHOUSE);
        Database.insert(order);
        
        
        //OrderItems                                      
        SCOrderItem__c orderItem = new SCOrderItem__c(InstalledBase__c = instBase.Id,
                                                      Order__c         = order.Id);
        Database.insert(new List<SCOrderItem__c> {orderItem});
        
        
        //Appointment
        SCAppointment__c appoint = new SCAppointment__c(AssignmentStatus__c = SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL,
                                                        End__c              = DateTime.newInstance(2012,1,2,13,00,00),
                                                        Order__c            = order.Id,
                                                        Resource__c         = resource.Id,
                                                        Start__c            = DateTime.newInstance(2012,1,2,12,00,00),
                                                        Type__c             = SCfwConstants.DOMVAL_APPOINTMENTTYP_JOB);
        Database.insert(new List<SCAppointment__c>{appoint});
        
        
        //close order
        if (closeOrder)
        {
            order.Closed__c    = DateTime.now();
            order.Followup1__c = SCfwConstants.DOMVAL_FOLLOWUP1_NONE;
            order.Followup2__c = SCfwConstants.DOMVAL_FOLLOWUP2_NONE;
            order.Status__c    = SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL;
            order.Type__c      = SCfwConstants.DOMVAL_ORDERTYPE_INHOUSE;
            
            Database.update(new List<SCOrder__c>{order});
        }
        
        
        //etc
        Map<Id,Id> mapAssignment = new Map<Id,Id>();
        for (SCAppointment__c app : [SELECT Assignment__c, OrderItem__c FROM SCAppointment__c])
        {
            mapAssignment.put(app.OrderItem__c, app.Assignment__c);
        }
        
        
        //OrderLines
        List<SCOrderLine__c> listOrdLine = new List<SCOrderLine__c>();
        i=0;
        for (SCArticle__c art : listArticle)
        {
            i++;
            listOrdLine.add(new SCOrderLine__c(Article__c    = art.Id,
                                               Assignment__c = mapAssignment.get(orderItem.Id),
                                               Order__c      = order.Id,
                                               OrderItem__c  = orderItem.Id,
                                               Qty__c        = i));
        }
        Database.insert(listOrdLine);
        
        
        //OrderRepairCodes
        String errLoc1;
        String errLoc2;
        for (Schema.PicklistEntry schemaPickEntry : SCOrderRepairCode__c.ErrorLocation1__c.getDescribe().getPicklistValues())
        {
            errLoc1 = schemaPickEntry.getValue();
        }
        for (Schema.PicklistEntry schemaPickEntry : SCOrderRepairCode__c.ErrorLocation2__c.getDescribe().getPicklistValues())
        {
            errLoc2 = schemaPickEntry.getValue();
        }
        Database.insert(new SCOrderRepairCode__c(Assignment__c     = mapAssignment.get(orderItem.Id),
                                                 ErrorLocation1__c = errLoc1,
                                                 ErrorLocation2__c = errLoc2,
                                                 Order__c          = order.Id,
                                                 OrderItem__c      = orderItem.Id));
        
        return(order);
    }
    
    
    
    
    
    
    
    
    
    
    //Start 'process' with an closed order
    //And the ERP-Fields aren't set
    static testMethod void processC_S0_CaseA()
    {
        SCOrder__c order = upInsertNecessaryData(true);
        
        Test.startTest();
        
        Boolean result = CCWCOrderCloseEx.process(order.Id);
        
        Test.stopTest();
        
        System.assertEquals(true, result);
        System.assertEquals('S0-Active', [SELECT ERPAutoOrderClose__c FROM SCOrder__c WHERE Id = :order.Id LIMIT 1].ERPAutoOrderClose__c);
    }
    
    //Start 'process' with an closed order
    //And the ERP-Fields are set
    static testMethod void processC_S0_CaseB()
    {
        SCOrder__c order = upInsertNecessaryData(true);
        order.ERPOrderNo__c = 'Test ERPOrderNo';
        order.ERPStatusOrderCreate__c = 'ok';
        Database.update(order);
        
        Test.startTest();
        
        Boolean result = CCWCOrderCloseEx.process(order.Id);
        
        Test.stopTest();
        
        System.assertEquals(true, result);
        System.assertEquals('S1-Update', [SELECT ERPAutoOrderClose__c FROM SCOrder__c WHERE Id = :order.Id LIMIT 1].ERPAutoOrderClose__c);
    }
    
    //Start 'process' with an closed order and status 'S0...'
   /* static testMethod void processC_S1()
    {
        SCOrder__c order = upInsertNecessaryData(true);
        order.ERPAutoOrderClose__c = 'S1-Update';
        order.ERPOrderNo__c = 'Test ERPOrderNo';
        order.ERPStatusOrderCreate__c = 'ok';
        Database.update(order);
        
        Test.startTest();
        
        Boolean result = CCWCOrderCloseEx.process(order.Id);
        
        Test.stopTest();
        
        System.assertEquals(true, result);
        System.assertEquals('S1-Update', [SELECT ERPAutoOrderClose__c FROM SCOrder__c WHERE Id = :order.Id LIMIT 1].ERPAutoOrderClose__c);
    }*/
    
    //Start 'process' with an closed order and status 'S1...'
    static testMethod void processC_S2()
    {
        SCOrder__c order = upInsertNecessaryData(true);
        order.ERPAutoOrderClose__c = 'S1-Update';
        order.ERPOrderNo__c = 'Test ERPOrderNo';
        order.ERPStatusOrderCreate__c = 'ok';
        order.ERPStatusEquipmentUpdate__c = 'ok';
        Database.update(order);
        
        Test.startTest();
        
        Boolean result = CCWCOrderCloseEx.process(order.Id);
        
        Test.stopTest();
        
        System.assertEquals(true, result);
        System.assertEquals('S3-OrderClose', [SELECT ERPAutoOrderClose__c FROM SCOrder__c WHERE Id = :order.Id LIMIT 1].ERPAutoOrderClose__c);
    }
    
    //Start 'process' with an closed order and status 'S1...'
    static testMethod void processC_S2AfterMoveMat()
    {
        SCOrder__c order = upInsertNecessaryData(true);
        order.ERPAutoOrderClose__c = 'S2-MoveMat';
        order.ERPOrderNo__c = 'Test ERPOrderNo';
        order.ERPStatusOrderCreate__c = 'ok';
        order.ERPStatusEquipmentUpdate__c = 'ok';
        order.ERPStatusMaterialMovement__c = 'ok';
        Database.update(order);
        
        Test.startTest();
        
        CCWCOrderCloseEx.processNextAfterMoveMat(order.Id);
        
        Test.stopTest();
        
        System.assertEquals('S3-OrderClose', [SELECT ERPAutoOrderClose__c FROM SCOrder__c WHERE Id = :order.Id LIMIT 1].ERPAutoOrderClose__c);
    }
    
    //Start 'process' with an closed order and status 'S2...'
    static testMethod void processC_S3()
    {
        SCOrder__c order = upInsertNecessaryData(true);
        order.ERPAutoOrderClose__c = 'S2-MoveMat';
        order.ERPOrderNo__c = 'Test ERPOrderNo';
        order.ERPStatusOrderCreate__c = 'ok';
        order.ERPStatusMaterialMovement__c = 'ok';
        Database.update(order);
        
        Test.startTest();
        
        Boolean result = CCWCOrderCloseEx.process(order.Id);
        
        Test.stopTest();
        
        System.assertEquals(true, result);
        System.assertEquals('S3-OrderClose', [SELECT ERPAutoOrderClose__c FROM SCOrder__c WHERE Id = :order.Id LIMIT 1].ERPAutoOrderClose__c);
    }
    
    //Start 'process' with an closed order and status 'S3...'
    /*static testMethod void processC_S4()
    {
        SCOrder__c order = upInsertNecessaryData(true);
        order.ERPAutoOrderClose__c = 'S3-OrderClose';
        order.ERPOrderNo__c = 'Test ERPOrderNo';
        order.ERPStatusOrderCreate__c = 'ok';
        order.ERPStatusOrderClose__c = 'ok';
        Database.update(order);
        
        Test.startTest();
        
        Boolean result = CCWCOrderCloseEx.process(order.Id);
        
        Test.stopTest();
        
        System.assertEquals(true, result);
        System.assertEquals('S4-Archive', [SELECT ERPAutoOrderClose__c FROM SCOrder__c WHERE Id = :order.Id LIMIT 1].ERPAutoOrderClose__c);
    }*/
    
    //Start 'process' with an closed order and status 'S4...'
    static testMethod void processC_Finished()
    {
        SCOrder__c order = upInsertNecessaryData(true);
        order.ERPAutoOrderClose__c = 'S4-Archive';
        order.ERPOrderNo__c = 'Test ERPOrderNo';
        order.ERPStatusOrderCreate__c = 'ok';
        order.ERPStatusArchiveDocumentInsert__c = 'ok';
        Database.update(order);
        
        Test.startTest();
        
        Boolean result = CCWCOrderCloseEx.process(order.Id);
        
        Test.stopTest();
        
        System.assertEquals(true, result);
        System.assertEquals('Finished', [SELECT ERPAutoOrderClose__c FROM SCOrder__c WHERE Id = :order.Id LIMIT 1].ERPAutoOrderClose__c);
    }
    
    //Try to start 'process' with an false Id
    static testMethod void processE1()
    {
        SCOrder__c order = upInsertNecessaryData(false);
        
        Test.startTest();
        
        Boolean result = CCWCOrderCloseEx.process('test');
        
        Test.stopTest();
        
        System.assertEquals(false, result);
        System.assertEquals('Inactive', [SELECT ERPAutoOrderClose__c FROM SCOrder__c WHERE Id = :order.Id LIMIT 1].ERPAutoOrderClose__c);
    }
    
    //Try to start 'process' with ERPAutoOrderClose__c-Value 'Finished'
    static testMethod void processE2()
    {
        SCOrder__c order = upInsertNecessaryData(true);
        order.ERPAutoOrderClose__c = 'Finished';
        
        Database.update(order);
        
        Test.startTest();
        
        Boolean result = CCWCOrderCloseEx.process(order.Id);
        
        Test.stopTest();
        
        System.assertEquals(false, result);
        System.assertEquals('Finished', [SELECT ERPAutoOrderClose__c FROM SCOrder__c WHERE Id = :order.Id LIMIT 1].ERPAutoOrderClose__c);
    }
    
    //Start 'processNext' with an closed order and wrong status 'S4...'
    //The result has to be status 'S1...'
    /*static testMethod void processNextE()
    {
        SCOrder__c order = upInsertNecessaryData(true);
        order.ERPAutoOrderClose__c = 'S4-Archive';
        order.ERPOrderNo__c = 'Test ERPOrderNo';
        order.ERPStatusOrderCreate__c = 'ok';
        Database.update(order);
        
        Test.startTest();
        
        CCWCOrderCloseEx.processNext(order.Id);
        
        Test.stopTest();
        
        System.assertEquals('S1-Update', [SELECT ERPAutoOrderClose__c FROM SCOrder__c WHERE Id = :order.Id LIMIT 1].ERPAutoOrderClose__c);
    }*/
    
}
