/*
 * @(#)SCContactHistoryController.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCContactHistoryController
{
    public Id orderId;
    public Id contractId { get; private set; }
    public Id historyId;
    protected SCboOrder boOrder;
    protected SCboContract boContract;
    
    public Boolean isNewEntry { get; set; }
    public List<SCContactHistory__c> contacts { get; set; }
    public SCContactHistory__c contHist { get; set; }
    
    //Account info variables
    public Account account{ get; set; }
    public Boolean isContract{ get; set; }

    /**
     * Constructor
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCContactHistoryController()
    {
        System.debug('#### SCContactHistoryController()');
        orderId = (Id)ApexPages.CurrentPage().getParameters().get('oid');
        contractId = (Id)ApexPages.CurrentPage().getParameters().get('cid');
        historyId = (Id)ApexPages.CurrentPage().getParameters().get('hid');

        if (null != historyId)
        {
            SCContactHistory__c hist = [select Id, Order__c, Contract__c 
                                          from SCContactHistory__c 
                                         where Id = :historyId];
            orderId = hist.Order__c;
            contractId = hist.Contract__c;
        } // if (null != historyId)
        isNewEntry = false;
        
        readContactHistory();
        
    }

    /**
     * Reads the contact history data for an order or a contract.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public void readContactHistory()
    {
        SCContract__c contract = new SCContract__c();
        if (null != orderId)
        {
            contacts = [select Id, Name, Order__c, Contract__c, Account__c, 
                               Account__r.FirstName__c, Account__r.LastName__c, 
                               Status__c, Reason__c, Information__c, 
                               LastCall__c, NextCall__c, CreatedBy.Name, 
                               CreatedDate  
                               from SCContactHistory__c 
                         where Order__c = :orderId 
                         order by Name desc];
                         
            
            String role = (String) ApexPages.CurrentPage().getParameters().get('role');
            if( role != null )
            {
                List<SCOrderRole__c> orderRole = new List<SCORderRole__c>();
                if( role == 'ir')
                {
                    orderRole = [ Select Account__r.Name, Account__r.Mobile__c, Account__r.Phone,
                                  Account__r.PersonHomePhone__c, Account__r.Email__c, Account__r.BillingAddress__c
                                  From SCOrderRole__c where Order__c =: orderId and OrderRole__c =: '50303' ];
                }
                else
                {
                    orderRole = [ Select Account__r.Name, Account__r.Mobile__c, Account__r.Phone,
                                  Account__r.PersonHomePhone__c, Account__r.Email__c, Account__r.BillingAddress__c
                                  From SCOrderRole__c where Order__c =: orderId and OrderRole__c =: '50301' ];
                }
                account = orderRole.get(0).Account__r;
                isContract = false;
            }
        } // if (null != orderId)
        else if (null != contractId)
        {
            contacts = [select Id, Name, Order__c, Contract__c, Account__c, 
                               Account__r.FirstName__c, Account__r.LastName__c, 
                               Status__c, Reason__c, Information__c, 
                               LastCall__c, NextCall__c, CreatedBy.Name, 
                               CreatedDate  
                          from SCContactHistory__c 
                         where Contract__c = :contractId 
                         order by Name desc];

            if( ApexPages.currentPage().getParameters().get('co') != null )
            {
                contract = [ Select AccountOwner__r.Name, AccountOwner__r.Mobile__c, AccountOwner__r.Phone,
                             AccountOwner__r.PersonHomePhone__c, AccountOwner__r.Email__c, AccountOwner__r.BillingAddress__c
                             From SCContract__c where SCContract__c.Id =: contractId ];
                account = contract.AccountOwner__r;
                
            }else
            {
                contract = [ Select Account__r.Name, Account__r.Mobile__c, Account__r.Phone,
                             Account__r.PersonHomePhone__c, Account__r.Email__c, Account__r.BillingAddress__c
                             From SCContract__c where SCContract__c.Id =: contractId ];
                account = account = contract.Account__r;
            }
            isContract = true;
    
        } // else if (null != contractId)
    } // readContactHistory

    /**
     * Initialisations for creation a new entry.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference newContact()
    {
        System.debug('#### SCContactHistoryController(): newContact');
        contHist = new SCContactHistory__c();
        if (null != orderId)
        {
            boOrder = new SCboOrder();
            boOrder.readForContactHistory(orderId);
            contHist.Order__c = orderId;
            if (null != boOrder.order.Contract__c)
            {
                contHist.Contract__c = boOrder.order.Contract__c;
            } // if (null != boOrder.order.Contract__c)
        } // if (null != orderId)
        else if (null != contractId)
        {
            boContract = new SCboContract();
            boContract.readForContactHistory(contractId);
            contHist.Contract__c = contractId;
            SCContractVisit__c nextVisit = boContract.getNextVisit();
            if ((null != nextVisit) && (null != nextVisit.Order__c))
            {
                boOrder = new SCboOrder();
                boOrder.readForContactHistory(nextVisit.Order__c);
                contHist.Order__c = orderId;
            } // if ((null != nextVisit) && (null != nextVisit.Order__c))
        } // else if (null != contractId)
        
        visitWithoutOrder = null;
        canCreateOrder = null;
        isNewEntry = true;
        return null;
    } // newContact

    /**
     * Save a new contact entry.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference saveContact()
    {
        System.debug('#### SCContactHistoryController(): saveContact');
        
        if (null != contHist.Contract__c)
        {
            SCContract__c contract = new SCContract__c(Id = contHist.Contract__c, 
                                                       ContactHistStatus__c = contHist.Status__c, 
                                                       ContactHistReason__c = contHist.Reason__c, 
                                                       ContactHistNextCall__c = contHist.NextCall__c);
            if (('NO_NEED' == contHist.Status__c) || 
                ('MAINT_BY_INST' == contHist.Status__c) || 
                ('NO_VAI_UNIT' == contHist.Status__c))
            {
                contract.EnableAutoScheduling__c = false;
                contract.EnableOrderCreation__c = false;
            }
            update contract;
        } // if ((null != contHist.Contract__c) && ...
        
        if ((null != contHist.Order__c) && 
            ('DONE' == contHist.Status__c))
        {
            SCOrder__c order = new SCOrder__c(Id = contHist.Order__c, 
                                              FollowUp1__c = null, 
                                              FollowUp2__c = null);
            update order;
        } // if ((null != contHist.Order__c) && ...
        
        try
        {
            insert contHist;
            isNewEntry = false;
            readContactHistory();
        }
        catch (Exception e)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
                                                          e.getMessage());
            ApexPages.addMessage(msg);
        }
        return null;
    } // saveContact

    /**
     * Cancel the creation of a new contact without saving.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference cancel()
    {
        System.debug('#### SCContactHistoryController(): cancel');
        isNewEntry = false;
        return null;
    } // cancel

    /**
     * Set the field NextCall__c depending on Status__c.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference setNextCall()
    {
        System.debug('#### SCContactHistoryController(): setNextCall: Status   -> ' + contHist.Status__c);
        System.debug('#### SCContactHistoryController(): setNextCall: NextCall -> ' + contHist.NextCall__c);
        // only make a change, if no value is set
        if (('FOLLOW_UP' == contHist.Status__c) && (null == contHist.NextCall__c))
        {
            contHist.NextCall__c = DateTime.now().addDays(1);
        } // if (('FOLLOW_UP' == contHist.Status__c) && (null == contHist.NextCall__c))
        // reset the naxt call only if hte status is not follow-up
        else if ('FOLLOW_UP' != contHist.Status__c)
        {
            contHist.NextCall__c = null;
        } // else if ('FOLLOW_UP' != contHist.Status__c)
        System.debug('#### SCContactHistoryController(): setNextCall: contHist -> ' + contHist);
        return null;
    } // setNextCall
    
    /**
     * Gets the planned maintenance date.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public String getMaintenanceDate()
    {
        String maintDate = '';
        
        // if we have an order
        if (null != orderId)
        {
            // get the date from the first appointment
            if (boOrder.appointments.size() > 0)
            {
                maintDate = boOrder.appointments[0].Start__c.date().format();
            } // if (boOrder.appointments.size() > 0)
            // if no appointments available, get the preferred start date
            else
            {
                maintDate = boOrder.order.CustomerPrefStart__c.format();
            }
        } // if (null != orderId)
        else if (null != contractId)
        {
            SCContractVisit__c nextVisit = boCOntract.getNextVisit();
            if (null != nextVisit)
            {
                // the visit has an order, so get the date from this
                if (null != boOrder)
                {
                    if (boOrder.appointments.size() > 0)
                    {
                        maintDate = boOrder.appointments[0].Start__c.date().format();
                    } // if (boOrder.appointments.size() > 0)
                    // if no appointments available, get the preferred start date
                    else
                    {
                        maintDate = boOrder.order.CustomerPrefStart__c.format();
                    }
                } // if (null != boOrder)
                else
                {
                    maintDate = nextVisit.DueDate__c.format();
                } // else [if (null != boOrder)]
            } // if (null != nextVisit)
        } // else if (null != contractId)
        
        return maintDate;
    } // getMaintenanceDate
    
    /**
     * Gets the phone number of the contact person.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public String getPhone()
    {
        String phone = '';
        
        // if we have an order
        if (null != orderId)
        {
            SCOrderRole__c role = boOrder.getRole(SCfwConstants.DOMVAL_ORDERROLE_LE);
            if (null != role)
            {
                phone = role.Phone__c;
            }
        } // if (null != orderId)
        else if (null != contractId)
        {
            phone = boContract.contract.Account__r.Phone;
        } // else if (null != contractId)
        
        return phone;
    } // getPhone
    
    /**
     * Gets the phone number 2 of the contact person.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public String getPhone2()
    {
        String phone2 = '';
        
        // if we have an order
        if (null != orderId)
        {
            SCOrderRole__c role = boOrder.getRole(SCfwConstants.DOMVAL_ORDERROLE_LE);
            if (null != role)
            {
                phone2 = role.Phone2__c;
            }
        } // if (null != orderId)
        else if (null != contractId)
        {
            phone2 = boContract.contract.Account__r.Phone2__c;
        } // else if (null != contractId)
        
        return phone2;
    } // getPhone2
    
    /**
     * Gets the name of the contact person.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public String getName()
    {
        String name = '';
        
        // if we have an order
        if (null != orderId)
        {
            SCOrderRole__c role = boOrder.getRole(SCfwConstants.DOMVAL_ORDERROLE_LE);
            if (null != role)
            {
                contHist.Account__c = role.Account__c;
                if (SCbase.isSet(role.Name2__c))
                {
                    name = role.Name2__c + ' ';
                } // if (SCbase.isSet(role.Name2__c))
                name += role.Name1__c;
            } // if (null != role)
        } // if (null != orderId)
        else if (null != contractId)
        {
            contHist.Account__c = boContract.contract.Account__c;
            if (boContract.contract.Account__r.isPersonAccount__c)
            {
                if (SCbase.isSet(boContract.contract.Account__r.FirstName__c))
                {
                    name = boContract.contract.Account__r.FirstName__c + ' ';
                } // if (SCbase.isSet(role.Account__r.FirstName__c))
                name += boContract.contract.Account__r.LastName__c;
            } // if (boContract.contract.Account__r.isPersonAccount__c)
            else
            {
                if (SCbase.isSet(boContract.contract.Account__r.Name2__c))
                {
                    name = boContract.contract.Account__r.Name2__c + ' ';
                } // if (SCbase.isSet(boContract.contract.Account__r.Name2__c))
                name += boContract.contract.Account__r.Name;
            } // else [if (boContract.contract.Account__r.isPersonAccount__c)]
        } // else if (null != contractId)
        
        return name;
    } // getName
        
    /**
     * Gets the name of the scheduled engineer.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public String getEngineer()
    {
        String name = '';
        
        // get the date from the first appointment
        if ((null != boOrder) && (boOrder.appointments.size() > 0))
        {
            name = boOrder.appointments[0].Assignment__r.Employee__c + 
                   ' (' + boOrder.appointments[0].Assignment__r.Resource__r.alias_txt__c + ')';
        } // if ((null != boOrder) && (boOrder.appointments.size() > 0))
        return name;
    } // getEngineer
    
    /**
     * Gets the name of the contract.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public String getContractName()
    {
        String contractName = '';
        
        // if we have an order
        if (null != orderId)
        {
            contractName = boOrder.order.Contract__r.util_details__c;
        } // if (null != orderId)
        else if (null != contractId)
        {
            contractName = boContract.contract.util_details__c;
        } // else if (null != contractId)
        
        return contractName;
    } // getContractName
    
    /**
     * Gets the name of the order.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public String getOrderName()
    {
        String orderName = '';
        
        // if we have an order
        if (null != orderId)
        {
            orderName = boOrder.order.Name;
        } // if (null != orderId)
        else if (null != contractId)
        {
            SCContractVisit__c nextVisit = boContract.getNextVisit();
            if ((null != nextVisit) && (null != nextVisit.Order__c))
            {
                orderName = nextVisit.Order__r.Name;
            } // if ((null != nextVisit) && (null != nextVisit.Order__c))
        } // else if (null != contractId)
        
        return orderName;
    } // getOrderName
    
    /**
     * Gets an indicator if the next contract visit
     * has no order reference.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public Boolean visitWithoutOrder
    {
        get
        {
            if (null == visitWithoutOrder)
            {
                if (null != contractId)
                {
                    SCContractVisit__c nextVisit = boContract.getNextVisit();
                    if (null != nextVisit)
                    {
                        visitWithoutOrder = (null == nextVisit.Order__c);
                    } // if (null != nextVisit)
                } // if (null != contractId)
                else
                {
                    visitWithoutOrder = false;
                } // else [if (null != contractId)]
            } // if (null == visitWithoutOrder)
            return visitWithoutOrder;
        }
        
        set;
    } // visitWithoutOrder
    
    /**
     * Gets an indicator if there is a next contract visit
     * without an order.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference skipNextVisit()
    {
        if (null != contractId)
        {
            SCContractVisit__c nextVisit = boContract.getNextVisit();
            if (null != nextVisit)
            {
                //set the status of the contract visit
                nextVisit.Status__c = 'Skip';
                upsert nextVisit;
                // read the contract again, with all changed data
                boContract.readForContactHistory(contractId);
                // from now the buttons [Skip next visit] and [Create order] are disabled
                visitWithoutOrder = false;
                canCreateOrder = false;
            } // if (null != nextVisit)
        } // if (null != contractId)
        return null;
    } // skipNextVisit
    
    /**
     * Gets an indicator if an order can be created for 
     * the next contract visit.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public Boolean canCreateOrder
    {
        get
        {
            if (null == canCreateOrder)
            {
                if (null != contractId)
                {
                    SCContractVisit__c nextVisit = boContract.getNextVisit();
                    if (null != nextVisit)
                    {
                        List<String> visitIds = new List<String>();
                        visitIds.add(nextVisit.Id);
                        canCreateOrder = ('OK' == SCbtcContractOrderCreate.canCreateOrders(visitIds));
                    } // if (null != nextVisit)
                } // if (null != contractId)
                else
                {
                    canCreateOrder = false;
                } // else [if (null != contractId)]
            } // if (null == canCreateOrder)
            return canCreateOrder;
        }
        
        set;
    } // canCreateOrder
    
    /**
     * Creates an order for the next contract visit.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference createOrder()
    {
        if (null != contractId)
        {
            SCContractVisit__c nextVisit = boContract.getNextVisit();
            if (null != nextVisit)
            {
                List<String> visitIds = new List<String>();
                visitIds.add(nextVisit.Id);
                // create order
                SCbtcContractOrderCreate.syncCreateOrders(visitIds);
                // read the contract again, with all changed data
                boContract.readForContactHistory(contractId);
            } // if (null != nextVisit)
        } // if (null != contractId)
        return null;
    } // createOrder
    
    /**
     * Dispatch an order for the next contract visit.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference dispatchOrder()
    {
        if (null != contractId)
        {
            SCContractVisit__c nextVisit = boContract.getNextVisit();
            if (null != nextVisit)
            {
                // dispatch order
                try
                {
                    SCOrder__c order = [select id, (Select id from OrderItem__r limit 1) 
                                          from SCOrder__c where id = :nextVisit.Order__c];
                    if (order.OrderItem__r.size() > 0)
                    {
                        SCPlanningSessionExtension planningSession = new SCPlanningSessionExtension();
                        String orderItemId = order.OrderItem__r[0].id; 
                        SCDemandController demandController = 
                                new SCDemandController(orderItemId, planningSession.sessionID, -2);
    
                        nextVisit.Status__c = 'scheduled';
                        update nextVisit;

                        // read the new order data for displaying
                        boOrder = new SCboOrder();
                        boOrder.readForContactHistory(nextVisit.Order__c);
                        contHist.Order__c = nextVisit.Order__c;
                    } // if (order.OrderItem__r.size() > 0)
                }
                catch (CalloutException e)
                {
                    // handle multiple exceptions
                    SCAppointmentException[] exs = SCAppointmentException.getExceptions(e);
                    
                    for (Exception ex : exs)
                    {
                        ApexPages.addMessages(ex);
                    }
                }
                
                // from now the buttons [Skip next visit] and [Create order] are disabled
                visitWithoutOrder = false;
                canCreateOrder = false;
            } // if (null != nextVisit)
        } // if (null != contractId)
        return null;
    } // dispatchOrder
} // class SCContactHistoryController
