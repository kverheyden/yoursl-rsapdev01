/*
 * Author: Duc Nguyen Tien (ngdawid@gmail.com/d.nguyentien@yoursl.de)
 * Components:
 * CdeOrderAufstellauftragController.cls
 * CdeOrderAufstellauftragControllerTest.cls
 * CdeOrderAufstellauftragController.vfp
 * SCOrder__c.Button.GenerateContract
 * static resource checkbox_false_img 
 * static resrouce checkbox_true_img 
 * 
 * Manual steps:
 * -deploy new SCOrder__c layouts with button on it.
 */
public class CdeOrderAufstellauftragController {
    public ApexPages.StandardController controller {get;set;}
    public List<SCOrderItem__c> orderItems {get;set;}
    //public Map<ID, SCInstalledBaseLocationContact__c> orderItemIdToContactMap {get;set;}
    public Contact ansprechpartner {get;set;}
    public SCOrder__c order {get;set;}
    public Integer maxNumOfOrderItems {
        get{
        	return order.OrderItem__r.size();    
        }
    }
    
    public CdeOrderAufstellauftragController(ApexPages.StandardController stdCtlr){
        
       	order = (SCOrder__c) stdCtlr.getRecord();
        
        List<Contact> contacts = [Select ID, FirstName, LastName, Phone from Contact where AccountID = :order.utilAccountCA__c and Firstname = 'Verkaufsberater'];
        if(!contacts.isEmpty())
            ansprechpartner = contacts.get(0);
      	/* Duc Nguyen Tien | Ansprechpartner now is taken from Contact of related account through a utilAccountCA__r relation.
        List<SCOrderItem__c> orderItems = [Select ID, InstalledBase__r.InstalledBaseLocation__c from SCOrderItem__c where Order__c =: order.ID];
        Set<ID> installationLocationIDs = new Set<ID>();
        Map<ID,ID> orderItemToBaseLocation = new Map<ID,ID>();
        orderItemIdToContactMap = new Map<ID, SCInstalledBaseLocationContact__c>();
        
        for(SCOrderItem__c orderItem : orderItems){
            orderItemIdToContactMap.put(orderItem.ID, new SCInstalledBaseLocationContact__c()); // To ensure the existence of value when the page will ask for it.
            if(orderItem.InstalledBase__r.InstalledBaseLocation__c == null) 
                continue; 
            orderItemToBaseLocation.put(orderItem.InstalledBase__r.InstalledBaseLocation__c, orderItem.ID );
            installationLocationIDs.add(orderItem.InstalledBase__r.InstalledBaseLocation__c);
        }
        if(!installationLocationIDs.isEmpty()){
            Map<ID, SCInstalledBaseLocationContact__c> contacts = new Map<ID, SCInstalledBaseLocationContact__c>([Select ID,InstalledBaseLocation__c, Email__c,Mobile__c,Phone__c,Name from SCInstalledBaseLocationContact__c where InstalledBaseLocation__c in :installationLocationIDs]);
            if(!contacts.keySet().isEmpty())
                for(SCInstalledBaseLocationContact__c contact : contacts.values()){
                    if(orderItemToBaseLocation.containsKey(contact.InstalledBaseLocation__c))
                       orderItemIdToContactMap.put(orderItemToBaseLocation.get(contact.InstalledBaseLocation__c), contact);
                }
        }
        */
        
    }
    
}
