/**********************************************************************
Name:  CCWSCheckPromotionMemberTest 

Test Class for CCWSCheckPromotionMember

History                                                            
-------                                                            
Date  		AUTHOR								DETAIL 
02/14/2014 	Bernd Werner <b.werner@yoursl.de> 	creation of this class      
      
***********************************************************************/
@isTest
private class CCWSCheckPromotionMemberTest { 

    static testMethod void myUnitTest() {
        List<Account> lAccountList	= new List<Account>();
    	
    	// Create TestAccounts
        Account oTestAccount = new Account();
        oTestAccount.Name = 'Max Mustermann';
	    oTestAccount.BillingCountry__c = 'DE';
	    oTestAccount.ID2__c = '12345678';
	    lAccountList.add(oTestAccount);
	    
	    Account oTestAccount2 = new Account();
        oTestAccount2.Name = 'Max Mustermann';
	    oTestAccount2.BillingCountry__c = 'DE';
	    oTestAccount2.ID2__c = '123456789';
	    lAccountList.add(oTestAccount2);
	    
	    Account oTestAccount3 = new Account();
        oTestAccount3.Name = 'Max Mustermann';
	    oTestAccount3.BillingCountry__c = 'DE';
	    oTestAccount3.ID2__c = '1234567891';
	    lAccountList.add(oTestAccount3);
	    
	    Account oTestAccount4 = new Account();
        oTestAccount4.Name = 'Max Mustermann';
	    oTestAccount4.BillingCountry__c = 'DE';
	    oTestAccount4.ID2__c = '12345678912';
	    lAccountList.add(oTestAccount4);
	    
	    Account oTestAccount5 = new Account();
        oTestAccount5.Name = 'Max Mustermann';
	    oTestAccount5.BillingCountry__c = 'DE';
	    oTestAccount5.ID2__c = '123456789123';
	    lAccountList.add(oTestAccount5);
	    
	    insert lAccountList;
	    
	    // Create a Testpromotion
	    Promotion__c oTestPromotion		= new Promotion__c();
	    oTestPromotion.Name				= 'Testpromotion';
	    oTestPromotion.PromotionID__c	= '54321';
	    insert oTestPromotion;
	    
	    // Add Member to Promotion
	    CCWSAddPromotionMember.addPromotionMember('12345678;123456789;1234567891;12345678912;123456789123', '54321', 'test321');
	    
	    // No Promotion
	    CCWSCheckPromotionMember.checkPromotionMember('54321ert', 'precheck');
	    // Empty Promotion
	    CCWSCheckPromotionMember.checkPromotionMember('', 'precheck');
	    // No Mode
	    CCWSCheckPromotionMember.checkPromotionMember('54321', '');
	    // Check Member 
	    CCWSCheckPromotionMember.checkPromotionMember('54321', 'precheck');
	    // Set Status
	    CCWSCheckPromotionMember.checkPromotionMember('54321', 'SetStatus');
	    
	    // Create a second Testpromotion and Test more than one promotion found
	    Promotion__c oTestPromotion2		= new Promotion__c();
	    oTestPromotion2.Name				= 'Testpromotion';
	    oTestPromotion2.PromotionID__c		= '54321';
	    insert oTestPromotion2;
	    CCWSCheckPromotionMember.checkPromotionMember('54321', 'precheck');
	    
    }
}
