/**********************************************************************
Name: RestServiceUtilities()
======================================================
Purpose: 
Logs URL-Parameters and returned Object in  
======================================================
History 
------- 
Date AUTHOR DETAIL 
03/27/2014 Jan Mensfeld INITIAL DEVELOPMENT
***********************************************************************/

public with sharing class RestServiceUtilities{
	public Enum WSStatus {
		SUCCESS,
		WARNING,
		ERROR
	}
	
	public class WSLog {
		public Datetime RequestStart { get; set; }
		public WSStatus Status { get; set; }
		public String JSONRequest { get; set; }
		public WSLogs__c Log { get; private set; }
		
		public WSLog() {
			Log = new WSLogs__c();
			RequestStart = null;
			Status = WSStatus.SUCCESS;
			JSONRequest = null;
		}
		
		public WSLog setStart(DateTime start) {
			this.RequestStart = this.RequestStart == null ? start : this.RequestStart;
			return this;
		}
		
		public void setStatus(WSStatus status) {
			this.Status = status;
		}
		
		public WSLog setJSONRequest(String jsonRequest) {
			this.JSONRequest = this.JSONRequest == null ? jsonRequest : this.JSONRequest;
			return this;
		}
		
		public void insertLog(){
			if (!isDebugActive())
				return;
			
			if (this.Status != null)
				this.Log.Status__c = String.valueOf(this.Status);
			setServiceName(this.Log);
			RestRequest request = RestContext.request;
			RestResponse response = RestContext.response;
			
			this.Log.RemoteAddress__c = request.remoteAddress;
			
			this.Log.Source__c = SOURCE_INTERNAL;
			this.Log.URI__c = request.requestURI + getParameters(request);
			this.Log.HttpMethod__c = request.httpMethod;
			if (this.JSONRequest == null)
				this.Log.RequestBody__c = request.RequestBody.toString();
			else
				this.Log.RequestBody__c = JSONRequest;
			
			if (response.responseBody.toString().length() > 32000)
				this.Log.RawResponse__c = response.responseBody.toString().subString(0, 30000) + '[...]';
			else 
				this.Log.RawResponse__c = response.responseBody.toString();
			
			this.Log.ResponseTime__c = System.now().getTime() - this.RequestStart.getTime();
			insert this.Log;
			
			Attachment attachment = new Attachment();
			attachment.Body = response.responseBody;
			attachment.ParentId = this.Log.Id;
			attachment.ContentType = 'text/plain';
			attachment.Name = 'Raw Response.txt';
			insert attachment;
		}
		
	}
	
	
	private static final String SOURCE_INTERNAL = 'Internal';
	private static final String SOURCE_EXTERNAL = 'External';
	private static final String DEBUG_IS_ACTIVE = 'DebugIsActive';
	private static final String STATUS_SUCCESS = 'Success';
	private static final String STATUS_ERROR = 'Error';
		
	
	public static void insertNewLog(Datetime requestStart, Object obj, Boolean success) {
		if (!isDebugActive())
			return;
		WSLogs__c objLog = new WSLogs__c();
		objLog.Status__c = success ? STATUS_SUCCESS : STATUS_ERROR;
		setServiceName(objLog);
		RestRequest request = RestContext.request;
		RestResponse response = RestContext.response;
		
		objLog.RemoteAddress__c = request.remoteAddress;
		
		objLog.Source__c = SOURCE_INTERNAL;
		objLog.URI__c = request.requestURI + getParameters(request);
		objLog.HttpMethod__c = request.httpMethod;
		if (obj == null)
			objLog.RequestBody__c = request.RequestBody.toString();
		else
			objLog.RequestBody__c = JSON.serialize(obj);
		
		if (response.responseBody.toString().length() > 32000)
			objLog.RawResponse__c = response.responseBody.toString().subString(0, 30000) + '[...]';
		else 
			objLog.RawResponse__c = response.responseBody.toString();
		
		objLog.ResponseTime__c = System.now().getTime() - requestStart.getTime();
		insert objLog;
		Attachment attachment = new Attachment();
		attachment.Body = response.responseBody;
		attachment.ParentId = objLog.Id;
		attachment.ContentType = 'text/plain';
		attachment.Name = 'Raw Response.txt';
		insert attachment;
	}
	
	public static void insertNewLog(DateTime requestStart, Boolean success) {
		if (!isDebugActive())
			return;
		WSLogs__c objLog = new WSLogs__c();
		objLog.Status__c = success ? STATUS_SUCCESS : STATUS_ERROR;
		setServiceName(objLog);
		RestRequest request = RestContext.request;
		RestResponse response = RestContext.response;
		
		objLog.RemoteAddress__c = request.remoteAddress;
		
		objLog.Source__c = SOURCE_INTERNAL;
		objLog.URI__c = request.requestURI + getParameters(request);
		objLog.HttpMethod__c = request.httpMethod;
		
		if (response.responseBody.toString().length() > 32000)
			objLog.RawResponse__c = response.responseBody.toString().subString(0, 30000) + '[...]';
		else 
			objLog.RawResponse__c = response.responseBody.toString();
		
		objLog.ResponseTime__c = System.now().getTime() - requestStart.getTime();
		insert objLog;
		Attachment attachment = new Attachment();
		attachment.Body = response.responseBody;
		attachment.ParentId = objLog.Id;
		attachment.ContentType = 'text/plain';
		attachment.Name = 'Raw Response.txt';
		insert attachment;
	}

	private static Boolean isDebugActive() {
		System.debug('JM Debug Webservicelog active');
		try {
			WSDebugSettings__c debugSetting = WSDebugSettings__c.getValues(DEBUG_IS_ACTIVE);
			return debugSetting.Value__c == '1';			
		} catch (Exception e) {
			System.debug(e.getTypeName() + ': ' + e.getMessage());
			System.debug('JM Debug Webservicelog inactive');
			return false;
		}
	}
	
	private static String getParameters(RestRequest request) {
		Map<String, String> mapParameters = request.params;
		List<String> listParameters = new List<String>();
		for (String s : mapParameters.keySet())
			listParameters.add(s + '=' + mapParameters.get(s));
		if (listParameters.size() > 0 )
			return '?' + String.join(listParameters, '&');
		return '';
	}
	
	private static void setServiceName(WSLogs__c objLog) {
		List<String> wsURIParts = RestContext.Request.RequestURI.split('/');
		String wsName = wsURIParts[wsURIParts.size() - 1];
		objLog.ServiceName__c = wsName;
	}
}
