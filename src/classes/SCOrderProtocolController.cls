/**
 * @(#)SCOrderProtocolController.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 *
 * This class summarizes SCOrderLog__c SC, Order__History and NoteAndAttachment
 * depending on SCOrder__c
 *
 * @author Eugen Tiessen <etiessen@gms-online.de>
 */

public class SCOrderProtocolController 
{
    // the source data to diaplay
    private List<SCOrderLog__c> orderLog { private get; private set; }
    private List<SCOrder__History> orderHistory { private get; private set; }
    private SCOrder__c orderAttachments { get; private set; }
    
    // the order protocol details 
    public List<History> historyLog {get; private set;}
    private Map<String,String> orderFieldNames {get; private set; }
    private Map<String,String> translation {get; private set;}
    
    //Used for [Attach File] 
    public String orderName {get;private set;}
    
    public Boolean popup {get; set;}
       
    //Initialize Objects after setting the order id
    public Id oid 
    {
        get; 
        set
        {
            oid = value;
            historyLog = new List<History>();
            // system.debug('Start initializing:' + datetime.Now());
            //PMS 38913/INC0255284: ORD-0000499447 kann nicht aufgerufen werden (AB) 
            // added limit 500
            orderLog = [Select g.UserName__c, toLabel(g.Type__c),toLabel(g.SubType__c), g.Info__c, g.Id,g.CreatedDate, g.CreatedDate__c, g.CreatedBy.Name,g.CreatedById 
            		From SCOrderLog__c g WHERE g.SCOrderId__c=:oid order by g.CreatedDate__c asc limit 500];
            
            //#### GMSNA 05.04.2013 - quick hack
            //#### TODO: remove if the ASE Time synch has been implemented --> ET, HS
            for(SCOrderLog__c item : orderLog)
            {
                item.CreatedDate__c = item.CreatedDate__c.addSeconds(0); //#### assumption - quick and dirty
            }
            
            orderHistory = [Select s.ParentId, s.OldValue, s.NewValue, s.IsDeleted, s.Id, s.Field, s.CreatedDate, s.CreatedBy.Name, s.CreatedById From SCOrder__History s where s.parentID=:oid order by s.CreatedDate asc];
            
            orderAttachments = [Select Name,(Select Id, isNote,Title, CreatedDate, CreatedById, LastModifiedDate, CreatedBy.Name From NotesAndAttachments order by CreatedDate desc) From SCOrder__c s WHERE id = :oid limit 1];
            orderName = orderAttachments.Name;
            
            system.debug('#### order history results -> ' + orderHistory);
            system.debug('#### order attachment results -> ' + orderAttachments);
            initOrderProtocol();
            
            // system.debug('Initializing ready:'+datetime.now());
        }
    }
    
    
    
   /**
    * Default constructor
    */
    public SCOrderProtocolController() 
    {
       popup = true;
    }
    
   /**
    * Used for adding notes
    * @return Prefix of Note Object
    */
    public String getNotePrefix() 
    {
        return Note.SObjectType.getDescribe().getKeyPrefix();
    }
    
   /**
    * Initialize OrderProtocol and basic translation maps (weekends, picklist content) and 
    * merges SCOrderLog__c, SCOrder__History and NoteAndAttachment to public List<History> historyLog
    */ 
    private void initOrderProtocol()
    {
        // initialize the translations only once to prevent describe exceptions
        if(orderFieldNames == null)
        {
            orderFieldNames = new Map<String,String>();
            translation = new Map<String,String>();
            
            //--<Add Weekday translations>-----------------------------------------        
            translation.put('Mon', Label.SC_app_WeekdayMonday);
            translation.put('Tue', Label.SC_app_WeekdayTuesday);
            translation.put('Wed', Label.SC_app_WeekdayWednesday);
            translation.put('Thu', Label.SC_app_WeekdayThursday);
            translation.put('Fri', Label.SC_app_WeekdayFriday);
            translation.put('Sat', Label.SC_app_WeekdaySaturday);
            translation.put('Sun', Label.SC_app_WeekdaySunday);
            
        
            //--<determine the relevant picklist translations>---------------------         
            Map<String,Schema.SObjectField> fieldMap = SCOrder__c.getSObjectType().getDescribe().fields.getMap();
            system.debug('describe fieldMap:' + fieldMap.values());
            
            //Initialize Order Field Names, Picklist Values and Translations
            for(Schema.SObjectField field : fieldMap.values()) 
            {
                Schema.DescribeFieldResult dr = field.getDescribe();
                system.debug('describe field:'+dr);
               
                orderFieldNames.put(dr.getName(), dr.getLabel());
    
                // Translate only picklist values
                if(dr.getType() == Schema.Displaytype.Picklist)
                {
                    system.debug('found Picklist:' + dr.getName());
                    addPicklistTranslations(dr.getPicklistValues(), dr.getName());
                }
                else
                {
                    //not a picklist
                    system.debug('found :' + dr.getType());
                }
            }
            system.debug('size of translation map=' + this.translation.size());
            
        } // if(orderFieldNames       

        //--<Determine the user names>----------------------------------------- 
        Map<String,String> userNameMap = new Map<String,String>();
        for(SCOrderLog__c order : orderLog)
        {
            userNameMap.put(order.UserName__c, order.UserName__c);
        }
        
        List<User> usrList = [Select Name, Alias From User WHERE Alias IN :userNameMap.keySet()];
        
        for(User usr : usrList)
        {
            userNameMap.put(usr.Alias,usr.Name);
        }
         
        //--<read and merge the content>---------------------------------------
        
        List<History> orderLogList=new List<History>();
        List<History> orderHistoryList=new List<History>();
        List<History> orderNoteList=new List<History>();
        
        //Create History Objects for SCOrderLog__c and append to orderLogList
        //for(SCOrderLog__c log: orderLog){
        for(Integer i = orderLog.size()-1 ; i>=0 ; i--)
        {
            SCOrderLog__c log = orderLog.get(i);
            History tmp=new History();
            tmp.createdDate=(log.CreatedDate__c!=null) ? log.CreatedDate__c : log.CreatedDate;
            //tmp.username=(log.Username__c!=null) ? log.UserName__c : log.CreatedBy.name;
            tmp.username=(log.Username__c!=null) ? userNameMap.get(log.UserName__c) : log.CreatedBy.name;
            tmp.userId=log.CreatedById;
            tmp.action=log.Type__c;
            tmp.action=(log.SubType__c!=null) ? tmp.action + ' (' + log.SubType__c + ')' : tmp.action;
            tmp.info= '*' + log.Info__c;
            tmp.weekday=getTranslation(tmp.createdDate.format('E'));
            
            orderLogList.add(tmp);  
        }
        
        //Create History Objects for SCOrder__History and add to orderHistoryList
        //for(SCOrder__History log: orderHistory){
        for(Integer i = orderHistory.size() - 1; i >= 0; i--)
        {
            SCOrder__History log = orderHistory.get(i);
            
            History tmp = new History();
            tmp.createdDate = log.CreatedDate;
            tmp.username = log.CreatedBy.Name;
            tmp.userId = log.CreatedById;
            tmp.action = (orderFieldNames.get(log.Field)!=null) ? orderFieldNames.get(log.Field) : log.Field;
            if(log.NewValue!=null && log.OldValue!=null)
            {
                tmp.info=this.getTranslation(log.Field,log.OldValue+'')+' -> '+this.getTranslation(log.Field,log.NewValue+'');
            }
            else if(log.NewValue!=null)
            {
                tmp.info=this.getTranslation(log.Field,log.NewValue+'');    
            }
            else
            {
                tmp.info='';
            }
            tmp.weekday = getTranslation(tmp.createdDate.format('E'));
            
            orderHistoryList.add(tmp);
        }
        
        //Create History Objects for NoteAndAttachment and append to orderNoteList
        NoteAndAttachment[] atts = orderAttachments.getSObjects('NotesAndAttachments');
        if(atts != null && atts.size() > 0)
        {
            for(NoteAndAttachment log: atts)
            {
                History tmp = new History();
                tmp.createdDate = log.CreatedDate;
                tmp.username = log.CreatedBy.name;
                tmp.userId = log.CreatedById;
                tmp.action = (log.isNote) ? Label.SC_app_Note:Label.SC_app_Attachment;
                tmp.info = log.Title;
                tmp.weekday = getTranslation(tmp.createdDate.format('E'));
                tmp.id = log.id;
                
                orderNoteList.add(tmp); 
            }
        }
        
        //Merge all lists
        // system.debug('#### history log -> ' + orderHistoryList);
        historyLog = mergeList(orderLogList, orderHistoryList);
        // system.debug('#### history log after merge 1-> ' + orderHistoryList);
        historyLog = mergeList(historyLog, orderNoteList);
        // system.debug('#### history log after merge 2-> ' + orderHistoryList);
    }
    
    /**
    * Add PicklistValues to translation map. id=prefix+value
    * @param pList PicklistValues
    * @param prefix e.g. Fieldname
    */
    private void addPicklistTranslations(List<Schema.PicklistEntry> pList, String prefix)
    {
        for(Schema.PicklistEntry entry: pList)
        {
            this.translation.put(prefix+entry.getValue(),entry.getLabel());
            // system.debug('entry-> value:'+entry.getValue()+' label:'+entry.getLabel());
        }
    }
    
    /**
    * Translates a value
    * @param value to translate
    * @return translated value if translateable, otherwise the input value 
    */
    private String getTranslation(String value)
    {
        return getTranslation('', value);
    }
    
   /**
    * Translates a value
    *
    * @param prefix for the value (e.g. fieldname)  
    * @param value to translate
    *
    * @return translated value if translateable, otherwise the input value 
    */
    public String getTranslation(String prefix, String value)
    {
        String label = this.translation.get(prefix+value);
        if(label == null)
        {
            return value;
        }
        return label;
    }
    
    /**
    * Merge Lists already sorted by date 
    * @param listBase a sorted List<History>
    * @param listToMerge a sorted List<History>
    * @return a merged and sorted List<History>
    */
    private List<History> mergeList(List<History> listBase, List<History> listToMerge )
    {
        Integer listBaseI = 0;
        final Integer LIST_BASE_I_MAX = listBase.size()-1;
        Integer listToMergeI = 0;
        final Integer LIST_TO_MERGE_I_MAX = listToMerge.size()-1;
        
        List<History> history = new List<History>();
        while(true)
        {
            //no items available. ready!
            if(listBaseI > LIST_BASE_I_MAX && listToMergeI > LIST_TO_MERGE_I_MAX)
            {
                break;
            }
            
            //no more listBaseItems
            if(listBaseI > LIST_BASE_I_MAX)
            {
                history.add(listToMerge.get(listToMergeI++));
            }
            //no more listToMergeItems
            else if(listToMergeI > LIST_TO_MERGE_I_MAX)
            {
                history.add(listBase.get(listBaseI++));
            }
            //compare
            else
            {
                History lB=listBase.get(listBaseI);
                History lTM=listToMerge.get(listToMergeI);
                if(lB.createdDate > lTM.createdDate)
                {
                    history.add(lB);
                    listBaseI++;
                }
                else
                {
                    history.add(lTM);
                    listToMergeI++;
                }
            }
            //break;
        }
        return history;
    }
    
   /**
    * This class represents one entry for SCOrderProtocol
    *
    */
    public class History
    {
        public DateTime createdDate {get; set;}
        public String username {get; private set;}
        public String action {get; private set; }
        public String info {get; private set;}
        public String weekday{get;private set;}
        public ID id {get; private set; }
        
        //reserved
        public ID userId {get; private set; }
        
       /**
        * formats the date using the locale and the local time zone of the context user
        *
        * @return formatted date
        */
        public String getValueOfDate()
        {
            return createdDate.format('yyyy-MM-dd HH:mm:ss');
        }   
    }
}
