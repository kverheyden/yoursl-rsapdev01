/*
 * @(#)SCboOrderLog.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCboOrderLog
{
    /**
     * Standard constructor
     */
    public SCboOrderLog()
    {
    }
    
    /**
     * Creates aorder log entry with the given data.
     *
     * @param    orderId    id of the order
     * @param    type       type of the log entry
     * @param    subType    subtype of the log entry
     * @param    info       text of the log entry
     */
    public static void createLog(Id orderId, String type, String subType, String info)
    {
        if (null != info)
        {
            if (info.length() > 2000)
            {
                info = info.substring(0, 2000);
            }
        }

        SCOrderLog__c log = new SCOrderLog__c(SCOrderId__c = orderId, 
                                              Type__c = type, 
                                              SubType__c = subType, 
                                              Info__c = info, 
                                              CreatedDate__c = DateTime.now(), 
                                              UserName__c = UserInfo.getName());
        insert log;
    } // createLog
} // class SCboOrderLog
