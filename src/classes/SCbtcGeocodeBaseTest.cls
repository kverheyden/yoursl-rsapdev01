/*
 * @(#)SCbtcGeocodeBaseTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * @author Sebastian Schrage
 */
@isTest
private class SCbtcGeocodeBaseTest
{
    static testMethod void testCodeCoverageA()
    {
        try { SCHelperTestClass3.createCustomSettings('DE',true); } catch(Exception e) {}
        try { SCHelperTestClass.createDomsForOrderCreation(); } catch(Exception e) {}
        
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        
        Account acc = new Account(Name = 'test Account Name');
        Database.insert(acc);
        
        
        Test.startTest();
        
        
        try { SCbtcGeocodeBase.isEven(1); } catch(Exception e) {}
        try { SCbtcGeocodeBase.isEven(2); } catch(Exception e) {}
        try { SCbtcGeocodeBase.isOdd(1); } catch(Exception e) {}
        try { SCbtcGeocodeBase.isOdd(2); } catch(Exception e) {}
        try { SCbtcGeocodeBase.isDoubleCall('GB'); } catch(Exception e) {}
        try { SCbtcGeocodeBase.isDoubleCall('NL'); } catch(Exception e) {}
        try { SCbtcGeocodeBase.isDoubleCall('DE'); } catch(Exception e) {}
        
        
        AvsAddress addr0 = new AvsAddress();
        addr0.geoX = 0;
        addr0.geoY = 0;
        AvsAddress addr1 = new AvsAddress();
        addr1.geoX = 50;
        addr1.geoY = 50;
        AvsResult res1 = new AvsResult();
        res1.items = new List<AvsAddress>{addr0,addr1};
        AvsResult res2 = new AvsResult();
        
        SCbtcGeocodeBase.GeoCodeHolder holder = new SCbtcGeocodeBase.GeoCodeHolder();
        
        SCbtcGeocodeBase obj = new SCbtcGeocodeBase();
        
        try { obj.getAddressDataForResultInfo(true,addr1,res1,res1,0); } catch(Exception e) {}
        try { obj.getAddressDataForResultInfo(true,addr1,res1,res1,1); } catch(Exception e) {}
        try { obj.getAddressDataForResultInfo(true,addr1,res1,null,0); } catch(Exception e) {}
        try { obj.getAddressDataForResultInfo(false,addr1,res1,res1,0); } catch(Exception e) {}
        try { obj.getAddressDataForResultInfo(false,addr1,res1,res1,1); } catch(Exception e) {}
        try { obj.getAddressDataForResultInfo(false,addr1,res1,null,0); } catch(Exception e) {}
        
        obj.processingMode = 'SIM';
        
        try { obj.getAddressDataForResultInfo(true,addr1,res1,res1,0); } catch(Exception e) {}
        try { obj.getAddressDataForResultInfo(true,addr1,res1,res1,1); } catch(Exception e) {}
        try { obj.getAddressDataForResultInfo(true,addr1,res1,null,0); } catch(Exception e) {}
        try { obj.getAddressDataForResultInfo(false,addr1,res1,res1,0); } catch(Exception e) {}
        try { obj.getAddressDataForResultInfo(false,addr1,res1,res1,1); } catch(Exception e) {}
        try { obj.getAddressDataForResultInfo(false,addr1,res1,null,0); } catch(Exception e) {}
        
        try { obj.getEmailGeoPrefix('test kindFormat', null, 'test fieldSeparator', false); } catch(Exception e) {}
        try { obj.getEmailGeoPrefix('test kindFormat', acc.Id, 'test fieldSeparator', false); } catch(Exception e) {}
        try { obj.getEmployeePrefix('test employee', 'test fieldSeparator'); } catch(Exception e) {}
        
        try { obj.prepareSetGeoCode(true, true, addr0, res1, res1, holder); } catch(Exception e) {}       
        try { obj.prepareSetGeoCode(false, false, addr0, res1, res1, holder); } catch(Exception e) {}       
        
        obj.methodForCodeCoverage(obj, addr0, addr1, res1, res2);
        
        Test.stopTest();
    }
}
