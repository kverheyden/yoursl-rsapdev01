@isTest
private class CCWCRestCustomerRequestTest {
	
	static testMethod void testCustomerRequestPost() {
		String requestJSON = 
		
		'{' +
		'  "PricingStartDate__c" : "2014-09-02",' + 
		'  "PricingShipmentQuantityDiscount__c" : "a2ic00000008v5tAAA",' + 
		'  "RecordTypeId" : "012c00000004bxSAAQ",' + 
		'  "CentralEffort__c" : false,' + 
		'  "PricingAnnualOrderDiscount__c" : "a2fc00000006E4ZAAU",' + 
		'  "PricingCaseCompensation__c" : true,' + 
		'  "PricingUnitThroughputDiscount__c" : "a2lc00000015OpyAAE",' + 
		'  "PaymentDirectDebit__c" : true,' + 
		'  "PricingSponsoringDiscount__c" : "a2kc000000094HhAAI",' + 
		'  "NumberofAttachments__c" : 1,' + 
		'  "PricingCustomerOwnedUnit__c" : false,' + 
		'  "IntroductionCateringBottle__c" : true,' + 
		'  "PricingWaterConceptDiscount__c" : "a2mc0000000E13BAAS"' + 
		'}';
		
		RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        
        request.requestURI = '/CustomerRequest/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(requestJSON);
        
		System.assertEquals(requestJSON, request.requestBody.toString());
		
		RestContext.request = request;
        RestContext.response = response;		
		
		CCWCRestCustomerRequest.doPost();
		System.debug('RESPONSE: ' + RestContext.Response.ResponseBody.toString());
		List<Request__c> insertedRequest = [SELECT OutletCompanyName__c FROM Request__c LIMIT 1];
		System.debug('JM Debug request inserted:' + insertedRequest);
		

	}
	
	static testMethod void testCustomerRequestPostWithExternalId() {
		Request__c requ = new Request__c();
		requ.ExternalId__c = 'A8234-89023-123123';
		insert requ;
		
		String requestJSON = 
		
		'{' +
		'  "ExternalId__c" : "123133",' + 
		'  "Nonsense" : "this field does not exist",' + 
		'  "PricingStartDate__c" : "2014-09-02",' + 
		'  "PricingShipmentQuantityDiscount__c" : "a2ic00000008v5tAAA",' + 
		'  "RecordTypeId" : "012c00000004bxSAAQ",' + 
		'  "CentralEffort__c" : false,' + 
		'  "PricingAnnualOrderDiscount__c" : "a2fc00000006E4ZAAU",' + 
		'  "PricingCaseCompensation__c" : true,' + 
		'  "PricingUnitThroughputDiscount__c" : "a2lc00000015OpyAAE",' + 
		'  "PaymentDirectDebit__c" : true,' + 
		'  "PricingSponsoringDiscount__c" : "a2kc000000094HhAAI",' + 
		'  "NumberofAttachments__c" : 1,' + 
		'  "PricingCustomerOwnedUnit__c" : false,' + 
		'  "IntroductionCateringBottle__c" : true,' + 
		'  "PricingWaterConceptDiscount__c" : "a2mc0000000E13BAAS"' + 
		'}';
		
		RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        
        request.requestURI = '/CustomerRequest/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(requestJSON);
        
		System.assertEquals(requestJSON, request.requestBody.toString());
		
		RestContext.request = request;
        RestContext.response = response;		
		
		CCWCRestCustomerRequest.doPost();
		System.debug('RESPONSE: ' + RestContext.Response.ResponseBody.toString());
		List<Request__c> insertedRequest = [SELECT OutletCompanyName__c FROM Request__c LIMIT 1];
		System.debug('JM Debug request inserted:' + insertedRequest);
		

	}
	
	
}
