/*
 * @(#)SCMapViewData.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
* @author Eugen Tiessen <etiessen@gms-online.de>
* @version $Revision$, $Date$
*/
public class SCMapViewTour {
	
	static final String[] hexCode = new String[]{'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
	
	static Integer lastColor = 23;
	
	public Date tourDate 
	{
		get;
		set
		{
			tourDate = value;
			this.tourDateString =  this.getFormattedTourDate(); 
		}
	}
	public String tourDateString {get; private set;}
	
	public SCMapViewMarker startWaypoint {get; set;}
	public SCMapViewMarker endWaypoint {get; set;}
	public List<SCMapViewMarker> waypoints {get; set;}
	public List<SCMapViewMarker.LocationPoint> points {get; set;}
	
	//<orderId,tourItem>
	public Map<String,SCMapViewTourItem> tourItemsMap {get; set;}
	public List<SCMapViewTourItem> tourItemsList {get; set;}
	
	//only for visualizing
	public SCMapViewTourItem engineerTourItem {get;set;}
	public String engineer {get; set;}
	public String engineerId {get; set;}
	
	public Boolean hasOrder {get; set;}
	
	private Integer countOrderItems = 0;
	

	public String color {get; set;}
	public String tourIndex {get; set;}
	
	public SCMapViewTour(Boolean autoColor){
		waypoints = new List<SCMapViewMarker>();
		points = new List<SCMapViewMarker.LocationPoint>();
		this.tourItemsList = new List<SCMapViewTourItem>();
		this.tourItemsMap = new Map<String,SCMapViewTourItem>();
		this.color=this.getNextColor();
		this.hasOrder = false;
	}
	
	public SCMapViewTour(String color){
		waypoints = new List<SCMapViewMarker>();
		points = new List<SCMapViewMarker.LocationPoint>();
		this.tourItemsList = new List<SCMapViewTourItem>();
		this.tourItemsMap = new Map<String,SCMapViewTourItem>();
		this.color=color;
		this.hasOrder = false;
	}
	
	public SCMapViewTour(){
		waypoints = new List<SCMapViewMarker>();
		points = new List<SCMapViewMarker.LocationPoint>();
		this.tourItemsList = new List<SCMapViewTourItem>();
		this.tourItemsMap = new Map<String,SCMapViewTourItem>();
		color = SCMapViewMarker.DEFAULT_COLOR;
		this.hasOrder = false;

	}
	
	public void setColor(String color){
		this.color = color;
		
		if(this.startWaypoint != null)
			this.startWaypoint.color = this.color;
		
		if(this.endWaypoint != null)
			this.endWaypoint.color = this.color;
			
		for(SCMapViewMarker marker: this.waypoints)
		{
			marker.color = this.color;
		}
	}
	
	public String getColor(){
		return this.color;
	}
	
	public void setAutoColor(Boolean auto){
		if(auto)
			this.setColor(this.getNextColor());
		else
			this.setColor(SCMapViewMarker.DEFAULT_COLOR);
	}
		
	
	public String getNextColor(){
		lastColor += 23;
		String myColor='#';
		for(Integer i=0; i<3; i++ )
		{
			
			Integer state = Math.mod(lastColor + i,3);
			if(state == 0)
			{
				myColor += '00';
				continue;
			}
			else if(state == 1)
			{
				//if(Math.mod(lastColor + i,6)==1)
				if(i == 1)
				{
					myColor += 'AA';
				}
				else
				{
					myColor += 'AA';
				}
				
				continue;
			}
				
			//linear part
			//myColor += hexCode[(Integer)(Math.mod( lastColor,16)) ];
			
			//generic part
			//myColor += hexCode[(Integer)(Math.random()*16)];
			//myColor += hexCode[(Integer)(Math.random()*16)];
			myColor += hexCode[(lastColor & 240) >> 4];
			myColor += hexCode[lastColor & 15];
		}
		return myColor;
		//return '#' + ((Integer)(Math.random()*100)) + ((Integer)(Math.random()*100)) + ((Integer)(Math.random()*100));
		//return '#' + (36+(Integer)(lastColor*Math.random()) & 63) + (36+(Integer)(lastColor*Math.random()) & 63) + (36+(Integer)(lastColor*Math.random()) & 63);
		//return '#'+Math.mod(99,lastColor)+Math.mod(99,lastColor)+Math.mod(99,lastColor);
	}
	
	public void setStartWaypoint (Double lat, Double lng, String content){
		//SCMapViewMarker.Point point=new SCMapViewMarker.Point(lat, lng);
		//startWaypoint= new SCMapViewMarker(point,content,SCMapViewMarker.ENGINEER_ICON,color);
		setStartWaypoint (lat, lng, content,color);
	}
	public void setStartWaypoint (Double lat, Double lng, String content,String color){
		SCMapViewMarker.Point point=new SCMapViewMarker.Point(lat, lng);
		startWaypoint= new SCMapViewMarker(point,content,SCMapViewMarker.ENGINEER_ICON,color);
		
	}
	
	public void setStartWaypoint (SCMapViewMarker marker){
		marker.color = color;	
		startWaypoint = marker;
	}
	
	public void setEndWaypoint(Double lat, Double lng, String content){
		SCMapViewMarker.Point point=new SCMapViewMarker.Point(lat, lng);
		endWaypoint= new SCMapViewMarker(point,content,SCMapViewMarker.ENGINEER_ICON,color);
		
	}
	 
	public void setEndWaypoint (SCMapViewMarker marker){
		marker.color = color;	
		endWaypoint = marker;
	}
	
	public void addWaypoint(Double lat, Double lng, String content){
		/*SCMapViewMarker.Point point=new SCMapViewMarker.Point(lat, lng);
		SCMapViewMarker waypoint=new SCMapViewMarker(point,content,SCMapViewMarker.ORDER_ICON,color);
		waypoints.add(waypoint);
		
		points.add(new SCMapViewMarker.LocationPoint(point));*/
		this.addWaypoint(lat,lng,content,null);
	}
	
	public void addWaypoint(Double lat, Double lng, String content, String textIcon){
		SCMapViewMarker.Point point=new SCMapViewMarker.Point(lat, lng);
		SCMapViewMarker waypoint=new SCMapViewMarker(point,content,SCMapViewMarker.ORDER_ICON,color);
		
		if(textIcon != null)
			waypoint.textIcon = textIcon;
			
		waypoints.add(waypoint);
		
		points.add(new SCMapViewMarker.LocationPoint(point));
	}
	
	public void addWaypoint(SCMapViewMarker marker){
		marker.color = color;	
		waypoints.add(marker);
		points.add(new SCMapViewMarker.LocationPoint(marker.point));
	}
	
	public void addTourItem(SCMapViewTourItem item){
		if(item.itemtype == 'ORD' && item.orderId != null)
		{
			this.hasOrder = true;
			item.orderIndex = ++this.countOrderItems;
			this.tourItemsMap.put(item.orderId + '_' + item.startTimeNotFormatted,item);
		}
		else if(item.itemtype == 'ENG' && item.engineerId != null)
		{
			this.tourItemsMap.put(item.engineerId,item);
		}
			
		this.tourItemsList.add(item);
		
	}
	
	public SCMapViewTourItem getTourItem(String orderId){
		return this.tourItemsMap.get(orderId);
	}
	
	public String getFormattedTourDate()
	{
		return this.tourDate.format();
	}
	
	/**
	 * Compare SCMapViewTour Objects
	 */
	public Integer compareTo(SCMapViewTour tour)
	{
		Integer nameCompare = this.engineer.compareTo(tour.engineer);
		if(nameCompare != 0)
			return nameCompare;
		
		return (this.tourDate < tour.tourDate) ? -1 : 1;
			
	}
	
	
	
	
		
	
}
