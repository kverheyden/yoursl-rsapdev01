/*
 * @(#)SCOrderRecallExtension.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * This extension is used to create an order recall.
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCOrderRecallExtension 
{
    public SCboOrder boOrder {get; set;}
    public SCOrderRole__c roleRE {get; set;}
    public SCboOrderItem boOrderItem {get; set;}
    public SCOrder__c newOrder {get; set;}
    public Boolean canRecall {get; private set;}
    public Boolean recallNotValid {get; private set;}
    public String empty {get; set;}
    public SCTimeReport__c lastTimeReport {get; set;}
    public String errMsg {get; set;}
    
    
    public SCOrderRecallExtension(ApexPages.StandardController controller) 
    {
        empty = '';
        errMsg = null;
        canRecall = true;
        recallNotValid = false;
        newOrder = new SCOrder__c();
        boOrder = new SCboorder();
        boOrder.readById(controller.getId());

        lastTimeReport = boOrder.getLastEmployee();
        
        roleRE = boOrder.getRole(SCfwConstants.DOMVAL_ORDERROLE_RE);
        boOrderItem = ((null != boOrder.boOrderItems) && (boOrder.boOrderItems.size() > 0)) ? boOrder.boOrderItems[0] : null;
        
        validate();
    }
    
   /*
    * Checks if we can create a recall.
    */
    private void validate()
    {
        // 1. Check the order status
        if ((SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED != boOrder.order.Status__c) && 
            (SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL != boOrder.order.Status__c))
        {
            canRecall = false;
        }

        // 2. Check the order type
        if ((SCfwConstants.DOMVAL_ORDERTYPE_INHOUSE == boOrder.order.Type__c) ||  
            (SCfwConstants.DOMVAL_ORDERTYPE_SALESORDER == boOrder.order.Type__c) || 
            (SCfwConstants.DOMVAL_ORDERTYPE_CONSULTING == boOrder.order.Type__c) || 
            (SCfwConstants.DOMVAL_ORDERTYPE_COMISSIONING == boOrder.order.Type__c) || 
            (SCfwConstants.DOMVAL_ORDERTYPE_SPAREPARTSALES == boOrder.order.Type__c))
        {
            canRecall = false;
        }

         // if (canRecall && (null != lastTimeReport) && ...
    } // validate
    
   /*
    * Retrieves the address of the location as string.
    */
    public String getLocationAddress() 
    {
        if ((null != boOrderItem) && (null != boOrderItem.orderitem))
        {
            return boOrderItem.orderitem.InstalledBase__r.InstalledBaseLocation__r.Address__c + '<BR/>' + '<BR/>' +
                   'Lati: ' + boOrderItem.orderitem.InstalledBase__r.InstalledBaseLocation__r.GeoY__c  + ' / ' + 
                   'Long:' + boOrderItem.orderitem.InstalledBase__r.InstalledBaseLocation__r.GeoX__c;   
        }
        return '';           
    } 

   /*
    * Checks the lock type and risc class of the account.
    */
    public Boolean getIsCustomerLocked()
    {
        /*
        public final static String DOMVAL_LOCKTYP_CASH = '50002';
        public final static String DOMVAL_LOCKTYP_LOCKED = '50003';
        DOMVAL_RISKCLASS_NONE = '51001';
        DOMVAL_RISKCLASS_REMINDERS = '51002';
        */
        return ((null != roleRE) && 
                (null != roleRE.Account__r) && 
                (((null != roleRE.Account__r.LockType__c) && 
                  (SCfwConstants.DOMVAL_LOCKTYP_NONE != roleRE.Account__r.LockType__c)) || 
                 ((null != roleRE.Account__r.RiskClass__c) && 
                  (SCfwConstants.DOMVAL_RISKCLASS_NONE != roleRE.Account__r.RiskClass__c))));
    } // getIsCustomerLocked
    
   /*
    * Creates a new order, save recall infos in the new order and open the order creation page.
    */
    public PageReference onSave()
    {
        PageReference page = null;
        errMsg = null;

        if (!SCBase.isSet(newOrder.RecallReason__c))
        {
            errMsg = System.Label.SC_msg_RecallNoReason;
        }
        else if (('other' == newOrder.RecallReason__c) && 
                 !SCBase.isSet(newOrder.RecallReasonDescription__c))
        {
            errMsg = System.Label.SC_msg_RecallNoDescription;
        }
        else
        {
            SCboOrder recallOrder = boOrder.copyOrder();
            
            recallOrder.order.OrderOrigin__c = boOrder.order.Id;
            recallOrder.order.OrderOrigin__r = boOrder.order;
            recallOrder.save();
            
            recallOrder.order.RecallReason__c = newOrder.RecallReason__c;
            recallOrder.order.RecallReasonDescription__c = newOrder.RecallReasonDescription__c;
            if (null != lastTimeReport)
            {
                recallOrder.order.RecallDays__c = lastTimeReport.End__c.date().daysBetween(Date.today());
                recallOrder.order.RecallLastEmployee__c = lastTimeReport.Resource__r.Employee__c;
            }
            recallOrder.order.Info__c = System.Label.SC_msg_RecallOfOrder + ' ' + boOrder.order.Name;
            recallOrder.save();
            
            SCOrderRole__c roleLE = recallOrder.getRole(SCfwConstants.DOMVAL_ORDERROLE_LE);
            SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
            String url = '/apex/' + appSettings.ORDERCREATION_PAGE__c + '?aid=' + roleLE.Account__c + '&oid=' + recallOrder.order.id;
            if (null != lastTimeReport)
            {
                url += '&emplId=' + lastTimeReport.Resource__r.Employee__c;
            }
            page = new PageReference(url);
        }
        return page;
    } // onSave
}
