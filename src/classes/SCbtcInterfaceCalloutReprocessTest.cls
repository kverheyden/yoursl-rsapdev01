/*
 * @(#)SCbtcInterfaceCalloutReprocessTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Test class checking the calculating a due date, price list for maintenances
 * @version $Revision$, $Date$
 */
@isTest 
private class SCbtcInterfaceCalloutReprocessTest 
{
    private static Boolean seeAllData = CCWCTestBase.isSeeingAllData();



    public static testMethod void testCheckErrorMessage() 
    {
        Test.StartTest();
        
        boolean result1 = SCbtcInterfaceCalloutReprocess.checkErrorMessage('Error: Fehler bei HTTP-Zugriff: IF_HTTP_CLIENT->RECEIVE 1 HTTPIO_PLG_CANCELED');
        
        System.assertEquals(true, result1);
        
        boolean result2 = SCbtcInterfaceCalloutReprocess.checkErrorMessage('Blubb');
        
        System.assertEquals(false, result2);
        
        Test.stopTest();
    }   

 
     public static testMethod void testNichtAusloesen() 
    {
    	prepareDataNoCallout();
    	
        List<sObject> scope = [select id,Name,ID2__c, Data__c,CalloutReprocessed__c,Direction__c,Error_fixed__c,Error_fix_comment__c,IDoc__c,InterfaceHandler__c,
			Interface__c,Maintenance__c,MaterialMovement__c,MessageID__c,Order__c,ReferenceID2__c,ReferenceID__c,
			Replenishment__c,ResourceAssignment__c,Response__c,ResultCode__c,ResultInfo__c,Start__c,Stock__c,Type__c
         	 from SCInterfaceLog__c where ID2__c = 'testId2'];
         	
        Test.StartTest();
        
        SCbtcInterfaceCalloutReprocess icr = new SCbtcInterfaceCalloutReprocess();
        
        
        icr.executeCore(scope);
        
        List<SCInterfaceLog__c> items = [select id,name,ID2__c,CalloutReprocessed__c  from SCInterfaceLog__c where ID2__c = 'testId2'];
         
        System.assertEquals(true, items[0].CalloutReprocessed__c );
         
        Test.stopTest();
    } 
    
     
     public static testMethod void testIdocAusloesen() 
    {
    	prepareDataCalloutIdoc();
    	
        List<sObject> scope = [select id,Name,ID2__c, Data__c,CalloutReprocessed__c,Direction__c,Error_fixed__c,Error_fix_comment__c,IDoc__c,InterfaceHandler__c,
			Interface__c,Maintenance__c,MaterialMovement__c,MessageID__c,Order__c,ReferenceID2__c,ReferenceID__c,
			Replenishment__c,ResourceAssignment__c,Response__c,ResultCode__c,ResultInfo__c,Start__c,Stock__c,Type__c
         	 from SCInterfaceLog__c where ID2__c = 'testId2-idoc'];
         	
        Test.StartTest();
        
        SCbtcInterfaceCalloutReprocess icr = new SCbtcInterfaceCalloutReprocess();
        
        
        icr.executeCore(scope);
        
        List<SCInterfaceLog__c> items = [select id,name,ID2__c,CalloutReprocessed__c,Error_fixed__c  from SCInterfaceLog__c where ID2__c = 'testId2-idoc'];
         
        System.assertEquals(true, items[0].CalloutReprocessed__c );
        System.assertEquals(true, items[0].Error_fixed__c );
         
        Test.stopTest();
    } 
    
    public static testMethod void testCCWCOrderCreateAusloesen() 
    {
    	prepareDataCallout('CCWCOrderCreate');
    	
        List<sObject> scope = [select id,Name,ID2__c, Data__c,CalloutReprocessed__c,Direction__c,Error_fixed__c,Error_fix_comment__c,IDoc__c,InterfaceHandler__c,
			Interface__c,Maintenance__c,MaterialMovement__c,MessageID__c,Order__c,ReferenceID2__c,ReferenceID__c,
			Replenishment__c,ResourceAssignment__c,Response__c,ResultCode__c,ResultInfo__c,Start__c,Stock__c,Type__c
         	 from SCInterfaceLog__c where ID2__c = 'testId2-callout'];
         	
        Test.StartTest();
        
        SCbtcInterfaceCalloutReprocess icr = new SCbtcInterfaceCalloutReprocess();
        
        
        icr.executeCore(scope);
        
        List<SCInterfaceLog__c> items = [select id,name,ID2__c,CalloutReprocessed__c,Error_fixed__c  
        	from SCInterfaceLog__c where ID2__c = 'testId2-callout'];
         
        System.assertEquals(true, items[0].CalloutReprocessed__c );
        //System.assertEquals(true, items[0].Error_fixed__c );
         
        Test.stopTest();
    } 
    
    
 
    public static testMethod void testMuster() 
    {
        Test.StartTest();
        Test.stopTest();
    }   

    public static void prepareDataNoCallout() 
    {
		SCOrder__c ordNew = new SCOrder__c();
		ordNew.ID2__c = 'testOrd';
		ordNew.ERPOrderNo__c = null;
		ordNew.ERPStatusOrderCreate__c = 'ok';
		insert ordNew;
		
		
		SCInterfaceLog__c iflog = new SCInterfaceLog__c();
		
		iflog.Data__c = 'test Data';
	    iflog.ID2__c = 'testId2';
		iflog.CalloutReprocessed__c = false; 
		iflog.Direction__c = 'outbound';
		iflog.Error_fixed__c = false;
		iflog.Error_fix_comment__c = '';
	
		iflog.IDoc__c = null;
		iflog.InterfaceHandler__c = 'ARCHIVE_DOCUMENT_INSERT';
		iflog.Interface__c = 'CCWCArchiveDocumentInsert';
		iflog.Maintenance__c = null;
		iflog.MaterialMovement__c = null;
		iflog.MessageID__c = 'dau89zfdiuruzpöbvpgikuböow';
		//iflog.Name = 'test';
		iflog.Order__c = ordNew.id;
		iflog.ReferenceID2__c = '';
		iflog.ReferenceID__c = null;
        iflog.Replenishment__c = null;
        iflog.ResourceAssignment__c = null;
        iflog.Response__c = null;
        iflog.ResultCode__c = 'E100';
        iflog.ResultInfo__c = 'Error Text matched nicht!';
        iflog.Start__c = date.today();
        iflog.Stock__c = null;
        iflog.Type__c = '';

		insert iflog;
		//FROM SCInterfaceLog__c where ' +
		//		' ResultCode__c in (\'E100\',\'E101\',\'E102\',\'E103\') ' +
		//		' and Error_fixed__c = false ' +
		//		' and CalloutReprocessed__c = false';
    } 
    
    public static void prepareDataCalloutIdoc() 
    {
		SCOrder__c ordNew = new SCOrder__c();
		ordNew.ID2__c = 'testOrd';
		ordNew.ERPOrderNo__c = null;
		ordNew.ERPStatusOrderCreate__c = 'ok';
		insert ordNew;
		
		
		SCInterfaceLog__c iflog = new SCInterfaceLog__c();
		
		iflog.Data__c = 'test Data';
	    iflog.ID2__c = 'testId2-idoc';
		iflog.CalloutReprocessed__c = false; 
		iflog.Direction__c = 'outbound';
		iflog.Error_fixed__c = false;
		iflog.Error_fix_comment__c = '';
	
		iflog.IDoc__c = '0123456789';
		iflog.InterfaceHandler__c = 'ARCHIVE_DOCUMENT_INSERT';
		iflog.Interface__c = 'CCWCArchiveDocumentInsert';
		iflog.Maintenance__c = null;
		iflog.MaterialMovement__c = null;
		iflog.MessageID__c = 'dau89zfdiuruzpöbvpgikuböow';
		//iflog.Name = 'test';
		iflog.Order__c = ordNew.id;
		iflog.ReferenceID2__c = '';
		iflog.ReferenceID__c = null;
        iflog.Replenishment__c = null;
        iflog.ResourceAssignment__c = null;
        iflog.Response__c = null;
        iflog.ResultCode__c = 'E100';
        iflog.ResultInfo__c = 'IO Exception:';
        iflog.Start__c = date.today();
        iflog.Stock__c = null;
        iflog.Type__c = '';

		insert iflog;
		//FROM SCInterfaceLog__c where ' +
		//		' ResultCode__c in (\'E100\',\'E101\',\'E102\',\'E103\') ' +
		//		' and Error_fixed__c = false ' +
		//		' and CalloutReprocessed__c = false';
    } 
    
    
    public static void prepareDataCallout(String interfaceName)
    {
    	SCOrder__c ordNew = new SCOrder__c();
    	ordNew.ID2__c = 'testOrd';
		ordNew.ERPOrderNo__c = null;
    	if(interfaceName != 'CCWCOrderCreate')
    	{
			ordNew.ERPStatusOrderCreate__c = 'ok';	
    	}
    	else
    	{
    		ordNew.ERPStatusOrderCreate__c = 'error';
    	}
    	
    	insert ordNew;
		
		
		SCInterfaceLog__c iflog = new SCInterfaceLog__c();
		
		iflog.Data__c = 'test Data';
	    iflog.ID2__c = 'testId2-callout';
		iflog.CalloutReprocessed__c = false; 
		iflog.Direction__c = 'outbound';
		iflog.Error_fixed__c = false;
		iflog.Error_fix_comment__c = '';
	
		iflog.IDoc__c = null;
		iflog.InterfaceHandler__c = 'ARCHIVE_DOCUMENT_INSERT';
		iflog.Interface__c = interfaceName;
		iflog.Maintenance__c = null;
		iflog.MaterialMovement__c = null;
		iflog.MessageID__c = 'dau89zfdiuruzpöbvpgikuböow';
		//iflog.Name = 'test';
		iflog.Order__c = ordNew.id;
		iflog.ReferenceID2__c = '';
		iflog.ReferenceID__c = null;
        iflog.Replenishment__c = null;
        iflog.ResourceAssignment__c = null;
        iflog.Response__c = null;
        iflog.ResultCode__c = 'E100';
        iflog.ResultInfo__c = 'IO Exception:';
        iflog.Start__c = date.today();
        iflog.Stock__c = null;
        iflog.Type__c = '';


		insert iflog;
    }
    
    /*public static testMethod void testConstructors()
    {
    	Test.StartTest();
    	//SCbtcInterfaceCalloutReprocess obj1 = SCbtcInterfaceCalloutReprocess(1, 'test');
    	SCbtcInterfaceCalloutReprocess obj2 = SCbtcInterfaceCalloutReprocess('0',1, 'test');
    	Test.stopTest();
		
		//System.assertNotEquals(obj1, null);
		System.assertNotEquals(obj2, null);
    }*/
    
    public static testMethod void testStartMethodNoIdoc() 
    {
        SCInterfaceLog__c ifLog = new SCInterfaceLog__c();
        ifLog.ResultCode__c = 'E101';
        ifLog.InterfaceHandler__c = 'CCWCOrderCreate';
        
        ifLog.ResultInfo__c = 'Aktion CANCEL nicht erlaubt für IDoc 0000000107650783 Error:fillAndSendERPData: Call ws.IDocReprocessingRequestResponse_Out(MessageHeader, IDocNumber, PartnerNumber, PartnerType, Action) : Cancel or resend failed with maxSevCode3 messageHeaderBusiness';
        
        insert ifLog;
        
        
        Test.StartTest();
        
		ID id1 =  SCbtcInterfaceCalloutReprocess.asyncProcessAll(1,'test');

		Test.stopTest();
		
		System.assertNotEquals(id1, null);
        
    }
    
       
    public static testMethod void testOrderCreate() 
    {
        SCInterfaceLog__c ifLog = new SCInterfaceLog__c();
        ifLog.ResultCode__c = 'E101';
        ifLog.InterfaceHandler__c = 'CCWCOrderCreate';
        ifLog.IDoc__c = null;        
        ifLog.ResultInfo__c = 'HTTP-Fehler:';       
        insert ifLog;
               
        Test.StartTest();        
		List<SCInterfaceLog__c> l =  SCbtcInterfaceCalloutReprocess.syncProcess(ifLog.id,'test');
		Test.stopTest();		
		System.assertNotEquals(l, null);
    }
    
    public static testMethod void testOrderEquipmentUpdate() 
    {
        SCInterfaceLog__c ifLog = new SCInterfaceLog__c();
        ifLog.ResultCode__c = 'E101';
        ifLog.InterfaceHandler__c = 'CCWCOrderEquipmentUpdate';
        ifLog.IDoc__c = null;        
        ifLog.ResultInfo__c = 'HTTP-Fehler:';       
        insert ifLog;
               
        Test.StartTest();        
		List<SCInterfaceLog__c> l =  SCbtcInterfaceCalloutReprocess.syncProcess(ifLog.id,'test');
		Test.stopTest();		
		System.assertNotEquals(l, null);
    }
    
    public static testMethod void testOrderClose() 
    {
        SCInterfaceLog__c ifLog = new SCInterfaceLog__c();
        ifLog.ResultCode__c = 'E101';
        ifLog.InterfaceHandler__c = 'CCWCOrderClose';
        ifLog.IDoc__c = null;        
        ifLog.ResultInfo__c = 'HTTP-Fehler:';       
        insert ifLog;
               
        Test.StartTest();        
		List<SCInterfaceLog__c> l =  SCbtcInterfaceCalloutReprocess.syncProcess(ifLog.id,'test');
		Test.stopTest();		
		System.assertNotEquals(l, null);
    }
    
    public static testMethod void testOrderExOpAdd() 
    {
        SCInterfaceLog__c ifLog = new SCInterfaceLog__c();
        ifLog.ResultCode__c = 'E101';
        ifLog.InterfaceHandler__c = 'CCWCOrderExternalOperationAdd';
        ifLog.IDoc__c = null;        
        ifLog.ResultInfo__c = 'HTTP-Fehler:';       
        insert ifLog;
               
        Test.StartTest();        
		List<SCInterfaceLog__c> l =  SCbtcInterfaceCalloutReprocess.syncProcess(ifLog.id,'test');
		Test.stopTest();		
		System.assertNotEquals(l, null);
    }
    

    public static testMethod void testOrderExOpRem() 
    {
        SCInterfaceLog__c ifLog = new SCInterfaceLog__c();
        ifLog.ResultCode__c = 'E101';
        ifLog.InterfaceHandler__c = 'CCWCOrderExternalOperationRem';
        ifLog.IDoc__c = null;        
        ifLog.ResultInfo__c = 'HTTP-Fehler:';       
        insert ifLog;
               
        Test.StartTest();        
		List<SCInterfaceLog__c> l =  SCbtcInterfaceCalloutReprocess.syncProcess(ifLog.id,'test');
		Test.stopTest();		
		System.assertNotEquals(l, null);
    }
    
    
    public static testMethod void testMovementCreate()             
    {
        SCInterfaceLog__c ifLog = new SCInterfaceLog__c();
        ifLog.ResultCode__c = 'E101';
        ifLog.InterfaceHandler__c = 'CCWCMaterialMovementCreate';
        ifLog.IDoc__c = null;        
        ifLog.ResultInfo__c = 'HTTP-Fehler:';       
        insert ifLog;
               
        Test.StartTest();        
		List<SCInterfaceLog__c> l =  SCbtcInterfaceCalloutReprocess.syncProcess(ifLog.id,'test');
		Test.stopTest();		
		System.assertNotEquals(l, null);
    }
    
    public static testMethod void testReservationCreate()             
    {
        SCInterfaceLog__c ifLog = new SCInterfaceLog__c();
        ifLog.ResultCode__c = 'E101';
        ifLog.InterfaceHandler__c = 'CCWCMaterialReservationCreate';
        ifLog.IDoc__c = null;        
        ifLog.ResultInfo__c = 'HTTP-Fehler:';       
        insert ifLog;
               
        Test.StartTest();        
		List<SCInterfaceLog__c> l =  SCbtcInterfaceCalloutReprocess.syncProcess(ifLog.id,'test');
		Test.stopTest();		
		System.assertNotEquals(l, null);
    }

    public static testMethod void testArchive()             
    {
        SCInterfaceLog__c ifLog = new SCInterfaceLog__c();
        ifLog.ResultCode__c = 'E101';
        ifLog.InterfaceHandler__c = 'CCWCArchiveDocumentInsert';
        ifLog.IDoc__c = null;        
        ifLog.ResultInfo__c = 'HTTP-Fehler:';       
        insert ifLog;
               
        Test.StartTest();        
		List<SCInterfaceLog__c> l =  SCbtcInterfaceCalloutReprocess.syncProcess(ifLog.id,'test');
		Test.stopTest();		
		System.assertNotEquals(l, null);
    }
    
    

    public static testMethod void testPackage()             
    {
        SCInterfaceLog__c ifLog = new SCInterfaceLog__c();
        ifLog.ResultCode__c = 'E101';
        ifLog.InterfaceHandler__c = 'CCWCMaterialPackageReceived';
        ifLog.IDoc__c = null;        
        ifLog.ResultInfo__c = 'HTTP-Fehler:';       
        insert ifLog;
               
        Test.StartTest();        
		List<SCInterfaceLog__c> l =  SCbtcInterfaceCalloutReprocess.syncProcess(ifLog.id,'test');
		Test.stopTest();		
		System.assertNotEquals(l, null);
    }
            
        
    public static testMethod void testSelectOrder() 
    {
        SCOrder__c ord = new SCOrder__c();
		ord.ERPOrderNo__c = '123456789';
		ord.ERPStatusOrderCreate__c = 'ok';
		ord.ERPStatusOrderClose__c  = 'none'; 
		ord.ERPStatusEquipmentUpdate__c  = 'none';  
		ord.ERPStatusExternalAssignmentAdd__c  = 'none';   
		ord.ERPStatusExternalAssignmentRem__c  = 'none'; 	
		ord.ERPStatusMaterialMovement__c  = 'none'; 
		ord.ERPStatusMaterialReservation__c  = 'none'; 		
		ord.ERPStatusArchiveDocumentInsert__c   = 'none'; 
        
        
        insert ord;
        
        
        Test.StartTest();
        
		SCOrder__c ord1 = SCbtcInterfaceCalloutReprocess.selcetOrder(ord.id);
		Test.stopTest();
		
		System.assertNotEquals(ord1, null);
        
    }
     
     public static testMethod void testSelectExternalOperationByName() 
    {
        CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        CCWCTestBase.setAppSettingsForOrder(appSettings, seeAllData);
        CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
            ccSettings = CCSettings__c.getInstance();
        }
        CCWCTestBase.setCCSettings(ccSettings, seeAllData);
        
        CCWCTestBase.createDomsForOrderCreation(seeAllData);
        Boolean doUpsert = true;
        SCOrder__c order = CCWCTestBase.createTestOrder(appSettings, doUpsert);
        
        SCOrderExternalAssignment__c ass = new SCOrderExternalAssignment__c();
		ass.ERPOperationID__c = 'test';
		ass.Order__c = order.id;

        insert ass;
             
        SCOrderExternalAssignment__c a = [select name from SCOrderExternalAssignment__c where ERPOperationID__c = 'test' ];   
        Test.StartTest();
        
		SCOrderExternalAssignment__c ass1 = SCbtcInterfaceCalloutReprocess.selectExternalOperationByName(a.name);
		//SCOrderExternalAssignment__c ass2 = SCbtcInterfaceCalloutReprocess.selectExternalOperationByOrderAndStatus(ord.id,s);
		
		Test.stopTest();
		
		System.assertNotEquals(ass1, null);
        
    }

    public static testMethod void testExternalOperationByOrderAndStatus() 
    {
        CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        CCWCTestBase.setAppSettingsForOrder(appSettings, seeAllData);
        CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
            ccSettings = CCSettings__c.getInstance();
        }
        CCWCTestBase.setCCSettings(ccSettings, seeAllData);
        
        CCWCTestBase.createDomsForOrderCreation(seeAllData);
        Boolean doUpsert = true;
        SCOrder__c order = CCWCTestBase.createTestOrder(appSettings, doUpsert);
        
        SCOrderExternalAssignment__c ass = new SCOrderExternalAssignment__c();
		ass.ERPOperationID__c = 'test';
		ass.Order__c = order.id;
             
        Test.StartTest();
        List<String> s = new List<String>();
        s.add('test');
		SCOrderExternalAssignment__c ass1 = SCbtcInterfaceCalloutReprocess.selectExternalOperationByOrderAndStatus(order.id,s);
		Test.stopTest();
		
		System.assertEquals(ass1, null);
        
    }
    
    
    public static testMethod void testSelectMaterialMovments() 
    {
		// initiate and create articles
		SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        List<SCArticle__c> articleList = CCWCTestBase.createTestArticles(appSettings, true);

            SCPlant__c plant = new SCPlant__c(Name = '0359', Info__c = 'Test plant');
            insert plant;
            
            // initiate and create stocks
            List<SCStock__c> stockList = CCWCTestBase.createTestStocks(plant.Id, true);


		SCMaterialMovement__c mm = new SCMaterialMovement__c(); 
        mm.ERPStatus__c = 'none';
        mm.DeliveryNote__c = 'testMat'; 
        mm.Type__c = '1234';
        mm.Article__c = articleList[0].id;
        mm.Stock__c = stockList[0].id;
        mm.Qty__c = 1;
        insert mm;


		List<SCMaterialMovement__c> mml = [select id, name from SCMaterialMovement__c where  DeliveryNote__c =: 'testMat'];
		List<String> ids = new List<String>();
		
		for(SCMaterialMovement__c m : mml)
		{
			ids.add(m.name);
		}
		
        Test.StartTest();

		List<SCMaterialMovement__c> result = SCbtcInterfaceCalloutReprocess.selectMaterialMovments(ids);
		Test.stopTest();
		
		System.assertNotEquals(result.size(), 0);
        
    }
       
    private static void debug(String text)
    {
        System.debug('###...................' + text);
    }

}
