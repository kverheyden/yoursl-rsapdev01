public with sharing class cc_cceag_email_ctrl_Cart {

	public String currentCartID { get; set; }

	public cc_cceag_email_ctrl_Cart() {
		Map<String, String> params = Apexpages.currentPage().getParameters();
		currentCartID = params.get('cartId');
		System.debug(LoggingLevel.INFO, currentCartID);
	}

	public cc_cceag_bean_Cart currentCart {
		get {
			ccrz__E_Cart__c activeCart = cc_cceag_dao_Cart.retrieveCartWithItems(currentCartID);
	        if (activeCart != null)
	            return new cc_cceag_bean_Cart(activeCart);
	        else
	            return null;
		}
		set;
	}

}
