/**
* @author		Austen Buennemann
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				a.buennemann@yoursl.de
*
* @description	Equipment verification of SCInstalledBase__c
*
* @date			14.10.2014
*
* Timeline:
* Name               DateTime                  Description
* Austen Buennemann  14.10.2014 10:38          Class created
*/

public without sharing class CCWCAssetInventoryMobileUpdateRequest extends SCInterfaceExportBase{
    private static SCInstalledBase__c installedBase;
    
    public CCWCAssetInventoryMobileUpdateRequest()
    {
    }
    public CCWCAssetInventoryMobileUpdateRequest(String interfaceName, String interfaceHandler, String refType, String subclassName)
    {
        super(interfaceName, interfaceHandler, refType, subclassName);
    }
    public CCWCAssetInventoryMobileUpdateRequest(String interfaceName, String interfaceHandler, String refType, SCInstalledBase__c equipment)
    {
        installedBase = equipment;
    }
    
    
    @future(callout=true)     
	public static void executecallout(String sibString, boolean testMode){			
										
		SCInstalledBase__c sib = (SCInstalledBase__c) JSON.deserialize(sibString, SCInstalledBase__c.class);
		CCWCAssetInventoryMobileUpdateRequest.callout(sib.Id, false, testMode, sib);
	}
    
    
    /**
     * Prepare to make the web service callout synchrounously or asynchronously (future)
     *
     * ### Set the interface name, its interface handler and a refType for head id in an interface object!
     *
     * @param ibid      SCInstalledBase__c id
     * @param async     true: use a future method to call 
     *                  false: do a synchrounous callout
     * @param testMode true: emulate call
     * @param sib	SCInstalledBase__c;
     */
    public static String callout(String ibid, boolean async, boolean testMode, SCInstalledBase__c sib)
    {
        String interfaceName = 'AssetInventoryMobileUpdate';
        String interfaceHandler = 'CCWCAssetInventoryMobileUpdateRequest';
        String refType = 'SCInstalledBase__c';
        CCWCAssetInventoryMobileUpdateRequest aidr = new CCWCAssetInventoryMobileUpdateRequest(interfaceName, interfaceHandler, refType, sib);
        return aidr.calloutCore(ibid, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCAssetInventoryMobileUpdateRequest');
        
    } 
    
     /**
     * Prepare records to send
     *
     * @param Id SCInstalledBase__c
     * @param retValue Map Object
     * @param testMode if true no call is made, because in the test mode the salesforce framework prevent it.
     */
    public override void readSalesforceData(String installedBaseID, Map<String, Object> retValue, Boolean testMode)
    {
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        retValue.put('ROOT', installedBase);
        debug('SCInstalledBase__c: ' + retValue);
    }
    
    
    /**
     * Fills the send structure and sends the call to SAP
     * 
     * @param dataMap with the order under the key ROOT
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure
     *           message = data sent to ERP
     *           message_v1 = endpoint 
     *           message_v2 = a return value of web service 
     *           message_v3 = a mandatory fields error
     *
     */
    public override Boolean fillAndSendERPData(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs)
    {
        Boolean retValue = true;
        SCInstalledBase__c iBase = (SCInstalledBase__c)dataMap.get('ROOT');
        // instantiate the client of the web service
        piCceagDeSfdcCSAssetInventoryDataChange.AssetInventoryDataChangeRequest_OutPort ws = new piCceagDeSfdcCSAssetInventoryDataChange.AssetInventoryDataChangeRequest_OutPort();
        
        // sets the basic authorization for the web service
        ws.inputHttpHeaders_x = u.getBasicAuth();

        //Test
        //Map<String, String> autMap = new Map<String, String>(); //
        //autMap.put('Authorization: Basic', 'TEST'); //
		//ws.inputHttpHeaders_x = autMap; //

        // sets the endpoint of the web service
        ws.endpoint_x  = u.getEndpoint('AssetInventoryDataRequest');
        
        //Test
        //ws.endpoint_x  = 'http://requestb.in/13qzxbo1'; //  
        
        debug('endpoint: ' + ws.endpoint_x);
        rs.message_v1 = ws.endpoint_x;
		ws.timeout_x = u.getTimeOut();
        String errormessage = '';
        try
        {
            
            // instantiate the body of the call
            piCceagDeSfdcCSAssetInventoryDataChange.AssetHeader_element sfdcAssetHeader = new piCceagDeSfdcCSAssetInventoryDataChange.AssetHeader_element();
            
            List<piCceagDeSfdcCSAssetInventoryDataChange.AssetHeader_element>  assetItems = new List<piCceagDeSfdcCSAssetInventoryDataChange.AssetHeader_element>();
            
            setAssetInventoryDataChange(sfdcAssetHeader, iBase, u, testMode);

            assetItems.add(sfdcAssetHeader);

            String jsonSfdcAssetHeader = JSON.serialize(sfdcAssetHeader);
            String fromJSONMapOrder = getDataFromJSON(jsonSfdcAssetHeader);
            debug('from json Order: ' + fromJSONMapOrder);
            
            rs.message = '\n messageData: ' + sfdcAssetHeader ;
            // check if there are missing mandatory fields
            if(rs.message_v3 == null || rs.message_v3 == '')
            {
                // All mandatory fields are filled
                // callout
                if(!testMode)
                {
                    // It is not the test from the test class
                    // go
                    errormessage = 'Call ws.AssetInventoryMobileUpdate: \n\n';
                    rs.setCounter(1);
                    ws.AssetInventoryDataChangeRequest_Out(assetItems);
                    rs.Message_v2 = 'void';
                }
            }
        }
        catch(Exception e)
        {
            String prevMsg = e.getMessage();
            String newMsg = 'fillAndSendERPData: ' + errormessage + ' \n\n ' + prevMsg;
            debug(newMsg);
            e.setMessage(newMsg);
            throw e;

        }
        return retValue;
    }
        
     /**
     * sets the call out structure with data form
     *
     * @param cso callout order structure
     * @param iBase with input data
     * @param u instance of the util class CCWSUtil
     * @param testMode used to prevent call out by calls from CCWCOrderEquipmentUpdateTest class
     * @return ###
     */
    public String setAssetInventoryDataChange(piCceagDeSfdcCSAssetInventoryDataChange.AssetHeader_element cso, 
                                        SCInstalledBase__c iBase, CCWSUtil u, Boolean testMode)
    {
        CCSettings__c ccset = CCSettings__c.getInstance();
    
        String retValue = '';
        String step = '';
        try 
        {
            step = 'set AssetNumber = iBase.AssetNumber__c';
            cso.AssetNumber = iBase.AssetNumber__c;
                        
            step = 'set AssetSubNumber = iBase.AssetSubNumber__c';
            cso.AssetSubNumber = iBase.AssetSubNumber__c;
            
            step = 'set VerificationDate ';
            cso.VerificationDate = u.formatDate(iBase.AuditLast__c);

            step = 'set Note = iBase.AuditHint__c';
            cso.Note = iBase.AuditHint__c;
            

        }
        catch(Exception e)
        {
            String prevMsg = e.getMessage();
            String newMsg = 'setAssetInventoryMobileUpdate: ' + step + ' ' + prevMsg;
            debug(newMsg);
            e.setMessage(newMsg);
            throw e;
        }
        return retValue;    
    }
    
    /**
     * Sets the ERPStatusEquipmentUpdate__c in the root object to 'pending'
     * 
     * @param dataMap of objects to dispatch
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure with error messages
     * @param error true if some error has occurred
     */
    public override void setERPStatus(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs, Boolean error)
    {
    }

    
    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }


}
