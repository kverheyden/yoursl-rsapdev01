/*
 * @(#)SCStockOptPropItemApproveControllerTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCStockOptPropItemApproveControllerTest
{
    private static SCStockOptPropItemApproveController sopiac;
    
    static testMethod void testPositiv1()
    {
        
        // Prearing the test data
        ID stockId =  SCbtcStockOptimizationTestHelper.prepareTestData_A();
        System.debug('#### stockId: ' + stockId);
        SCbtcStockOptimization.syncOptimizeStock(stockId);
        SCStockOptimizationProposal__c  proposal = SCbtcStockOptimizationTestHelper.readProposal(stockId);
        System.debug('#### proposal: ' + proposal);
        List<SCStockOptimizationProposalItem__c> itemList = SCbtcStockOptimizationTestHelper.readProposalItem(proposal.Id);
        System.debug('#### itemList: ' + itemList);
        List<SCStockItem__c> stockItems = SCbtcStockOptimizationTestHelper.readStockItems(stockId);
        System.debug('#### stockItems: ' + stockItems.size());
        
        ApexPages.currentPage().getParameters().put('id', proposal.Id);
        
        SCStockOptimizationProposal__c pro = [ Select Stock__c, Status__c, Name, Id,
                                                      (Select Id, Name, Proposal__c, StockItem__c, ApprovedQuantity__c 
                                                       From StockOptimizationProposalItems__r
                                                       Order By Name ASC) 
                                               From SCStockOptimizationProposal__c
                                               Where Id = :proposal.id ];
        ApexPages.StandardController controller = new ApexPages.StandardController(pro);
        
        Test.startTest();

        sopiac = new SCStockOptPropItemApproveController(controller);
        
        for(Integer i = 0; i < pro.StockOptimizationProposalItems__r.size(); i++)
        {
            pro.StockOptimizationProposalItems__r[i].ApprovedQuantity__c = 99;
        } 


        System.debug('#### pro.StockOptimizationProposalItems__r.size(): ' + pro.StockOptimizationProposalItems__r.size());
        System.debug('#### sopiac.obj: ' + sopiac.obj);
        System.debug('#### sopiac.obj.StockOptimizationProposalItems__r.size(): ' + sopiac.obj.StockOptimizationProposalItems__r.size());
        
        sopiac.save();
        sopiac.onCancel();
        
        List<SCStockItem__c> newStockItems = new List<SCStockItem__c>([ Select Id, Name, MinQty__c From SCStockItem__c Where ID = :itemList[0].StockItem__c ]);
        SCStockOptimizationProposal__c p = [ Select Status__c From SCStockOptimizationProposal__c Where Id = :proposal.id ]; 
        
        System.assertEquals('Approved', p.Status__c);
        System.assertEquals(99, newStockItems[0].MinQty__c);
        
        Test.stopTest();
    
    }

}
