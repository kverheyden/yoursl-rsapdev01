public with sharing class AttachmentTriggerHandler {
	
	private static Map<Id, Attachment> RequestAttachmentsWithAttachment;

	public class AttachmentAIHandler extends TriggerHandlerBase {
		public override void mainEntry(TriggerParameters tp) {
			createContracts(tp.newList);
			setReadyToSendToSAPOnRequest(tp.newList);
			updateLastModifiedDateOnCustomDocument(tp.newList);
		}
	}

	public class AttachmentAUHandler extends TriggerHandlerBase {
		public override void mainEntry(TriggerParameters tp) {
			updateLastModifiedDateOnCustomDocument(tp.newList);
		}
	}

	public class AttachmentBDHandler extends TriggerHandlerBase {
		public override void mainEntry(TriggerParameters tp) {
			clearContentTypeOnCustomDocument(tp.oldList);
		}
	}

	public static void updateLastModifiedDateOnCustomDocument(List<Attachment> attachments) {
		List<CustomDocument__c> customDocuments = new List<CustomDocument__c>();
		for (Attachment a : attachments) {
			if (a.ParentId.getSObjectType() == CustomDocument__c.SObjectType)
				customDocuments.add(new CustomDocument__c(Id = a.ParentId));
		}
		update customDocuments;
	}

	public static void clearContentTypeOnCustomDocument(List<Attachment> attachments) {
		List<CustomDocument__c> customDocuments = new List<CustomDocument__c>();
		for (Attachment a : attachments) {
			if (a.ParentId.getSObjectType() == CustomDocument__c.SObjectType) 
				customDocuments.add(new CustomDocument__c(Id = a.ParentId, ContentType__c = ''));
		}
		update customDocuments;
	}

	public static void createContracts(List<Attachment> attachments) {
		CCWCPDFCreatorHandler creatorHandler = new CCWCPDFCreatorHandler();
		creatorHandler.UserSessionId = UserInfo.getSessionId();
		creatorHandler.createContractsCallout(attachments);
	}
	
	public static void setReadyToSendToSAPOnRequest(List<Attachment> attachments) {
		requestAttachmentsWithAttachment = getRequestAttachmentIdsWithAttachment(attachments);
		if (RequestAttachmentsWithAttachment.isEmpty())
			return;
		setAttachmentIdOnRequestAttachments();
		
		List<Request__c> requestsToUpdate = new List<Request__c>();
		List<RequestAttachment__c> requAttachments = [SELECT Request__r.Send_to_SAP__c, AttachmentId__c FROM RequestAttachment__c WHERE Id IN: requestAttachmentsWithAttachment.keySet()];
		for (RequestAttachment__c ra: requAttachments) {
			
			Request__c r = [SELECT NumberOfAttachments__c, (SELECT AttachmentId__c, Type__c FROM Request_Attachments__r WHERE AttachmentId__c != NULL) FROM Request__c WHERE Id =: ra.Request__c LIMIT 1];
			if (r.NumberofAttachments__c == r.Request_Attachments__r.size()) {
				ra.Request__r.IsReadyToSendToSAP__c = true;
				requestsToUpdate.add(ra.Request__r);
			}
		}
		update requestsToUpdate;
	}
	
	public static Map<Id, Attachment> getRequestAttachmentIdsWithAttachment(List<Attachment> attachments) {
		Map<Id, Attachment> RequestAttachmentIdsWithAttachment = new Map<Id, Attachment>();
		for (Attachment a : attachments) {
			if (a.ParentId.getSObjectType() == RequestAttachment__c.SObjectType)
				RequestAttachmentIdsWithAttachment.put(a.ParentId, a);
		}
		return RequestAttachmentIdsWithAttachment;
	}
	
	public static void setAttachmentIdOnRequestAttachments() {
		Map<Id, RequestAttachment__c> requestAttachments = new Map<Id, RequestAttachment__c>([
			SELECT Request__r.Send_to_SAP__c, Request__r.NumberOfAttachments__c, AttachmentId__c FROM RequestAttachment__c 
			WHERE Id IN: requestAttachmentsWithAttachment.keySet()]);
		Map<Id, RequestAttachment__c> ras = new Map<Id, RequestAttachment__c>();
	
		List<RequestAttachment__c> requestAttachmentsToUpdate = new List<RequestAttachment__c>();
		for (RequestAttachment__c ra : requestAttachments.values()) {
			Attachment a = requestAttachmentsWithAttachment.get(ra.Id);
			if (a == null)
				continue;
			ra.AttachmentId__c = a.Id;
			requestAttachmentsToUpdate.add(ra);
		}
		update requestAttachmentsToUpdate;
	}
}
