/*
 * @(#)CCWCMaterialReservationCreate.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * The class exports an Salesforce order close information into SAP
 * 
 * The entry method is: 
 *      callout(String plantName, String stockName, String mode, boolean async, boolean testMode)
 *
 * Methods to override: 
 *      fillAndSendERPData(...)
 *      fillAndSendERPData(...)
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
public without sharing class CCWCMaterialReservationCreate extends SCInterfaceExportBase 
{
    public static String MODE_REPLENISHMENT = 'REPLENISHMENT';
    public static String MODE_RESERVATION_FOR_ORDER = 'RESERVATION_FOR_ORDER';
    public static String MODE_RESERVATION_FOR_STOCK = 'RESERVATION_FOR_STOCK';

    public ID orderID = null;
    
    public CCWCMaterialReservationCreate()
    {
    }
    public CCWCMaterialReservationCreate(String interfaceName, String interfaceHandler, String refType, String subclassName)
    {
        super(interfaceName, interfaceHandler, refType, subclassName);
    }
    // If there is a four parameter constructor, Salesforce expects three parameter constructor
    public CCWCMaterialReservationCreate(String interfaceName, String interfaceHandler, String refType)
    {
        super(interfaceName, interfaceHandler, refType, 'Dummy');
    }


    /*
    * Used in the SCbtcClearingPostProcess function to post process the material requisitions
    */
    public static void calloutbyMobileAssignment(String oaid, boolean async, boolean testMode)
    {
        // read the material reservations for this assignment
        List<SCMaterialMovement__c> items = [select id from SCMaterialMovement__c where assignment__c = :oaid 
            and Status__c = '5402'  // MATSTAT_ORDER
            and Type__c = '5202'    // MATREQUEST_EMPL
            and Article__c <> null and Article__r.ERPRelevant__c = true and Article__r.orderable__c = true
            and ERPStatus__c = 'none' and source__c = 'mobile'];
        if(items != null && items.size() > 0)
        {
            List<String> movementids = new List<String>();
            for(SCMaterialMovement__c item : items)
            {
                movementids.add(item.id);
            }
            CCWCMaterialReservationCreate.callout(movementids, async, testMode);
        }    
    }



    /**
     * Prepare to make the webservice callout synchrounously or asynchronously (future)
     *
     * ### Set the interface name, its interface handler and a refType for head id in an interface object!
     *
     * @param oid   order id
     * @param async     true: use a future method to call 
     *                  false: do a synchrounous callout
     * @param testMode true: emulate call
     */
    public static String callout(String oid, boolean async, boolean testMode)
    {
        String interfaceName = 'SAP_MATERIAL_RESERVATION_CREATE';
        String interfaceHandler = 'CCWCMaterialReservationCreate';
        String refType = null;
        CCWCMaterialReservationCreate oc = new CCWCMaterialReservationCreate(interfaceName, interfaceHandler, refType);
        return oc.calloutCore(oid, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCMaterialReservationCreate', null, null, MODE_RESERVATION_FOR_ORDER);
    } // callout   

    /**
     * Prepare to make the webservice callout synchrounously or asynchronously (future)
     *
     * ### Set the interface name, its interface handler and a refType for head id in an interface object!
     *
     * @param plantName
     * @param stockName
     * @param mode
     *              MODE_REPLENISHMENT = 'REPLENISHMENT';
     *              MODE_RESERVATION_FOR_ORDER = 'RESERVATION_FOR_ORDER';
     *              MODE_RESERVATION_FOR_STOCK = 'RESERVATION_FOR_STOCK';
     * @param async     true: use a future method to call 
     *                  false: do a synchrounous callout
     * @param testMode true: emulate call
     */
    public static String callout(String plantName, String stockName, String mode, boolean async, boolean testMode)
    {
        ID oid = null;
        String interfaceName = 'SAP_MATERIAL_RESERVATION_CREATE';
        String interfaceHandler = 'CCWCMaterialReservationCreate';
        String refType = null;
        CCWCMaterialReservationCreate oc = new CCWCMaterialReservationCreate(interfaceName, interfaceHandler, refType);
        return oc.calloutCore(oid, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCMaterialReservationCreate',
                       plantName, stockName, mode);
    } // callout   


    /**
     * Makes a reservation on the base of mode and appropriate to mode object id.
     * 
     * 
     * Call examples:
     * String stockID = 'a1VD0000000TqUiMAK'; 
     * String mode = CCWCMaterialReservationCreate.MODE_RESERVATION_FOR_STOCK; 
     * boolean async = false; 
     * boolean testMode = false;
     * String msgID = CCWCMaterialReservationCreate.callout(stockID, mode, async, testMode);
     *
     * String replenishmentID = 'a0yL00000004lpvIAA'; 
     * String mode = CCWCMaterialReservationCreate.MODE_REPLENISHMENT; 
     * boolean async = false; 
     * boolean testMode = false;
     * String msgID = CCWCMaterialReservationCreate.callout(replenishmentID, mode, async, testMode);     
     * 
     * String orderID = 'a18D0000000V1vEIAS'; 
     * String mode = CCWCMaterialReservationCreate.MODE_RESERVATION_FOR_ORDER; 
     * boolean async = false; 
     * boolean testMode = false;
     * String msgID = CCWCMaterialReservationCreate.callout(orderID, mode, async, testMode);
     *    
     * @param oid: replenishmentId or stockId or orderID
     * @param mode
     *              MODE_REPLENISHMENT = 'REPLENISHMENT';
     *              MODE_RESERVATION_FOR_ORDER = 'RESERVATION_FOR_ORDER';
     *              MODE_RESERVATION_FOR_STOCK = 'RESERVATION_FOR_STOCK';
     * @param async     true: use a future method to call 
     *                  false: do a synchrounous callout
     * @param testMode true: emulate call
     *
     * @return the messageID of the web call
     */
    public static String callout(String oid, String mode, boolean async, boolean testMode)
    {
        String interfaceName = 'SAP_MATERIAL_RESERVATION_CREATE';
        String interfaceHandler = 'CCWCMaterialReservationCreate';
        String refType = null;
        String plantName = null;
        String stockName = null;
        CCWCMaterialReservationCreate oc = new CCWCMaterialReservationCreate(interfaceName, interfaceHandler, refType);
        return oc.calloutCore(oid, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCMaterialReservationCreate',
                       plantName, stockName, mode);
    } // callout   

    /**
     * Prepare to make the webservice callout synchrounously or asynchronously (future)
     *
     * ### Set the interface name, its interface handler and a refType for head id in an interface object!
     *
     * @param async     true: use a future method to call 
     *                  false: do a synchrounous callout
     * @param testMode true: emulate call
     * @return  messageID, important for the test class. Do not change 
     */
    public static String callout(List<String> movementIDList, boolean async, boolean testMode)
    {
            ID oid = null;
            String interfaceName = 'SAP_MATERIAL_RESERVATION_CREATE';
            String interfaceHandler = 'CCWCMaterialReservationCreate';
            String refType = null;
            String plantName = null;
            String stockName = null;
            String mode = null;
            CCWCMaterialMovementCreate oc = new CCWCMaterialMovementCreate(interfaceName, interfaceHandler, refType);
            return oc.calloutCore(oid, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCMaterialReservationCreate',
                           plantName, stockName, mode, movementIDList);
    } // callout   

    /**
     * Reads an material reservation to send
     *
     * Selection                                        
     *  5202 Material Reservation for an engineer Replenishment is null and with or without an order                                        
     *  5222 Material Replenishment Nachversorgung (Order is null) und Replenishment is not null                Selection           Processing          
     *  
     *  Parameter           Type    MaterialMovement Status     ERPStatus       ERPStatus   By Response 
     *  replenishment   replenishment__c != null        5222    5402 Order      none    pending ok, error   Ordered 5403 by ok  
     *  reservation for order   replenishment__c == null    order__c != null    5202    5402 Order      none    pending ok, error   Ordered 5403 by ok  
     *  reservation for stock   replenishment__c == null    order__c == null    5202    5402 Order      none    pending ok, error   Ordered 5403 by ok  
     * 
     * @param objectId in this case without meaning
     * @param testMode if true no call is made, because in the test mode the salesforce framework prevent it.
     *
     */
    public override void readSalesforceData(String objectID, Map<String, Object> retValue, Boolean testMode)
    {
        debug('plantName: ' + plantName + ', stockName: ' + stockName);
        List<SCMaterialMovement__c> mml = null;
        if(originIdOrNameList == null)
        {
            
            String stmt = 'Select Id, Name, Type__c, Plant__c, Stock__c, Stock__r.Name, DeliveryPlant__c, DeliveryStock__r.Name, '
                        + ' Article__r.Name, Article__r.Unit__c, Qty__c, RequisitionDate__c, order__c, replenishment__c  '
                        + ' from SCMaterialMovement__c ' 
                        + ' where Status__c = \'5402\' and Article__c <> null '
                        + ' and Article__r.ERPRelevant__c = true and Article__r.orderable__c = true'
                        + ' and (ERPStatus__c = \'none\' or ERPStatus__c = \'error\') ';
            // for all modes:
            if(objectID == null && plantName != null && plantName != '')
            {
                stmt += ' and Plant__c = \'' + plantName + '\'';
            }   
            if(objectID == null && stockName != null && stockName != '')
            {
                stmt += ' and Stock__r.Name = \'' +  stockName + '\'';
            }   
    
            if(mode == MODE_RESERVATION_FOR_ORDER && objectID != null && objectID != '')
            {
                stmt += ' and Order__c = \'' + objectID + '\' and Type__c = \'5202\'';
                // set the orderID for the later use in the next setting function
                setReferenceID(objectID);
                setReferenceType('SCOrder__c');
                orderID = objectID; 
            }            
            else if(mode == MODE_REPLENISHMENT && objectID == null && (plantName != null || stockName != null)) 
            {
                // without objectID = replenishmentID
                stmt += ' and Replenishment__c <> null and Type__c = \'5222\'';
                //setReferenceID(objectID);
                setReferenceType('SCMaterialReplenishment__c');
                //orderID = objectID; 
            }
            else if(mode == MODE_REPLENISHMENT && objectID != null && plantName == null && stockName == null)  
            {
                // with objectID = replenishmentID
                stmt += ' and Replenishment__c = \'' + objectID + '\' and Type__c = \'5222\'';
                //setReferenceID(objectID);
                setReferenceType('SCMaterialReplenishment__c');
                //orderID = objectID; 
            }
            else if(mode == MODE_REPLENISHMENT && objectID != null && (plantName != null || stockName != null))  
            {
                // not possible condition
                String msg = 'Error in calling of the CCWCMaterialReservationCreate.callout(): by setting of objectID plantName and stockName must not be set!';
                throw new SCfwException(msg);
            }
            else if(mode == MODE_RESERVATION_FOR_ORDER && objectID != null && plantName != null && stockName != null)
            {
                // with objectID = orderID it is set on the beginning of creating statement
                stmt += ' and Replenishment__c = null and Order__c <> null and Type__c = \'5202\'';
            }
            else if(mode == MODE_RESERVATION_FOR_ORDER && objectID != null && (plantName != null || stockName != null) )
            {
                // not possible condition
                String msg = 'Error in calling of the CCWCMaterialReservationCreate.callout(): by setting of objectID plantName and stockName must not be set!';
                throw new SCfwException(msg);
            }
            else if(mode == MODE_RESERVATION_FOR_STOCK && objectID == null && (plantName != null || stockName != null))
            {
                // without objectID = stockID
                stmt += ' and Replenishment__c = null and Order__c = null and Type__c = \'5202\'';
                setReferenceType('SCStock__c');
            }
            else if(mode == MODE_RESERVATION_FOR_STOCK && objectID != null && plantName == null && stockName == null) 
            {
                // with objectID = stockID
                stmt += 'and Stock__c = \'' + objectID + '\' and Replenishment__c = null and Order__c = null and Type__c = \'5202\'';
                setReferenceType('SCStock__c');
            }
            else if(mode == MODE_RESERVATION_FOR_STOCK && objectID != null && (plantName != null || stockName != null))  
            {
                // not possible condition
                String msg = 'Error in calling of the CCWCMaterialReservationCreate.callout(): by setting of objectID plantName and stockName must not be set!';
                throw new SCfwException(msg);
            }
            else
            {
                String msg = 'Handling for the mode : ' + mode + 'objectID: ' + objectID +  ', plantName: ' + plantName + ', stockName: ' + stockName + ' has not been defined!';
                throw new SCfwException(msg);
            }
            stmt += ' order by Stock__c, Name';             
            mml = Database.query(stmt);
        }
        else
        {
            mml = [Select Id, Name, Type__c, Plant__c, Stock__c, Stock__r.Name, DeliveryPlant__c, DeliveryStock__r.Name, 
                   Article__r.Name, Article__r.Unit__c, Qty__c, RequisitionDate__c, order__c, replenishment__c 
                   from SCMaterialMovement__c 
                   where id in :originIdOrNameList
                   and Article__r.ERPRelevant__c = true and Article__r.orderable__c = true
                   order by Stock__c];
        }                                 
        retValue.put(ROOT, mml);
        debug('material movement list: ' + mml);                                        
    }

    /**
     * Fills the send structure and sends the call to SAP
     * 
     * @param dataMap with the order under the key ROOT
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure
     *           message = data sent to ERP
     *           message_v1 = endpoint 
     *           message_v2 = a return value of web service 
     *           message_v3 = a mandatory fields error
     *
     */
    /* 
    public override Boolean fillAndSendERPData(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs)
    {
        Boolean retValue = true;
        List<SCMaterialMovement__c> mml = (List<SCMaterialMovement__c>)dataMap.get(ROOT);
        if(mml != null && mml.size() > 0)
        {
            // instantiate the client of the web service
            piCceagDeSfdcCSMaterialReservationCreate.HTTPS_Port ws = new piCceagDeSfdcCSMaterialReservationCreate.HTTPS_Port();
    
            // sets the basic authorization for the web service
            ws.inputHttpHeaders_x = u.getBasicAuth();
    
            // sets the endpoint of the web service
            ws.endpoint_x  = u.getEndpoint('MaterialReservationCreate');
            debug('endpoint: ' + ws.endpoint_x);
            rs.message_v1 = ws.endpoint_x;
            
            try
            {
                // instantiate a message header
                piCceagDeSfdcCSMaterialReservationCreate.BusinessDocumentMessageHeader messageHeader = new piCceagDeSfdcCSMaterialReservationCreate.BusinessDocumentMessageHeader();
                
                // instantiate the body of the call
                piCceagDeSfdcCSMaterialReservationCreate.ReservationItem[] reservationArr = setMovements(mml,u, testMode);
                if(reservationArr.size() > 0)
                {
                    setMessageHeader(messageHeader, mml, messageID, u, testMode);
                    
        
                    String jsonInputMessageHeader = JSON.serialize(messageHeader);
                    String fromJSONMapMessageHeader = getDataFromJSON(jsonInputMessageHeader);
                    debug('from json Message Header: ' + fromJSONMapMessageHeader);
        
                    String jsonInputReservationArray = JSON.serialize(reservationArr);
                    String fromJSONMapReservationArray = getDataFromJSON(jsonInputReservationArray);
                    debug('from json material array: ' + fromJSONMapReservationArray);
                    
                    rs.message = '\n\nMessageHeader: ' + fromJSONMapMessageHeader + ',\n\nmessageData: ' + fromJSONMapReservationArray;
                    // check if there are missing mandatory fields
                    if(rs.message_v3 == null || rs.message_v3 == '')
                    {
                        // All mandatory fields are filled
                        // callout
                        if(!testMode)
                        {
                            // It is not the test from the test class
                            // go
                            rs.setCounter(reservationArr.size());
                            ws.MaterialReservationCreate_Out(messageHeader, reservationArr);
                            rs.Message_v2 = 'void';
                        }
                    }
                }
                else
                {
                    retValue = false;
                    // the interface log must not be written
                }
            }
            catch(Exception e)
            {
                throw e;
            }
        }    
        else
        {
            retValue = false;
        }
        return retValue;
    }
    */
    /**
     * Fills the send structure and sends the call to SAP
     * 
     * @param dataMap with the order under the key ROOT
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure
     *           message = data sent to ERP
     *           message_v1 = endpoint 
     *           message_v2 = a return value of web service 
     *           message_v3 = a mandatory fields error
     * @param ilpList the list with the changable part of the interface log fields
     *
     */
    public override Boolean fillAndSendERPDataWithManyInterfaceLogs(Map<String, Object> dataMap, String messageID, 
                                        CCWSUtil u, Boolean testMode, SCReturnStruct rs, List<InterfaceLogPart> ilpList)  
    {
        Boolean retValue = true;
        List<SCMaterialMovement__c> mml = (List<SCMaterialMovement__c>)dataMap.get(ROOT);
        if(mml != null && mml.size() > 0)
        {
            // instantiate the client of the web service
            piCceagDeSfdcCSMaterialReservationCreate.HTTPS_Port ws = new piCceagDeSfdcCSMaterialReservationCreate.HTTPS_Port();
    
            // sets the basic authorization for the web service
            ws.inputHttpHeaders_x = u.getBasicAuth();
    
            // sets the endpoint of the web service
            ws.endpoint_x  = u.getEndpoint('MaterialReservationCreate');
            debug('endpoint: ' + ws.endpoint_x);
            rs.message_v1 = ws.endpoint_x;
            ws.timeout_x = u.getTimeOut();
            
            try
            {
                // instantiate a message header
                piCceagDeSfdcCSMaterialReservationCreate.BusinessDocumentMessageHeader messageHeader = new piCceagDeSfdcCSMaterialReservationCreate.BusinessDocumentMessageHeader();
                
                // instantiate the body of the call
                piCceagDeSfdcCSMaterialReservationCreate.ReservationItem[] movementArr = setMovements(mml,u, testMode, ilpList);
                if(movementArr.size() > 0)
                {
                    setMessageHeader(messageHeader, mml, messageID, u, testMode);

                    String jsonInputMessageHeader = JSON.serialize(messageHeader);
                    String fromJSONMapMessageHeader = getDataFromJSON(jsonInputMessageHeader);
                    debug('from json Message Header: ' + fromJSONMapMessageHeader);
        
                    String jsonInputMovementArray = JSON.serialize(movementArr);
                    String fromJSONMapMovementArray = getDataFromJSON(jsonInputMovementArray);
                    debug('from json material array: ' + fromJSONMapMovementArray);

                    // add message header to the interface log parts
                    for(InterfaceLogPart ilp: ilpList)
                    {
                        ilp.data = '\n\nMessageHeader: ' + fromJSONMapMessageHeader + ilp.data;
                    }
                    
                    rs.message = '\n\nMessageHeader: ' + fromJSONMapMessageHeader 
                               + ',\n\nmessageData: ' + fromJSONMapMovementArray;
                    // check if there are missing mandatory fields
                    if(rs.message_v3 == null || rs.message_v3 == '')
                    {
                        debug('all mandatory fields are filled');
                        // All mandatory fields are filled
                        // callout
                        if(!testMode)
                        {
                            // It is not the test from the test class
                            // go
                            rs.setCounter(movementArr.size());
                            debug('count:' + rs.getCounter());
                            debug('movement array before web call: ' + movementArr);
                            ws.MaterialReservationCreate_Out(messageHeader, movementArr);
                            rs.Message_v2 = 'void';
                        }
                    }
                }
                else
                {
                    retValue = false;
                    // Interface Log must not be written
                }    
            }
            catch(Exception e)
            {
                throw e;
            }
        }
        else
        {
            retValue = false;
        }
        return retValue;    
    }

    
    /**
     * sets data into the message header
     * messageID, referenceID = order.Name, CreationDateTime, SenderBusinessSystemID, RecipientBusinessSystemID
     *
     * @param mh message header 
     * @param order 
     * @param messageID MS: milliseconds R: random, e.g.    MS:1351254139745-R:0.7486318721270884
     * @param u CCWSUtil instance
     * @param testMode used to prevent call out by calls from CCWCOrderCloseTest class
     *
     */
    public void  setMessageHeader(piCceagDeSfdcCSMaterialReservationCreate.BusinessDocumentMessageHeader mh, 
                                         List<SCMaterialMovement__c> mml, String messageID, CCWSUtil u, Boolean testMode)
    {
        if(mml != null && mml.size() > 0)
        {
            mh.MessageID = messageID;
    //      mh.MessageUUID;
            mh.ReferenceID = mml[0].Name;
    //      mh.ReferenceUUID;
            mh.CreationDateTime = u.getFormatedCreationDateTime();
            if(testMode)
            {
                mh.TestDataIndicator = 'Test';
            }    
            mh.SenderBusinessSystemID = u.getSenderBusinessSystemID();
            mh.RecipientBusinessSystemID = u.getRecipientBusinessSystemID ();
            debug('Message Header: ' + mh);
        }    
    }

    /**
     * sets the call out order structure with data form an order
     *
     * @param mml list of material movements
     * @param u instance of the util class CCWSUtil
     * @param testMode used to prevent call out by calls from CCWCOrderCloseTest class
     *
     * @return ReservationItem[] 
     *
     */
    /*
    public piCceagDeSfdcCSMaterialReservationCreate.ReservationItem[] setMovements(List<SCMaterialMovement__c> mml, 
                        CCWSUtil u, Boolean testMode)
    {
        List<piCceagDeSfdcCSMaterialReservationCreate.ReservationItem> retValue = new List<piCceagDeSfdcCSMaterialReservationCreate.ReservationItem>();
        if(mml != null)
        {
            String step = '';
            try
            {
                for(SCMaterialMovement__c mr: mml)
                {
                    piCceagDeSfdcCSMaterialReservationCreate.ReservationItem ri = new piCceagDeSfdcCSMaterialReservationCreate.ReservationItem();
                    ri.MovementType = u.getSAPMaterialMovementType(mr.Type__c);
                    ri.ReceivingPlant = mr.Plant__c;
                    ri.ReceivingStorageLocation = mr.Stock__r.Name;
                    ri.Plant = mr.DeliveryPlant__c;
                    ri.StorageLocation = mr.DeliveryStock__r.Name;
                    //ri.Batch;
                    ri.MaterialID = mr.Article__r.Name;
                    ri.UnitOfMeasure = u.getSAPUnitOfMeasure(mr.Article__r.Unit__c);
                    
                    mi.Quantity = '0';
                    if(mr.Qty__c != null)
                    {
                        ri.Quantity = '' + mr.Qty__c.stripTrailingZeros().toPlainString();        // remove zeros and dezimal places
                    }                       
                    
                    ri.UnloadingPoint = mr.Name;
                    ri.ReservationDate = mr.RequisitionDate__c;
                    retValue.add(ri);
                }
            }
            catch(Exception e)
            {
                String prevMsg = e.getMessage();
                String newMsg = 'setMovements: ' + step + ' ' + prevMsg;
                debug(newMsg);
                e.setMessage(newMsg);
                throw e;
            }
        }    
        return retValue;    
    }
    */
    public piCceagDeSfdcCSMaterialReservationCreate.ReservationItem[] setMovements(List<SCMaterialMovement__c> mml, CCWSUtil u, Boolean testMode,
                                                                    List<InterfaceLogPart> ilpList)
    {
        List<piCceagDeSfdcCSMaterialReservationCreate.ReservationItem> retValue = new List<piCceagDeSfdcCSMaterialReservationCreate.ReservationItem>();
        if(mml != null)
        {
            String step = '';
            try
            {
                ID stockPrev = null;
                InterfaceLogPart ilpCurrent = null;

                piCceagDeSfdcCSMaterialReservationCreate.ReservationItem[] partArrOfMovementItem = null; // the part array for logging              
                for(SCMaterialMovement__c mr: mml)
                {
                    debug('stockPrev: ' + stockPrev);
                    if(stockPrev == null || stockPrev != null && mr.Stock__c != null && stockPrev != mr.Stock__c)
                    {
                        if(stockPrev != null)
                        {
                            // set the data of the previous stock to interface log part
                            String jsonInputMovementArray = JSON.serialize(partArrOfMovementItem);
                            String fromJSONMapMovementArray = getDataFromJSON(jsonInputMovementArray);
                            debug('from json material array: ' + fromJSONMapMovementArray);
                            ilpCurrent.data = ',\n\nmessageData: ' + fromJSONMapMovementArray;
                        }
                        partArrOfMovementItem = new List<piCceagDeSfdcCSMaterialReservationCreate.ReservationItem>();               
                        ilpCurrent = new InterfaceLogPart();
                        ilpList.add(ilpCurrent);
                        ilpCurrent.referenceID = mr.Stock__c; 
                        ilpCurrent.refType = 'SCStock__c';  
                        if(refType != null && refType != 'SCStock__c' && referenceID != null)
                        {
                            ilpCurrent.refType2 = refType;
                            ilpCurrent.referenceID2 = referenceID;
                        }   
                        stockPrev = mr.Stock__c;
                        
                        // now set the order and replenishment references
                        if(mr.order__c != null)
                        {
                            ilpCurrent.order = mr.order__c;
                        }
                        if(mr.Replenishment__c != null)
                        {
                            ilpCurrent.Replenishment = mr.Replenishment__c;
                        }
                    }

                    ilpCurrent.count++;

                    piCceagDeSfdcCSMaterialReservationCreate.ReservationItem ri = new piCceagDeSfdcCSMaterialReservationCreate.ReservationItem();
                    ri.MovementType = u.getSAPMaterialMovementType(mr.Type__c);
                    ri.ReceivingPlant = mr.Plant__c;
                    ri.ReceivingStorageLocation = mr.Stock__r.Name;
                    ri.Plant = mr.DeliveryPlant__c;
                    ri.StorageLocation = mr.DeliveryStock__r.Name;
                    //ri.Batch;
                    ri.MaterialID = mr.Article__r.Name;
                    ri.UnitOfMeasure = u.getSAPUnitOfMeasure(mr.Article__r.Unit__c);
                    ri.Quantity = '0';
                    if(mr.Qty__c != null)
                    {
                        ri.Quantity = '' + mr.Qty__c.stripTrailingZeros().toPlainString();        // remove zeros and dezimal places
                    }   
                    
                    
                    ri.UnloadingPoint = mr.Name;
                    ri.ReservationDate = mr.RequisitionDate__c;
                    retValue.add(ri);
                    partArrOfMovementItem.add(ri);
                }//for
                // logging for last stock
                if(stockPrev != null)
                {
                    // set the data of the previous stock to interface log part
                    String jsonInputMovementArray = JSON.serialize(partArrOfMovementItem);
                    String fromJSONMapMovementArray = getDataFromJSON(jsonInputMovementArray);
                    debug('from json material array: ' + fromJSONMapMovementArray);
                    ilpCurrent.data = ',\n\nmessageData: ' + fromJSONMapMovementArray;
                }
                
                
            }
            catch(Exception e)
            {
                String prevMsg = e.getMessage();
                String newMsg = 'setMovements: ' + step + ' ' + prevMsg;
                debug(newMsg);
                e.setMessage(newMsg);
                throw e;
            }
        }    
        return retValue;    
    }

    /**
     * Sets the ERPStatus in the root object list to 'pending'
     * 
     * @param dataMap of obects to dispatch
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure with error messages
     * @param error true if some error has been occured
     */
    public override void setERPStatus(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs, Boolean error)
    {
        if(dataMap != null)
        {
            List<SCMaterialMovement__c> mml = (List<SCMaterialMovement__c>)dataMap.get(ROOT);
            if(mml != null)
            {
                String status = null;
                // default interface Log
                ID defInterfaceLogId = null;
                if(interfaceLogList.size() > 0 && interfaceLogList[0].id != null)
                {
                    defInterfaceLogId = interfaceLogList[0].id; 
                }
                for(SCMaterialMovement__c mr: mml)
                {
                    ID loopInterfaceLogId = getInterfaceLogId(interfaceLogList, mr.Stock__c);
                    if(loopInterfaceLogId == null)
                    {
                        loopInterfaceLogId = defInterfaceLogId; 
                    }
                    if(loopInterfaceLogId != null)
                    {
                        mr.InterfaceLog__c = loopInterfaceLogId;
                    }
                    if(error)
                    {
                        mr.ERPStatus__c = 'error';
                    }
                    else
                    {   
                        mr.ERPStatus__c = 'pending';
                    }
                    // we do not override error status with pending
                    // if there is only one error in material movement then the flag in order for all is also set to error
                    if(status == null || status != 'error')
                    {
                        status = mr.ERPStatus__c;                       
                    }
                }       
                
                update mml;
                debug('materal movement after update: ' + mml);
                if(orderId != null)
                {
                    updateOrder(orderId, status);
                }               
            }
        }       
    }

    public void updateOrder(ID orderID, String status)
    {
        if(orderID != null)
        {
            List<SCOrder__c> orderList = [Select ID from SCOrder__c where id = :orderID];
            if(orderList != null && orderList.size() > 0)
            {
                for(SCOrder__c o: orderList)
                {
                    if(status != null)
                    {
                        o.ERPStatusMaterialReservation__c = status;
                    }       
                }
            }
            update orderList;
        }       
    }


    /**
     * gets the account number of the invoice recipient. If there is not such the service recipient is returned instead
     *
     * @param order the order tree with data
     *
     * @return the account number of the invoice recipient
     */
    public String getInvoiceRecipient(SCOrder__c order)
    {
        String retValue = order.OrderRole__r[0].Account__r.AccountNumber;
        if(order.OrderRole__r[1] != null)
        {
            retValue = order.OrderRole__r[1].Account__r.AccountNumber;
        }
        return retValue;
    }
    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }
}
