/*
 * @(#)SCfwInterfaceRequestPendingException.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 
public virtual class SCfwInterfaceRequestPendingException extends SCfwException {

	public SCOrder__c order;
	public SCMaterialMovement__c materialMovement;
	

    public SCfwInterfaceRequestPendingException(Boolean log, String message)
    {

        this.setMessage(message);
    }

    public SCfwInterfaceRequestPendingException(Integer key)
    {
        this.setMessage(fwExceptionTexts.getExceptionText(key));
    }

    public SCfwInterfaceRequestPendingException(Integer key, String param)
    {
        this.setMessage(fwExceptionTexts.getExceptionText(key, param));
    }

    public SCfwInterfaceRequestPendingException(Integer key, String param1, String param2)
    {
        this.setMessage(fwExceptionTexts.getExceptionText(key, param1, param2));
    }

    public SCfwInterfaceRequestPendingException(Integer key, String param1, String param2, String param3)
    {
        this.setMessage(fwExceptionTexts.getExceptionText(key, param1, param2, param3));
    }

    /*public SCfwInterfaceRequestPendingException(String message, Boolean logic)
    {
        this.setMessage(message);
        //logicError = logic;
    }*/
       
}
