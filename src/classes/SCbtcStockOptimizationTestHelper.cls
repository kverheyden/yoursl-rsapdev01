/*
 * @(#)SCbtcContractVisitCreateTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Please do not change the functions, because they are used in SCbtcStockOptimizationTest asserts
 */
 
 public class SCbtcStockOptimizationTestHelper
 {
    
    public static void createOptimizationRuleSetItems_A(Id ruleSetId)
    {
//        Set by the function prepareTestData_A()
//        Decimal maxPartValue = 1000;
//        Decimal maxPartVolume = 50;
//        Decimal maxPartWeight = 30;
//        Decimal maxStockValue = 30000;
//        Decimal maxStockVolume = 500;
//        Decimal maxStockWeight = 560;
//        String optimizationInterval = '365';
        // R0 on the black list
        // R1 BLP > 1000 (maxPartValue) 
        // R2 Weight > 30 (maxPartWeight)
        // R3 Volume > 50 (maxPartVolume)
        // R7 on the white list
        // The rules R4 - R5 are set by the rules from S4 -S9
        createOptimizationRuleSetItem('S4', ruleSetId, 'consummed <=',    1, 'then',  0); 
        createOptimizationRuleSetItem('S5', ruleSetId, 'consummed <=',   10, 'then',  1);
        createOptimizationRuleSetItem('S6', ruleSetId, 'consummed <=',   20, 'then',  3,  '<', 99, null, null);
        createOptimizationRuleSetItem('S7', ruleSetId, 'consummed <=',   20, 'then',  2,  '>', 99, null, null);
        createOptimizationRuleSetItem('S8', ruleSetId, 'consummed <=',   50, 'then',  4);
        createOptimizationRuleSetItem('S9', ruleSetId, 'consummed <=',   99, 'then',  5, '<',  99, true, '3');
    }

    public static void createAllItems_A(ID plantId, ID ruleSetId, ID stockId, ID priceListId, ID blackListId, ID whiteListId)
    {
        // Created by the caller of the function prepareTestData_A()
        // the stock's: max weight 560 kg , max value = 30000

        // article                                                       Stock
        // rule,  name,  unit, wUnit,weight,vol,   repl,   price,priceUnit,premanent,  minQty, qty,movementQty
        cI('R0', 'SB001','201','KGM',     1,  1,   null,      10,        1,     true,       5,  10,          2,        plantId, ruleSetId, stockId, priceListId, blackListId, null);
        cI('R7', 'SB002','201','KGM',     1,  1,   null,      10,        1,     true,       5,  10,        100,        plantId, ruleSetId, stockId, priceListId,        null, whiteListId);
        cI('R1', 'SA001','201','KGM',     1,  1,   null,    2000,        1,     true,       5,  10,          2,        plantId, ruleSetId, stockId, priceListId, null, null);
        cI('R2', 'SA002','201','KGM',    31,  1,   null,      10,        1,     true,       5,  10,          2,        plantId, ruleSetId, stockId, priceListId, null, null);
        cI('R3', 'SA003','201','KGM',    10, 51,   null,      10,        1,     true,       5,  10,          2,        plantId, ruleSetId, stockId, priceListId, null, null);
        cI('S4', 'S0001','201','KGM',     1,  1,   null,      10,        1,     true,       5,  10,          1,        plantId, ruleSetId, stockId, priceListId, null, null);
        cI('S5', 'S0002','201','KGM',     1,  1,   null,      10,        1,     true,       5,  10,         10,        plantId, ruleSetId, stockId, priceListId, null, null);
        cI('S6', 'S0004','201','KGM',     1,  1,   null,      10,        1,     true,       5,  10,         20,        plantId, ruleSetId, stockId, priceListId, null, null);
        cI('S7', 'S0003','201','KGM',     1,  1,   null,     100,        1,     true,       5,  10,         20,        plantId, ruleSetId, stockId, priceListId, null, null);
        cI('S8', 'S0005','201', null,     1,  1,   null,     100,        1,     true,       5,  10,         50,        plantId, ruleSetId, stockId, priceListId, null, null);
        cI('S9', 'S0006','201', null,     1,  1,   null,     100,        1,     true,       5,  10,         99,        plantId, ruleSetId, stockId, priceListId, null, null);
       cI('S10', 'S0007','201', null,     1,  1,   null,      10,        1,     true,       5,  10,         99,        plantId, ruleSetId, stockId, priceListId, null, null);
        // supersession
        cI('E5', 'S0008','201', null,     1,  1,   null,      10,        1,     true,       5,  10,         10,        plantId, ruleSetId, stockId, priceListId, null, null);
        cI('E6', 'S0009','201', null,     1,  1,'S0008',      10,        1,     true,       5,  10,         20,        plantId, ruleSetId, stockId, priceListId, null, null);
        cI('E7', 'S0010','201', null,     1,  1,'S0009',      10,        1,     true,       5,  10,         20,        plantId, ruleSetId, stockId, priceListId, null, null);
       // Without Material Movements
       cIWithoutMatMovement('WithoutMovement', 
                  'S0011','201', 'KGM', 0.0,0.0,   null,       1,        1,     true,        1,  1,          0,        plantId, ruleSetId, stockId, priceListId, null, null);


    }
    /**
     * returns the stock id
     */
    public static ID prepareTestData_A()
    {
        SCPlant__c plant = createPlant();
        debug('plant: ' + plant);
        
        SCStockOptimizationList__c blackList = createOptimizationList('Black list', plant.Id);
        Id blackListId = blackList.Id;
        
        SCStockOptimizationList__c whiteList = createOptimizationList('White list', plant.Id);
        Id whiteListId = whiteList.Id;

        Decimal maxPartValue = 1000;
        Decimal maxPartVolume = 50;
        Decimal maxPartWeight = 30;
        Decimal maxStockValue = 34000;
        Decimal maxStockVolume = 500;
        Decimal maxStockWeight = 700;
        String optimizationInterval = '365';

        Id brandId = null;
        // create a price list
        SCPriceList__c priceList = createPriceList('GB', brandId);
        debug('priceList: ' + priceList);        
        // create a rule set
        SCStockOptimizationRuleSet__c ruleSet = createStockOptimizationRuleSet(blackListId,
                                                                                whiteListId,
                                                                                maxPartValue,
                                                                                maxPartVolume,
                                                                                maxPartWeight,
                                                                                maxStockValue,
                                                                                maxStockVolume,
                                                                                maxStockWeight,
                                                                                optimizationInterval,
                                                                                priceList.Id);
        debug('ruleSet: ' + ruleSet);
        // create optimization rule set items
        createOptimizationRuleSetItems_A(ruleSet.Id);
        
        // create stock        
        Date start = Date.today();
        start = start.addDays(-480);
        SCStock__c stock = createStock(plant.Id, start, ruleSet.Id);
        debug('stock: ' + stock);

        createAllItems_A(plant.Id, ruleSet.Id, stock.Id, priceList.Id, blackList.Id, whiteList.Id);

        return stock.Id;
    }
    
    public static SCPlant__c createPlant()
    {
        SCPlant__c plant = new SCPlant__c();
        plant.Name = 'SO Test GB Plant';
        plant.Info__c = 'SO Test GB Plant';
        plant.ID2__c = 'SO Test GB Plant';
        plant.Description__c = 'SO Test GB Plant';
        
        upsert plant;
        return plant; 
    }    
    
    public static SCStockOptimizationList__c createOptimizationList(String type, Id plantId)
    {
        SCStockOptimizationList__c optList = new SCStockOptimizationList__c();

        optList.ID2__c = 'SO Test GB List ' + type;
        optList.Plant__c = plantId; 
        optList.Name = 'SO Test GB List ' + type + ' Name';
        optList.Type__c = type;
        
        upsert optList;
        return optList;
    }

    public static SCPriceList__c createPriceList(String country, Id brand)
    {
        SCPriceList__c priceList = new SCPriceList__c();
        priceList.Name = 'SO Test GB price list name';
        priceList.Country__c = country;
        priceList.Brand__c = brand;
        
        priceList.ID2__c = 'SO Test GB price list';
        upsert priceList;
        return priceList;
    }


    public static SCStockOptimizationRuleSet__c createStockOptimizationRuleSet(Id blackListId,
                                                                        Id whiteListId,
                                                                        Decimal maxPartValue,
                                                                        Decimal maxPartVolume,
                                                                        Decimal maxPartWeight,
                                                                        Decimal maxStockValue,
                                                                        Decimal maxStockVolume,
                                                                        Decimal maxStockWeight,
                                                                        String optimizationInterval,
                                                                        Id priceListId)
    {
        SCStockOptimizationRuleSet__c ruleSet = new SCStockOptimizationRuleSet__c();
        ruleSet.BlackList__c = blackListId;
        ruleSet.WhiteList__c = whiteListId;
        ruleSet.Description__c = 'SO Test GB Rule Set';
        ruleSet.ID2__c = 'SO Test GB Rule Set ID2';
        ruleSet.MaxPartValue__c = maxPartValue;
        ruleSet.MaxPartVolume__c = maxPartVolume;
        ruleSet.MaxPartWeight__c = maxPartWeight;
        ruleSet.MaxStockValue__c = maxStockValue;
        ruleSet.MaxStockVolume__c = maxStockVolume;
        ruleSet.MaxStockWeight__c = maxStockWeight;
        ruleSet.OptimizationInterval__c = optimizationInterval;
        ruleSet.PriceList__c = priceListId;
        ruleSet.Name = 'SO Test GB Rule Set Name';

        upsert ruleSet;
 
        return ruleSet;
    }
    public static SCStock__c createStock(Id plantId, Date start, ID ruleSetId)
    {
        SCStock__c stock = new SCStock__c();
        stock.Description__c = 'SO Test GB Stock';
        stock.ID2__c = 'SO Test GB Stock';
        stock.MaxValue__c = 30000; // change 
        stock.MaxWeight__c = 560;
        stock.Name = 'SO Test GB Stock';
        stock.Plant__c = plantId;
        stock.StockProfileOptimisation__c = true;
        stock.StockOptimizationStart__c = start;
        stock.StockOptimizationRuleSet__c = ruleSetId;
        //stock.AutoApprove__c = true; do not set approval, because it changes
        // the deltas and assertions will fail
        
        upsert stock;
        return stock;
    }


    // create package of items
    public static void cI( String rule,
                    String articleName,
                    String articleUnit,
                    String articleWeightUnit,
                    Decimal articleWeight,
                    Decimal articleVolume,
                    String articleReplacedBy,
                    Decimal articlePrice,
                    Decimal articlePriceUnit,
                    Boolean stockItemPermanent,
                    Decimal stockItemMinQty,
                    Decimal stockItemQty,
                    Decimal movementQty,
                    Id plantId,
                    Id ruleSetId,
                    Id stockId,
                    Id priceListId,
                    Id blackListId,
                    Id whiteListId)
    {
        debug('Test rule: ' + rule);
        // create an article
        SCArticle__c article = createArticle(articleName, 
                                             articleUnit,
                                             articleWeightUnit, 
                                             articleWeight,
                                             articleVolume,
                                             articleReplacedBy);
        debug('article: ' + articleName + ' ' + article);
        // create a price list item
        SCPriceListItem__c priceListItem = createPriceListItem(priceListId, 
                                                  article.Id,
                                                  articlePrice,
                                                  articlePriceUnit);
        debug('priceListItem: ' + articleName + ' ' + priceListItem);
        
        // add the article to the black list
        if(blackListId != null)
        {
            addArticleToList(blackListId, article.id);
        }    
        // add the article to the white list
        if(whiteListId != null)
        {
            addArticleToList(whiteListId, article.id);
        }    
        
        // create stock item
        createStockItem(stockId,
                        article.Id,
                        stockItemMinQty,
                        stockItemPermanent,
                        stockItemQty);


        // create material movements
        createMaterialMovements(stockId, article.Id, movementQty);
    }// createItems
     

    // create package of items
    public static void cIWithoutMatMovement( String rule,
                    String articleName,
                    String articleUnit,
                    String articleWeightUnit,
                    Decimal articleWeight,
                    Decimal articleVolume,
                    String articleReplacedBy,
                    Decimal articlePrice,
                    Decimal articlePriceUnit,
                    Boolean stockItemPermanent,
                    Decimal stockItemMinQty,
                    Decimal stockItemQty,
                    Decimal movementQty,
                    Id plantId,
                    Id ruleSetId,
                    Id stockId,
                    Id priceListId,
                    Id blackListId,
                    Id whiteListId)
    {
        debug('Test rule: ' + rule);
        // create an article
        SCArticle__c article = createArticle(articleName, 
                                             articleUnit,
                                             articleWeightUnit, 
                                             articleWeight,
                                             articleVolume,
                                             articleReplacedBy);
        debug('article: ' + articleName + ' ' + article);
        // create a price list item
        SCPriceListItem__c priceListItem = createPriceListItem(priceListId, 
                                                  article.Id,
                                                  articlePrice,
                                                  articlePriceUnit);
        debug('priceListItem: ' + articleName + ' ' + priceListItem);
        
        // add the article to the black list
        if(blackListId != null)
        {
            addArticleToList(blackListId, article.id);
        }    
        // add the article to the white list
        if(whiteListId != null)
        {
            addArticleToList(whiteListId, article.id);
        }    
        
        // create stock item
        createStockItem(stockId,
                        article.Id,
                        stockItemMinQty,
                        stockItemPermanent,
                        stockItemQty);


        // create material movements
        //createMaterialMovements(stockId, article.Id, movementQty);
    }// createItems

 
    public static List<SCArticle__c> createArticles()
    {
        List<SCArticle__c> articleList = new List<SCArticle__c>();
        articleList.add(createArticle('01', '201', 'KGM', 1, 1, null));

        return articleList;
    }
        
    public static SCArticle__c createArticle(String name, 
                                     String unit,
                                     String weightUnit, 
                                     Decimal weight,
                                     Decimal volume,
                                     String articleReplacedBy)
    {
        Id articleReplacedById = null;
        if(articleReplacedBy != null)
        {
            List<SCArticle__c> articleList = [Select id from SCArticle__c where name =  :articleReplacedBy];
            if(articleList.size() > 0)
            {
                articleReplacedById = articleList[0].id;
            }
        }
        SCArticle__c a = new SCArticle__c();
        a.Name = name;
        a.Unit__c = unit;
        a.WeightUnit__c = weightUnit;
        a.Weight__c = weight;
        a.Volume__c = volume;
        a.LockType__c = '50201';
        if(articleReplacedById != null)
        {
            a.ReplacedBy__c = articleReplacedById;
        }
        upsert a;
        
        return a;
    }

    public static SCPriceListItem__c createPriceListItem(Id priceListId, 
                                                  Id articleId,
                                                  Decimal price,
                                                  Decimal priceUnit)
    {
        SCPriceListItem__c priceListItem = new SCPriceListItem__c();
        priceListItem.PriceList__c = priceListId;
        priceListItem.Article__c = articleId;
        priceListItem.Price__c = price;
        priceListItem.PriceUnit__c = priceUnit;
        
        upsert priceListItem;
        return priceListItem;
    }

    public static  void addArticleToList(Id listId, Id articleId)
    {
        debug('Black or white listId: ' + listId + ', articleId: ' + articleId); 
        SCStockOptimizationListItem__c  item = new SCStockOptimizationListItem__c();
        item.StockOptimizationList__c = listId;
        item.Article__c = articleId;
        upsert item;
    }    

    public static void createStockItem(Id stockId,
                                Id articleId,
                                Decimal stockItemMinQty,
                                Boolean stockItemPermanent,
                                Decimal stockItemQty)
    {
        SCStockItem__c stockItem = new SCStockItem__c();
        stockItem.Article__c = articleId;
        stockItem.MinQty__c = stockItemMinQty;
        stockItem.Permanent__c = stockItemPermanent;
        stockItem.Qty__c = stockItemQty;
        stockItem.Stock__c = stockId;
        upsert stockItem;
        debug('stock item: ' + stockItem);
    }


    public static void createOptimizationRuleSetItem(String ruleItem,
                                              Id ruleSetId,
                                              String dummy1,
                                              Decimal consummed,
                                              String dummy2,
                                              Decimal proposalValue)
    {
        Decimal price = null;
        String listPriceOperator = null;
        Boolean calculateRange = null;
        String range = null;
        createOptimizationRuleSetItem(ruleItem,
                                      ruleSetId,
                                      dummy1,
                                      consummed,
                                      dummy2,
                                      proposalValue,
                                      listPriceOperator,
                                      price,
                                      calculateRange,
                                      range);
    }

    public static void createOptimizationRuleSetItem(String ruleItemName,
                                              Id ruleSetId,
                                              String dummy1,
                                              Decimal consummed,
                                              String dummy2,
                                              Decimal proposalValue,
                                              String listPriceOperator,
                                              Decimal price,
                                              Boolean calculateRange,
                                              String range)
    {
        debug('ruleSetItem Name: ' + ruleItemName); 
        SCStockOptimizationRuleSetItem__c item = new SCStockOptimizationRuleSetItem__c();
        item.StockOptimizationRuleSet__c = ruleSetId;
        item.Consummed__c = consummed;
        item.ProposalValue__c = proposalValue;
        if(price != null)
        {
            item.Listprice__c = price;
        }
        if(listPriceOperator != null)
        {    
            item.ListpriceOperator__c = listPriceOperator;
        }    
        if(calculateRange != null)
        {
            item.CalculateRange__c = calculateRange;
        }    
        if(range != null)
        {
            item.Range__c = range;
        }    
        
        upsert item;
        debug('ruleSetItem: ' + ruleItemName + item); 
    }

    public static void  createMaterialMovements(ID stockId, ID articleId, Decimal qty)
    {
        SCMaterialMovement__c mm = new SCMaterialMovement__c();
        mm.Stock__c = stockId;
        mm.Article__c = articleId;
        mm.Type__c = '5204';
        mm.Qty__c = qty;
        upsert mm;
        debug('materialMovement: ' + mm);
    }

    public static SCStockOptimizationProposal__c readProposal(ID stockId)
    {
        debug('readProposal stock id: ' + stockId);
        SCStockOptimizationProposal__c ret = null;
        List<SCStockOptimizationProposal__c> proposalList = [SELECT ID2__c, PermanentStockItemsCount__c, Plant__c, 
                                                            StockMinValue__c, StockMinWeight__c, ProposalValue__c, 
                                                            ProposalWeight__c, ProposalItems__c, Id, Stock__c, 
                                                            StockCurrentValue__c, StockCurrentWeight__c, StockMaxValue__c, 
                                                            StockMaxWeight__c, Name, StockOptimizationRuleSet__c, 
                                                            TemporaryStockItemsCount__c, TotalCounted__c, TotalProposalQty__c, 
                                                            TotalStockQuantity__c 
                                                            FROM SCStockOptimizationProposal__c
                                                            where Stock__c = :stockId];
        if(proposalList.size() > 0)
        {
            ret = proposalList[0];
        }                                                                   
        return ret;
    }
    
    public static Map<String, SCStockOptimizationProposalItem__c> getMapArticleToProposalItem(Id proposalId)
    {
        Map<String, SCStockOptimizationProposalItem__c> ret = new Map<String, SCStockOptimizationProposalItem__c>();
        List<SCStockOptimizationProposalItem__c> itemList = [SELECT AppliedRule__c, ApprovedQuantity__c, Article__c, 
                                                             Article__r.Name,
                                                             ArticleName__c, CountedQuantity__c,  Delta__c, Name, 
                                                             StockItemMinQuantity__c, Proposal__c, ProposalQuantity__c, 
                                                             Id, StockItem__c
                                                             FROM SCStockOptimizationProposalItem__c
                                                             where Proposal__c = :proposalId];
        for(SCStockOptimizationProposalItem__c item: itemList)
        {
            debug('read propsalItem: ' + item.Article__r.Name + ' -> ' + item);
            ret.put(item.Article__r.Name, item);
        }
        return ret;
    }    

    public static List<SCStockOptimizationProposalItem__c> readProposalItem(Id proposalId)
    {
        List<SCStockOptimizationProposalItem__c> itemList = [SELECT AppliedRule__c, ApprovedQuantity__c, Article__c, 
                                                             Article__r.Name,
                                                             ArticleName__c, CountedQuantity__c,  Delta__c, Name, 
                                                             StockItemMinQuantity__c, Proposal__c, ProposalQuantity__c, 
                                                             Id, StockItem__c
                                                             FROM SCStockOptimizationProposalItem__c
                                                             where Proposal__c = :proposalId];
        return itemList;
    }   
    
    public static List<SCStockItem__c> readStockItems(Id stockId)
    {
        List<SCStockItem__c> itemList = [Select Id, Name, MinQty__c 
                                         From SCStockItem__c 
                                         Where Stock__c = :stockId];
        return itemList;
    }   

    private static void debug(String text)
    {
        System.debug('###...................' + text);
    }
 
 }
