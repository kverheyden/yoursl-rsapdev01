/*
 * @(#)AvsAddress.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * helper data container for address validation 
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
global class AvsAddress
{
    // input and output values
    public String country {get; set;}
    public String countryState {get; set;}
    public String county {get; set;}
    public String postalcode {get; set;}
    public String city {get; set;}
    public String district {get; set;}
    public String street {get; set;}
    public String housenumber { get; set; }

    // output values
    // House number range from / to 
    public String noFrom {get; set;}
    public String noTo {get; set;}

    // Geo coordinates: longitude (+West/-East)
    public Double geoX {get; set;}
    // Geo coordinates: latitude (+North/-South)
    public Double geoY {get; set;}
    
    // Geo coordinates approximated by neighbour search
    public Boolean geoApprox { get; set; }
    
    public String title {get; set;}      // -> Account.PersonTitle
    public String salutation {get; set;} // QAS title -> Account.Salutation
    public String firstname {get; set;}
    public String surname {get; set;}
    public String name2 { get; set; }
    public String organisation {get; set;}
    public String telephoneNumber {get; set;}
    public String telephoneNumber2 {get; set;}
    public String faxNumber {get; set;}
    public String mobileNumber {get; set;}
    public String email {get; set;}
    public Date birthdate {get; set;}
    public String extension { get; set; }
    public String flatno { get; set; }
    public String floor { get; set; }
    public String description { get; set; }
    
    // Status information
    public Integer matchType {get; set;}
    public String  matchInfo {get; set;}
    
    // Account Type (only for UK)
    public String type {get; set;}

    // helper for selecting address validation results
    public boolean selected {get; set;}
    
    // Moniker to get precise information from QAS
    // It is first used to get longitude and latitude of the address
    // But it can be used to get address distributed into fields
    // such as town, county, street and so on
    public String moniker {get; set;}

}
