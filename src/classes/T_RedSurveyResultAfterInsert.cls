public with sharing class T_RedSurveyResultAfterInsert {
	
	public void VisUpdate(List<RedSurveyResult__c> RedSRList) {
		
		List<Visitation__c> visUpdateList = new List<Visitation__c>();
		List<Account> accountUpdateList = new List<Account>();
		Set<Id> accountIdSet = new Set<Id>();	
		
		for (RedSurveyResult__c redSR: RedSRList) {
			if (redSR.Account__c != null)
				AccountIdSet.add(redSR.Account__c);
		}
		
		List<Account> accountList = [Select Id,SalesLastVisit__c, RedActivation__c, RedArea__c, 
													RedAssortment__c, RedCooler__c, RedScore__c, 
													RedScoreTrend__c, RedScoreDate__c,
													LastRedSurveyManager__c,
													RedScoreManager__c,
													RedScoreDateManager__c,
													RedScoreTrendManager__c
										FROM Account 
										WHERE Id IN: AccountIdSet];
		
		Map<Id, Account> AccountMap = new Map<Id, Account>(AccountList);
		
		for(RedSurveyResult__c RedSRLoop: RedSRList){
			if(RedSRLoop.VisitationId__c != '')
			{
				Visitation__c newVis = new Visitation__c();
				newVis.Id = RedSRLoop.VisitationId__c;
				newVis.RedSurveyResult__c = RedSRLoop.Id;
				System.Debug(newVis);
				VisUpdateList.add(newVis);
				
				Account visitAccount = AccountMap.get(RedSRLoop.Account__c);
				if (visitAccount != null) {
					if (RedSRLoop.Source__c == 'SalesApp') {
						visitAccount.SalesLastVisit__c = Date.valueOf(newVis.Time__c);
						visitAccount.RedActivation__c = RedSRLoop.ActivationScore__c;
						visitAccount.RedArea__c = RedSRLoop.AreaScore__c;
						visitAccount.RedAssortment__c = RedSRLoop.AssortmentScore__c;
						visitAccount.RedCooler__c = RedSRLoop.CoolerScore__c;

						visitAccount.LastRedSurvey__c = RedSRLoop.Id;
						visitAccount.RedScore__c = RedSRLoop.RedScore__c;
						visitAccount.RedScoreDate__c = RedSRLoop.RedResultDate__c;
						visitAccount.REDScoreTrend__c = RedSRLoop.RedScoreTrend__c;
					} 
					else if (RedSRLoop.Source__c == 'ManagerApp') {
						visitAccount.LastRedSurveyManager__c = RedSRLoop.Id;
						visitAccount.RedScoreManager__c = RedSRLoop.RedScore__c;
						visitAccount.RedScoreDateManager__c = RedSRLoop.RedResultDate__c;
						visitAccount.REDScoreTrendManager__c = RedSRLoop.RedScoreTrend__c;
					}

					AccountUpdateList.add(visitAccount);
				}
			}
		}
		
		try{
			update VisUpdateList;
			update AccountUpdateList;
		} 
		catch(Exception e){
			System.debug('Trigger exception occurred: RedSurveyResultAfterInsert -- ' + e.getMessage());
		}
	}
}
