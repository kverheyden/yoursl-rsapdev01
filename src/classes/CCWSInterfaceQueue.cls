/*
 * @(#)CCWSInterfaceQueue.cls 
 * 
 * Copyright 2012 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 /*
 * CCWSInterfaceQueue.queueProcess(null, 10, null);
 */
 
global with sharing class CCWSInterfaceQueue {
	
	


     /*
     * Add requests to queue
     *
     */  
     public static void queueAdd(String handler, String data)
     {
		SCInterfaceQueue__c item = new SCInterfaceQueue__c();
        item.InterfaceHandler__c = handler;
        //item.Data__c             = data;
        item.Status__c           = 'pending'; 
         
        insert item;
         
        Attachment a = new Attachment(parentId = item.id, Name=handler, Body=Blob.valueOf(data),Description = '', ContentType='text/plain');
		insert a;
		
		system.debug('#### item: ' + item.name + ' added.');
     }
     
    /*
     * Add requests to queue
     *
     */  
     public static void queueAdd(String handler, String data, Exception error)
     {
		SCInterfaceQueue__c item = new SCInterfaceQueue__c();
        item.InterfaceHandler__c = handler;
        String exStr  = 'Exception('+error.getTypeName()+'):  \n'+ error.getMessage() + '\n Cause:  '+ error.getCause() 
        	+'\n StackTrace: ' + error.getStackTraceString();
        item.Exception__c    = exStr.left(32000);	
        item.Status__c           = 'pending'; 
        insert item;
         
        Attachment a = new Attachment(parentId = item.id, Name=handler, Body=Blob.valueOf(data),Description = '', ContentType='text/plain');
		insert a;
		
		system.debug('#### item: ' + item.name + ' added.');
     }
     
         /*
     * Add requests to queue
     *
     */  
     public static void queueAdd(String handler, String data, SCfwInterfaceRequestPendingException error)
     {
		SCInterfaceQueue__c item = new SCInterfaceQueue__c();
        item.InterfaceHandler__c = handler;
        String exStr  = 'Exception('+error.getTypeName()+'):  \n'+ error.getMessage() + '\n Cause:  '+ error.getCause() 
        	+'\n StackTrace: ' + error.getStackTraceString();
        item.Exception__c    = exStr.left(32000);	
        item.Status__c           = 'pending'; 
        if(error.order != null)
        {
        	item.Order__c 				= error.order.id;
        }
        if(error.materialMovement != null)
        {
        	item.MaterialMovement__c	= error.materialMovement.id;
        }
         
        insert item;
         
        Attachment a = new Attachment(parentId = item.id, Name=handler, Body=Blob.valueOf(data),Description = '', ContentType='text/plain');
		insert a;
		
		system.debug('#### item: ' + item.name + ' added.');
     }
     
     
     /*
     * get the queued Interface Requests
     *
     */
     private static Map<Id,SCInterfaceQueue__c> queueGet(String handler, Integer batchsize)
     {
         Map<Id,SCInterfaceQueue__c> items;
         if(handler == null)
         {
             items = new Map<Id,SCInterfaceQueue__c>(
             [
             	SELECT 
             		id, status__c, InterfaceHandler__c, Data__c,
             		(
             			SELECT
             				Id
             			FROM
             				Attachments
             		) 
             	FROM 
             		SCInterfaceQueue__c 
             	WHERE 
             		status__c = 'pending' 
             		AND 
             		queueStartProcessing__c = '1' 
             	ORDER BY name ASC 
             	LIMIT :batchsize
             ]);
         }
         else
         {
         	items = new Map<Id,SCInterfaceQueue__c>(
              [
			 	SELECT 
			 		id, status__c, InterfaceHandler__c, Data__c,
			 		(
			 			SELECT
			 				Id
			 			FROM
			 				Attachments
			 		) 
			 	FROM 
			 		SCInterfaceQueue__c 
			 	WHERE 
			 		status__c = 'pending' 
			 		AND 
			 		queueStartProcessing__c = '1'
			 		AND 
			 		InterfaceHandler__c = :handler 
			 	ORDER BY name ASC 
			 	LIMIT :batchsize
			 ]);
             //items = [SELECT id, status__c, InterfaceHandler__c, Data__c FROM SCInterfaceQueue__c WHERE status__c = 'pending' AND queueStartProcessing__c = '1' and InterfaceHandler__c = :handler order by name asc limit :batchsize];
         }
         return items; 
     }        


 	/*
     * get the queued Interface Request For the given id
     *
     */
     private static Map<Id,SCInterfaceQueue__c> getInterfaceQueueEntry(ID theID)
     {
         Map<Id,SCInterfaceQueue__c> items;

         items = new Map<Id,SCInterfaceQueue__c>(
             [
             	SELECT 
             		id, status__c, InterfaceHandler__c, Data__c,
             		(
             			SELECT
             				Id
             			FROM
             				Attachments
             		) 
             	FROM 
             		SCInterfaceQueue__c 
             	WHERE 
             		id =: theID
             ]);
             
         return items; 
     } 
     
     
     /**
     * WebService Method:  Excecute the batch process method, called by the jenkins cronjob
     * @param String handler: 		The interface handler, if null, all items are processed
     * @param Integer batchsize: 	The batch size, maximum for the CCWSMaterialStockChange = 24
     * @param String ignore: 		currently not used
     */  
     public static String queueProcess(String handler,  Integer batchsize, String ignore)
     {
     
     
        Map<Id,SCInterfaceQueue__c> itemMap = queueGet(handler, batchsize);
        /*
     	SCfwException e = new SCfwException('This exception has not been thrown. It is used to get the stack to see how the queueProcess has been called!');   
        SCInterfaceLog__c il = new SCInterfaceLog__c(Count__c = 1, Interface__c = 'INTERFACE_QUEUE2', InterfaceHandler__c = 'CCWSInterfaceQueue2', 
                            ResultCode__c = 'E000', Start__c = Datetime.now(), Data__c = 'handler: ' + handler + ', batchSize: ' + batchSize
                            + ', UserInfo.Name: ' + UserInfo.getUserName() + ', UserInfo.SessionId: ' + UserInfo.getSessionID() 
                            + ', Exception with Stack: ' + SCfwException.getExceptionInfo(e));
        insert il;
		*/
        return processItemMap(itemMap, batchsize );
     }
     
     
    /**
     * WebService Method: Exceute the interface queue entry with given id
     * @param  ID queueEntryID: The id of the queue entry
     */  
     WebService static String execute(ID queueEntryID)
     {
		
		System.debug('### Have found queueEntry '+ queueEntryID);
		//System.debug('### Have found queueEntry.id '+ queueEntry.id);

        Map<Id,SCInterfaceQueue__c> itemMap = getInterfaceQueueEntry(queueEntryID);

        return processItemMap(itemMap, 1 );
     }    
     
     
      /**
     * Exceute the given Map of interface queue entries
     * @param  ID queueEntryID: The id of the queue entry
     * @param Integer batchsize: 	The batch size, maximum for the CCWSMaterialStockChange = 24
     */ 
     private static String processItemMap(Map<Id,SCInterfaceQueue__c> itemMap, Integer batchSize )
     {

         List<SCInterfaceQueue__c> items = itemMap.values();
		 Map<Id,Attachment> attachmentMap = queueAttachmentGet(itemMap.keySet());
		
     	 String handler;
     	 Integer numProcessed = 0;
         Integer successed = 0;
         Integer failed = 0;
     	 List<SCInterfaceQueue__c> processeditems =  new List<SCInterfaceQueue__c>(); 
     	 
     	 for(SCInterfaceQueue__c item : items)
         {
         	System.debug('### get item '+ item.InterfaceHandler__c);
         	handler = item.InterfaceHandler__c;
         	String data = attachmentMap.get(item.id).Body.toString();
         	
         	if(handler == 'ReprocessingRequest')
         	{
         		// ReprocessingRequest
         		System.debug('### exceute ReprocessingRequest');
         		handleReprocessingRequest(data);
         		item.Status__c = 'ok';
	            successed++;
	            item.Data__c = 'Reprocessing Request done.\n Please look in the new interface logs for the result';
	            processeditems.add(item);
         	}	
			else if(handler == 'CCWSMaterialStockChange')
         	{
         		System.debug('### exceute CCWSMaterialStockChange');
            	CCWSMaterialStockChange.CCWSMaterialStockChangeClass incomingMessage = (CCWSMaterialStockChange.CCWSMaterialStockChangeClass)JSON.deserialize(data, CCWSMaterialStockChange.CCWSMaterialStockChangeClass.class);
         	    try
         	    {
	         	    //GenericServiceResponseMessageClass retValue = 
	         	    CCWSMaterialStockChange.doProcess(incomingMessage,'Reprocess');
	         	    item.Status__c = 'ok';
	            	successed++;
	            	item.Data__c = 'Computation did not throw an exception.';
					
					/*if(retValue != null)
					{
	            		item.Status__c = 'ok';
	            		successed++;
	            		item.Data__c = 'Computation did not throw an exception.';
					}
					else
					{
						item.Status__c = 'error';
						failed++;
						item.Data__c = 'Computation did not throw an exception.';
					}*/
         	    }
         	    catch(Exception error)
         	    {
         	    	String exStr = 'Exception('+error.getTypeName()+'):  \n'+ error.getMessage() + '\n Cause:  '+ error.getCause() 
        				+'\n StackTrace: ' + error.getStackTraceString();
        			item.Data__c = exStr.left(32000);	
        			item.Status__c = 'error';
					failed++;
         	    }
            	processeditems.add(item);
         	}
         	else if(handler == 'CCWSGenericResponse') 
         	{
         		System.debug('### exceute GenericResponse');
         		
         		CCWSGenericResponse.GenericServiceResponseMessageClass incomingMessage = (CCWSGenericResponse.GenericServiceResponseMessageClass)JSON.deserialize(data, CCWSGenericResponse.GenericServiceResponseMessageClass.class);
         	    
         	    Boolean success;
         	    try
         	    {
         	    	 success = CCWSGenericResponse.doProcess(incomingMessage);
         	    	 item.Data__c = 'Computation did not throw an exception.';
         	    }
         	    catch(Exception error)
         	    {
         	    	String exStr = 'Exception('+error.getTypeName()+'):  \n'+ error.getMessage() + '\n Cause:  '+ error.getCause() 
        				+'\n StackTrace: ' + error.getStackTraceString();
        			item.Data__c = exStr.left(32000);	
         	    	success = false;
         	    }
				if(success)
				{
            		item.Status__c = 'ok';
            		successed++;
				}
				else
				{
					item.Status__c = 'error';
					failed++;
				}
            	processeditems.add(item);
         	}
         	else if(handler == 'CCWSEquipmentChange') 
         	{
         		System.debug('### exceute CCWSEquipmentChange');
         		
         		List<CCWSEquipmentChange.CCWSEquipmentChangeClass> incomingMessage = (List<CCWSEquipmentChange.CCWSEquipmentChangeClass>)JSON.deserialize(data, List<CCWSEquipmentChange.CCWSEquipmentChangeClass>.class);
         	    
         	    Boolean success;
         	    try
         	    {
         	    	 success = CCWSEquipmentChange.doProcess(incomingMessage);
         	    	 item.Data__c = 'Computation did not throw an exception.';
         	    }
         	    catch(Exception error)
         	    {
         	    	String exStr = 'Exception('+error.getTypeName()+'):  \n'+ error.getMessage() + '\n Cause:  '+ error.getCause() 
        				+'\n StackTrace: ' + error.getStackTraceString();
        			item.Data__c = exStr.left(32000);	
         	    	success = false;
         	    }
				if(success)
				{
            		item.Status__c = 'ok';
            		successed++;
				}
				else
				{
					item.Status__c = 'error';
					failed++;
				}
            	processeditems.add(item);
         	} 
         	else // TODO add further classes 
         	{
         		return 'Error: Wrong interface handler ' + handler;
         	}

            numProcessed++;
            
            // break when batch size is reached.
            // as the query is limited to the batch size, this should be automatically given
            if(numProcessed >= batchsize)
            {
                break;
            }
            
            
         }
         update processeditems;
         return 'queue [' + handler + '] processed ' + processeditems.size() + '[successed : ' + successed +' / failed: '+failed+ ']';
     	
     }
     
     
     //mapping interfacequeue.id -> attachments
     public static Map<Id,Attachment> queueAttachmentGet(Set<Id> itemIds)
     {
     	Map<Id,Attachment> attMap = new Map<Id,Attachment>();
     	List<Attachment> attList =
     	[
     		SELECT
     			Body,
     			Name,
     			parentId
     		FROM
     			Attachment
     		WHERE
     			parentId =:itemIds
     	];
     	
     	for(Attachment a : attList)
     	{
     		//TODO: Validation: only one attachment per item
     		attMap.put(a.parentId,a);
     	}
     	return attMap;
     }
     
     public static void handleReprocessingRequest(String data)
     {
     	SCInterfaceLog__c ifLog = (SCInterfaceLog__c)JSON.deserialize(data, SCInterfaceLog__c.class);


            if(ifLog.InterfaceHandler__c == 'CCWCMaterialMovementCreate' || ifLog.InterfaceHandler__c == 'CCWCMaterialMovementCreateResponse')
            {
            	System.debug('Case retry CCWCMaterialMovementCreate' );
				List<String> mmNames = SCbtcInterfaceCalloutReprocess.extractMaterialMovementNames(ifLog.Data__c);
				SCOrder__c ord =  SCbtcInterfaceCalloutReprocess.selcetOrder(ifLog.Order__c);
				System.debug(mmNames);
 				List<SCMaterialMovement__c> mmList = SCbtcInterfaceCalloutReprocess.selectMaterialMovments(mmNames);
 				System.debug(mmList);
 				List<String> mmIds = new List<String>();
				//TODO: Update callout status
				for(SCMaterialMovement__c mm : mmList)
 				{
 						mmIds.add(mm.id);
 				}
            	if(mmIds.size() > 0)
            	{
            		// call 
            		CCWCMaterialMovementCreate.callout(mmIds, true, false);
            	}
            	else
            	{
            		
            	}
            }
            else if(ifLog.InterfaceHandler__c == 'CCWCMaterialReservationCreate' || ifLog.InterfaceHandler__c == 'CCWCMaterialReservationCreateResponse')
            {
            	System.debug('Case retry CCWCMaterialReservationCreate' );
				List<String> mmNames = SCbtcInterfaceCalloutReprocess.extractMaterialMovementNames(ifLog.Data__c);
				SCOrder__c ord =  SCbtcInterfaceCalloutReprocess.selcetOrder(ifLog.Order__c);
				System.debug(mmNames);
 				List<SCMaterialMovement__c> mmList = SCbtcInterfaceCalloutReprocess.selectMaterialMovments(mmNames);
 				System.debug(mmList);
 				List<String> mmIds = new List<String>();
				//TODO: Update callout status 
				for(SCMaterialMovement__c mm : mmList)
 				{
 						mmIds.add(mm.id);
 				}
            	// check if order create is not ok already 
            	if(SCbtcInterfaceCalloutReprocess.canCallout(ord))
            	{
            		// call 
            		CCWCMaterialReservationCreate.callout(mmIds, true, false);
            	}
            }
            else if(ifLog.InterfaceHandler__c == 'CCWCArchiveDocumentInsert' || ifLog.InterfaceHandler__c == 'CCWCArchiveDocumentInsertResponse')
            {
            	System.debug('Case retry CCWCArchiveDocumentInsert' );
            	SCOrder__c ord =  SCbtcInterfaceCalloutReprocess.selcetOrder(ifLog.Order__c);
            
            	// check if order create is not ok already 
            	if(SCbtcInterfaceCalloutReprocess.canCallout(ord))
            	{
            		// call
            		System.debug('call CCWCArchiveDocumentInsert.calloutOid:' + ord.name);
            		CCWCArchiveDocumentInsert.calloutOid(ord.id, true, false);
            	}
            }
            
     }
      
      
    private void debug(String text)
    {
        //if(mode.equalsIgnoreCase('test') || mode.equalsIgnoreCase('trace'))
        //{
            System.debug('###...................' + text);
        //}
    }

}
