// PoC Trigger-Framework
public class AttachmentTriggerDispatcher extends TriggerDispatcherBase{
	private static Boolean isAfterInsertProcessing = false;
	private static Boolean isAfterUpdateProcessing = false;
	private static Boolean isBeforeDeleteProcessing = false;

	public override void afterInsert(TriggerParameters tp) {
		if(!isAfterInsertProcessing) {
			isAfterInsertProcessing = true;
			execute(new AttachmentTriggerHandler.AttachmentAIHandler(), tp, TriggerParameters.TriggerEvent.afterInsert);
			isAfterInsertProcessing = false;
		}
		else 
			execute(null, tp, TriggerParameters.TriggerEvent.afterInsert);
	}

	public override void afterUpdate(TriggerParameters tp) {
		if(!isAfterUpdateProcessing) {
			isAfterUpdateProcessing = true;
			execute(new AttachmentTriggerHandler.AttachmentAUHandler(), tp, TriggerParameters.TriggerEvent.afterUpdate);
			isAfterUpdateProcessing = false;
		}
		else
			execute(null, tp, TriggerParameters.TriggerEvent.afterUpdate);
	}

	public override void beforeDelete(TriggerParameters tp) {
		if(!isBeforeDeleteProcessing) {
			isBeforeDeleteProcessing = true;
			execute(new AttachmentTriggerHandler.AttachmentBDHandler(), tp, TriggerParameters.TriggerEvent.beforeDelete);
			isBeforeDeleteProcessing = false;
		}
		else
			execute(null, tp, TriggerParameters.TriggerEvent.beforeDelete);
	}

}
