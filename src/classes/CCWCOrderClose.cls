/*
 * @(#)CCWCOrderClose.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * The class exports an Salesforce order close information into SAP
 * 
 * The entry method is: 
 *      callout(String oid, boolean async, boolean testMode)
 *
 * Methods to override: 
 *      fillAndSendERPData(...)
 *      fillAndSendERPData(...)
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
public without sharing class CCWCOrderClose extends SCInterfaceExportBase 
{
    public static String ORDER_REPAIR_CODE_1 = 'ORDER_REPAIR_CODE_1';
    public static String ORDER_REPAIR_CODE_2 = 'ORDER_REPAIR_CODE_2';

    public CCWCOrderClose()
    {
    }
    public CCWCOrderClose(String interfaceName, String interfaceHandler, String refType, String subclassName)
    {
        super(interfaceName, interfaceHandler, refType, subclassName);
    }
    public CCWCOrderClose(String interfaceName, String interfaceHandler, String refType)
    {
        //super(interfaceName, interfaceHandler, refType, subclassName);
    }

    /**
     * Prepare to make the webservice callout synchrounously or asynchronously (future)
     *
     * ### Set the interface name, its interface handler and a refType for head id in an interface object!
     *
     * @param oid   order id
     * @param async     true: use a future method to call 
     *                  false: do a synchrounous callout
     * @param testMode true: emulate call
     */
    public static String callout(String oid, boolean async, boolean testMode)
    {
        String interfaceName = 'SAP_ORDER_CLOSE';
        String interfaceHandler = 'CCWCOrderClose';
        String refType = 'SCOrder__c';
        CCWCOrderClose oc = new CCWCOrderClose(interfaceName, interfaceHandler, refType);
        return oc.calloutCore(oid, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCOrderClose');
    } // callout   

    public static String cancel(String oid, boolean async, boolean testMode)
    {
        String interfaceName = 'SAP_ORDER_CANCEL';
        String interfaceHandler = 'CCWCOrderClose';
        String refType = 'SCOrder__c';
        CCWCOrderClose oc = new CCWCOrderClose(interfaceName, interfaceHandler, refType);
        return oc.calloutCore(oid, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCOrderClose');
    } // callout   

    /**
     * Reads an order to send
     *
     * @param orderId
     * @param testMode if true no call is made, because in the test mode the salesforce framework prevent it.
     *
     */
    public override void readSalesforceData(String orderID, Map<String, Object> retValue, Boolean testMode)
    {
        List<SCOrder__c> ol =  [Select ID, Name, IdExt__c, ERPOrderNo__c, Status__c, Type__c, 
                                Closed__c, DepartmentCurrent__r.Name, Description__c, 
                               CustomerPrefStart__c, PriceList__r.ID2__c, 
                               CompanyCode__c, SalesArea__c, Division__c, DistributionChannel__c, SalesOffice__c,
                               tolabel(CancelReason__c), info__c, ERPStatusOrderClose__c,ERPStatusOrderCreate__c,
                               (Select ID, Installedbase__r.ProductModel__r.Name, Installedbase__r.IdExt__c, ErrorText__c, 
                                ErrorSymptom1__c, ErrorSymptom2__c from OrderItem__r order by Name),
                                (Select Type__c, Description__c, Duration__c, Start__c, End__c, CreatedDate, Distance__c,
                                Resource__r.DefaultDepartment__r.Name,  
                                Resource__r.EmployeeNumber_txt__c
                                from TimeReports__r where Type__c in ('7503', '7505', '7533', '7525', '7534', '7527', '7535'))
                                from SCOrder__c where ID = : orderId
// Status handling is in UI implemented!
//                                and ERPStatusOrderCreate__c = 'ok'
//                                and (ERPStatusEquipmentUpdate__c = 'none' or ERPStatusEquipmentUpdate__c = 'ok')
//                                and ERPStatusMaterialMovement__c = 'ok'
//                                and Status__c = '5506' and InvoicingReleased__c = true
//                                and (ERPStatusOrderClose__c = 'none' or  ERPStatusOrderClose__c = 'error')
                                ];
                               
        if(ol.size() > 0)
        {
            retValue.put(ROOT, ol[0]);

            debug('order: ' + retValue);
            List<ID> orderItemIdList = new List<ID>();
            if(ol[0].OrderItem__r.size() > 0)
            {
                for(SCOrderItem__c oi : ol[0].OrderItem__r)
                {
                    orderItemIdList.add(oi.id);
                }
            }
            debug('order item id list: ' + orderItemIdList);
            List<SCOrderRepairCode__c> orcList1 = [Select id, OrderItem__c, Order__c, ErrorLocation1__c, ErrorLocation2__c 
                                                from SCOrderRepairCode__c where Order__c = : orderID and OrderItem__c = null];
            retValue.put(ORDER_REPAIR_CODE_1, orcList1);        


            List<SCOrderRepairCode__c> orcList2 = [Select id, OrderItem__c, Order__c, ErrorLocation1__c, ErrorLocation2__c 
                                                from SCOrderRepairCode__c where OrderItem__c in : orderItemIdList order by OrderItem__c, Name];
            retValue.put(ORDER_REPAIR_CODE_2, orcList2);        
        }
        else
        {
            // commented out because we make a web call without condition in this class
            // so we must not know here why the web call could not be carried out
            /*
            List<SCOrder__c> ol2 = [Select ERPStatusOrderCreate__c, ERPStatusEquipmentUpdate__c, ERPStatusMaterialMovement__c, 
                                Status__c, InvoicingReleased__c, ERPStatusOrderClose__c from SCOrder__c where id = :orderID];
            String msg = 'The order with id: ' + orderId + ' is not yet ready to be closed.';
            if(ol2.size() > 0)
            {
                if(ol2[0].ERPStatusOrderCreate__c != 'ok')
                {
                    msg += '\nERPStatusOrderCreate__c: \'' + ol2[0].ERPStatusOrderCreate__c + '\' expected \'ok\'';
                }   
                if( ol2[0].ERPStatusEquipmentUpdate__c != null
                    &&  ol2[0].ERPStatusEquipmentUpdate__c != 'none' 
                    && ol2[0].ERPStatusEquipmentUpdate__c != 'ok')
                {
                    msg += '\nERPStatusEquipmentUpdate__c: \''+  ol2[0].ERPStatusEquipmentUpdate__c + '\' expected \'ok\' or \'none\'';
                }   
                if(ol2[0].ERPStatusMaterialMovement__c != 'ok' || ol2[0].ERPStatusMaterialMovement__c != 'none')
                {
                    msg += '\nERPStatusMaterialMovement__c: \'' +  ol2[0].ERPStatusMaterialMovement__c + '\' expected \'ok\' or \'none\'';
                }   
                if(ol2[0].Status__c != '5506')
                {
                    msg += '\nStatus__c: \''  +  ol2[0].Status__c + '\' expected \'5506\'';
                }   
                if(ol2[0].InvoicingReleased__c != true)
                {
                    msg += '\nInvoicingReleased__c: \''  +  ol2[0].InvoicingReleased__c + '\' expected \'true\'';
                }   
                if(ol2[0].ERPStatusOrderClose__c != null && ol2[0].ERPStatusOrderClose__c != 'error' && ol2[0].ERPStatusOrderClose__c != 'none')
                {
                    msg += '\nERPStatusOrderClose__c: \''  +  ol2[0].ERPStatusOrderClose__c + '\' expected \'error\' or \'none\'';
                }   
            }
            setReferenceID(orderID);
            debug(msg);
            throw new SCfwException(msg);
            */
        }
    }

    /**
     * Fills the send structure and sends the call to SAP
     * 
     * @param dataMap with the order under the key ROOT
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure
     *           message = data sent to ERP
     *           message_v1 = endpoint 
     *           message_v2 = a return value of web service 
     *           message_v3 = a mandatory fields error
     *
     */
    public override Boolean fillAndSendERPData(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs)
    {
        Boolean retValue = true;
        SCOrder__c order = (SCOrder__c)dataMap.get(ROOT);
        
        // Check if we can close the order
        if(order.ERPStatusOrderClose__c == 'ok')
        {
            rs.message = 'The OrderClose was skipped as ERPStatusOrderClose__c is already [ok]';
            rs.message_v4 = 'E001';
            return retValue;
        }
        else if (order.ERPStatusOrderCreate__c != 'ok')
        {
            rs.message = 'The OrderClose was skipped as ERPStatusOrderCreate__c was not [ok]';
            rs.message_v4 = 'E001';
            return retValue;
        }
        
        
        // instantiate the client of the web service
        piCceagDeSfdcCSOrderClose.HTTPS_Port ws = new piCceagDeSfdcCSOrderClose.HTTPS_Port();

        // sets the basic authorization for the web service
        ws.inputHttpHeaders_x = u.getBasicAuth();

        // sets the endpoint of the web service
        ws.endpoint_x  = u.getEndpoint('OrderClose');
        debug('endpoint: ' + ws.endpoint_x);
        rs.message_v1 = ws.endpoint_x;
        ws.timeout_x = u.getTimeOut();
        
        try
        {
            // instantiate a message header
            piCceagDeSfdcCSOrderClose.BusinessDocumentMessageHeader messageHeader = new piCceagDeSfdcCSOrderClose.BusinessDocumentMessageHeader();
            setMessageHeader(messageHeader, order, messageID, u, testMode);
            
            // instantiate the body of the call
            piCceagDeSfdcCSOrderClose.CustomerServiceOrder_element customerServiceOrder = new piCceagDeSfdcCSOrderClose.CustomerServiceOrder_element();
            
            // set the data to the body
            // rs.message_v3 = 
            List<SCOrderRepairCode__c> orcList1 = (List<SCOrderRepairCode__c>)dataMap.get(ORDER_REPAIR_CODE_1);
            List<SCOrderRepairCode__c> orcList2 = (List<SCOrderRepairCode__c>)dataMap.get(ORDER_REPAIR_CODE_2);
            
            Boolean cancelorder = interfaceName == 'SAP_ORDER_CANCEL';
            setCustomerServiceOrder(customerServiceOrder, order, orcList1, orcList2, u, testMode, cancelorder);


            String jsonInputMessageHeader = JSON.serialize(messageHeader);
            String fromJSONMapMessageHeader = getDataFromJSON(jsonInputMessageHeader);
            debug('from json Message Header: ' + fromJSONMapMessageHeader);

            String jsonInputOrder = JSON.serialize(customerServiceOrder);
            String fromJSONMapOrder = getDataFromJSON(jsonInputOrder);
            debug('from json Order: ' + fromJSONMapOrder);
            
            rs.message = '\n\nMessageHeader: ' + fromJSONMapMessageHeader + '\n\nmessageData: ' + fromJSONMapOrder ;
            // check if there are missing mandatory fields
            if(rs.message_v3 == null || rs.message_v3 == '')
            {
                // All mandatory fields are filled
                // callout
                if(!testMode)
                {
                    // It is not the test from the test class
                    // go
                    rs.setCounter(1);
                    ws.CustomerServiceOrderClose_Out(messageHeader, customerServiceOrder);
                    rs.Message_v2 = 'void';
                    
                }
            }
        }
        catch(Exception e)
        {
            throw e;
        }

        return retValue;
    }
    
    /**
     * sets data into the message header
     * messageID, referenceID = order.Name, CreationDateTime, SenderBusinessSystemID, RecipientBusinessSystemID
     *
     * @param mh message header 
     * @param order 
     * @param messageID MS: milliseconds R: random, e.g.    MS:1351254139745-R:0.7486318721270884
     * @param u CCWSUtil instance
     * @param testMode used to prevent call out by calls from CCWCOrderCloseTest class
     *
     */
    public void  setMessageHeader(piCceagDeSfdcCSOrderClose.BusinessDocumentMessageHeader mh, 
                                         SCOrder__c order, String messageID, CCWSUtil u, Boolean testMode)
    {
        mh.MessageID = messageID;
//      mh.MessageUUID;
        mh.ReferenceID = order.Name;
//      mh.ReferenceUUID;
        mh.CreationDateTime = u.getFormatedCreationDateTime();
        if(testMode)
        {
            mh.TestDataIndicator = 'Test';
        }    
        mh.SenderBusinessSystemID = u.getSenderBusinessSystemID();
        mh.RecipientBusinessSystemID = u.getRecipientBusinessSystemID ();
        debug('Message Header: ' + mh);
    }

    /**
     * sets the call out order structure with data form an order
     *
     * @param cso callout order sturcture
     * @param order order with input data
     * @param u instance of the util class CCWSUtil
     * @param testMode used to prevent call out by calls from CCWCOrderCloseTest class
     *
     */
    public String setCustomerServiceOrder(piCceagDeSfdcCSOrderClose.CustomerServiceOrder_element cso, 
                                        SCOrder__c order, List<SCOrderRepairCode__c> orcList1, 
                                        List<SCOrderRepairCode__c> orcList2,
                                        CCWSUtil u, Boolean testMode, Boolean cancelorder)
    {
        CCSettings__c ccset = CCSettings__c.getInstance();
        
        String retValue = '';
        String step = '';
        try
        {
            step = 'Reference Number';
            cso.ReferenceNumber = order.Name;
            
            step = 'Order Number';                              
            cso.OrderNumber = order.ERPOrderNo__c;                          
            retValue = checkMandatory(retValue, cso.OrderNumber, 'ERP Order Number');                

            step = 'Deletion Flag';
            cso.DeletionFlag = '' + u.isDeleted(order.Status__c);           
            retValue = checkMandatory(retValue, cso.OrderNumber, 'Deletion Flag');                
            

            step = 'Short Text';
            // 27.11.2012 KU pass only a fixed text 
            if(ccset.SAPOrderShortText__c != null)
            {
                cso.ShortText = ccset.SAPOrderShortText__c;
            }
            
            step = 'Reference Date';
            cso.ReferenceDate = u.formatToDate(order.Closed__c);
            
            step = 'Reference Time';
            //cso.ReferenceTime = u.formatDateTime(order.Closed__c);
            cso.ReferenceTime = u.formatToGmtTimeZ(order.Closed__c);
            
            
            step = 'Price List';            
            cso.PriceList = order.PriceList__r.ID2__c;

            step = 'Notification Data';
            cso.NotificationData = new piCceagDeSfdcCSOrderClose.NotificationData_element();
            
            step = 'Long Text';
            if(u.isDeleted(order.Status__c))
            {
                // cancelreason, info
                cso.NotificationData.LongText = '[' + order.CancelReason__c + '] \n' + order.info__c;
            }
            else
            {
                if(order.OrderItem__r.size() > 0)
                {
                	cso.NotificationData.LongText = order.OrderItem__r[0].ErrorText__c;
                }
                else
                {
                	cso.NotificationData.LongText = 'Order has no Order Item!';
                }
            }

            
            //GMSNA 23.04.2013 only pass this values for REPAIR - SAP Team (Mr. Haas)
            //CCEAG
            //5701 ZC02-1-Reparatur
            
            // GMSGB 11.12.13 it es possible that orders are created with missing orderitems
            // It has to be possible that these orders are cancelled.
            // Therfore ignore the symptom codes if no order item is given.
            if(order.OrderItem__r.size() > 0 && SCfwConstants.DOMVAL_ORDERTYPE_DEFAULT.contains(order.Type__c) )
            { 
                step = 'Code Group';
                cso.NotificationData.CodeGroup = order.OrderItem__r[0].ErrorSymptom1__c;
                
                step = 'Code ID';
                cso.NotificationData.CodeID = order.OrderItem__r[0].ErrorSymptom2__c;
            }                
            
            if(orcList1 != null && orcList1.size() > 0 || orcList2 != null && orcList2.size() > 0)
            {
                step = 'Reasons';
                cso.NotificationData.Reasons = new piCceagDeSfdcCSOrderClose.Reasons_element();
                cso.NotificationData.Reasons.Reason = new List<piCceagDeSfdcCSOrderClose.Reason_element>();
                for(SCOrderRepairCode__c orc: orcList1)
                {
                    step = 'Reason without order item';
                    piCceagDeSfdcCSOrderClose.Reason_element reason = new piCceagDeSfdcCSOrderClose.Reason_element();
                    reason.ProblemCode = orc.ErrorLocation1__c;
                    reason.ProblemID = orc.ErrorLocation2__c;
                    cso.NotificationData.Reasons.Reason.add(reason);
                }
                for(SCOrderRepairCode__c orc: orcList2)
                {
                    step = 'Reason with order item';
                    piCceagDeSfdcCSOrderClose.Reason_element reason = new piCceagDeSfdcCSOrderClose.Reason_element();
                    reason.ProblemCode = orc.ErrorLocation1__c;
                    reason.ProblemID = orc.ErrorLocation2__c;
                    cso.NotificationData.Reasons.Reason.add(reason);
                }
            }
            
            
            step = 'Time Confirmations';
            cso.TimeConfirmations = new piCceagDeSfdcCSOrderClose.TimeConfirmations_element();
            cso.TimeConfirmations.TimeConfirmation = new List<piCceagDeSfdcCSOrderClose.TimeConfirmation_element>();
            for(SCTimeReport__c tr: order.TimeReports__r)
            {
                if(tr.Duration__c != null && tr.Duration__c > 0)
                {
                    piCceagDeSfdcCSOrderClose.TimeConfirmation_element tce = new piCceagDeSfdcCSOrderClose.TimeConfirmation_element();
                    tce.OperationNumber = '0010'; // fixed value
                    step = 'Work Center';           
                    // send the engineer number as the work center
                    //tce.WorkCenter = u.getWorkCenter();
                    tce.WorkCenter = tr.Resource__r.EmployeeNumber_txt__c;
                    
                    step = 'Plant';
                    tce.Plant = tr.Resource__r.DefaultDepartment__r.Name;
                    tce.ShortText = tr.Description__c;
                    
                    tce.ActualWork = '' + tr.Duration__c;        // in Minutes  00:00
                    
                    tce.UnitOfMeasure = 'MIN'; 
                    Boolean distance = false;
                    step = 'Activity Type';
                    tce.ActivityType = u.mapActivityType(tr.Type__c, distance);     
                    tce.PostingDate = Date.today(); // Date
                    step = 'Work Start Date';            
                    // GMSGB 29.08.13  u.toDate does not consider the GMS / MESZ difference   
                    // PMS 35838/INC0057322: Fehlermeldung Startzeit ist größer als Endezeit        
                    // Wrong tce.WorkStartDate = u.toDate(tr.Start__c); 
                    DateTime startDateTime = tr.Start__c;
                    //startDateTime.formatGmt();
                    tce.WorkStartDate =     u.toDate(startDateTime.dateGMT());  // transform date to GMT     
                    tce.WorkStartTime = u.formatToTime(tr.Start__c) + 'Z';      // error in output it is the DateTime type
                    //tce.WorkStartTime = tr.Start__c;
                    step = 'Work End Date';
                    tce.WorkEndDate = u.formatToDate(tr.End__c);
                    tce.WorkEndTime = u.formatToTime(tr.End__c) + 'Z'; // DateTime
                    //tce.WorkEndTime = tr.End__c;
                    cso.TimeConfirmations.TimeConfirmation.add(tce); 
                    if(tr.Type__c == '7505')
                    {
                        if(tr.Distance__c != null && tr.Distance__c > 0)
                        {
                            piCceagDeSfdcCSOrderClose.TimeConfirmation_element tce2 = new piCceagDeSfdcCSOrderClose.TimeConfirmation_element();
                            tce2.OperationNumber = '0010'; // fixed value           
                            // tce2.WorkCenter = u.getWorkCenter();
                            tce2.WorkCenter = tr.Resource__r.EmployeeNumber_txt__c;
                            tce2.Plant = tr.Resource__r.DefaultDepartment__r.Name;
                            tce2.ShortText = tr.Description__c;
                            
                            tce2.ActualWork = '0';
                            if(tr.Distance__c != null)
                            {
                                tce2.ActualWork = '' + tr.Distance__c.stripTrailingZeros().toPlainString();        // remove zeros ;  // KM set as H
                            }
                            
                            tce2.UnitOfMeasure = 'H'; 
                            distance = true;
                            tce2.ActivityType = u.mapActivityType(tr.Type__c, distance);
                            tce2.PostingDate = Date.today(); // Date   
		                    // GMSGB 29.08.13  u.toDate does not consider the GMS / MESZ difference   
		                    // PMS 35838/INC0057322: Fehlermeldung Startzeit ist größer als Endezeit   
		                    // und
		                    // PMS 36597/INC0106041: SAP Auftragsnummer 000400005382 erzeugt Fehlermeldung E103     
		                    // Wrong tce.WorkStartDate = u.toDate(tr.Start__c);                
                            tce2.WorkStartDate =  u.toDate(startDateTime.dateGMT());               // 
                            //tce2.WorkStartTime = tr.Start__c; // DateTime
                            tce2.WorkStartTime = u.formatToTime(tr.Start__c) + 'Z'; // DateTime
                            tce2.WorkEndDate = u.formatToDate(tr.End__c);
                            //tce2.WorkEndTime = tr.End__c; // DateTime
                            tce2.WorkEndTime = u.formatToTime(tr.End__c) + 'Z'; // DateTime
                            
                            cso.TimeConfirmations.TimeConfirmation.add(tce2); 
                        }
                    }
                }
            }
        }
        catch(Exception e)
        {
            String prevMsg = e.getMessage();
            String newMsg = 'setCustomerServiceOrder: ' + step + ' ' + prevMsg;
            debug(newMsg);
            e.setMessage(newMsg);
            throw e;
        }
        return retValue;    
    }

    /**
     * Sets the ERPStatusOrderClose in the root object to 'pending'
     * 
     * @param dataMap of obects to dispatch
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure with error messages
     * @param error true if some error has been occured
     */
    public override void setERPStatus(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs, Boolean error)
    {

        
        if(dataMap != null)
        {
            SCOrder__c order = (SCOrder__c)dataMap.get(ROOT);
                    
            if(order != null)
            {
                // abort the ERPStatus setting, when the order staus is already 'ok'
                if(order.ERPStatusOrderClose__c == 'ok')
                {
                    return;
                }
                // GMSGB PMS 36271: Auslösen von Interface Schnittstellen verhindern, wenn OderCreate noch nicht zurück ist
                else if(!testMode && order.ERPStatusOrderCreate__c != 'ok')
                {
                    return;
                }
                
                if(error)
                {
                    order.ERPStatusOrderClose__c = 'error';
                }
                else
                {   
                    order.ERPStatusOrderClose__c = 'pending';
                }   
                update order;
                debug('order after update: ' + order);
            }
        }        
    }
    
    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }
}
