/**
 * @(#)SCMapObjectViewControllerTest
 * Copyright 2012 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

 /**
 * @author Eugen Tiessen <etiessen@gms-online.de>
 * @version $Revision$, $Date$
 */

@isTest
private class SCMapObjectViewControllerTest {

    static testMethod void myUnitTest() {
        Test.startTest();
        
        SCHelperTestClass.createOrderTestSet(true);
        
        User u = 
        [
            SELECT 
                Id, Alias, FirstName, LastName,
                Street, PostalCode, City,
                GeoX__c, GeoY__c, Country
            FROM 
                User 
            WHERE 
                ID = :UserInfo.getUserId()
            LIMIT 1
        ];
        
        SCHelperTestClass.resources.add(new SCResource__c(    Employee__c = u.Id,
                                            Employee__r = u,
                                            alias_txt__c = SCHelperTestClass.createResAlias(u),
                                            FirstName_txt__c = u.FirstName, 
                                            LastName_txt__c = u.LastName, 
                                            //Scheduleable__c = true,
                                            EnableScheduling__c = true,
                                            Type__c = '2601',
                                            Description__c = u.FirstName + u.LastName
                                            ));
        SCHelperTestClass.resources.add(new SCResource__c(    Employee__c = u.Id,
                                            Employee__r = u,
                                            alias_txt__c = SCHelperTestClass.createResAlias(u),
                                            FirstName_txt__c = u.FirstName, 
                                            LastName_txt__c = u.LastName, 
                                            //Scheduleable__c = true,
                                            EnableScheduling__c = true,
                                            Type__c = '2601',
                                            Description__c = u.FirstName + u.LastName
                                            ));                                            
        
        SCHelperTestClass.resources.add(new SCResource__c(    Employee__c = u.Id,
                                            Employee__r = u,
                                            alias_txt__c = SCHelperTestClass.createResAlias(u),
                                            FirstName_txt__c = u.FirstName, 
                                            LastName_txt__c = u.LastName, 
                                            //Scheduleable__c = true,
                                            EnableScheduling__c = true,
                                            Type__c = '2601',
                                            Description__c = u.FirstName + u.LastName
                                            ));
        SCHelperTestClass.resources.add(new SCResource__c(    Employee__c = u.Id,
                                            Employee__r = u,
                                            alias_txt__c = SCHelperTestClass.createResAlias(u),
                                            FirstName_txt__c = u.FirstName, 
                                            LastName_txt__c = u.LastName, 
                                            //Scheduleable__c = true,
                                            EnableScheduling__c = true,
                                            Type__c = '2601',
                                            Description__c = u.FirstName + u.LastName
                                            ));                                            
        
        SCHelperTestClass.resources.add(new SCResource__c(    Employee__c = u.Id,
                                            Employee__r = u,
                                            alias_txt__c = SCHelperTestClass.createResAlias(u),
                                            FirstName_txt__c = u.FirstName, 
                                            LastName_txt__c = u.LastName, 
                                            //Scheduleable__c = true,
                                            EnableScheduling__c = true,
                                            Type__c = '2601',
                                            Description__c = u.FirstName + u.LastName
                                            ));
        SCHelperTestClass.resources.add(new SCResource__c(    Employee__c = u.Id,
                                            Employee__r = u,
                                            alias_txt__c = SCHelperTestClass.createResAlias(u),
                                            FirstName_txt__c = u.FirstName, 
                                            LastName_txt__c = u.LastName, 
                                            //Scheduleable__c = true,
                                            EnableScheduling__c = true,
                                            Type__c = '2601',
                                            Description__c = u.FirstName + u.LastName
                                            ));                                            
        
        SCHelperTestClass.resources.add(new SCResource__c(    Employee__c = u.Id,
                                            Employee__r = u,
                                            alias_txt__c = SCHelperTestClass.createResAlias(u),
                                            FirstName_txt__c = u.FirstName, 
                                            LastName_txt__c = u.LastName, 
                                            //Scheduleable__c = true,
                                            EnableScheduling__c = true,
                                            Type__c = '2601',
                                            Description__c = u.FirstName + u.LastName
                                            ));
        SCHelperTestClass.resources.add(new SCResource__c(    Employee__c = u.Id,
                                            Employee__r = u,
                                            alias_txt__c = SCHelperTestClass.createResAlias(u), 
                                            FirstName_txt__c = u.FirstName, 
                                            LastName_txt__c = u.LastName, 
                                            //Scheduleable__c = true,
                                            EnableScheduling__c = true,
                                            Type__c = '2601',
                                            Description__c = u.FirstName + u.LastName
                                            ));                                            
        
        upsert SCHelperTestClass.resources;
                                          
        SCHelperTestClass.createTestAssignments(SCHelperTestClass.resources,new List<SCStock__c>(),null,true);
       
        
        
        SCMapObjectViewController    movc = new SCMapObjectViewController( SCHelperTestClass.orderItem.id);
                                     movc = new SCMapObjectViewController( SCHelperTestClass.appointments.get(0).id);
                                     movc = new SCMapObjectViewController( SCHelperTestClass.resourceAssignments.get(0).id);
                                     
                                     //Errors
                                     movc = new SCMapObjectViewController( (SCHelperTestClass.orderItem.id + '').substring(0,3) + '123456789012' );
                                     movc = new SCMapObjectViewController( (SCHelperTestClass.appointments.get(0).id + '').substring(0,3) + '123456789012');
                                     movc = new SCMapObjectViewController( (SCHelperTestClass.resourceAssignments.get(0).id + '').substring(0,3) + '123456789012');
                                     movc = new SCMapObjectViewController( (SCHelperTestClass.orderItem.id + '').substring(0,12));
                                    
        
        Test.stopTest();  
    }
}
