/*
 * @(#)SCOrderNoteControllerTest.cls
 *
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * Testclass for the SCOrderNoteController.
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCOrderNoteControllerTest
{
    private static SCOrderNoteController onc;
    
    static testMethod void orderNoteControllerPositiv1()
    {
        Test.startTest();  
        
        SCHelperTestClass.createOrderTestSet(true);
        ApexPages.currentPage().getParameters().put('oid', SCHelperTestClass.order.Id); 
        
        onc = new SCOrderNoteController();
        
        onc.note.Title = 'Title of the note';
        onc.note.Body  = 'Body text of the note';
        
        onc.onCreateNote();
        onc.onCancel();
        
        Test.stopTest();  
    }
    
    static testMethod void orderNoteControllerAttachmentPositiv1()
    {
        Test.startTest();  
        
        SCHelperTestClass.createOrderTestSet(true);
        ApexPages.currentPage().getParameters().put('oid', SCHelperTestClass.order.Id); 
        
        onc = new SCOrderNoteController();
        onc.onCreateAttachment();
        
        onc.attachment.name = 'myFile.txt';
        onc.attachment.Body  = Blob.valueof('Body text of the attachment');
        onc.retURL = '/apex/' + SCHelperTestClass.order.Id;
        onc.onCreateAttachment();
        
        onc.onCancel();
        
        Test.stopTest();  
    }
    
    static testMethod void orderNoteControllerNegativ1()
    {
        Test.startTest();  
        
        SCHelperTestClass.createOrderTestSet(true);
        ApexPages.currentPage().getParameters().put('oid', SCHelperTestClass.order.Id); 
        
        onc = new SCOrderNoteController();
        
        onc.note.Title = null;
        onc.note.Body  = null;
        
        onc.onCreateNote();
        
        Test.stopTest();  
    }
    
    static testMethod void orderNoteControllerNegativ2()
    {
        Test.startTest();  
        
        onc = new SCOrderNoteController();
        onc.onCreateNote();
        
        Test.stopTest();  
    }
}
