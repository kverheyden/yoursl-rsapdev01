/*
 * @(#)SCReleaseJobTest.cls SCCloud    hs 29.Oct.2010
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author HS <hschroeder@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCReleaseJobTest 
{
    /**
     * Class under test
     */
    static testMethod void testReleaseJob()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        Database.insert(new SCCalendar__c(Name = appSettings.ASE_RELEASEJOB_CALENDAR__c));
        
        SchedulableContext ctx = null;
        // create job class
        SCReleaseJob job = new SCReleaseJob();
        // run job
        job.execute(ctx);
    }
}
