/*
 * @(#)SCInventoryUploadControllerTest.cls 
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This class tests the functionality of the main class
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest (SeeAllData = true)
private class SCInventoryUploadControllerTest
{
    private static SCInventoryUploadController iuc;

    static testMethod void inventoryUploadControllerPositiv1() 
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createTestArticles(true);
        SCHelperTestClass.createTestStockItem(SCHelperTestClass.stocks[0].id, SCHelperTestClass.article.id, true);
        
        Id inventoryId = SCboInventory.createInventory(SCHelperTestClass.stocks[0].id, 
                                                       'Test Description', 
                                                       '2013', 
                                                       true, 
                                                       Date.today());
                                                       
                                                       
        ApexPages.currentPage().getParameters().put('oid', inventoryId);
    
        iuc = new SCInventoryUploadController();
        iuc.input = SCHelperTestClass.articles[0].name + ' 2';
        
        /*
        SCInventory__c i = [ Select Stock__c From SCInventory__c Where Id = :inventoryId ];
        SCStock__c s = [Select ValuationType__c From SCStock__c Where Id = :i.Stock__c ];
        s.ValuationType__c = true;
        update s;
        */
        
        Test.startTest();
        
        iuc.processText();
        iuc.finish = true;
        iuc.processAgain();
        
        Test.stopTest();
    }
    
    static testMethod void inventoryUploadControllerPositiv2() 
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createTestArticles(true);
        SCHelperTestClass.createTestStockItem(SCHelperTestClass.stocks[0].id, SCHelperTestClass.article.id, true);
        
        Id inventoryId = SCboInventory.createInventory(SCHelperTestClass.stocks[0].id, 
                                                       'Test Description', 
                                                       '2013', 
                                                       true, 
                                                       Date.today());
                                                       
                                                       
        ApexPages.currentPage().getParameters().put('oid', inventoryId);
    
        iuc = new SCInventoryUploadController();
        
        SCInventory__c i = [ Select Stock__c From SCInventory__c Where Id = :inventoryId ];
        SCStock__c s = [Select ValuationType__c From SCStock__c Where Id = :i.Stock__c ];
        s.ValuationType__c = false;
        update s;
        
        List<SCInventoryItem__c> invItems = [Select toLabel(ValuationType__c), Article__r.Name From SCInventoryItem__c Where Inventory__c = : inventoryId ];
        
        iuc.input = invItems[0].Article__r.name + ' ' + invItems[0].ValuationType__c + ' 2';
        
        Test.startTest();
        
        iuc.processText();
        iuc.finish = true;
        iuc.processAgain();
        
        Test.stopTest();
    }
    
    static testMethod void inventoryUploadControllerPositiv3() 
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createTestArticles(true);
        SCHelperTestClass.createTestStockItem(SCHelperTestClass.stocks[0].id, SCHelperTestClass.article.id, true);
        
        Id inventoryId = SCboInventory.createInventory(SCHelperTestClass.stocks[0].id, 
                                                       'Test Description', 
                                                       '2013', 
                                                       true, 
                                                       Date.today());
                                                       
                                                       
        ApexPages.currentPage().getParameters().put('oid', inventoryId);
    
        iuc = new SCInventoryUploadController();
        
        SCInventory__c i = [ Select Stock__c From SCInventory__c Where Id = :inventoryId ];
        SCStock__c s = [Select ValuationType__c From SCStock__c Where Id = :i.Stock__c ];
        s.ValuationType__c = true;
        update s;
        
        List<SCInventoryItem__c> invItems = [Select toLabel(ValuationType__c), Article__r.Name From SCInventoryItem__c Where Inventory__c = : inventoryId ];
        
        iuc.input = invItems[0].Article__r.name + ' ' + invItems[0].ValuationType__c + ' 2';
        
        Test.startTest();
        
        iuc.processText();
        iuc.finish = true;
        iuc.processAgain();
        
        Test.stopTest();
    }
    
    static testMethod void inventoryUploadControllerNegativ1() 
    {
        iuc = new SCInventoryUploadController();
        
        Test.startTest();
        
        iuc.processText();
        
        Test.stopTest();
    }
    
    static testMethod void inventoryUploadControllerNegativ2() 
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createTestArticles(true);
        
        Id inventoryId = SCboInventory.createInventory(SCHelperTestClass.stocks[0].id, 
                                                       'Test Description', 
                                                       '2012', 
                                                       true, 
                                                       Date.today());
                                                       
        ApexPages.currentPage().getParameters().put('oid', inventoryId);
    
        iuc = new SCInventoryUploadController();
        iuc.input = SCHelperTestClass.articles[0].name + ' ZNEU A';
    
        Test.startTest();
        
        iuc.processText();

        iuc.processAgain();
        
        Test.stopTest();
    }
    
}
