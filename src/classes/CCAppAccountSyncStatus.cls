/**
* @author		Austen Buennemann
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				a.buennemann@yoursl.de
*
* @description	Part 1: Return Map of Accounts with sync to App relevance.
* 				Part 2: Set Account Owner if sync check successful.
*
* @date			08.09.2014
*
* Timeline:
* Name               DateTime                  Description
* Austen Buennemann  08.09.2014 16:39          Class created
*/

public with sharing class CCAppAccountSyncStatus {

/*
App Sync Check Description Logic:
IF  CCAccountRole__c.AccountRole__c =='ZR'  AND  CCAccountRole__c.MasterAccount__c != NULL AND Request__c.CeNumber__c  == Account.CeNumber__c  AND Account.Request__c  == Request.Id
IF CCAccountRole__c. EmployeeDeNumber__c == User.Id2__c.replace ('@cc-eag.de','')) 
THNN Account. OwnerId = User.Id
*/
    public Map<Id,Account> checkFromAccountRole(Set<Id> accSet){

    	 List<Account> relAccounts = [Select Id, CeNumber__c, RequestID__c From Account Where Id =: accSet];
    	 List<Request__c> requests = [Select Id, CeNumber__c From Request__c Where Account__c =: accSet];
    	 Map<Id,Account> syncAccounts = new Map<Id,Account>();
    	 if(relAccounts != null && requests != null){
    	 	for(Account account: relAccounts){
    	 		for(Request__c request : requests){
    	 			if(account.CeNumber__c != null && account.CeNumber__c == request.CeNumber__c 
    	 					&& account.RequestID__c != null && account.RequestID__c == request.Id){
    	 				syncAccounts.put(account.Id, account);
    	 			}
    	 		}
    	 	}
    	 }
    	 return syncAccounts;
    }
    

    public MAP<string,User> mapValidUsers = new MAP<string,User>();
    public list<string> listUserId2 = new list<string>();
    
    public void setAccountOwner(List<Account> accountUpdate){
    	
    	Set<Id> accIdSet= new Set<Id>();
    	for (Account account: accountUpdate){
    		if(account != null)accIdSet.add(account.Id);
    	}
    	
    	List<Account> accounts = [Select Id, Owner.Name From Account Where Id =: accIdSet];
    	
    	accIdSet = new Set<Id>();
    	if(accounts != null){
	    	for (Account account: accounts){	    		
	    		if(account != null && account.Owner.Name.toLowerCase().contains('sapapi'))accIdSet.add(account.Id);
	    	}
	
			if(accIdSet != null){
		    	List<CCAccountRole__c> scope = [Select MasterAccount__c, Id, ID2__c, EmployeeDeNumber__c, AccountRole__c 
		    										From CCAccountRole__c Where MasterAccount__c =: accIdSet];
		    										
		    	if(scope != null ){
			    	// Set for CCAppAccountSyncStatus
			        Set<Id> accset= new Set<Id>();
			        
			        // loop all AR?s
			        for(CCAccountRole__c AR : scope){
			            // check if it is a valid AccountRole == ?ZR? && MasterAccount__c != null
			            if(AR.AccountRole__c =='ZR' && AR.MasterAccount__c != NULL){
			                listUserId2.add(AR.EmployeeDeNumber__c + '@cc-eag.de');
			                // add AB for CCAppAccountSyncStatus
			                accset.add(AR.MasterAccount__c);
			            }
			        }
			        // if the list is not empty select the Users Info?s
			        if(listUserId2.size()>0){
			            SelectUser();
			        }
			        
			        CCAppAccountSyncStatus appchk = new CCAppAccountSyncStatus();
			    	Map<Id, Account> accSyncMap = appchk.checkFromAccountRole(accset);        
			        Map<ID, Account> toUpdateMap = new Map<Id,Account>();
			        
			        // prepare Account Update
			        for(CCAccountRole__c AR : scope)
			        {    
			             if(AR.AccountRole__c =='ZR' && AR.MasterAccount__c != NULL && accSyncMap.containsKey(AR.MasterAccount__c)){
			                if(mapValidUsers.get(AR.EmployeeDeNumber__c.toLowerCase())!=NULL)
			                {
			                    Account tmpAcc = new Account();
			                    tmpAcc.Id = AR.MasterAccount__c;
			                    tmpAcc.OwnerId = mapValidUsers.get(AR.EmployeeDeNumber__c.toLowerCase()).Id;
			                    toUpdateMap.put(tmpAcc.ID, tmpAcc );
			                }
			            }
			        }
			        
			        // update the Account?s if the list is not empty
			        if(toUpdateMap.keySet().size()>0)
			        {
			                update toUpdateMap.values();
			        }
		    	}
			}
    	}
    }

   // selecting all necessary Users
    public void SelectUser()
    {
        list<User> listTMPUsers = new list<User>();
        listTMPUsers = [SELECT Id, Name, IsActive, ID2__c FROM User WHERE IsActive=true AND ID2__c IN:listUserId2];
        if(listTMPUsers.size()>0)
        {
            String tmp_user;
            for(User tU: listTMPUsers)
            {
                if(tU.ID2__c != NULL)
                {
                    tmp_user = tU.ID2__c.toLowerCase();
                    mapValidUsers.put(tmp_user.replace('@cc-eag.de',''),tU);
                }
            }
        }
    }

}
