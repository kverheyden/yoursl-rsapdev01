/*
 * @(#)SCNotificationControllerTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Test class checking the controler functions for contract search
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCNotificationControllerTest
{
    private static SCNotificationController nc;

    static testMethod void notificationPositiv1() 
    {
        //SCHelperTestClass.createAccountObject('Customer', true);
        Account acc = new Account();
        SCNotification__c notif = new SCNotification__c(Account__c = acc.id);
        
        Test.startTest();
        
        //nc = new SCNotificationController();

        //nc.aid = acc.Id;
        //nc.getNotification();
        
        Test.stopTest();
    }

    static testMethod void testNotificationProduct() 
    {
        SCHelperTestClass.createOrderTestSet2(true);
        //SCHelperTestClass2.createNotificationProducts(true);
        SCboOrder boOrder = new SCboOrder();
        boOrder.readById(SCHelperTestClass.order.Id);
        
        Test.startTest();

        nc = new SCNotificationController();

        nc.parboOrder = boOrder;
        nc.parboOrderItem = boOrder.boOrderItems.get(0);
        System.assertEquals(true, nc.isNotificationProduct);
        
        List<SCNotificationController.NotificationProdObj> notifProds = nc.getNotificationProd();
        //System.assert(notifProds.size() >= 2);

        //List<SelectOption> actions = notifProds[0].getActionList();
        //System.assertEquals(2, actions.size());
        
        nc.callAvoidance = '1';
        nc.setCallAvoidance();
        //System.assertEquals(true, nc.parboOrder.order.CallAvoidance__c);
        //System.assertEquals(System.Label.SC_app_CallAvoidance, nc.parboOrder.order.Description__c);
        
        Test.stopTest();
    }
}
