/**
* @author		Austen Buennemann
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				a.buennemann@yoursl.de
*
* @description	The class exports Salesforce SalesOrder__c records with SalesOrderItem__c entries to SAP
*
* @date			21.05.2014
*
*/

public without sharing class CCWCSalesOrder extends SCInterfaceExportBase{
	private static SalesOrder__c RequestItem;
	private static List<SalesOrderItem__c> RequestLineItem;

    public CCWCSalesOrder()
    {
    }  
    
    public CCWCSalesOrder(String interfaceName, String interfaceHandler, String refType, String subclassName)
    {
        super(interfaceName, interfaceHandler, refType, subclassName);
    }
    
    public CCWCSalesOrder(String interfaceName, String interfaceHandler, String refType, SalesOrder__c item, List<SalesOrderItem__c> lineitems)
    {
    	RequestItem = item;
    	RequestLineItem = lineitems;
    }
          
     /**
     * Prepare to make the web service callout synchrounously or asynchronously (future)
     *
     * ### Set the interface name, its interface handler and a refType for head id in an interface object!
     *
     * @param ibid      SalesOrder__c id
     * @param testMode true: emulate call
     * @param item String list with SalesOrder__c elements
     * @param lineitems String list SalesOrderItem__c elements
     */
     
    @future(callout=true)
	public static void futurecallout(String ibid, boolean testMode, String item, String[] lineitems){											
		SalesOrder__c requestentry = (SalesOrder__c)JSON.deserialize(item, SalesOrder__c.class);
		List<SalesOrderItem__c> reqlineitems = new List<SalesOrderItem__c>();
		if(lineitems != null){
			for(String lineitem: lineitems){
				reqlineitems.add((SalesOrderItem__c)JSON.deserialize(lineitem, SalesOrderItem__c.class));
			}
			CCWCSalesOrder.callout(ibid, false, testMode, requestentry, reqlineitems);
		}else{
			CCWCSalesOrder.callout(ibid, false, testMode, requestentry, null);
		}
	}
    
    public static String callout(String ibid, boolean async, boolean testMode, SalesOrder__c item, List<SalesOrderItem__c> lineitems)
    {
        String interfaceName = 'SalesOrderSFDC_Out';
        String interfaceHandler = 'CCWCSalesOrderHandler';
        String refType = 'SalesOrder__c';
        CCWCSalesOrder request = new CCWCSalesOrder(interfaceName, interfaceHandler, refType, item, lineitems);
        debug('Request ID: ' + ibid);
        return request.calloutCore(ibid, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCSalesOrder');
        
    }  

     /**
     * Reads an SalesOrder__c record to send
     *
     * @param Id SalesOrder__c
     * @param retValue Map Object
     * @param testMode if true no call is made, because in the test mode the salesforce framework prevent it.
     */
    public override void readSalesforceData(String Id, Map<String, Object> retValue, Boolean testMode)
    {    	
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();                
        retValue.put('ROOT', RequestItem);
        debug('SalesOrder__c: ' + retValue);
    }

    /**
     * Fills the send structure and sends the call to SAP
     * 
     * @param dataMap with the SalesOrder__c Item under the key ROOT
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure
     *           message = data sent to ERP
     *           message_v1 = endpoint 
     *           message_v2 = a return value of web service 
     *           message_v3 = a mandatory fields error
     *
     */
      
    public override Boolean fillAndSendERPData(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs)
    {
        Boolean retValue = true;
        SalesOrder__c request = (SalesOrder__c)dataMap.get('ROOT');
        // instantiate the client of the web service
        piCceagDeSfdc_SalesOrder.CCWSSalesOrderRequestResponse_OutPort ws = new piCceagDeSfdc_SalesOrder.CCWSSalesOrderRequestResponse_OutPort();
        
        // sets the basic authorization for the web service
        ws.inputHttpHeaders_x = u.getBasicAuth();
        
        //Test
        //Map<String, String> autMap = new Map<String, String>();
        //autMap.put('Authorization: Basic', 'TEST');
		//ws.inputHttpHeaders_x = autMap;
		
        // sets the endpoint of the web service
        ws.endpoint_x  = u.getEndpoint('SalesOrder');
        
        //Test
        //ws.endpoint_x  = 'http://requestb.in/1gl49z81'; 
        
        
        debug('endpoint: ' + ws.endpoint_x);
        rs.message_v1 = ws.endpoint_x;
		ws.timeout_x = u.getTimeOut();
        String errormessage = '';
        
        
        try
        {

            // set the data to the body
            piCceagDeSfdc_SalesOrder.BAPISDHD1 ORDER_HEADER_IN = new piCceagDeSfdc_SalesOrder.BAPISDHD1 ();             
            
            piCceagDeSfdc_SalesOrder.ORDER_PARTNERS_element ORDER_PARTNERS = new piCceagDeSfdc_SalesOrder.ORDER_PARTNERS_element();
            List<piCceagDeSfdc_SalesOrder.BAPIPARTNR> items_ORDER_PARTNERS = new List<piCceagDeSfdc_SalesOrder.BAPIPARTNR>();
            
            
            piCceagDeSfdc_SalesOrder.ORDER_ITEMS_IN_element ORDER_ITEMS_IN = new piCceagDeSfdc_SalesOrder.ORDER_ITEMS_IN_element();
            List<piCceagDeSfdc_SalesOrder.BAPIITEMIN> items_ORDER_ITEMS_IN = new List<piCceagDeSfdc_SalesOrder.BAPIITEMIN>();
            

			piCceagDeSfdc_SalesOrder.ORDER_SCHEDULES_IN_element ORDER_SCHEDULES_IN = new piCceagDeSfdc_SalesOrder.ORDER_SCHEDULES_IN_element();
			List<piCceagDeSfdc_SalesOrder.BAPISCHDL> items_ORDER_SCHEDULES_IN = new List<piCceagDeSfdc_SalesOrder.BAPISCHDL>();
            
       
            ORDER_PARTNERS.item = items_ORDER_PARTNERS;  
            ORDER_ITEMS_IN.item = items_ORDER_ITEMS_IN;     
            ORDER_SCHEDULES_IN.item = items_ORDER_SCHEDULES_IN;  
            setRequesteData(ORDER_HEADER_IN, items_ORDER_PARTNERS, items_ORDER_ITEMS_IN, items_ORDER_SCHEDULES_IN, request, u, testMode);      
            
            
            String jsonsfdcItems = JSON.serialize(request);
            String fromJSONMapRequest = getDataFromJSON(jsonsfdcItems);
            debug('from json SalesOrder__c Item: ' + fromJSONMapRequest);
            
			jsonsfdcItems = JSON.serialize(RequestLineItem);
            fromJSONMapRequest = getDataFromJSON(jsonsfdcItems);
            debug('from json SalesOrderItem__c: ' + fromJSONMapRequest);             
                                                
            rs.message = '\n messageData: ' + request + ' ' + RequestLineItem;
            
            // check if there are missing mandatory fields
            if(rs.message_v3 == null || rs.message_v3 == '')
            {
                // All mandatory fields are filled
                // callout
                if(!testMode)
                {
                    // It is not the test from the test class
                    // go
                    errormessage = 'Call ws.SalesOrder: \n\n';
                    rs.setCounter(1);
                    ws.CreateOrder(ORDER_HEADER_IN, null, null, ORDER_ITEMS_IN, ORDER_PARTNERS, ORDER_SCHEDULES_IN, null);
                    rs.Message_v2 = 'void';
                }
            }
        }
        catch(Exception e)
        {
            String prevMsg = e.getMessage();
            String newMsg = 'fillAndSendERPData: ' + errormessage + ' \n\n ' + prevMsg;
            debug(newMsg);
            e.setMessage(newMsg);
            throw e;

        }
                
        return retValue;
    }
   
     /**
     * sets the call out Request structure with data form an SalesOrder__c
     *
     * @param ORDER_HEADER_IN callout Sales Order Header structure
     * @param items_ORDER_PARTNERS callout Partner Lineitem structure 
     * @param items_ORDER_ITEMS_IN callout Order Items structure
     * @param items_ORDER_SCHEDULES_IN callout Schedule Items structure
     * @param item Sales Order Header item with input data
     * @param u instance of the util class CCWSUtil
     * @param testMode
     * @return ###
     */
    public String setRequesteData(piCceagDeSfdc_SalesOrder.BAPISDHD1 ORDER_HEADER_IN, 
    								List<piCceagDeSfdc_SalesOrder.BAPIPARTNR> items_ORDER_PARTNERS, 
    								List<piCceagDeSfdc_SalesOrder.BAPIITEMIN> items_ORDER_ITEMS_IN, 
    								List<piCceagDeSfdc_SalesOrder.BAPISCHDL> items_ORDER_SCHEDULES_IN, SalesOrder__c item, CCWSUtil u, Boolean testMode)
    {
        CCSettings__c ccset = CCSettings__c.getInstance();
    
        String retValue = '';
     	      
        try 
        {
        	ORDER_HEADER_IN.REQ_DATE_H = string.valueOf(item.RequestedDeliveryDate__c);
			ORDER_HEADER_IN.PURCH_NO_C = item.Name;	
			if(item.Type__c=='Paid Order'){
				ORDER_HEADER_IN.DOC_TYPE = 'ZOR';
			}
			else if(item.Type__c=='Free Order'){
				ORDER_HEADER_IN.DOC_TYPE ='ZFRE'; 
			}
			
            piCceagDeSfdc_SalesOrder.BAPIPARTNR item_ORDER_PARTNERS = new piCceagDeSfdc_SalesOrder.BAPIPARTNR();
            item_ORDER_PARTNERS.PARTN_NUMB = item.Account__r.ID2__c;
            item_ORDER_PARTNERS.PARTN_ROLE = 'WE';
            items_ORDER_PARTNERS.add(item_ORDER_PARTNERS);
			
						
          	if (RequestLineItem != null){
          		integer counter = 0;
	        	for (SalesOrderItem__c lineitem : RequestLineItem){
	        		counter++;
	        		/*
	        		Note: ORDER_PARTNERS note once only!
		            piCceagDeSfdc_SalesOrder.BAPIPARTNR item_ORDER_PARTNERS = new piCceagDeSfdc_SalesOrder.BAPIPARTNR();
		            item_ORDER_PARTNERS.ITM_NUMBER = string.valueOf(counter);
		            item_ORDER_PARTNERS.PARTN_NUMB = lineitem.ShipToCustomerNumber__c;
		            item_ORDER_PARTNERS.PARTN_ROLE = 'WE';
		            items_ORDER_PARTNERS.add(item_ORDER_PARTNERS);
		            */
		            
		 			piCceagDeSfdc_SalesOrder.BAPIITEMIN item_ORDER_ITEMS_IN = new piCceagDeSfdc_SalesOrder.BAPIITEMIN();         
		 			item_ORDER_ITEMS_IN.ITM_NUMBER = string.valueOf(counter);
		 			item_ORDER_ITEMS_IN.MATERIAL = lineitem.ExternalArticleNumber__c;   
		 			items_ORDER_ITEMS_IN.add(item_ORDER_ITEMS_IN);
		 			
		 			piCceagDeSfdc_SalesOrder.BAPISCHDL item_ORDER_SCHEDULES_IN = new piCceagDeSfdc_SalesOrder.BAPISCHDL();         
		 			item_ORDER_SCHEDULES_IN.ITM_NUMBER = string.valueOf(counter);
		 			//item_ORDER_SCHEDULES_IN.SCHED_LINE  = '10'; 
		 			//item_ORDER_SCHEDULES_IN.REQ_DATE  = '2014-05-14';
		 			item_ORDER_SCHEDULES_IN.REQ_QTY  = string.valueOf(lineitem.Quantity__c);		
		 			items_ORDER_SCHEDULES_IN.add(item_ORDER_SCHEDULES_IN);
	        	}
         	}
						 
        }
        catch(Exception e)
        {
            String prevMsg = e.getMessage();
            String newMsg = 'set_SalesOrder_Data: ' + ORDER_HEADER_IN + ' ' + items_ORDER_PARTNERS + ' ' + items_ORDER_ITEMS_IN + ' ' + items_ORDER_SCHEDULES_IN + ' ' + prevMsg;
            debug(newMsg);
            e.setMessage(newMsg);
            throw e;
        }
        
        return retValue;    
    }

   /**
     * Sets the ERPStatusEquipmentUpdate__c in the root object to 'pending'
     * 
     * @param dataMap of objects to dispatch
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure with error messages
     * @param error true if some error has occurred
     */
    public override void setERPStatus(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs, Boolean error)
    {
    }
    

    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }

}
