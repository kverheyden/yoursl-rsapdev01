/*
 * @(#)SCbtcStockItemDeltaOverviewLoad.cls 
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Used to load stick item data in the table SCStockItemDeltaOverview
 */

global with sharing class SCbtcStockItemDeltaOverviewLoad extends SCbtcBase 
       implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts  
{
   // Activates the e-mail notification (see CCSettings) to forward the service report 
    private Boolean emailNotificationEnable;
    private String  emailNotificationTemplate;
    
    // Required to create the pdf document in the batch job
    private String sessionid;

    // we process one pdf export per execute (to avoid governor limits)
    private static final Integer batchSize = 1;
    private static final Integer batchClearingDelta = 30;  // the data must be at least this number of seconds old (settling time)

    private ID batchprocessid = null;
    public ID getBatchProcessId()
    {
        return batchprocessid;
    }
    String mode = 'productive';
    Integer max_cycles = 0;
    public String extraCondition = null;

    // the interface log id
    public Id ifLogID = null;
    // Query for batch job 
    private String query; 




    //---<Helper functions to start the batch job>---------------------------------------

   /*
    * Starts the asynchronosu bach job. Use this method in the external job scheduler  
    * @param see below
    */
    public static ID asyncProcessAll(Integer max_cycles, String mode)
    {
        System.debug('###mode: ' + mode);
        SCbtcStockItemDeltaOverviewLoad btc = new SCbtcStockItemDeltaOverviewLoad(max_cycles, mode);

        btc.batchprocessid  = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    }  

   /*
    * Starts the asynchronosu bach job. Use this method in the external job scheduler  
    * You can specify an optional additional condition that is added to the statement
    * @param see below
    */
    public static ID asyncProcessAll(Integer max_cycles, String mode, String extraCondition)
    {
        System.debug('###mode: ' + mode);
        SCbtcStockItemDeltaOverviewLoad btc = new SCbtcStockItemDeltaOverviewLoad(max_cycles, mode, extraCondition);

        btc.batchprocessid  = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    }  

   /*
    * Starts the asynchronosu bach job to process exactly the specified assigment
    * @param see below
    */
    public static ID asyncProcess(ID iflogID, String mode)
    {
        System.debug('###mode: ' + mode);
        SCbtcStockItemDeltaOverviewLoad btc = new SCbtcStockItemDeltaOverviewLoad(iflogID, 0, mode);
        btc.batchprocessid  = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    }  


   /*
    * Starts the synchronosu bach job to process exactly the specified assigment
    * @param see below
    */

    public static List<SCStockItemDeltaOverview__c> syncProcess(ID ifLogID, String mode)
    {
        System.debug('###mode: ' + mode);
        SCbtcStockItemDeltaOverviewLoad btc = new SCbtcStockItemDeltaOverviewLoad(ifLogID, 0, mode);
        btc.startCore(false, false); // prepares the query 
        
        List<SObject> mList = Database.query(btc.query); // runs the query        
        List<SCStockItemDeltaOverview__c> retValue = btc.executeCore(mList);
        return retValue;
    }  

    //---<constructurs>------------------------------------------------------------------

    /**
     * Constructor
     * @param see below
     */
    public SCbtcStockItemDeltaOverviewLoad(Integer max_cycles, String mode)
    {
        this(null, max_cycles, mode, null);
    }

    /**
     * Constructor
     * @param see below
     */
    public SCbtcStockItemDeltaOverviewLoad(Id assignmentid, Integer max_cycles, String mode)
    {
        this(assignmentid, max_cycles, mode, null);
    }


    /**
     * Constructor
     * @param see below
     */
    public SCbtcStockItemDeltaOverviewLoad(Integer max_cycles, String mode, String extraCondition)
    {
        this(null, max_cycles, mode, extraCondition);
    }


    /**
     * Constructor
     * @param ifLogID   the id of the interfaceLog to process
     * @param max_cycles    maximum number of execution processes (0: not limited, 1: process only 1, ....)
     * @mode               'test' or 'trace'    trace additional information during test execution
     *                     'productive'         no additional tracing
     * @extraCondition     additional conditions (see soql statement below)
     */
    public SCbtcStockItemDeltaOverviewLoad(Id ifLogID, Integer max_cycles, String mode, String extraCondition)
    {
        this.ifLogID = ifLogID;
        this.max_cycles = max_cycles;
        this.mode = mode;
        this.extraCondition = extraCondition;
        
        // required to be able to call the pdf creation web service in a valid user context 
        this.sessionid = UserInfo.getSessionId();
            
        // evaluate the CCEAG settings of the current (API) user for this process and store it in the batch job for later access
        CCSettings__c ccSettings = CCSettings__c.getInstance();
        
        // check if the automati e-mail notification is enabled        
        this.emailNotificationEnable = ccSettings.IFEnableEmailNotification__c == 1;
        
    }


    //---<batch preparation and processing>------------------------------------------------------------------

   /*
    * Called by the framework when the batch job is started. We construct the soql statement
    * @param BC the batch context
    * @return the query locator with the selected mainenances
    */
    global override Database.QueryLocator start(Database.BatchableContext BC)
    {
        debug('start');
        Boolean aborted = abortOneOfTwoSameJobsRunning(BC.getJobId(), 'SCbtcStockItemDeltaOverviewLoad', 'start');
        return startCore(true, aborted);
    } // start

   /*
    * Prepares the SOQL statement that is used to select the records that have to be processed
    * @param returnsRetValue   true: executes the query and returns the query locator. false: null only the soql statement is created
    * @param aborted
    * @return the query locator that represents the records to be processed
    */
    public Database.QueryLocator startCore(Boolean returnsRetValue, Boolean aborted)
    {

             /*   SELECT Id, Name,Plant__r.Name,
                               (SELECT Id,Resource__r.Alias__c, Resource__c
                                               FROM Resource_Assignments__r
                                               WHERE ValidFrom__c <= TODAY
											   AND ValidTo__c >= TODAY
                                               AND Stock__r.Name LIKE 'S%'),
                               ( SELECT Id,  Qty__c, ValuationType__c, Article__r.Name
                                               FROM StockItem__r )
                               FROM  SCStock__c
                               WHERE Id IN
                               ( SELECT Stock__c
                                  FROM  SCResourceAssignment__c
                                  WHERE  ValidFrom__c <= TODAY
                                  AND ValidTo__c >= TODAY
                                  AND Stock__r.Name LIKE 'S%')*/
                
                //Wird das benötig?
                //LIMIT ???];             


        debug('start: after checking statement');

       	query = 'SELECT Id, Name,Plant__r.Name, ' +
                '               (SELECT Id,Resource__r.Alias__c, Resource__c' +
                '                               FROM Resource_Assignments__r' +
                '                               WHERE ValidFrom__c <= TODAY' +
				'							   AND ValidTo__c >= TODAY' +
                '                               AND Stock__r.Name LIKE \'S%\'),' +
                '               ( SELECT Id,  Qty__c, ValuationType__c, Article__r.Name' +
                '                               FROM StockItem__r )' +
                '               FROM  SCStock__c' +
                '               WHERE Id IN' +
                '               ( SELECT Stock__c' +
                '                  				FROM  SCResourceAssignment__c' +
                '                  				WHERE  ValidFrom__c <= TODAY' +
                '                  				AND ValidTo__c >= TODAY' +
                '                  				AND Stock__r.Name LIKE \'S%\')';


        Boolean isRunning = isJobRunningForCallerCountry('SCbtcStockItemDeltaOverviewLoad') == '1';
        
        // do not read any data if aborted or already running 
        if(aborted || isRunning)
        {
            query += ' limit 0';
        }
        else
        {
            if(max_cycles > 0)
            {
                query += ' limit ' + max_cycles;
            }
        }               

        debug('query: ' + query);
        
        if(returnsRetValue)
        {
            return Database.getQueryLocator(query);
        }
        return null;
    }

   /*
    * Called for each batch of records to process.
    * @param BC the batch context
    * @param scope the list records to be processed
    */
    global override void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        debug('execute');
        if(abortOneOfTwoSameJobsRunning(BC.getJobId(), 'SCbtcStockItemDeltaOverviewLoad', 'execute'))
        {
            return;
        }
        executeCore(scope);
    } // execute

   /*
    * Called to execute each batch
    */
    public List<SCStockItemDeltaOverview__c> executeCore(List<sObject> scope)
    {
    	debug('executeCore');
        List<SCStockItemDeltaOverview__c> overviewList = new List<SCStockItemDeltaOverview__c>();
        for(sObject rec: scope)
        {
            String errorText = '';
            SCStock__c stock = (SCStock__c)rec;
        	system.debug('#### processing stock ' + stock.name);
            for(SCStockItem__c stockItem : stock.stockItem__r)
            {
            	SCStockItemDeltaOverview__c overviewItem = new SCStockItemDeltaOverview__c();
                overviewItem.Stock__c = stock.Name;
                overviewItem.Plant__c = stock.Plant__r.Name;
                               
                //Used for upsert
                overviewItem.ID2__c   		= stockItem.Id;
                overviewItem.StockItem__c   = stockItem.Id;
                overviewItem.QtyCP__c       = stockItem.Qty__c;
                overviewItem.Article__c     	= stockItem.Article__r.Name;
                overviewItem.ValuationType__c   = stockItem.ValuationType__c;
                               
                overviewItem.LastModifiedDateByCP__c = System.Now();
                            
                if(stock.Resource_Assignments__r != null && stock.Resource_Assignments__r.size() > 0)
                {
                 	overviewItem.Resource__c 	= stock.Resource_Assignments__r.get(0).Resource__c;
                }
                overviewList.add(overviewItem);
        	}
        }

		system.debug('#### stockitem delta overview items: ' + overviewList);
		upsert overviewList Id2__c;
		
		return overviewList;

    }
    


   /*
    * Called by the framework when the batch job has been completed. 
    * @param BC the batch context
    */
    global override void finish(Database.BatchableContext BC)
    {
    } // finish


	/*
	* Checks if the given ErrorMessage belongs to the error messages that indicate that an
	* automated reprocess can be successful
	* @param errorMessage: The error message
	*
	* The following ErrorMessages should be detected
	* The following Error messages should be reprocessed:
	* 
	* Error: Fehler bei HTTP-Zugriff: IF_HTTP_CLIENT->RECEIVE 1 HTTPIO_PLG_CANCELED
	* Error: Auftrag 400013773 ist gesperrt und kann nicht geändert werden
	* Error: Meldung 10094319 durch ALEREMOTE gesperrt, Der Status des Auftrags 200444935 lässt keine Änderungen zu
	* Error:IO Exception: Remote host closed connection during handshake
	* Error: HTTP-Fehler: 500 Internal Server Error Internal error
	* Error:You have uncommitted work pending. Please commit or rollback before calling out
	* Error:IO Exception: Read timed out
	* Error:fillAndSendERPData: Call ws.CustomerServiceOrderEquipmentUpdate_Out: 
	* IO Exception: Remote host closed connection during handshake"
	* Error:Web service callout failed: Failed to get next element
	* Error:fillAndSendERPData: Call ws.CustomerServiceOrderEquipmentUpdate_Out: 
	* Web service callout failed: Unexpected element. Parser was expecting element 'http://schemas.xmlsoap.org/soap/envelope/:Envelope' but found ':html'"
	* Error:Web service callout failed: Unexpected element. Parser was expecting element 'http://schemas.xmlsoap.org/soap/envelope/:Envelope' but found ':html'
	* Error:IO Exception: Unable to tunnel through proxy. Proxy returns "HTTP/1.0 503 Service Unavailable"
	*
	*
	*/
	public static boolean checkErrorMessage(String errorMessage)
	{
		if(errorMessage.contains('IO Exception:') ||
			errorMessage.contains('HTTP-Fehler:') ||
			errorMessage.contains('Web service callout failed:') ||
			errorMessage.contains('Error: Fehler bei HTTP-Zugriff: IF_HTTP_CLIENT->RECEIVE 1 HTTPIO_PLG_CANCELED') ||
		  	(errorMessage.contains('Error: Auftrag') && errorMessage.contains('ist gesperrt und kann nicht geändert werden' )) ||
			errorMessage.contains('Error:IO Exception: Remote host closed connection during handshake') ||
			errorMessage.contains('Error: HTTP-Fehler: 500 Internal Server Error Internal error') ||
			//errorMessage.contains('Error:You have uncommitted work pending. Please commit or rollback before calling out') ||
			errorMessage.contains('Error:IO Exception: Read timed out') ||
			errorMessage.contains('Error:Web service callout failed: Failed to get next element') ||
			errorMessage.contains('Web service callout failed: Unexpected element. Parser was expecting element') ||
			errorMessage.contains('Error:IO Exception: Unable to tunnel through proxy. Proxy returns \"HTTP/1.0 503 Service Unavailable\"')
		)
		{
			return true;
		}
		
		return false;
	}

   /*
    * Checks if there are any ERP errors that would prevent us from creating a callout. 
	* An order should have a successful orderCreate callout
    * @return true if the callout is possible
    */
    public static SCOrder__c selcetOrder(String orderId)
    {
		List<SCOrder__c> items = [select id, name, ERPOrderNo__c, ERPStatusOrderCreate__c,ERPStatusOrderClose__c, ERPStatusEquipmentUpdate__c, 
			ERPStatusExternalAssignmentAdd__c,  ERPStatusExternalAssignmentRem__c, 	ERPStatusMaterialMovement__c,
			ERPStatusMaterialReservation__c, ERPStatusArchiveDocumentInsert__c
			from SCOrder__c where id =: orderId ];
		if(items.size() > 0)
		{
			return items[0];
		}	
		else
		{
			return null;
		}
			
    }
    
    
    /*
	* Selects one List of ExternalAssignemnt based on a name (OXA-1234567890)
	* @param exOpName The ExternalAssignemnt Name
	* @return an ExternalAssigment
	*/
    public static SCOrderExternalAssignment__c  selectExternalOperationByName(String exOpName)
    {
		List<SCOrderExternalAssignment__c> assignemnts = [select id, name
			from SCOrderExternalAssignment__c where name =: exOpName ]; 	
		if (assignemnts.size() > 0)
		{	
			return assignemnts[0];
		}
		else
		{
			return null;
		}	
    }
    
    /*
	* Selects one List of ExternalAssignemnt based on a OrderIn and the guessed status 
	* @param orderId The OrderID
	* @param orderId A List of possibel status 
	* @return an ExternalAssigment
	*/
    public static SCOrderExternalAssignment__c  selectExternalOperationByOrderAndStatus(String orderId, List<String> status)
    {
		List<SCOrderExternalAssignment__c> assignemnts = [select id, name, Status__c
			from SCOrderExternalAssignment__c where Order__c =: orderId and status__c in: status]; 	
		if (assignemnts.size() > 0)
		{	
			return assignemnts[0];
		}
		else
		{
			return null;
		}		
    }
    
    /*
	* Selects a List of Material Movements based on a List om MM Names 
	* @param a List of MM Names
	* @return a List of Material Movements
	*/
    public static List<SCMaterialMovement__c>  selectMaterialMovments(List<String> mmNameList)
    {
		return [select id, name, ERPStatus__c
			from SCMaterialMovement__c where name in: mmNameList ]; 	
    }
    
    
    
    /*
	* Extracts the ExternalAssignemnt Names OXA-1234567890 from the input string 
	* @param String data Input Error Message
	* @return a List of ExternalAssignemnt Names
	*/
    public static String extractExternalOperationName(String data)
    {
    	Integer indexOf = data.indexOf('OXA-');
    	if(indexOf > -1)
    	{
    		return data.substring(indexOf,Math.min(indexOf+14,data.length()));
    	}
    	else
    	{
    		return '';
    	}
    	
    }
    

    /*
	* Extracts the MaterialMovemnet Names MM-1234567890 from the input string 
	* @param String data Input Error Message
	* @return a List of MM Names
	*/
    public static List<String> extractMaterialMovementNames(String data)
    {
    	List<String> mmNames = new List<String>();
    	Boolean continoue = true;
    	String wc = data; 
    	
    	while(continoue)
    	{
	    	Integer indexOf = wc.indexOf('MM-');
	    	if(indexOf > -1)
	    	{
	    		mmNames.add(wc.substring(indexOf,Math.min(indexOf+13,wc.length())));
	    		wc = wc.substring(Math.min(indexOf+13,wc.length()), wc.length());
	    	}
	    	else
	    	{
	    		continoue = false;
	    	}
    	}
    	
    	return mmNames;
    	
    }
    
    

    
    

   /*
    * Checks if there are any ERP errors that would prevent us from creating a callout. 
	* An order should have a successful orderCreate callout
    * @return true if the callout is possible
    */
    public static Boolean canCallout(SCOrder__c order)
    {
		return (order != null && 
                order.ERPOrderNo__c != null && 
                order.ERPStatusOrderCreate__c == 'ok' 
                );
    }

	/*
	* Debug method
	*/
    private void debug(String text)
    {
        if(mode.equalsIgnoreCase('test') || mode.equalsIgnoreCase('trace'))
        {
            System.debug('###...................' + text);
        }
    }
    
}
