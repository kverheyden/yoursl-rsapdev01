@isTest
private class CCWCRestCdeOrderTest{
	static testMethod void testDoPost() {
		/*insert test data
    	*********************/ 
		CCWCRestCdeOrder.CdeOrderWrapper newOrder = new CCWCRestCdeOrder.CdeOrderWrapper();
		CCWCRestCdeOrder.CdeOrderItemWrapper newOrderItem = new CCWCRestCdeOrder.CdeOrderItemWrapper();
		newOrder.Contact = 'Active';
		newOrder.Street = 'Some street';
		newOrder.Contact = 'Order Contact';
		newOrder.EarliestDeliveryDate = '2014-09-08';
		newOrder.Email = 'some@mail.com';
		newOrder.Mobile = '01723338888';
		newOrder.Phone = '03000449908908';
		newOrder.RequestedDeliveryDate = '2014-09-05';
		
		newOrder.CdeOrderItems = new List<CCWCRestCdeOrder.CdeOrderItemWrapper>();
		newOrder.CdeOrderItems.add(newOrderItem);
		
		String jsonMsg = JSON.serialize(newOrder);
		System.debug('JSON ' + jsonMsg);
		/*Start the test...
    	*************************/
		Test.startTest();
		
		RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.requestURI = '/CdeOrder/';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonMsg);
        RestContext.request = req;
        RestContext.response = res;
        
        CCWCRestCdeOrder.ResponseWrapper resp = new CCWCRestCdeOrder.ResponseWrapper();
        CCWCRestCdeOrder.doPost(newOrder);
        System.debug('URI: ' + RestContext.Request.RequestURI);
        System.debug('BODY: ' + RestContext.Request.RequestBody.toString());
        System.assert(resp != null);
        
        Test.stopTest();
	}

}
