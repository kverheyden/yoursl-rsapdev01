/**
 * @created 2014-11-03T10:00:00+02:00
 * @author "thomas richter" <thomas@meformobile.com>
 */
@isTest
public class CCAccountRoleSetSoldToTriggerTest 
{
    /***********************************************************
     ***********************************************************
     **** Account.IsSoldTo 
     **** isSoldTo = (role='AG' & master=slave & status='active')
	 ***********************************************************
	 ***********************************************************/
    static testMethod void testIsSoldToInsertActive()
    {        
        Account account = new Account(Name='Account');
        insert account;
        
        CCAccountRole__c role = new CCAccountRole__c(AccountRole__c = 'AG',Status__c = 'Active',MasterAccount__c = account.Id,SlaveAccount__c = account.Id);
        
        Test.startTest();
        insert role;
		Test.stopTest();        
        
        account = [SELECT Id, IsSoldTo__c FROM Account WHERE Id=:account.Id];
        System.assertEquals(true, account.IsSoldTo__c);
    }
    static testMethod void testIsSoldToInsertNotActive()
    {        
        Account account = new Account(Name='Account', IsSoldTo__c = false);
        insert account;
                
        Test.startTest();
        CCAccountRole__c role = new CCAccountRole__c(AccountRole__c = 'AG',Status__c = '',MasterAccount__c = account.Id, SlaveAccount__c = account.Id);
        insert role;
		Test.stopTest();
        
        Account tAccount = [SELECT Id, IsSoldTo__c FROM Account WHERE Id=:account.Id];        
        System.assertEquals(false, tAccount.IsSoldTo__c);
    }    
    static testMethod void testIsSoldToInsertNotAG()
    {        
        Account account = new Account(Name='Account');
        insert account;
        
        CCAccountRole__c role = new CCAccountRole__c(AccountRole__c = 'WE',Status__c = 'Active',MasterAccount__c = account.Id,SlaveAccount__c = account.Id);
        
        Test.startTest();
        insert role;
		Test.stopTest();        
        
        account = [SELECT Id, IsSoldTo__c FROM Account WHERE Id=:account.Id];        
        System.assertEquals(false, account.IsSoldTo__c);
    }    
    static testMethod void testIsSoldToInsertDifferentAccounts()
    {        
        Account account = new Account(Name='Account');
        insert account;
        Account account2 = new Account(Name='Account2');
        insert account2;
        
        CCAccountRole__c role = new CCAccountRole__c(AccountRole__c = 'AG',Status__c = 'Active',MasterAccount__c = account.Id,SlaveAccount__c = account2.Id);
        
        Test.startTest();
        insert role;
		Test.stopTest();        
        
        account = [SELECT Id, IsSoldTo__c FROM Account WHERE Id=:account.Id];
        account2 = [SELECT Id, IsSoldTo__c FROM Account WHERE Id=:account2.Id];        
        System.assertEquals(false, account.IsSoldTo__c);
        System.assertEquals(false, account2.IsSoldTo__c);        
    }  
    static testMethod void testIsSoldToUpdateStatusToActive()
    {        
        Account account = new Account(Name='Account');
        insert account;
        
        CCAccountRole__c role = new CCAccountRole__c(AccountRole__c = 'AG',Status__c = '',MasterAccount__c = account.Id,SlaveAccount__c = account.Id);
        insert role;
        account = [SELECT Id, IsSoldTo__c FROM Account WHERE Id=:account.Id];        
      	System.assertEquals(false, account.IsSoldTo__c);
        
        Test.startTest();
        role.Status__c = 'Active';
        update role;
		Test.stopTest();        
        
        account = [SELECT Id, IsSoldTo__c FROM Account WHERE Id=:account.Id];        
        System.assertEquals(true, account.IsSoldTo__c);
    }      
    static testMethod void testIsSoldToUpdateStatusToInActive()
    {        
        Account account = new Account(Name='Account');
        insert account;
        
        CCAccountRole__c role = new CCAccountRole__c(AccountRole__c = 'AG',Status__c = 'Active',MasterAccount__c = account.Id,SlaveAccount__c = account.Id);
        insert role;
        account = [SELECT Id, IsSoldTo__c FROM Account WHERE Id=:account.Id];        
      	System.assertEquals(true, account.IsSoldTo__c);
        
        Test.startTest();
        role.Status__c = '';
        update role;
		Test.stopTest();        
        
        account = [SELECT Id, IsSoldTo__c FROM Account WHERE Id=:account.Id];        
        System.assertEquals(false, account.IsSoldTo__c);
    }    
    static testMethod void testIsSoldToRemoveRoleAG()
    {
        Account account = new Account(Name='Account');
        insert account;
        CCAccountRole__c role = new CCAccountRole__c(AccountRole__c = 'AG',Status__c = 'Active',MasterAccount__c = account.Id,SlaveAccount__c = account.Id);
        insert role;
        
        account = [SELECT Id, IsSoldTo__c FROM Account WHERE Id=:account.Id];        
      	System.assertEquals(true, account.IsSoldTo__c);
        
        Test.startTest();
        role = [SELECT Id FROM CCAccountRole__c WHERE Id=:role.Id];
        delete role;
        Test.stopTest();

        account = [SELECT Id, IsSoldTo__c FROM Account WHERE Id=:account.Id];        
      	System.assertEquals(false, account.IsSoldTo__c);
    }
    static testMethod void testIsSoldToRemoveRoleWE()
    {
        Account account = new Account(Name='Account');
        insert account;
        CCAccountRole__c role = new CCAccountRole__c(AccountRole__c = 'AG',Status__c = 'Active',MasterAccount__c = account.Id,SlaveAccount__c = account.Id);
        insert role;
        
        CCAccountRole__c role2 = new CCAccountRole__c(AccountRole__c = 'WE',Status__c = 'Active',MasterAccount__c = account.Id,SlaveAccount__c = account.Id);
        insert role2;        
        
        account = [SELECT Id, IsSoldTo__c FROM Account WHERE Id=:account.Id];        
      	System.assertEquals(true, account.IsSoldTo__c);
        
        Test.startTest();
        role2 = [SELECT Id FROM CCAccountRole__c WHERE Id=:role2.Id];
        delete role2;
        Test.stopTest();

        account = [SELECT Id, IsSoldTo__c FROM Account WHERE Id=:account.Id];        
      	System.assertEquals(true, account.IsSoldTo__c);
    }   
    static testMethod void testIsSoldToRemoveRoleAGWithDifferentAccounts()
    {
        Account account = new Account(Name='Account');
        insert account;
        Account account2 = new Account(Name='Account2');
        insert account2;
        
        CCAccountRole__c role = new CCAccountRole__c(AccountRole__c = 'AG',Status__c = 'Active',MasterAccount__c = account.Id,SlaveAccount__c = account.Id);
        insert role;
        
        CCAccountRole__c role2 = new CCAccountRole__c(AccountRole__c = 'AG',Status__c = 'Active',MasterAccount__c = account.Id,SlaveAccount__c = account2.Id);
        insert role2;        
        
        account = [SELECT Id, IsSoldTo__c FROM Account WHERE Id=:account.Id];        
      	System.assertEquals(true, account.IsSoldTo__c);
        account2 = [SELECT Id, IsSoldTo__c FROM Account WHERE Id=:account2.Id];        
      	System.assertEquals(false, account2.IsSoldTo__c);

        
        Test.startTest();
        role2 = [SELECT Id FROM CCAccountRole__c WHERE Id=:role2.Id];
        delete role2;
        Test.stopTest();

        account = [SELECT Id, IsSoldTo__c FROM Account WHERE Id=:account.Id];        
      	System.assertEquals(true, account.IsSoldTo__c);
    } 
    static testMethod void testIsSoldToRemoveRoleAGNotActive()
    {
        Account account = new Account(Name='Account');
        insert account;

        // hier ist die reihenfolge wichtig! es kann eigentlich nur 1 AG geben, weswegen der letzte mit active hier gewinnt. damit aber der inactive anschließend gelöscht werden kann
        // ohne das was kaputt geht, muss der fall getestet werden!
        CCAccountRole__c role2 = new CCAccountRole__c(AccountRole__c = 'AG',Status__c = '',MasterAccount__c = account.Id,SlaveAccount__c = account.Id);
        insert role2;        
        
        CCAccountRole__c role = new CCAccountRole__c(AccountRole__c = 'AG',Status__c = 'Active',MasterAccount__c = account.Id,SlaveAccount__c = account.Id);
        insert role;

        
        account = [SELECT Id, IsSoldTo__c FROM Account WHERE Id=:account.Id];        
      	System.assertEquals(true, account.IsSoldTo__c);
        
        Test.startTest();
        role2 = [SELECT Id FROM CCAccountRole__c WHERE Id=:role2.Id];
        delete role2;
        Test.stopTest();

        account = [SELECT Id, IsSoldTo__c FROM Account WHERE Id=:account.Id];        
      	System.assertEquals(true, account.IsSoldTo__c);
    }     
    
    /***********************************************************
     ***********************************************************
     **** Account.SoldToAccount__c 
     **** SoldToAccount__c = (Master of role='WE'& status='active')
	 ***********************************************************
	 ***********************************************************/
	static testMethod void testSoldToAccountNotActive()
    {
        Account shipTo = new Account(Name='ShipToAccount');
        insert shipTo;
        Account soldTo = new Account(Name='SoldToAccount');
        insert soldTO;
        
        CCAccountRole__c role = new CCAccountRole__c(AccountRole__c = 'WE',
                                                     Status__c = '',
                                                     MasterAccount__c = soldTo.Id, 
                                                     SlaveAccount__c = shipTo.Id);
        
        Test.startTest();
        insert role;
		Test.stopTest();        
        
        shipTo = [SELECT Id, SoldToAccount__c FROM Account WHERE Id=:shipTo.Id];
        System.assertEquals(null, shipTo.SoldToAccount__c);
    }
    
	static testMethod void testSoldToAccountActive()
    {
        Account shipTo = new Account(Name='ShipToAccount');
        insert shipTo;
        Account soldTo = new Account(Name='SoldToAccount');
        insert soldTO;
        
        CCAccountRole__c role = new CCAccountRole__c(AccountRole__c = 'WE',
                                                     Status__c = 'Active',
                                                     MasterAccount__c = soldTo.Id, 
                                                     SlaveAccount__c = shipTo.Id);
        
        Test.startTest();
        insert role;
		Test.stopTest();        
        
        shipTo = [SELECT Id, SoldToAccount__c FROM Account WHERE Id=:shipTo.Id];
        System.assertEquals(soldTo.Id, shipTo.SoldToAccount__c);
        soldTo = [SELECT Id, SoldToAccount__c FROM Account WHERE Id=:soldTo.Id];
        System.assertEquals(null, soldTo.SoldToAccount__c);

    }    
    
	static testMethod void testSoldToAccountSelfReference()
    {
        Account soldTo = new Account(Name='SoldToAccount');
        insert soldTO;
        
        CCAccountRole__c role = new CCAccountRole__c(AccountRole__c = 'WE',
                                                     Status__c = 'Active',
                                                     MasterAccount__c = soldTo.Id, 
                                                     SlaveAccount__c = soldTo.Id);
        
        Test.startTest();
        insert role;
		Test.stopTest();
        
        soldTo = [SELECT Id, SoldToAccount__c FROM Account WHERE Id=:soldTo.Id];
        System.assertEquals(null, soldTo.SoldToAccount__c);
    }
    
	static testMethod void testSoldToAccountRemoveActive()
    {
        Account shipTo = new Account(Name='ShipToAccount');
        insert shipTo;
        Account soldTo = new Account(Name='SoldToAccount');
        insert soldTO;
        
        CCAccountRole__c role = new CCAccountRole__c(AccountRole__c = 'WE',
                                                     Status__c = 'Active',
                                                     MasterAccount__c = soldTo.Id, 
                                                     SlaveAccount__c = shipTo.Id);
        insert role;
        shipTo = [SELECT Id, SoldToAccount__c FROM Account WHERE Id=:shipTo.Id];
        System.assertEquals(soldTo.Id, shipTo.SoldToAccount__c);
        soldTo = [SELECT Id, SoldToAccount__c FROM Account WHERE Id=:soldTo.Id];
        System.assertEquals(null, soldTo.SoldToAccount__c);
        
		CCAccountRole__c r1 = [SELECT Id, Status__c FROM CCAccountRole__c WHERE Id=:role.Id];
        
        Test.startTest();
        r1.Status__c = '';
        update r1;
		Test.stopTest();        

        shipTo = [SELECT Id, SoldToAccount__c FROM Account WHERE Id=:shipTo.Id];
        System.assertEquals(null, shipTo.SoldToAccount__c);
        soldTo = [SELECT Id, SoldToAccount__c FROM Account WHERE Id=:soldTo.Id];
        System.assertEquals(null, soldTo.SoldToAccount__c);

    }    
    
	static testMethod void testSoldToAccountRemoveRole()
    {
        Account shipTo = new Account(Name='ShipToAccount');
        insert shipTo;
        Account soldTo = new Account(Name='SoldToAccount');
        insert soldTO;
        
        CCAccountRole__c role = new CCAccountRole__c(AccountRole__c = 'WE',
                                                     Status__c = 'Active',
                                                     MasterAccount__c = soldTo.Id, 
                                                     SlaveAccount__c = shipTo.Id);
        insert role;
        shipTo = [SELECT Id, SoldToAccount__c FROM Account WHERE Id=:shipTo.Id];
        System.assertEquals(soldTo.Id, shipTo.SoldToAccount__c);
        soldTo = [SELECT Id, SoldToAccount__c FROM Account WHERE Id=:soldTo.Id];
        System.assertEquals(null, soldTo.SoldToAccount__c);
        
		CCAccountRole__c r1 = [SELECT Id, Status__c FROM CCAccountRole__c WHERE Id=:role.Id];
        
        Test.startTest();
		delete r1;
		Test.stopTest();        

        shipTo = [SELECT Id, SoldToAccount__c FROM Account WHERE Id=:shipTo.Id];
        System.assertEquals(null, shipTo.SoldToAccount__c);
        soldTo = [SELECT Id, SoldToAccount__c FROM Account WHERE Id=:soldTo.Id];
        System.assertEquals(null, soldTo.SoldToAccount__c);

    }        
}
