public class AccountTriggerDispatcher extends TriggerDispatcherBase {
	private static Boolean isBeforeInsertProcessing = false;
	private static Boolean isAfterInsertProcessing = false; 
	private static Boolean isBeforeUpdateProcessing = false;
	private static Boolean isAfterUpdateProcessing = false;
	
	public override void beforeInsert(TriggerParameters tp) {
		if (!isBeforeInsertProcessing) {
			isBeforeInsertProcessing = true;
			execute(new AccountTriggerHandler.BIHandler(), tp, TriggerParameters.TriggerEvent.beforeInsert);
			isBeforeInsertProcessing = false;
		}
		else 
			execute(null, tp, TriggerParameters.TriggerEvent.beforeInsert);
	}
	
	public override void afterInsert(TriggerParameters tp) {
		if (!isAfterInsertProcessing) {
			isAfterInsertProcessing = true;
			execute(new AccountTriggerHandler.AIHandler(), tp, TriggerParameters.TriggerEvent.afterInsert);
			isAfterInsertProcessing = false;
		}
		else
			execute(null, tp, TriggerParameters.TriggerEvent.afterInsert);
	}
	
	public override void beforeUpdate(TriggerParameters tp) {
		if (!isBeforeUpdateProcessing) {
			isBeforeUpdateProcessing = true;
			execute(new AccountTriggerHandler.BUHandler(), tp, TriggerParameters.TriggerEvent.afterInsert);
			isBeforeUpdateProcessing = false;
		}
		else
			execute(null, tp, TriggerParameters.TriggerEvent.beforeUpdate);
	}
	
	public override void afterUpdate(TriggerParameters tp) {
		if (!isAfterUpdateProcessing) {
			isAfterUpdateProcessing = true;
			execute(new AccountTriggerHandler.AUHandler(), tp, TriggerParameters.TriggerEvent.afterInsert);
			isAfterUpdateProcessing = false;
		}
		else
			execute(null, tp, TriggerParameters.TriggerEvent.beforeUpdate);
	}
}
