/*
 * @(#)SCPartsServicesTabCompControllerTest.cls
 *  
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Alex Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCPartsServicesTabCompControllerTest 
{
    /*static testMethod void testRepairCodes() 
    {
        SCHelperTestClass3.createCustomSettings('DE', true);
        SCHelperTestClass.createDomsForOrderCreation();
        // insert a test order (the insert trigger fires)
        SCHelperTestClass.createOrderTestSet3(true);
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);
        SCHelperTestClass.createTestCalendar(true);
        SCHelperTestClass.createTestBusinessUnit(SCHelperTestClass.stocks[0].Id, SCHelperTestClass.calendar.Id, true);
        SCHelperTestClass.createTestAssignments(SCHelperTestClass.resources, SCHelperTestClass.stocks, 
                                              SCHelperTestClass.businessUnit.Id, true);
        SCHelperTestClass.createTestStockItem(SCHelperTestClass.stocks[3].Id, SCHelperTestClass.articles[0].Id, false);
        SCHelperTestClass.stockItem.Qty__c = 2.0;
        insert SCHelperTestClass.stockItem;

        Test.startTest();
        
        SCPartsServicesTabComponentController controller = new SCPartsServicesTabComponentController();
        Id appointmentId = SCHelperTestClass.appointments[0].Id;          
        ApexPages.currentPage().getParameters().put('aid', appointmentId);
        controller.pageController = new SCProcessOrderController();
        System.AssertNotEquals(null, controller.getCurAssignment());

        controller.addRepairCodeTextConf = '1111111111';
        controller.getArticleClassService();
        controller.getStockInfos();
        controller.getHasStock();
        controller.getTitleRepairCodes();
        controller.getMasterForRepairGroup();
        
        List<SelectOption> itemList = controller.getItemList();
        System.AssertNotEquals(0, itemList.size());
        
        System.AssertEquals(true, controller.getIsOrderItemSelected());
        
        List<SCOrderRepairCode__c> repCodes = controller.repairCodes;
        System.AssertEquals(0, repCodes.size());
        
        // try to add articles, that does not exist
        controller.add();
        controller.errorArticleName = 'X1';
        controller.returnArticleName = 'Y1';
        controller.addNewRepairCode();
        System.AssertNotEquals(null, controller.errArticleSearchErr);
        System.AssertNotEquals(null, controller.retArticleSearchErr);
        
        // add articles (for order item), that exists
        controller.add();
        controller.errorArticleName = SCHelperTestClass.articles[0].Text_en__c;
        controller.returnArticleName = SCHelperTestClass.articles[1].Text_en__c;
        controller.newRepairCode.ErrorLocation1__c         = '01';
        controller.newRepairCode.ErrorLocation2__c         = '001';
        controller.newRepairCode.ErrorType1__c             = '001';
        controller.newRepairCode.ErrorType1New__c          = 'Text';
        controller.newRepairCode.ErrorType2__c             = '999';
        controller.newRepairCode.ErrorActivityCode1__c     = '01';
        controller.newRepairCode.ErrorActivityCode1New__c  = 'Text';
        controller.newRepairCode.ErrorActivityCode2__c     = '99';
        controller.newRepairCode.ErrorDescription__c       = 'Desc';
        controller.newRepairCode.ErrorCausing__c           = true;
        controller.addNewRepairCode();
        System.AssertEquals(null, controller.errArticleSearchErr);
        System.AssertEquals(null, controller.retArticleSearchErr);
        repCodes = controller.repairCodes;
        System.AssertEquals(1, repCodes.size());
        
        ApexPages.currentPage().getParameters().put('pos', '0');
        controller.editRepairCode();
        controller.newRepairCode.ErrorArticleTrackingNo__c = '901010053486<<<<458471270041T3';
        controller.trackingCodeUpdated();
        controller.addNewRepairCode();
        
        // add articles (for order), that exists
        controller.curSelItem = '0';
        controller.add();
        controller.returnArticleName = SCHelperTestClass.articles[2].Text_en__c;
        controller.addNewRepairCode();
        System.AssertEquals(null, controller.errArticleSearchErr);
        System.AssertEquals(null, controller.retArticleSearchErr);
        repCodes = controller.repairCodes;
        System.AssertEquals(0, repCodes.size());
        
        controller.curSelItem = '1';
        ApexPages.currentPage().getParameters().put('pos', '0');
        controller.delRepairCode();
        repCodes = controller.repairCodes;
        System.AssertEquals(0, repCodes.size());
        controller.curSelItem = '0';
        
        controller.saveOrder();
        controller.refreshData();
        
        Test.stopTest();
    }*/ // testRepairCodes

    static testMethod void testOrderLines() 
    {
        SCHelperTestClass3.createCustomSettings('DE', true);
        SCHelperTestClass.createDomsForOrderCreation();
        // insert a test order (the insert trigger fires)
        SCHelperTestClass.createOrderTestSet3(true);

        Test.startTest();
        
        SCPartsServicesTabComponentController controller = new SCPartsServicesTabComponentController();
        Id appointmentId = SCHelperTestClass.appointments[0].Id;          
        ApexPages.currentPage().getParameters().put('aid', appointmentId);
        controller.pageController = new SCProcessOrderController();
        System.AssertNotEquals(null, controller.getCurAssignment());

        System.AssertEquals(false, controller.getIsBookingEnabled());
        controller.getPriceLists();
        controller.addFromStock();
        controller.addServiceCharges();
        controller.addOrderLine();
        controller.getBoOrderLines();
        System.AssertNotEquals(0, controller.articleIds.length());
        controller.applyInvoicingType();
        controller.calculatePricing();
        controller.reCalculatePricing();
        ApexPages.currentPage().getParameters().put('currentLineRow', '0');
        controller.removeOrderLine();
        
        Test.stopTest();
    } // testOrderLines

    static testMethod void testTimeReports() 
    {
        SCHelperTestClass3.createCustomSettings('DE', true);
        SCHelperTestClass.createDomsForOrderCreation();
        // insert a test order (the insert trigger fires)
        SCHelperTestClass.createOrderTestSet3(true);
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);
        SCHelperTestClass.createTestCalendar(true);
        SCHelperTestClass.createTestBusinessUnit(SCHelperTestClass.stocks[5].Id, 
                                               SCHelperTestClass.calendar.Id, true);
        SCHelperTestClass.createTestAssignments(SCHelperTestClass.resources,
                                              SCHelperTestClass.stocks, 
                                              SCHelperTestClass.businessUnit.Id,
                                              true);
        SCHelperTestClass2.createTimeReports(SCHelperTestClass.resources, true);

        Test.startTest();
        
        SCPartsServicesTabComponentController controller = new SCPartsServicesTabComponentController();
        Id appointmentId = SCHelperTestClass.appointments[0].Id;          
        ApexPages.currentPage().getParameters().put('aid', appointmentId);
        controller.pageController = new SCProcessOrderController();
        System.AssertNotEquals(null, controller.getCurAssignment());

        controller.getDayList();
        controller.getErrMsg();
        controller.loadAllTimeReportData();
        controller.getIsTimeReportEditable();
        System.AssertNotEquals(0, controller.roundGrid);
        
        String selDay = controller.selectedDay;
        System.AssertNotEquals(null, selDay);
        controller.selectedDay = selDay;

        ((SCProcessOrderController)controller.pageController).assignmentChanged = true;
        System.AssertEquals(null, controller.trDistance);
        controller.trDistance = '100000';
        controller.trDistance = '100';
        System.AssertNotEquals(null, controller.trDistance);

        ((SCProcessOrderController)controller.pageController).assignmentChanged = true;
        System.AssertEquals(null, controller.trMileageNew);
        controller.trMileageNew = '100000';
        controller.trMileageNew = '100';
        System.AssertNotEquals(null, controller.trMileageNew);

        ((SCProcessOrderController)controller.pageController).assignmentChanged = true;
        System.AssertEquals(100000, controller.trMileageOld);
        
        System.AssertEquals('09:00', controller.trTravelTimeStart);
        controller.trTravelTimeStart = '10:00';
        System.AssertEquals('10:00', controller.trTravelTimeStart);

        System.AssertNotEquals(null, controller.trTravelTimeDur);
        
        System.AssertEquals(null, controller.trTravelTimeDesc);
        controller.trTravelTimeDesc = 'Travel time';
        System.AssertNotEquals(null, controller.trTravelTimeDesc);
        
        System.AssertEquals(null, controller.trLabourTimeStart);
        controller.trLabourTimeStart = '11:00';
        System.AssertEquals('11:00', controller.trLabourTimeStart);

        System.AssertEquals('01:00', controller.trTravelTimeDur);

        System.AssertNotEquals(null, controller.trLabourTimeDur);
        
        System.AssertEquals(null, controller.trLabourTimeEnd);
        controller.trLabourTimeEnd = '12:00';
        System.AssertNotEquals(null, controller.trLabourTimeEnd);

        System.AssertEquals('01:00', controller.trLabourTimeDur);
        
        System.AssertEquals(null, controller.trLabourTimeDesc);
        controller.trLabourTimeDesc = 'Travel time';
        System.AssertNotEquals(null, controller.trLabourTimeDesc);

        Test.stopTest();
    } // testTimeReports
} // SCPartsServicesTabCompController
