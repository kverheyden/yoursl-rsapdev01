/**
* @author		Austen Buennemann
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				a.buennemann@yoursl.de
*
* @description	Handler for AccountMarketingAttribute Trigger
*
* @date			25.09.2014
*
* Timeline:
* Name               DateTime                  Description
* Austen Buennemann  25.09.2014 14:05          Class created
*/

public with sharing class CCAccountMarketingAttributeHandler {
	
	public void deleteMarketingAttribute(AccountMarketingAttribute__c [] attributes){
		Set<Id> attribs = new Set<Id>();
		for(AccountMarketingAttribute__c att : attributes){
			if(att != null && att.ToDelete__c == true){
				attribs.add(att.Id);
			}
		}
		if(!attribs.isEmpty()){
			CCAccountMarketingAttributeHandler.executeDelete(attribs);
		}
		
	}
	
	@future
	public static void executeDelete(Set<ID> attIds){
		List<AccountMarketingAttribute__c> attribs = new List<AccountMarketingAttribute__c>();
		Map<Id, AccountMarketingAttribute__c> attMap = new Map<Id, AccountMarketingAttribute__c>(
													[Select Id From AccountMarketingAttribute__c Where Id IN: attIds]);
		for(Id i :  attIds){
			if(attMap.containsKey(i)){
				attribs.add(attMap.get(i));
			}
		}
		if(!attribs.isEmpty()){
			delete attribs;
		}
	}
}
