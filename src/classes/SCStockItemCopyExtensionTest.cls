/*
 * @(#)SCStockItemCopyExtensionTest.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 * <p>
 * This class contains unit tests for copy stock items.
 * <p>
 * A complete testing environment is created first. Remember that all data
 * created in a Unit test is not commited to the database!!
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */

@isTest
private class SCStockItemCopyExtensionTest
{
    /**
     * testCopyItems with deleteing items before
     */
    static testMethod void testCopyItems1()
    {
        // first we create the test data, this will be
        // - stocks belonging to one plant
        // - articles
        // - business unit with stock1
        // - employees with their resource
        // - resource assigments for each employee
        // - stock items
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);
        SCHelperTestClass.createTestArticles(true);
        SCHelperTestClass.createTestUsers(true);
        SCHelperTestClass.createTestResources(SCHelperTestClass.users, true);
        SCHelperTestClass.createTestCalendar(true);
        SCHelperTestClass.createTestBusinessUnit(SCHelperTestClass.stocks[0].Id, 
                                               SCHelperTestClass.calendar.Id, true);
        SCHelperTestClass.createTestAssignments(SCHelperTestClass.resources, 
                                              SCHelperTestClass.stocks, 
                                              SCHelperTestClass.businessUnit.Id, true);
        SCHelperTestClass.createTestStockItems(SCHelperTestClass.articles,
                                             SCHelperTestClass.stocks[1].Id, 
                                             SCHelperTestClass.stocks[2].Id, false);
        SCHelperTestClass.stockItems[0].Qty__c = 5.0;
        SCHelperTestClass.stockItems[0].MinQty__c = 1.0;
        SCHelperTestClass.stockItems[0].MaxQty__c = 5.0;
        SCHelperTestClass.stockItems[0].ReplenishmentQty__c = 2.0;
        SCHelperTestClass.stockItems[1].Qty__c = 5.0;
        SCHelperTestClass.stockItems[1].MinQty__c = 2.0;
        SCHelperTestClass.stockItems[1].MaxQty__c = 5.0;
        SCHelperTestClass.stockItems[1].ReplenishmentQty__c = 3.0;
        SCHelperTestClass.stockItems[2].Qty__c = 5.0;
        SCHelperTestClass.stockItems[2].MinQty__c = 3.0;
        SCHelperTestClass.stockItems[2].MaxQty__c = 8.0;
        SCHelperTestClass.stockItems[2].ReplenishmentQty__c = 4.0;
        SCHelperTestClass.stockItems[3].Qty__c = 5.0;
        SCHelperTestClass.stockItems[3].MinQty__c = 4.0;
        SCHelperTestClass.stockItems[3].MaxQty__c = 8.0;
        SCHelperTestClass.stockItems[3].ReplenishmentQty__c = 5.0;
        insert SCHelperTestClass.stockItems;
        
        
        Test.startTest();
        
        SCApplicationSettings__c appSettings = [SELECT DEFAULT_PLANT__c FROM SCApplicationSettings__c];
        appSettings.DEFAULT_PLANT__c = SCHelperTestClass.plant.name;
        update appSettings;
        
        ApexPages.StandardController con = new ApexPages.StandardController(SCHelperTestClass.stocks[1]);
        SCStockItemCopyExtension ext = new SCStockItemCopyExtension(con);
        System.assertEquals(false, ext.copyOk);
        
        List<SelectOption> sourceList = ext.getSourceList();
        System.assertNotEquals(0, sourceList.size());
        ext.sourceStock = SCHelperTestClass.stocks[2].Id;

        ext.deleteItems = true;
        ext.copyMinQty = true;
        ext.copyMaxQty = true;
        ext.copyReplQty = true;
        
        ext.onCopyItems();
        System.assertEquals(true, ext.copyOk);
        
        Test.stopTest();

        List<SCStockItem__c> stockItems = [Select Id, Qty__c 
                                             from SCStockItem__c 
                                            where Stock__c = :SCHelperTestClass.stocks[1].Id];
        System.assertEquals(2, stockItems.size());
        
        SCStockItem__c stockItem1 = [Select Id, Qty__c, MinQty__c, MaxQty__c, ReplenishmentQty__c 
                                            from SCStockItem__c 
                                           where Stock__c = :SCHelperTestClass.stocks[1].Id 
                                             and Article__c = :SCHelperTestClass.articles[2].Id];
        System.assertEquals(0, stockItem1.Qty__c);
        System.assertEquals(3, stockItem1.MinQty__c);
        System.assertEquals(8, stockItem1.MaxQty__c);
        System.assertEquals(4, stockItem1.ReplenishmentQty__c);
        
        SCStockItem__c stockItem2 = [Select Id, Qty__c, MinQty__c, MaxQty__c, ReplenishmentQty__c 
                                            from SCStockItem__c 
                                           where Stock__c = :SCHelperTestClass.stocks[1].Id 
                                             and Article__c = :SCHelperTestClass.articles[3].Id];
        System.assertEquals(0, stockItem2.Qty__c);
        System.assertEquals(4, stockItem2.MinQty__c);
        System.assertEquals(8, stockItem2.MaxQty__c);
        System.assertEquals(5, stockItem2.ReplenishmentQty__c);
    } // testCopyItems1

    /**
     * testCopyItems without deleteing items before
     */
    static testMethod void testCopyItems2()
    {
        // first we create the test data, this will be
        // - stocks belonging to one plant
        // - articles
        // - business unit with stock1
        // - employees with their resource
        // - resource assigments for each employee
        // - stock items
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);
        SCHelperTestClass.createTestArticles(true);
        SCHelperTestClass.createTestUsers(true);
        SCHelperTestClass.createTestResources(SCHelperTestClass.users, true);
        SCHelperTestClass.createTestCalendar(true);
        SCHelperTestClass.createTestBusinessUnit(SCHelperTestClass.stocks[0].Id, 
                                               SCHelperTestClass.calendar.Id, true);
        SCHelperTestClass.createTestAssignments(SCHelperTestClass.resources, 
                                              SCHelperTestClass.stocks, 
                                              SCHelperTestClass.businessUnit.Id, true);
        SCHelperTestClass.createTestStockItems(SCHelperTestClass.articles,
                                             SCHelperTestClass.stocks[1].Id, 
                                             SCHelperTestClass.stocks[2].Id, false);
        SCHelperTestClass.stockItems[0].Qty__c = 5.0;
        SCHelperTestClass.stockItems[0].MinQty__c = 1.0;
        SCHelperTestClass.stockItems[0].MaxQty__c = 5.0;
        SCHelperTestClass.stockItems[0].ReplenishmentQty__c = 2.0;
        SCHelperTestClass.stockItems[0].Article__c = SCHelperTestClass.articles[2].Id;
        SCHelperTestClass.stockItems[1].Qty__c = 5.0;
        SCHelperTestClass.stockItems[1].MinQty__c = 2.0;
        SCHelperTestClass.stockItems[1].MaxQty__c = 5.0;
        SCHelperTestClass.stockItems[1].ReplenishmentQty__c = 3.0;
        SCHelperTestClass.stockItems[2].Qty__c = 5.0;
        SCHelperTestClass.stockItems[2].MinQty__c = 3.0;
        SCHelperTestClass.stockItems[2].MaxQty__c = 8.0;
        SCHelperTestClass.stockItems[2].ReplenishmentQty__c = 4.0;
        SCHelperTestClass.stockItems[3].Qty__c = 5.0;
        SCHelperTestClass.stockItems[3].MinQty__c = 4.0;
        SCHelperTestClass.stockItems[3].MaxQty__c = 8.0;
        SCHelperTestClass.stockItems[3].ReplenishmentQty__c = 5.0;
        insert SCHelperTestClass.stockItems;
        
        
        Test.startTest();
        
        SCApplicationSettings__c appSettings = [SELECT DEFAULT_PLANT__c FROM SCApplicationSettings__c];
        appSettings.DEFAULT_PLANT__c = SCHelperTestClass.plant.name;
        update appSettings;
        
        ApexPages.StandardController con = new ApexPages.StandardController(SCHelperTestClass.stocks[1]);
        SCStockItemCopyExtension ext = new SCStockItemCopyExtension(con);
        System.assertEquals(false, ext.copyOk);
        
        List<SelectOption> sourceList = ext.getSourceList();
        System.assertNotEquals(0, sourceList.size());
        ext.sourceStock = SCHelperTestClass.stocks[2].Id;

        ext.deleteItems = false;
        ext.copyMinQty = true;
        ext.copyMaxQty = true;
        ext.copyReplQty = false;
        
        ext.onCopyItems();
        System.assertEquals(true, ext.copyOk);
        
        Test.stopTest();

        List<SCStockItem__c> stockItems = [Select Id, Qty__c 
                                             from SCStockItem__c 
                                            where Stock__c = :SCHelperTestClass.stocks[1].Id];
        System.assertEquals(3, stockItems.size());
        
        SCStockItem__c stockItem1 = [Select Id, Qty__c, MinQty__c, MaxQty__c, ReplenishmentQty__c 
                                            from SCStockItem__c 
                                           where Stock__c = :SCHelperTestClass.stocks[1].Id 
                                             and Article__c = :SCHelperTestClass.articles[1].Id];
        System.assertEquals(5, stockItem1.Qty__c);
        System.assertEquals(2, stockItem1.MinQty__c);
        System.assertEquals(5, stockItem1.MaxQty__c);
        System.assertEquals(3, stockItem1.ReplenishmentQty__c);
        
        SCStockItem__c stockItem2 = [Select Id, Qty__c, MinQty__c, MaxQty__c, ReplenishmentQty__c 
                                            from SCStockItem__c 
                                           where Stock__c = :SCHelperTestClass.stocks[1].Id 
                                             and Article__c = :SCHelperTestClass.articles[2].Id];
        System.assertEquals(5, stockItem2.Qty__c);
        System.assertEquals(3, stockItem2.MinQty__c);
        System.assertEquals(8, stockItem2.MaxQty__c);
        System.assertEquals(2, stockItem2.ReplenishmentQty__c);
        
        SCStockItem__c stockItem3 = [Select Id, Qty__c, MinQty__c, MaxQty__c, ReplenishmentQty__c 
                                            from SCStockItem__c 
                                           where Stock__c = :SCHelperTestClass.stocks[1].Id 
                                             and Article__c = :SCHelperTestClass.articles[3].Id];
        System.assertEquals(0, stockItem3.Qty__c);
        System.assertEquals(4, stockItem3.MinQty__c);
        System.assertEquals(8, stockItem3.MaxQty__c);
        System.assertEquals(null, stockItem3.ReplenishmentQty__c);
    } // testCopyItems2
} // SCStockItemCopyExtensionTest
