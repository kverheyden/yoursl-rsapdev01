/*
 * @(#)SCAddressValidationNLControllerTest.cls 
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Tests the address validation function by emulating the calls to 
 * the web services. 
 */
@isTest
private class SCAddressValidationNLControllerTest{

    static testMethod void testController() {
        
        //TODO: implement
        SCAddressValidationNLController a = new SCAddressValidationNLController();
        a.showMap = true;
        a.showEdit = true;
        a.approxSearch = true;
        a.addrSelectedItem = 0;
        a.getAddr();
        
        a.setAddr(null);
        
        AvsAddress addr = new AvsAddress();
        addr.country = 'nl';
        addr.city = 'Amsterdam';
        addr.houseNumber = '10';
        addr.street = 'Grot';
        addr.matchInfo = 'test';
        a.setAddr(addr);
        a.OnValidateAddress();
        a.getAddrSel();
        
        a.getAddr();
        a.getResultsAvailable();
        a.getIsSelectedAddrGeocoded();
        a.OnApproximate();
        a.OnGeocode();
        
        a.OnSelectItem();
        a.OnButtonShowMap();
        a.OnClickShowMap();
        a.getMapUrl();
    }
    
    //@author GMSS
    static testMethod void CodeCoverageA()
    {
        SCAddressValidationNLController a = new SCAddressValidationNLController();
        
        AvsResult res = new AvsResult();
        res.items = new List<AvsAddress>{null};
        a.addrResult = res;
        a.addrSelectedItem = 0;
        try { a.OnDoit(); } catch(Exception e) {}
        
        AvsAddress addr = new AvsAddress();
        addr.geox = 0;
        addr.geoy = 0;
        res = new AvsResult();
        res.items = new List<AvsAddress>{addr};
        
        a.addrResult = res;
        a.addrSelectedItem = 0;
        try { a.OnSelectItem(); } catch(Exception e) {}
        
        a.approxSearch = false;
        try { a.OnDoit(); } catch(Exception e) {}
        
        a.approxSearch = true;
        try { a.OnDoit(); } catch(Exception e) {}
        
    }
}
