/**********************************************************************
Name:  AlertBatch
======================================================
Purpose:                                                            
Runs each night and sends out an Alert to the Owner of an Picture of succsess in case it expiers in two weeks                                                     
======================================================
History                                                            
-------                                                            
Date  	AUTHOR	DETAIL 
11/26/2013 Bernd Werner	<b.werner@yoursl.de>            
***********************************************************************/

global class AlertBatch implements Database.Batchable<sObject>{

	global Database.QueryLocator start(Database.BatchableContext BC)
	{ 
    	String limitstring ='';
    	Integer qsize = 1000;
    	if (qsize != 0){
    		limitstring = ' LIMIT ' + qsize;
    	}
    	// Calculate the dateof expiration
        Integer iExpirationThresholdDays = 14;
        SalesAppSettings__c oExpirationThresholdDays = SalesAppSettings__c.getInstance( 'PofSExpirationThresholdDays' );
        if( oExpirationThresholdDays != null ){
            try{
                iExpirationThresholdDays = Integer.valueOf( oExpirationThresholdDays.Value__c );
            }catch( Exception oException ){
                iExpirationThresholdDays = 14;
            }
        }
    	date firedate = System.today() + iExpirationThresholdDays; 
		String query = 'SELECT Id, OwnerId, OwnerMail__c, Name, Bezeichnung__c FROM PictureOfSuccess__c WHERE ValidUntil__c='+ string.ValueOf(firedate);
    	query = query + limitstring;
    	system.debug('##### Query: '+query);
    	return Database.getQueryLocator(query);  
	}

	global void execute(Database.BatchableContext BC, List<PictureOfSuccess__c> scope)
	{
		System.debug('####Scope: '+scope);
		Map <String,List<PictureOfSuccess__c>> alertMap = new Map<String,List<PictureOfSuccess__c>>();
    	for (PictureOfSuccess__c pofs : scope)
    	{
    		if(!alertMap.containsKey(pofs.OwnerMail__c))
    		{
    			List <PictureOfSuccess__c> tmpList = new List <PictureOfSuccess__c>();
    			tmpList.add(pofs);
    			alertMap.put(pofs.OwnerMail__c,tmpList);
    		}
    		else
    		{
    			alertMap.get(pofs.OwnerMail__c).add(pofs);
    		}
    	}
    	System.debug('###Alert Map: '+alertMap);
    	
    	// Reserve Mail capacity
    	Messaging.reserveSingleEmailCapacity(alertMap.size());
    	// Collect Data and send Mail
    	for (String mailadd:alertMap.keySet()){
    		String BodyText='Attention, \n\rthe follwoing Picture of sccusess will expire in two weeks:\n\r';
    		for (PictureOfSuccess__c tmpPic:alertMap.get(mailadd))
    		{
    			BodyText = BodyText + '- ' + tmpPic.Name+'\n\r';
    		}
    		// Compose Mail:
    		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    		// Define Address:
    		String[] toAddresses = new String[] {mailadd};
    		// Assign the addresses for the To to the mail object.
			mail.setToAddresses(toAddresses);
			// Specify the address used when the recipients reply to the email. 
			mail.setReplyTo('support@test.com');
			// Specify the name used as the display name.
			mail.setSenderDisplayName('Picture of Success monitoring');
			// Specify the subject line for your email address.
			mail.setSubject('Picture of success expiration warning');
			// Specify the text content of the email.
			mail.setPlainTextBody(BodyText);
			// Send the email you have created.
			System.debug('###Mail: '+mail);
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    	}
    	
    	
	}

	global void finish(Database.BatchableContext BC)
	{
		System.debug('Batch Process Complete');
	}
}


// fsfaAlertBatch batch = new fsfaAlertBatch();
// Id batchId = Database.executeBatch(batch);
