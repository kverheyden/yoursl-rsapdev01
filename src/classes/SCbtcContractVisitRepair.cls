/*
 * @(#)SCbtcContractVisitRepair.cls
 * 
 * Copyright 2011 by GMSDevelopment GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * The job is to be called only one time.
 *
 * The batch job is used for repairing of the contract visits that have yet status = scheduled
 * but their orders are completed or cancelled or closed finally.
 * 
 * if the order status is 'completed' or 'close final' then the contract visit status is set to 'completed'.
 * if the order status is 'cancelled' then the contract visit status is set to 'cancelled'.
 *
 * An InterfaceLog record is created with the result info as:
 * The contract visit status set from : cv.Status__c  to cancelled on behalf of the order status :  cv.Order__r.Status__c 
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
global class SCbtcContractVisitRepair extends SCbtcBase
       implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts
{
    private ID batchprocessid = null;
    private String mode = 'productive';
    String country = 'DE';

    Integer max_cycles = 0;    
    List<String> visitIdList = null;    // visitIdList = null, normal batch call
                                        // visitIdList != null, orders are repaird only for the list
                                        // of visits    

    // Object for application settings
    private static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();

    private Id contractId = null;

    /**
     * Entry point for starting the apex batch repairing all contracts 
     * @param mode could be 'trace', 'test' or 'productive'
     */
    public static ID repairAll(String country, Integer max_cycles, String mode) 
    {
        String department =  null;
        SCbtcContractVisitRepair btc = new SCbtcContractVisitRepair(country, max_cycles, mode);
        btc.batchprocessid = btc.executeBatch();
        return btc.batchprocessid;
    } // repairAll


    /**
     * Entry point for starting the apex batch dispatching an order having the 
     * SCContractVisit__c.Status__c = 'repair' and SCContractVisit__c.Order__c = :orderId
     * @param contractId
     * @param mode could be 'test' or 'productive'
     */
    public static ID repairContract(Id contractId, String country, Integer max_cycles, String mode)
    {
        SCbtcContractVisitRepair btc = new SCbtcContractVisitRepair(contractId, country, max_cycles, mode);
        btc.batchprocessid = btc.executeBatch();
        return btc.batchprocessid;
    } // repairContract

    /**
     * Entry point for starting the apex batch dispatching an order having the 
     * SCContractVisit__c.Status__c = 'repair' and SCContractVisit__c.Order__c.Name = :orderName
     * @param orderName
     * @param mode could be 'test' or 'productive'
     */
    public static ID repairContract(String contractName, String country, Integer max_cycles, String mode)
    {
        Id contractId = null;
        System.debug('###mode: ' + mode);
        System.debug('###contractName: ' + contractName);
        if(contractName != null)
        {
            List<SCContract__c> contractList = [Select Id from SCContract__c where Name = :contractName];
            if(contractList.size() > 0)
            {
                contractId = contractList[0].Id;
                System.debug('###contractId: ' + contractId);
                SCbtcContractVisitRepair btc = new SCbtcContractVisitRepair(contractId, country, max_cycles, mode);
                btc.batchprocessid = btc.executeBatch();
                return btc.batchprocessid;
            }
        }
        return null;
    } // repairContractVisit


    Webservice static void syncRepairContractVisits(String contractID)
    {
        System.debug('syncRepairContractVisits');
        String country = 'DE';
        if(contractId != null)
        {
            SCContract__c c = [Select Country__c from SCContract__c where Id = :contractId];
            country = c.Country__c;
        }
        System.debug('###Country: ' + country);
        List<RecordType> recordTypeIdList = [Select Id from RecordType where DeveloperName in ('Contract', 'Insurance') and SobjectType = :(SCfwConstants.NamespacePrefix + 'SCContract__c')];
        System.debug('recordTypeIdList: ' + recordTypeIdList);

        List<String> statusList = new List<String>();
        statusList.add('5506'); // Completed
        statusList.add('5507'); // Cancelled
        statusList.add('5508'); // Closed final

        List<SCContractVisit__c> visitList = [select ID, Contract__r.ID, Status__c, Order__c, Order__r.Status__c
                                            from SCContractVisit__c 
                                            where Contract__r.Country__c = :country
                                            and Contract__c = :contractID
//                                            and Order__r.Status__c in :statusList
//                                            and Status__c = 'scheduled'
                                            ];
        Integer max_cycles = 0;
        
        SCBtcContractVisitRepair btc = new SCbtcContractVisitRepair(contractId, country, max_cycles, 'trace');
        List<SCInterfaceLog__c> interfaceLogList = new List<SCInterfaceLog__c>();
        for(SCContractVisit__c cv: visitList)
        {         
            btc.repairContractVisit(cv, interfaceLogList);
        }
        update visitList;
        insert interfaceLogList;        
    } //syncCreateContractVisits


    /**
     * Constructor
     * @param mode could be 'test' or 'productive'
     */
    public SCbtcContractVisitRepair(String country, Integer max_cycles, String mode)
    {
        this(null, country, max_cycles, mode);
    }


    /**
     * Constructor
     * @param contractID
     * @param mode could be 'test' or 'productive'
     */
    public SCbtcContractVisitRepair(Id contractId, String country, Integer max_cycles, String mode)
    {
        this.contractId = contractId;
        this.country = country;
        this.max_cycles = max_cycles;
        this.mode = mode;
    }

   /*
    * Called by the framework when the batch job is started. We construct the soql statement
    * for reading the resource assignments.
    * @param BC the batch context
    * @return the query locator with the selected material movements (up to 50 mio records possible)
    *
    *  Query for check the changes in the database
    *  select id, Status__c from SCContractVisit__c where Order__r.Status__c in ('5506', '5507', '5508') 
    *  and Status__c = 'scheduled'
    *
    */
    global override Database.QueryLocator start(Database.BatchableContext BC)
    {
        debug('start');
        // one order will be created from one SContractVisit__c record 
        /* To check the syntax of SOQL statement */
        List<String> statusList = new List<String>();
        statusList.add('5506'); // Completed
        statusList.add('5507'); // Cancelled
        statusList.add('5508'); // Closed final

        List<SCContractVisit__c> visitList = [select ID, Contract__r.ID, Status__c, Order__c, Order__r.Status__c
                                            from SCContractVisit__c 
                                            where Contract__r.Country__c = 'DE'
                                            and Order__r.Status__c in :statusList
                                            and Status__c = 'scheduled'
                                            Limit 1];
        
        debug('start: after checking statement');
        /**/
        String query = null;
        if(country == null || country.trim() == '')
        {
            country = 'XX';
        }
        debug('country: ' + country);
        // 5506, Completed; 5507 Cancelled; 5508 Closed final
        
        if(contractId == null)
        {
            query = ' select ID, Contract__r.ID, Status__c, Order__c, Order__r.Status__c '
                  + ' from SCContractVisit__c where Contract__r.Country__c = \'' + country + '\''
                  + ' and Order__r.Status__c in (\'5506\', \'5507\', \'5508\') ' 
                  + ' and Status__c = \'scheduled\' ';

            if(max_cycles > 0)
            {
                query += ' limit ' + max_cycles;
            }
        }
        else
        {
            query = ' select ID, Contract__r.ID, Status__c, Order__c, Order__r.Status__c '
                  + ' from SCContractVisit__c where Contract__r.Country__c = \'' + country + '\''
                  + ' and Order__r.Status__c in (\'5506\', \'5507\', \'5508\') '
                  + ' and Status__c = \'scheduled\' '
                  + ' and Contract__r.ID = \'' + contractId + '\'';
        }
        debug('query: ' + query);
        return Database.getQueryLocator(query);
    } // start
    
   /*
    * Called for each batch of records to process.
    * @param BC the batch context
    * @param scope the list records to be processed
    */
    global override void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        debug('execute');
        List<SCInterfaceLog__c> interfaceLogList = new List<SCInterfaceLog__c>();
        for(sObject res: scope)
        {
            debug('sObject: ' + res);
            SCContractVisit__c cv = (SCContractVisit__c)res;
            repairContractVisit(cv, interfaceLogList);
        }
        upsert scope;
        insert interfaceLogList;
    } // execute

   /*
    * Called by the framework when the batch job has been completed. 
    * We send an e-mail notification about the status
    * @param BC the batch context
    */
    global override void finish(Database.BatchableContext BC)
    {
    } // finish

    /**
     * Repairs the status of the Contract Visit
     */
    public void repairContractVisit(SCContractVisit__c cv, List<SCInterfaceLog__c> interfaceLogList)
    {
        debug('repairContractVisit');
        debug('contractVisit: ' + cv);
        debug('contractVisitID: ' + cv.Id);
        
        DateTime start = Datetime.now();
        Boolean thrownException = false;
        String resultInfo = '';
        Integer count = 0;
        String step = '';
        Boolean error = false;
        Boolean changed = false;
        try
        {
            step = 'setting the status to completed';
            if( cv.Status__c == 'scheduled'
                && (cv.Order__r.Status__c == '5506'    // completed
                     || cv.Order__r.Status__c == '5508')) // closed final
            {
                resultInfo = 'The contract visit status set from \'' + cv.Status__c + '\' to \'completed\' ' 
                + ' on behalf of the order status \'' + cv.Order__r.Status__c + '\'.'; 
                cv.Status__c = 'completed';
                changed = true;
            }           
            else if( cv.Status__c == 'scheduled'
                && cv.Order__r.Status__c == '5507') // cancelled
            {
                resultInfo = 'The contract visit status set from \'' + cv.Status__c + '\' to \'skip\' ' 
                + ' on behalf of the order status \'' + cv.Order__r.Status__c + '\'.'; 
                cv.Status__c = 'skip';
                changed = true;
            }           

        }
        catch(Exception e)
        {
            thrownException = true;
            resultInfo = 'step: ' + step + ', exception: ' + e.getMessage();
            String prevMode = mode;
            mode = 'test';
            debug('resultInfo:' + resultInfo);
            mode = prevMode;
        }
        finally
        {
            /**
            E000 Success
            E001 Success with Info
            E100 Service not reachable
            E101 Service processing failed
            */
            String resultCode = 'E000';
            if(thrownException || error)
            {
                resultCode = 'E101';
            }
            if(changed)
            {
                logBatchInternal(interfaceLogList, 'CONTRACT_VISIT_REPAIR', 'SCbtcContractVisitRepair',
                                    cv.Contract__r.Id, cv.Order__c, resultCode, 
                                    resultInfo, null, start, count); 
            }                        
         }
    }// repairContract

 public static void logBatchInternal(List<SCInterfaceLog__c> interfaceLogList, String interfaceName, String interfaceHandler,
                                ID referenceID, ID referenceID2, String resultCode, 
                                String resultInfo, String data, Datetime start, Integer count)
    {
        SCInterfaceLog__c ic = new SCInterfaceLog__c();
        interfaceLogList.add(ic);
        ic.Interface__c = interfaceName;
        ic.InterfaceHandler__c = interfaceHandler;
        ic.Type__c = 'BATCH';
        ic.Count__c = count;
        ic.ReferenceID__c = referenceID;
        ic.ReferenceID2__c = referenceID2;
        ic.ResultCode__c = '';
        ic.ResultCode__c = resultCode;
        ic.ResultInfo__c = resultInfo;
        ic.Data__c = data;
        ic.Direction__c = 'internal';
        ic.Start__c = start;
        ic.Stop__c = Datetime.now();
        Long duration = ic.Stop__c.getTime() - ic.Start__c.getTime();
        ic.Duration__c = duration; 
        if(interfaceName == 'CONTRACT_VISIT_REPAIR')
        {
            ic.Contract__c = referenceID;
            ic.Order__c = referenceID2;
        }
    }       

    private void debug(String text)
    {
        if(mode.equalsIgnoreCase('test')
           || mode.equalsIgnoreCase('trace'))
        {
            System.debug('###...................' + text);
        }
    }

}
