/*
 * @(#)SCbtcMaterialMovementTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Test class checking the wrapper for booking material movements (async, sync)
 * @author Norbert Armbruster <narmbruster@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest 
private class SCbtcMaterialMovementTest
{
   /*
    * Check the asynchrouous booking of material movements 
    * Mode=STOCK all material movements for a specific stock
    */
    public static testMethod void testAsyncBatchSTOCK() 
    {
        TestData data = new TestData(10, 2);
        Test.StartTest();
        SCbtcMaterialMovement.asyncBookPending('STOCK', data.stock.id, 'epssupport@gms-online.de');
        Test.StopTest();

        // evaluate the results
        ID aid = data.article.id; 
        ID pid = data.plant.id;
        ID sid = data.stock.id;
        ID s2id = data.stock2.id;
        
        // all material movements for this stock and article have to be booked
        //TODO System.AssertEquals(data.max, database.countquery('SELECT count() from SCMaterialMovement__c where article__c =:aid and stock__c =:sid and status__c=\'' + SCfwConstants.DOMVAL_MATSTAT_BOOKED + '\''));

        // In the plant - only the items of the stock have to be booked 
        //TODO System.AssertEquals(data.max, database.countquery('SELECT count() from SCMaterialMovement__c where article__c =:aid and stock__r.plant__c =:pid and status__c=\'' + SCfwConstants.DOMVAL_MATSTAT_BOOKED + '\''));

        // check the booked quanity
        //TODO List<SCStockItem__c> res = database.query('SELECT qty__c from SCStockItem__c where article__c =:aid and stock__c = :sid');
        //TODO System.AssertEquals(data.max * data.qty, res[0].qty__c);
   } // testAsyncBatchSTOCK

   /*
    * Check the asynchrouous booking of material movements 
    * Mode=STOCK all material movements for a specific stock
    */
    public static testMethod void testAsyncBatchPLANT() 
    {
        TestData data = new TestData(10, 3);
        Test.StartTest();
        SCbtcMaterialMovement.asyncBookPending('PLANT', data.plant.id, null);
        Test.StopTest();

        // evaluate the results
        ID aid = data.article.id; 
        ID pid = data.plant.id;
        ID sid = data.stock.id;
        ID s2id = data.stock2.id;
        
        // In the plant - all items with this article have to be booked 
        //TODO System.AssertEquals(data.max * 2, database.countquery('SELECT count() from SCMaterialMovement__c where article__c =:aid and stock__r.plant__c =:pid and status__c=\'' + SCfwConstants.DOMVAL_MATSTAT_BOOKED + '\''));

        // check the booked quanity
        //TODO List<SCStockItem__c> res = database.query('SELECT qty__c from SCStockItem__c where article__c =:aid and stock__r.plant__c = :pid');
        
        List<SCStockItem__c> res2 = database.query('SELECT stock__c, qty__c from SCStockItem__c where stock__r.plant__c = :pid');
        System.debug('###na###');
        System.debug(res2);
        
        // we expect to stock item records - one for each stock
        //TODO System.AssertEquals(2, res.size());
        //TODO System.AssertEquals(2 * data.max * data.qty, res[0].qty__c + res[1].qty__c);
   } // testAsyncBatchPLANT

   /*
    * Check the asynchrouous booking of material movements 
    * Mode=ALL all material movements for a specific stock
    */
    public static testMethod void testAsyncBatchALL() 
    {
        // Test of MODE ALL is not recomended as we would be booking live data as well
        System.AssertEquals(1, 1);
    } // testAsyncBatchALL

   /*
    * Check the asynchrouous booking of material movements 
    * This method is also used for a mass test with 200 records.
    */
    public static testMethod void testAsyncBatchSTOCK200() 
    {
        TestData data = new TestData(200, 2);
        Test.StartTest();
        SCbtcMaterialMovement.asyncBookPending('STOCK', data.stock.id, 'epssupport@gms-online.de');
        Test.StopTest();

        // evaluate the results
        ID aid = data.article.id; 
        ID pid = data.plant.id;
        ID sid = data.stock.id;
        ID s2id = data.stock2.id;
        
        // all material movements for this stock and article have to be booked
        //TODO System.AssertEquals(data.max, database.countquery('SELECT count() from SCMaterialMovement__c where article__c =:aid and stock__c =:sid and status__c=\'' + SCfwConstants.DOMVAL_MATSTAT_BOOKED + '\''));

        // In the plant - only the items of the stock have to be booked 
        //TODO System.AssertEquals(data.max, database.countquery('SELECT count() from SCMaterialMovement__c where article__c =:aid and stock__r.plant__c =:pid and status__c=\'' + SCfwConstants.DOMVAL_MATSTAT_BOOKED + '\''));

        // check the booked quanity
        //TODO List<SCStockItem__c> res = database.query('SELECT qty__c from SCStockItem__c where article__c =:aid and stock__c = :sid');
        //TODO System.AssertEquals(data.max * data.qty, res[0].qty__c);
   } // testAsyncBatchSTOCK200


   /*
    * Check the asynchrouous booking of material movements 
    * Mode=STOCK all material movements for a specific stock
    */
    public static testMethod void testSyncBatchSTOCK() 
    {
        TestData data = new TestData(10, 2);
        Test.StartTest();
        SCbtcMaterialMovement.BookPending('STOCK', data.stock.id);
        Test.StopTest();

        // evaluate the results
        ID aid = data.article.id; 
        ID pid = data.plant.id;
        ID sid = data.stock.id;
        ID s2id = data.stock2.id;
        
        // all material movements for this stock and article have to be booked
        System.AssertEquals(data.max, database.countquery('SELECT count() from SCMaterialMovement__c where article__c =:aid and stock__c =:sid and status__c=\'' + SCfwConstants.DOMVAL_MATSTAT_BOOKED + '\''));

        // In the plant - only the items of the stock have to be booked 
        System.AssertEquals(data.max, database.countquery('SELECT count() from SCMaterialMovement__c where article__c =:aid and stock__r.plant__c =:pid and status__c=\'' + SCfwConstants.DOMVAL_MATSTAT_BOOKED + '\''));

        // check the booked quanity
        List<SCStockItem__c> res = database.query('SELECT qty__c from SCStockItem__c where article__c =:aid and stock__c = :sid');
        System.AssertEquals(data.max * data.qty, res[0].qty__c);
   } // testSyncBatchSTOCK
    

   /*
    * Test data for booking material
    */
    class TestData
    {
        // maximum number of items to book
        public integer max;
        // the quantity to book
        public integer qty; 
        // our test plant and stock and article
        public SCPlant__c plant;
        public SCStock__c stock;
        public SCStock__c stock2;
        public SCArticle__c article;
        // our material movements to book
        public List<SCMaterialMovement__c> movements;

        
       /*
        * Constructor for the test data
        * @param max  maximum number of items to book (1...200)
        * @param qty the quantity to book (1..10)        
        */
        public TestData(integer max, integer qty)
        {
            this.max = max;
            this.qty = qty; 
            
            // now create the test data
            plant = new SCPlant__c(name = 'my1234');
            insert plant;
            
            stock = new SCStock__c(name = 'my2345', plant__c = plant.id);
            insert stock;
    
            stock2 = new SCStock__c(name = 'my2346', plant__c = plant.id);
            insert stock2;
            
            article = new SCArticle__c(name = 'my123456789');
            insert article;
            
                
            // Create test material movements - this simulates one execute.    
            // Important - the Salesforce.com test framework only allows you to test one execute.    
            movements = new List<SCMaterialMovement__c>();
            for(integer i = 0; i < max; i++)
            {
                SCMaterialMovement__c m = new SCMaterialMovement__c(stock__c = stock.id,
                    article__c = article.id, type__c = SCfwConstants.DOMVAL_MATMOVETYP_DELIVERY, 
                    status__c = SCfwConstants.DOMVAL_MATSTAT_BOOK, qty__c = qty);
                movements.add(m);
    
                m = new SCMaterialMovement__c(stock__c = stock2.id,
                    article__c = article.id, type__c = SCfwConstants.DOMVAL_MATMOVETYP_DELIVERY, 
                    status__c = SCfwConstants.DOMVAL_MATSTAT_BOOK, qty__c = qty);
                movements.add(m);
            }
            insert movements;
            
            // System.debug('###SCbtcMaterialMovementTestData###);
            // System.debug(plant);
            // System.debug(stock);
            // System.debug(stock2);
            // System.debug(article);
            // System.debug(movements);
        } // public TestData       
    } // class TestData

} // SCbtcMaterialMovementTest
