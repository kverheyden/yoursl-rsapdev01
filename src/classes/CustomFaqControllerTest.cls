@isTest
private class CustomFaqControllerTest 
{

    static testMethod void myUnitTest() {
        // create Account
        List<Account> newAccounts = createAccount();

        //create FAQ
        Faq__c newFaq = createFAQ();
        
/*****************************************************************
    PART 1 - MAIN TEST
*****************************************************************/      
        
        //PageReference pageRef = Page.CokeConnectFAQ;
        PageReference pageRef = Page.CustomFAQAction;
        pageRef.getParameters().put('id',newFaq.Id);
        pageRef.getParameters().put('action','show');
        
        Test.setCurrentPageReference(pageRef);
        Test.setCurrentPage(pageRef);
        
        
        
        Faq__c tmpFaq = new Faq__c();
        Attachment tmpAtt = new Attachment();
        ApexPages.StandardController stdFaq = new ApexPages.StandardController(tmpFaq);
        CustomFaqController CCFE = new CustomFaqController(stdFaq);
        //Test.startTest();
        
        system.debug('### newFaq Id:' + ApexPages.currentPage().getParameters().get('id'));
        system.debug('### newFaq CCFE.myFaq:' + CCFE.myFaq);
        CCFE.myfile = tmpAtt;
        CCFE.editFaq();
        CCFE.saveFaq();
        
        CCFE.showList();
        
        // CREATE NEW FAQ
        Faq__c new2Faq = new Faq__c();
        new2Faq.Status__c = 'Draft';
        new2Faq.Title__c = 'Faq 2';
        new2Faq.Application__c = 'SalesApp';
        new2Faq.Category__c = 'General';
        
        tmpAtt.Name = 'File 1';
        tmpAtt.Body = Blob.valueOf('Unit Test Attachment Body');
        
        CCFE.newFaq();
        CCFE.myFaq = new2Faq;
        CCFE.myfile = tmpAtt;
        CCFE.saveFaq();
        CCFE.newFaq();
        CCFE.cancel();  
        CCFE.showList();


        CCFE.getmyfile();
         
        CCFE.delAtt();
        
        CCFE.myFaq = new2Faq;
        CCFE.myfile = tmpAtt;
        CCFE.saveFaq();
        CCFE.deleteAttachment();        
              


/*****************************************************************
    PART 2 - ID +  edit
*****************************************************************/
        //PageReference pageRef2 = Page.CokeConnectFAQ;
        PageReference pageRef2 = Page.CustomFAQAction;
        pageRef2.getParameters().put('id',new2Faq.Id);
        pageRef2.getParameters().put('action','edit');
        
        Test.setCurrentPageReference(pageRef2);
        Test.setCurrentPage(pageRef2);
        
        ApexPages.StandardController stdFaq2 = new ApexPages.StandardController(tmpFaq);
        CustomFaqController CCFE2 = new CustomFaqController(stdFaq2);
        
        CCFE2.cancel();
   
/*****************************************************************
    PART 3 - only ID
*****************************************************************/
        //PageReference pageRef3 = Page.CokeConnectFAQ;
        PageReference pageRef3 = Page.CustomFAQAction;
        pageRef3.getParameters().put('id',new2Faq.Id);

        
        Test.setCurrentPageReference(pageRef3);
        Test.setCurrentPage(pageRef3);
        
        ApexPages.StandardController stdFaq3 = new ApexPages.StandardController(tmpFaq);
        CustomFaqController CCFE3 = new CustomFaqController(stdFaq3);     
        
/*****************************************************************
    PART 4 - NO ID
*****************************************************************/
        //PageReference pageRef4 = Page.CokeConnectFAQ;
        PageReference pageRef4 = Page.CustomFAQAction;
        Test.setCurrentPageReference(pageRef4);
        Test.setCurrentPage(pageRef4);

        ApexPages.StandardController stdFaq4 = new ApexPages.StandardController(tmpFaq);
        CustomFaqController CCFE4 = new CustomFaqController(stdFaq4);         

/*****************************************************************
    PART 5 - only action=new
*****************************************************************/
        //PageReference pageRef5 = Page.CokeConnectFAQ;
        PageReference pageRef5 = Page.CustomFAQAction;
        pageRef5.getParameters().put('action','new');
        
        Test.setCurrentPageReference(pageRef5);
        Test.setCurrentPage(pageRef5);

        ApexPages.StandardController stdFaq5 = new ApexPages.StandardController(tmpFaq);
        CustomFaqController CCFE5 = new CustomFaqController(stdFaq5);
        
        CCFE5.cancel();
        
        //Test.stopTest();
        
    }




   
    // create test Account
    public static List<Account> createAccount()
    {
        List<Account> newAccounts = new List<Account>();    
        Account acc1 = new Account();
        acc1.Name = 'Sector 7';
        //acc1.OwnerId = newUsers[0].Id;
        newAccounts.add(acc1);
        
        insert newAccounts;
        return newAccounts;
    }
    
    // create test FAQ
    public static Faq__c createFAQ()
    {
        // create FAQ
        Faq__c newFaq = new Faq__c();  
        newFaq.Status__c = 'Draft';
        newFaq.Title__c = 'Faq 1';
        newFaq.Application__c = 'SalesApp';
        newFaq.Category__c = 'General';
        
        insert newFaq;      
        return newFaq;
    }
}
