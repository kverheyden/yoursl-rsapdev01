public with sharing class cc_cceag_ctrl_Login {
	public String username { get; set; }
	public String password { get; set; }

	public cc_cceag_ctrl_Login() {
		
	}

	public PageReference login() {
		PageReference ref = Site.login(username, password, null);
		if (ref != null) {
			/*ref = Page.ccrz__CCPage;
	        ref.getParameters().put('pageKey', 'Landing');*/
        	return ref;
		}
		else
			return null;
	}
	
	public PageReference forwardToCokeID() {		
        if (UserInfo.getUserType() == 'Guest') {
            ecomSettings__c setting = [SELECT value__C FROM ecomSettings__c WHERE Name='ECOMCokeIDLoginURL'];
//        	CCSettings__c settings = CCSettings__c.getInstance();        	
//        	PageReference ref = new PageReference(settings.ECOMCokeIDLoginURL__c);
        	PageReference ref = new PageReference(setting.value__c);
        	return ref;
        }
        else {
        	PageReference ref = Page.ccrz__CCPage;
            String url = ref.getUrl();
            url = url.replace('ccpage', 'CCPage');
            ref = new PageReference(url);
        	ref.getParameters().put('pageKey', 'Landing');
        	return ref;
        }
    }
}
