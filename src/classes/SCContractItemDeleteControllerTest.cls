/*
 * @(#)SCContractItemDeleteControllerTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * Test class checking the controler functions.
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
 @isTest
private class SCContractItemDeleteControllerTest
{
    private static SCContractItemDeleteController cic;

    static testMethod void contractItemDeleteControllerPositiv1() 
    {
        cic = new SCContractItemDeleteController();
        ApexPages.currentPage().getParameters().put('id', 'XXX');
        cic.deleteItems();
        
    }
    
    static testMethod void contractItemDeleteControllerPositiv2() 
    {
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.createInstalledBaseLocation(true);
        SCHelperTestClass.createInstalledBase(true);
        SCHelperTestClass.createProductModel(true);       
        SCHelperTestClass2.createContractTemplates(true);

        // Contract
        SCHelperTestClass2.createContract('Contract', SCHelperTestClass2.insuranceTemplate.Id, SCHelperTestClass.account.Id, SCHelperTestClass.installedBaseSingle.Id, true);
        
        // Item
        SCContractItem__c contractItem = new SCContractItem__c();
        contractItem.Contract__c = SCHelperTestClass2.contracts.get(0).Id;
        contractItem.InstalledBase__c = SCHelperTestClass.installedBaseSingle.Id;
        contractItem.MaintenancePrice__c = 50;
        contractItem.MaintenancePriceCalc__c = 40;
        contractItem.ValidFrom__c = system.today();
        

        ApexPages.currentPage().getParameters().put('id', SCHelperTestClass2.contracts.get(0).Id);
        
        List<SCContractItem__c> contractItemList = new List<SCContractItem__c>();
        contractItemList.add(contractItem);
        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(contractItemList);
       
               
        cic = new SCContractItemDeleteController(ssc);
        cic.deleteItems();
    }
}
