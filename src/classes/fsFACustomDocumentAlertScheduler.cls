/**
* @author           Oliver Preuschl
*                   H&W Consult GmbH
*                   Bahnhofstr. 3
*                   21244 Buchholz i.d.N.
*                   http://www.hundw.com
*
* @description      Sends internal information emails when a custom document is about to expire (in two weeks)
*
* @date             22.07.2014
*
* Timeline:
* Name              DateTime                Version        Description
* Oliver Preuschl   22.07.2014              *1.0*          Creatd the class
*/

global class fsFACustomDocumentAlertScheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
		fsFACustomDocumentAlert LO_CustomDocumentAlert = new fsFACustomDocumentAlert();
		LO_CustomDocumentAlert.sendExpiringCustomDocumentsEmails();
	}
}
