public with sharing class CalculatorLoaderController {

    public string getLoadCalculator() {
        try {
            String calculatorName = ApexPages.currentPage().getParameters().get('name');
            return [select id, body from document where contenttype='text/html' and DeveloperName=:calculatorName limit 1].body.tostring();
        } catch(exception e) { return 'Calculator not found';}
    }
    
    public string getCalculatorCombinedMenuId(){    
        return getCalculatorId('CalculatorCombinedMenu');
    }
    
    public string getCalculatorPITAId(){    
        return getCalculatorId('CalculatorPITA');
    }
    
     public string getCalculatorIPPCooledId(){    
        return getCalculatorId('CalculatorIPPCooled');
    }
    
    public string getCalculatorIPPUncooledId(){    
        return getCalculatorId('CalculatorIPPUncooled');
    }
    
    
    public string getCalculatorId(String name){
    
    try {
            return [select id from document where contenttype='text/html' and DeveloperName=:name limit 1].id;
        } catch(exception e) { return null; }
    }
    
    
}
