/*
 * @(#)SCutilInterventionValidationTest.cls
 *
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * @author Dietrich Herdt <dherdt@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCutilInterventionValidationTest
{

    static testMethod void validate()
    {
        SCutilInterventionValidation validation  = new SCutilInterventionValidation();
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet(true);
        
        SCboOrder boOrder = new SCboOrder(SCHelperTestClass.Order);
         
        boOrder.readById(SCHelperTestClass.Order.Id);
        
        boOrder.boOrderItems[0].orderItem.InstalledBase__r.InstallationDate__c = Date.today();
        for (SCboOrderItem item : boOrder.boOrderItems)
        {
            System.debug('#### List Length');
            List<SCboOrderRepairCode> repairCodes = new List<SCboOrderRepairCode>();
            item.boRepairCodes = repairCodes;
            repairCodes.add(new SCboOrderRepairCode());
            System.debug('#### List Length' + item.boRepairCodes.size());
            // item.orderItem.InstalledBase__r = SCHelperTestClass.installedBaseSingle;
        }
        
        try
        {
            validation.validate(boOrder, new SCStock__c());
            // no error occurred and we expect no error
            System.assert(true);
        }
        catch (Exception e)
        {
            // An error occurred
            System.assert(false);
        }
    }
    
    static testMethod void validateOrderTest()
    {
        SCutilInterventionValidation validation  = new SCutilInterventionValidation();
        
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet(true);
        // upsert SCHelperTestClass.order;
        SCboOrder boOrder = new SCboOrder(SCHelperTestClass.Order);
        boOrder.order.UsedBy__c = 'Service';
        
        System.debug('#### boOrder'+boOrder);
        
        // create a copy for testing
        SCboOrder testboOrder = boOrder;
        
        // set order type to null and call the method
        testboOrder.order.Type__c = null;
        validateOrderTestHelper(testboOrder, validation);
        
        // set order type to null and call the method
        testboOrder = boOrder;
        
        validateOrderTestHelper(testboOrder, validation);
               
        // set duration to 1 minute
        testboOrder = boOrder;
        testboOrder.order.Duration__c = 1;
        validateOrderTestHelper(testboOrder, validation);
               
        // set invoicing type to null and call the method
        testboOrder = boOrder;
        testboOrder.order.InvoicingType__c = null;
        validateOrderTestHelper(testboOrder, validation);
        
        // set invoicing type to SCfwConstants.DOMVAL_INVOICINGTYPE_CONTRACT 
        // and call the method
        testboOrder = boOrder;
        testboOrder.order.InvoicingType__c = SCfwConstants.DOMVAL_INVOICINGTYPE_CONTRACT;
        validateOrderTestHelper(testboOrder, validation);
                
        // set invoicing subtype to null and call the method
        testboOrder = boOrder;
        testboOrder.order.InvoicingSubtype__c = null;
        validateOrderTestHelper(testboOrder, validation);  
        
        // set payment type to null and call the method
        testboOrder = boOrder;
        testboOrder.order.PaymentType__c = null;
        validateOrderTestHelper(testboOrder, validation);
          
        // set payment type to SCfwConstants.DOMVAL_PAYMENTTYPE_CONTRACT 
        // and call the method
        testboOrder = boOrder;
        testboOrder.order.PaymentType__c = SCfwConstants.DOMVAL_PAYMENTTYPE_CONTRACT;
        validateOrderTestHelper(testboOrder, validation);
          
        // remove order items and call the method
        testboOrder = boOrder;
        testboOrder.boOrderItems.clear();
        validateOrderTestHelper(testboOrder, validation);
    }    

    /*
     * The test for validateSerialNumber()
    */
    static testMethod void validateSerialNumberTest()
    {
        SCutilInterventionValidation validation  = new SCutilInterventionValidation();
        SCHelperTestClass.createInstalledBase(true);

        // save the installed base as local copy
        SCInstalledBase__c testInstalledBase = SCHelperTestClass.installedBaseSingle;

        // call the method for installed base with not valid serial number
        validation.validateSerialNumber(testInstalledBase);

        // set the serial number to null
        testInstalledBase.SerialNo__c = null;

        // call the method for installed base with the serial number equals null  
        try
        {
            validation.validateSerialNumber(testInstalledBase);
            // no error occurred
            System.assert(false);
        }
        catch (Exception e)
        {
            // An error occurred
            System.assert(true);
        }
        
        // set the serial number to invalid number, for example 11 digits
        testInstalledBase.SerialNo__c = '13344222910';

        // call the method for installed base with the serial number equals null  
        try
        {
            validation.validateSerialNumber(testInstalledBase);
            // no error occurred
            System.assert(true);
        }
        catch (Exception e)
        {
            // An error occurred
            System.assert(true);
        }
    }

    /*
     * The test for validateInstalledBase()
     */
    static testMethod void validateInstalledBaseTest()
    {
        SCutilInterventionValidation validation  = new SCutilInterventionValidation();
        // set the installed base to null
        SCInstalledBase__c testInstalledBase = new SCInstalledBase__c ();

        // call the method for installed base without installed date
        try
        {
            validation.validateInstalledBase(testInstalledBase);
            // no error occurred
            System.assert(false);
        }
        catch (Exception e)
        {
            // An error occurred
            System.assert(true);
        }
        
        // only simple test, the method is empty
        validation.validateTimes();
    }

    /**
     * The test for validateProduct()
     */
    static testMethod void validateProductTest()
    {
        SCutilInterventionValidation validation  = new SCutilInterventionValidation();
        SCHelperTestClass.createInstalledBase(true);

        // save the installed base as local copy
        SCInstalledBase__c testInstalledBase = SCHelperTestClass.installedBaseSingle;

        // call the method for installed base with the product model
        validation.validateProduct(testInstalledBase);

        // set the product model and the brand to null and call the method
        testInstalledBase = SCHelperTestClass.installedBaseSingle;
        testInstalledBase.ProductModel__c = null;
        testInstalledBase.Brand__c = null;
        validateProductTestHelper(testInstalledBase, validation);

        // the the ProductUnitClass to null and call the method
        testInstalledBase = SCHelperTestClass.installedBaseSingle;
        testInstalledBase.ProductUnitClass__c = null;
        validateProductTestHelper(testInstalledBase, validation);

        // the the ProductUnitClass to null and call the method
        testInstalledBase = SCHelperTestClass.installedBaseSingle;
        testInstalledBase.ProductUnitClass__c = null;
        validateProductTestHelper(testInstalledBase, validation);

        // the the ProductUnitType to null and call the method
        testInstalledBase = SCHelperTestClass.installedBaseSingle;
        testInstalledBase.ProductUnitType__c = null;
        validateProductTestHelper(testInstalledBase, validation);

        // the the ProductGroup to null and call the method
        testInstalledBase = SCHelperTestClass.installedBaseSingle;
        testInstalledBase.ProductGroup__c = null;
        validateProductTestHelper(testInstalledBase, validation);

        // the the ProductPower to null and call the method
        testInstalledBase = SCHelperTestClass.installedBaseSingle;
        testInstalledBase.ProductPower__c = null;
        validateProductTestHelper(testInstalledBase, validation);

        // the the ProductPower to null and call the method
        testInstalledBase = SCHelperTestClass.installedBaseSingle;
        testInstalledBase.ProductEnergy__c = null;
        validateProductTestHelper(testInstalledBase, validation);
    }

    /**
     * Test helper for validateProduct()
     */
    public static void validateProductTestHelper (SCInstalledBase__c testInstalledBase, 
                                                  SCutilInterventionValidation validation)
    {
        try
        {
            validation.validateProduct(testInstalledBase);
            // no error occurred
            System.assert(false);
        }
        catch (Exception e)
        {
            // An error occurred
            System.assert(true);
        }
    }
    

    /**
     * Test helper for validateOrder()
     */
    public static void validateOrderTestHelper (SCboOrder boOrder, 
                                                SCutilInterventionValidation validation)
    {
        try
        {
            validation.validateOrder(boOrder);
            // no error occurred
            System.assert(false);
        }
        catch (Exception e)
        {
            // An error occurred
            System.assert(true);
        }
    }    
}
