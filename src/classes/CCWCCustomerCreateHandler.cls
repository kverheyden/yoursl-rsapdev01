/**
* @author		Austen Buennemann
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				a.buennemann@yoursl.de
*
* @description	Handler for Record__c callouts of CCWCCustomerCreate
*
* @date			24.03.2014
*
* Timeline:
* Name               DateTime                  Description
* Austen Buennemann  24.03.2014 10:29          Class created
* Bernd Werner       31.08.2014 09:00          add customer update process
* Austen Buennemann  03.09.2014 10:30          check and comment
*/

public with sharing class CCWCCustomerCreateHandler {
	
	public Set<String>relevantFieldsForSAP { 
		get{
			Set<String> tmpFieldList = new Set<String>();
			if(relevantFieldsForSAP == null){				
				for (Schema.FieldSetMember f : Schema.SObjectType.Request__c.fieldSets.FieldsToSendToSAPforChange.getFields()){
					system.debug('###FieldName');
					tmpFieldList.add(f.getFieldPath());
				}
			}
			return tmpFieldList;		
		} 
		set; 
	} 
	
	/**
	* Check if the changed field are relevant for transfering the record to SAP
	*
	* @param fieldString Field-String
	* @return boolean true or false
	* 
	*/
	public Boolean checkSAPRelevance (String fieldString){
		if(fieldString==null || fieldString==''){
			return false;
		}
		for(String tmpField : fieldString.split(',')){
			if(relevantFieldsForSAP.contains(tmpField)){			
				return true;
			}
			// commented out of the if clause: tmpField.length() == 18 &&  BW 08.10.2014
		}
		
		return  false;
	} 
	
	/**
	* Prepare single callout for CustomerCreate to SAP
	*
	* @param ibid 	SFDC Id
	* @param test	Testmode
	* 
	*/
	public void callout(String ibid, boolean test){
		List <Request__c> request =  [Select Id, Send_to_SAP__c, RecordTypeId From Request__c Where Id =: ibid];
		CCWCCustomerCreateHandler send = new CCWCCustomerCreateHandler();
		send.sendCallout(request, test, true);
	}
	
	/**
	* Prepare trigger callouts for CustomerCreate to SAP
	*
	* @param newRequests 	Request Records
	* @param test			Testmode
	* @param execute		If true, send callout to SAP without check Send_to_SAP__c option.
	* 
	*/
    public void sendCallout(Request__c [] newRequests, Boolean isTest, boolean execute){   	
    	Set<Id> idSet = new Set<Id>();
    	//********************************************************************
    	// Filter all requests that schould not be submitted to SAP
    	//********************************************************************
    	
    	SalesAppSettings__c updateType = [Select Value__c From SalesAppSettings__c Where Name = 'RequestMasterDataUpdateRecordTypeId' LIMIT 1]; 
    	for(Request__c entry: newRequests){
    		if(entry != null){
	    		if( updateType != null  && updateType.Value__c != null && updateType.Value__c == entry.RecordTypeId){
	    			// if(	entry.AccountID2__c=='' || entry.AccountID2__c==null)
	    			if(entry.RelatedAccount__c==null || (!checkSAPRelevance(entry.ChangedFields__c) && !checkSAPRelevance(entry.DeletedFieldsCSV__c)))
	    			{
	    				continue;
	    			}
		    	}
	    		idSet.add(entry.Id);
    		}
    	}
    	//********************************************************************
		// End Filter
		//********************************************************************
		
		
		Map<Id, Request__c> request = new Map<Id, Request__c>( [Select VerificationDocumentTypes__c, VatId__c, VHWednesdayTo__c, VHWednesdayFrom__c, 
										VHTuesdayTo__c, VHTuesdayFrom__c, VHThursdayTo__c, VHThursdayFrom__c, VHMondayTo__c, VHMondayFrom__c, VHFridayTo__c, 
										VHFridayFrom__c, UnrestrictedDelivery__c, TaxNumber__c, SystemModstamp, Subtradechannel__c, Sponsoring__c, 
										Source__c, SecondMainBrandWater__c, SalesVolumeYear__c, SalesVolumeOrder__c, SFID__c, RecyclingViaCceag__c, 
										RecyclingStreet__c, RecyclingPostalCode__c, RecyclingPhone__c, RecyclingName__c, RecyclingName2__c, 
										RecyclingMobile__c, RecyclingIban__c, RecyclingHouseNo__c, RecyclingEmail__c, RecyclingCreditTo__c, 
										RecyclingCreditToBankAccount__c, RecyclingCity__c, RecordTypeId, RecordTypeDeveloperName__c, 
										PricingWaterConcept__c, PricingUnitThroughputYearly__c, PricingStartDate__c, PricingCustomerOwnedUnit__c, 
										PricingCaseCompensation__c, PaymentType__c, PaymentDirectDebit__c, PayerStreet__c, PayerPostalCode__c, 
										PayerPhone__c, PayerName__c, PayerName2__c, PayerMobile__c, PayerIban__c, PayerHouseNo__c, PayerFax__c, 
										PayerEmail__c, PayerCustomerNumber__c, PayerCity__c, OwnerId, OutletWebsite__c, OutletStreet__c, 
										OutletSeason2To__c, OutletSeason2From__c, OutletSeason1To__c, OutletSeason1From__c, 
										OutletPostalCode__c, OutletPhone__c, OutletMobile__c, OutletKeyAccountNumber__c, OutletIsSeasonal__c, 
										OutletIsPayer__c, OutletIsBillTo__c, OutletHouseNo__c, OutletHasChain__c, OutletFax__c, OutletEmail__c, 
										OutletDistrict__c, OutletCustomerClass__c, OutletCompanyName__c, OutletCity__c, OperatorSalutation__c, 
										OperatorLastName__c, OperatorIsIndividual__c, OperatorFirstName__c, OperatorCompanyName__c, Name, 
										MarketingAttributeIDsCSV__c, MainBrandWheatBeer__c, MainBrandWater__c, MainBrandCola__c, MainBrandBeer__c, 
										LowEmissionZoneType__c, LeadingOutlet__c, IsInLowEmissionZone__c, IsDeleted, IntroductionCateringBottle__c, Id, ID2__c, GfghCustomerNumbersCSV__c, 
										EndDateWaterContractBinding__c, EndDateColaContractBinding__c, EndDateBeerContractBinding__c, DeliveryType__c, 
										DHWednesdayTo__c, DHWednesdayFrom__c, DHTuesdayTo__c, DHTuesdayFrom__c, DHThursdayTo__c, DHThursdayFrom__c, 
										DHMondayTo__c, DHMondayFrom__c, DHFridayTo__c, DHFridayFrom__c, CurrencyIsoCode, CreatedDate, CreatedById, 
										EmployeeNumberOfCreator__c, CreatedByDeNumber__c, EmployeeNumberOfCreatorsGVL__c, ContractObligationWater__c, ContractObligationCola__c, ContractObligationBeer__c, 
										ContactSalutation__c, ContactPhone__c, ContactLastName__c, ContactFirstName__c, CentralEffort__c, ShipToStreet__c, 
										ShipToPostalCode__c, ShipToPhone__c, ShipToName__c, ShipToName2__c, ShipToMobile__c, ShipToHouseNo__c, 
										ShipToFax__c, ShipToEmail__c, ShipToCity__c, BillToCustomerNumber__c, LeadingOutlet__r.ID2__c, 
										BlockingReason__c, Comment__c, Market__c, RecyclingFax__c, CollectionAuth__c, DeliveryTransactionCode__c, PaymentMethod__c,
										AccountID2__c, BillingCompanyName__c, BillingFirstName__c, BillingLastName__c, BillingOutletName__c, ContactAdditionalInformation__c, 
										ContactBirthdate__c, NumberofAttachments__c, PayerBankCode__c, PayerCompanyName__c, PayerFirstName__c, PayerIsIndividual__c, 
										PayerLastName__c, PayerOutletName__c, RecyclingBankCode__c, RecyclingCompanyName__c, RecyclingFirstName__c, RecyclingIsIndividual__c, 
										RecyclingLastName__c, RecyclingOutletName__c, RelatedAccount__c, ShipToCompanyName__c,
										ShipToFirstName__c, ShipToIsIndividual__c, ShipToLastName__c, ShipToOutletName__c, Status__c, 
										CreatedBy.Name, RecordType.Name, Owner.Name, CreatedBy.ERPSalesArea__c, CreatedBy.ERPDivision__c, 
										CreatedBy.ERPDistributionChannel__c, CreatedBy.ERPCompanyCode__c,										
										PricingAnnualOrderDiscount__c, PricingAnnualOrderDiscount__r.DiscountLevel__c,
										PricingPaymentDiscount__c, PricingPaymentDiscount__r.DiscountPercent__c,PricingPaymentDiscount__r.DiscountLevel__c,PricingPaymentDiscount__r.PaymentType__c,   
										SponsoringDiscountLevel__c, 
										PricingUnitThroughputDiscount__c, PricingUnitThroughputDiscount__r.PemPom__c, PricingUnitThroughputDiscount__r.DiscountLevel__c,
										PricingWaterConceptDiscount__c, PricingWaterConceptDiscount__r.DiscountLevel__c,
										CreatedBy.SalesGroup__c, CreatedBy.SalesOffice__c, CreatedBy.EmployeeNumber__c, CreatedBy.EmployeeNumberGVL__c,
										ChangedFields__c, DeletedFieldsCSV__c,CustomDeviceThroughputIndicator__c, DistributionChannel__c
										From Request__c  Where Id =: idSet]);  	
										// PricingSponsoringDiscount__c, PricingSponsoringDiscount__r.DiscountLevel__c,
		List<RequestAttachment__c>	attachments = new List<RequestAttachment__c>([SELECT Id, Request__c, Type__c, AttachmentId__c, ValidFrom__c, ValidTo__c, DocumentNumber__c 
												FROM RequestAttachment__c Where Request__c =: idSet]);
		// Translate Marketing attributes
		// Set with unique ids of marketing attributes (used in Query)
		Set<Id>marketingAttribuestIds = new Set<Id>();
		// Map with Request__c IDs and a set with IDs for marketing attributes
		Map<ID,Set<ID>>request2Attributes = new Map<ID,Set<ID>>();
		List<String> tmpIdList = new List<String>();
		// Collect all IDs of marketing atttributes
		for (Request__c tmpRequest : request.values()){
			if(tmpRequest.MarketingAttributeIDsCSV__c!=null){
				tmpIdList = tmpRequest.MarketingAttributeIDsCSV__c.split(',');
				if(tmpIdList.size()>0){
					for (String tmpAttId :tmpIdList){
						if (tmpAttId.length() == 18){
							// Put the ID to the set if not already exists
							if(!marketingAttribuestIds.contains(tmpAttId)){
								marketingAttribuestIds.add(tmpAttId);
							}
							// Add the request to the map if not already exists and add the ID
							if(!request2Attributes.containsKey(tmpRequest.Id)){
								request2Attributes.put(tmpRequest.Id,new Set<Id>());
							}
							request2Attributes.get(tmpRequest.Id).add(tmpAttId);
						}
					}
				}
			}
		}
		// if we have marketing attributes in our set, perform the query
		Map<Id, MarketingAttribute__c> attributes = new Map<Id, MarketingAttribute__c>( [Select Id, UniqueKey__c from MarketingAttribute__c where Id in : marketingAttribuestIds]);
		// Translate and replace
		for (Id reqId : request2Attributes.keySet()){
			String replaceString = '';
			for (Id attId : request2Attributes.get(reqId)){
				if(attributes.containsKey(attId)){
					if(replaceString==''){
						replaceString=attributes.get(attId).UniqueKey__c;
					}
					else{
						replaceString += ','+attributes.get(attId).UniqueKey__c;
					}
				}
			}
			if(request.containsKey(reqId)){
				request.get(reqId).MarketingAttributeIDsCSV__c = replaceString;
			}
		}
		
		
									
    	if(newRequests != null && idSet.size()>0){
	    	for(Request__c newRequest : newRequests){
	    		if(newRequest.Send_to_SAP__c || execute){  
					System.debug('Creation process: isFuture = ' + System.isFuture()); 
			    	String item = JSON.serialize(request.get(newRequest.Id));
			    	if(attachments != null){
			    		List<String> attach = new List<String>();
			    		for(RequestAttachment__c attachment : attachments){
			    			if(attachment.Request__c == request.get(newRequest.Id).Id){
			    				attach.add(JSON.serialize(attachment));
			    			}
			    		}	
						if (System.isFuture())
			    			CCWCCustomerCreate.simpleCallout(newRequest.Id, isTest, item, attach);
						else
			    			CCWCCustomerCreate.futurecallout(newRequest.Id, isTest, item, attach);
			    	}else{
						if (System.isFuture())
							CCWCCustomerCreate.simpleCallout(newRequest.Id, isTest, item, null);	
						else
							CCWCCustomerCreate.futurecallout(newRequest.Id, isTest, item, null);	
			    	}
	    		}	    		
    		}
    	}
    }
    
    /**
	* Prepare trigger callouts for CustomerCreate to SAP with RequestAttachment__c
	*
	* @param attachments 	RequestAttachment__c objects
	* @param test			Testmode
	* 
	*/
    
    public void sendRequestCallout(RequestAttachment__c [] attachments, boolean test){
    	Set<Id> idSet = new Set<Id>();
    	for(RequestAttachment__c attachment : attachments){
    		idSet.add(attachment.Request__c);
    	}
    	
    	List<Request__c> requests = new List<Request__c>([Select NumberofAttachments__c, Id, Send_to_SAP__c From Request__c r Where Id =: idSet]);
    	Set<Id> idSet2 = new Set<Id>();
    	for(Request__c entry : requests){
    		idSet2.add(entry.Id);
    	}
    	List <RequestAttachment__c> reqAttachments =  new List <RequestAttachment__c>([Select Id, Request__c From RequestAttachment__c 
    													Where Request__c  =: idSet2]);
    													
    	List<Request__c> newRequests = new List<Request__c>();
    	integer attachment_nr;
    	if(requests != null){
    		
	    	for (Request__c req : requests){
	    		attachment_nr = 0;
	    		for(RequestAttachment__c att : reqAttachments){
	    			if(req.Id == att.Request__c){
	    				attachment_nr++;
	    			}
	    		}
	    		if(req.NumberofAttachments__c > 0 && req.NumberofAttachments__c == attachment_nr){
	    			newRequests.add(req);
	    		}
	    	}
    	}
    	CCWCCustomerCreateHandler send = new CCWCCustomerCreateHandler();
		if(!newRequests.isEmpty()){ 
	    	send.sendCallout(newRequests, test, true);
	    	
	    }
    }
    
    /**
	* Prepare trigger callouts for CustomerCreate to SAP with Attachments
	*
	* @param attachments 	Attachment objects
	* @param test			Testmode
	* 
	*/    
    public void sendRequest(Attachment [] attachments, boolean test){
    	Set<Id> idSet = new Set<Id>();
    	boolean checkexecute = false;
   	    	
    	for(Attachment att : attachments){
    		if(att.ParentId.getSObjectType().getDescribe().getName() == 'RequestAttachment__c'){
    			idSet.add(att.ParentId);
    			checkexecute = true;    			
    		}
    	}
		if(checkexecute){
			List<RequestAttachment__c> reqAttachments =  new List<RequestAttachment__c>([Select Id, Request__c, AttachmentId__c From RequestAttachment__c Where Id  =: idSet]);
			List<RequestAttachment__c> updateReqAtt =  new List<RequestAttachment__c>(); 													
    		
	    	for(Attachment att : attachments){
	    		if(att.ParentId.getSObjectType().getDescribe().getName() == 'RequestAttachment__c'){
	    			
	    			if(!reqAttachments.isEmpty()){ 
	    				for(RequestAttachment__c req : reqAttachments){
	    					if(att.ParentId == req.Id && (req.AttachmentId__c == null || req.AttachmentId__c == '')){
								req.AttachmentId__c = att.Id;
								updateReqAtt.add(req);								
	    					}
	    				}
	    			}
	    		}
	    	}
			CCWCCustomerCreateHandler send = new CCWCCustomerCreateHandler();  	    	
	    	if(!updateReqAtt.isEmpty()){ 
	    		update updateReqAtt;
	    		send.sendRequestCallout(updateReqAtt, test);	
	    	}									
		}
    }
 }
