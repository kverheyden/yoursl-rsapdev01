/*
 * @(#)SCTimereportController.cls
 *
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Implements the controller for simple hovers that can be used to
 * display context sensitive information. The controller evalates
 * the mode and oid field and reads the corresponding data.
 *
 * @author Dietrich Herdt  <dherdt@gms-online.de>
 * @author Sebastian Schrage  <sschrage@gms-online.de>
 * @version $Revision$, $Date$
 */
 
public with sharing class SCTimereportController extends SCfwPageControllerBase
{
    public PageReference changeTimeReportStatus() 
    {
        return null;
    }
    
    public String timeReportStatus { get; set; }
    public String testStatus { get; set; }
    public SCTool__c toolData { get; set; }
    public String day { get; set; }
    public String resourceAlias { get; set; }
    public String rid { get; set; }
    public String resourceName { get; set; }
    public SCTimeReport__c timeReport { get; set; }
    public SCResource__c resource { get; set; }
    public String userId { get; set; }
    
    public String dayStartListString { get; set; }
    public Boolean isOpenDayExists { get; set; }
    public Boolean openDayButton { get; set; }
    
    Boolean globalChangedResource = false;
    Id globalSelectedResourceId;
    List<SCResource__c> globalListResource;
    SCApplicationSettings__c globalAppset;
    
    
    public SCTimereportController()
    {    
        SCTimereportControllerConstructorCode();
    }
    
    public void SCTimereportControllerConstructorCode()
    {
        toolData = new SCTool__c();
        timeReport = new SCTimeReport__c();
        //dayStartListString = new List <String> ();
        if (globalAppset == null)
        {
            globalAppset = SCApplicationSettings__c.getInstance();
        }
        
        if(globalAppset.TIMEREPORT_OPENDAYBUTTON__c)
        {
            openDayButton = true;
        }
        else
        {
            openDayButton = false;
        }
        System.debug('#### SCTimereportController openDayButton '+openDayButton);
                    
        toolData.Start__c = date.TODAY();
        
        resource = new SCResource__c ();
        
        userId = UserInfo.getUserId();      
        // search the resource for the user
        try
        {
            if (globalAppset.RESOURCE_CHANGE_ALLOWED__c == 1)
            {
                if ((globalListResource == null) || (globalListResource.size() == 0))
                {
                    globalListResource = [SELECT Id, Name, Employee__c FROM SCResource__c ORDER BY Name];
                }
                
                if (globalSelectedResourceId == null)
                {
                    Boolean found = false;
                    for (SCResource__c res : globalListResource) { if (res.employee__c == userId) { resource = res; found = true; } }
                    if (!found) { resource = globalListResource.get(0); }
                }
                else
                {
                    for (SCResource__c res : globalListResource) { if (res.Id == globalSelectedResourceId) { resource = res; } }
                }
                
                if (resource.employee__c == userId) { globalChangedResource = false; }
                else { globalChangedResource = true; }
            }
            else
            {
                resource = [SELECT Id, Name, Employee__c 
                            FROM SCResource__c 
                            WHERE employee__c = :userId];
            }
        }
        catch (Exception e)
        {
            resource.Name = 'GMS' + e;        
        }        
    }
    
    /*
     * System-Admins may choose the Resource.
     */
    public Boolean getAllowedToChooseTheResource()
    {
        try
        {
            if (globalAppset.RESOURCE_CHANGE_ALLOWED__c == 1) { return true; }
        }
        catch (Exception e)
        {
            System.debug(Logginglevel.ERROR, 'Cannot read the field from SCApplicationSettings (SCTimereportController).');   
        }
        return false;
    }
    
    /*
     * Creates a List of SelectedOptions, which is need for the choice of a resource
     */
    public List<SelectOption> getAllResourceList()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        if ((globalListResource == null) || (globalListResource.size() == 0))
        {
            globalListResource = [SELECT Id, Name, Employee__c FROM SCResource__c ORDER BY Name];
        }
        
        for (SCResource__c res : globalListResource)
        {
            options.add(new SelectOption(res.Id,res.Name));
        }
        
        return options;
    }
    
    public String getSelectedResource()
    {
        if (globalSelectedResourceId == null)
        {
            return resource.Id;
        }
        return globalSelectedResourceId;
    }
    
    public void setSelectedResource(String ResourceId)
    {
        try
        {
            globalSelectedResourceId = (Id) resourceId;
        }
        catch (Exception e)
        {
            System.debug(Logginglevel.ERROR, 'Unexpected Error (SCTimereportController.setSelectedResource(' + ResourceId + ')).');   
        }
    }
    
    public PageReference ChangeResource()
    {
        SCTimereportControllerConstructorCode();
        OnSelect();
        System.debug('###+###resource: ' + resource);
        return null;
    }
    
    public String getDifferentRescource()
    {
        if (globalChangedResource) { return 'true'; }
        return 'false';
    }
    
    public String getRescourceId()
    {
        if (resource != null) { return resource.Id; }
        return null;
    }

    /*
     * Calculates the number of day between current day and day entered.
     */ 
    public Integer getDayOffset() 
    {
        //if ((null != (SCProcessOrderController)pageController) && 
        //    (null != ((SCProcessOrderController)pageController).toolObj))
        if (toolData.Start__c != null)
        {
            return Date.today().daysBetween(toolData.Start__c);
        }
        return 0;
    }
      
    /**
     * Set the parameter for the component (rid and day).
     *
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference OnSelect() 
    {   
        // clear the list
        if  (dayStartListString != null)
        {
            dayStartListString = null;
        }
        
        // convert into date string       
        Datetime startDateTime = datetime.newInstance (toolData.Start__c.year(),
                                                       toolData.Start__c.month(),
                                                       toolData.Start__c.day(),
                                                       00, 00, 00);
        day = startDateTime.format('yyyyMMdd');

        //testStatus = timeReport.Status__c;
        rid = resource.Id;
        
        //=============================================================
        // NEW
        SCboTimereport boTimereport = new SCboTimereport();
        
        Datetime startTime =
                Datetime.newInstance(Integer.ValueOf(day.substring(0,4)),
                                     Integer.ValueOf(day.substring(4,6)),
                                     Integer.ValueOf(day.substring(6,8)),
                                     00, 00, 0);

        Datetime endTime =
                Datetime.newInstance(Integer.ValueOf(day.substring(0,4)),
                                     Integer.ValueOf(day.substring(4,6)),
                                     Integer.ValueOf(day.substring(6,8)),
                                     23, 59, 59);
        
        try
        {   
            // search all day starts for the day                          
            List <SCTimeReport__c> dayStartList = 
                boTimereport.readAllDayStart(rid, startTime, endTime);
            
            List <String> dayStartListIds = boTimereport.readAllDayStartsWithLink(rid, startTime, endTime);
            
            System.debug('#### OnSelect dayStartList '+dayStartList);
            System.debug('#### OnSelect dayStartListIds '+dayStartListIds);
            
            /*
            for (SCTimereport__c timeReportStart :dayStartList)
            {
                //dayStartListString.add(timeReportStart.Id);
                if (dayStartListString == null)
                {
                    dayStartListString = timeReportStart.Id;
                }
                else
                {
                    dayStartListString = dayStartListString + ',' +
                                         timeReportStart.Id;
                }
            }
            */
            
            for (String timeReportStartId :dayStartListIds)
            {
                //dayStartListString.add(timeReportStart.Id);
                if (dayStartListString == null)
                {
                    dayStartListString = timeReportStartId;
                }
                else
                {
                    dayStartListString = dayStartListString + ',' +
                                         timeReportStartId;
                }
            }
            
            System.debug('#### OnSelect dayStartListString '+dayStartListString);
        }
        catch (Exception e)
        {
            dayStartListString = null;
        }    
        System.debug('### The day starts are '+dayStartListString);
        //===============================================================                   
        return null;
        //return startDate;
    }
}
