/*
 * @(#)SCStockProfileExtension.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCStockProfileExtension extends SCMatMoveBaseExtension
{
    private SCStockProfile__c stockProfile;
    private List<SCStockProfileItem__c> profileItems;
    
    public String input { get; set;}
    public String output { get; set;}

    public Boolean checked { get; private set; }
    public Boolean importOk { get; private set; }
    
    
    /**
     * Initialize the page objects for selecting an article and 
     * a material movement type.
     * Sets the quamtity for the actual stock to 0.
     *
     * @param    controller    the standard controller
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCStockProfileExtension(ApexPages.StandardController controller)
    {
        System.debug('#### SCStockProfileExtension()');
        
        this.stockProfile = (SCStockProfile__c)controller.getRecord();
        checked = false;
        importOk = false;
        
        input = SCStockItem__c.Article__c.getDescribe().getLabel();
        input += '\t' + SCStockItem__c.MinQty__c.getDescribe().getLabel();
        input += '\t' + SCStockItem__c.MaxQty__c.getDescribe().getLabel();
        input += '\t' + SCStockItem__c.ReplenishmentQty__c.getDescribe().getLabel();
        input += '\n';
        
        String articleNoLabel = SCStockItem__c.Article__c.getDescribe().getLabel();
        output = articleNoLabel; 
        output += TAB_SEP + SCStockItem__c.MinQty__c.getDescribe().getLabel();
        output += TAB_SEP + SCStockItem__c.MaxQty__c.getDescribe().getLabel();
        output += TAB_SEP + SCStockItem__c.ReplenishmentQty__c.getDescribe().getLabel();
        output += TAB_SEP + System.Label.SC_app_Status + TAB_SEP + System.Label.SC_app_Remark + '<br>';
        output += '<br><br><br><br><br>';
    } // SCStockProfileExtension
    
    /**
     * Checks the items for the current stock profile.
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference checkItems()
    {
        System.debug('#### checkItems()');
        
        profileItems = [select Id, Name, Article__c, MinQty__c, MaxQty__c, ReplenishmentQty__c from SCStockProfileItem__c where StockProfile__c = :stockProfile.Id];
        output = checkProfileItems(input, profileItems, stockProfile.Id);
        checked = (output.indexOf(System.Label.SC_btn_Ok) > 0);

        return null;
    } // checkItems
    
    /**
     * Imports the items for the current stock profile.
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference importItems()
    {
        System.debug('#### importItems()');
        
        try
        {
            // update or insert the stock profile items
            upsert profileItems;
            importOk = true;
        }
        catch (DmlException e)
        {
            ApexPages.addMessages(e);
        }
        return null;
    } // importItems
} // SCStockProfileExtension
