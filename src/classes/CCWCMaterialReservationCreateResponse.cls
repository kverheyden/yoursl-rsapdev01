/*
 * @(#)CCWCMaterialReservationCreateResponse.cls 
 * 
 * Copyright 2012 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
global without sharing class CCWCMaterialReservationCreateResponse  
{
    
    public static void processMaterialMovementReservationResponse(String messageID, String requestMessageID, String headExternalID, 
                                                  List<CCWSGenericResponse.ReferenceItem> referenceList, 
                                                  String MaximumLogItemSeverityCode, 
                                                  CCWSGenericResponse.LogItemClass[] logItemArr, 
                                                  List<SCInterfaceLog__c> interfaceLogList,
                                                  CCWSGenericResponse.GenericServiceResponseMessageClass GenericServiceResponseMessage)
    {
        SCInterfaceLog__c responseInterfaceLog = null;
        processMaterialMovementReservationResponse(messageID, requestMessageID, headExternalID, 
                                                  referenceList, 
                                                  MaximumLogItemSeverityCode, 
                                                  logItemArr, 
                                                  interfaceLogList,
                                                  GenericServiceResponseMessage,
                                                  responseInterfaceLog);
    }
    
    public static void processMaterialMovementReservationResponse(String messageID, String requestMessageID, String headExternalID, 
                                                  List<CCWSGenericResponse.ReferenceItem> referenceList, 
                                                  String MaximumLogItemSeverityCode, 
                                                  CCWSGenericResponse.LogItemClass[] logItemArr, 
                                                  List<SCInterfaceLog__c> interfaceLogList,
                                                  CCWSGenericResponse.GenericServiceResponseMessageClass GenericServiceResponseMessage,
                                                  SCInterfaceLog__c responseInterfaceLog)
    {
        debug('referenceList: ' + referenceList);
        String interfaceName = 'SAP_MATERIAL_RESERVATION_CREATE';
        String interfaceHandler = 'CCWCMaterialReservationCreateResponse';
        String type = 'INBOUND';
        String direction = 'inbound';
        ID referenceID = null;
        String refType = null;
        ID referenceID2 = null;
        String refType2 = '';
        ID responseID = null;
        String resultCode = 'E000';
        String resultInfo = 'Success'; 
        
        String jsonInput = JSON.serialize(GenericServiceResponseMessage);
        SCInterfaceBase ib = new SCInterfaceBase();
        String fromJSONMap = ib.getDataFromJSON(jsonInput);
        debug('from json: ' + fromJSONMap);
        
        
        String data = 'headExternalID: ' + headExternalID + ',\n\nreferenceList: ' + referenceList + ',\n\nMaximumLogItemSeverityCode: ' + MaximumLogItemSeverityCode
                    + '\n allResponse: ' + fromJSONMap;
        debug('data: ' + data);            
        // Fill interface log response created by a pivot web service
        responseInterfaceLog.Interface__c = interfaceName;
        responseInterfaceLog.InterfaceHandler__c = interfaceHandler;
        responseInterfaceLog.Direction__c = direction;            
        responseInterfaceLog.MessageID__c = messageID;            
        responseInterfaceLog.ReferenceID__c = referenceID;            
        responseInterfaceLog.ResultCode__c = resultCode;            
        responseInterfaceLog.ResultInfo__c = resultInfo;
        responseInterfaceLog.Data__c = data;
        responseInterfaceLog.Data__c = responseInterfaceLog.Data__c.left(32000);

        String step = '';
        Savepoint sp = Database.setSavepoint();
        try
        {
            step = 'find the movements'; 
            List<SCMaterialMovement__c> mml = readReservations(referenceList,responseInterfaceLog ); 
            debug('mml: ' + mml);
            
            // find the interfacelog
            step = 'find a request interface';
            List<SCInterfaceLog__c> requestInterfaceLogList = CCWSGenericResponse.readOutoingInterfaceLogs(requestMessageID, null, null, 'SAP_MATERIAL_RESERVATION_CREATE');
            //GMSGB 02.09.13 It is possible that the SAP response is faster than the commit of the call
            // Thus it is possible that the interface does not exist.
            debug('requestInterfaceLogList: ' + requestInterfaceLogList); 

            // update material movements
            List<String> listIDType = new List<String>();
            listIDType.add('MaterialReservation');
            listIDType.add('IDoc number');
            listIDType.add('Number of Reservation/Dependent Requirement');
            
            List<ID> orderIDList = new List<ID>();
            Map<ID, String> mapOrderIdToStatus = new Map<ID, String>();
            for(SCMaterialMovement__c mr: mml)
            {
                CCWSGenericResponse.ReferenceItem referenceItem = CCWSGenericResponse.getReferenceItem(referenceList, listIDType, mr.Name);
                debug('reference item loop:' + referenceItem);
                if(referenceItem != null)
                {
                    CCWSGenericResponse.LogItemClass[] loopLogItemArr = CCWSGenericResponse.getLogItems(mr.Name, logItemArr);
                    debug('loop log Item Arr: ' + loopLogItemArr);
                    mr.Status__c = '5403';
                    mr.ERPStatus__c = CCWSGenericResponse.getResultStatus(loopLogItemArr);
                    mr.ERPResultInfo__c = CCWSGenericResponse.getResultInfo(messageID, headExternalID, referenceItem, 
                                          MaximumLogItemSeverityCode, loopLogItemArr, 'HeadMaterialMovement');
                    mr.ERPResultDate__c = DateTime.now();
                    // write response interface log
                    responseInterfaceLog.Count__c++;
                    
                    if(mr.Order__c != null)
                    {
                        orderIDList.add(mr.Order__c);
                        String prevStatus = mapOrderIdToStatus.get(mr.Order__c);
                        if(prevStatus == null || prevStatus != null && prevStatus != 'error')
                        {
                            mapOrderIdToStatus.put(mr.Order__c, mr.ERPStatus__c);
                        }   
                    }
                }    
            }    
            update mml; 
            debug('mml after update: ' + mml);          

            step = 'update orders';
            updateOrders(orderIDList, mapOrderIdToStatus);
            
            step = 'write a response interface log';

            debug('responseInterfaceLog: ' + responseInterfaceLog);

            // update request interface log
            step = 'update the request interface log';
            
            if(responseInterfaceLog != null)
            {
                for(SCInterfaceLog__c requestInterfaceLog: requestInterfaceLogList)
                {
                    responseInterfaceLog.order__c = requestInterfaceLog.order__c;
                    requestInterfaceLog.Response__c = responseInterfaceLog.Id;
                    requestInterfaceLog.Idoc__c = responseInterfaceLog.Idoc__c;
                    interfaceLogList.add(requestInterfaceLog);
                }   
            }
            else
            {
                throw new SCfwException('A response interface log object could not be crated for messageID: ' + messageID); 
            }
        }
        
        catch(SCfwInterfaceRequestPendingException errorRequestNotPending)
        {
        	Database.rollback(sp);
        	throw errorRequestNotPending;
        }
        catch(SCfwException e) 
        {
            Database.rollback(sp);
            throw e;
        }  
        catch(Exception e) 
        {
            Database.rollback(sp);
            throw e;
        } 
		
    }//processCreateOrderResponse

    public static List<SCMaterialMovement__c> readReservations(List<CCWSGenericResponse.ReferenceItem> referenceList,
    															SCInterfaceLog__c responseInterfaceLog)
    {
        List<String> mmNameList = new List<String>();
        for(CCWSGenericResponse.ReferenceItem ri: referenceList)
        {
            if(ri.externalID != null)
            {
                mmNameList.add(ri.externalID);
            }   
        }
        List<SCMaterialMovement__c> items = [select ID, Name, ERPStatus__c, Order__c, Order__r.ID from SCMaterialMovement__c where name in : mmNameList ];
                           // and (ERPStatus__c = 'error' or ERPStatus__c = 'pending' or ERPStatus__c = 'none')];
        List<SCMaterialMovement__c> retValue = new List<SCMaterialMovement__c>();
              
        

        if(items.size() == 0)
        {      	
            throw new SCfwException('There no material reservations for names: ' + mmNameList);
        }

        if(items.size() > 0)
        {            
            debug('read movements: ' + items);
        }
        
        //Test
        //SCfwException er = new SCfwException('Test SCfwException');
        //throw er;

        for (SCMaterialMovement__c mm : items)
        {
        	// Test
        	//SCfwInterfaceRequestPendingException er = new SCfwInterfaceRequestPendingException('The  request status is still \'none\'!');
        	//er.materialMovement = mm;
        	//er.order 			= mm.Order__r;
        	//throw er;
        
        	
        	if(mm.ERPStatus__c == 'error' || mm.ERPStatus__c == 'pending')
        	{
        		retValue.add(mm);
        	}
        	// test if any status is none
        	else if(mm.ERPStatus__c == 'none')
        	{
        		// there are uncommitted MaterialMovements for the SAP request
        		// abort the current computation and add it to the InterfraceQueue
        		SCfwInterfaceRequestPendingException e = new SCfwInterfaceRequestPendingException('The  request status is still \'none\'!');
        		e.materialMovement = mm;
        		e.order = mm.Order__r;
        		throw e;
        	}
        	
        	// test if any status is none
        	else if(mm.ERPStatus__c == 'ok')
        	{
       			//SCfwInterfaceRequestPendingException e = new SCfwInterfaceRequestPendingException('The  material movements for names: ' + mm.Name + 'is already confirmed by SAP!');
        		//e.materialMovement = mm;
        		//e.order = mm.Order__r;
        		//throw e;
        		debug('The  material movements for names: ' + mm.Name + 'is already confirmed by SAP!');
        		retValue.add(mm);
        	}
        	else
        	{
        		SCfwInterfaceRequestPendingException e = new SCfwInterfaceRequestPendingException('The  material movements for names: ' + mm.Name + 'is has ERPStatus__c ' 
        			+ mm.ERPStatus__c);
        		e.materialMovement = mm;
        		e.order = mm.Order__r;
        		throw e;	
        	}
        }
     
        
        return retValue;
    }

    public static void updateOrders(List<ID> orderIDList, Map<ID, String> mapIDToStatus)
    {
        List<SCOrder__c> orderList = [Select ID from SCOrder__c where id in :orderIDList];
        if(orderList != null && orderList.size() > 0)
        {
            for(SCOrder__c o: orderList)
            {
                String status = mapIdToStatus.get(o.ID);
                if(status != null)
                {
                    o.ERPStatusMaterialReservation__c = status;
                    o.ERPResultDate__c = DateTime.now();
                }       
            }
        }
        update orderList;
    }
    

    public static void debug(String msg)
    {
        System.debug('###...' +  msg);        
    }

   
}
