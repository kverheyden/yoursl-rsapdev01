/*
 * @(#)SCbtcContractItemRepair .cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Repairs the field MaintenanceLastDate__c of SCContractItem__c objects beloning
 * to the active Contracts having active templates.
 * The value of the SCOrder__c.Closed__c of the last completed contract visit is set
 * into the field MaintenanceLastDate__c
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
global class SCbtcContractItemRepair extends SCbtcBase
       implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts
{
    private static Integer batchSize = 90; // one statement in the deleteSuperfluous
                                    // two statements in execute
    private ID batchprocessid = null;
    private String mode = 'productive';
    String country = 'DE';

    Integer max_cycles = 0;    
    List<String> visitIdList = null;    // visitIdList = null, normal batch call
                                        // visitIdList != null, orders are repaird only for the list
                                        // of visits    

    // Object for application settings
    private static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();

    private Id contractId = null;

    /**
     * Entry point for starting the apex batch repairing all contracts 
     * @param mode could be 'trace', 'test' or 'productive'
     */
    public static ID repairAll(String country, Integer max_cycles, String mode)
    {
        String department =  null;
        SCbtcContractItemRepair btc = new SCbtcContractItemRepair(country, max_cycles, mode);
        btc.batchprocessid = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    } // repairAll


    /**
     * Entry point for starting the apex batch dispatching an order having the 
     * SCContractVisit__c.Status__c = 'repair' and SCContractVisit__c.Order__c = :orderId
     * @param contractId
     * @param mode could be 'test' or 'productive'
     */
    public static ID repairContract(Id contractId, String country, Integer max_cycles, String mode)
    {
        SCbtcContractItemRepair btc = new SCbtcContractItemRepair(contractId, country, max_cycles, mode);
        btc.batchprocessid = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    } // repairContract

    /**
     * Entry point for starting the apex batch dispatching an order having the 
     * SCContractVisit__c.Status__c = 'repair' and SCContractVisit__c.Order__c.Name = :orderName
     * @param orderName
     * @param mode could be 'test' or 'productive'
     */
    public static ID repairContract(String contractName, String country, Integer max_cycles, String mode)
    {
        Id contractId = null;
        System.debug('###mode: ' + mode);
        System.debug('###contractName: ' + contractName);
        if(contractName != null)
        {
            List<SCContract__c> contractList = [Select Id from SCContract__c where Name = :contractName];
            if(contractList.size() > 0)
            {
                contractId = contractList[0].Id;
                System.debug('###contractId: ' + contractId);
                SCbtcContractItemRepair btc = new SCbtcContractItemRepair(contractId, country, max_cycles, mode);
                btc.batchprocessid = btc.executeBatch(batchSize);
                return btc.batchprocessid;
            }
        }
        return null;
    } // repairContract


    /**
     * Constructor
     * @param mode could be 'test' or 'productive'
     */
    public SCbtcContractItemRepair(String country, Integer max_cycles, String mode)
    {
        this(null, country, max_cycles, mode);
    }


    /**
     * Constructor
     * @param contractID
     * @param mode could be 'test' or 'productive'
     */
    public SCbtcContractItemRepair(Id contractId, String country, Integer max_cycles, String mode)
    {
        this.contractId = contractId;
        this.country = country;
        this.max_cycles = max_cycles;
        this.mode = mode;
    }

   /*
    * Called by the framework when the batch job is started. We construct the soql statement
    * for reading the resource assignments.
    * @param BC the batch context
    * @return the query locator with the selected material movements (up to 50 mio records possible)
    */
    global override Database.QueryLocator start(Database.BatchableContext BC)
    {
        debug('start');
        /* To check the syntax of SOQL statement */
        List<SCContract__c> visitList = [select ID
                                            from SCContract__c 
                                            where  DepartmentCurrent__c = null
                                            limit 1];
        
        debug('start: after checking statement');
        /**/
        String query = null;
        if(country == null || country.trim() == '')
        {
            country = 'XX';
        }
        debug('country: ' + country);
        if(contractId == null)
        {
            query = ' select ID, MaintenanceFirstDate__c, MaintenanceLastDate__c, MaintenanceCreationShiftFactor__c '
                  + ' from SCContract__c where Country__c = \'' + country + '\'';

           query += ' and (status__c = \'Active\' or status__c = \'Created\')'
                    + ' and Template__r.Status__c = \'Active\' ';
            if(max_cycles > 0)
            {
                query += ' limit ' + max_cycles;
            }
        }
        else
        {
            query = ' select ID, MaintenanceFirstDate__c, MaintenanceLastDate__c, MaintenanceCreationShiftFactor__c '
                  + ' from SCContract__c where ID = \'' + contractId + '\' '  ;
           query += ' and (status__c = \'Active\' or status__c = \'Created\')'
                    + ' and Template__r.Status__c = \'Active\' ';
            if(max_cycles > 0)
            {
                query += ' limit ' + max_cycles;
            }
        }
        debug('query: ' + query);
        return Database.getQueryLocator(query);
    } // start
    
   /*
    * Called for each batch of records to process.
    * @param BC the batch context
    * @param scope the list records to be processed
    */
    global override void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        debug('execute');
        List<SCInterfaceLog__c> interfaceLogList = new List<SCInterfaceLog__c>();
        List<SCContractItem__c> contractItemList = new List<SCContractItem__c>();
        for(sObject res: scope)
        {
            debug('sObject: ' + res);
            SCContract__c c = (SCContract__c)res;
            repairContract(c, contractItemList, interfaceLogList);
        }
        upsert contractItemList;
        insert interfaceLogList;
    } // execute

   /*
    * Called by the framework when the batch job has been completed. 
    * We send an e-mail notification about the status
    * @param BC the batch context
    */
    global override void finish(Database.BatchableContext BC)
    {
    } // finish

    /**
     * Dispatches an order SCContractVisit__c.Order__c
     */
    public void repairContract(SCContract__c c, 
                            List<SCContractItem__c> contractItemList, 
                            List<SCInterfaceLog__c> interfaceLogList)
    {
        debug('repairContract');
        debug('contract: ' + c);
        debug('contractID: ' + c.Id);
        
        DateTime start = Datetime.now();
        Boolean thrownException = false;
        String resultInfo = '';
        Integer count = 0;
        String step = '';
        Boolean error = false;
        Boolean repaired = false;
        SCReturnStruct returnStruct = new SCReturnStruct();
        try
        {
            // read visits for the contract sorted with the due date having Status__c = 'completed'
            // then find its visit items and their contract items.
            // Set the last maintenance date
            step = 'set last maintenance date';
            repaired = setLastMaintenace(c, contractItemList, interfaceLogList, returnStruct);
        }
        catch(Exception e)
        {
            thrownException = true;
            resultInfo = 'step: ' + step + ', exception: ' + e.getMessage();
            String prevMode = mode;
            mode = 'test';
            debug('resultInfo:' + resultInfo);
            mode = prevMode;
        }
        finally
        {
            /**
            E000 Success
            E001 Success with Info
            E100 Service not reachable
            E101 Service processing failed
            */
            String resultCode = 'E000';
            if(thrownException || error)
            {
                resultCode = 'E101';
            }
            if(repaired)
            {
                if(returnStruct.message != null)
                {
                    resultInfo = returnStruct.message + ' ' + resultInfo;
                }
                logBatchInternal(interfaceLogList, 'CONTRACT_ITEM_REPAIR', 'SCbtcContractItemRepair',
                                c.Id, null, resultCode, 
                                resultInfo, null, start, count); 
            }                    
         }
    }// repairContract

    /**
     * 
     * Sets the last order close date into the contract item's last maintenance date.
     * The contract items are found in the following way:
     * First are found the last contract visit that is completed and Order is not empty.
     * Then for this visit are found the contract items hanging on the contract visit items
     * of the previously found contract visit.
     * Additionaly the next maintenance date is calculated.
     * The last maintenance date is set if it is less than the close date.
     *
     * @return true if set, else false
     */
    public Boolean setLastMaintenace(SCContract__c c, 
                            List<SCContractItem__c> contractItemList, 
                            List<SCInterfaceLog__c> interfaceLogList,
                            SCReturnStruct returnStruct)
    {
        Boolean ret = false;
        List<SCContractVisit__c> visitList = [Select Status__c, StartDate__c, Order__r.Closed__c 
                                    from SCContractVisit__c 
                                    where Contract__c = :c.Id 
                                    //and Contract__r.MaintenanceCreationShiftFactor__c <> '0'
                                    and Order__c <> null and Status__c = 'completed' 
                                    order by DueDate__c desc];
        debug('visitList: ' + visitList);
        if(visitList.size() > 0)
        {
            List<SCContractItem__c> ciList = [Select Id, MaintenanceInterval__c, MaintenanceLastDate__c, MaintenanceNextDate__c 
                                             from SCContractItem__c where Contract__c = :c.Id
                                             and id in (Select ContractItem__c from SCContractVisitItem__c where ContractVisit__c = :visitList[0].Id)];
            debug('ciList: ' + ciList);
            for(SCContractItem__c ci: ciList)
            {   
                if(visitList[0].Order__r.Closed__c != null)
                {                      
                    debug('oder closed: ' + visitList[0].Order__r.Closed__c.date());
                    debug('1 ci.MaintenanceLastDate__c: ' + ci.MaintenanceLastDate__c);
                    
                    if(ci.MaintenanceLastDate__c == null
                    || visitList[0].Order__r.Closed__c.date() > ci.MaintenanceLastDate__c)
                    {
                        String strMaintenanceDate = 'null';
                        if(ci.MaintenanceLastDate__c != null)
                        {
                            strMaintenanceDate = ci.MaintenanceLastDate__c.format();
                        }
                        returnStruct.message = 'Last Maintenance Date - previous: ' + strMaintenanceDate
                        + ' new: ' + visitList[0].Order__r.Closed__c.date().format();
                        debug('2 ci.MaintenanceLastDate__c: ' + ci.MaintenanceLastDate__c);
                        ci.MaintenanceLastDate__c = visitList[0].Order__r.Closed__c.date();
                        ret = true;
                        contractItemList.add(ci);
                    }    
                }
            }         
        } // for
        return ret;
    }

    public static void logBatchInternal(List<SCInterfaceLog__c> interfaceLogList, String interfaceName, String interfaceHandler,
                                ID referenceID, ID referenceID2, String resultCode, 
                                String resultInfo, String data, Datetime start, Integer count)
    {
        SCInterfaceLog__c ic = new SCInterfaceLog__c();
        interfaceLogList.add(ic);
        ic.Interface__c = interfaceName;
        ic.InterfaceHandler__c = interfaceHandler;
        ic.Type__c = 'BATCH';
        ic.Count__c = count;
        ic.ReferenceID__c = referenceID;
        ic.ReferenceID2__c = referenceID2;
        ic.ResultCode__c = '';
        ic.ResultCode__c = resultCode;
        ic.ResultInfo__c = resultInfo;
        ic.Data__c = data;
        ic.Direction__c = 'internal';
        ic.Start__c = start;
        ic.Stop__c = Datetime.now();
        Long duration = ic.Stop__c.getTime() - ic.Start__c.getTime();
        ic.Duration__c = duration; 
        if(interfaceName == 'CONTRACT_VISIT'
            || interfaceName == 'CONTRACT_ORDER'
            || interfaceName == 'CONTRACT_VALIDATE'
            || interfaceName == 'CONTRACT_ITEM_REPAIR')
        {
            ic.Contract__c = referenceID;
        }
    }       

    private void debug(String text)
    {
        if(mode.equalsIgnoreCase('test')
           || mode.equalsIgnoreCase('trace'))
        {
            System.debug('###...................' + text);
        }
    }

}
