/**
 * test class for cc_cceag_dao_Account
 * initial author: thomas richter
 *  - but only my method will be tested ...
 */
@isTest
public class cc_cceag_dao_test_Account 
{
    private static CCAccountRole__c createWERole (Account soldTo, Account shipTo)
    {
        CCAccountRole__c roleWE = new CCAccountRole__c();
        roleWE.AccountRole__c = 'WE';
        roleWE.MasterAccount__c = soldTo.Id;
        roleWE.SlaveAccount__c = shipTo.Id;
        
        insert roleWE;
        
        return roleWE;
    }
    
    
    static testMethod void testIsWEAccount()
    {
        Account soldTo = new Account();
        soldTo.Name = 'SoldTo';
        insert soldTo;
        
        Account shipTo = new Account();
        shipTo.Name = 'Name';
        insert shipTo;
        
        // no role also means not to be an WE!
        System.assertEquals(false, cc_cceag_dao_Account.isWEAccount(shipTo.Id));
        
        // add WE role
        createWERole(soldTo, shipTo);
        
        System.assertEquals(true, cc_cceag_dao_Account.isWEAccount(shipTo.Id));
        System.assertEquals(false, cc_cceag_dao_Account.isWEAccount(soldTo.Id));
    }
    
    
    
    static testMethod void testIsAGAccount()
    {
		Account soldTo = new Account();
        soldTo.Name = 'SoldTo';
        insert soldTo;

        // no role also means not to be an AG!
        System.assertEquals(false, cc_cceag_dao_Account.isAGAccount(soldTo.Id));
        
        // add AG role
        CCAccountRole__c roleAG = new CCAccountRole__c();
        roleAG.AccountRole__c = 'AG';
        roleAG.SlaveAccount__c = soldTo.Id;
        roleAG.MasterAccount__c = soldTo.Id;
        insert roleAG;
        
        System.assertEquals(true, cc_cceag_dao_Account.isAGAccount(soldTo.Id));
    }
    
    static Account createSoldTo()
    {
        Account soldTo = new Account(Name='SoldTo');
        insert soldTo;
        CCAccountRole__c role = new CCAccountRole__c(AccountRole__c='AG',MasterAccount__c=soldTo.Id, SlaveAccount__c=soldTo.Id);
        insert role;
        return soldTo;
    }
    
    static Account createShipTo(Account soldTo)
    {
        Account shipTo = new Account(Name='ShipTo');
        insert shipTo;
        
        CCAccountRole__c roleWE = new CCAccountRole__c(AccountRole__c='WE', SlaveAccount__c=shipTo.Id, MasterAccount__c=soldTO.Id);
        insert roleWE;

		return shipTo;
    } 
    
    static Account createRE(Account soldTo)
    {
        Account re = new Account(Name='RE');
        insert re;
        
        CCAccountRole__c role = new CCAccountRole__c(AccountRole__c='RE', SlaveAccount__c=re.Id, MasterAccount__c=soldTo.Id);
        insert role;

		return re;
    } 
    
    static Account createRG(Account soldTo)
    {
        Account rg = new Account(Name='RG');
        insert rg;
        
        CCAccountRole__c role = new CCAccountRole__c(AccountRole__c='RG', SlaveAccount__c=rg.Id, MasterAccount__c=soldTo.Id);
        insert role;

		return rg;
    }     
    
	static testMethod void testFetchSoldToAccountForSoldTo()
    {
        Account soldTo = createSoldTo();
        
        Account resultAccount = cc_cceag_dao_Account.fetchSoldToAccount(soldTo);
        System.assertEquals(soldTo.Id, resultAccount.Id);
    }

	static testMethod void testFetchSoldToAccountForShipTo()
    {
        Account soldTo = createSoldTo();
        Account shipTo = createShipTo(soldTo);
        
        Account resultAccount = cc_cceag_dao_Account.fetchSoldToAccount(shipTo);
        System.assertEquals(soldTo.Id, resultAccount.Id);
    }
    
    static testMethod void testFetchSoldToAccountForAccountWORole()
    {
        Account account = new Account(Name='Account');
        insert account;
        Account r = cc_cceag_dao_Account.fetchSoldToAccount(account);
        System.assertEquals(null, r);
    }
    
    public static testMethod void testFetchSoldToAccountForWEAccount()
    {    
        Account soldTo = createSoldTo();
        Account shipTo = createShipTo(soldTO);
        
        createWERole(soldTo, shipTo);
        
        Account account = cc_cceag_dao_Account.fetchSoldToAccount(soldTo);
        
        System.assertEquals(soldTo.Id, account.Id);
    }
    
    static testMethod void testFetchInvoiceRecipientAccountForRE()
    {
        Account soldTo = createSoldTo();
        Account shipTo = createShipTo(soldTo);
        Account re = createRE(soldTo);
        
        // shipto and soldto should result in same RE
        Test.startTest();
        Account r1 = cc_cceag_dao_Account.fetchInvoiceRecipientAccount(soldTo);
        System.assertEquals(re.Id, r1!=null ? r1.Id : null);
                
        Account r2 = cc_cceag_dao_Account.fetchInvoiceRecipientAccount(shipTo);
        System.assertEquals(re.Id, r2!=null ? r2.Id : null);
        Test.stopTest();
    }
    
    
    static testMethod void testFetchInvoiceRecipientAccountForRG()
    {
        Account soldTo = createSoldTo();
        Account shipTo = createShipTo(soldTo);
        Account rg = createRG(soldTo);
        
        // shipto and soldto should result in same RG
        Test.startTest();
        Account r1 = cc_cceag_dao_Account.fetchInvoiceRecipientAccount(soldTo);
        System.assertEquals(rg.Id, r1!=null ? r1.Id : null);
                
        Account r2 = cc_cceag_dao_Account.fetchInvoiceRecipientAccount(shipTo);
        System.assertEquals(rg.Id, r2!=null ? r2.Id : null);
        Test.stopTest();
    }

    
    static testMethod void testFetchInvoiceRecipientAccountForRGAndRE()
    {
        Account soldTo = createSoldTo();
        Account shipTo = createShipTo(soldTo);
        Account re = createRE(soldTo);
        Account rg = createRG(soldTo);
        
        // shipto and soldto should result in same RE!!! (because RE has higher priority than RG!)
        Test.startTest();
        Account r1 = cc_cceag_dao_Account.fetchInvoiceRecipientAccount(soldTo);
        System.assertEquals(re.Id, r1!=null ? r1.Id : null);
                
        Account r2 = cc_cceag_dao_Account.fetchInvoiceRecipientAccount(shipTo);
        System.assertEquals(re.Id, r2!=null ? r2.Id : null);
        Test.stopTest();
    }
	
    
    static testMethod void testFetchInvoiceRecipientAccountWithoutREAndRG()
    {
        Account soldTo = createSoldTo();   
        Account shipTo = createShipTo(soldTo);
        
        Test.startTest();
        Account r1 = cc_cceag_dao_Account.fetchInvoiceRecipientAccount(soldTo);
        System.assertEquals(null, r1);
        
        Account r2 = cc_cceag_dao_Account.fetchInvoiceRecipientAccount(shipTo);
        System.assertEquals(null, r2);
        Test.stopTest();
    }
    
    static testMethod void testFetchInvoiceRecipientAccountWithoutSoldTo()
    {
        Account account = new Account(Name='Account');
        insert account;
        
        Account r = cc_cceag_dao_Account.fetchInvoiceRecipientAccount(account);
        System.assertEquals(null, r);
    }

    static testMethod void testEnsureFieldsForAccountSingleField()
    {
        Account a = new Account(Name='AccountName',AccountNumber='XXX');
        insert a;
        
        Account accountToTest = [Select Id FROM Account WHERE Id=:a.Id];
                              
        Test.startTest();
        cc_cceag_dao_Account.ensureFieldsForAccount(accountToTest, 'AccountNumber');
        Test.stopTest();
        System.assertEquals('XXX', accountToTest.AccountNumber);        
    }
    
    static testMethod void testEnsureFieldsForAccountMultipleFields()
    {
        Account a = new Account(Name='AccountName',AccountNumber='XXX',Name2__c='ZZZ');
        insert a;
        
        Account accountToTest = [Select Id FROM Account WHERE Id=:a.Id];
                              
        Test.startTest();
        cc_cceag_dao_Account.ensureFieldsForAccount(accountToTest, 'AccountNumber,Name2__c');
        Test.stopTest();
        System.assertEquals('XXX', accountToTest.AccountNumber);        
        System.assertEquals('ZZZ', accountToTest.Name2__c);
    }
    
    static testMethod void testfetchUserContactId()
    {
     	Id cId = cc_cceag_dao_Account.fetchUserContactId('1234');
     	system.assert(cId == null);
 		
 		User u = null;
 		Profile p = [SELECT Id FROM Profile WHERE Name='CloudCraze Customer Community User'];
     	List<User> guests = [SELECT FirstName, LastName, ContactId, AccountId, Id FROM User WHERE ProfileId =: p.Id  and isActive=true];
     	if(guests != null && guests.size() > 0) {
      		u = guests[0];
     	}
 		
     	String ids = String.valueOf(u.Id);
     	cId = cc_cceag_dao_Account.fetchUserContactId(idS);
     	system.assert(u.ContactId == cId);
    }
    
    static testMethod void testfetchAccountId()
    {
     	Id aId = cc_cceag_dao_Account.fetchAccountId('1234');
     	system.assert(aId == null);
     	
     	Account acc = new Account();
     	acc.Name = 'daoTestAccount';
     	acc.AccountNumber = '123456';
     	insert acc;
     	
     	Account a = [SELECT Id, Name, AccountNumber FROM Account WHERE Name='daoTestAccount'];
     	
     	Contact cont = new Contact();
     	cont.FirstName = 'Tester';
     	cont.LastName = 'daoTester';
     	cont.AccountId = a.Id;
     	
     	insert cont;
     	
     	Contact c = [SELECT Id, Name, AccountId FROM Contact WHERE FirstName='Tester' and LastName='daoTester'];
 		
 		Profile p = [SELECT Id FROM Profile WHERE Name='CloudCraze Customer Community User'];
 		User u = new User(alias = 'daoT', email='dao@TestUser.com', emailencodingkey='UTF-8', ProfileId = p.Id,
 							lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', ContactId = c.Id,
 							timezonesidkey='America/Los_Angeles', username='dao@TestUser.com');
     	insert u;
     	
     	User use = [SELECT Id, ContactId, AccountId, Name FROM User WHERE Email='dao@TestUser.com'];
     	
 		
     	String ids = String.valueOf(use.Id);
     	aId = cc_cceag_dao_Account.fetchAccountId(idS);
     	system.assert(a.Id == aId);
    }
    
    static testMethod void testfetchPlant()
    {
 		SCPlant__c scp = new SCPlant__c();
 		scp.Name__c = 'testPlant';
		scp.Street__c = 'testPlantStreet';
		scp.ZipCode__c = '12345';
		scp.City__c = 'testPlantCity';
		insert scp;
		
		SCPlant__c p = [SELECT Name__c, Name FROM SCPlant__c WHERE Name__c='testPlant'];
		SCPlant__c cId = cc_cceag_dao_Account.fetchPlant(p.Name);
		
		System.assert(p.Name__c == cID.Name__c); 		
    }
    
    static testMethod void testfetchSCAccountInfo()
    {
    	SCAccountInfo__c aId = cc_cceag_dao_Account.fetchSCAccountInfo('1234');
     	system.assert(aId == null);
    	
    	Account acc = new Account();
     	acc.Name = 'daoTestAccount';
     	acc.AccountNumber = '123456';
     	insert acc;
     	
     	Account a = [SELECT Id, Name, AccountNumber FROM Account WHERE Name='daoTestAccount'];
     	
 		SCAccountInfo__c scp = new SCAccountInfo__c();
 		scp.Account__c = a.Id;
		scp.DistributionChannel__c = 'Z1';
		scp.CustomerType__c = '15';
		insert scp;
				
		SCAccountInfo__c cId = cc_cceag_dao_Account.fetchSCAccountInfo(a.Id);
		
		System.assert(scp.Account__c == cID.Account__c); 		
    }
    
    static testMethod void testensureFieldsForAccount()
    {    	
    	Account acc = new Account();
     	acc.Name = 'daoTestAccount';
     	acc.AccountNumber = '123456';
     	insert acc;
     	
     	Account a = [SELECT Id, Name FROM Account WHERE Name='daoTestAccount'];
     	
		cc_cceag_dao_Account.ensureFieldsForAccount(a, 'AccountNumber');
		
		System.assert(a.AccountNumber == '123456'); 		
    }
    
    static testMethod void testgetPromotedProductsForAccount()
    {    	
        // stc are stored with leading zeros, but sometimes referenced in account without leading zero!
        
    	Account acc = new Account();
     	acc.Name = 'daoTestAccount';
     	acc.AccountNumber = '123456';
     	acc.Subtradechannel__c = '21';
     	insert acc;
     	
     	Account a = [SELECT Id, Name FROM Account WHERE Name='daoTestAccount'];
     	
		List<Map<String,Object>> products = cc_cceag_dao_Account.getPromotedProductsForAccount(a.Id);
		system.assert(products.size() == 0); //Expected to be Zero since there are no Promoted Products for This account
		
		//Create the product
		ccrz__E_Product__c product = new ccrz__E_Product__c();
		product.Name = 'testPromotedProduct';
		product.NumberOfSalesUnitsPerPallet__c = 10;
		product.ccrz__ProductId__c = '1234567890';
		product.ccrz__SKU__c = '1234567890';
		insert product;
		
		Product2 prod2 = new Product2();
		prod2.ID2__c = product.ccrz__ProductId__c;
		prod2.Name = 'TestProd2';
		insert prod2;
		
		//Create the media
		ccrz__E_ProductMedia__c medi = new ccrz__E_ProductMedia__c();
		medi.ccrz__MediaType__c = 'Product Search Image';
		medi.ccrz__ProductMediaSource__c = 'URI';
		medi.ccrz__URI__c = 'noImage';
		medi.ccrz__Product__c = product.Id;
		insert medi;
		
		Subtradechannel__c sTC = new Subtradechannel__c();
		sTC.Subtradechannel__c = '021';
		sTC.Name = '021';
		insert sTC;
		
		SubtradechannelProduct__c sTCP = new SubtradechannelProduct__c();
		sTCP.Product2__c = prod2.Id;
		sTCP.Subtradechannel__c = sTC.Id;
		insert sTCP;
		
		products = cc_cceag_dao_Account.getPromotedProductsForAccount(a.Id);
		system.assert(products.size() == 1);
		
	}
    
}
