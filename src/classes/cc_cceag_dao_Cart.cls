public with sharing class cc_cceag_dao_Cart {
    public cc_cceag_dao_Cart() {
        
    }



    public static ccrz__E_Cart__c retrieveCartByEncryptedId(String encryptedId, Boolean withItems){
        try{
            ccrz__E_Cart__c activeCart = [
                SELECT
                    Id
                FROM
                    ccrz__E_Cart__c
                WHERE
                    ccrz__EncryptedId__c =: encryptedId
                LIMIT 1
            ];
            return (withItems) ? retrieveCartWithItems(activeCart.Id) : retrieveCart(activeCart.Id);
        }
        catch (Exception e) {
            return null;
        }
    }

    public static ccrz__E_Cart__c retrieveCart(String cartId) {
        try { 
            ccrz__E_Cart__c activeCart = [
                SELECT
                    Id,
                    Name,
                    ccrz__Account__c,
                    ccrz__CartStatus__c, 
                    ccrz__CurrencyISOCode__c, 
                    ccrz__ActiveCart__c,
                    ccrz__CartType__c,
                    ccrz__Storefront__c,
                    ccrz__EncryptedId__c,
                    ccrz__ShipTo__c,
                    ccrz__ShipTo__r.ccrz__AddressFirstline__c,
                    ccrz__ShipTo__r.ccrz__City__c,
                    ccrz__ShipTo__r.ccrz__State__c,
                    ccrz__ShipTo__r.ccrz__StateISOCode__c,
                    ccrz__ShipTo__r.ccrz__PostalCode__c,
                    ccrz__ShipTo__r.ccrz__Country__c,
                    ccrz__ShipTo__r.ccrz__CountryISOCode__c,
                    ccrz__ShipTo__r.ccrz__Partner_Id__c,
                    ccrz__ShipTo__r.ccrz__ShippingComments__c,
                    ccrz__ShipTo__r.ccrz__CompanyName__c,
                    ccrz__BillTo__c,
                    ccrz__BillTo__r.ccrz__AddressFirstline__c,
                    ccrz__BillTo__r.ccrz__City__c,
                    ccrz__BillTo__r.ccrz__State__c,
                    ccrz__BillTo__r.ccrz__StateISOCode__c,
                    ccrz__BillTo__r.ccrz__PostalCode__c,
                    ccrz__BillTo__r.ccrz__Country__c,
                    ccrz__BillTo__r.ccrz__CountryISOCode__c,
                    ccrz__BillTo__r.ccrz__Partner_Id__c,
                    ccrz__BillTo__r.ccrz__CompanyName__c,
                    ccrz__RequestDate__c,
                    ccrz__ValidationStatus__c,
                    ccrz__ShipComplete__c,
                    ccrz__ShipMethod__c,
                    ccrz__ShipAmount__c
                FROM
                    ccrz__E_Cart__c
                WHERE
                    Id =: cartId
                LIMIT 1
            ];
            return activeCart;
        }
        catch (Exception e) {
            return null;
        }
    }

    public static ccrz__E_Cart__c retrieveCartWithItems(String cartId) {
        try { 
            ccrz__E_Cart__c activeCart = [
                SELECT
                    Id,
                    Name,
                    ccrz__Name__c,
                    ccrz__Account__c,
                    ccrz__CartStatus__c, 
                    ccrz__CurrencyISOCode__c, 
                    ccrz__ActiveCart__c,
                    ccrz__CartType__c,
                    ccrz__Storefront__c,
                    ccrz__EncryptedId__c,
                    ccrz__ShipTo__c,
                    ccrz__cartID__c,
                    ccrz__SessionID__c,
                    ccrz__ShipTo__r.ccrz__AddressFirstline__c,
                    ccrz__ShipTo__r.ccrz__City__c,
                    ccrz__ShipTo__r.ccrz__State__c,
                    ccrz__ShipTo__r.ccrz__StateISOCode__c,
                    ccrz__ShipTo__r.ccrz__PostalCode__c,
                    ccrz__ShipTo__r.ccrz__Country__c,
                    ccrz__ShipTo__r.ccrz__CountryISOCode__c,
                    ccrz__ShipTo__r.ccrz__Partner_Id__c,
                    ccrz__ShipTo__r.ccrz__ShippingComments__c,
                    ccrz__ShipTo__r.ccrz__CompanyName__c,
                    ccrz__ShipTo__r.ccrz__MailStop__c,
                    ccrz__BillTo__c,
                    ccrz__BillTo__r.ccrz__AddressFirstline__c,
                    ccrz__BillTo__r.ccrz__City__c,
                    ccrz__BillTo__r.ccrz__State__c,
                    ccrz__BillTo__r.ccrz__StateISOCode__c,
                    ccrz__BillTo__r.ccrz__PostalCode__c,
                    ccrz__BillTo__r.ccrz__Country__c,
                    ccrz__BillTo__r.ccrz__CountryISOCode__c,
                    ccrz__BillTo__r.ccrz__Partner_Id__c,
                    ccrz__BillTo__r.ccrz__CompanyName__c,
                    ccrz__RequestDate__c,
                    ccrz__ValidationStatus__c,
                    ccrz__ShipComplete__c,
                    ccrz__ShipMethod__c,
                    ccrz__ShipAmount__c,
                    ccrz__BuyerEmail__c,
                    ccrz__BuyerFirstName__c,
                    ccrz__BuyerLastName__c,
                    ccrz__BuyerPhone__c,
                    (
                        SELECT
                            ccrz__Product__c,
                            ccrz__Product__r.Name,
                            ccrz__Product__r.ccrz__SKU__c,
                            ccrz__cartItemType__c,
                            ccrz__Quantity__c,
                            ccrz__ProductType__c,
                            ccrz__UnitOfMeasure__c,
                            ccrz__StoreId__c
                        FROM
                            ccrz__E_CartItems__r
                    )
                FROM
                    ccrz__E_Cart__c
                WHERE
                    Id =: cartId
                LIMIT 1
            ];
            return activeCart;
        }
        catch (Exception e) {
            return null;
        }
    }

    public static ccrz__E_Cart__c retrieveActiveCart(String userId, String storeName) {
        try { 
            ccrz__E_Cart__c activeCart = [
                SELECT
                    Id,
                    Name,
                    ccrz__Account__c,
                    ccrz__CartStatus__c, 
                    ccrz__CurrencyISOCode__c, 
                    ccrz__ActiveCart__c,
                    ccrz__CartType__c,
                    ccrz__Storefront__c,
                    ccrz__EncryptedId__c,
                    ccrz__ShipTo__c,
                    ccrz__ShipTo__r.ccrz__AddressFirstline__c,
                    ccrz__ShipTo__r.ccrz__City__c,
                    ccrz__ShipTo__r.ccrz__State__c,
                    ccrz__ShipTo__r.ccrz__StateISOCode__c,
                    ccrz__ShipTo__r.ccrz__PostalCode__c,
                    ccrz__ShipTo__r.ccrz__Country__c,
                    ccrz__ShipTo__r.ccrz__CountryISOCode__c,
                    ccrz__ShipTo__r.ccrz__Partner_Id__c,
                    ccrz__ShipTo__r.ccrz__ShippingComments__c,
                    ccrz__ShipTo__r.ccrz__CompanyName__c,
                    ccrz__ShipTo__r.ccrz__MailStop__c,
                    ccrz__BillTo__c,
                    ccrz__BillTo__r.ccrz__AddressFirstline__c,
                    ccrz__BillTo__r.ccrz__City__c,
                    ccrz__BillTo__r.ccrz__State__c,
                    ccrz__BillTo__r.ccrz__StateISOCode__c,
                    ccrz__BillTo__r.ccrz__PostalCode__c,
                    ccrz__BillTo__r.ccrz__Country__c,
                    ccrz__BillTo__r.ccrz__CountryISOCode__c,
                    ccrz__BillTo__r.ccrz__Partner_Id__c,
                    ccrz__BillTo__r.ccrz__CompanyName__c,
                    ccrz__RequestDate__c,
                    ccrz__ValidationStatus__c,
                    ccrz__ShipComplete__c,
                    ccrz__ShipMethod__c,
                    ccrz__ShipAmount__c
                FROM
                    ccrz__E_Cart__c
                WHERE
                    ccrz__CartType__c = 'Cart'
                    AND ccrz__CartStatus__c = 'Open'
                    AND IsDeleted = false
                    AND ccrz__Storefront__c =: storeName
                    AND Ownerid =: userId
                    AND ccrz__ActiveCart__c = true
                LIMIT 1
            ];
            return activeCart;
        }
        catch (Exception e) {
            return null;
        }
    }

    /**
     * Retrieves order data by encrypted id, along with order lines.
     * @param encryptedOrderId
     * @return Order
     */
    public static ccrz__E_Order__c getOrderByEncId(String encryptedOrderId) {
        ccrz__E_Order__c eOrder = null;

        List<ccrz__E_Order__c> orderList =
            [select 
                Id,
                Name,
                ccrz__OrderID__c,
                ccrz__PONumber__c,
                ccrz__Name__c,
                ccrz__Storefront__c,
                ccrz__EncryptedId__c,
                ccrz__OrderNumber__c,
                ccrz__OriginatedCart__c,
                ccrz__OriginatedCart__r.ccrz__EncryptedID__c,
                ccrz__OrderStatus__c,
                Owner.Name,
                ccrz__CurrencyISOCode__c,
                
                ccrz__BuyerEmail__c,
                ccrz__BuyerFirstName__c,
                ccrz__BuyerLastName__c,
                ccrz__BuyerPhone__c,
                ccrz__BuyerCompanyName__c,

                ccrz__TotalSurcharge__c,
                ccrz__ShipAmount__c,
                ccrz__SubTotalAmount__c,
                ccrz__TotalDiscount__c,
                ccrz__TaxAmount__c,
                ccrz__TotalAmount__c,
                ccrz__PaymentMethod__c,
                ccrz__ShipMethod__c,
                ccrz__RequestDate__c,
                ccrz__Note__c,
                CCEAGDeliveryTime__c,
                CCEAGNewsletter__c,
                ccrz__EffectiveAccountID__c,
                ccrz__User__c,
                ccrz__Contact__c,
                ccrz__ContractId__c,
                
                ccrz__ShipTo__c,
                ccrz__ShipTo__r.ccrz__FirstName__c,
                ccrz__ShipTo__r.ccrz__LastName__c,
                ccrz__ShipTo__r.ccrz__Email__c,
                ccrz__ShipTo__r.ccrz__HomePhone__c,
                ccrz__ShipTo__r.ccrz__CompanyName__c,
                
                ccrz__ShipTo__r.ccrz__AddressFirstline__c,
                ccrz__ShipTo__r.ccrz__AddressSecondline__c,
                ccrz__ShipTo__r.ccrz__City__c,
                ccrz__ShipTo__r.ccrz__State__c,
                ccrz__ShipTo__r.ccrz__PostalCode__c,
                ccrz__ShipTo__r.ccrz__Country__c,
                ccrz__ShipTo__r.ccrz__CountryISOCode__c,
                ccrz__ShipTo__r.ccrz__Partner_Id__c,
                
                ccrz__BillTo__c,
                ccrz__BillTo__r.ccrz__FirstName__c,
                ccrz__BillTo__r.ccrz__LastName__c,
                ccrz__BillTo__r.ccrz__Email__c,
                ccrz__BillTo__r.ccrz__HomePhone__c,
                
                ccrz__BillTo__r.ccrz__AddressFirstline__c,
                ccrz__BillTo__r.ccrz__AddressSecondline__c,
                ccrz__BillTo__r.ccrz__City__c,
                ccrz__BillTo__r.ccrz__State__c,
                ccrz__BillTo__r.ccrz__PostalCode__c,
                ccrz__BillTo__r.ccrz__Country__c,
                ccrz__BillTo__r.ccrz__CountryISOCode__c,
                ccrz__BillTo__r.ccrz__Partner_Id__c,
                
                (select Name, ccrz__OrderItemId__c, ccrz__ProductType__c, ccrz__Price__c, ccrz__Quantity__c, ccrz__StoreId__c, 
                    ccrz__SubAmount__c, ccrz__Product__r.name,ccrz__Product__r.ccrz__SKU__c, ccrz__UnitOfMeasure__c 
                 from ccrz__E_OrderItems__r),
                 (select ccrz__title__c, ccrz__description__c from ccrz__OrderTerms__r)

            from ccrz__E_Order__c
            where (ccrz__EncryptedId__c = :encryptedOrderId) OR (Id = :encryptedOrderId)];

        if(orderList.size() > 0) {
            eOrder = orderList[0];
        }
        
        return eOrder;
    }

    /*
     * Retrieves related products for the passed in list of product Ids, filtered by
     * the relation type and standard available statuses.  Limits to 8 products.
     * @param productIdList list of product ids
     * @param relations related product relation types
     * @param limitResults determines whether or not to put a limit on the results
     * @param isSkus indicates that the passed in list is of skus, if false then they are ids
     * @return list of related products
     */
    public static List<ccrz__E_RelatedProduct__c> getRelatedProducts(List<String> dataList, List<String> relations) {
        List<String> includeStatuses = getStandardStatusFilter();
        String query = buildRelatedProductBaseQuery();
        query += ' and ccrz__RelatedProductType__c in :relations ';
        query += ' and ccrz__Product__c in :dataList  ';
        return Database.query(query);
    }

    private static String buildRelatedProductBaseQuery() {
        return 'Select ' +
                        'Id, ' +
                        'ccrz__Enabled__c, ' +
                        'ccrz__EndDate__c, ' +
                        'Name, ' +
                        'ccrz__RelatedProductType__c, ' +
                        'ccrz__RelatedProduct__r.Id, ' +
                        'ccrz__RelatedProduct__r.ccrz__ShortDesc__c, ' +
                        'ccrz__RelatedProduct__r.ccrz__StartDate__c, ' +
                        'ccrz__StartDate__c, ' +
                        'ccrz__Sequence__c, ' +
                        'ccrz__RelatedProduct__c, ' +
                        'ccrz__Product__c, ' +
                        'ccrz__RelatedProduct__r.ccrz__Sku__c, ' +
                        'ccrz__RelatedProduct__r.ccrz__ProductStatus__c, ' +
                        'ccrz__RelatedProduct__r.Name ' +
                    'From ' +
                        'ccrz__E_RelatedProduct__c ' +
                    'Where ' +
                        'ccrz__RelatedProduct__r.ccrz__ProductStatus__c in :includeStatuses ';
    }

    /*
     * Builds a list of statuses used for standard filtering of products.
     * @return list of statuses
     */
    private static List<String> getStandardStatusFilter() {
        List<String> statusCodes = new List<String>();
        statusCodes.add('Released');
        return statusCodes;
    }

    /**
     * Retrieves the order corresponding to the passed in id.  If the passed in where clause
     * is not blank, then append and query appropriately. 
     * @param whereClause
     * @return List of orders
     */
    public static List<ccrz__E_Order__c> getOrders(String whereClause, String sortClause) {
        List<ccrz__E_Order__c> orderList = new List<ccrz__E_Order__c>();
        String query = buildOrderSelectQuery();
        query += 'From ccrz__E_Order__c '   ;
        if (whereClause != null)
            query += whereClause;
        if (sortClause != null)
            query += sortClause;
        orderList = Database.query(query);
        return orderList;
    }

    /**
     * Constructs the select clause for the order query.
     * @return select portion of the query
     */
    public static String buildOrderSelectQuery() {
        String query = 
            'Select ' +
                'Id, ' +
                'Name, ' +
                'ccrz__OrderID__c, ' +
                'ccrz__EncryptedId__c, ' +
                'ccrz__OrderNumber__c, ' +
                'ccrz__OrderDate__c, ' +
                'ccrz__PONumber__c, ' +
                'ccrz__OriginatedCart__c, ' +
                'ccrz__OriginatedCart__r.ccrz__EncryptedID__c, ' +
                'ccrz__CurrencyISOCode__c, ' +
                'ccrz__OrderStatus__c, ' +
                'Owner.Name, ' +
                'ccrz__TotalAmount__c, ' +
                'ccrz__RequestDate__c ';
        return query;
    }

}
