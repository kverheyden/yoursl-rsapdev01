/**********************************************************************

Name: fsFACDEOrderCreate

======================================================

Purpose: 
 
This class creates a new SCOrder, SCOrderItem based on a CDEOrderItem

======================================================

History 

------- 

Date            AUTHOR              DETAIL

06/04/2014      Oliver Preuschl     INITIAL DEVELOPMENT

***********************************************************************/

public with sharing class fsFACDEOrderCreate {
    

    public class CDEOrderCreateException extends Exception{}

    //The maximum number of SCOrders that will be send to SAP in one execution
    private static final Integer MAX_SCORDER_NUMBER = 3;

    //------------------------------------------------------------------------------
    //Non static variables----------------------------------------------------------

    private List< CdeOrderItem__c > GL_NewCDEOrderItems;
    private List< CdeOrderItem__c > GL_SCOrderCreatedCDEOrderItems;

    //------------------------------------------------------------------------------
    //Getter and setter methods-----------------------------------------------------

    //Stores the User Data required for the generation of the orders (SalesCo, ...)
    private static Map< Id, User > GM_UserDetails;

    //Gets the User Data required for the generation of the orders (SalesCo, ...)
    private static User getUserDetails( Id PV_UserId ){
        if( GM_UserDetails == null ){
            GM_UserDetails = new Map< Id, User >();
        }
        if( !GM_UserDetails.containsKey( PV_UserId ) ){
            User LO_User = [ SELECT ERPSalesArea__c, ERPDistributionChannel__c, ERPDivision__c, SalesOffice__c, ERPWorkcenter__c, ERPWorkCenterPlant__c FROM User WHERE ( Id =: PV_UserId ) ];
            GM_UserDetails.put( PV_UserId, LO_User );
        }
        return GM_UserDetails.get( PV_UserId );
    }

    private static String GV_PriceListId2{
        get{
            if( GV_PriceListId2 == null ){
                SalesAppSettings__c LO_PriceListSetting = SalesAppSettings__c.getInstance( 'CDEPriceList' );
                if( LO_PriceListSetting != null ){
                    GV_PriceListId2 = LO_PriceListSetting.Value__c;
                }
            }
            return GV_PriceListId2;
        }
        set;
    }

    //------------------------------------------------------------------------------
    //Controllers-------------------------------------------------------------------

    //Controller, get maximum 5 CDEOrderItems with status 'New' and 
    public fsFACDEOrderCreate( Set< Id > PS_RecordIds, String PV_ObjectName ){
        System.debug( '### - Start fsFACDEOrderCreate' );

        if( PV_ObjectName == 'CDEOrderItem__c' ){
            GL_NewCDEOrderItems = [ SELECT Id, Name, Status__c, SCOrder__c, ProductModel__r.Id2__c, Room__c, Building__c, Floor__c, LongText__c, CDEOrder__r.OwnerId, CDEOrder__c, CDEOrder__r.Account__c, CDEOrder__r.Account__r.AccountNumber, CDEOrder__r.Account__r.Name, CDEOrder__r.RequestedDeliveryDate__c, CDEOrder__r.Phone__c FROM CdeOrderItem__c WHERE ( ( Id IN: PS_RecordIds )AND ( CDEOrder__r.Send_to_SAP__c = true ) AND ( Status__c = 'Processing' )  AND ( CDEOrder__r.Account__c != null ) ) LIMIT :MAX_SCORDER_NUMBER ];
        }else if( PV_ObjectName == 'CDEOrder__c' ){
            GL_NewCDEOrderItems = [ SELECT Id, Name, Status__c, SCOrder__c, ProductModel__r.Id2__c, Room__c, Building__c, Floor__c, LongText__c, CDEOrder__r.OwnerId, CDEOrder__c, CDEOrder__r.Account__c, CDEOrder__r.Account__r.AccountNumber, CDEOrder__r.Account__r.Name, CDEOrder__r.RequestedDeliveryDate__c, CDEOrder__r.Phone__c FROM CdeOrderItem__c WHERE ( ( CDEOrder__r.Id IN: PS_RecordIds )AND ( CDEOrder__r.Send_to_SAP__c = true ) AND ( Status__c = 'Processing' )  AND ( CDEOrder__r.Account__c != null ) ) LIMIT :MAX_SCORDER_NUMBER ];
        }

        System.debug( '### - Stop fsFACDEOrderCreate' );
    }

    //------------------------------------------------------------------------------
    //Methods-----------------------------------------------------------------------

    //Create the clockport oerders and create the orders in SAP afterwards
    public void createOrders(){
        System.debug( '### - Start createOrders' );
		
        if( GL_NewCDEOrderItems != null ){
            if( GL_NewCDEOrderItems.size() > 0 ){
            	// Create CDE Order objects to "reset" the send to SAP flag
            	Map<Id,CDEOrder__c>mapCDEOrders = new Map<Id,CDEOrder__c>();
                //Stores the newly created SCOrders
                List< SCOrder__c > LL_SCOrders = new List< SCOrder__c >();
                //Stores the CDEOrderItems that have been processed and have to be updated later
                List< CdeOrderItem__c > LL_CDEOrderItems = new List< CdeOrderItem__c >();
                //Stores the names of the newly created SCOrders
                Map< String, CdeOrderItem__c > LM_OrderNames_CDEOrderItems = new Map< String, CdeOrderItem__c >();
				system.debug('###Line Items: '+LM_OrderNames_CDEOrderItems);
                //Create the SCOrders one by one
                for( CDEOrderItem__c LO_CDEOrderItem: GL_NewCDEOrderItems ){
                	// Check if the CDE Order is already in our map. if not, add it with the flag SnedToSAP set to false                	
                	if(!mapCDEOrders.containsKey(LO_CDEOrderItem.CDEOrder__c)){
                		CDEOrder__c objectTmpCDEOrder 		= new CDEOrder__c();
                		objectTmpCDEOrder.Id				= LO_CDEOrderItem.CDEOrder__c;
                		objectTmpCDEOrder.Send_to_SAP__c	= false;
                		mapCDEOrders.put(LO_CDEOrderItem.CDEOrder__c,objectTmpCDEOrder);
                	}
                    //Create the SCOrder
                    String LV_OrderName = createOrder( LO_CDEOrderItem.CDEOrder__r, LO_CDEOrderItem, null );
                    system.debug('LV Order Name: '+LV_OrderName);
                    //Was the creation successful?
                    if( LV_OrderName != null ){
                        //Save the name of the SCOrder
                        LM_OrderNames_CDEOrderItems.put( LV_OrderName, LO_CDEOrderItem );
                        //Remove the InstalledBase from the list for this this ProductModel, so it will not be used again
                        //LL_InstalledBases.remove( 0 );
                    }else{
                        //No SCOrder was created
                        LO_CDEOrderItem.Status__c = 'Processing Error';
                        LL_CDEOrderItems.add( LO_CDEOrderItem );
                    }
                }

                //Get the newly created SCOrders
                for( SCOrder__c LO_Order: [ SELECT Id, ERPStatusOrderCreate__c, Name, OwnerId FROM SCOrder__c WHERE ( Name IN: LM_OrderNames_CDEOrderItems.KeySet() ) ] ){
                    CdeOrderItem__c LO_CDEOrderItem = LM_OrderNames_CDEOrderItems.get( LO_Order.Name );

                    //Reset ERP field values to make sure SAP will accept the orders
                    LO_Order.ERPStatusOrderCreate__c = 'none';
                    LO_Order.ERPOrderNo__c = '';
                    LO_Order.Channel__c = '5316';
                    LO_Order.OwnerId = LO_CDEOrderItem.CDEOrder__r.OwnerId;
                    LL_SCOrders.add( LO_Order );

                    //Update the CDEOrderItem Status and Reference to the newly created SCOrder
                    LO_CDEOrderItem.Status__c = 'SCOrder Created';
                    LO_CDEOrderItem.SCOrder__c = LO_Order.Id;
                    LL_CDEOrderItems.add( LO_CDEOrderItem );
                }

                //Update the newly created SCOrder and the updated OrderItems
                update( LL_SCOrders ); 
                update( LL_CDEOrderItems );
                //Start the callout to SAP to create the orders
                List< Id > LL_SCOrderIds = new List< Id >();
                for( SCOrder__c LO_SCOrder: LL_SCOrders ){
                    LL_SCOrderIds.add( LO_SCOrder.Id );
                }
                //fsFASCOrderCreate.sendSCOrdersToSAP( LL_SCOrderIds );
                // Update all CDE Orders with Falg not send
                if(mapCDEOrders.size()>0){
                	update mapCDEOrders.values();

                }
            }
        }

        System.debug( LoggingLevel.Error, '### - Stop createOrders' );
    }

    //Create one ClockPortOrder for a specified CDEOrder and one related CDEOrderItem
    private static String createOrder( CDEOrder__c PO_CDEOrder, CdeOrderItem__c PO_CDEOrderItem, SCInstalledBase__c PO_InstalledBase ){
        System.debug( '### - Start createOrder' );

        //Generate the generic data structure for the ClockPort Order
        CCWSOrderCreateFromExternal.CreateOrderFromExternalMessageClass LO_OrderInputData = createOrderStructure();
        //Populate the ClockPortOrder data structure with data from the CDEOrder
        User LO_UserDetails = getUserDetails( PO_CDEOrder.OwnerId );
        populateOrderData( LO_OrderInputData, PO_CDEOrder, PO_CDEOrderItem, PO_InstalledBase, LO_UserDetails );
        //Call the provided interface to generate the ClockPortOrder
        CCWSOrderCreateFromExternal.GenericServiceResponseMessageClass LO_Response = CCWSOrderCreateFromExternal.transmit( LO_OrderInputData );
        
        //Get the Name of the newly created order
        String LV_OrderName;
        try{
            LV_OrderName = LO_Response.References.References.Reference.get( 0 ).ReferenceId;
        }catch( Exception LO_Exception ){
            LV_OrderName = null;
            //throw new CDEOrderCreateException( 'No SCOrderName was received from CCWSOrderCreateFromExternal.transmit. Response: ' + LO_Response );
        }
        //if( ( LV_OrderName == null ) || ( LV_OrderName == '' ) ){
        //    throw new CDEOrderCreateException( 'No SCOrderName was received from CCWSOrderCreateFromExternal.transmit. Response: ' + LO_Response );
        //}
        
        System.debug( '### - Stop createOrder' );
        return LV_OrderName;
    }

    //Creates the generic data structure for the generation of  ClockPortOrder
    private static CCWSOrderCreateFromExternal.CreateOrderFromExternalMessageClass createOrderStructure(){
        System.debug( '### - Start CreateOrderFromExternalMessageClass' );

        CCWSOrderCreateFromExternal.CreateOrderFromExternalMessageClass LO_OrderInputData = new CCWSOrderCreateFromExternal.CreateOrderFromExternalMessageClass();
        LO_OrderInputData.MessageHeader = new CCWSOrderCreateFromExternal.MessageHeaderClass();
        LO_OrderInputData.PlainFields = new CCWSOrderCreateFromExternal.CreateOrderExternalFieldsClass();
        LO_OrderInputData.SalesData = new CCWSOrderCreateFromExternal.SalesDataClass();
        LO_OrderInputData.Partners = new CCWSOrderCreateFromExternal.PartnerContainer();
        LO_OrderInputData.Partners.Partner = new List<CCWSOrderCreateFromExternal.PartnerClass>();
        LO_OrderInputData.ContactInfo =  new CCWSOrderCreateFromExternal.ContactInfoClass();

        System.debug( '### - Stop CreateOrderFromExternalMessageClass' );
        
        return LO_OrderInputData;
    }

    //Populates a ClockPortOrder data structure with data from a CDEOrder, one related CDEOrderItem and a User record
    private static void populateOrderData( CCWSOrderCreateFromExternal.CreateOrderFromExternalMessageClass PO_OrderInputData, CDEOrder__c PO_CDEOrder, CdeOrderItem__c PO_CDEOrderItem, SCInstalledBase__c PO_InstalledBase, User PO_User ){
        System.debug( '### - Start populateOrderData' );

        PO_OrderInputData.SalesData.SalesArea = PO_User.ERPSalesArea__c;
        PO_OrderInputData.SalesData.DistributionChannel = PO_User.ERPDistributionChannel__c;
        PO_OrderInputData.SalesData.Division = PO_User.ERPDivision__c;
        PO_OrderInputData.SalesData.SalesOffice = PO_User.SalesOffice__c;
        CCWSOrderCreateFromExternal.PartnerClass LO_Partner;
        LO_Partner = new CCWSOrderCreateFromExternal.PartnerClass();
        LO_Partner.PartnerFunction = 'AG';
        //LO_Partner.PartnerNumber = PO_CDEOrder.Account__r.Id2__c;
        LO_Partner.PartnerNumber = PO_CDEOrder.Account__r.AccountNumber;
        PO_OrderInputData.Partners.Partner.add( LO_Partner );
        LO_Partner = new CCWSOrderCreateFromExternal.PartnerClass();
        LO_Partner.PartnerFunction = 'WE';
        //LO_Partner.PartnerNumber = PO_CDEOrder.Account__r.Id2__c;
        LO_Partner.PartnerNumber = PO_CDEOrder.Account__r.AccountNumber;
        PO_OrderInputData.Partners.Partner.add( LO_Partner );

        String LV_SerialNumber;
        if( PO_InstalledBase != null ){
            LV_SerialNumber = PO_InstalledBase.SerialNo__c;
        }else{
            LV_SerialNumber = '';
        }
        String LV_PriceList;
        if( PO_InstalledBase != null ){
            LV_PriceList = PO_InstalledBase.PriceList__r.Id2__c;
        }else{
            LV_PriceList = GV_PriceListId2;
        }
        PO_OrderInputData.PlainFields.OrderID = PO_CDEOrderItem.Name;
        PO_OrderInputData.PlainFields.WorkflowId = ''; //Not required
        PO_OrderInputData.PlainFields.OrderType = 'ZC04'; //Not sure
        PO_OrderInputData.PlainFields.ShortText = ''; //Not required
        PO_OrderInputData.PlainFields.MainWorkCenter = PO_User.ERPWorkcenter__c; //Not sure
        PO_OrderInputData.PlainFields.WorkCenterPlanned = ''; //Not found, TBD
        PO_OrderInputData.PlainFields.Plant = PO_User.ERPWorkCenterPlant__c; //Not required
        PO_OrderInputData.PlainFields.BasicStartDate = PO_CDEOrder.RequestedDeliveryDate__c;
        PO_OrderInputData.PlainFields.SerialNumber = LV_SerialNumber;
        PO_OrderInputData.PlainFields.Assembly = PO_CDEOrderItem.ProductModel__r.Id2__c;
        PO_OrderInputData.PlainFields.LongText = PO_CDEOrderItem.LongText__c; //Not required
        PO_OrderInputData.PlainFields.Pricelist = LV_PriceList; //Not sure

        PO_OrderInputData.ContactInfo.Name1 = PO_CDEOrder.Account__r.Name;
        PO_OrderInputData.ContactInfo.RoomNumber = PO_CDEOrderItem.Room__c;
        PO_OrderInputData.ContactInfo.Building = PO_CDEOrderItem.Building__c;
        PO_OrderInputData.ContactInfo.Floor = PO_CDEOrderItem.Floor__c;
        PO_OrderInputData.ContactInfo.TelephoneNumber = PO_CDEOrder.Phone__c;

        System.debug( '### - Stop populateOrderData' );
    }

    //Controller, get maximum 5 CDEOrderItems with status 'New' and 
    public static Set< Id > allocateCDEOrders( Set< Id > PS_RecordIds, String PV_ObjectName ){
        System.debug( '### - Start allocateCDEOrders' );

        Set< Id > LS_AllocatedCDEOrderItemIds = new Set< Id >();
        List< CdeOrderItem__c > LL_AllocationCDEOrderItems;
        if( PV_ObjectName == 'CDEOrderItem__c' ){
            LL_AllocationCDEOrderItems = [ SELECT Id, Name, Status__c, SCOrder__c, ProductModel__r.Id2__c, Room__c, Building__c, Floor__c, LongText__c, CDEOrder__c, CDEOrder__r.Account__c, CDEOrder__r.Account__r.AccountNumber, CDEOrder__r.Account__r.Name, CDEOrder__r.RequestedDeliveryDate__c, CDEOrder__r.Phone__c FROM CdeOrderItem__c WHERE ( ( Id IN: PS_RecordIds )AND ( CDEOrder__r.Send_to_SAP__c = true ) AND ( Status__c = 'New' )  AND ( CDEOrder__r.Account__c != null ) ) LIMIT 5 ];
        }else if( PV_ObjectName == 'CDEOrder__c' ){
            LL_AllocationCDEOrderItems = [ SELECT Id, Name, Status__c, SCOrder__c, ProductModel__r.Id2__c, Room__c, Building__c, Floor__c, LongText__c, CDEOrder__c, CDEOrder__r.Account__c, CDEOrder__r.Account__r.AccountNumber, CDEOrder__r.Account__r.Name, CDEOrder__r.RequestedDeliveryDate__c, CDEOrder__r.Phone__c FROM CdeOrderItem__c WHERE ( ( CDEOrder__r.Id IN: PS_RecordIds )AND ( CDEOrder__r.Send_to_SAP__c = true ) AND ( Status__c = 'New' )  AND ( CDEOrder__r.Account__c != null ) ) LIMIT 5 ];
        }
        for( CDEOrderItem__c LO_AllocationCDEOrderItem: LL_AllocationCDEOrderItems ){
            LO_AllocationCDEOrderItem.Status__c = 'Processing';
            LS_AllocatedCDEOrderItemIds.add( LO_AllocationCDEOrderItem.Id );
        }
        update( LL_AllocationCDEOrderItems );

        System.debug( '### - Stop allocateCDEOrders' );

        return ( LS_AllocatedCDEOrderItemIds );
    }

    //Allocate the CDEOrderItems for Processing and call the future job
    public static void createSCOrdersFromCDEOrderItems( Set< Id > PS_CDEOrderItemIds ){
        Set< Id > LS_AllocatedCDEOrderItems = allocateCDEOrders( PS_CDEOrderItemIds, 'CDEOrderItem__c' );
        if ( LS_AllocatedCDEOrderItems.size() > 0 ) {
            createSCOrdersFromCDEOrderItemsFuture( LS_AllocatedCDEOrderItems );
        }
    }

    //Allocate the CDEOrderItems for Processing and call the future job
    public static void createSCOrdersFromCDEOrders( Set< Id > PS_CDEOrderItemIds ){
        Set< Id > LS_AllocatedCDEOrderItems = allocateCDEOrders( PS_CDEOrderItemIds, 'CDEOrder__c' );
        if( LS_AllocatedCDEOrderItems.size() > 0 ){
            createSCOrdersFromCDEOrderItemsFuture( LS_AllocatedCDEOrderItems );
        }
    }

    @future(callout=true)
    public static void createSCOrdersFromCDEOrderItemsFuture( Set< Id > PS_CDEOrderItemIds ){
        fsFACDEOrderCreate LO_OrderCreate = new fsFACDEOrderCreate( PS_CDEOrderItemIds, 'CDEOrderItem__c' );
        LO_OrderCreate.createOrders();
    }

}
