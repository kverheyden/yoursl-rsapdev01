/*
 * @(#)SCAddressServiceImplDETest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/** 
 * Tests for the google maps api integration. 
 */
@isTest
private class SCAddressServiceImplDETest
{
    // ensure testcoverage of web service wrapper 
    static testMethod void testCheck()
    {
        SCAddressServiceImplDE addrService = new SCAddressServiceImplDE();
        
        AvsAddress addr = new AvsAddress();
        addr.postalcode = '91207';
        addr.city = 'Lauf';
        addr.street = 'Weigmann';
        addr.housenumber = '';
        addr.country = 'DE';
        addr.matchInfo = 'test';

        String addrUrl = addrService.createAddrUrl(addr);
        AvsResult result = addrService.check(addr);
        
        System.assertEquals(1, result.items.size());
        System.assertEquals('91207', result.items[0].postalcode);
        System.assertEquals('Lauf', result.items[0].city);
        System.assertEquals('Weigmannstraße', result.items[0].street);
        System.assertEquals('Bayern', result.items[0].county);
        System.assertEquals('DE', result.items[0].country);
        System.assertEquals('EXACT', result.status);
    } // testCheck

    static testMethod void testGeocode()
    {
        SCAddressServiceImplDE addrService = new SCAddressServiceImplDE();
        
        AvsAddress addr = new AvsAddress();
        addr.postalcode = '91207';
        addr.city = 'Lauf';
        addr.street = 'Weigmann';
        addr.housenumber = '';
        addr.country = 'DE';
        addr.matchInfo = 'test';

        AvsResult result = addrService.geocode(addr);
        
        System.assertEquals(1, result.items.size());
        System.assertEquals('91207', result.items[0].postalcode);
        System.assertEquals('Lauf', result.items[0].city);
        System.assertEquals('Weigmannstraße', result.items[0].street);
        System.assertEquals('Bayern', result.items[0].county);
        System.assertEquals('DE', result.items[0].country);
        System.assertEquals('EXACT', result.status);
    } // testGeocode

    static testMethod void testSignUrl()
    {
        SCAddressServiceImplDE addrService = new SCAddressServiceImplDE();
        
        String url = 'http://maps.google.com/maps/api/geocode/json?address=New+York&sensor=false';
        String id = 'clientID';
        String key = 'vNIXE0xscrmjlyV-12Nj_BvUPaw=';
        
        String signedUrl = addrService.getSignedUrl(url, id, key);
        System.assertEquals('http://maps.google.com/maps/api/geocode/json?address=New+York&sensor=false&client=clientID&signature=KrU1TzVQM7Ur0i8i7K3huiw3MsA=', 
                            signedUrl);
    } // testSignUrl
}
