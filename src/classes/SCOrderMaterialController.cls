/*
 * @(#)SCOrderMaterialController.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Implements the material management inside the order.
 * It will be use during:
 *  - material request = mode 1
 *  - material status  = mode 2
 *  - material booking = mode 3
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCOrderMaterialController
{
    public static SCfwDomain domMatStat = new SCfwDomain('DOM_MATERIALMOVEMENT_STATUS');

    public Id stockId;
    public Id orderId;
    public List<SelectOption> receiverList { get; set;}
    public List<SelectOption> sourceList { get; set;}
    public Id sourceStock { get; set;}
    public Id receiverStock { get; set;}
    public String matMoveType { get; set;}
    public Boolean assignmentOk { get; private set; }
    public Boolean requestOk { get; private set; }
    public Boolean bookingOk { get; private set; }
    public SCTool__c dates { get; set;}
    public SCStock__c stock { get; set;}
    public SCMaterialMovement__c matMove { get; set;}
    public List<SCMatMoveBaseExtension.RequestArticleInfo> artInfoList { get; set;}
    public Boolean isSelected { get; set;}
    public String errMsg { get; set;}
    public String msgHint { get; set;}
    public List<SCMaterialMovement__c> matMoves { get; set;}
    public Integer sysParamMatFeedBackV4 { get; set;}
    
    public Boolean getValuationTypeActivated() { return valuationTypeActivated; }
    public Boolean getHasValTypeToBeSelected() { return hasValTypeToBeSelected; }
    
    private Integer sysParamStockV2;
    private String mode;
    private SCMatMoveBaseExtension matMoveBase = new SCMatMoveBaseExtension();
    private String replishmentType;
    private Boolean valuationTypeActivated = false;
    private Boolean hasValTypeToBeSelected = false;
    private SCApplicationSettings__c appSettings;
    
    public Boolean getShowPricing()
    {
        if (appSettings == null) {appSettings = SCApplicationSettings__c.getInstance(); }
        if (appSettings.SHOW_PRICING__c == 1) { return true; }
        return false;
    }
    
    public Boolean getShowReplensihmentType()
    {
        if (appSettings == null) {appSettings = SCApplicationSettings__c.getInstance(); }
        if (appSettings.SHOW_REPLENISHMENT_TYPE__c == 1) { return true; }
        return false;
    }
    
    /**
     * Default constructor
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCOrderMaterialController()
    {
        System.debug('#### SCOrderMaterialController (default)');

        mode = ApexPages.currentPage().getParameters().get('m');
        if ((null == mode) || (mode.length() == 0))
        {
            mode = '1';
        } // if ((null == mode) || (mode.length() == 0))
        orderId = ApexPages.currentPage().getParameters().get('oid');
        stockId = ApexPages.currentPage().getParameters().get('sid');
        
        init(mode, orderId, stockId);
    } // SCOrderMaterialController

    /**
     * Constructor for use in order processing.
     *
     * @param    mode       mode: 1 = request, 2 = mat status, 3 = booking
     * @param    oid        id of the order for which the material action should be done
     * @param    sid        id of the stock for which the material action should be done
     * @param    replType   replenishment type which should be used
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCOrderMaterialController(String mode, String oid, String sid, String replType)
    {
        System.debug('#### SCOrderMaterialController');

        this.mode = mode;
        if ((null == this.mode) || (this.mode.length() == 0))
        {
            mode = '3';
        } // if ((null == this.mode) || (this.mode.length() == 0))
        orderId = oid;
        stockId = sid;
        replishmentType = replType;
        
        init(mode, orderId, stockId);
    } // SCOrderMaterialController
    
    /**
     * Initialize the intern variables.
     *
     * @param    mode       mode: 1 = request, 2 = mat status, 3 = booking
     * @param    orderId    id of the order for which the material action should be done
     * @param    stockId    id of the stock for which the material action should be done
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public void init(String mode, String orderId, String stockId)
    {
        System.debug('#### init: mode        -> ' + mode);
        System.debug('#### init: orderId     -> ' + orderId);
        System.debug('#### init: stockId     -> ' + stockId);
        
        try
        {
            stock = [Select Id, Name, Info__c, Plant__c, Plant__r.Name, ValuationType__c
                       from SCStock__c where Id = :stockId];
            
            valuationTypeActivated = stock.ValuationType__c;
            hasValTypeToBeSelected = stock.ValuationType__c;
        }
        catch (Exception e) { }
        
        errMsg = '';
        msgHint = '';
        isSelected = true;

        if (mode.equals('1'))
        {
            initArticleInfos();
            
            dates = new SCTool__c();
            dates.Start__c = Date.today();
            dates.End__c = Date.today();

            matMove = new SCMaterialMovement__c();
            String defReplType;
            if (null != replishmentType)
            {
                defReplType = replishmentType;
            } // if (null != replishmentType)
            else
            {
                if (appSettings == null) {appSettings = SCApplicationSettings__c.getInstance(); }
                defReplType = appSettings.DEFAULT_REPLENISHMENT_TYPE__c;
            } // else [if (null != replishmentType)]
            if ((null == defReplType) || (defReplType.length() == 0))
            {
                defReplType = SCfwConstants.DOMVAL_MATERIALREPLENISHMENT_TYPE_NONE;
            } // if ((null == defReplType) || (defReplType.length() == 0))
            matMove.ReplenishmentType__c = defReplType; 

            sourceList = new List<SelectOption>();
            receiverList = new List<SelectOption>();
            assignmentOk = matMoveBase.getMatMoveInfos(stockId, dates.Start__c, 
                                                   sourceList, receiverList, 
                                                   SCMatMoveBaseExtension.REQUEST_MODE_ORDER);
            requestOk = !assignmentOk;
        } // if (mode.equals('1'))
        else if (mode.equals('2'))
        {
            initMaterialMovements();
        } // else if (mode.equals('2'))
        else if (mode.equals('3'))
        {
            initArticleInfos();
            
            
            dates = new SCTool__c();
            dates.Start__c = Date.today();
            matMove = new SCMaterialMovement__c();
            String defReplType;
            if (null != replishmentType)
            {
                defReplType = replishmentType;
            } // if (null != replishmentType)
            else
            {
                if (appSettings == null) {appSettings = SCApplicationSettings__c.getInstance(); } 
                defReplType = appSettings.DEFAULT_REPLENISHMENT_TYPE__c;
            } // else [if (null != replishmentType)]
            if ((null == defReplType) || (defReplType.length() == 0))
            {
                defReplType = SCfwConstants.DOMVAL_MATERIALREPLENISHMENT_TYPE_NONE;
            } // if ((null == defReplType) || (defReplType.length() == 0))
            matMove.ReplenishmentType__c = defReplType; 

            sourceList = new List<SelectOption>();
            receiverList = new List<SelectOption>();
            assignmentOk = matMoveBase.getMatMoveInfos(stockId, dates.Start__c, 
                                                   sourceList, receiverList, 
                                                   SCMatMoveBaseExtension.REQUEST_MODE_ORDER);
            bookingOk = !assignmentOk;
            
            SCfwDomainValue domVal = SCMatMoveBaseExtension.domSysParam.getDomainValue('1400159');
            sysParamStockV2 = domVal.getControlParameterInt('V2');
            domVal = SCMatMoveBaseExtension.domSysParam.getDomainValue('1400097');
            sysParamMatFeedBackV4 = domVal.getControlParameterInt('V4');
        } // else if (mode.equals('3'))
    } // SCOrderMaterialController
    
    /**
     * Getter for the list of material movement types
     *
     * @return    list of select options containing the material movment types
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public List<SelectOption> getTypeList()
    {
        Integer nV5;
        Integer nV15;
        String label;
        String language = UserInfo.getLanguage();
        List<SelectOption> domValList = new List<SelectOption>();
        
        for (SCfwDomainValue domainValue :SCMatMoveBaseExtension.domMatMove.getDomainValues())
        {
            nV5 = domainValue.getControlParameterInt('V5');
            nV15 = domainValue.getControlParameterInt('V15');
                        
            if (((1 == nV5) && (nV15 > 1)) || 
                domainValue.getId().equals(SCfwConstants.DOMVAL_MATMOVETYP_MATREQUEST_EMPL))
            {
                label = domainValue.getText(language);
                domValList.add(new SelectOption(domainValue.getId(), 
                                                (null == label) ? domainValue.getId() : label));
            } // if (((1 == nV5) && (nV15 > 1)) || ...
        } // for (SCfwDomainValue domainValue :SCMatMoveBaseExtension.domMatMove.getDomainValues())
        return domValList;
    } // getTypeList

    /**
     * This method initialize a list with the infos for 
     * the article to be requested. It will be used for 
     * displaying in the page.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    private void initArticleInfos()
    {
        System.debug('#### initArticleInfos(): orderId -> ' + orderId);

        SCboOrderLine boOrderLine = new SCboOrderLine();
        List<SCOrderLine__c> orderLines = boOrderLine.readByOrder(orderId);
        artInfoList = new List<SCMatMoveBaseExtension.RequestArticleInfo>();
        
        for (SCOrderLine__c line :orderLines)
        {
            String matStat = line.MaterialStatus__c;
            SCfwDomainValue domVal = domMatStat.getDomainValue(matStat);
            Integer matStatV11 = domVal.getControlParameterInt('V11');
            
            if ((mode.equals('1') && 
                 (SCfwConstants.DOMVAL_ORDERLINETYPE_INV == line.Type__c) && 
                 (SCfwConstants.DOMVAL_MATSTAT_ORDERABLE == matStat) && 
                 (line.Article__r.Orderable__c == true)) || 
                (mode.equals('3') && 
                 (SCfwConstants.DOMVAL_ORDERLINETYPE_INV == line.Type__c) && 
                 (1 == matStatV11)))
            {
                SCMatMoveBaseExtension.RequestArticleInfo artInfo = new SCMatMoveBaseExtension.RequestArticleInfo();
                artInfo.article = line.Article__r;
                artInfo.orderLineId = line.Id;
                artInfo.qty = line.Qty__c;
                artInfo.valuationType = line.ValuationType__c;
                artInfo.originalValType = line.ValuationType__c;
                artInfo.existValType = line.ValuationType__c != null ? true : false;
                artInfo.hasArticleValType = line.Article__r.ValuationType__c;
                artInfo.isSet = (artInfo.qty > 0.0);
                
                artInfoList.add(artInfo);
            } // if ((mode.equals('1') && 
            
            if ((!valuationTypeActivated) && (line.ValuationType__c != null)) 
            { 
                valuationTypeActivated = true; 
            }
        } // for (SCOrderLine__c line :orderLines)
        System.debug('#### initArticleInfos(): artInfoList -> ' + artInfoList);
    } // initArticleInfos

    /**
     * Checks if there are articles.
     *
     * @return    true if there are articles
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public Boolean getHasArticles()
    {
        return ((null != artInfoList) && (artInfoList.size() > 0));
    } // getHasArticles
    
    /**
     * This method initialize a list with the material
     * movements for displaying in the page.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    private void initMaterialMovements()
    {
        System.debug('#### initMaterialMovements()');

        matMoves = [Select Article__r.Id, Article__r.Name, Article__r.ArticleNameCalc__c, 
                           CreatedDate, CreatedBy.Alias, Type__c, Status__c, Qty__c, ValuationType__c,
                           DeliveryStock__r.Name, Plant__c, Stock__r.Name, Stock__r.ValuationType__c,
                           Order__r.Name, RequisitionDate__c, ListPrice__c, PrimeCost__c, 
                           Replenishment__r.Id, Replenishment__r.Name, 
                           DeliveryPlant__c, ERPResultDate__c, ERPResultInfo__c, ERPStatus__c
                      from SCMaterialMovement__c 
                      where Order__c = :orderId 
                      order by RequisitionDate__c desc, Article__r.Name];
        
        for (SCMaterialMovement__c matMove : matMoves)
        {
            if ((valuationTypeActivated) || (matMove.ValuationType__c != null) || (matMove.Stock__r.ValuationType__c))
            {
                valuationTypeActivated = true;
                break;
            }
        }
    } // initMaterialMovements

    /**
     * Selects/Deselects all entries in the displayed list.
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference selectAll()
    {
        System.debug('#### selectAll()');

        errMsg = '';
        msgHint = '';
        isSelected = !isSelected;
        if (null != artInfoList)
        {
            for (SCMatMoveBaseExtension.RequestArticleInfo artInfo :artInfoList)
            {
                artInfo.isSet = isSelected;
            } // for (SCMatMoveBaseExtension.RequestArticleInfo artInfo :artInfoList)
        } // if (null != artInfoList)
        return null;
    } // selectAll

    /**
     * This Methode checks for existing material movements and 
     * creates the material movements if the check is ok.
     *
     * @param    check       if true, the check will be done, 
     *                       otherwise only creates material movements
     * @param    forOrder    if true the material movements get a reference to the order
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    private void doMatMove(Boolean check, Boolean forOrder)
    {
        System.debug('#### doMatMove(): check -> ' + check);
        Map<Id,Set<String>> articleMap = new Map<Id,Set<String>>();
        List<Id> orderLineIdList = new List<Id>();
        Map<Id,String> ordlineValtypeMap = new Map<Id,String>();
        
        SCStock__c recStock = [Select Id, ValuationType__c from SCStock__c where Id = :receiverStock ];
        valuationTypeActivated = recStock.ValuationType__c;
        hasValTypeToBeSelected = recStock.ValuationType__c;
        
        String receiver = '';
        errMsg = '';

        for (SCMatMoveBaseExtension.RequestArticleInfo artInfo :artInfoList)
        {
            if (artInfo.isSet && (artInfo.qty > 0.0))
            {
                if ((hasValTypeToBeSelected) && (artInfo.article.ValuationType__c))
                {
                    if (artInfo.valuationType == null)
                    {
                //SAP calculate the VType
                        artInfo.valuationType = null;
                //        errMsg += 'Missing valuation type at article ' + artInfo.article.Name.escapeHtml4() + '<br/>';
                    }
                }
                else
                {
                    artInfo.valuationType = null;
                }
                
                Set<String> valtypeSet = articleMap.get(artInfo.article.Id);
                if (valtypeSet == null) { valtypeSet = new Set<String>(); }
                valtypeSet.add(artInfo.valuationType);
                articleMap.put(artInfo.article.Id, valtypeSet);
                orderLineIdList.add(artInfo.orderLineId);
                
                if (  ((artInfo.valuationType == null) && (artInfo.originalValtype != null))
                    ||((artInfo.valuationType != null) && (!artInfo.valuationType.equalsIgnoreCase(artInfo.originalValtype)))
                   )
                {
                    ordlineValtypeMap.put(artInfo.orderLineId, artInfo.valuationType);
                }
            } // if (artInfo.isSet && (artInfo.qty > 0.0))
        } // for (SCMatMoveBaseExtension.RequestArticleInfo artInfo :artInfoList)

        if (check)
        {
            // check if there a still material movements for the articles
            String infos = matMoveBase.checkForMaterialMovements(stockId, articleMap, 
                                                             SCfwConstants.DOMVAL_MATMOVETYP_MATREQUEST_EMPL);
            
            // material movements found, so inform the user
            if (infos.length() > 0)
            {
                if (errMsg.length() > 0)
                {
                    errMsg += '<br/>';
                }
                
                receiver = receiverList.get(0).getLabel();
                receiver = receiver.subString(receiver.IndexOf('('));
                
                errMsg = System.Label.SC_msg_MatMovesExists1 + ' ' + receiver + ':<br><br>';
                errMsg += infos + '<br/>';
                errMsg += System.Label.SC_msg_MatMovesExists2;
            } // if (infos.length() > 0)
        } // if (check)

        if (errMsg.length() > 0) { System.debug('#### doMatMove(): errMsg -> ' + errMsg); }
        
        if ((errMsg.length() == 0) && (articleMap.size() > 0))
        {
            String replType = ((null != matMove) && 
                               (SCfwConstants.DOMVAL_MATERIALREPLENISHMENT_TYPE_NONE != 
                                matMove.ReplenishmentType__c)) ? 
                                matMove.ReplenishmentType__c : null;
                               
            requestOk = matMoveBase.createMatMoves(artInfoList, sourceStock, receiverStock, 
                                                   dates.Start__c, (forOrder ? orderId : null), 
                                                   SCfwConstants.DOMVAL_MATSTAT_ORDER, 
                                                   matMoveType, replType);
            if (requestOk)
            {
            
                SCboMatStatUpdate matStatUpdate = new SCboMatStatUpdate();
                if (matStatUpdate.syncOrderLines(orderLineIdList))
                {
                    matStatUpdate.syncOrder(orderId);
                } // if (matStatUpdate.syncOrderLines(orderLineIdList))
                
                if (ordlineValtypeMap.size() > 0)
                {
                    List<SCOrderLine__c> listOrderline = new List<SCOrderLine__c>();
                    for (SCOrderLine__c line : [SELECT Id, ValuationType__c FROM SCOrderLine__c WHERE Id IN :ordlineValtypeMap.keySet()])
                    {
                        line.ValuationType__c = ordlineValtypeMap.get(line.Id);
                        listOrderline.add(line);
                    }
                    Database.update(listOrderline);
                }

                //###### CCE ###### -->
                CCWCMaterialReservationCreate.callout(orderId, true, false);
                //###### CCE ###### <--
                
            } // if (requestOk)
        } // if ((errMsg.length() == 0) && (artIdList.size() > 0))
    } // doMatMove

    /**
     * Methode is called, when the user clicks [Order].
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference checkAndRequestMat()
    {
        System.debug('#### checkAndRequestMat()');

        doMatMove(true, true);
        return null;
    } // checkAndRequestMat

    /**
     * Methode is called, when the user clicks [Yes] after 
     * he confirms the question.
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference requestMat()
    {
        System.debug('#### requestMat()');

        doMatMove(false, true);
        return null;
    } // requestMat

    /**
     * This Methode checks the quantity in the stock and 
     * book the material if the check is ok.
     *
     * @param    check       if true, the check will be done, 
     *                       otherwise only booking material
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    private void doMatBook(Boolean check)
    {
        System.debug('#### doMatBook(): check -> ' + check);

        Map<Id,Set<String>> articleMap = new Map<Id,Set<String>>();
        List<Id> orderLineIdList = new List<Id>();
        for (SCMatMoveBaseExtension.RequestArticleInfo artInfo :artInfoList)
        {
            if (artInfo.isSet && (artInfo.qty > 0.0))
            {
                Set<String> valtypeSet = articleMap.get(artInfo.article.Id);
                if (valtypeSet == null) { valtypeSet = new Set<String>(); }
                valtypeSet.add(artInfo.valuationType);
                articleMap.put(artInfo.article.Id, valtypeSet);
                
                orderLineIdList.add(artInfo.orderLineId);
            } // if (artInfo.isSet && (artInfo.qty > 0.0))
        } // for (SCMatMoveBaseExtension.RequestArticleInfo artInfo :artInfoList)
        
        errMsg = '';
        msgHint = '';
        if (check && 
            (1 == sysParamStockV2) && (sysParamMatFeedBackV4 > 0))
        {
            Boolean stockValType = false;
            Map<String, Double> qtyMap = new Map<String, Double>();
            for (SCStockItem__c item :[Select Id, Article__c, Qty__c, ValuationType__c, Stock__r.ValuationType__c
                                         from SCStockItem__c 
                                        where Stock__c = :stockId and
                                              Article__c in :articleMap.keySet()])
            {
                qtyMap.put(item.Article__c + '-' + (item.ValuationType__c != null? item.ValuationType__c : ''), item.Qty__c);
                stockValType = item.stock__r.ValuationType__c;
            } // for (SCStockItem__c item :[Select Id, Article__c, Qty__c ...

            for (SCMatMoveBaseExtension.RequestArticleInfo artInfo :artInfoList)
            {
                if (artInfo.isSet && (artInfo.qty > 0.0))
                {
                    Double qty = qtyMap.get(artInfo.article.Id + '-' + artInfo.valuationType);
                    if ((null != qty) && (artInfo.qty > qty))
                    {
                        if (stockValType)
                        {
                            errMsg += artInfo.article.Name + ' - ' + 
                                      artInfo.article.ArticleNameCalc__c + ' - ' + 
                                      artInfo.valuationType + '<br/>';
                        }
                        else
                        {
                            errMsg += artInfo.article.Name + ' - ' + 
                                      artInfo.article.ArticleNameCalc__c + '<br/>';
                        }
                    } // if ((null != qty) && (artInfo.qty > qty))
                } // if (artInfo.isSet && (artInfo.qty > 0.0))
            } // for (SCMatMoveBaseExtension.RequestArticleInfo artInfo :artInfoList)
            
            if (errMsg.length() > 0)
            {
                errMsg = System.Label.SC_msg_MatBookNoQty + '<br/><br/>' + errMsg + '<br/>';
                errMsg += (1 == sysParamMatFeedBackV4) ? System.Label.SC_msg_MatBookQuestion: 
                                                         System.Label.SC_msg_MatBookHint;
            } // if (errMsg.length() > 0)
        } // if (check && ...
        
        if ((errMsg.length() == 0) && (articleMap.size() > 0))
        {
            String replType = ((null != matMove) && 
                               (null != matMove.ReplenishmentType__c)) ? 
                               matMove.ReplenishmentType__c : null;
           

            bookingOk = matMoveBase.createMatMoves(artInfoList, sourceStock, stockId, 
                                                   dates.Start__c, orderId, 
                                                   SCfwConstants.DOMVAL_MATSTAT_BOOK, 
                                                   SCfwConstants.DOMVAL_MATMOVETYP_CONSUMPTION, 
                                                   replType);
            if (bookingOk)
            {
                if (matMoveBase.bookMaterial(stockId, orderId))
                {
                    if (getHasNotBookedArtikels())
                    {
                        msgHint = System.Label.SC_msg_ArticlesNotBooked;
                        
                        // remove all booked articles from the list, 
                        // so the user can see which are not booked
                        SCMatMoveBaseExtension.RequestArticleInfo artInfo;
                        for (Integer i=artInfoList.size()-1; i>=0; i--)
                        {
                            artInfo = artInfoList.get(i);
                            
                            String key = artInfo.article.Id;
                            if(artInfo.valuationType != null)
                            {
                                key += '-' + artInfo.valuationType;
                            }
                            if (matMoveBase.boMatMove.articleWithNegativStockQty.get(key) == null)
                            {
                                artInfoList.remove(i);
                            }
                        } // for (Integer i=artInfoList.size()-1; i>=0; i--)
                    } // if (getHasNotBookedArtikels())
                    SCboMatStatUpdate matStatUpdate = new SCboMatStatUpdate();
                    if (matStatUpdate.syncOrderLines(orderLineIdList))
                    {
                        matStatUpdate.syncOrder(orderId);
                    } // if (matStatUpdate.syncOrderLines(orderLineIdList))
                } // if (matMoveBase.bookMaterial(stockId, orderId))
                
                //###### CCE ###### -->
                CCWCMaterialMovementCreate.callout(orderId, true, false);
                //###### CCE ###### <--
                
                
            } // if (bookingOk)
        } // if ((errMsg.length() == 0) && (artIdList.size() > 0))
    } // doMatBook

    /**
     * Methode is called, when the user clicks [Order].
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference checkAndBookMat()
    {
        System.debug('#### checkAndBookMat()');

        doMatBook(true);
        return null;
    } // checkAndBookMat

    /**
     * Methode is called, when the user clicks [Yes] after 
     * he confirms the question.
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference bookMat()
    {
        System.debug('#### bookMat()');

        doMatBook(false);
        return null;
    } // bookMat

    /**
     * Checks if there are articles which could not be 
     * booked because of negativ stock quantities.
     *
     * @return    true if there are articles which could not be 
     *            booked because of negativ stock quantities
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public Boolean getHasNotBookedArtikels()
    {
        System.debug('#### getHasNotBookedArtikels()');

        return (matMoveBase.boMatMove.articleWithNegativStockQty.size() > 0);
    } // getHasNotBookedArtikels
    
    
    class ValType
    {
        public String key {get; set;}
        public String label {get; set;}       
    }
    
    /*
     * Methods for the valuationType selected list
     */
    private static List<ValType> valtypetranslations = new List<ValType>();
    
    public List<SelectOption> getValuationTypeOptions()
    {
        List<SelectOption> listOption = new List<SelectOption>();
        if((valtypetranslations == null) || (valtypetranslations.size() == 0))
        {
            for (Schema.PicklistEntry schemaPickEntry : SCOrderLine__c.ValuationType__c.getDescribe().getPicklistValues())
            {
                ValType v = new ValType();
                v.key = schemaPickEntry.getValue().toLowerCase();
                v.label = schemaPickEntry.getLabel();
                valtypetranslations.add(v);
            }
        }
        
        for (ValType v : valtypetranslations)
        {
            system.debug('###valtype###' + v.key + ' ## ' + v.label);
            listOption.add(new SelectOption(v.key, v.label));
        }
        return listOption;
    }
} // SCOrderMaterialController
