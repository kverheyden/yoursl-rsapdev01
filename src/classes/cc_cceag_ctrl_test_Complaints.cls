@isTest(SeeAllData=true)
public class cc_cceag_ctrl_test_Complaints 
{
    static testMethod void testHandleException()
    {
        ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
        
        Test.startTest();
        cc_cceag_ctrl_Complaints.handleException(new NullPointerException(),result);
        Test.stopTest();
        
        System.assertEquals(1, result.messages.size());
    }
    
    static testMethod void testSetupResultFromContext()
    {
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        
        Test.startTest();
        ccrz.cc_RemoteActionResult res = cc_cceag_ctrl_Complaints.setupResultFromContext(ctx);
        Test.stopTest();
        
        System.assertEquals(ctx, res.inputContext);
        System.assertEquals(false, res.success);
    }
    
    static testMethod void testFetchData()
    {
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        
        Test.startTest();
        ccrz.cc_RemoteActionResult res = cc_cceag_ctrl_Complaints.fetchData(ctx);
        Test.stopTest();

        System.assertEquals(true, res.success);
        System.assert(res.data instanceof List<Object>);
    }
    
    static testMethod void testSaveData()
    {
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        
        Test.startTest();
        ccrz.cc_RemoteActionResult res = cc_cceag_ctrl_Complaints.saveData(ctx,'Topic','Message');
        Test.stopTest();
        
        System.assertEquals(true, res.success);
		System.assert(res.data instanceof Map<string,string>);
        String caseId = ((Map<string,string>)res.data).get('caseId');
        System.assert(String.isNotBlank(caseId));
        Case c = [SELECT Id,Thematische_Zuordnung__c,ECOM_Thematische_Zuordnung__c,Description,Kontaktrichtung__c FROM Case WHERE ID = :caseId];
        System.assertEquals('Topic', c.Thematische_Zuordnung__c);
        System.assertEquals('Topic', c.ECOM_Thematische_Zuordnung__c);
        System.assertEquals('Message', c.Description);
        System.assertEquals('E-Commerce', c.Kontaktrichtung__c);
    }
    
    static testMethod void testUploadAttachmentWithNoCaseId()
    {
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        
        Test.startTest();
        Map<String,String> res = cc_cceag_ctrl_Complaints.doUploadAttachment(ctx,null,'body','name',null);
        Test.stopTest();

        System.assert(res.get('error')=='Case Id was null');
    }
    
    static testMethod void testUploadAttachmentWithNoBody()
    {
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        
        Case c = new Case();
        insert c;
        
        Test.startTest();
        Map<String,String> res = cc_cceag_ctrl_Complaints.doUploadAttachment(ctx,c.Id,null,'name',null);
        Test.stopTest();

        System.assert(res.get('error')=='Attachment Body was null');
    }
    
    static testMethod void testUploadAttachmentWithWrongCaseId()
    {
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
             
        System.runAs ( new User(Id = UserInfo.getUserId()) ) {
        	Contact contact = new Contact(LastName='lastname');
	        insert contact;
        }
        
        User user;
        System.runAs ( new User(Id = UserInfo.getUserId()) ) {        
            user = new User(ContactId=[SELECT Id FROM Contact LIMIT 1].Id,
                                 Username='u@u.de', 
                                 LastName='lastname', 
                                 Email='u@u.de', 
                                 Alias='alias', 
                                 CommunityNickname='nickname', 
                                 TimeZoneSidKey='Europe/Berlin', 
                                 LocaleSidKey='de', 
                                 EmailEncodingKey='UTF-8', 
                                // UserRoleId=[SELECT Id FROM UserRole WHERE Name like 'DE' LIMIT 1].Id,
                                 ProfileId=[SELECT Id FROM Profile WHERE Name like '%CloudCraze%' Limit 1].Id, 
                                 LanguageLocaleKey='de');
            insert user;
        }
        
        Map<String,String> res;
        System.runAs(user) {
        	res = cc_cceag_ctrl_Complaints.doUploadAttachment(ctx,'xxx','body','name',null);
		}

        System.assert(res.get('error')=='Case not found!');
    }

    static testMethod void testUploadAttachmentWithoutAttachment()
    {
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
             
        case ca;
        Contact contact;
        Account a;
        System.runAs ( new User(Id = UserInfo.getUserId()) ) {
            a = new Account(Name='account',AccountNumber='xxx');
            insert a;
        }
        contact = new Contact(LastName='lastname',AccountId = a.Id);
        insert contact;
        
        
        User user;
        System.runAs ( new User(Id = UserInfo.getUserId()) ) {        
            user = new User(ContactId=contact.Id,
                                 Username='u@u.de', 
                                 LastName='lastname', 
                                 Email='u@u.de', 
                                 Alias='alias', 
                                 CommunityNickname='nickname', 
                                 TimeZoneSidKey='Europe/Berlin', 
                                 LocaleSidKey='de', 
                                 EmailEncodingKey='UTF-8', 
                                 ProfileId=[SELECT Id FROM Profile WHERE Name like '%CloudCraze%' Limit 1].Id, 
                                 LanguageLocaleKey='de');
            insert user;
        }

        ca = new Case(ContactId=contact.Id);
        ca.OwnerId = user.Id;
        insert ca;
        System.Debug('**********************************');
        System.Debug(ca);

        
        Map<String,String> res;
        System.runAs(user) {
        	res = cc_cceag_ctrl_Complaints.doUploadAttachment(ctx,ca.Id,'body','name',null);
		}

        System.assert(res.get('success')=='1');
    }    
    
}
