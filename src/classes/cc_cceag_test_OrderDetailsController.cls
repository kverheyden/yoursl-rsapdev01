/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class cc_cceag_test_OrderDetailsController {

    static testMethod void myUnitTest() {
        ccrz__E_order__c o = new ccrz__E_Order__c(
            ccrz__OrderId__c='MyTestOrder',
            ccrz__PONumber__c='MyTestOrder',
            ccrz__TaxAmount__c=1.99, 
            ccrz__ShipAmount__c=2.99, 
            ccrz__CurrencyIsoCode__c='USD',
            ccrz__OrderStatus__c = 'Order Submitted',
            ccrz__storefront__c='DefaultStore',
            ccrz__RequestDate__c = Date.today(),
            CCEAGDeliveryTime__c = Date.today().addDays(30).format(),
            CCEAGNewsletter__c = false
        );
        insert o;
        cc_cceag_OrderDetailsController ctrl = new cc_cceag_OrderDetailsController();
        String orderName = cc_cceag_OrderDetailsController.getOrderNumber(o.id);
        system.assertEquals('MyTestOrder', orderName);
        //test exception case
        orderName = cc_cceag_OrderDetailsController.getOrderNumber('12345');
        system.assertEquals(null, orderName);
        Folder f = [select id from Folder where name='Ecommerce'];
        EmailTemplate e = new EmailTemplate (developerName = 'CC_CCEAG_Order',  FolderId = f.Id, TemplateType= 'Text', Name = 'test'); 
		if([select id from EmailTemplate where developerName='CC_CCEAG_Order'].size() == 0) {
			insert e;
		}
		cc_cceag_OrderDetailsController.sendOrderEmail(o.id, 'roy.lou@cloudcraze.com');
        
    }
    
    
}
