public with sharing class SCProductModelTriggerDispatcher extends TriggerDispatcherBase {
	public override void beforeUpdate(TriggerParameters tp) {
		execute(new SCProductModelTriggerHandler.SCProductModelBUHandler(), tp, TriggerParameters.TriggerEvent.beforeUpdate);
	}

	public override void beforeInsert(TriggerParameters tp) {
		execute(null, tp, TriggerParameters.TriggerEvent.beforeInsert);
	}
}
