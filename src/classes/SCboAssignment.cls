/**
 * @(#)SCAssignmentAfterUpdate.cls    ASE1.0 hs 15.10.2010
 * 
 * Copyright (c) 2009 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 * 
 * $Id: SCAssignmentAfterUpdate.cls 7692 2010-10-15 16:18:41Z hschroeder $
 */
public class SCboAssignment
{
    // prevent to call our own trigger
    static Boolean ignoreTrigger = false;

    /**
     * Trigger method
     *
     * @param triggerNew
     * @param triggerOld
     * @author HS <hschroeder@gms-online.de>
     */
    public static void beforeUpdate(List<SCAssignment__c> triggerNew, List<SCAssignment__c> triggerOld)
    {
        for (Integer i = 0; i < triggerNew.size(); i++)
        {
            if (triggerNew[i].TriggerHint__c == '1')
            {
                ignoreTrigger = true;
                triggerNew[i].TriggerHint__c = null;
            }
            else
            {
                ignoreTrigger = false;
            }
            
            //CCEAG --> 
            // detect if the assignment status will beclosed and set the timestamp that is used in the 
            // SCbtcClearingPostProcess to initate the service report creation and post processing

            // System.debug('SCboAssignment.beforeUpdate, Status New: ' + triggerNew + ', Old: ' + triggerOld);
            
            if (triggerNew[i].Status__c != triggerOld[i].Status__c &&
                triggerNew[i].Status__c == SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED)
            {
                // Please note: the mobile workshop process sets the assignment status to SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL
                // so we should expect no side effects with the mobile client and backend order processing that sets DOMVAL_ORDERSTATUS_COMPLETED 

                triggerNew[i].ClearingCompleted__c = DateTime.Now();       // store the timestamp - for settling delay (so all data has been uploaded)
                triggerNew[i].ClearingPostProcessStatus__c = 'pending';    // mark to be processed by SCbtcClearingPostProcess 
            }            
            //CCEAG <--
        }
    }
    
    /**
     * Trigger method
     *
     * @param triggerNew
     * @param triggerOld
     * @author HS <hschroeder@gms-online.de>
     */
    public static void afterUpdate(List<SCAssignment__c> triggerNew, List<SCAssignment__c> triggerOld)
    {
        if (ignoreTrigger)
        {
            return;
        }
            
        List <Id> newAssIds = new List<ID>();
        Map <Id, String> newAssStatus = new Map<Id, String>();
        
        List<SCTimeReport__c> timeReports = new List<SCTimeReport__c>();
        for (Integer i = 0; i < triggerNew.size(); i++)
        {
            System.debug('Processing Assignment, New: ' + triggerNew + ', Old: ' + triggerOld);
            
            if (triggerNew[i].Status__c != triggerOld[i].Status__c)
            {
                newAssIds.add(triggerNew[i].Id);                
                newAssStatus.put(triggerNew[i].Id, triggerNew[i].Status__c);
                
                //PMS 34649: CP New Fields for Mobile Client - Time Reports Light and  PDF printoputs
                //ET: generate time reports from assignment
                Boolean createTimeReports = true; //Should be an application setting
                
                if(createTimeReports && triggerNew[i].Status__c == SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED) 
                {
                    timeReports.addAll(SCboTimeReport.generateTimeReportsLight(triggerNew[i]));
                }              
            }
        }
        
        //Upsert generated time reports if available
        if(timeReports.size() > 0)
        {
            system.debug('trying to upsert timeReports');
            upsert timeReports;
        }

        List<SCAppointment__c> apps = [ select assignment__c,  Id2__c, AssignmentStatus__c from SCAppointment__c where Assignment__c in :newAssIds];
        for (Integer i = 0; i < apps.size(); i++)
        {
            String status = newAssStatus.get(apps[i].assignment__c);
            if(status != null)
            {
                System.debug('Processing Assignment Id -->, New Status: ' + apps[i].assignment__c + '--->, Status: ' + status);
                apps[i].AssignmentStatus__c = status;
                // set release status in ASE
                AseSetStatus.callout(apps[i].ID2__c, apps[i].ID, apps[i].AssignmentStatus__c);
            }
        }            
        update apps;
    }


    /**
     * Gets the order id for a given assignment id
     *
     * @param assignmentId     Id of the order assignment
     * @author Martin Assmann <massmann@gms-online.de>
     */
    public static String getOrderID(String assignmentId)
    {
        String orderId;
        List<SCAssignment__c> assignments = new List<SCAssignment__c>();
        
        assignments = [select Order__r.Id
                      from SCAssignment__c 
                      where Id = :assignmentId];
        
        //return null if id could not be found
        if (assignments != null && assignments.size() > 0) 
        {
            orderId = assignments.get(0).order__r.id;
        }
        else
        {
            orderId = null;
        }
        
        return orderId; 
    }
    
    
    
}
