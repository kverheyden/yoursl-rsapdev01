/**
* @author thomas richter <thomas@meformobile.com>
*/
public class EComProduct2ToCloudcrazeProductResolver 
{
    /**
     * this method is used to set a ccProduct Reference on an given list of sobject based on a Product2 reference
     * - it's a generic implementation - provide the fieldname for the product2 reference and cloudcraze product 
     * reference fields
     * 
     * - mapping between Product2 and CCProduct is done using Product2.ID2__c and ccrz__E_Product__c.ccrz__ProductId__c
     * 
     * @author thomas richter <thomas@meformobile.com>
     */
	public void setCloudCrazeProductReferenceFromProduct2Reference(SObject[] objects,String Product2FieldName,String CloudCrazeProductFieldName) 
    {
        // collect all references products
        List<ID> prodIds = new List<ID>();
        for (SObject obj : objects) {
            prodIds.add((ID)obj.get(Product2FieldName));
        }
        // get all referenced products
        List<Product2> products = [SELECT Id, ID2__c FROM Product2 WHERE Id in :prodIds];
        Map<ID,String> productIdsToMatNr = new Map<ID,String>();
        for (Product2 product : products) {
            productIdsToMatNr.put(product.Id, product.ID2__c);
        }

        // get corresponding cloudcraze product
		List<ccrz__E_Product__c> ccProducts = [SELECT Id, ccrz__ProductId__c FROM ccrz__E_Product__c WHERE ccrz__ProductId__c IN :productIdsToMatNr.values()];
        Map<String,ID> matNrToCCProductId = new Map<String,ID>();
        for (ccrz__E_Product__c ccProduct : ccProducts) {
            matNrToCCProductId.put(ccProduct.ccrz__ProductId__c, ccProduct.Id);
        }
        
        // now update subtradechannelproduct records
        for (SObject obj : objects) {
            ID prodId = (ID)obj.get(Product2FieldName);
            String matNr = productIdsToMatNr.get(prodId);
            ID ccProdId = matNrToCCProductId.get(matNr);
            obj.put(CloudCrazeProductFieldName,ccProdId);
        }
    }
}
