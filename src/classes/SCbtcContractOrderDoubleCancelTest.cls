/*
 * @(#)SCbtcContractOrderDoubleCancelTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCbtcContractOrderDoubleCancelTest
{
    static testMethod void testOrderCreatePositive()
    {
        Test.StartTest();
        SCContractTestHelper cth = new SCContractTestHelper();
        String contractID = cth.prepareForBtcContractVisitCreateTest();
        System.debug('###Test contract: ' + cth.contract);
        System.debug('###ContractId: ' + contractID);

        SCbtcContractOrderDoubleCancel.cancel('20110101', '20110102', 0, 'trace');
        SCbtcContractOrderDoubleCancel btc = new SCbtcContractOrderDoubleCancel('20110101', '20110102', 0, 'trace');
        btc.start(null);
        btc.finish(null);
        
        SCbtcContractVisitCreate.syncCreateContractVisits(contractID);

        
        // Order Creation
        
        SCContract__c c = [Select Name, Id from SCContract__c where Id = :contractID];
        List<SCContractVisit__c> visitList = [Select Id from SCContractVisit__c where Contract__c = :contractID];
        List<String> visitIdList = new List<String>();
        for(SCContractVisit__c visit: visitList)
        {
            visitIdList.add(visit.Id);
            // create only one order
            break;
        } 
        SCbtcContractOrderCreate.canCreateOrders(visitIdList);
        SCbtcContractOrderCreate.syncCreateOrders(visitIdList);

        // allow the creation of double order
        for(SCContractVisit__c visit: visitList)
        {
            visit.Status__c = 'open';
            visit.Order__c = null;
            // create only one order
            break;
        } 
        update visitList;
        // creation of the double order
        SCbtcContractOrderCreate.canCreateOrders(visitIdList);
        SCbtcContractOrderCreate.syncCreateOrders(visitIdList);

        
        List<SCOrder__c> oL = [Select id from SCOrder__c where Contract__c = :c.Id];
        System.debug('SCOrderList: ' + oL);
        Id orderId = null;
        if(oL.size() > 0)
        {
            orderId = oL[0].id;
            List<SCInterfaceLog__c> scope = new List<SCInterfaceLog__c>();
            SCInterfaceLog__c il = new SCInterfaceLog__c();
            
            il.Contract__c = contractID;
            
            il.Order__c = orderId;
            scope.add(il);
            btc.execute(null, scope);
        }
        Test.StopTest();
    }
    
    //@author GMSS
    /*static testMethod void testCodeCoverageA()
    {
        SCOrder__c order = new SCOrder__C();
        Database.insert(order);
        
        SCContract__c contract = new SCContract__c();
        Database.insert(contract);
        
        SCInterfaceLog__c log  = new SCInterfaceLog__c(Order__c = order.Id, Contract__c = contract.Id);
        SCInterfaceLog__c log1 = new SCInterfaceLog__c();
        SCInterfaceLog__c log2 = new SCInterfaceLog__c();
        SCInterfaceLog__c log3 = new SCInterfaceLog__c();
        Database.insert(new List<SCInterfaceLog__c>{log,log1,log2,log3});
        
        
        Test.startTest();
        
        
        SCbtcContractOrderDoubleCancel obj = new SCbtcContractOrderDoubleCancel(Date.Today().format(), Date.Today().format(), 10, 'test');
        try { obj.execute(null, new List<sObject>{log3}); } catch(Exception e) { }
        try { obj.cancelOrder(log, new List<SCInterfaceLog__c>{log1,log2}); } catch(Exception e) { }
        
        
        Test.stopTest();
    }*/
}
