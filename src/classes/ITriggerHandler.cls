/**
* @author Hari Krishnan
* @date 07/16/2013
* @description Interface for trigger handlers. Logic for the first time events are placed under the mainEntry 
* method and the logic for the subsequent events raised on the same transaction (reentrant) are placed under 
* the inProgressEntry method.
*/
public interface ITriggerHandler {
	
	/** 
	* @author Jan Mensfeld
	* @date 30.10.2014
	* @description Method that runs if the trigger is  the first time in the execution context. 
	* Subclasses have to implement this method.
	* @param TriggerParameters The trigger parameters such as the list of records before and after the update.
	*/
	void mainEntry(TriggerParameters tp);
	
	/** 
	* @author Jan Mensfeld
	* @date 30.10.2014
	* @description Called for the subsequent times in the same execution context. The trigger handlers can chose
	* to ignore if they are not running recursively.
	* @param TriggerParameters The trigger parameters such as the list of records before and after the update.
	*/
	void inProgressEntry(TriggerParameters tp);
	
	/** 
	* @author Jan Mensfeld
	* @date 30.10.2014
	* @description Updated the objects if there are any object to update.
	*/
	void updateObjects();
}
