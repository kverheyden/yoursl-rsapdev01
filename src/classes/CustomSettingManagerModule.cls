public class CustomSettingManagerModule {
    Schema.SObjectType sType;
    public String sTypeStr {get;set;}
    public List<String> fields {get;set;}
    public List<String> queryableFields {get;set;}
    public List<CustomSettingRecordWrapper> settings {get;set;}
    public Boolean importMode {get;set;}
    
    public String selectedCustomSettingType {
        get;
        set{
       		if(sobjectTypeConfiguration.containsKey(value) 
            ){
           		selectedCustomSettingType = value;
                sType = sobjectTypeConfiguration.get(value);
                sTypeStr = String.valueOf(sType);
            	fields = fieldConfiguration.get(sType);
                queryableFields = queryableFieldConfiguration.get(sType);
            }
        }
    }
    public List<SelectOption> availableCustomSettingTypes {
        get{
            if(availableCustomSettingTypes == null){
                availableCustomSettingTypes = new List<SelectOption>();
                for(String str : sobjectTypeConfiguration.keySet()){
               		availableCustomSettingTypes.add(new SelectOption(str,str));
                }
            }
            return availableCustomSettingTypes;
        }
        set;
    }
    
    public CustomSettingManagerModule(){
        this.importMode = false;
        
        if(!String.isBlank(ApexPages.currentPage().getParameters().get('sType'))){
            this.sTypeStr = ApexPages.currentPage().getParameters().get('sType');
        }else
        	this.sTypeStr = 'SalesAppSettings__c';
        
        if(sobjectTypeConfiguration.containsKey(this.sTypeStr)){
            this.sType = sobjectTypeConfiguration.get(this.sTypeStr);
			this.fields = fieldConfiguration.get(sType);
        	this.queryableFields = queryableFieldConfiguration.get(sType);
        }else{
            throw new CustomSettingException('Unsupported custom setting');
        }
        queryRecords();
    }
    public PageReference save(){
        List<Sobject> toInsert = new List<SObject>();
        List<Sobject> toUpdate = new List<SObject>();
        for(CustomSettingRecordWrapper settingWrapper : this.settings){
            if(settingWrapper.include){
                if(settingWrapper.setting.get('Id') != null)
                	toUpdate.add(settingWrapper.setting);
                else 
                    toInsert.add(settingWrapper.setting);
            }
        }
        if(!toUpdate.isEmpty()){
            update toUpdate;
        }
        if(!toInsert.isEmpty()){
            insert toInsert;
        }
        queryRecords();
        return null;
    }
    public PageReference reload(){
        queryRecords();
        return null;
    }
    public void queryRecords(){
        String query = 'Select ';
        query += String.join(fields,' , ');
        query += ' from '+sTypeStr;
        List<Sobject> records = Database.query(query);
        settings = new List<CustomSettingRecordWrapper>();
        for(Sobject record : records){
            settings.add(new CustomSettingRecordWrapper(record, this));
        }
    }
    public PageReference getAffiliateData(){
        loadAllAffiliateData();
        return null;
    }
    public Map<String, List<ID>> idsCache {get;set;}
    public void loadAllAffiliateData(){
    	idsCache = new Map<String, List<ID>>();
        for(CustomSettingRecordWrapper settingWrapper : settings){
            for(String valueField : this.queryableFields){
                ID temp;
                try{
                	temp = settingWrapper.affilateDataMap.get(valueField).value;
                }catch(Exception e){
                    continue;
                }
                String prefix = String.valueOf(temp).substring(0,3);
                if(!idsCache.containsKey(prefix))
                    idsCache.put(prefix, new List<ID>());
                idsCache.get(prefix).add(temp);
            }
        }
        
        Map<Id, Sobject> objectsCache = new Map<ID,Sobject>();
        Map<String,String> prefixConfiguration = new Map<String,String>();
        debug = '';
        for(String prefix : idsCache.keySet()){
            Schema.SObjectType sType = idsCache.get(prefix).get(0).getSobjectType();
            String sObjectStr = String.valueOf(sType);
            if(!prefixConfiguration.containsKey(prefix))
                prefixConfiguration.put(prefix,sObjectStr);
            List<Id> ids = idsCache.get(prefix);
            if(sObjectFieldConfiguration.get(sObjectStr) != null && sObjectStr != null){
                String queryStr = 'Select Id, '+sObjectFieldConfiguration.get(sObjectStr)+' from '+sObjectStr+' where Id in: ids';
                debug += '[prefix: '+prefix+'|queryString: '+queryStr+']';
                List<SObject> tempSobjectList = Database.query(queryStr);
                if(!tempSobjectList.isEmpty())
                    objectsCache.putAll(tempSobjectList);
            }
        }
        for(CustomSettingRecordWrapper settingWrapper : settings){
            for(String valueField : this.queryableFields){
                ID temp;
                try{
                	temp = settingWrapper.affilateDataMap.get(valueField).value;
                }catch(Exception e){
                    continue;
                }
                if(objectsCache.containsKey(temp)){
                    String prefix = String.valueOf(temp).substring(0,3);
                    String sObjectStr = prefixConfiguration.get(prefix);
                    String fieldName = sObjectFieldConfiguration.get(sObjectStr);
                    settingWrapper.affilateDataMap.get(valueField).value = String.valueOf(objectsCache.get(temp).get(fieldName));
                    settingWrapper.affilateDataMap.get(valueField).targetField = fieldName;
                    settingWrapper.affilateDataMap.get(valueField).sType = sObjectStr;
                }else{
                    settingWrapper.affilateDataMap.get(valueField).value = null;
                    settingWrapper.affilateDataMap.get(valueField).targetField = null;
                    settingWrapper.affilateDataMap.get(valueField).sType = null;
                }
            }
        }
    }
    public String debug1 {get;set;}
    public String importJson {get;set;}
    public PageReference importConfiguration(){
        if(!String.isBlank(importJson)){
            importMode = true;
            Map<String, Id> existingSettings = new Map<String, Id>();
            
            String pkFieldName = '';
            String query = '';
            if(customSettingTypeConfiguration.get(this.sTypeStr)){ // this line is veryfing if cs is a list or hierarchy type. if list then it will query for Name field, else for SetupOwnerID.
                pkFieldName = 'SetupOwnerId';
                query = 'Select Id, SetupOwnerId from '+this.sTypeStr;
            }else{
                pkFieldName = 'Name';
                query = 'Select Id, Name from '+this.sTypeStr;
            }
            system.debug(query);
            List<Sobject> records = Database.query(query);
            for(Sobject sobj : records)
                existingSettings.put((String)sobj.get(pkFieldName), (Id) sobj.get('Id'));
            
            debug1 = '';
            settings.clear();
            List<CSImpExpSetting> importedSettings = (List<CSImpExpSetting>) System.JSON.deserialize(importJson, List<CSImpExpSetting>.class);
            
            Map<String, List<String>> queryArguments = new Map<String,List<String>>();
            for(CSImpExpSetting setting : importedSettings){
                Type objType = Type.forName(setting.SettingType);
                Sobject obj = (SObject) objType.newInstance();
                CustomSettingRecordWrapper settingWrapper = new CustomSettingRecordWrapper(obj, this);
                for(CSImpExpField field : setting.fields){
                    if(field.settingField == pkFieldName){
                        if(existingSettings.containsKey(field.value)){
                            settingWrapper.importStatus = 'Override';
                            settingWrapper.setting.put('Id',existingSettings.get(field.settingField));
                        }else{
                            settingWrapper.importStatus = 'New';
                        }
                    }
                    if(field.isQueryable){
                        if(!queryArguments.containsKey(field.sType)){
                            queryArguments.put(field.sType, new List<String>());
                        }
                        queryArguments.get(field.sType).add(field.value);
                        
                        CustomSettingRecordAffiliateData fieldWrapper = 
                            new CustomSettingRecordAffiliateData(settingWrapper, field.settingField, field.value, field.targetField, field.sType);
                        settingWrapper.affilateDataMap.put(field.settingField, fieldWrapper);
                    }else{
                        settingWrapper.affilateDataMap.put(field.settingField, new CustomSettingRecordAffiliateData(settingWrapper, field.settingField));
                        try{
                            settingWrapper.setting.put(field.settingField,field.value);
                        }catch(Exception e){
                            debug1 += e.getTypeName();
                            debug1 += ' | '+e.getMessage();
                            debug1 += ' | '+e.getCause();
                            if(e.getTypeName() == 'System.SObjectException'){
                                String msg = e.getMessage();
                                if(msg.startsWith('Illegal assignment from String to ')){
                                    if(msg.substring(34, msg.length()) == 'Decimal'){
                                        settingWrapper.setting.put(field.settingField, Decimal.valueOf(field.value));
                                    }else if(msg.substring(34, msg.length()) == 'Boolean'){
                                        settingWrapper.setting.put(field.settingField, Boolean.valueOf(field.value));
                                    }else{
                                        throw e;
                                    }
                                }else{
                                    throw e;
                                }
                            }else{
                                throw e;
                            }
                        }
                    }
                }
                settings.add(settingWrapper);
            }
            Map<String, Map<String, Sobject>> objectsCache = new Map<String, Map<String, Sobject>>();
            for(String sObjType : queryArguments.keySet()){
                List<String> queryArgumentSet = queryArguments.get(sObjType);
                Map<String, Sobject> temp = new Map<String, Sobject>();
                debug1 += 'sObjectFieldConfiguration.get(sObjType): '+sObjType+' | '+sObjectFieldConfiguration.get(sObjType);
                if(sObjectFieldConfiguration.get(sObjType) != null)
                for(SObject sobj : Database.query('Select Id, '+sObjectFieldConfiguration.get(sObjType)+' from '+sObjType+' where '+sObjectFieldConfiguration.get(sObjType)+' in: queryArgumentSet')){
                    temp.put((String)sobj.get(sObjectFieldConfiguration.get(sObjType)),sobj);
                }
                objectsCache.put(sObjType, temp);
            }
            for(CustomSettingRecordWrapper settingWrapper : settings){
                for(String valueField : this.queryableFields){
                    if(settingWrapper.affilateDataMap.containsKey(valueField)){
                        CustomSettingRecordAffiliateData fieldWrapper = settingWrapper.affilateDataMap.get(valueField);
                        if(String.isBlank(fieldWrapper.sType)){
                            settingWrapper.setting.put(valueField,fieldWrapper.value);
                        }else if(objectsCache.containsKey(fieldWrapper.sType) && objectsCache.get(fieldWrapper.sType).containsKey(fieldWrapper.value)){
                            settingWrapper.setting.put(fieldWrapper.settingField,(ID)objectsCache.get(fieldWrapper.sType).get(fieldWrapper.value).get('Id'));
                        }else{
                            settingWrapper.include = false;
                        }
                    }
                }
            }
        }
        return null;
    }
    public String exportJson {get;set;}
    public PageReference exportConfiguration(){
        JSONGenerator gen = JSON.createGenerator(true);
        
        List<CSImpExpSetting> exportSettings = new List<CSImpExpSetting>();
        for(CustomSettingRecordWrapper recWrapper : settings){
            if(recWrapper.include){
                CSImpExpSetting setting = new CSImpExpSetting();
                setting.settingType = sTypeStr;
                setting.fields = new List<CSImpExpField>();
                for(CustomSettingRecordAffiliateData affiliateData : recWrapper.affilateDataMap.values()){
                    if(affiliateData.settingField == 'Id') continue;
                	CSImpExpField field = new CSImpExpField();
                    field.settingField = affiliateData.settingField;
					field.isQueryable = affiliateData.isQueryable;
            		field.value = affiliateData.value;
					field.targetField = affiliateData.targetField;
					field.sType = affiliateData.sType;
                    setting.fields.add(field);
                }
                exportSettings.add(setting);
            }
        }
        exportJson = JSON.serialize(exportSettings);
		return null;
    }
    public String debug {get;set;}
    // ##################### Helper class ##################### //
    public class CSImpExpSetting{
        public String SettingType;
        public List<CSImpExpField> fields;
    }
    public class CSImpExpField{
        public String settingField;
		public Boolean isQueryable;
		public String value;
		public String targetField;
		public String sType;
    }
    
    public class CustomSettingRecordWrapper{
        public Boolean include {
            get{
                if(include == null)
                    include = true;
                return include;
            }
            set;
        }
        public String importStatus {get;set;}
        public Sobject setting {get;set;}
        public Map<String, CustomSettingRecordAffiliateData> affilateDataMap {get;set;}
        public CustomSettingRecordWrapper(Sobject rec, CustomSettingManagerModule parent){
            setting = rec;
            affilateDataMap = new Map<String, CustomSettingRecordAffiliateData>();
            for(String fldName : parent.fields){
                affilateDataMap.put(fldName, new CustomSettingRecordAffiliateData(this, fldName));
            }
            for(String fldName : parent.queryableFields){
                affilateDataMap.put(fldName, new CustomSettingRecordAffiliateData(this, fldName,String.valueOf(setting.get(fldName)), '', ''));
            }
        }
    }
    public class CustomSettingRecordAffiliateData{
        CustomSettingRecordWrapper parent {get;set;}
        public Boolean isQueryable {get;set;}
        public Boolean isQueried {get;set;}
        public String settingField {get;set;}
        public String targetField {get;set;}
        public String sType {get;set;}
        public String value {
            get{
                if(isQueryable != null && !isQueryable){
                    return String.valueOf(parent.setting.get(settingField));
                }
                return value;
            }
            set{
            	if(isQueryable != null && !isQueryable){
                    parent.setting.put(settingField,value);
                }
                this.value = value;
            }
        }
        public CustomSettingRecordAffiliateData(CustomSettingRecordWrapper parent, String settingField, String value, String targetField, String sType){
            this.parent = parent;
            this.settingField = settingField;
            this.targetField = targetField;
            this.sType = sType;
            this.value = value;
            this.isQueryable = true;
            this.isQueried = false;
        }
        public CustomSettingRecordAffiliateData(CustomSettingRecordWrapper parent, String settingField){
            this.parent = parent;
            this.settingField = settingField;
            this.isQueryable = false;
            this.isQueried = false;
        }
    }
    public class CustomSettingException extends Exception{}
    public static Map<String, String> sObjectFieldConfiguration {get;set;}
    static{
        sObjectFieldConfiguration = new Map<String, String>();
        sObjectFieldConfiguration.put('User', 'Name');
        sObjectFieldConfiguration.put('ContentWorkspace', 'Name');
        sObjectFieldConfiguration.put('RecordType', 'DeveloperName');
        sObjectFieldConfiguration.put('Document', 'DeveloperName');
        sObjectFieldConfiguration.put('Folder', 'DeveloperName');
        sObjectFieldConfiguration.put('Pricebook2','Name');
        sObjectFieldConfiguration.put('PermissionSet','Name');
        sObjectFieldConfiguration.put('CollaborationGroup','Name');
        sObjectFieldConfiguration.put('Profile','Name');
        sObjectFieldConfiguration.put('Organization','Name');
    }
    public static Map<String, Schema.SObjectType> sobjectTypeConfiguration {get;set;}
    public static Map<String, Boolean> customSettingTypeConfiguration {get;set;}
    public static Map<Schema.SObjectType, List<String>> fieldConfiguration {get;set;}
    public static Map<Schema.SObjectType, List<String>> queryableFieldConfiguration {get;set;}
    static{
        sobjectTypeConfiguration = new Map<String, Schema.SObjectType>();
        //sobjectTypeConfiguration.put('CCSettings__c', CCSettings__c.SobjectType);
        sobjectTypeConfiguration.put('SalesAppSettings__c', SalesAppSettings__c.SobjectType);
        sobjectTypeConfiguration.put('PofsClickAreaTypes__c', PofsClickAreaTypes__c.SobjectType);
        sobjectTypeConfiguration.put('PofSDocumentsFolder__c', PofSDocumentsFolder__c.SobjectType);
        sobjectTypeConfiguration.put('PofSSettings__c',PofSSettings__c.SobjectType);
        sobjectTypeConfiguration.put('RedSurveyArticleReplacements__c',RedSurveyArticleReplacements__c.SobjectType);
        sobjectTypeConfiguration.put('WSDebugSettings__c',WSDebugSettings__c.SobjectType);
        
        customSettingTypeConfiguration = new Map<String, Boolean>();
        //customSettingTypeConfiguration.put('CCSettings__c', true);
        customSettingTypeConfiguration.put('SalesAppSettings__c', false);
        customSettingTypeConfiguration.put('PofsClickAreaTypes__c', false);
        customSettingTypeConfiguration.put('PofSDocumentsFolder__c', false);
        customSettingTypeConfiguration.put('PofSSettings__c',false);
        customSettingTypeConfiguration.put('RedSurveyArticleReplacements__c', false);
        customSettingTypeConfiguration.put('WSDebugSettings__c', false);
        
        queryableFieldConfiguration = new Map<Schema.SObjectType, List<String>>();
        //queryableFieldConfiguration.put(CCSettings__c.SobjectType, new List<String>{'SetupOwnerId'});
        queryableFieldConfiguration.put(SalesAppSettings__c.SobjectType, new List<String>{'Value__c'});
        queryableFieldConfiguration.put(PofsClickAreaTypes__c.SobjectType, new List<String>{'Icon__c'});
        queryableFieldConfiguration.put(PofSDocumentsFolder__c.SobjectType, new List<String>{'FolderId__c'});
        queryableFieldConfiguration.put(PofSSettings__c.SobjectType, new List<String>());
        queryableFieldConfiguration.put(WSDebugSettings__c.SobjectType, new List<String>{'Value__c'});
        queryableFieldConfiguration.put(RedSurveyArticleReplacements__c.SobjectType, new List<String>());
        
        fieldConfiguration = new Map<Schema.SObjectType, List<String>>();
        for(Schema.SObjectType sobjType : sobjectTypeConfiguration.values()){
            List<String> fields = new List<String>();
            Boolean isHierarchySetting = false;
            if(customSettingTypeConfiguration.containsKey(String.valueOf(sobjType))){
                isHierarchySetting = customSettingTypeConfiguration.get(String.valueOf(sobjType));
            }
            Map <String, Schema.SObjectField> fieldMap = sobjType.getDescribe().fields.getMap();
            for(Schema.SObjectField sField : fieldMap.Values()){
            	schema.describefieldresult dField = sField.getDescribe();
                if(dField.isCustom()){
                	fields.add(dField.getName());
                }else if(isHierarchySetting && dField.getName() == 'SetupOwnerId'){
                    fields.add(dField.getName());
                }else if(!isHierarchySetting && dField.getName() == 'Name'){
                    fields.add(dField.getName());
                }
            }
            fieldConfiguration.put(sobjType, fields);
        }
    }
    
    
}
