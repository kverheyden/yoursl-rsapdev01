/*
 * @(#)SCOrderExternalAssignmentControllerTest.cls 
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This class tests the functionality of the main class
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
 @isTest (SeeAllData = true)
private class SCOrderExternalAssignmentControllerTest
{
    private static SCOrderExternalAssignmentController oea;

    static testMethod void orderExternalAssignmentPositiv1() 
    {
        // Order
        SCHelperTestClass.createOrderTestSet(true);
        // oid
        ApexPages.currentPage().getParameters().put('oid', SCHelperTestClass.order.Id);
        // Controller
        SCOrderExternalAssignment__c c = new SCOrderExternalAssignment__c();
        ApexPages.StandardController sc = new ApexPages.StandardController(c);
        oea = new SCOrderExternalAssignmentController(sc);
        // Vendor
        SCVendor__c vendor = new SCVendor__c(Street__c     = 'Bahnhofstr.', 
                                             HouseNo__c    = '1', 
                                             PostalCode__c = '33100', 
                                             City__c       = 'Paderborn',
                                             Country__c    = 'DE', 
                                             Status__c     = 'Active', 
                                             Phone__c      = '0123456', 
                                             Name1__c      = 'New Test Vendor', 
                                             Mobile__c     = '01233435354', 
                                             ID2__c        = 'ABC00001',
                                             Name          = 'New Vendor 001');
        insert vendor;
        
        // Vendor Contract
        SCVendorContract__c vendorContract = new SCVendorContract__c(Vendor__c = vendor.id,
                                                                     ID2__c    = 'ABCD000001',
                                                                     Name      = 'New Vendor Contract 001');
        insert vendorContract;
                                                                     
        // Vendor Contract Item
        SCVendorContractItem__c vendorContractItem = new SCVendorContractItem__c(VendorContract__c = vendorContract.Id, 
                                                                                 Status__c = 'Active', 
                                                                                 Name      = 'New Vendor Contract Item 001', 
                                                                                 ID2__c    = 'VCI000001');                                                                         
        insert vendorContractItem;
        
        //  Vendor Contract Service
        SCVendorContractService__c vendorContractService = new SCVendorContractService__c(VendorContractItem__c = vendorContractItem.Id, 
                                                                                          Name = 'VCS001', 
                                                                                          IsSelected__c = true, 
                                                                                          ID2__c = 'AAAA00001', 
                                                                                          GrossPrice__c = 30.99);
        insert vendorContractService;       
        
        Test.startTest();
        
        oea.extAssignment.Vendor__c = vendor.id;
        oea.getListVendorContracts();
        oea.getListVendorContractItems();
        oea.getReadServices();
        oea.checkOrder();
        oea.save();
        oea.cancel();
        
        Test.stopTest();
    }
    
    static testMethod void orderExternalAssignmentNegative1() 
    {
        // Order
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.order.Status__c = '5506';
        upsert SCHelperTestClass.order;
        // oid
        //ApexPages.currentPage().getParameters().put('oid', SCHelperTestClass.order.Id);
        // Controller
        SCOrderExternalAssignment__c c = new SCOrderExternalAssignment__c();
        ApexPages.StandardController sc = new ApexPages.StandardController(c);
        oea = new SCOrderExternalAssignmentController(sc);
        // Vendor
        SCVendor__c vendor = new SCVendor__c(Street__c     = 'Bahnhofstr.', 
                                             HouseNo__c    = '1', 
                                             PostalCode__c = '33100', 
                                             City__c       = 'Paderborn',
                                             Country__c    = 'DE', 
                                             Status__c     = 'Active', 
                                             Phone__c      = '0123456', 
                                             Name1__c      = 'New Test Vendor', 
                                             Mobile__c     = '01233435354', 
                                             ID2__c        = 'ABC00001',
                                             Name          = 'New Vendor 001');
        insert vendor;
        
        // Vendor Contract
        SCVendorContract__c vendorContract = new SCVendorContract__c(Vendor__c = vendor.id,
                                                                     ID2__c    = 'ABCD000001',
                                                                     Name      = 'New Vendor Contract 001');
        insert vendorContract;
                                                                     
        // Vendor Contract Item
        SCVendorContractItem__c vendorContractItem = new SCVendorContractItem__c(VendorContract__c = vendorContract.Id, 
                                                                                 Status__c = 'Active', 
                                                                                 Name      = 'New Vendor Contract Item 001', 
                                                                                 ID2__c    = 'VCI000001');                                                                         
        insert vendorContractItem;
        
        //  Vendor Contract Service
        SCVendorContractService__c vendorContractService = new SCVendorContractService__c(VendorContractItem__c = vendorContractItem.Id, 
                                                                                          Name = 'VCS001', 
                                                                                          IsSelected__c = true, 
                                                                                          ID2__c = 'AAAA00001', 
                                                                                          GrossPrice__c = 30.99);
        insert vendorContractService;       
        
        Test.startTest();
        oea.extAssignment.Order__c = SCHelperTestClass.order.Id;
        oea.extAssignment.Vendor__c = vendor.id;
        oea.getListVendorContracts();
        oea.getListVendorContractItems();
        oea.getReadServices();
        oea.checkOrder();
        oea.save();

        
        Test.stopTest();
    }
    
    static testMethod void orderExternalAssignmentNegative2() 
    {
        // Order
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.order.Status__c = '5506';
        upsert SCHelperTestClass.order;
        // oid
        //ApexPages.currentPage().getParameters().put('oid', SCHelperTestClass.order.Id);
        // Controller
        SCOrderExternalAssignment__c c = new SCOrderExternalAssignment__c();
        ApexPages.StandardController sc = new ApexPages.StandardController(c);
        oea = new SCOrderExternalAssignmentController(sc);
        // Vendor
        SCVendor__c vendor = new SCVendor__c(Street__c     = 'Bahnhofstr.', 
                                             HouseNo__c    = '1', 
                                             PostalCode__c = '33100', 
                                             City__c       = 'Paderborn',
                                             Country__c    = 'DE', 
                                             Status__c     = 'Active', 
                                             Phone__c      = '0123456', 
                                             Name1__c      = 'New Test Vendor', 
                                             Mobile__c     = '01233435354', 
                                             ID2__c        = 'ABC00001',
                                             Name          = 'New Vendor 001');
        insert vendor;
        
        // Vendor Contract
        SCVendorContract__c vendorContract = new SCVendorContract__c(Vendor__c = vendor.id,
                                                                     ID2__c    = 'ABCD000001',
                                                                     Name      = 'New Vendor Contract 001');
        insert vendorContract;
                                                                     
        // Vendor Contract Item
        SCVendorContractItem__c vendorContractItem = new SCVendorContractItem__c(VendorContract__c = vendorContract.Id, 
                                                                                 Status__c = 'Active', 
                                                                                 Name      = 'New Vendor Contract Item 001', 
                                                                                 ID2__c    = 'VCI000001');                                                                         
        insert vendorContractItem;
        
        //  Vendor Contract Service
        SCVendorContractService__c vendorContractService = new SCVendorContractService__c(VendorContractItem__c = vendorContractItem.Id, 
                                                                                          Name = 'VCS001', 
                                                                                          IsSelected__c = true, 
                                                                                          ID2__c = 'AAAA00001', 
                                                                                          GrossPrice__c = 30.99);
        insert vendorContractService;       
        
        Test.startTest();

        oea.getCanProcesOrder();
        oea.oid = null;
        oea.cancel();
        /*
        SCHelperTestClass.order = null;
        oea.extAssignment.Order__c = null;
        oea.extAssignment.Vendor__c = null;
        oea.extAssignment = null;
        oea.save();
        */

        Test.stopTest();
    }
}
