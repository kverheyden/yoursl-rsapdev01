/*
 * @(#)SCCreditNoteCreationExtensionTest.cls
 *
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * 
 * @author Martin Möller <mmoeller@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCCreditNoteCreationExtensionTest
{
    /**
     * Test the simple credit note creation process
     */
    static testMethod void testCreateCreditNote()
    {   
        // create an order with order roles, lines and all 
        SCHelperTestClass.createOrderTestSet(true);

        // Now modify the order so that we can create a credit note
        SCHelperTestClass.order.type__c = SCfwConstants.DOMVAL_ORDERTYPE_DEFAULT;
        SCHelperTestClass.order.status__c = SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL;
        SCHelperTestClass.order.invoicingstatus__c = SCfwConstants.DOMVAL_INVOICING_STATUS_BOOKED;
        update SCHelperTestClass.order;

        Test.startTest();

        // Page 1 - selection of the mode
        
        // create the standard controller (SCOrder) that reads the order details
        ApexPages.StandardController controller = new ApexPages.StandardController(SCHelperTestClass.order);
        SCCreditNoteCreationExtension extension = new SCCreditNoteCreationExtension(controller);

        // we only want to create a creditnote - no invoice    
        extension.modeCreateInvoice = false;
        extension.onModeChanged(); 

        // the wizard must be initialized and set to 3 steps (for creating the credit note) plus one summary page ()
        System.assertEquals(3, extension.wiz.steps);    
        System.assertEquals(1, extension.wiz.step);    

        // the order must be initialized and available
        System.assert(extension.bo != null && extension.bo.order.id == SCHelperTestClass.order.id);

        // Page 2 - now go to the next page of the wizard    
        extension.OnNext();
        System.assertEquals(2, extension.wiz.step);    
 
        // we must have at least one order line       
        List<SCCreditNoteCreationExtension.Item> orderlines = extension.getItems();
        System.assert(orderlines != null && orderlines.size() > 0); 
        
        // enter the creditnote reason
        extension.data.CreditnoteReason__c = '378019'; // invoice splitting
        extension.data.Description__c = 'test';

        // all orderlines have to be selected as default
        integer cnt = extension.getTotalCount();
        System.assertEquals(cnt, extension.SelectedCount);

        // deselect all order lines        
        extension.selectedAll = false;
        extension.onCheckAll();
        System.assertEquals(0, extension.SelectedCount);
        
        extension.onCheckOne();
        
        // re-select all order lines        
        extension.selectedAll = true;
        extension.onCheckAll();
        System.assertEquals(cnt, extension.SelectedCount);

        // Page 3 - credit note confirmation page
        extension.OnNext();
        System.assertEquals(3, extension.wiz.step);    
        extension.getPriceLists();
        
        orderlines = extension.getItems();
        System.assert(orderlines != null && orderlines.size() > 0); 
        
        List<SCCreditNoteCreationExtension.OrderLineObj> olobj = extension.creditNoteOrdLineObjs;
        for(SCCreditNoteCreationExtension.OrderLineObj ol : olobj)
        {
            List<SelectOption> invtypes = ol.invTypes;
            List<SelectOption> invsubtypes = ol.invSubTypes;
            ol.getIsPriceInvalid();
        }

        // now create the credit note
        extension.onFinish();

        // Page 4 - summary age
        System.assertEquals(4, extension.wiz.step);    
        System.assert(extension.boCreditNote.order.id != null);
        System.assertEquals(extension.boCreditNote.order.OrderOrigin__c, SCHelperTestClass.order.Id);

        extension.onContinue();
        extension.onCancel();
        
        Test.stopTest();
    }
    
    /**
     * Test the advanced credit note creation process including a follow-up invoice
     */
    static testMethod void testCreateCreditNoteAndInvoice()
    {   
        // create an order with order roles, lines and all 
        SCHelperTestClass.createOrderTestSet(true);

        // Now modify the order so that we can create a credit note
        SCHelperTestClass.order.type__c = SCfwConstants.DOMVAL_ORDERTYPE_DEFAULT;
        SCHelperTestClass.order.status__c = SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL;
        SCHelperTestClass.order.invoicingstatus__c = SCfwConstants.DOMVAL_INVOICING_STATUS_BOOKED;
        update SCHelperTestClass.order;

        Test.startTest();

        // Page 1 - selection of the mode
        
        // create the standard controller (SCOrder) that reads the order details
        ApexPages.StandardController controller = new ApexPages.StandardController(SCHelperTestClass.order);
        SCCreditNoteCreationExtension extension = new SCCreditNoteCreationExtension(controller);

        // default mode modeCreateInvoice = true - create credit note and invoice

        // the wizard must be initialized and set to 3 steps (for creating the credit note) plus one confirmation page (step 4)
        System.assertEquals(4, extension.wiz.steps);    
        System.assertEquals(1, extension.wiz.step);    

        // the order must be initialized and available
        System.assert(extension.bo != null && extension.bo.order.id == SCHelperTestClass.order.id);

        // Page 2 - now go to the next page of the wizard    
        extension.OnNext();

        // enter the creditnote reason
        extension.data.CreditnoteReason__c = '378019';
        extension.data.Description__c = 'test';

        // Page 3 - credit note confirmation page
        extension.OnNext();
        System.assertEquals(3, extension.wiz.step);    

        // one step back and one forward
        extension.onPrev();
        System.assertEquals(2, extension.wiz.step);    
        extension.OnNext();
        System.assertEquals(3, extension.wiz.step);    

        // Page 4 - invoice creation page
        extension.OnNext();
        System.assertEquals(4, extension.wiz.step);    

        List<SelectOption> invtype = extension.getInvoiceTypes();
        List<SelectOption> invsubtype = extension.getInvoiceSubTypes();
        
        List<SCCreditNoteCreationExtension.OrderLineObj> olobj = extension.invoiceOrdLineObjs;
        for(SCCreditNoteCreationExtension.OrderLineObj ol : olobj)
        {
            List<SelectOption> invtypes = ol.invTypes;
            List<SelectOption> invsubtypes = ol.invSubTypes;
            ol.getIsPriceInvalid();
        }
        
        // now create the credit note
        extension.onFinish();

        // Page 5 - summary age
        System.assertEquals(5, extension.wiz.step);    
        System.assert(extension.boCreditNote.order.id != null);
        System.assertEquals(extension.boCreditNote.order.OrderOrigin__c, SCHelperTestClass.order.Id);
        System.assert(extension.boInvoice.order.id != null);
        System.assertEquals(extension.boInvoice.order.OrderOrigin__c, SCHelperTestClass.order.Id);

        Test.stopTest();
    }
    
    /**
     * Test the advanced credit note creation process including a follow-up invoice
     * with a specific credit note reason
     */
    static testMethod void testCreateCreditNoteAndInvoice2()
    {   
        // create an order with order roles, lines and all 
        SCHelperTestClass.createOrderTestSet(true);

        // Now modify the order so that we can create a credit note
        SCHelperTestClass.order.type__c = SCfwConstants.DOMVAL_ORDERTYPE_DEFAULT;
        SCHelperTestClass.order.status__c = SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL;
        SCHelperTestClass.order.invoicingstatus__c = SCfwConstants.DOMVAL_INVOICING_STATUS_BOOKED;
        update SCHelperTestClass.order;

        Test.startTest();

        // Page 1 - selection of the mode
        
        // create the standard controller (SCOrder) that reads the order details
        ApexPages.StandardController controller = new ApexPages.StandardController(SCHelperTestClass.order);
        SCCreditNoteCreationExtension extension = new SCCreditNoteCreationExtension(controller);

        // default mode modeCreateInvoice = true - create credit note and invoice

        // the wizard must be initialized and set to 3 steps (for creating the credit note) plus one confirmation page (step 4)
        System.assertEquals(4, extension.wiz.steps);    
        System.assertEquals(1, extension.wiz.step);    

        // the order must be initialized and available
        System.assert(extension.bo != null && extension.bo.order.id == SCHelperTestClass.order.id);

        // Page 2 - now go to the next page of the wizard    
        extension.OnNext();

        // enter the creditnote reason
        extension.data.CreditnoteReason__c = '378025';
        extension.data.Description__c = 'test';

        // Page 3 - credit note confirmation page
        /*extension.OnNext();
        System.assertEquals(3, extension.wiz.step);    

        // one step back and one forward
        extension.onPrev();
        System.assertEquals(2, extension.wiz.step);    
        extension.OnNext();
        System.assertEquals(3, extension.wiz.step);    

        // Page 4 - invoice creation page
        extension.OnNext();
        System.assertEquals(4, extension.wiz.step);    

        List<SelectOption> invtype = extension.getInvoiceTypes();
        List<SelectOption> invsubtype = extension.getInvoiceSubTypes();
        
        List<SCCreditNoteCreationExtension.OrderLineObj> olobj = extension.invoiceOrdLineObjs;
        for(SCCreditNoteCreationExtension.OrderLineObj ol : olobj)
        {
            List<SelectOption> invtypes = ol.invTypes;
            List<SelectOption> invsubtypes = ol.invSubTypes;
            ol.getIsPriceInvalid();
        }
        
        // now create the credit note
        extension.onFinish();

        // Page 5 - summary age
        System.assertEquals(5, extension.wiz.step);    
        System.assert(extension.boCreditNote.order.id != null);
        System.assertEquals(extension.boCreditNote.order.OrderOrigin__c, SCHelperTestClass.order.Id);
        System.assert(extension.boInvoice.order.id != null);
        System.assertEquals(extension.boInvoice.order.OrderOrigin__c, SCHelperTestClass.order.Id);

        Test.stopTest();*/
    }
    
    /**
     * Test the advanced credit note creation process including two follow-up invoices
     */
    static testMethod void testCreateCreditNoteAnd2Invoices()
    {   
        // create an order with order roles, lines and all 
        SCHelperTestClass.createOrderTestSet(true);

        // Now modify the order so that we can create a credit note
        SCHelperTestClass.order.type__c = SCfwConstants.DOMVAL_ORDERTYPE_DEFAULT;
        SCHelperTestClass.order.status__c = SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL;
        SCHelperTestClass.order.invoicingstatus__c = SCfwConstants.DOMVAL_INVOICING_STATUS_BOOKED;
        update SCHelperTestClass.order;

        Test.startTest();

        // Page 1 - selection of the mode
        
        // create the standard controller (SCOrder) that reads the order details
        ApexPages.StandardController controller = new ApexPages.StandardController(SCHelperTestClass.order);
        SCCreditNoteCreationExtension extension = new SCCreditNoteCreationExtension(controller);

        // we only want to create a creditnote and two invoices
        extension.splitInvoice = true;
        extension.onModeChanged(); 

        // the wizard must be initialized and set to 8 steps
        System.assertEquals(8, extension.wiz.steps);    
        System.assertEquals(1, extension.wiz.step);    

        // the order must be initialized and available
        System.assert(extension.bo != null && extension.bo.order.id == SCHelperTestClass.order.id);

        // Page 2 - now go to the next page of the wizard    
        extension.OnNext();

        // enter the creditnote reason
        extension.data.CreditnoteReason__c = '378027';
        extension.data.Description__c = 'test';

        // Page 3 - credit note confirmation page
        extension.OnNext();
        System.assertEquals(3, extension.wiz.step);    

        // Page 4 - invoice creation page
        extension.OnNext();
        System.assertEquals(4, extension.wiz.step);    
        extension.getInvoiceSelect();

        // one step back and one forward
        extension.onPrev();
        System.assertEquals(3, extension.wiz.step);    
        extension.OnNext();
        System.assertEquals(4, extension.wiz.step);    
        List<SCCreditNoteCreationExtension.Item> items = extension.getItems();
        System.assertEquals(2, items.size());
        items[0].selInvoice = '1';
        items[1].selInvoice = '2';

        // Page 5 - changes roles
        extension.OnNext();
        System.assertEquals(5, extension.wiz.step);
        List<SCfwOrderControllerBase.OrderRoleObj> roles = extension.invRoleObjList;
        extension.getRoleAccountIds();
        extension.getTitle();

        // Page 6 - changes lines
        extension.OnNext();
        System.assertEquals(6, extension.wiz.step);
        extension.getPriceLists();
        extension.calculatePricing();
        extension.reCalculatePricing();
        extension.applyInvoicingType();

        List<SelectOption> invtype = extension.getInvoiceTypes();
        List<SelectOption> invsubtype = extension.getInvoiceSubTypes();
        
        List<SCCreditNoteCreationExtension.OrderLineObj> olobj = extension.invoiceOrdLineObjs;
        for(SCCreditNoteCreationExtension.OrderLineObj ol : olobj)
        {
            List<SelectOption> invtypes = ol.invTypes;
            List<SelectOption> invsubtypes = ol.invSubTypes;
            ol.getIsPriceInvalid();
        }
        ApexPages.currentPage().getParameters().put('currentLineRow', '0.0');
        extension.removeLine();

        // Page 7 - changes roles
        extension.OnNext();
        System.assertEquals(7, extension.wiz.step);    
        roles = extension.inv2RoleObjList;
        extension.getRoleAccountIds();
        extension.getTitle();

        // one step back and one forward
        extension.onPrev();
        System.assertEquals(6, extension.wiz.step);    
        extension.OnNext();
        System.assertEquals(7, extension.wiz.step);    

        // Page 8 - changes lines
        extension.OnNext();
        System.assertEquals(8, extension.wiz.step);    
        extension.getPriceLists();
        extension.calculatePricing();
        extension.reCalculatePricing();
        extension.applyInvoicingType();

        invtype = extension.getInvoiceTypes();
        invsubtype = extension.getInvoiceSubTypes();
        
        olobj = extension.invoice2OrdLineObjs;
        for(SCCreditNoteCreationExtension.OrderLineObj ol : olobj)
        {
            List<SelectOption> invtypes = ol.invTypes;
            List<SelectOption> invsubtypes = ol.invSubTypes;
            ol.getIsPriceInvalid();
        }
        ApexPages.currentPage().getParameters().put('currentLineRow', '0.0');
        extension.removeLine();
        
        // now create the credit note
        extension.onFinish();

        // Page 5 - summary age
        System.assertEquals(9, extension.wiz.step);    
        System.assert(extension.boCreditNote.order.id != null);
        System.assertEquals(extension.boCreditNote.order.OrderOrigin__c, SCHelperTestClass.order.Id);
        System.assert(extension.boInvoice.order.id != null);
        System.assertEquals(extension.boInvoice.order.OrderOrigin__c, SCHelperTestClass.order.Id);
        System.assert(extension.boInvoice2.order.id != null);
        System.assertEquals(extension.boInvoice2.order.OrderOrigin__c, SCHelperTestClass.order.Id);

        Test.stopTest();
    }

    /**
     * Test the validation 
     */
    static testMethod void testValidation()
    {   
        // create an order with order roles, lines and all 
        SCHelperTestClass.createOrderTestSet(true);

        SCHelperTestClass.order.type__c = SCfwConstants.DOMVAL_ORDERTYPE_DEFAULT;
        SCHelperTestClass.order.status__c = SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL;
        SCHelperTestClass.order.invoicingstatus__c = SCfwConstants.DOMVAL_INVOICING_STATUS_BOOKED;
        update SCHelperTestClass.order;

        // Now modify the order so that we can create a credit note
        Test.startTest();

        // Page 1 - selection of the mode
        
        // create the standard controller (SCOrder) that reads the order details
        ApexPages.StandardController controller = new ApexPages.StandardController(SCHelperTestClass.order);
        SCCreditNoteCreationExtension extension = new SCCreditNoteCreationExtension(controller);

        // Validation of the order type
        extension.ord.type__c = SCfwConstants.DOMVAL_ORDERTYPE_CREDIT;
        extension.Validate();
        System.assertEquals(false, extension.canCreateCreditnote); 

        // Validation of the order status
        extension.ord.type__c = SCfwConstants.DOMVAL_ORDERTYPE_DEFAULT;
        extension.ord.status__c = SCfwConstants.DOMVAL_ORDERSTATUS_OPEN;
        extension.Validate();
        System.assertEquals(false, extension.canCreateCreditnote); 

        // Validation of the order status
        extension.ord.type__c = SCfwConstants.DOMVAL_ORDERTYPE_DEFAULT;
        extension.ord.status__c = SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL;
        extension.ord.invoicingstatus__c = SCfwConstants.DOMVAL_INVOICING_STATUS_OPEN;
        extension.Validate();
        System.assertEquals(false, extension.canCreateCreditnote); 

        // Validation of the invoicing status
        extension.ord.type__c = SCfwConstants.DOMVAL_ORDERTYPE_DEFAULT;
        extension.ord.status__c = SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL;
        extension.ord.invoicingstatus__c = SCfwConstants.DOMVAL_INVOICING_STATUS_BOOKED;
        extension.Validate();
        System.assertEquals(true, extension.canCreateCreditnote); 

        // page 2
        extension.ord.CreditNoteBy__c = null;
        extension.onNext();
        System.assertEquals(2, extension.wiz.step);
        
        // validation - credit note reason missing
        // canCreateCreditnote
        extension.data.CreditnoteReason__c = null;
        System.assertEquals(false, extension.validateNext());

        extension.data.CreditnoteReason__c = '378019'; // invoice splitting
        extension.data.Description__c = 'test';
        System.assertEquals(true, extension.validateNext());

        extension.selectedAll = false;
        extension.onCheckAll();
        System.assertEquals(false, extension.validateNext());
        extension.selectedAll = true;
        extension.onCheckAll();
        System.assertEquals(true, extension.validateNext());

        extension.ord.CreditNoteBy__c = SCHelperTestClass.users[0].Id;
        extension.Validate();
        System.assertEquals(false, extension.canCreateCreditnote); 

      // --> to cover all code pieces
        List<Object> invRoleObjList = extension.invRoleObjList;
        List<Object> roleObjList = extension.roleObjList;
        extension.getPriceLists();
        extension.calculatePricing();
        extension.reCalculatePricing();
        extension.delRole();
        // getTitle on wiz.step > 3
        extension.onFinish();
        extension.getTitle();
       //  <-- to cover all code pieces

        // page 3
        extension.onNext();
        // page 4
        extension.onNext();
        // page 3
        extension.onPrev();

        Test.stopTest();
    }
}
