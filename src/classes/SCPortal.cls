/*
 * @(#)SCPortal.cls    
 * 
 * Copyright 2014 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This is a main class of the portal implementation.
 *
 * @author Sergey Utko
 */
public with sharing virtual class SCPortal
{
	/*** General Portal Variables ***/
	// The user object of the currently logged-in user
	protected User currentUser;
	// Vendor / CDS user identificator
	public Boolean isVendor { get; set; }
	// Selector used in the dynamic SOQL query
	// It defines which clause should be taken: WHERE or AND
	// because some SOQL conditions are used dynamically and are depending on the 'isVendor' variable 
	public String whereOrAnd = ''; 
	// The ID of the currently logged vendor
	public Id vendorId;
	// The Vendor object (depends on the current user)
	public SCVendor__c vendor { get; set; }
	
	/*** Filter Variables ***/
	// A select-list of the filters depending on the current user (vendor) 
	public List<SelectOption> searchFilters { get; set; }
	// Selected filter
	public String selectedFilter { get; set; } 
	// Map for the loaded filters
	public Map<ID, SCSearchFilter__c> filters = new Map<ID, SCSearchFilter__c>();
	// New filter title
	public String newFilterTitle { get; set; }
	// String for the remane function
	public String renamedFilterTitle { get; set; }
	// Maximun amount of filters that user can have
	public static final Integer maxNumberOfFilters = 5; 
	// Filter object name that is in context (eg. SCORder__c, SCInstalledBase__c)
	public String filterObjectType;
	// Filter group that is in the context (Vendor Portal = 001, ...) 
	public String filterGroupId;
	// Standard filter title
	public String standardFilterTitle = 'Standardfilter';
		
	/*** CSV Upload Variables ***/
	// The uploaded CSV file itself
	public transient Blob fileBody { get; set; }
	// Processed list from the CSV to be displayed on the page
	public List<List<String>> csvList { get; set; }
	// Custom CSV text (input from the user into the textarea)
	public transient String csvUserInput { get; set; }
	// Delmiter for the CSV text
	public String delimiter { get; set;}
	// Map of imported fields
	public Map<Integer,String> importedFields { get; set; }
	// Developer Name of the doc
	public String docDevname = 'Muster_CSV_Datei_Verdor_Portal';
	
   /*
    * Standard constructor
  	*/
	public SCPortal()
	{
		delimiter = '\\t';
	}

	public String getCSVFileId()
	{
		try
		{
		    
			Document fileId =  [  Select Id
								  From Document 
								  Where DeveloperName = :docDevname];
			return String.valueOf(fileId.Id);
		}
		catch(Exception e)
		{
			addPageMessage(e, 'Kein Muster-Document im System Vorhanden!');
		}
		
		return null;
	}

   /*
    * Reading current user information
    * @return user object
  	*/
	public User readUser()
	{
		if(currentUser != null)
		{
			return currentUser;
		}
		else
		{
			try
			{
				currentUser = [Select Name, ERPSalesArea__c From User Where Id = :UserInfo.getUserId()];
			}
			catch(Exception e)
			{
				addPageMessage(e, Label.SC_msg_VP_UserQueryError);
				return null;
			}
		}
		
		return currentUser;
	}
		
   /*
    * Reading current vendor based on the user
  	*/
	public void readVendor()
	{
		if(currentUser != null)
		{	
			try
			{
				vendor = [Select SystemModstamp, Street__c, Status__c, PostalCode__c, Phone__c, Name1__c, 
								 Name, Mobile__c, LockType__c, Id, ID2__c, HouseNo__c, Fax__c, Email__c, Country__c, City__c 
						  From SCVendor__c
						  //Where User__c = :currentUser.Id
						  Where Id IN (Select Vendor__c From SCVendorUser__c Where User__c = :currentUser.Id)
						  Order By CreatedDate DESC Limit 1];
						  
				if(vendor != null)
				{
					isVendor = true;
				}
			}
			catch(Exception e)
			{
				
			}
		}
	} 
	
	public Boolean getReadVendorForHeader()
	{
		User u = readUser();
		if(currentUser != null)
		{	
			try
			{
				vendor = [Select SystemModstamp, Street__c, Status__c, PostalCode__c, Phone__c, Name1__c, 
								 Name, Mobile__c, LockType__c, Id, ID2__c, HouseNo__c, Fax__c, Email__c, Country__c, City__c 
						  From SCVendor__c
						  //Where User__c = :currentUser.Id
						  Where Id IN (Select Vendor__c From SCVendorUser__c Where User__c = :u.Id)
						  Order By CreatedDate DESC Limit 1];
						  
				if(vendor != null)
				{
					return true;
				}
			}
			catch(Exception e)
			{
				
			}
		}
		
		return false;
	}
	
   /*
    * Helper method for getting fields of the corresponding field-set of the given object
    * and putting them into the query string.
    * @param sObjectName	Name of the sObject from which the field-set should be taken
    * @param fieldSetName	The name of the field-set
    * @return Comma separated string with all fields from the field set
  	*/
	public String readFieldSet(String sObjectName, String fieldSetName)
	{
		String fieldsForQuery = 'Id, Name';
        
        for(Schema.FieldSetMember field : Schema.getGlobalDescribe().get(sObjectName).getDescribe().FieldSets.getMap().get(fieldSetName).getFields()) 
        {
         	if(field.getFieldPath() != 'Id' && field.getFieldPath() != 'Name')
         	{
         		fieldsForQuery += ', ' + field.getFieldPath();
         	}
        }
		
		return fieldsForQuery;
	}
	
   /**
    * Format the date from the page to the DB-Date-Format
    *
    * @param   Date to format
    * @param   Boolean variable used to define whether a start or end date need to be formatted.
    *          If true  - a start time 00:00:01 will be added (start of the day)
    *		   If false - an end of the day will be formatted 23:59:59
    * @return  Datetime as String
    */ 
    public String getFormat(Date dateToFormat, Boolean isStart)
    {
        Datetime newDate = Datetime.newInstance(dateToFormat.year(), dateToFormat.month(), dateToFormat.day(),0,0,0);
        String newDateFormat = '';
        if(isStart)
        {
        	newDateFormat = newDate.format('yyyy-MM-dd') + 'T00:00:01.000Z';
        }
        else
        {
        	newDateFormat = newDate.format('yyyy-MM-dd') + 'T23:59:59.000Z';
        }
        return newDateFormat;
    }
    public String getFormat(Date dateToFormat)
    {
    	return getFormat(dateToFormat,true);
    }

   /*
    * Generates current datetime to the excel format. Used on the excel page. 
    * @return Current datetime as formatted string
  	*/    
    public String getCurrentDateTimeFormatForExcel()
    {
    	Datetime dt = Datetime.NOW();
    	return dt.format('yyyy-MM-dd') + 'T' + String.valueOf(dt.hour()) + ':' + String.valueOf(dt.minute()) + ':' + String.valueOf(dt.second()) + 'Z';
    }
	
   /*
    * Helper method creates an error message to be displayed on the page.
    * @param e    		Exception object
    * @param errorText  The custom text for the error message 
    * @return Message object
  	*/
	public static void addPageMessage(Exception e, String messageText, ApexPages.Severity severity)
	{
		ApexPages.Message errorMessage;
		
		if(e != null)
		{
			String errAll = '<br/>' +
							'Error message: ' + e.getMessage() + '<br/>' + 
			 				'Stack trace: ' + e.getStackTraceString();
			 				
			errorMessage = new ApexPages.Message(severity, '<b style="color:#cc0000;">' + messageText + '</b>' + errAll);
		}
		else
		{
			errorMessage = new ApexPages.Message(severity, messageText);
		}
		
		ApexPages.addMessage(errorMessage);
	}
	public static void addPageMessage(Exception e, String messageText)
	{
		ApexPages.Severity severity = ApexPages.Severity.ERROR;
		addPageMessage(e, messageText, severity);
	}
	public static void addPageMessage(String messageText, ApexPages.Severity severity)
	{
		addPageMessage(null, messageText, severity);
	}
	public static void addPageMessage(String messageText)
	{
		ApexPages.Severity severity = ApexPages.Severity.INFO;
		addPageMessage(null, messageText, severity);
	}
	
   /*
    * This method defines the value of the 'whereOrAnd' variable for the use in a SOQL query
    * @return String WHERE or AND
  	*/
	public String checkWhereOrAnd()
	{
		if(String.isBlank(whereOrAnd))
		{
			whereOrAnd = ' Where ';
		}
		else
		{
			whereOrAnd = ' And ';
		}
		
		return whereOrAnd;
	}
	
	/******************** Filter Methods ********************/
	
   /*
    * Assign selected filter values to the variables
  	*/
	public virtual void assignFilter(SCSearchFilter__c filter)
	{
	}
	
   /*
    * Read all filters objects for the current user for the current context (group) and object
  	*/
	public void readFilters()
	{
		try
		{
			// Selecting search filters of the currently logged-in vendor
			// Group: 001 > Vendor Portal			
			filters = new Map<Id, SCSearchFilter__c>([Select Name, Id, Title__c, Object__c, Filter__c, Default__c, User__c
													    From SCSearchFilter__c
													   Where User__c   = :currentUser.id
													     And Object__c = :filterObjectType
													     And Group__c  = :filterGroupId
												    Order By CreatedDate ASC]);
			
			// Creating a list of user filters to be displayed on the page 									  
			if(!filters.isEmpty())
			{		
		        for(SCSearchFilter__c filter : filters.values())
		        {	            
		            // Setting default filter values to variables
		            if(filter.Default__c)
		            {
						assignFilter(filter);
		            }
		        }
			}
		}
		catch(Exception e)
		{
			addPageMessage(e, Label.SC_msg_VP_FilterQueryError);
		}
	}
	
   /*
    * This method loads filters based on the current vendor,
    * object and group. After that fills the option-list with those values.
  	*/
	public List<SelectOption> getLoadFilters()
	{
		searchFilters = new List<SelectOption>();
		
		try
		{
			if(filters.isEmpty())
			{
				readFilters();
			}
			
			if(!filters.isEmpty())
			{		
		        for(SCSearchFilter__c filter : filters.values())
		        {
		            searchFilters.add(new SelectOption(filter.id, filter.Title__c));
		        }
			}
		}
		catch(Exception e)
		{
			addPageMessage(e, Label.SC_msg_VP_FilterQueryError);
		}
		
		return searchFilters;
	}
	
   /*
    * This function runs if the user changes the filter on the page
  	*/
	public void switchFilter()
	{
		try
		{
			// First removing a default flag from existing filter
			removeDefaultFlagFilter();
			// Then setting a default flag to the selected filter
			setDefaultFlagFilter(filters.get(selectedFilter));
			// And finally asigning filter values to the variables
			assignFilter(filters.get(selectedFilter));
		}
		catch(Exception e)
		{
			addPageMessage(e, Label.SC_msg_VP_FilterSwitchError);
		}
	}
	
   /*
    * Checks whether the current vendor have any filter entries.
    * If not - creates a new default filter entry.
  	*/
	public void checkDefaultFilter()
	{
		try
		{
			// No filters founded - the user visits the page for the very first time. 
			// Creating a default filter for this user.
			List<SCSearchFilter__c> tmpFilters = [Select Id 
												    From SCSearchFilter__c
												   Where User__c   = :currentUser.id
													 And Object__c = :filterObjectType
													 And Group__c  = :filterGroupId
												Order By CreatedDate ASC];
			
			if(tmpFilters.isEmpty())
			{
				createDefaultFilter();
			}
		}
		catch(Exception e)
		{
			addPageMessage(e, Label.SC_msg_VP_FilterCreationError);
		}
	}
	
   /*
    * Created default filter entry for the current user (vendor) with predefined values.
  	*/
	public void createDefaultFilter()
	{
		Map<String,String> tmpFilters = new Map<String,String>();
		
		try
		{
			SCSearchFilter__c defaultFilter = new SCSearchFilter__c(Default__c  = true,
																	User__c     = currentUser.id,
																	Title__c    = standardFilterTitle,
																	Object__c   = filterObjectType,
																	Group__c    = filterGroupId,
																	Filter__c   = '');

			insert defaultFilter;
		}
		catch(Exception e)
		{
			addPageMessage(e, Label.SC_msg_VP_FilterCreationError);
		}
	}
	
   /*
    * Creates a new filter object.
  	*/
	public void createNewFilter()
	{
		if(filters.values().size() < maxNumberOfFilters)
		{
			try
			{	
				// First removing a default flag from existing filter.
				// A new filter will get this flag.
				removeDefaultFlagFilter();
				
				if(String.isBlank(newFilterTitle))
				{
					newFilterTitle = 'Filter (' + DateTime.now().format('dd-MM-yyyy HH:mm:ss') + ')';
				}
				
				SCSearchFilter__c newFilter = new SCSearchFilter__c(Default__c  = true,
																	User__c     = currentUser.id,
																	Title__c    = String.escapeSingleQuotes(newFilterTitle),
																	Object__c   = filterObjectType,
																	Group__c    = filterGroupId,
																	Filter__c   = generateJSONFilter());
			
			
			
				insert newFilter;
				
				// Inserting the new filter to the map to be able display it on the page.
				filters.put(newFilter.Id, newFilter);
				
				// Setting selected filter variable to the new filter ID
				selectedFilter = newFilter.Id;
	
			}
			catch(Exception e)
			{
				addPageMessage(e, Label.SC_msg_VP_FilterUpdateError);
			}
		}
		else
		{
			addPageMessage(Label.SC_msg_VP_MaxFilter + ' ' + maxNumberOfFilters + ' Filter anlegen.', ApexPages.Severity.WARNING);
		}
	}
	
   /*
    * Removes a default flag from the default filter (Default__c = false)
  	*/
	public void removeDefaultFlagFilter()
	{
		if(filters != null && !filters.isEmpty())
		{
			for(SCSearchFilter__c f : filters.values())
		    {
		    	if(f.Default__c)
		    	{
		    		f.Default__c = false;
		    		update f;
		    		break;
		    	}	
		    }
		}
	}
	
   /*
    * Sets a default flag to the given filter (Default__c = true)
  	*/
	public void setDefaultFlagFilter(SCSearchFilter__c filter)
	{
		if(filter != null && !filter.Default__c)
		{
    		filter.Default__c = true;
    		update filter;
		}
	}
	
   /*
    * Renames current filter (sets user-defined title)
  	*/
	public void filterRename()
	{
		if(String.isBlank(renamedFilterTitle))
		{
			renamedFilterTitle = 'Filter ' + DateTime.now().format('dd-MM-yyyy HH:mm:ss');
		}
		
		try
		{
			if(!filters.isEmpty())
			{				
				SCSearchFilter__c updatedFilter = filters.get(selectedFilter);
				updatedFilter.Title__c = String.escapeSingleQuotes(renamedFilterTitle);
			
				update updatedFilter;
			}
		}
		catch(Exception e)
		{
			addPageMessage(e, Label.SC_msg_VP_FilterUpdateError);
		}
	}
	
   /*
    * Updates the current filter object. 
    * Runs if the user clicks on the "Assign filter" button.
    * This function updates the Filter__c field with the current filter variables.
  	*/
	public void filterUpdate()
	{		
		try
		{
			if(!filters.isEmpty())
			{				
				SCSearchFilter__c updatedFilter = filters.get(selectedFilter);
				updatedFilter.Filter__c = generateJSONFilter(); //JSON.serialize(params);
			
				update updatedFilter;
			}
		}
		catch(Exception e)
		{
			addPageMessage(e, Label.SC_msg_VP_FilterUpdateError);
		}
	}
	
   /*
    * This method deletes the current filter from the database.
    * Then the first filter in the list will be assigned.
  	*/
	public void filterDelete()
	{
		try
		{
			if(!filters.isEmpty())
			{				
				if(filters.size() > 1)
				{
					SCSearchFilter__c filterToDelete = filters.get(selectedFilter);
					filters.remove(selectedFilter);
					delete filterToDelete;
					
					List<Id> filterIds = new List<Id>();
					filterIds.addAll(filters.keySet());
					selectedFilter = filterIds.get(0);
					
					setDefaultFlagFilter(filters.get(selectedFilter));			 
					assignFilter(filters.get(selectedFilter));
				}
				else
				{
					addPageMessage(Label.SC_msg_VP_LastFilterRemovalError, ApexPages.Severity.WARNING);
				}
			}
		}
		catch(Exception e)
		{
			addPageMessage(e, Label.SC_msg_VP_FilterDeleteError);
		}
	}
	
   /*
    * Generates a JSON string from all filter fields.
    * This string will be saved to the Filter__c field of the SCSearchFilter_c object
    * @return JSON string
  	*/
	public virtual String generateJSONFilter()
	{
		return null;
	}
	
	
	/******************** CSV Upload & Process Methods ********************/

   /*
    * Gets the uploaded file and sends it to the "parseCSV(...)" method for the process
  	*/	
	/*
	public void uploadCSV()
	{
		if(fileBody != null)
		{		
			try
			{
				csvList = parseCSV(fileBody.toString(), false);
			}
			catch(Exception e)
			{
				String s1 = EncodingUtil.convertToHex(fileBody);
				Blob b = HexToUTF(s1);
				csvList = parseCSV(b.toString(), false);
			}
		}
	}
	*/
	
	public void processCSVText()
	{
		if(String.isNotBlank(csvUserInput))
		{
			csvList = parseCSV(csvUserInput, false);
		}
	}
	
   /*
    * Process the CSV data.
    *
    * @param  contents		CSV data
    * @param  skipHeaders	if true the method returns no header row (the first array element will removed)
    * @return list of lists of strings
  	*/
	public List<List<String>> parseCSV(String contents, Boolean skipHeaders) 
	{
		if(String.isBlank(delimiter))
		{
			delimiter = ';';
		}
		
		List<List<String>> allFields = new List<List<String>>();
	
		// replace instances where a double quote begins a field containing a comma
		// in this case you get a double quote followed by a doubled double quote
		// do this for beginning and end of a field
		contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
		// now replace all remaining double quotes - we do this so that we can reconstruct
		// fields with commas inside assuming they begin and end with a double quote
		contents = contents.replaceAll('""','DBLQT');
		// we are not attempting to handle fields with a newline inside of them
		// so, split on newline to get the spreadsheet rows
		List<String> lines = new List<String>();
		try 
		{
			lines = contents.split('\n');
		} 
		catch (System.ListException e) 
		{
			System.debug('Limits exceeded?' + e.getMessage());
		}
		
		Integer num = 0;
				
		for(String line : lines) 
		{
			// check for blank CSV lines (only commas)
			if (line.replaceAll(delimiter,'').trim().length() == 0)
			{
				break;
			}
			
			List<String> fields = line.split(delimiter);	
			List<String> cleanFields = new List<String>();
			String compositeField;
			Boolean makeCompositeField = false;
			
			for(String field : fields) 
			{
				if (field.startsWith('"') && field.endsWith('"')) 
				{
					cleanFields.add(field.replaceAll('DBLQT','"'));
				} 
				else if (field.startsWith('"')) 
				{
					makeCompositeField = true;
					compositeField = field;
				} 
				else if (field.endsWith('"')) 
				{
					compositeField += ',' + field;
					cleanFields.add(compositeField.replaceAll('DBLQT','"'));
					makeCompositeField = false;
				} 
				else if (makeCompositeField) 
				{
					compositeField +=  ',' + field;
				} 
				else 
				{
					cleanFields.add(field.replaceAll('DBLQT','"'));
				}
			}
			
			allFields.add(cleanFields);
		}
		
		if (skipHeaders)
		{
			allFields.remove(0);
		}
		
		return allFields;		
	}

	// convert Hex to UTF-8 (now works with multibyte locales!)
	/*
	private static Map<String, Integer> hexMap = new Map<String, Integer>();
	static
	{
	  hexMap.put('0', 0);
	  hexMap.put('1', 1);
	  hexMap.put('2', 2);
	  hexMap.put('3', 3);
	  hexMap.put('4', 4);
	  hexMap.put('5', 5);
	  hexMap.put('6', 6);
	  hexMap.put('7', 7);
	  hexMap.put('8', 8);
	  hexMap.put('9', 9);
	  hexMap.put('A', 10);
	  hexMap.put('B', 11);
	  hexMap.put('C', 12);
	  hexMap.put('D', 13);
	  hexMap.put('E', 14);
	  hexMap.put('F', 15);
	  hexMap.put('a', 10);
	  hexMap.put('b', 11);
	  hexMap.put('c', 12);
	  hexMap.put('d', 13);
	  hexMap.put('e', 14);
	  hexMap.put('f', 15);
	}
	
	public class UTFException extends Exception
	{
	}
	
	public static List<Integer> hexToInt(String hex) 
	{
	  List <Integer> retVal = new List<Integer> ();
	  Integer i = 0;
	  while(i < hex.length()) 
	  {
	    // http://en.wikipedia.org/wiki/UTF-8
	    integer numberOfBytes = 1;
	    integer byte1 = 0,
	    byte2 = 0,
	    byte3 = 0,
	    byte4 = 0;
	    integer utfCode = 0;
	    byte1 = (hexMap.get(hex.substring(i, i + 1)) * 16) + (hexMap.get(hex.substring(i + 1, i + 2)));
	
	    //invalid sequences for byte1
	    if(byte1 >= 128 && byte1 <= 191) 
	    {
	      throw new UTFException('UTF-8:Continuation byte as first byte');
	    }
	    if(byte1 >= 192 && byte1 <= 193) 
	    {
	      throw new UTFException('UTF-8:Invalid 2-byte sequence');
	    }
	    if(byte1 >= 245) 
	    {
	      throw new UTFException('UTF-8:Invalid 4,5 or 6-byte sequence');
	    }
	
	    if(byte1 >= 192) 
	    {
	      numberOfBytes = 2;
	      byte2 = (hexMap.get(hex.substring(i + 2, i + 2 + 1)) * 16) + (hexMap.get(hex.substring(i + 2 + 1, i + 2 + 2)));
	    }
	    if(byte1 >= 224) 
	    {
	      numberOfBytes = 3;
	      byte3 = (hexMap.get(hex.substring(i + 4, i + 4 + 1)) * 16) + (hexMap.get(hex.substring(i + 4 + 1, i + 4 + 2)));
	    }
	    if(byte1 >= 240) 
	    {
	      numberOfBytes = 4;
	      byte4 = (hexMap.get(hex.substring(i + 6, i + 6 + 1)) * 16) + (hexMap.get(hex.substring(i + 6 + 1, i + 6 + 2)));
	    }
	    if(numberOfBytes == 1) 
	    {
	      utfCode = byte1;
	    } 
	    else if(numberOfBytes == 2) 
	    {
	      utfCode = Math.mod(byte1, 32) * 64 + Math.mod(byte2, 64);
	    } 
	    else if(numberOfBytes == 3) 
	    {
	      utfCode = Math.mod(byte1, 16) * 64 * 64 + Math.mod(byte2, 64) * 64 + Math.mod(byte3, 64);
	    } 
	    else if(numberOfBytes == 4) 
	    {
	      utfCode = Math.mod(byte1, 8) * 64 * 64 * 64 + Math.mod(byte2, 64) * 64 * 64 + Math.mod(byte3, 64) * 64 + Math.mod(byte4, 64);
	    }
	
	    retVal.add(utfCode);
	    i += 2 * numberOfBytes;
	  }
	  return retVal;
	}
	
	public static Blob HexToUTF(string hex) 
	{
	  string text = String.fromCharArray(hexToInt(hex));
	  return Blob.valueOf(text);
	}
	*/

}
