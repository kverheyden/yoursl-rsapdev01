/*
 * @(#)SCStockProfileAssocExtension.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCStockProfileAssocExtension extends SCMatMoveBaseExtension
{
    public SCStock__c stock;
    public List<StockProfileAssoc> stockProfileAssocs { get; set;}

    public Boolean savingOk { get; private set; }

    /**
     * Reads the material movements for one article of a stock.
     *
     * @param    controller    the standard controller
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCStockProfileAssocExtension(ApexPages.StandardController controller)
    {
        this.stock = (SCStock__c)controller.getRecord();
        this.savingOk = false;
        
        // now create and fill the list for displaying in the page
        stockProfileAssocs = new List<StockProfileAssoc>(); 
        createProfileAssocList(stockProfileAssocs, stock.Id);
    } // SCStockProfileAssocExtension
    
    /**
     * Saves the changes made at the assigments.
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference saveAssocs()
    {
        System.debug('#### saveAssocs()');

        this.savingOk = correctProfileAssocs(stockProfileAssocs, stock.Id);
        
        PageReference curPage = ApexPages.currentPage();
        curPage.getParameters().put('success', 'true');
        curPage.getParameters().put('id', ApexPages.currentPage().getParameters().get('id'));
        curPage.setRedirect(true);
        return curPage;
    } // saveAssocs
} // SCStockProfileAssocExtension
