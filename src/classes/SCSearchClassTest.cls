/*
 * @(#)SCSearchClassTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCSearchClassTest
{
    /**
     * testAccountUpdate
     */       
    static testMethod void TestA()
    {
        Test.startTest();
        
        SCSearchClass obj = new SCSearchClass();
        obj.doSearch();
        obj.search = 'sc';
        obj.doSearch();
        obj.radioSel = '1';
        obj.doSearch();
        obj.days = null;
        obj.resource.Employee__c = null;
        obj.doSearch();
        System.assertEquals(obj.getorgid(),UserInfo.getOrganizationId());
        
        Test.stopTest();
    }
}
