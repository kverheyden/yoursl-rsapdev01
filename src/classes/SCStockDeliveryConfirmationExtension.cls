public with sharing class SCStockDeliveryConfirmationExtension 
{
    String stockid;
    public SCStock__c stock {get; set;}
    public String selecteddelivery  {get; set;}
    public List<SCMaterialMovement__c> movements {get; set;}
    public List<Delivery> deliveries {get; set;}
    

   /*
    * Constructs the extension and reads the pending material movements
    * of type "Delivery" with the status "in transit".
    */
    public SCStockDeliveryConfirmationExtension(ApexPages.StandardController controller) 
    {
        selecteddelivery = '*';
        // Checking whether the Stock-ID param exists in the URL string
        if(ApexPages.currentPage().getParameters().containsKey('sid') && ApexPages.currentPage().getParameters().get('sid') != '')
        {
            // Reading the Stock-ID
            stockid = ApexPages.currentPage().getParameters().get('sid');
            
            if(stockid != null)
            {
                // Reading the current stock
                stock = [Select Id, Name, Locked__c, Plant__r.Name From SCStock__c Where id = :stockid];
                
                // read the material movements of the type delivery that are "in transit"
                readMovements();
            }
        }
        else
        {
            stockid = null;
            // messages = System.Label.SC_msg_PleaseEnterInformation + ': ' + sObjectType.SCStock__c.label;
        }    
    }
    
   /*
    * Read the delivery material movements that have to be confirmed. 
    * The deliveries are grouped by the delivery note number.
    * @param  stockid     the id of the stock
    * @return deliveries  list of deliveries (grouped by delivery note number)
    */
    void readMovements()
    {
        deliveries = new List<Delivery>();
        movements  = new List<SCMaterialMovement__c>();
        if(stockid != null)
        {
        
            movements = [select id, name, Stock__c, 
                            article__c, valuationtype__c, qty__c, serialno__c, 
                            article__r.ArticleNameCalc__c, status__c,
                            DeliveryNote__c, DeliveryQty__c, DeliveryStock__c, DeliveryStock__r.plant__r.name, DeliveryDate__c,
                            Replenishment__c, ReplenishmentType__c, RequisitionDate__c, 
                            Order__c, OrderLine__c, ProcessDate__c
                         from SCMaterialMovement__c where stock__c = :stockid 
                            and type__c = '5207'   // delivery
                            and status__c = '5423' // in transit
                            order by DeliveryNote__c, article__r.name
                            ];
            if(movements == null)
            {
                movements  = new List<SCMaterialMovement__c>();
            }
            else
            {                
                Integer i = 0;
                Delivery d = new Delivery();
                d.note = 'none';
                for(SCMaterialMovement__c m : movements)
                {
                    if(d.note != m.DeliveryNote__c)
                    {
                        d = new Delivery();
                        
                        d.note = m.DeliveryNote__c;
                        d.confirmed = false;
                        d.idx = i;
                        deliveries.add(d);
                        
                        i++;
                    }
                    // calculate the total number of items that are confirmed
                    if(m.qty__c != null)
                    {
                        d.sumqty += m.qty__c;
                    }
                    if(m.DeliveryQty__c != null)
                    {
                        d.sumdeliveryqty += m.DeliveryQty__c;
                    }
                    d.mov.add(m);
                }    
            }
        } // if(stockid..
    }

   /*
    * Called when the command link "Confirm package x [the delivery note no]" is pressed
    * Initiates the booking of the delivered parts and updates the status to "booked"
    */    
    public PageReference onConfirm()
    {
        String stridx = ApexPages.currentPage().getParameters().get('dnote');
        if(stridx != null)
        {
            Integer idx = Integer.valueof(stridx);
            
            if(bookDelivery(deliveries[idx]))
            {
                deliveries[idx].confirmed = true;
            }
        } 
        return null;
    }
    
   /*
    * Returns to the stock.    
    */
    public PageReference onCancel()
    {
        return new PageReference('/' + stockid);
    }

   /*
    * Books the items in the following form:
    * - Call the web service WSMaterialDelivery to book the delivery to the stock items
    * - update the material movement status to "booked"    
    */    
    Boolean bookDelivery(Delivery d)
    {
    
        List<WSMaterialDelivery.DeliveryData> result; 
        try
        {
            // 1. book the quantity into the stockitems
            List<WSMaterialDelivery.DeliveryData> wsitems = new List<WSMaterialDelivery.DeliveryData>();        
            for(SCMaterialMovement__c m : d.mov)
            {
                WSMaterialDelivery.DeliveryData wsitem = new WSMaterialDelivery.DeliveryData();
                wsitem.stockiD         = stockid;
                wsitem.articleid       = m.article__c;
                wsitem.valuationtype   = m.valuationtype__c;
                wsitem.type            = '5227';    // receipt confirmation
                wsitem.deliverydate    =  date.today();
                wsitem.deliverynoteno  = m.DeliveryNote__c;
                wsitem.matmoveid       = m.id;
                wsitem.qty             = m.qty__c;
                wsitem.serialno        = m.serialno__c;
                            
                wsitems.add(wsitem);
            }
            // Book direkt to the stock                   
            WSMaterialDelivery.processinternal('BOOK', wsitems, false); // no delivery confirmation

            
            // 2. Create the material delivery confirmation records (used to inform SAP to book the data from "in transit" into the engineer stock (315)
            List<SCMaterialMovement__c> mmlist = new List<SCMaterialMovement__c>(); 

            for(SCMaterialMovement__c m : d.mov)
            {
                SCMaterialMovement__c mreceipt = new SCMaterialMovement__c();
                mreceipt.Stock__c         = stockid;
                mreceipt.Type__c          = '5227'; // Material receipt information
                mreceipt.Status__c        = '5408'; // booked
                mreceipt.Article__c       = m.Article__c;
                mreceipt.ValuationType__c = m.ValuationType__c;
                mreceipt.Qty__c           = m.qty__c;
                mreceipt.DeliveryQty__c   = m.DeliveryQty__c;
                mreceipt.DeliveryStock__c = stockid; // the stock must be she same to allow the booking in SAP of in transit into the stock!
                mreceipt.DeliveryDate__c  = m.DeliveryDate__c;
                mreceipt.DeliveryNote__c  = m.DeliveryNote__c;
                mreceipt.Order__c         = m.Order__c;
                mreceipt.OrderLine__c     = m.OrderLine__c;
                mreceipt.serialno__c      = m.serialno__c;
                mreceipt.ERPStatus__c = 'none';
                            
                mmlist.add(mreceipt);

            }            
            insert mmlist;
            
            List<String> movementIDList = new List<String>();
            for(SCMaterialMovement__c m : mmlist)
            {
                // List of ID of the material movements. 
                // Used for steering the CCWCMaterialMovementCreate callout
                movementIDList.add(m.ID);
            }
            
            CCWCMaterialMovementCreate.callout(movementIDList, true, false);
                

            // 3. create a material movement "5232 sparepart package received" 
            SCMaterialMovement__c mmpac = new SCMaterialMovement__c();
            mmpac.Stock__c  = stockid;
            mmpac.Type__c   = '5232'; // sparepart package reveived
            mmpac.Status__c = '5408'; // booked
            mmpac.Qty__c          = d.sumqty;            // the total number of parts confirmed
            mmpac.DeliveryQty__c  = d.sumdeliveryqty;    // the total number of parts delivered
            mmpac.DeliveryNote__c = d.note;
            mmpac.ERPStatus__c = 'none';                 // still to be booked
            mmpac.Article__c = null;                     // no article reference
                
            insert mmpac;
            
            // 4. now call the spare package received interface to inform SAP that the package is received
            CCWCMaterialPackageReceived.callout(mmpac.id, true, false);

            // 5. update the status in memory only (alread done on db level by the wsmaterialdelivery)
            for(SCMaterialMovement__c m : d.mov)
            {
                m.status__c = '5405'; // delivered            
            }
            
            return true;
        }
        catch(Exception e)
        {
        }        
        return false;
    }

 
    public class Delivery
    {
        public Delivery()
        {
            mov = new List<SCMaterialMovement__c>();
            sumqty = 0.0;
            sumdeliveryqty = 0.0;
        }
        public Integer idx {get; set;}    
        public String note {get; set;}
        public Decimal sumqty {get; set;}
        public Decimal sumdeliveryqty {get; set;}
        public List<SCMaterialMovement__c> mov {get; set;}
        public boolean confirmed {get; set;}
    }    

}
