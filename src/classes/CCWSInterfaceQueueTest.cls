/*
 * @(#)CCWSInterfaceQueueTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Test class checking the calculating a due date, price list for maintenances
 * @version $Revision$, $Date$
 */
@isTest 
private class CCWSInterfaceQueueTest 
{
    private static Boolean seeAllData = CCWCTestBase.isSeeingAllData();



    public static testMethod void test1() 
    {
        Test.StartTest();
        String data = '{"References":{"References":{"Reference":[{"ReferenceID":"001100007004","IDType":"Order Number","ExternalID":"ORD-0000027918"},{"ReferenceID":"000010090180","IDType":"Notification No","ExternalID":"ORD-0000027918"},{"ReferenceID":"0000000112200329","IDType":"IDoc number","ExternalID":null}]},"Operation":"CloseOrder","ExternalID":"ORD-0000027918"},"MessageHeader":{"TestDataIndicator":false,"SenderBusinessSystemID":"SAP-QPI","ReferenceUUID":null,"ReferenceID":"CP20140319124528333-259234","RecipientBusinessSystemID":"SFCPDE-CCEAGQA","MessageUUID":"5324C61379020C40E10080000A6210B7","MessageID":"5324C61179020C40E10080000A6210B7","CreationDateTime":"2014-03-19T11:43:13.000Z"},"Log":{"MaximumLogItemSeverityCode":"3","Item":[{"WebURI":null,"TypeID":"032(RU)","SeverityCode":"3","Note":"Arbeitsplatz DE166268 in Werk 0356 nicht vorhanden (Bitte Eingabe prÃ¼fen)","ExternalID":null},{"WebURI":null,"TypeID":"085(IW)","SeverityCode":"1","Note":"Auftrag 1100007004 mit Meldung 10090180 gesichert","ExternalID":null}]}}';
		Exception error = new SCfwException('Test Error');       
        SCfwInterfaceRequestPendingException perr = new SCfwInterfaceRequestPendingException('Test Pending Error'); 
        
        CCWSInterfaceQueue.queueAdd('CCWCMaterialMovementCreate',data);
        CCWSInterfaceQueue.queueAdd('CCWSEquipmentChange',data);
        CCWSInterfaceQueue.queueAdd('ReprocessingRequest',data,error);
        CCWSInterfaceQueue.queueAdd('CCWSMaterialStockChange',data,perr);
               
        //List<SCInterfaceQueue__c> items = [select id, name from SCInterfaceQueue__c];
        
        String res = CCWSInterfaceQueue.queueProcess('CCWCMaterialMovementCreate', 0, 'test');
        Test.stopTest();
         
        System.assertNotEquals(res, null);
        
       
    }   

 
    public static testMethod void test2() 
    {
        Test.StartTest();
        
       	SCInterfaceQueue__c item = createQueueEntry();

               
        //List<SCInterfaceQueue__c> items = [select id, name from SCInterfaceQueue__c];
        
        String res = CCWSInterfaceQueue.execute(item.id);
        Test.stopTest();
        
        System.assertNotEquals(res, null);

    }


    /*public static testMethod void test3() 
    {
        Test.StartTest();
        

               
       String data = '{"References":{"References":{"Reference":[{"ReferenceID":"001100007004","IDType":"Order Number","ExternalID":"ORD-0000027918"},{"ReferenceID":"000010090180","IDType":"Notification No","ExternalID":"ORD-0000027918"},{"ReferenceID":"0000000112200329","IDType":"IDoc number","ExternalID":null}]},"Operation":"CloseOrder","ExternalID":"ORD-0000027918"},"MessageHeader":{"TestDataIndicator":false,"SenderBusinessSystemID":"SAP-QPI","ReferenceUUID":null,"ReferenceID":"CP20140319124528333-259234","RecipientBusinessSystemID":"SFCPDE-CCEAGQA","MessageUUID":"5324C61379020C40E10080000A6210B7","MessageID":"5324C61179020C40E10080000A6210B7","CreationDateTime":"2014-03-19T11:43:13.000Z"},"Log":{"MaximumLogItemSeverityCode":"3","Item":[{"WebURI":null,"TypeID":"032(RU)","SeverityCode":"3","Note":"Arbeitsplatz DE166268 in Werk 0356 nicht vorhanden (Bitte Eingabe prÃ¼fen)","ExternalID":null},{"WebURI":null,"TypeID":"085(IW)","SeverityCode":"1","Note":"Auftrag 1100007004 mit Meldung 10090180 gesichert","ExternalID":null}]}}';

        
        String res = CCWSInterfaceQueue.handleReprocessingRequest(data);
        Test.stopTest();
        
        System.assertNotEquals(res, null);

    }*/
    
    
        
    private static SCInterfaceQueue__c createQueueEntry()
    {
    	String handler = 'CCWSGenericResponse';
    	SCInterfaceQueue__c item = new SCInterfaceQueue__c();
        item.InterfaceHandler__c = handler;
        item.Exception__c = 'Exception(SCfwInterfaceRequestPendingException):The order with name: ORD-0000027918 has ERPStatusOrderClose__c \'none\'.	Cause: null StackTrace: Class.CCWCOrderCloseResponse.readOrder: line 193, column 1';
		item.Status__c           = 'pending';                
        insert item;
         
        String data = '{"References":{"References":{"Reference":[{"ReferenceID":"001100007004","IDType":"Order Number","ExternalID":"ORD-0000027918"},{"ReferenceID":"000010090180","IDType":"Notification No","ExternalID":"ORD-0000027918"},{"ReferenceID":"0000000112200329","IDType":"IDoc number","ExternalID":null}]},"Operation":"CloseOrder","ExternalID":"ORD-0000027918"},"MessageHeader":{"TestDataIndicator":false,"SenderBusinessSystemID":"SAP-QPI","ReferenceUUID":null,"ReferenceID":"CP20140319124528333-259234","RecipientBusinessSystemID":"SFCPDE-CCEAGQA","MessageUUID":"5324C61379020C40E10080000A6210B7","MessageID":"5324C61179020C40E10080000A6210B7","CreationDateTime":"2014-03-19T11:43:13.000Z"},"Log":{"MaximumLogItemSeverityCode":"3","Item":[{"WebURI":null,"TypeID":"032(RU)","SeverityCode":"3","Note":"Arbeitsplatz DE166268 in Werk 0356 nicht vorhanden (Bitte Eingabe prÃ¼fen)","ExternalID":null},{"WebURI":null,"TypeID":"085(IW)","SeverityCode":"1","Note":"Auftrag 1100007004 mit Meldung 10090180 gesichert","ExternalID":null}]}}';
        Attachment a = new Attachment(parentId = item.id, Name=handler, Body=Blob.valueOf(data),Description = '', ContentType='text/plain');
		insert a; 
         
    	return item; 
    }
   
       
    private static void debug(String text)
    {
        System.debug('###...................' + text);
    }

}
