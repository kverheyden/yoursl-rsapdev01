/*
 * @(#)SCbtcGeocodeBase.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * The base class for geocoding.
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
global virtual class SCbtcGeocodeBase extends SCbtcBase
    implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts
{
    public String traceMode = 'productive';

    /**
     * Number of done calls
     */
    Integer callsDone = 0;
    
    /**
     * Processing mode. There are following processing modes
     *                ALLNEW  geocodes only new records not yet geocoded (geox = 0 and geoy =0). 
     *                ALL     geocdes all records (also the already geocoded)
     *                SIM_ALLNEW  like ALLNEW but only simulation
     *                SIM_ALL     like ALL but only simulation
     */
    public String processingMode = null;

    /**
    *    Format list for the data in a final email and in the SCInterfaceLog.Data__c
    *    It comprise the column names with their width in the form 
    *    COLUMN_NAME(WIDTH)
    */
    public static List<String> formatList = getFormatList();
    
    private static String employeeFormat = 'EMPLOYEE(30)';
    /**
    * Constructs a format list
    */
    public static List<String> getFormatList()
    {
        List<String> ret = new List<String>();
        ret.add('EMPLOYEE(30)');
        ret.add('COUNTRY(7)');
        ret.add('COUNTRY_STATE(12)');
        ret.add('COUNTY(20)');
        ret.add('POSTAL_CODE(10)');
        ret.add('CITY(150)');
        ret.add('DISTRICT(10)');
        ret.add('STREET(12)');
        ret.add('HOUSE_NUMBER(10)');
        ret.add('NO_FROM(6)');
        ret.add('NO_TO(6)');
        ret.add('GEO_Y(15)');
        ret.add('GEO_X(15)');
        ret.add('GEO_APPROX(6)');
        ret.add('MATCH_TYPE(12)');
        ret.add('MATCH_INFO(15)');
        ret.add('MONIKER(200)');
        ret.add('INFO(100)');
        return ret;
    }
    /**
     * Prefixes to construct the first column of grid for information data.
     */
    /**
    *
    */
    public static String emailTitlePrefix = 'Kind(7)';
    public static String emailOrigPrefix = '-->O(7)';
    public static String emailCheckPrefix = '-->C(7)';
    public static String emailGeoPrefix = '-->G(4)';

    public static String dataTitlePrefix = 'Kind(12)';
    public static String dataOrigPrefix =  '-->Original:(12)';
    public static String dataCheckPrefix = '-->Check(12)';
    public static String dataGeoPrefix = '-->Geocoded(12)';
    public static String dataCheckPrefix2 = '  Check(12)';
    public static String dataGeoPrefix2 = '  Geocoded(12)';

    public Boolean exactlyGeoCoded = true;

    /**
    *    Determines whether the parameter is an even number
    */
    public static Boolean isEven(Integer i)
    {
        Boolean ret = false;
        Integer quotient = i /2;
        Integer checkProduct = quotient * 2;
        if(checkProduct == i)
        {
            ret = true;
        }
        return ret;
    }

    /**
    * Determines whehter the parameter is an odd number
    */
    public static Boolean isOdd(Integer i)
    {
        return !isEven(i);
    }

    /**
    * Determines whether the country need a double call to the Geocode Web Service.
    * Check and Geocode
    */
    public static Boolean isDoubleCall(String country)
    {
        Boolean ret = false;
        if(country != null)
        {
            if(country.equalsIgnoreCase('GB')
               || country.equalsIgnoreCase('NL'))
            {
                ret = true;
            }
        }    
        return ret;
    }
    /**
     * logs a geocoding process
     */
    public void logBatchInternal(List<SCInterfaceLog__c> interfaceLogList, String interfaceName, String interfaceHandler,
                                ID referenceID, ID referenceID2, String resultCode, 
                                String resultInfo, String data, Datetime start, Integer count)
    {
        SCInterfaceLog__c ic = new SCInterfaceLog__c();
        if(interfaceLogList != null)
        {
            interfaceLogList.add(ic);
        }
        ic.Interface__c = interfaceName;
        ic.InterfaceHandler__c = interfaceHandler;
        ic.Type__c = 'BATCH';
        ic.Count__c = count;
        ic.ReferenceID__c = referenceID;
        ic.ReferenceID2__c = referenceID2;
        ic.ResultCode__c = '';
        ic.ResultCode__c = resultCode;
        ic.ResultInfo__c = resultInfo;
        ic.Data__c = data;
        ic.Direction__c = 'internal';
        ic.Start__c = start;
        ic.Stop__c = Datetime.now();
        Long duration = ic.Stop__c.getTime() - ic.Start__c.getTime();
        ic.Duration__c = duration; 
        if(interfaceName == 'GEOCODE_ACCOUNT')
        {
            ic.Account__c = referenceID;
        }
        else if(interfaceName == 'GEOCODE_IB_LOCATION')
        {
            ic.InstalledBaseLocation__c = referenceID;
        }
        else if(interfaceName == 'GEOCODE_R_ASSIGNMENT')
        {
            ic.ResourceAssignment__c  = referenceID;
        }
/*	causes You have uncommitted work pending. Please commit or rollback before calling out
        if(interfaceLogList == null)
        {
            insert ic;
        }
*/
    }       
    /**
    * Break the geocoding process if the number of calls for the day is expired.
    */
    public void breakJobByExhaustedDailyCallouts(Database.BatchableContext BC, 
                                                    Integer limitOfOutCalls, 
                                                    Integer callCnt)
    {
        Integer oft = 100;
        Integer threshold = 200;
        debug('breakJobByExhaustedDailyCallouts callCnt : ' + callCnt);
        if(isMultiplyOf(oft, callCnt))
        {
            Integer callsDone = getOutgoingWebCallsForToday();
            Integer upperLimit = limitofOutCalls  - threshold; 
            debug('callsDone: ' + callsDone + ' upperLimit: ' + upperLimit);
            if(callsDone > upperLimit)
            {
                if(BC != null)
                {
                    Id jobId = BC.getJobID();
                    System.abortJob(jobId);
                }    
            }
        }    
    } //breakJobByExhaustedDailyCallouts

    /**
    * Determines whether the second paramter is divided by the first with the rest = 0
    */
    public static Boolean isMultiplyOf(Integer multiplier, Integer i)
    {
        Boolean ret = false;
        Integer quotient = i /multiplier;
        Integer checkProduct = quotient * multiplier;
        if(checkProduct == i)
        {
            ret = true;
        }
        return ret;
    }

    /**
    * Gets the number of WebCalls to the GeoCoding Web service from the AsyncApexJob
    */
    public static Integer getOutgoingWebCallsForToday()
    {
        Integer ret = 0;
        List<String> classNamesList = new List<String>();
        classNamesList.add('SCbtcGeocodeAccount');
        classNamesList.add('SCbtcGeocodeInstalledBaseLocation');
        classNamesList.add('SCbtcGeocodeResources');
        
        AggregateResult[] aggRes = [Select sum(JobItemsProcessed) from AsyncApexJob 
                                where ApexClass.Name in :classNamesList and CreatedDate = TODAY and JobType = 'BatchApex'];
        if(aggRes.size() > 0)
        {
            Double dSum = (Double)aggRes[0].get('expr0');
            if(dSum != null)
            {
                ret = dSum.intValue();
            }
            else
            {
                ret = 0;
            }    
        }                        
        return ret;
    }

    /**
    * Gets the data for the result info of the interface log
    */
    public String getAddressDataForResultInfo(Boolean doubleCallOut, AvsAddress checkAddress, 
                                            AvsResult addrResult, AvsResult geocodingResult,
                                            Integer indexOfAddr)
    {
        String ret = '';
        // use processing mode for marking simulation
        // e.g. ', simulated'
        // e.g. 'Valid, Set'
        // e.g. 'Invalid, Simulated'
        // e.g. 'Invalid, not set'
        if(doubleCallOut)
        {
            if(addrResult != null
                && addrResult.items.size() > 0)
            {
                ret = 'items delivered by check: ' + addrResult.items.size();
            }    
            if(geocodingResult != null 
                && geocodingResult.items.size() > 0
                && geocodingResult.items[indexOfAddr].geoX != null 
                && geocodingResult.items[indexOfAddr].geoX != 0
                && geocodingResult.items[indexOfAddr].geoY != null 
                && geocodingResult.items[indexOfAddr].geoY != 0)
            {
                ret += ', geoX: ' + geocodingResult.items[indexOfAddr].geoX;
                ret += ', geoY: ' + geocodingResult.items[indexOfAddr].geoY;
                ret += ', Approx: ' + geocodingResult.items[indexOfAddr].geoApprox;
                if(processingMode != null && processingMode.startsWith('SIM'))
                {
                    ret += ', simulated.';
                }
                else
                {
                    ret += ', set.';
                }
            }
            else
            {
                ret = ', not set.';
            }    
        }
        else
        {
            if(geocodingResult != null 
                && geocodingResult.items.size() > 0
                && geocodingResult.items[indexOfAddr].geoX != null 
                && geocodingResult.items[indexOfAddr].geoX != 0
                && geocodingResult.items[indexOfAddr].geoY != null 
                && geocodingResult.items[indexOfAddr].geoY != 0)
            {
                ret += ', geoX: ' + geocodingResult.items[indexOfAddr].geoX;
                ret += ', geoY: ' + geocodingResult.items[indexOfAddr].geoY;
                ret += ', Approx: ' + geocodingResult.items[indexOfAddr].geoApprox;
                if(processingMode != null && processingMode.startsWith('SIM'))
                {
                    ret += ', simulated.';
                }
                else
                {
                    ret += ', set.';
                }
            }
            else
            {
                ret = ', not set.';
            }    
        }
        return ret;
    }

    /**
    * Gets a grid title on behalf of the static format list defined on the begin of the class
    */
    public String getGridTitle(String titlePrefixFormat, String fieldSeparator, String employee)
    {
        String ret = '';
        String title = getFormatedTitle(formatList, fieldSeparator, employee);
        title = title.trim();
        String kindFormat = titlePrefixFormat;
        String titlePrefix = getTitlePrefix(kindFormat, fieldSeparator);
        ret = titlePrefix + title;
        return ret;
    }


    /**
    * Gets email data for the sending to an operator
    */
    public String getEmailData(AvsAddress addr, AvsResult addrResult, 
                                AvsResult geocodingResult, ID recordId, String fieldSeparator,
                                String employee, String error)
    {
        String ret = '';
        
        String originalAddress = getFormatedAddress(formatList, addr, fieldSeparator, error);
        String originalPrefix = getPrefix(emailOrigPrefix, recordId, fieldSeparator);
        String employeePart = '';
        if(employee != null && employee != '')
        {
            employeePart = getEmployeePrefix(employee, fieldSeparator);
        }    
        ret += '\n' + originalPrefix + employeePart + originalAddress; 
        if(addrResult != null 
            && addrResult.items != null
            && addrResult.items.size() > 0)
        {    
//            ret += '\nCheck Results: ' +  addrResult.items.size();
            Integer cnt = 0;
            for(AvsAddress checkAddr: addrResult.items)
            {
                cnt++;
                if(cnt > 1)
                {
                    break;
                }
                String checkAddress = getFormatedAddress(formatList, checkAddr, fieldSeparator, error);
                ret += '\n';
                ret += getPrefix(emailCheckPrefix, recordId, fieldSeparator);
                if(employee != null && employee != '')
                {
                    ret += getEmployeePrefix(null, fieldSeparator);
                }    
                ret += checkAddress;
            }
        }    
        
        if(geocodingResult != null 
            && geocodingResult.items != null
            && geocodingResult.items.size() > 0)
        {    
//            ret += '\nGeocoding Results: ' +  geocodingResult.items.size();
            Integer cnt = 0;
            for(AvsAddress geoAddr: geocodingResult.items)
            {
                cnt++;
                if(cnt > 1)
                {
                    break;
                }
                String geoAddress = getFormatedAddress(formatList, geoAddr, fieldSeparator, error);
                ret += '\n';
                Boolean approx = false;
                if(addrResult != null)
                {
                    if(addrResult.items.size() > 1)
                    {
                        approx = true;
                    }
                    if(exactlyGeoCoded)
                    {
                        approx = false;
                    }
                }
                else
                {
                    if(geocodingResult.items.size() > 1)
                    {
                        approx = true;
                        if(exactlyGeoCoded)
                        {
                            approx = false;
                        }
                    }
                }
                
                ret += getEmailGeoPrefix(emailGeoPrefix, recordId, fieldSeparator, approx);
                if(employee != null && employee != '')
                {
                    ret += getEmployeePrefix(null, fieldSeparator);
                }    
                ret += geoAddress;
            }
        }    
        return ret;
    }

    /**
    * Gets data to fill the field SCInterfaceLog.Data__c.
    * The data in the form:
    * Title
    * Original record
    * Check records
    * Geocoded records
    */
    public String getData(AvsAddress addr, AvsResult addrResult, 
                          AvsResult geocodingResult, ID recordId, String fieldSeparator, String employee)
    {
        String ret = '';

        // Title
        String title = getGridTitle(dataTitlePrefix, fieldSeparator, employee);
        ret = title;
        
        // original Address
        ret += '\nOriginal Address:';
        String originalAddress = getFormatedAddress(formatList, addr, fieldSeparator, null);
        String originalPrefix = getPrefix(dataOrigPrefix, recordId, fieldSeparator);
        debug('### originalPrefix: ' + originalPrefix);
        String employeePart = '';
        if(employee != null && employee != '')
        {
            employeePart = getEmployeePrefix(employee, fieldSeparator);
        }    
        ret += '\n'+  originalPrefix + employeePart + originalAddress; 
        ret += '\n';
        if(addrResult != null 
            && addrResult.items != null
            && addrResult.items.size() > 0)
        {    
            ret += '\nCheck Results: ' +  addrResult.items.size();
            Integer cnt = 0;
            for(AvsAddress checkAddr: addrResult.items)
            {
                cnt++;
                if(cnt > 25)
                {
                    break;
                }
                String checkAddress = getFormatedAddress(formatList, checkAddr, fieldSeparator, null);
                ret += '\n';
                if(cnt == 1)
                {
                    ret += getPrefix(dataCheckPrefix, recordId, fieldSeparator);
                }
                else
                {
                    ret += getPrefix(dataCheckPrefix2, recordId, fieldSeparator);
                }
                if(employee != null && employee != '')
                {
                    ret += employeePart;
                }
                ret += checkAddress;
            }
        }    
        
        if(geocodingResult != null 
            && geocodingResult.items != null
            && geocodingResult.items.size() > 0)
        {    
            ret += '\nGeocoding Results: ' +  geocodingResult.items.size();
            Integer cnt = 0;
            for(AvsAddress geoAddr: geocodingResult.items)
            {
                cnt++;
                if(cnt > 25)
                {
                    break;
                }
                String geoAddress = getFormatedAddress(formatList, geoAddr, fieldSeparator, null);
                ret += '\n';
                if(cnt == 1)
                {
                    ret += getPrefix(dataGeoPrefix, recordId, fieldSeparator);
                }
                else
                {
                    ret += getPrefix(dataGeoPrefix2, recordId, fieldSeparator);
                }
                if(employee != null && employee != '')
                {
                    ret += employeePart;
                }    
                ret += geoAddress;
            }
        }    
        return ret;
    }
    

    /**
    * Get the formated address on the base of the static format list defined on the begin of the class
    */
    public  String getFormatedAddress(List<String> formatOfFields, AvsAddress addr, String fieldSeparator, String error)
    {
        String ret = '';
        for(String fieldFormat: formatOfFields)
        {
            if(fieldFormat.startsWith('EMPLOYEE'))
            {
                continue;
            }   
            String fieldValue = getFormatedFieldValue(fieldFormat, addr, error);
            ret += fieldValue + ' ' + fieldSeparator + ' ';
        }
        ret = ret.trim();
        return ret;
    }

    /**
    * Gets formated Title
    */
    public  String getFormatedTitle(List<String> formatOfFields, String fieldSeparator, String employee)
    {
        System.debug('###format of Fields: ' + formatOfFields);
        String ret = '';
        for(String fieldFormat: formatOfFields)
        {
            if(fieldFormat.startsWith('EMPLOYEE')
               && employee == null)
            {
                continue;
            }   
            String fieldValue = getFormatedFieldTitle(fieldFormat);
            ret += fieldValue + ' ' + fieldSeparator + ' ';
        }
        ret = ret.trim();
        return ret;
    }

    /**
    * Gets a prefix with the kind format for the first column and Id for the second column.
    */
    public String getPrefix(String kindFormat, ID recordId, String fieldSeparator)
    {
        String ret = '';
        String kindValue = getFormatedFieldTitle(kindFormat);
        ret += kindValue + ' ' + fieldSeparator + ' ';
        String recordIdValue = null;
        if(recordId == null)
        {
            recordIdValue = '(null)         ';
        }
        else
        {
            recordIdValue = recordId;
        }
        ret += recordIdValue + ' ' + fieldSeparator + ' ';
        return ret;
    }

    /**
    * Gets a prefix with the kind format for the first column and Id for the second column.
    */
    public String getEmailGeoPrefix(String kindFormat, ID recordId, String fieldSeparator, Boolean approx)
    {
        String ret = '';
        String kindValue = getFormatedFieldTitle(kindFormat);
        String approxApp = ' A ';
        if(approx == false)
        {
            approxApp = ' E ';
        }
        ret += kindValue + approxApp + ' ' + fieldSeparator + ' ';
        String recordIdValue = null;
        if(recordId == null)
        {
            recordIdValue = '(null)         ';
        }
        else
        {
            recordIdValue = recordId;
        }
        ret += recordIdValue + ' ' + fieldSeparator + ' ';
        return ret;
    }


    public String getEmployeePrefix(String employee, String fieldSeparator)
    {
        debug('### EmployeePar: ' + employee);
        
        String fieldFormat = employeeFormat;
        debug('### fieldFormat: ' + fieldFormat);
        
        String fieldName = getFieldName(fieldFormat);
        debug('### fieldName: ' + fieldName);
        
        Integer length = getFieldLength(fieldFormat);
        debug('### length: ' + length);
        
        String value = employee;
        String ret = formatValue(value, length);
        ret +=  ' ' + fieldSeparator + ' ';
        debug('### ret: ' + ret);
        return ret;
    }

    /**
    * Gets a title prefix with the kind format for the first column and Id for the second column.
    *
    */
    public String getTitlePrefix(String kindFormat, String fieldSeparator)
    {
        String ret = '';
        String kindValue = getFormatedFieldTitle(kindFormat);
        ret += kindValue + ' ' + fieldSeparator + ' ';
        String recordIdValue = 'ID                ';
        ret += recordIdValue + ' ' + fieldSeparator + ' ';
        return ret;
    }

    /**
    * Gets formated field value
    */
    public String getFormatedFieldValue(String fieldFormat, AvsAddress addr, String error)
    {
        String fieldName = getFieldName(fieldFormat);
        Integer length = getFieldLength(fieldFormat);
        String value = '';
        if(fieldName.equalsIgnoreCase('INFO'))
        {
            value = error;
        }
        else
        {
            value = getFieldValue(fieldName, addr);
        }
        String ret = formatValue(value, length);
        return ret;
    }
    
    /**
    * Gets formated field title
    */
    public String getFormatedFieldTitle(String fieldFormat)
    {
        String fieldName = getFieldName(fieldFormat);
        Integer length = getFieldLength(fieldFormat);
        String ret = formatValue(fieldName, length);
        return ret;
    }


    /**
    * Gets a field name from the field format
    */
    public static String getFieldName(String fieldFormat)
    {
        String ret = '';
        // COUNTRY(5);
        if(fieldFormat != null)
        {
            Integer openBracket = fieldFormat.indexOf('(');
            if(openBracket > -1)
            {
                ret = fieldFormat.substring(0, openBracket);
            }
        }
        return ret;
    } 

    /**
    * Gets a field length from the field format
    */
    public static Integer getFieldLength(String fieldFormat)
    {
        Integer ret = 0;
        if(fieldFormat != null)
        {
            Integer openBracket = fieldFormat.indexOf('(');
            if(openBracket > -1)
            {
                // take after the open bracket
                String token = fieldFormat.substring(openBracket + 1);
                token = token.trim();
                // remove close bracket
                if(token.length() > 1)
                {
                    token = token.substring(0, token.length()-1);
                    token = token.trim();
                    try
                    {
                        ret = Integer.valueOf(token);
                    }
                    catch(Exception e) {}
                }
            }
        }
        return ret;
    }

    /**
    * Spaces used by formating a cell of the column
    */
    public static String spaces = getSpaces(200);

    /**
    * Function to initialize the spaces variable
    */
    public static String getSpaces(Integer n)
    {
        String ret = '';
        for(Integer i= 0; i< 200; i++)
        {
            ret += ' ';
        }
        return ret;
    }
    /**
    * Gets a field value from the address on behalf the name.
    */
    public static String getFieldValue(String fieldName, AvsAddress addr)
    {
        String ret = '(null)';
        if(fieldName != null && addr != null)
        {
            if(fieldName.equalsIgnoreCase('COUNTRY'))
            {
                if(addr.country != null)
                {
                    ret = addr.country;
                }
            }
            else if(fieldName.equalsIgnoreCase('COUNTRY_STATE'))
            {
                if(addr.countryState != null)
                {
                    ret = addr.countryState;
                }
            }
            else if(fieldName.equalsIgnoreCase('COUNTY'))
            {
                if(addr.county != null)
                {
                    ret = addr.county;
                }
            }
            else if(fieldName.equalsIgnoreCase('POSTAL_CODE'))
            {
                if(addr.postalCode!= null)
                {
                    ret = addr.postalCode;
                }
            }
            else if(fieldName.equalsIgnoreCase('CITY'))
            {
                if(addr.city!= null)
                {
                    ret = addr.city;
                }
            }
            else if(fieldName.equalsIgnoreCase('DISTRICT'))
            {
                if(addr.district!= null)
                {
                    ret = addr.district;
                }
            }
            else if(fieldName.equalsIgnoreCase('STREET'))
            {
                if(addr.street!= null)
                {
                    ret = addr.street;
                }
            }
            else if(fieldName.equalsIgnoreCase('HOUSE_NUMBER'))
            {
                if(addr.houseNumber!= null)
                {
                    ret = addr.houseNumber;
                }
            }
            else if(fieldName.equalsIgnoreCase('NO_FROM'))
            {
                if(addr.noFrom!= null)
                {
                    ret = addr.noFrom;
                }
            }
            else if(fieldName.equalsIgnoreCase('NO_TO'))
            {
                if(addr.noTo!= null)
                {
                    ret = addr.noTo;
                }
            }
            else if(fieldName.equalsIgnoreCase('GEO_X'))
            {
                if(addr.geoX!= null)
                {
                    ret = addr.geoX + '';
                }
            }
            else if(fieldName.equalsIgnoreCase('GEO_Y'))
            {
                if(addr.geoY!= null)
                {
                    ret = addr.geoY + '';
                }
            }
            else if(fieldName.equalsIgnoreCase('GEO_APPROX'))
            {
                if(addr.geoApprox!= null)
                {
                    ret = addr.geoApprox + '';
                }
            }
            else if(fieldName.equalsIgnoreCase('MATCH_TYPE'))
            {
                if(addr.matchType!= null)
                {
                    ret = addr.matchType + '';
                }
            }
            else if(fieldName.equalsIgnoreCase('MATCH_INFO'))
            {
                if(addr.matchInfo!= null)
                {
                    ret = addr.matchInfo;
                }
            }
            else if(fieldName.equalsIgnoreCase('MONIKER'))
            {
                if(addr.moniker!= null)
                {
                    ret = addr.moniker;
                }
            }
            
        }
        return ret;
    }

    /**
    * Formats the value for the cell
    */
    public static String formatValue(String value, Integer length)
    {
        String ret = '';
        if(value != null)
        {
            ret = value;
        }
        else
        {
            ret = value = '';
        }
        Integer valueLength = value.length();
        if(valueLength > length)
        {
            ret = ret.substring(0, length);
        }
        else if (valueLength < length)
        {
            Integer rest = length - valueLength;
            ret += spaces.substring(0, rest);
        }
        return ret;
    }

    /**
     * At checking QAS can deliver many rows. This function finds the row with the address that
     * maches exactly our seek address. If it is not the case, -1 is returned.
     *
     */
    public Integer getIndexQAS(AvsAddress checkAddr, AvsResult addrResult)
    {
        Integer ret = -1;
        Integer cnt = 0;
        for(AvsAddress loopAddr: addrResult.items)
        {
            Boolean theSameAddress = isTheSameAddrQAS(checkAddr, loopAddr);
            if(theSameAddress)
            {
                debug('QAS It is the same address!');
                ret = cnt;
                break;
            }
            cnt++;
        }
        if(ret == -1)
        {
            exactlyGeoCoded = false;
            ret = 0;
        }
        return ret;
    }
    /**
     * The checking result from QAS have the zipcode and city set. In the field city there is a string having
     * more data such as flat number, house number, steet, and city. 
     * We take the fields from the structure checkAddr and look up for their counterparts in the field city of
     * addrFromQAS.
     * 
     */
    public Boolean isTheSameAddrQAS(AvsAddress checkAddr, AvsAddress addrFromQAS)
    {
        Boolean ret = false;
        
        if(checkAddr.postalcode == addrFromQAS.postalcode)
        {
            ret = true;
        }     
        else
        {
            return ret;
        }
        List<String> tokenList = addrFromQAS.city.split(',');
        if(!SCfwCollectionUtils.isContained(checkAddr.housenumber, tokenList))
        {
            return false;
        }
        if(!SCfwCollectionUtils.isContained(checkAddr.street, tokenList))
        {
            return false;
        }
        if(!SCfwCollectionUtils.isContained(checkAddr.city, tokenList))
        {
            return false;
        }
       
        return ret;
    }

    public Integer getIndex(AvsAddress checkAddr, AvsResult addrResult)
    {
        Integer ret = -1;
        Integer cnt = 0;
        for(AvsAddress loopAddr: addrResult.items)
        {
            Boolean theSameAddress = isTheSameAddr(checkAddr, loopAddr);
            if(theSameAddress)
            {
                ret = cnt;
                break;
            }
            cnt++;
        }
        if(ret == -1)
        {
            exactlyGeoCoded = false;
            ret = 0;
        }
        return ret;
    }

    public Boolean isTheSameAddr(AvsAddress checkee, AvsAddress toCheck)
    {
        Boolean ret = false;
        if(checkee.postalcode == toCheck.postalcode
           && checkee.city == toCheck.city
           && checkee.street == toCheck.street
           && checkee.housenumber == toCheck.housenumber)
        {
            debug('It is the same address!');
            ret = true;
        }        
        return ret;
    }

    public class GeoCodeHolder
    {
        public Boolean geoApprox = null;
        public String geoStatus = null;
        public Double geoX = null;
        public Double geoY = null;
    }
    /**
    * Prepares settin coordinates into an account.
    * @param sync true if the synchron call, else false
    * @param doubleCallOut true if on the reason of country we need to have two web calls
    * @param checkAddress the addresss to geocode
    * @param addrResult the result of check call
    * @param geocodingResult the result of geocode call
    * @param the account, we now working out
    * @return the text for the SCInterfaceLog.ResultInfo__c
    */
    public String prepareSetGeoCode(Boolean sync, Boolean doubleCallOut, AvsAddress checkAddress, AvsResult addrResult, 
        AvsResult geocodingResult, GeoCodeHolder holder)
    {
        String ret = '';
        Integer indexOfAddr = 0;
        if(doubleCallOut)
        {
            // from between many addresses got at the first call
            // the one address has been taken and to this address we have only one geocoded result.
            Integer checkSize = addrResult.items.size();
            if(checkSize >= 1)
            {
                if(checkSize == 1)
                {
                    holder.geoApprox = false;
                }
                else if(checkSize > 1)
                {
                    holder.geoApprox = true;
                    if(exactlyGeoCoded)
                    {
                        // In between of many addresses the one matching exactly have been found
                        holder.geoApprox = false;
                    }
                }
                Integer geoCodingSize = geocodingResult.items.size();
                if(geoCodingSize >= 1)
                {
                    if(geoCodingSize > 1)
                    {
                        holder.geoApprox = true;
                    }
                    holder.geoX = geocodingResult.items[0].geox;
                    holder.geoY = geocodingResult.items[0].geoy;
                    if(holder.geoX != 0 || holder.geoY != 0)
                    {
                        holder.geoStatus = 'Geocoded';
                    }
                    else
                    {
                        holder.geoStatus = 'Invalid';
                    }
                }
                else
                {
                    holder.geoStatus = 'Invalid';
                }
            }
            else
            {
                holder.geoStatus = 'Invalid';
            }        
        }
        else
        {
            // DE
            indexOfAddr = getIndex(checkAddress, geocodingResult);
            Integer geoCodingSize = geocodingResult.items.size();
            if(geoCodingSize >= 1)
            {
                if(indexOfAddr == -1)
                {
                    indexOfAddr = 0;
                }
                if(geoCodingSize == 1)
                {
                    holder.geoApprox = false;
                }
                else if(geoCodingSize > 1)
                {
                    holder.geoApprox = true;
                    if(exactlyGeoCoded)
                    {
                        // In between of many addresses the one matching exactly have been found
                        holder.geoApprox = false;
                    }
                }
                holder.geoX = geocodingResult.items[indexOfAddr].geox;
                holder.geoY = geocodingResult.items[indexOfAddr].geoy;
                if(holder.geoX != 0 || holder.geoY != 0)
                {
                    holder.geoStatus = 'Geocoded';
                }
                else
                {
                    holder.geoStatus = 'Invalid';
                }
            }
            else
            {
                holder.geoStatus = 'Invalid';
            }    
        }
        if(holder.geoStatus != null && holder.geoStatus.equalsIgnoreCase('Invalid'))
        {
            ret = 'Invalid. ';
        }
        else
        {
            ret = 'Valid. ';
        }

        String info = getAddressDataForResultInfo(doubleCallOut, checkAddress, addrResult, geocodingResult, indexOfAddr);
        ret += info;
        return ret;
    }//prepareSetGeoCode

    /**
    * traces the text if the trace mode is equal to test or trace
    */
    public void debug(String text)
    {
        if(traceMode.equalsIgnoreCase('test')
            || traceMode.equalsIgnoreCase('trace'))
        {
            System.debug('###....................' + text);
        }
    }

 //@author GMSSS
    public void methodForCodeCoverage(SCbtcGeocodeBase obj, AvsAddress addr0, AvsAddress addr1, AvsResult res1, AvsResult res2)
    {
        if (Test.isRunningTest())
        {
            try { obj.isTheSameAddrQAS(addr0,addr0); } catch(Exception e) {}
            try { obj.isTheSameAddrQAS(addr0,addr1); } catch(Exception e) {}
            try { obj.isTheSameAddr(addr0,addr0); } catch(Exception e) {}
            try { obj.isTheSameAddr(addr0,addr1); } catch(Exception e) {}
            try { obj.getIndexQAS(addr0,res1); } catch(Exception e) {}
            try { obj.getIndexQAS(addr0,res2); } catch(Exception e) {}
        }
    }




}
