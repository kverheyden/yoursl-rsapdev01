global with sharing class SCInstalledBaseMethods {
	
	public static void doAfterUpdate(Map<id, SCInstalledBase__c> oldlistSCInstalledBase, Map<id, SCInstalledBase__c> newlistSCInstalledBase) 
	{
    	System.debug('###SCInstalledBaseMethods - doAfterUpdate (new & old)');
    	// CokeOne - CR 4.3.4.4 - Sprint 3 - Automatic OrderCreation
    	// As an IT Manager I want an automatic refurbishment order (ZC15) created whenever a ZC09 is closed and the equipment was transfered 
        // to 'Ready for refurbishment' 
 		SCInstalledBaseOrderHandler OrderHandler = new SCInstalledBaseOrderHandler();
 		OrderHandler.CheckForAppropriateRecords(oldlistSCInstalledBase, newlistSCInstalledBase);
				
	}
	
	public static void doAfterUpdate(List<SCInstalledBase__c> listSCInstalledBase) {
		//CCWCAssetInventoryDataMobileUpdate send = new CCWCAssetInventoryDataMobileUpdate();
    	//send.sendCallout(Trigger.new, true, false);
    	

    	
	}	
	
	public static void doBeforeInsertUpdate(List<SCInstalledBase__c> listSCInstallesBase) {
		//  ProductModel ID, ProductModel
	    Map<Id, SCProductModel__c> productModels = new Map<Id, SCProductModel__c>();
	    
	    // prepare a list of all required product models 
	    for (SCInstalledBase__c ib : listSCInstallesBase ) {
	        if(ib.ProductModel__c != null) {
	            productModels.put(ib.ProductModel__c, null);
	        }
	    }
	    
		if(productModels.size() < 1)
			return;
		
        // now read the product model details 
        for (SCProductModel__c product : [SELECT Id, Group__c, UnitClass__c, 
											UnitType__c, Brand__c, Power__c
											FROM SCProductModel__c
											WHERE Id in :productModels.keySet()]) {
            productModels.put(product.Id, product);
        }
    
        // fill the fields with the product model data
        for (SCInstalledBase__c ib : listSCInstallesBase ) {
            if (productModels.containsKey(ib.ProductModel__c)) {
                SCProductModel__c product = productModels.get(ib.ProductModel__c);
                
                if (product == null)
                    continue;
                                
                ib.ProductUnitClass__c = product.UnitClass__c;
                ib.ProductUnitType__c  = product.UnitType__c;
                //!!DO NOT CHANGE BRANDING!!
                //ib.Brand__c         = product.Brand__c;
                ib.ProductGroup__c  = product.Group__c;
                ib.ProductPower__c  = product.Power__c;
            }
        }	    
	}
}
