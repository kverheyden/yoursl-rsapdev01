public with sharing class CustomDocumentController {
    
    private Apexpages.Standardcontroller controller;
    private PageReference page;
    public string id;
    
    public CustomDocument__c myCDoc  {get;set;}
    public string action {get;set;}
    public transient Attachment myfile;
    public Attachment getMyfile()
    {
        myfile = new Attachment();
        return myfile;
    }
    public list<Attachment> ListCDocAttachments {get;set;}
    public Attachment currentAttachment {get;set;} 
    
        
   public CustomDocumentController(ApexPages.StandardController stdController) {
        this.controller = stdController;
        this.myCDoc = (CustomDocument__c)stdController.getrecord();
        this.page = ApexPages.currentPage();
        id = page.getParameters().get('id');
        
        if(ApexPages.currentPage().getParameters().get('action') != null)
            action = this.page.getParameters().get('action');
        
        if(this.id!=null)
        {
            try
            {
                myCDoc = [SELECT Id,Name,SalesArea__c,Subtradechannel__c,Tags__c,ContentType__c,Title__c,Url__c,TextContent__c,EmbedCode__c,ValidFrom__c,ValidKeyAccountNumber__c,ValidTo__c,LastModifiedDate,OwnerId FROM CustomDocument__c WHERE Id=:id LIMIT 1];
                getAttachment();
            }
            catch(Exception e) {System.debug(e.getTypeName() + ' caught: ' + e.getMessage());}
            
            if (ApexPages.currentPage().getParameters().get('retURL') != null || action == 'edit')
                action = 'edit';
            else
                action = 'show';
        }
        
        
        if(action==null)
            action = 'list';
    }

    public list<Attachment> getAttachment()
    {
        ListCDocAttachments = new list<Attachment>();
        ListCDocAttachments = [SELECT Id, Name, ParentId, ContentType, BodyLength FROM Attachment WHERE ParentId =:id ORDER BY CreatedDate DESC];
        if(ListCDocAttachments.size()>0){
            currentAttachment = ListCDocAttachments[0];
        }
        else{
            currentAttachment = null;
        }
        return ListCDocAttachments;
    }
       
    public PageReference showList() {
        return customDocumentPageReference('list');
    }
   
    public PageReference editDocument() {
        return customDocumentPageReference('edit');
    }   
    
    public PageReference newDocument() {
        return customDocumentPageReference('new');
    }   
    
    /******************************************
                 Delete Attachment
    ******************************************/  
    public PageReference deleteAttachment(){
        delete ListCDocAttachments;
        getAttachment();
        
        if(myCDoc.Id != null)
        {
          myCdoc.ContentType__c = '';
          update myCDoc;
        }
        
        return null;
    }
    

    /******************************************
                 cancel Doc
    ******************************************/    
    public PageReference cancel() {
        if(action == 'new')
            return customDocumentPageReference('list');

        if(action == 'edit')
            return customDocumentPageReference('read');
        
        return customDocumentPageReference('list');
    }
        
    /******************************************
                 SAVE NEW FAQ
    ******************************************/
    public PageReference saveDocument() {
        Boolean isSuccess = True;
        try {
            if (myCDoc.Id == null) 
                insert myCDoc;
            else 
                update myCDoc;
        }
        catch (DmlException e) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(myMsg);
            isSuccess = false;
            System.debug('DmlException caught: '+ e.getMessage());
            if (e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) {
                myfile = null;
                return null;
            }
        }
        catch (SObjectException e) {System.debug('SObjectException caught: '+ e.getMessage());}
        catch (Exception e) {System.debug('Exception caught: '+ e.getMessage());}
        system.debug('### Created new Doc: '+ myCDoc);
        /***************************
                Attachment
        ***************************/

        if (myfile.body != NULL) {
            system.debug('### new File found: '+ myfile.name);
            if (ListCDocAttachments != null &&ListCDocAttachments.size()>0) {
                system.debug('### try to delete existing Files: '+ ListCDocAttachments.size());
                //delAtt();
                delete ListCDocAttachments;
            }
        }
        Attachment docAttachment = new Attachment(parentId = myCDoc.Id, name = myfile.name, body = myfile.body);
        try {
            system.debug('### saveDocument Attachment: DocId='+ myCDoc.Id + ' FileName='+ myfile.name + ' ContentType='+ myfile.ContentType);
            if (myCDoc.Id != null &&myfile.body != NULL) {
                system.debug('### try to insert File: ');
                insert docAttachment;
            }
            myCDoc.ContentType__c = getContentTypeByFileEnding(myfile.Name);
            docAttachment.ContentType = myCDoc.ContentType__c;
            update myCDoc;
            update docAttachment;
        }
        catch (Exception e) {
            System.debug('### ' + e.getTypeName() + ' caught: '+ e.getMessage());
        }
        
        if (isSuccess)
            return customDocumentPageReference('show');
        return null;
    }
    
    private PageReference customDocumentPageReference(String actionName) {
        PageReference newPage = New PageReference('/apex/CustomDocumentAction');
        newPage.getParameters().put('action', actionName);
        if (actionName == 'read' || actionName == 'show' || actionName == 'edit')
            newPage.getParameters().put('id', myCDoc.id);
        newPage.setRedirect(true);
        return newPage;
    }
    
    private static String getContentTypeByFileEnding(String fileName) {
        string fileNameLower = fileName.toLowerCase();
        
        if(fileNameLower.endsWith('.png'))
            return 'image/png';
        
        if(fileNameLower.endsWith('.jpg') || fileName.endsWith('.jpeg'))
            return 'image/jpeg';
        
        if(fileNameLower.endsWith('.pdf'))
            return 'application/pdf';
        
        if(fileNameLower.endsWith('.txt'))
            return 'text/plain';
        
        return '';
    }
}
