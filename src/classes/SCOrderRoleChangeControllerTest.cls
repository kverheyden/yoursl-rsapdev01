/*
 * @(#)SCOrderRoleChangeControllerTest.cls
 *
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Testclass for the "Change Order Role Controller"
 * 
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCOrderRoleChangeControllerTest
{
    private static SCOrderRoleChangeController orc;
    
    static testMethod void orderRoleChangeControllerPositiv1()
    {
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.createOrderTestSet(true);
    
        ApexPages.currentPage().getParameters().put('oid', SCHelperTestClass.order.id);
        
        orc = new SCOrderRoleChangeController();
        
        orc.selAccountIRId = SCHelperTestClass.account.Id;
        orc.loadNewAccountIR();
        orc.changeIR();
        orc.goBack();
        orc.getAccountIds();
        
    }
}
