/*
 * @(#)SCContractConditionRelatedController.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.   
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This controller creates a related list with contract conditions
 * for the page layout
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 *
 */
 
public with sharing class SCContractConditionRelatedController
{
    private SCContract__c contract;
    public SCContract__c myContract;
    
    public List<SCContract__c> currentData;
    
    public List<SCCondition__c> conditions { get; set; }
   
    public Map<String, ConditionRow> mapCondRows { get; set; }
    
    private static SCApplicationSettings__c applicationSettings = SCApplicationSettings__c.getInstance();
    public String contractFieldName { get; set; }
    
    public String conditionId { get; set; }
    public String myContrId;
    public String myTmplId;
    public String myFrameId;
    public String recordTypeName;
    
    public Boolean showTemplate { get; set; }
    public Boolean showFrame { get; set; }
    public String thisContractId { get; set; }
    public String thisContractName { get; set; }
    public String SelectedConditionId { get; set; }
    
    public String recType { get; set; }
    
    /*
    public SCContractConditionRelatedController()
    {
    
    } */

   /*
    * Get the current contract data (Id, Frame, Template)
    */
    public SCContractConditionRelatedController(ApexPages.StandardController controller) 
    {
        this.contract = (SCContract__c)controller.getRecord();
        myContrId = String.valueOf(contract.Id);
        
        contractFieldName = applicationSettings.CONTRACT_FIELD_NAME__c;
        
        //System.debug('#### myContrId : ' + myContrId );

        myContract = [SELECT id, Name, RecordType.DeveloperName, Frame__c, Template__c from SCContract__c WHERE Id = :myContrId];
        myTmplId = String.valueOf(myContract.Template__c);
        myFrameId = String.valueOf(myContract.Frame__c);
        thisContractId = myContrId;
        thisContractName = String.valueOf(myContract.Name);
        recordTypeName = String.valueOf(myContract.RecordType.DeveloperName);
        
        recType = recordTypeName;
        
        mapCondRows = new Map<String, ConditionRow>();
    }  

   /* 
    * Returns a list of the contract condition(s) for the visual force page
    */
    public List<ConditionRow> getItemsCondition()
    {
        readConditions();
        
        return (List<ConditionRow>)mapCondRows.values();
    }
    
        
    public PageReference readConditions()
    {
        // 1. select the relevant conditions for this contract, frame contract or template
        if(recordTypeName != 'Contract')
        {
            conditions = [select Id,
                                Name, 
                                contract__r.RecordType.DeveloperName,
                                RecordTypeId,
                                Account__c,
                                Brand__c,
                                toLabel(ArticleClass__c),
                                toLabel(ArticleType__c), 
                                Article__c, 
                                Article__r.Name,
                                Article__r.ArticleNameCalc__c,
                                Article__r.Id, 
                                ContractPeriod__c, 
                                ValidFrom__c, 
                                ValidTo__c, 
                                ProductUnitClass__c, 
                                ProductUnitType__c, 
                                ProductModel__c,
                                ConditionValue__c,
                                Module__c,
                                ModuleName__c,
                                Price__c,
                                Runtime__c
                         from   SCCondition__c 
                         where  contract__c in (:contract.id, :myFrameId, :myTmplId)
                         order by contract__r.RecordType.DeveloperName, CreatedDate asc];
        }
        if(recordTypeName == 'Contract')
        {
            conditions = [select Id,
                                Name, 
                                contract__r.RecordType.DeveloperName,
                                RecordTypeId,
                                Account__c,
                                Brand__c,
                                toLabel(ArticleClass__c),
                                toLabel(ArticleType__c), 
                                Article__c, 
                                Article__r.Name,
                                Article__r.ArticleNameCalc__c,
                                Article__r.Id, 
                                ContractPeriod__c, 
                                ValidFrom__c, 
                                ValidTo__c, 
                                ProductUnitClass__c, 
                                ProductUnitType__c, 
                                ProductModel__c,
                                ConditionValue__c,
                                Module__c,
                                ModuleName__c,
                                Price__c,
                                Runtime__c
                         from   SCCondition__c 
                         where  contract__c = :contract.id
                         order by contract__r.RecordType.DeveloperName, CreatedDate asc];
        }
        
        // 2. Evaluate the conditions and create the output data 
        //    join the contract, frame contract and template conditions with 
        //    the same context (article ref, product ref or account ref....)

        System.debug('#### conditions: ' + conditions.size());

        for(SCCondition__c c : conditions)
        {
            // create a key from the context fields (article, article type and class, ....)
            // the key is used to eleminate douplicate conditions 
            String key = createKey(c);
            
            // check if we already have a condition of this type
            ConditionRow row = mapCondRows.get(key);
            if(row == null)
            {
                // we have a new condition - create the data object
                row = new ConditionRow();
                row.key = key;
                mapCondRows.put(key, row);
            }
            // now update the condition value
            row.setConditionValue(recordTypeName, c);
        }
        
        System.debug('#### mapCondRows: ' + mapCondRows.size());
        
        return null;
    }
  
    public class ConditionRow 
    {
        public String ID {get; set;}
        public String valueCurrent {get; set;}
        public String valueFrame {get; set;}
        public String valueTemplate {get; set;}
        public String key { get; set; }
        public String idCurrent { get; set; }
        public String nameCurrent { get; set; }
        
        public String ArticleClass  {get; set;}     
        public String ArticleType {get; set;}
        public String ArticleNo {get; set;}
       
        public SCCondition__c conData { get; set; }  
        
        public ConditionRow()
        {
        }
        
        
       /*
        * 
        * @param ctx the context where we are (recordTypeName Template.... Frame... or else contract / insurance)
        * @param c the condition record to evaluate 
        */
        public PageReference setConditionValue(String ctx, SCCondition__c c)
        {
            ID = c.ID;
            ArticleClass = c.ArticleClass__c;
            ArticleType  = c.ArticleType__c;
            ArticleNo    = c.Article__r.Name;
          
            conData = c;
            
            //###TODO: add the other condition context fields
            
            //###TODO: implement getArticleRef, getProductRef, .... functions
                   
            // Evaluation of the following record types:            
            // - TemplateContract     ctx.startswith('Template')
            // - TemplateInsurance    ctx.startswith('Template')    
            // - ContractFrame        ctx.contains('Frame')
            // - Contract             else
            // - Insurance            -"- 

            // get the current condition value
            String value  = String.valueOf(c.ConditionValue__c);
            
            // Determine the condition contract context 
            String condRT = c.contract__r.RecordType.DeveloperName;

            // Depending on the current display context (contract, frame contract or template dialog):            
            // Set the output fields depending on the context record type context
            if(ctx.startswith('Template'))
            {
                valueCurrent  = value;
                idCurrent = c.Id;
                nameCurrent = c.Name;
            }
            else if(ctx.contains('Frame'))
            {
                if(condRT.startswith('Template'))
                {
                    valueTemplate = value;
                }
                else
                {
                    valueCurrent  = value; 
                    idCurrent = c.Id;
                    nameCurrent = c.Name;
                }
            }
            else
            {   // contract or insurance level
                if(condRT.startswith('Template'))
                {
                    valueTemplate = value;
                }
                else if(condRT.contains('Frame'))
                {
                    valueFrame = value;
                }
                else
                {
                    valueCurrent  = value; 
                    idCurrent = c.Id;
                    nameCurrent = c.Name;
                }
            }
            return null;
        }
        
    }  

    public static String createKey(SCCondition__c c)
    {
        String key =       
                  c.ArticleClass__c + '-' + 
                  c.ArticleType__c + '-' + 
                  c.Article__c + '-' + 
                  c.ProductUnitType__c + '-' + 
                  c.ProductUnitClass__c + '-' + 
                  c.ProductModel__c + '-' + 
                  c.Account__c + '-' + 
                  c.Brand__c + '-' + 
                  c.ContractPeriod__c + '-' + 
                  c.ValidFrom__c + '-' + 
                  c.ValidTo__c + '-' +
                  c.Module__c + '-' + 
                  c.ModuleName__c;
         return key;
    }
    
   /* 
    * Detele Condition
    */
    public PageReference deleteCondition()
    {
        SCCondition__c conditionToDelete = [SELECT id, name FROM SCCondition__c WHERE id = :SelectedConditionId];
        try 
        {
            delete conditionToDelete;
        } 
        catch (DmlException e) 
        {
            System.debug('#### Delete Contract Condition: ' + e.getTypeName() + ' - ' + e.getCause() + ': ' + e.getMessage());
        }
        mapCondRows.clear();
        getItemsCondition();
        
        return null;
    }

}
