/**
* @author		Austen Buennemann
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				a.buennemann@yoursl.de
*
* @description	Unit Test for CCWCFaxService
*
* @date			18.09.2014
*
* Timeline:
* Name               DateTime                  Description
* Austen Buennemann  18.09.2014 09:48          Class created
*/

@isTest
private class CCWCFaxServiceTest {

    static testMethod void myUnitTest() {
    	
		CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        appSettings = SCApplicationSettings__c.getInstance();
        
        CCSettings__c ccSettings = new CCSettings__c();
        ccSettings.SAPDispBasicAuthUsername__c = 'user';
        ccSettings.SAPDispBasicAuthPassword__c = 'passsword';
        ccSettings.SAPDispEndpointFaxService__c = 'Endpoint';
        ccSettings.SAPDispServer__c = 'Server';
        insert ccSettings;
    	
		Account acc = new Account();
		acc.Name = 'SomeAccount';
		acc.GFGHPreferredOrderTransactionMethod__c = 'fax';
		acc.GFGHPreferedOrderFax__c = '0000000';
		insert acc;
		
		Attachment att = new Attachment();
		att.Name = 'SalesOrderSummary';
		att.ParentId = acc.Id;
		att.Body = Blob.valueOf('Something');
		insert att;
    	
		String attachment = EncodingUtil.base64Encode(att.Body);
		String parentAccount = JSON.serialize(acc);    
		CCWCFaxService.executecallout(parentAccount, attachment, true);
		CCWCFaxService.executecallout(parentAccount, attachment, false);
		List<Attachment> attachments = new List<Attachment>();
		attachments.add(att);
   		CCWCFaxServiceHandler sendFax = new CCWCFaxServiceHandler();
		sendFax.readAttachment(attachments);				
    }
}
