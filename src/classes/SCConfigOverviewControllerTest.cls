/*
 * @(#)SCConfigOverviewControllerTest.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * @author jpietrzyk@gms-online.de
 */
@isTest
private class SCConfigOverviewControllerTest
{
    private static Boolean seeAllData = CCWCTestBase.isSeeingAllData();
    public static testmethod void getEndpoint() 
    {
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        appSettings.ASE_ENDPOINT_URL__c = 'http://test.de';
        upsert appSettings;
        
        try
        {
            SCConfigOverviewController.isTest = true;
            SCConfigOverviewController coc = new SCConfigOverviewController();
            coc.getEndPoint();
        }
        catch(Exception e)
        {
            debug('exception: ' + SCfwException.getExceptionInfo(e));
        }    
    }
    
    public static testmethod void getJobs() 
    {
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        appSettings.ASE_ENDPOINT_URL__c = 'http://test.de';
        upsert appSettings;
        try
        {
            SCConfigOverviewController.isTest = true;
            SCConfigOverviewController coc = new SCConfigOverviewController();
            coc.getJobs();
        }
        catch(Exception e)
        {
            debug('exception: ' + SCfwException.getExceptionInfo(e));
        }    
    }

    public static testmethod void getContractsWithoutPostalcode() 
    {
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        appSettings.ASE_ENDPOINT_URL__c = 'http://test.de';
        upsert appSettings;
        try
        {
            SCConfigOverviewController.isTest = true;
            SCConfigOverviewController coc = new SCConfigOverviewController();
            coc.getContractsWithoutPostalcode();
        }
        catch(Exception e)
        {
            debug('exception: ' + SCfwException.getExceptionInfo(e));
        }    
    }

    private static testmethod void getRefreshContracts() 
    {
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        appSettings.ASE_ENDPOINT_URL__c = 'http://test.de';
        upsert appSettings;
        try
        {
            SCConfigOverviewController.isTest = true;
            SCConfigOverviewController coc = new SCConfigOverviewController();
            coc.getRefreshContracts();
        }
        catch(Exception e)
        {
            debug('exception: ' + SCfwException.getExceptionInfo(e));
        }    
    }

    private static testmethod void  refresh() 
    {
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        appSettings.ASE_ENDPOINT_URL__c = 'http://test.de';
        upsert appSettings;
        try
        {
            SCConfigOverviewController.isTest = true;
            SCConfigOverviewController coc = new SCConfigOverviewController();
            coc.refresh();
        }
        catch(Exception e)
        {
            debug('exception: ' + SCfwException.getExceptionInfo(e));
        }    
    }

    private static testmethod void  controlOperation() 
    {
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        appSettings.ASE_ENDPOINT_URL__c = 'http://test.de';
        upsert appSettings;
        try
        {
            SCConfigOverviewController.isTest = true;
            SCConfigOverviewController coc = new SCConfigOverviewController();
            List<String> operationList = new List<String>();
            coc.controlOperation(operationList);
        }
        catch(Exception e)
        {
            debug('exception: ' + SCfwException.getExceptionInfo(e));
        }    
    }

    private static testmethod void  callout() 
    {
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        appSettings.ASE_ENDPOINT_URL__c = 'http://test.de';
        upsert appSettings;
        try
        {
            SCConfigOverviewController.isTest = true;
            SCConfigOverviewController coc = new SCConfigOverviewController();
            List<String> refreshTypeList= new List<String>();
            coc.callout(refreshTypeList);
        }
        catch(Exception e)
        {
            debug('exception: ' + SCfwException.getExceptionInfo(e));
        }    
    }

    // simple test to increase code coverage    
    private static testmethod void test()
    {
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        appSettings.ASE_ENDPOINT_URL__c = 'http://test.de';
        upsert appSettings;

        SCAseControl__c obj = new SCAseControl__c();
        obj.name = 'testonly';
        obj.activity__c = 'U';
        insert obj;
    
        SCConfigOverviewController.isTest = true;
        Test.startTest();
        try
        {
            SCConfigOverviewController con = new SCConfigOverviewController();    
            
            con.domains = 'true';
            con.parameters = 'true';
            con.resources = 'true';
            con.calendars = 'true';
            con.qualifications = 'true';
            con.getJobs();  
            con.refresh(); 
            con.getContractsWithoutPostalcode(); 
            con.getRefreshContracts(); 
            con.getEndpoint(); 
    
            /*
            SCConfigOverviewController.isTest = false;
            SCConfigOverviewController con2 = new SCConfigOverviewController();    
            con.domains = 'true';
            con.parameters = 'true';
            con.resources = 'true';
            con.calendars = 'true';
            con.qualifications = 'true';
            con.refresh();
            */ 
        }
        catch(Exception e)
        {
            debug('exception: ' + SCfwException.getExceptionInfo(e));
        }    
        
        Test.stopTest();
    }

    private static testmethod void  onMaintenanceCalcDueDate() 
    {
        Test.startTest();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        appSettings.ASE_ENDPOINT_URL__c = 'http://test.de';
        upsert appSettings;
        try
        {
            SCConfigOverviewController.isTest = true;
            SCConfigOverviewController coc = new SCConfigOverviewController();
            coc.onMaintenanceCalcDueDate();
        }
        catch(UnexpectedException e)
        {
            debug('exception: ' + SCfwException.getExceptionInfo(e));
        }    
        catch(Exception e)
        {
            debug('exception: ' + SCfwException.getExceptionInfo(e));
        }
        Test.stopTest();    
    }

    private static testmethod void  onMaintenanceCreateOrders() 
    {
        Test.startTest();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        appSettings.ASE_ENDPOINT_URL__c = 'http://test.de';
        upsert appSettings;
        try
        {
            SCConfigOverviewController.isTest = true;
            SCConfigOverviewController coc = new SCConfigOverviewController();
            coc.onMaintenanceCreateOrders();
        }
        catch(UnexpectedException e)
        {
            debug('exception: ' + SCfwException.getExceptionInfo(e));
        }    
        catch(Exception e)
        {
            debug('exception: ' + SCfwException.getExceptionInfo(e));
        }
        Test.stopTest();    
    }


    private static testmethod void  onMaintenanceAbortCalcDueDate() 
    {
        Test.startTest();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        appSettings.ASE_ENDPOINT_URL__c = 'http://test.de';
        upsert appSettings;
        try
        {
            SCConfigOverviewController.isTest = true;
            SCConfigOverviewController coc = new SCConfigOverviewController();
            coc.onMaintenanceAbortCalcDueDate();
        }
        catch(Exception e)
        {
            debug('exception: ' + SCfwException.getExceptionInfo(e));
        }    
        Test.stopTest();
    }

    private static testmethod void  onMaintenanceAbortCreateOrders() 
    {
        Test.startTest();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        appSettings.ASE_ENDPOINT_URL__c = 'http://test.de';
        upsert appSettings;
        try
        {
            SCConfigOverviewController.isTest = true;
            SCConfigOverviewController coc = new SCConfigOverviewController();
            coc.onMaintenanceAbortCreateOrders();
        }
        catch(Exception e)
        {
            debug('exception: ' + SCfwException.getExceptionInfo(e));
        }    
        Test.stopTest();
    }

    private static testmethod void  onMaintenanceRefreshCalcDueDate() 
    {
        Test.startTest();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        appSettings.ASE_ENDPOINT_URL__c = 'http://test.de';
        upsert appSettings;
        try
        {
            SCConfigOverviewController.isTest = true;
            SCConfigOverviewController coc = new SCConfigOverviewController();
            coc.onMaintenanceRefreshCalcDueDate();
        }
        catch(Exception e)
        {
            debug('exception: ' + SCfwException.getExceptionInfo(e));
        }    
        Test.stopTest();
    }

    private static testmethod void  onMaintenanceRefreshCreateOrders() 
    {
        Test.startTest();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        appSettings.ASE_ENDPOINT_URL__c = 'http://test.de';
        upsert appSettings;
        try
        {
            SCConfigOverviewController.isTest = true;
            SCConfigOverviewController coc = new SCConfigOverviewController();
            coc.onMaintenanceRefreshCreateOrders();
        }
        catch(Exception e)
        {
            debug('exception: ' + SCfwException.getExceptionInfo(e));
        }    
        Test.stopTest();
    }

    private static testmethod void  getJobsInfo() 
    {
        Test.startTest();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        appSettings.ASE_ENDPOINT_URL__c = 'http://test.de';
        upsert appSettings;
        try
        {
            ID calcDueDateJobId = SCbtcMaintenanceDueDateSet.asyncCalculateAll(0, 'no trace');
            ID createOrderJobId = SCbtcMaintenanceOrderCreate.asyncCreateAll(0, 'no trace');

            SCConfigOverviewController.isTest = true;
            SCConfigOverviewController coc = new SCConfigOverviewController();
            coc.getJobsInfo();
        }
        catch(UnexpectedException e)
        {
            debug('exception: ' + SCfwException.getExceptionInfo(e));
        }    
        catch(Exception e)
        {
            debug('exception: ' + SCfwException.getExceptionInfo(e));
        }
        Test.stopTest();
    }



    private static void debug(String msg)
    {
        System.debug('####.........' + msg);
    }
}
