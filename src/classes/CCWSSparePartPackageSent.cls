/*
 * @(#)CCWSSparePartPackageSent.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 */
 
/*
 * Implements services to process delivered spare parts
 * @author Georg Birkenheuer <gbirkenheuer@gms-online.de>
 */
global without sharing class CCWSSparePartPackageSent 
{
   
    /*
     * Called by the ERP system when spare part packaged have been sent to the 
     * engineer (after pick&pack). The deliveries are used to create the 
     * required material movements and - depending on the material management model -
     * to book the delivered parts to the engineer van stock or simply to prepare the
     * receipt confirmation by the engineer.
     * 
     * @param incomingMessage the message fields
     * @version $Revision$, $Date$
     */
    WebService static GenericServiceResponseMessageClass transmit(CCWSSparePartPackageSentClass incomingMessage)
    {
        GenericServiceResponseMessageClass retValue = new GenericServiceResponseMessageClass();
        ResponseClass rc = new ResponseClass();
        CCWSInterfaceLog il = new CCWSInterfaceLog('SAP_SPAREPART_PACKAGE_SENT', 'CCWSSparePartPackageSent', 'INBOUND','inbound', rc); 
        boolean packageItemExist = prepareTransmitRetValue(retValue, incomingMessage, il);
        if(!packageItemExist)
        {
        	addInfoLog(retValue, null, 3, 'Package without Item sent! '  );
        	il.writeInterfaceLog();
        	return retValue;
        	
        }
        try
        {
            updateSpareParts(incomingMessage, retValue, il);
        }
        catch(Exception e)
        {
            il.addExceptionInfo(SCfwException.getExceptionInfo(e)); 
            addInfoLog(retValue, null, 3, e.getMessage());  // it ought to be enough to get ResultCode = 'E101' and 
        }
        finally
        {
            il.writeInterfaceLog();
        }   
        return retValue;
    }
    
    
  
    
    /**
    * Update and process of the Spare Part Sent items
    * 
    * 1. Select all material movements for all spare part packages (create missing)
    *
    * 2. Select Storage location and plant (SCStock__c and SCPlant__c) if the stock or plant do not exist - create it/them
    * 3. Check all MaterialMovements 
    * - set severity codes
    *   - if article does not exist (abort the spare part)
    *   - if storage location of the reservation is different as of the package, update them
    * 4. call WSMaterialDelivery.process('BOOK', wsitems) to process the spare part updates 
    *  
    */
    public static GenericServiceResponseMessageClass updateSpareParts(CCWSSparePartPackageSentClass data, 
                                                                      GenericServiceResponseMessageClass retValue,
                                                                      CCWSInterfaceLog il)
    {
        // enable quick access to the items in the package
        Savepoint sp = Database.setSavepoint();
        
        List<SCMaterialMovement__c> movements2update = new  List<SCMaterialMovement__c>();
        List<SCOrderLine__c> orderLines2update = new  List<SCOrderLine__c>();
        Map<String,PackageItem> messageItems = new Map<String,PackageItem>();
        Boolean doProcess = true;
        STring msg;
        for(PackageItem pi : data.item)
        {
            messageItems.put(pi.ReferenceNumber, pi);
        }
    

        // determine the stock (or create it if missing)
        SCStock__c storageLocation = null;
        if (data.PlainFields.Plant != null && data.PlainFields.Plant.trim() != '' && data.PlainFields.StorageLocation != null && data.PlainFields.StorageLocation.trim() != '')
        {
            storageLocation  =  CCWSUtil.selectOrCreateStock(data.PlainFields.Plant, data.PlainFields.StorageLocation, 'SparePartPackageSent');
        }
        else
        {
            doProcess = false;
            msg = 'No storage location is defined, abort';
            addInfoLog(retValue, null, 3, msg);
        }

        // Read all existing material movements (reservations) based on the reference number
        if (doProcess)
        {
            Map<String,SCMaterialMovement__c> mmItems = getMaterialMovements(data, storageLocation);

            // read all articles
            Map<String, SCArticle__c> articleItems = getArticles(data);
    
    		// check whether the stock items are existing for the article and valuation types in the current stock
			//Map<String, SCStockItem__c> stockItems = getStockItems(storageLocation, data, articleItems);            
            
            List<WSMaterialDelivery.DeliveryData> wsitems = new List<WSMaterialDelivery.DeliveryData>(); 
            for(PackageItem pi : data.Item)
            {
                try
                {
                    SCMaterialMovement__c currentMovement = mmItems.get(pi.ReferenceNumber);

                    
                    SCArticle__c currentArticle = articleItems.get(pi.MaterialID);
                    Boolean insertMM = true;
                    
                    
                    if (currentArticle == null)
                    {
                        msg = 'The article [' + pi.MaterialID + '] does not exist for [' + pi.ReferenceNumber + ']';
                        debug(msg);
                        addInfoLog(retValue, pi, 3, msg);
                        insertMM = false;
                        //throw new SCfwException(msg);
                    } 
                    else if (currentMovement == null)
                    {             
                        // if the original movement does not exist or the stock is different from the original reservation 
                        //msg = 'The original reservation material movement [' + pi.ReferenceNumber + '] does not exist for stock [' + StorageLocation.Name + ']';
                        //debug(msg);
                        //addInfoLog(retValue, pi, 2, msg);
                        //throw new SCfwException(msg);
                    } 
                    else
                    {
                        if (currentMovement.Article__r.name != pi.MaterialID)
                        {
                            // Replacing article is delivered
                            msg = 'Warning: reservation [' + pi.ReferenceNumber + '] contains article ' + currentMovement.Article__r.Name + ' but article [' + pi.MaterialID + '] is delivered';
                            msg += '\n The reservation is updated with new article!';
                            debug(msg);
                            addInfoLog(retValue, pi, 2, msg);
                            // Update the article at the referenced movement
                            currentMovement.Article__c = currentArticle.id;
                            movements2update.add(currentMovement);
                            
                            // if the movement has an corresponding order line
                            // update the order line
                            if(currentMovement.OrderLine__c != null)
                            {
                            	SCOrderLine__c ol = [select id, article__c from SCOrderLine__c where id =: currentMovement.OrderLine__c ];
                            	ol.article__c = currentArticle.id;
                            	orderLines2update.add(ol);
                            }
                            
                            
                        }   
                    }  
        
                    // set MaterialMovement values
                    if(insertMM)
                    {
                        debug('update current movement for the spare part ' + pi.ItemID);
            
                        WSMaterialDelivery.DeliveryData wsitem = new WSMaterialDelivery.DeliveryData();
                        wsitem.stockiD = StorageLocation.Id;
                        wsitem.articleid = currentArticle != null ? currentArticle.ID : null;    // TODO###
                        wsitem.valuationtype  = pi.ValuationType;
                        wsitem.type = '5207';    // delivery
                        wsitem.deliverydate =  date.today();
                        wsitem.deliverynoteno = data.PlainFields.PackageNumber; // GMSNA 22.01.2013 not required + '-' + pi.itemid;
                        wsitem.matmoveid = currentMovement  != null ? currentMovement.id : null;   
                        //try
                        wsitem.qty = decimal.valueOf(pi.Quantity); //### TODO try catch ?
                        //catch
                        wsitem.serialno = pi.batch;
                        
                        wsitems.add(wsitem);
                        msg = 'The spare part ' + pi.MaterialID +'/'+(currentMovement  != null ? currentMovement.id : null)+' ('
                            + pi.ValuationType+ ') for the stock '+ storageLocation.Name 
                            + ' on the plant ' + data.PlainFields.Plant + ' with the ID ' + pi.ItemID + ' was updated by ' +pi.Quantity + ' ' + pi.UnitOfMeasure +'.';
                        addInfoLog(retValue, pi, 0, msg);
                        il.incrementCount(1);
                    }
                    else
                    {
                        msg = 'The current Spare Part ' + pi.ItemID + ' is not inserted!';
                        debug(msg);
                        addInfoLog(retValue, pi, 3, msg);
                    }
                }
                catch (System.TypeException te)
                {
                    msg = 'The Quantity ['+ pi.Quantity + '] is not an allowed number. \n' + te.getMessage();
                    debug(msg);
                    il.addExceptionInfo(SCfwException.getExceptionInfo(te)); 
                    addInfoLog(retValue, pi, 3, 'StackTrace: '+msg);
                }
                catch(Exception e)
                {
                    String prevMsg = e.getMessage();
                    String newMsg = 'fillAndSendERPData: ' + prevMsg;
                    debug(newMsg);
                    // ### throw ???
                    // append log to message
                    e.setMessage(newMsg);
                    il.addExceptionInfo(SCfwException.getExceptionInfo(e)); 
                    addInfoLog(retValue, pi, 3, 'StackTrace: '+e.getMessage());    // it ought to be enough to get ResultCode = 'E101' and 
                    throw e;
                }
            } // for
            try
            {
	            if(movements2update.size() > 0)
	            {
	            	// update the original movement with the delivered article
	            	update movements2update;
	            }
	            if(orderLines2update.size() > 0)
	            {
	            	// update the original orderline with the delivered article
	            	update orderLines2update;
	            }
            }
            catch(Exception e)
            {
                    String prevMsg = e.getMessage();
                    String newMsg = 'Update on materialmovemnts failed: ' + prevMsg;
                    debug(newMsg);
                    e.setMessage(newMsg);
                    il.addExceptionInfo(SCfwException.getExceptionInfo(e)); 
                    addInfoLog(retValue, null, 3, 'StackTrace: '+e.getMessage());   
                    Database.rollback(sp);
                    throw e;
            }
            List<WSMaterialDelivery.DeliveryData> result; 
            try
            {
                result = WSMaterialDelivery.process('BOOK', wsitems);
            }
            catch(Exception e)
            {
                addInfoLog(retValue, null, 3, 'WSMaterialDelivery Error: '+SCfwException.getExceptionInfo(e));
            }
            
        }   
        return retValue;
    }//  updateSpareParts
    
   /*
    * Select the corresponding MaterialMovements for the ReferenceNumbers of the spare part package sent message.
    * If a MaterialMovement is missing for a ReferenceNumber, it is created
    * @param incomingMessage the message fields
    * @return List<SCMaterialMovement__c> a list of material movements
    *
    *   1. build set of SCMaterialMovement__c references
    *   2. select all MaterialMovements
    *   3. build set of found references
    *   4. compare references and found references and create MaterialMovements of reference was not found
    */
    public static Map<String,SCMaterialMovement__c> getMaterialMovements(CCWSSparePartPackageSentClass incomingMessage, SCStock__c stock)
    {
        Set<String> references = new Set<String>();
        for(PackageItem it : incomingMessage.item)
        {
            if (it.ReferenceNumber != null && it.ReferenceNumber.trim() != '')
            {
                references.add(it.ReferenceNumber);
            }
        }
                
        List<SCMaterialMovement__c> mmItems =  [SELECT ID, Name, Article__c, Article__r.id, Article__r.Name, Stock__r.Name, Stock__c, OrderLine__c FROM 
                                                SCMaterialMovement__c where Name in: references and stock__c = :stock.id for update];
        
        Map<String,SCMaterialMovement__c> mapMsgMM= new Map<String,SCMaterialMovement__c>();
        
        Set<String> referencesFound = new Set<String>();
        for(SCMaterialMovement__c mm : mmItems )
        {
            referencesFound.add(mm.Name);
            mapMsgMM.put(mm.Name,mm);
        }
                   
        return mapMsgMM; 
    }
    
    
    /*
    * Select the corresponding Articles for the ReferenceNumbers of the spare part package sent message.
    * @param incomingMessage the message fields
    * @return Map<String,SCArticle__c> a list of articles
    *
    *   build map of references,SCArticle__c  
    *   return map
    */
    public static Map<String,SCArticle__c> getArticles(CCWSSparePartPackageSentClass incomingMessage)
    {
        Set<String> references = new Set<String>();
        Map<String,SCArticle__c> mapMsgArticle= new Map<String,SCArticle__c>();
        
        for(PackageItem it : incomingMessage.item)
        {
            if (it.MaterialID != null && it.MaterialID.trim() != '')
            {
                references.add(it.MaterialID);
            }
            
        }
                
        List<SCArticle__c> articles =  [SELECT ID, Name, MobileEnabled__c FROM SCArticle__c where Name =: references];
        List<SCArticle__c> articles2update = new List<SCArticle__c>();
        Set<String> referencesFound = new Set<String>();
        for(SCArticle__c art : articles )
        {
            referencesFound.add(art.Name);
            mapMsgArticle.put(art.Name,art);
            if (art.MobileEnabled__c == false)
            {
            	art.MobileEnabled__c = true;
            	articles2update.add(art);
            }
            
        }
        
        
        
        // create missing articles        
        List<SCArticle__c> articles2create = new List<SCArticle__c>();
        for(PackageItem it : incomingMessage.item)
        {
            if(it.MaterialID != null &&  it.MaterialID.trim() != '' )
            {
                 if(!mapMsgArticle.containsKey(it.MaterialID))
                 {
                    // create article
                     SCArticle__c art =  CCWSUtil.createArticle(it.MaterialID);
                     art.MobileEnabled__c = true;
                     articles2create.add(art);
                     mapMsgArticle.put(art.Name,art);
                 }
            }           
        } 

        if(articles2create.size() > 0)
        {
            insert(articles2create);
        }
        
        if(articles2update.size() > 0)
        {
            update(articles2update);
        }
        
            
        return mapMsgArticle; 
    }
    
     /*
    * Select the corresponding StockItem for the Articles of the spare part package sent message.
    * @param storageLocaiton 	the Stock
    * @param incomingMessage    the message
    * @param articleItems 		a map of the articles
    * @return Map<String,SCStockItem__c> a list of stock items
    *
    *   build map of references,SCArticle__c  
    *   return map
    */
   public static  Map<String, SCStockItem__c> getStockItems(SCStock__c storageLocaiton,  CCWSSparePartPackageSentClass incomingMessage,  Map<String, SCArticle__c> articleItems)
   {
   		// Build the stockSelectors by a list of the given selectors
   		// As the articles do not contain an vlaluation type, we nee the package item of the 
   		// incoming message and the map corresponding articles that are identified by the 
   		// MaterialID and the storageLocaiton(Stock)
   		
   		List<SCArticle__c> articleList = new List<SCArticle__c>();
   		articleList.addAll(articleItems.values());
   		Map<String,PackageItem> packageItemBySelectors = new Map<String,PackageItem>();
   		for(PackageItem it : incomingMessage.item)
        {
            if (it.MaterialID != null && it.MaterialID.trim() != '')
            {
               SCArticle__c article = articleItems.get(it.MaterialID);
               String selector = storageLocaiton.Name+'-'+article.Name+'-'+it.ValuationType; 
               packageItemBySelectors.put(selector,it);
            }
            
        }


        List<String> utilstockItemSelectors = new List<String>();
        utilstockItemSelectors.addAll(packageItemBySelectors.keySet());
        
		if(utilstockItemSelectors.size() <= 0)
		{
			return null;
		}
	

   		List<SCStockItem__c> stockItems =  [SELECT ID, Name, ValuationType__c, Article__c, Stock__c, util_StockItemSelector__c FROM SCStockItem__c 
   			where util_StockItemSelector__c in: utilstockItemSelectors];
   			
        List<SCStockItem__c> stockItems2update = new List<SCStockItem__c>();
        Map<String,SCStockItem__c> mapMsgStockItems= new Map<String,SCStockItem__c>();
        Set<String> referencesFound = new Set<String>();
        for(SCStockItem__c stockItem : stockItems )
        {
            referencesFound.add(stockItem.util_StockItemSelector__c);
            mapMsgStockItems.put(stockItem.util_StockItemSelector__c,stockItem);
            
        }
   		
   		  // create missing stock items        
        List<SCStockItem__c> stockItems2create = new List<SCStockItem__c>();
        for(String selector : utilstockItemSelectors)
        {
        	if(!mapMsgStockItems.containsKey(selector))
            {
            // create stockItem
            SCStockItem__c stockItem =  new SCStockItem__c();
            PackageItem it = packageItemBySelectors.get(selector);
            SCArticle__c art = articleItems.get(it.MaterialID);
			stockItem.Article__c = art.ID;
			stockItem.ValuationType__c = it.ValuationType;
			stockItem.Stock__c = storageLocaiton.id;
			stockItem.ERPQty__c = 0;
			stockItems2create.add(stockItem);
			// put in map and list
            mapMsgStockItems.put(stockItem.util_StockItemSelector__c,stockItem);
            stockItems2create.add(stockItem);
   	        }
         
        } 

        if(stockItems2create.size() > 0)
        {
            insert(stockItems2create);
        }
   		
   		return mapMsgStockItems;
   }
   
  
    
   /*
    *
    *   Modify the severity code
    */
    public static void modifyMaxSeverityCode(GenericServiceResponseMessageClass retValue, Integer severityCode)
    {
        String prevMaxSeverityCode = retValue.Log.MaximumLogItemSeverityCode;
        Integer nPrevMaxSeverityCode = Integer.valueOf(prevMaxSeverityCode);
        if(nPrevMaxSeverityCode < severityCode)
        {
            retValue.Log.MaximumLogItemSeverityCode = '' + severityCode;
        }
    }


   /*
    * Used to prepare the web service response.
    */
    public static boolean prepareTransmitRetValue(GenericServiceResponseMessageClass retValue, 
                                        CCWSSparePartPackageSentClass incomingMessage,
                                        CCWSInterfaceLog il)
    {
        retValue.MessageHeader = new MessageHeaderClass();
        retValue.MessageHeader.MessageID = incomingMessage.MessageHeader.MessageID;
        retValue.References = new ReferencesRootClass();
        retValue.References.Operation = 'SparePartPackageSent';
        //retValue.References.ExternalID = incomingMessage.item[0].ItemID;
        retValue.Log = new LogClass();
        retValue.Log.MaximumLogItemSeverityCode = '0';
        retValue.Log.Item = new List<LogItemClass>();

        il.addInputData('Input from SAP: ', incomingMessage);
        il.setMessageID(retValue.MessageHeader.MessageID);
        il.addOutputData('Output from ClockPort: ', retValue);
        
        if(incomingMessage.Item == null || incomingMessage.Item.size() <= 0)
        {
        	retValue.References.ExternalID = 'noPackageContent';
            retValue.References.References = new ReferencesContainer();
            retValue.References.References.Reference = new ReferenceItem[1];
            retValue.References.References.Reference[0] = new ReferenceItem();
            retValue.References.References.Reference[0].ExternalID = 'NoPackageItemFound';
            retValue.References.References.Reference[0].IDType = 'SparePartPackageSent';
            retValue.References.References.Reference[0].ReferenceID = 'NoReference';
            return false;
        }
        else if (incomingMessage.Item.size() == 1)
        {
            retValue.References.ExternalID = incomingMessage.Item[0].ItemID;
            retValue.References.References = new ReferencesContainer();
            retValue.References.References.Reference = new ReferenceItem[1];
            retValue.References.References.Reference[0] = new ReferenceItem();
            retValue.References.References.Reference[0].ExternalID = incomingMessage.item[0].ItemID;
            retValue.References.References.Reference[0].IDType = 'SparePartPackageSent';
            retValue.References.References.Reference[0].ReferenceID = incomingMessage.item[0].ReferenceNumber;
        }
        else if (incomingMessage.item.size() > 1)
        {
            retValue.References.ExternalID = incomingMessage.item[0].ItemID;
            retValue.References.References = new ReferencesContainer();
            retValue.References.References.Reference = new ReferenceItem[incomingMessage.item.size()];
            for(Integer i = 0; i < incomingMessage.item.size(); i++ )
            {
                retValue.References.References.Reference[i] = new ReferenceItem();
                retValue.References.References.Reference[i].ExternalID = incomingMessage.item[i].MaterialID;
                retValue.References.References.Reference[i].IDType = 'SparePartPackageSent';
                retValue.References.References.Reference[i].ReferenceID = incomingMessage.item[0].ReferenceNumber;
            }
        }
        else
        {
            retValue.References.ExternalID = 'No SparePartPackage Items found!';
            retValue.References.References = new ReferencesContainer();
            retValue.References.References.Reference = new ReferenceItem[1];
            retValue.References.References.Reference[0] = new ReferenceItem();
            retValue.References.References.Reference[0].ExternalID = 'No SparePartPackage Items found!';
            retValue.Log.MaximumLogItemSeverityCode = '2';
            addInfoLog(retValue, null, 2, 'No SparePartPackage Items found!');
        }
        return true;
    }
    
   /*
    * Set the response items for a spare part package 
    */
    public static void addInfoLog(GenericServiceResponseMessageClass retValue, PackageItem pi, Integer severityCode, String note  )
    {
            LogItemClass li = new LogItemClass();
            modifyMaxSeverityCode(retValue, SeverityCode);
            if(pi != null)
            {
                li.ExternalID = pi.ItemID;
            }   
            li.Note = note;
            li.TypeID = 'SparePartMessage';
            li.SeverityCode = '' + SeverityCode;
            retValue.Log.Item.add(li);
    }
    
    /**
     * @author GeorgB
     *
     */
    public class ResponseClass  implements CCWSGenericResponseInterface
    {
        public String getResultCode(Object response)
        {
            String retValue = 'E000';
            Integer severityCode = getLocalMaxSeverityCode(response);
            if(severityCode == 0)
            {
                retValue = 'E000';
            }
            if(severityCode == 1)
            {
                retValue = 'E001';
            }
            else if(severityCode == 2)
            {
                retValue = 'E002';
            }
            else if(severityCode >= 3)
            {
                retValue = 'E101';
            }
            return retValue;        
        }
    
        public String getResultInfoForAll(Object response)
        {
            String retValue = '';
            if(response != null)
            {
                String resultCode = getResultCode(response);
                if(resultCode == 'E000')
                {
                    retValue = 'Success';
                }
                else if(resultCode == 'E001')
                {
                    retValue = 'Success with Info: ';
                }
                else if(resultCode == 'E002')
                {
                    retValue = 'Success with Warning: ';
                }
                else
                {
                    retValue = 'Error: ';
                }
                LogItemClass[] logItemArr = 
                    ((GenericServiceResponseMessageClass)response).Log.Item;
                Integer i = 0;
                for(LogItemClass li: logItemArr)
                {
                    if(i > 0)
                    {
                        retValue += ', ';
                    }
                    if(resultCode == 'E000')
                    {
                        if(li.SeverityCode == '0')
                        {
                            retValue += li.Note;
                            i++; 
                        }    
                    }
                    else if(resultCode == 'E001')
                    {
                        if(li.SeverityCode == '1')
                        {
                            retValue += li.Note;
                            i++; 
                        }    
                    }
                    else if(resultCode == 'E002')
                    {
                        if(li.SeverityCode == '2')
                        {
                            retValue += li.Note;
                            i++; 
                        }    
                    }
                    else
                    {
                        if(li.SeverityCode == '3')
                        {
                            retValue += li.Note;
                            i++; 
                        }    
                    }
                }
            }
            return retValue;
        }
    
        public Integer getLocalMaxSeverityCode(Object response)
        {
            LogItemClass[] logItemArr = 
                    ((GenericServiceResponseMessageClass)response).Log.Item;
            Integer retValue = 0;
            for(LogItemClass li: logItemArr)
            {
                debug('li: ' + li);
                String loopSeverityCode = '0';
                Integer severityCode = 0;
                if(li.SeverityCode != null)
                {
                    loopSeverityCode = li.SeverityCode.trim();
                    debug('loopSeverityCode: ' + loopSeverityCode); 
                    severityCode = Integer.valueOf(loopSeverityCode);
                }
                if(retValue < severityCode)
                {
                    retValue = severityCode;
                }    
            }
            debug('getLocalMaxSeverityCode: retValue: ' + retValue);
            return retValue;
        }                          
    }
    

   /*
    * Used for Debug messages.
    */
    public static void debug(String msg)
    {
        System.debug('###...' +  msg);        
    }

    global class CCWSSparePartPackageSentClass
    {
        WebService MessageHeaderClass MessageHeader;
        WebService PlainFieldsClass PlainFields;
        WebService PackageItem[] Item;
    }

    
    global class MessageHeaderClass
    {
        WebService String MessageID;
        WebService String MessageUUID;
    } 
    
    global class PackageItem
    {
        WebService String ItemID;
        WebService String MaterialDocumentNumber;
        WebService String MaterialDocumentYear;
        WebService String MaterialID;
        WebService String UnitOfMeasure;
        WebService String Quantity;
        WebService String ValuationType;
        WebService String Batch;
        WebService String ReferenceNumber;
    }
    
    global class PlainFieldsClass
    {
        WebService String PackageNumber;
        WebService String Plant;
        WebService String StorageLocation;
        WebService String UnloadingPoint;
        WebService String Status;

        
    }
    
    global class GenericServiceResponseMessageClass
    {
        WebService MessageHeaderClass MessageHeader;
        WebService ReferencesRootClass References;
        WebService LogClass Log;    
    }   

    global class ReferencesRootClass
    {
        WebService String ExternalID;
        WebService String Operation;
        WebService ReferencesContainer References;
    }

    global class ReferencesContainer
    {
        WebService ReferenceItem[] Reference;
    }

    global class ReferenceItem
    {
        WebService String ReferenceID;
        WebService String IDType;
        WebService String ExternalID;
    }
    
    global class LogClass
    {
        WebService String MaximumLogItemSeverityCode;
        WebService LogItemClass[] Item;
        
    }
    
    global class LogItemClass
    {
        WebService String TypeID;
        WebService String SeverityCode;
        WebService String Note;
        WebService String ExternalID;
    }
    

}
