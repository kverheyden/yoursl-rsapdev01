public with sharing class CdeOrderInstallationOrderController {

    public ApexPages.StandardController controller {get;set;}
    public List<SCOrderItem__c> orderItems {get;set;}
    public Contact contactPerson {get;set;}
    public SCOrder__c order {get;set;}
    public Integer maxNumOfOrderItems {
        get{
        	return order.OrderItem__r.size();    
        }
    }
    
    public CdeOrderInstallationOrderController(ApexPages.StandardController stdCtlr){
        
       	order = (SCOrder__c) stdCtlr.getRecord();
        
        List<Contact> contacts = [Select ID, FirstName, LastName, Phone from Contact where AccountID = :order.utilAccountCA__c and Firstname = 'Verkaufsberater'];
        if(!contacts.isEmpty())
            contactPerson = contacts.get(0);        
    }
    
}
