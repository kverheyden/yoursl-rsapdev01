@isTest
private class AccountAfterInsertCLSTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
    	/*
		- Marketing Attribute
		- CdeOrder__c.Request__c	=> Account__c.CdeOrder__c
		- Visitation__c.Request__c => Visitation__c.Account__c
		- RedSurveyResult__c.Request__c => RedSurveyResult__c.Account__c
		*/
        
        /* CCFSFA-1432 Duc Nguyen Tien | Start */
		String recTypte_update;
		recTypte_update = [Select Id From RecordType Where SobjectType = 'Request__c' AND DeveloperName = 'CustomerMasterDataUpdate' LIMIT 1].Id;
		                                    
        SalesAppSettings__c settingSalesApp = new SalesAppSettings__c();
        settingSalesApp.Name = 'RequestMasterDataUpdateRecordTypeId';
        settingSalesApp.Value__c = recTypte_update;
        insert settingSalesApp;
        /* CCFSFA-1432 Duc Nguyen Tien | End */
        
		system.debug('MarketingAttribute');
		// create MarketingAttribute
		MarketingAttribute__c tmpMA1 = new MarketingAttribute__c();
		tmpMA1.UniqueKey__c = 'tmpMA1';
		insert tmpMA1;
		
        system.debug('Request');
        // create Request
        Request__c tmpReq = new Request__c();
        tmpReq.MarketingAttributeIDsCSV__c = tmpMA1.Id ;
        insert tmpReq;
        
        system.debug('CdeOrder__c');
        // create CdeOrder__c
        CdeOrder__c tmpCDEO = new CdeOrder__c();
        tmpCDEO.Request__c = tmpReq.Id;
        insert tmpCDEO;
        
        system.debug('SalesOrder__c');
        // create SalesOrder__c
        SalesOrder__c tmpSO = new SalesOrder__c();
        tmpSO.Request__c  = tmpReq.Id;
        insert tmpSO;
        
        system.debug('SalesOrderItem__c');
        SalesOrderItem__c tmpSOI 	= new SalesOrderItem__c();
        tmpSOI.SalesOrder__c 		= tmpSO.Id;
        tmpSOI.Quantity__c			= 3;
        insert tmpSOI;
        
        system.debug('Visitation__c');
        // create Visitation__c
        Visitation__c tmpVisitation = new Visitation__c();
        tmpVisitation.Request__c = tmpReq.Id;
        
        insert tmpVisitation;
        
        system.debug('RedSurveyResult__c');
        // create RedSurveyResult__c
        RedSurveyResult__c tmpRSR = new RedSurveyResult__c();
        tmpRSR.Request__c = tmpReq.Id;
        insert tmpRSR;
        
        system.debug('Account');
        // create Account
        Account tmpAcc = new Account(); 
        string tmpReqIdStr = tmpReq.Id;
        tmpAcc.Name	= 'Test Account';
        tmpAcc.BillingCountry__c = 'Deutschland';
        tmpAcc.RequestID__c = tmpReqIdStr.left(15);
        tmpAcc.ID2__c = '12345';
        //tmpAcc.RequestID__c = tmpReq.Id;
        
        insert tmpAcc;
        
        List<Request__c> ListInsertRequest = new List<Request__c>();
        // create second Request
        Request__c tmpReq2 = new Request__c();
        tmpReq2.RelatedAccount__c=tmpAcc.ID;
        insert tmpReq2;
        Request__c tmpReq3 = new Request__c();
        tmpReq3.RelatedAccount__c=tmpAcc.ID;
        insert tmpReq3;
  
        tmpAcc.RequestID__c='112345';
        update tmpAcc;
        tmpAcc.RequestID__c=tmpReq2.Id;
        update tmpAcc;
    }
    
    

}
