/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class cc_cceag_ctrl_test_CartAlternatives {

    static testMethod void testFetchAlternateItems() {
        List<ccrz__E_Product__c> ps = new List<ccrz__E_Product__c>{
            new ccrz__E_Product__c(Name='Product1', ccrz__Sku__c='sku1', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12),
            new ccrz__E_Product__c(Name='Product2', ccrz__Sku__c='sku2', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12),
            new ccrz__E_Product__c(Name='Product3', ccrz__Sku__c='sku3', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12),
            new ccrz__E_Product__c(Name='Product4', ccrz__Sku__c='sku4', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12),
            
            new ccrz__E_Product__c(Name='Product1_1', ccrz__Sku__c='sku1_1', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12),
            new ccrz__E_Product__c(Name='Product1_2', ccrz__Sku__c='sku1_2', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12),
            new ccrz__E_Product__c(Name='Product1_3', ccrz__Sku__c='sku1_3', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12)
            
        };
        insert ps;
        
        List<ccrz__E_RelatedProduct__c> related = new List<ccrz__E_RelatedProduct__c> {
            new ccrz__E_RelatedProduct__c(ccrz__Product__r=new ccrz__e_product__c(ccrz__sku__c='sku1'), ccrz__RelatedProduct__r = new ccrz__e_product__c(ccrz__sku__c='sku1_1'), ccrz__RelatedProductType__c='CrossSell' ),
            new ccrz__E_RelatedProduct__c(ccrz__Product__r=new ccrz__e_product__c(ccrz__sku__c='sku1'), ccrz__RelatedProduct__r = new ccrz__e_product__c(ccrz__sku__c='sku1_2'), ccrz__RelatedProductType__c='CrossSell' ),
            new ccrz__E_RelatedProduct__c(ccrz__Product__r=new ccrz__e_product__c(ccrz__sku__c='sku1'), ccrz__RelatedProduct__r = new ccrz__e_product__c(ccrz__sku__c='sku1_3'), ccrz__RelatedProductType__c='CrossSell' )};
        insert related;
        
        list<ccrz__E_ProductInventoryItem__c> prodInvItems = new list<ccrz__E_ProductInventoryItem__c> {
            new ccrz__E_ProductInventoryItem__c(ccrz__Status__c='In Stock', ccrz__QtyAvailable__c=100, ccrz__InventoryLocationCode__c='', ccrz__ProductItem__r = new ccrz__E_Product__c(ccrz__Sku__c='sku1')),
            new ccrz__E_ProductInventoryItem__c(ccrz__Status__c='In Stock', ccrz__QtyAvailable__c=100, ccrz__InventoryLocationCode__c='', ccrz__ProductItem__r = new ccrz__E_Product__c(ccrz__Sku__c='sku2')),
            new ccrz__E_ProductInventoryItem__c(ccrz__Status__c='In Stock', ccrz__QtyAvailable__c=100, ccrz__InventoryLocationCode__c='', ccrz__ProductItem__r = new ccrz__E_Product__c(ccrz__Sku__c='sku3')),
            new ccrz__E_ProductInventoryItem__c(ccrz__Status__c='In Stock', ccrz__QtyAvailable__c=100, ccrz__InventoryLocationCode__c='', ccrz__ProductItem__r = new ccrz__E_Product__c(ccrz__Sku__c='sku4')),
            new ccrz__E_ProductInventoryItem__c(ccrz__Status__c='In Stock', ccrz__QtyAvailable__c=100, ccrz__InventoryLocationCode__c='', ccrz__ProductItem__r = new ccrz__E_Product__c(ccrz__Sku__c='sku1_1')),
            new ccrz__E_ProductInventoryItem__c(ccrz__Status__c='In Stock', ccrz__QtyAvailable__c=100, ccrz__InventoryLocationCode__c='', ccrz__ProductItem__r = new ccrz__E_Product__c(ccrz__Sku__c='sku1_2')),
            new ccrz__E_ProductInventoryItem__c(ccrz__Status__c='In Stock', ccrz__QtyAvailable__c=100, ccrz__InventoryLocationCode__c='', ccrz__ProductItem__r = new ccrz__E_Product__c(ccrz__Sku__c='sku1_3'))
        };
    
        insert prodInvItems; 
        
        List<ccrz__E_ProductMedia__c> mediaList = new List<ccrz__E_ProductMedia__c>();
        for(ccrz__E_Product__c thisProd : ps){
            ccrz__E_ProductMedia__c media = new ccrz__E_ProductMedia__c();
            media.ccrz__Product__c = thisProd.id;
            media.ccrz__URI__c = 'media URI Dummy';
            media.ccrz__EndDate__c = System.today();
            media.ccrz__StartDate__c = System.today();
            media.ccrz__ProductMediaSource__c = 'Attachment';
            media.ccrz__MediaType__c = 'Product Search Image';
            media.ccrz__Enabled__c = true;
            mediaList.add(media);
        }
        insert mediaList;
        list<Attachment> attachments = new List<Attachment>();
        for(ccrz__E_ProductMedia__c m : mediaList) {
        	Attachment a = new Attachment(Name='test.txt', Description = 'en_US',  ParentId = m.id);
			a.Body = Blob.valueOf('test');
			attachments.add(a);
        }
		insert attachments;
		
		Test.startTest();
        //test default constructor
        cc_cceag_ctrl_CartAlternatives ca = new cc_cceag_ctrl_CartAlternatives();
        
        
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        ctx.portalUserId = UserInfo.getUserId();
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz__E_Product__c p = [select id from ccrz__E_Product__c where ccrz__sku__c = 'sku1' limit 1];
        List<String> prodIds = new List<String> {p.id};
        Map<String, List<cc_cceag_bean_Product>> result = cc_cceag_ctrl_CartAlternatives.fetchAlternateItems(ctx, prodIds, null);
        system.debug(result);
        system.assert(result != null);
        Test.stopTest();
    }
    
    static testMethod void testPostProc() {
    	ccrz__E_Cart__c cart = new ccrz__E_Cart__c(ccrz__SessionID__c = 'test session');
    	insert cart;
    	cart = [select id, ccrz__EncryptedId__c from ccrz__E_Cart__c where id = :cart.Id];
    	ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
    	ctx.currentCartID = cart.ccrz__EncryptedId__c;
    	Test.startTest();
    	ccrz.cc_RemoteActionResult result = cc_cceag_ctrl_CartAlternatives.postProc(ctx);
    	system.assert(result.success);
    	//run exception flow
    	ctx.currentCartID = null;
    	result = cc_cceag_ctrl_CartAlternatives.postProc(ctx);
    	system.assert(result.success == false);
    	Test.stopTest();
    }
    static testMethod void testReplaceItem() {
    	ccrz__E_Cart__c cart = new ccrz__E_Cart__c(
			ccrz__SessionID__c = 'test session',
			ccrz__storefront__c='DefaultStore',
			ccrz__RequestDate__c = Date.today(),
			ccrz__ActiveCart__c = true,
			ccrz__CartStatus__c = 'Open',
			ccrz__CartType__c = 'Cart'
		);
		insert cart;
		ccrz__E_Product__c p1 = new ccrz__E_Product__c(Name='Product1', ccrz__Sku__c='sku1', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12);
		ccrz__E_Product__c p2 = new ccrz__E_Product__c(Name='Product2', ccrz__Sku__c='sku2', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12);
		ccrz__E_Product__c p3 = new ccrz__E_Product__c(Name='Product3', ccrz__Sku__c='sku3', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12);
		ccrz__E_Product__c p4 = new ccrz__E_Product__c(Name='Product4', ccrz__Sku__c='sku4', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12);
		
		List<ccrz__E_Product__c> ps = new List<ccrz__E_Product__c>{
			p1,p2,p3,p4
		};
		insert ps;
		
		List<ccrz__E_CartItem__c> cartItems = new List<ccrz__E_CartItem__c> {
			new ccrz__E_CartItem__c(ccrz__Quantity__c=1, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku1'), ccrz__cart__c=cart.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0, ccrz__cartItemType__c='Major'),
			new ccrz__E_CartItem__c(ccrz__Quantity__c=1, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku2'), ccrz__cart__c=cart.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0, ccrz__cartItemType__c='Major'),
			new ccrz__E_CartItem__c(ccrz__Quantity__c=1, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku3'), ccrz__cart__c=cart.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0, ccrz__cartItemType__c='Major')
		};
		insert cartItems;
		
		cart = [select id, ccrz__EncryptedId__c from ccrz__E_Cart__c where id = :cart.Id];
		
    	Test.startTest();
    	boolean result = cc_cceag_ctrl_CartAlternatives.replaceItem('DefaultStore', userInfo.getUserId(), cart.ccrz__EncryptedId__c, p1.id, p2.id, 3);
    	ccrz__E_CartItem__c cartItem = [select ccrz__quantity__c from ccrz__E_CartItem__c where ccrz__Product__c = :p2.id limit 1];
    	system.assertEquals(4, cartItem.ccrz__quantity__c);
    	
    	result = cc_cceag_ctrl_CartAlternatives.replaceItem('DefaultStore', userInfo.getUserId(), cart.ccrz__EncryptedId__c, p2.id, p1.id, 3);
    	cartItem = [select ccrz__quantity__c from ccrz__E_CartItem__c where ccrz__Product__c = :p1.id limit 1];
    	system.assertEquals(3, cartItem.ccrz__quantity__c);
    	Test.stopTest();
    }
    
    static testMethod void testReplaceItem2() {
    	ccrz__E_Cart__c cart = new ccrz__E_Cart__c(
			ccrz__SessionID__c = 'test session',
			ccrz__storefront__c='DefaultStore',
			ccrz__RequestDate__c = Date.today(),
			ccrz__ActiveCart__c = true,
			ccrz__CartStatus__c = 'Open',
			ccrz__CartType__c = 'Cart'
		);
		insert cart;
		ccrz__E_Product__c p1 = new ccrz__E_Product__c(Name='Product1', ccrz__Sku__c='sku1', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12);
		ccrz__E_Product__c p2 = new ccrz__E_Product__c(Name='Product2', ccrz__Sku__c='sku2', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12);
		ccrz__E_Product__c p3 = new ccrz__E_Product__c(Name='Product3', ccrz__Sku__c='sku3', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12);
		ccrz__E_Product__c p4 = new ccrz__E_Product__c(Name='Product4', ccrz__Sku__c='sku4', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12);
		
		List<ccrz__E_Product__c> ps = new List<ccrz__E_Product__c>{
			p1,p2,p3,p4
		};
		insert ps;
		
		List<ccrz__E_CartItem__c> cartItems = new List<ccrz__E_CartItem__c> {
			new ccrz__E_CartItem__c(ccrz__Quantity__c=1, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku1'), ccrz__cart__c=cart.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0, ccrz__cartItemType__c='Major'),
			new ccrz__E_CartItem__c(ccrz__Quantity__c=1, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku2'), ccrz__cart__c=cart.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0, ccrz__cartItemType__c='Major'),
			new ccrz__E_CartItem__c(ccrz__Quantity__c=1, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku3'), ccrz__cart__c=cart.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0, ccrz__cartItemType__c='Major')
		};
		insert cartItems;
		
		cart = [select id, ccrz__EncryptedId__c from ccrz__E_Cart__c where id = :cart.Id];
		
    	Test.startTest();
    	boolean result = cc_cceag_ctrl_CartAlternatives.replaceItem('DefaultStore', userInfo.getUserId(), cart.ccrz__EncryptedId__c, p2.id, p1.id, 3);
    	ccrz__E_CartItem__c cartItem = [select ccrz__quantity__c from ccrz__E_CartItem__c where ccrz__Product__c = :p1.id limit 1];
    	system.assertEquals(4, cartItem.ccrz__quantity__c);
    	Test.stopTest();
    }

    static testMethod void testCartEmail() {
        ccrz__E_Cart__c cart = new ccrz__E_Cart__c(
            ccrz__SessionID__c = 'test session',
            ccrz__storefront__c='DefaultStore',
            ccrz__RequestDate__c = Date.today(),
            ccrz__ActiveCart__c = true,
            ccrz__CartStatus__c = 'Open',
            ccrz__CartType__c = 'Cart'
        );
        insert cart;
        ccrz__E_Product__c p1 = new ccrz__E_Product__c(Name='Product1', ccrz__Sku__c='sku1', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12);
        ccrz__E_Product__c p2 = new ccrz__E_Product__c(Name='Product2', ccrz__Sku__c='sku2', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12);
        ccrz__E_Product__c p3 = new ccrz__E_Product__c(Name='Product3', ccrz__Sku__c='sku3', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12);
        ccrz__E_Product__c p4 = new ccrz__E_Product__c(Name='Product4', ccrz__Sku__c='sku4', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12);
        
        List<ccrz__E_Product__c> ps = new List<ccrz__E_Product__c>{
            p1,p2,p3,p4
        };
        insert ps;
        
        List<ccrz__E_CartItem__c> cartItems = new List<ccrz__E_CartItem__c> {
            new ccrz__E_CartItem__c(ccrz__Quantity__c=1, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku1'), ccrz__cart__c=cart.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0, ccrz__cartItemType__c='Major'),
            new ccrz__E_CartItem__c(ccrz__Quantity__c=1, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku2'), ccrz__cart__c=cart.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0, ccrz__cartItemType__c='Major'),
            new ccrz__E_CartItem__c(ccrz__Quantity__c=1, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku3'), ccrz__cart__c=cart.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0, ccrz__cartItemType__c='Major')
        };
        insert cartItems;
        
        cart = [select id, ccrz__EncryptedId__c from ccrz__E_Cart__c where id = :cart.Id];
        
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        ctx.portalUserId = UserInfo.getUserId();
        ctx.currentCartId = cart.ccrz__EncryptedId__c;

        Test.startTest();
        ccrz.cc_RemoteActionResult res = cc_cceag_ctrl_CartAlternatives.sendMail(ctx, 'test@testagcartem.com');
        system.assert(res.success);
        Test.stopTest();
    }
    
}
