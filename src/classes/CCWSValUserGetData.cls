/**********************************************************************
Name:  CCWSValUserGetData 
======================================================
Purpose:                                                            
Gets a Account ID, validates, that the user is a enabled Sales App user
Calls details of the giiven Account ID and delivers back Account Details  
======================================================
History                                                            
-------                                                            
Date  		AUTHOR			DETAIL 
01/30/2014 	Bernd Werner	<b.werner@yoursl.de>           
***********************************************************************/

 
global with sharing class CCWSValUserGetData {
	
	WebService static OperationResult getDebitor(String sAccountId)
	{
		// Initialize result Object
		OperationResult objOpResult = new OperationResult();
		
		// Initialize a list to hold all permissin set ids that are allowed to use the Sales app
		List<String> lPermIds = new List<String>();
		
		// Initialize a integer to count the number of authorized permission sets allocated to the user
		Integer iPerAss = 0;
		
		// User details
		String sAlias = ''; 
		
		// To hold the Account
		Account oAccount;
		
		// Get user ID from the user of the actual session
		String sUserId = UserInfo.getUserId();
				
		// First check, if we have a given sAccountId
		if(sAccountId==null || sAccountId==''){
			objOpResult.addError('No Acccount number given');
			return objOpResult;
		}

		// Get all Permissionsets that allow the user to use the Sales App and put IDs in a list
        SalesAppSettings__c per = SalesAppSettings__c.getall().get('PermissionSetId');
		if(per != null)
        	lPermIds.add(per.value__c);
		
		// Check if we find a authorized Permission set allocated to the user
		try{
			iPerAss =[Select count() From PermissionSetAssignment WHERE Assignee.Id = : sUserId AND PermissionSetId IN : lPermIds];
		}catch (QueryException e){
			objOpResult.addError(e.getMessage());
			return objOpResult;
		}
		system.debug('Number found Sets: '+iPerAss);
		// If no permission set was found, return error
		if(iPerAss==0){
			objOpResult.addError('This user is not authorized.');
			return objOpResult;
		}
		
		// Call user details to get the DE number as requested
		try{
			sAlias = [SELECT Alias FROM User WHERE Id= :sUserId Limit 1].Alias;
			system.debug('Alias: '+sAlias);
		 }catch (QueryException e){
			objOpResult.addError(e.getMessage());
			return objOpResult;
		}
		// If we are still alive, add alias to result 
		objOpResult.addUser(sAlias);
		
		// Additional ass users Mailadress
		objOpResult.addUserMail(UserInfo.getUserEmail());
		
		// Finally collect information about the give Account
		try{
			oAccount = [SELECT Phone, Name2__c, Name, Mobile__c, BillingStreet, BillingPostalCode, BillingCity, AccountNumber FROM Account WHERE Id = :sAccountId Limit 1]; 
			system.debug('Account: '+oAccount);
		}
		catch (QueryException e){
			system.debug('###Error Object: '+e);
			if(e.getMessage()== 'List has no rows for assignment to SObject'){
				objOpResult.addError('No Customer found with given ID');
				return objOpResult;
			}else {
				objOpResult.addError(e.getMessage());
				return objOpResult;
			}
		}
		// If we are still alive, add Account to result
		objOpResult.addDebitor(oAccount);
		return objOpResult;
	}
	
	
	
	/************************************************************************
	**
	**	Class to prepare and return a reult to the caller
	**
	************************************************************************/
	global class OperationResult{
		
		// Class variables
		webservice Account oCustomerDetails{get;set;}
		webservice String sUserID{get;set;}
		webservice String sUserMail{get;set;}
		webservice String sError{get;set;}
		webservice Boolean bSuccess{get;set;}
		
		// Constructor
		private OperationResult(){
			// MessageUUID='5257AA3FC97C0FE0E10080000A621094'; 
			this.bSuccess = true;
		}
		
		public boolean addUser(String sUser_id){
			this.sUserID = sUser_id;
			return true;
		}
		
		public boolean addUserMail(String mail){
			this.sUserMail = mail;
			return true; 
		}
		
		public boolean addDebitor(Account sSObjekt){
			this.oCustomerDetails = sSObjekt;
			return true;
		}
		
		public boolean addError(String sError){
			this.sError = sError;
			this.bSuccess = false;
			return true;
		}
	}// end global class OperationResult
	

}
