/**
* @author		Alexander Placidi
*
* @description	The contacts with firstname "Verkaufsberater" will get assigned to the account via the account lookup "PrimarySalesContact__c".
*				Object: Contact
*				TestClass: ContactMethodsTest, ContactsPrimarySales is obsolete
*
* @date			20.01.2014
*
* Timeline:
* Name               Date                      Description
* Alexander Placidi  20.01.2014                initial development
* Austen Buennemann  05.09.2014                Contains check in method setFlag() to prevent the trigger self reference.     
* Oliver Preuschl    17.09.2014				   Complete rewrite. Instead of mrking the primary sales contact by check box "IsPrimarySalesContact__c" the account lookup "PrimarySalesContact__c" is used.
*
*/


public with sharing class ContactMethods {
	 
	//Update the related accounts with the primary sales contacts 
	public static void updateAccountPrimarySalesContacts( List< Contact > listContacts ){
		System.debug( 'Start updateAccountPrimarySalesContacts, listContacts: ' + listContacts );

		Map< Id, Contact > mapAccountIds2PrimaryContacts = getAccountIds2PrimaryContacts( listContacts );
		updateAccountPrimarySalesContacts( mapAccountIds2PrimaryContacts );

		System.debug( 'Stop updateAccountPrimarySalesContacts' );
	}

	//Get the account ids with the related primary sales contacts
	private static Map< Id, Contact > getAccountIds2PrimaryContacts( List< Contact > listContacts ){
		System.debug( 'Start getAccountIds2PrimaryContacts, listContacts: ' + listContacts );

		Map< Id, Contact > mapAccountIds2PrimaryContacts = new Map< Id, Contact >();
		for( Contact oContact: listContacts ){
			if( oContact.FirstName == 'Verkaufsberater' ){
				mapAccountIds2PrimaryContacts.put( oContact.AccountId, oContact );
			}
		}

		System.debug( 'Stop getAccountIds2PrimaryContacts: ' + mapAccountIds2PrimaryContacts );

		return mapAccountIds2PrimaryContacts;
	}

	//Update the related accounts with the primary sales contacts 
	private static void updateAccountPrimarySalesContacts( Map< Id, Contact > mapAccountIds2PrimaryContacts ){
		System.debug( 'Start updateAccountPrimarySalesContacts, mapAccountIds2PrimaryContacts: ' + mapAccountIds2PrimaryContacts );

		List< Account > listAccounts = new List< Account >();
		for ( Account oAccount: [ SELECT Id, PrimarySalesContact__c FROM Account WHERE ( Id IN :mapAccountIds2PrimaryContacts.KeySet() ) ] ){
			Contact oPrimaryContact = mapAccountIds2PrimaryContacts.get( oAccount.Id );
			oAccount.PrimarySalesContact__c = oPrimaryContact.Id;
			listAccounts.add( oAccount );
		}
		update( listAccounts );

		System.debug( 'Stop updateAccountPrimarySalesContacts' );
	}

}
