/** 
* @author Jan Mensfeld
* @date 30.10.2014
* @description Interface for the trigger dispatching architecture.
*/
public interface ITriggerDispatcher{

	/** 
	* @author Jan Mensfeld
	* @date 30.10.2014
	* @description Called by the trigger framework to carry out the actions before the records are inserted.
	* @param TriggerParameters Contains the trigger parameters which includes the records that is getting inserted.
	*/	
	void beforeInsert(TriggerParameters tp);
	
	/** 
	* @author Jan Mensfeld
	* @date 30.10.2014
	* @description Called by the trigger framework to carry out the actions before the records are updated. 
	* @param TriggerParameters Contains the trigger parameters which includes the records that is getting updated.
	*/
	void beforeUpdate(TriggerParameters tp);
	
	/** 
	* @author Jan Mensfeld
	* @date 30.10.2014
	* @description Called by the trigger framework to carry out the actions before the records are deleted 
	* @param TriggerParameters Contains the trigger parameters which includes the records that is getting deleted.
	*/
	void beforeDelete(TriggerParameters tp);
	
	/** 
	* @author Jan Mensfeld
	* @date 30.10.2014
	* @description Called by the trigger framework to carry out the actions after the records are inserted.
	* @param TriggerParameters Contains the trigger parameters which includes the records that is getting inserted.
	*/	
	void afterInsert(TriggerParameters tp);
	
	/** 
	* @author Jan Mensfeld
	* @date 30.10.2014
	* @description Called by the trigger framework to carry out the actions after the records are updated. 
	* @param TriggerParameters Contains the trigger parameters which includes the records that is getting updated.
	*/
	void afterUpdate(TriggerParameters tp);

	/** 
	* @author Jan Mensfeld
	* @date 30.10.2014
	* @description Called by the trigger framework to carry out the actions after the records are deleted 
	* @param TriggerParameters Contains the trigger parameters which includes the records that is getting deleted.
	*/	
	void afterDelete(TriggerParameters tp);

		/** 
	* @author Jan Mensfeld
	* @date 30.10.2014
	* @description Called by the trigger framework to carry out the actions before the records are undeleted 
	* @param TriggerParameters Contains the trigger parameters which includes the records that got undeleted.
	*/
	void afterUnDelete(TriggerParameters tp);

	void executed();
}
