/*
 * @(#)SCHistoryPopupControllerTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCHistoryPopupControllerTest
{
    static testMethod void testHistoryPopup()
    {
        SCHelperTestClass.createAccountObject('Customer', true);

        Test.startTest();
        Id oId = SCHelperTestClass.account.Id;          
        ApexPages.currentPage().getParameters().put('oid', oId);
        ApexPages.currentPage().getParameters().put('mode', 'Account');
        SCHistoryPopupController controller = new SCHistoryPopupController();

        Test.stopTest();
    } // testHistoryPopup
} // SCHistoryPopupControllerTest
