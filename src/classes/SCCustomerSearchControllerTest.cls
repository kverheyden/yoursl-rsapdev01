//
    //  arlBookingNew Controller - Test Class
    //
    //  Version 1.0
    //  Created by jhu (arlanis) ---  September 2010
    //
    
@isTest
private class SCCustomerSearchControllerTest {

    static testMethod void myUnitTest() {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        
        User u = new User();
        u.FirstName ='First1';
        u.LastName = 'Tester';
        u.Alias = 'mtst';
        u.Email = 'mtester1@company.com';
        u.Username = u.Email;
        u.CommunityNickname = u.Alias;
        u.ProfileId = [select Id from Profile
                       where Name = 'System Administrator' OR Name = 'Systemadministrator'][0].Id;
        u.TimeZoneSidKey = 'America/Chicago';
        u.LocaleSidKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        u.LanguageLocaleKey = 'en_US';
        
        insert u;
                        
        User runningUser = u;

        //run test class as System Administrator        
        System.runAs(runningUser){  
            Account a = new Account();
            a.Name = 'Test JHU';
//          insert a;

            PageReference pageRef = Page.SCCustomerSearch;
            Test.setCurrentPage(pageRef);
            
            ApexPages.currentPage().getParameters().put('isP', 'false');        
            
            SCCustomerSearchController sc = new SCCustomerSearchController();
            
            List<SelectOption> options = sc.getAccTypes();
            sc.toggleSort();
            
            sc.runSearch();
            sc.accountName = 'Beard';
            sc.brand = 'Your Company Name';
            sc.optAccount = 'Personal';
            sc.street = 'a';
            sc.houseNo = '1';
            sc.postalCode = '44323 ';
            sc.city = 'Da';
            sc.runSearchCore();
            PageReference p1 = sc.createPA();
            PageReference p2 = sc.savePA();
            PageReference p3 = sc.cancelPA();
        }       
    }
    
    
    
    static testMethod void testSearch1()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        
        User u = new User();
        u.FirstName ='First1';
        u.LastName = 'Tester';
        u.Alias = 'mtst';
        u.Email = 'mtester1@company.com';
        u.Username = u.Email;
        u.CommunityNickname = u.Alias;
        u.ProfileId = [select Id from Profile
                       where Name = 'System Administrator' OR Name = 'Systemadministrator'][0].Id;
        u.TimeZoneSidKey = 'America/Chicago';
        u.LocaleSidKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        u.LanguageLocaleKey = 'en_US';
        
        u.ERPCompanyCode__c = 'CompanyCode';
        u.ERPSalesArea__c = '1234';
        u.ERPDivision__c = 'Division';
        u.ERPDistributionChannel__c = 'DistChannel';
        
        insert u;

        //run test class as System Administrator        
        System.runAs(u)
        {
            
            Account acc = new Account(Name = 'Tim Karl Müller',
                                      AccountNumber = '1234567890',
                                      FirstName__c = 'Tim',
                                      LastName__c = 'Müller',
                                      Name2__c = 'Karl',
                                      BillingStreet = 'Musterstraße',
                                      BillingHouseNo__c = '17',
                                      BillingPostalCode = '33098',
                                      BillingCity = 'Paderborn',
                                      BillingCountry__c = 'DE',
                                      Mobile__c = '0171/1234567',
                                      Phone = '05251/123456',
                                      Fax = '05251/123457',
                                      Email__c = 'tim@mueller.de',
                                      isPersonAccount__c = true,
                                      TaxCode1__c = '???',
                                      SearchCode__c = '!!!',
                                      SubTargetGroup__c = 'DE-01');
            Database.insert(acc);
            
            SCAccountInfo__c accInfo = new SCAccountInfo__c(Account__c = acc.Id,
                                                            DistributionChannel__c = 'DistChannel',
                                                            CompanyCode__c = 'CompanyCode',
                                                            SalesArea__c = 'SalesArea',
                                                            Division__c = 'Division',
                                                            GeoX__c = 123.123456,
                                                            GeoY__c = 12.123456);
            Database.insert(accInfo);
            
            SCInstalledBase__c ib = new SCInstalledBase__c(IdExt__c = 'test',
                                                           ManufacturerSerialNo__c = 'ManufacturerSerialNo',
                                                           SerialNo__c = 'SerialNo',
                                                           ShipTo__c = acc.Id,
                                                           SoldTo__c = acc.Id);
            Database.insert(ib);
            
            
            SCCustomerSearchController sc = new SCCustomerSearchController();
            sc.accountName = [SELECT Name FROM Account WHERE Id = :acc.Id].Name;
            sc.accountNumber = acc.accountNumber;
            sc.firstName = acc.firstname__c;
            sc.lastName = acc.lastname__c;
            sc.name2 = acc.name2__c;
            sc.street = acc.billingStreet;
            sc.houseNo = acc.billingHouseNo__c;
            sc.postalCode = acc.billingPostalCode;
            sc.city = acc.billingCity;
            //sc.country = acc.billingCountry__c;
            sc.mobile = acc.mobile__c;
            sc.phone = acc.phone;
            sc.fax = acc.fax;
            sc.email = acc.email__c;
            sc.taxcode = acc.taxcode1__c;
            sc.searchCode = acc.searchcode__c;
            sc.deletedcustomer = false;
            sc.optAccount = acc.isPersonAccount__c ? 'Personal' : 'Business';
            sc.targetGroup = acc.SubTargetGroup__c;
            
            sc.distributionChannel = accInfo.DistributionChannel__c;
            sc.equipmentNo = ib.IdExt__c;
            sc.serialNumber = ib.SerialNo__c;
            sc.manufacturerSerialNumber = ib.ManufacturerSerialNo__c;
            
            sc.toggleSort();
            sc.runSearch();
            
            System.assertEquals(1,sc.accounts.size());
            System.assertEquals(1,sc.accEntries.size());
        }
    }
    
    
    
    static testMethod void testSearch2()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        
        User u = new User();
        u.FirstName ='First1';
        u.LastName = 'Tester';
        u.Alias = 'mtst';
        u.Email = 'mtester1@company.com';
        u.Username = u.Email;
        u.CommunityNickname = u.Alias;
        u.ProfileId = [select Id from Profile
                       where Name = 'System Administrator' OR Name = 'Systemadministrator'][0].Id;
        u.TimeZoneSidKey = 'America/Chicago';
        u.LocaleSidKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        u.LanguageLocaleKey = 'en_US';
        
        u.ERPCompanyCode__c = 'CompanyCode';
        u.ERPSalesArea__c = '1234';
        u.ERPDivision__c = 'Division';
        u.ERPDistributionChannel__c = 'DistChannel';
        
        insert u;

        //run test class as System Administrator        
        System.runAs(u)
        {
            
            Account acc = new Account(Name = 'Tim Karl Müller',
                                      AccountNumber = '1234567890',
                                      isPersonAccount__c = false);
            Database.insert(acc);
            
            SCAccountInfo__c accInfo = new SCAccountInfo__c(Account__c = acc.Id,
                                                            DistributionChannel__c = 'DistChannel',
                                                            CompanyCode__c = 'CompanyCode',
                                                            SalesArea__c = 'SalesArea',
                                                            Division__c = 'Division',
                                                            GeoX__c = 123.123456,
                                                            GeoY__c = 12.123456);
            Database.insert(accInfo);
            
            
            SCCustomerSearchController sc = new SCCustomerSearchController();
            sc.accountNumber = acc.accountNumber;
            sc.optAccount = 'Business';
            
            sc.runSearch();
            
            System.assertNotEquals(0,sc.accounts.size());
            System.assertNotEquals(0,sc.accEntries.size());
        }
    }
    
    
    
    static testMethod void testCodeCoverage()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        
        Database.insert(new Brand__c(Name = 'test', Id2__c = 'test', Competitor__c = false));
        
        PageReference pageRef = Page.SCCustomerSearch;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('aids ', UserInfo.getUserId());
        
        SCCustomerSearchController sc = new SCCustomerSearchController();
        sc.getAccTypes();
        sc.getsel_brand_gb();
        sc.getsel_brand_de();
        sc.getsel_brand_de();
        sc.getsel_brand_it();
        sc.getListDistributionChannel();
        sc.gettrggrp();
        sc.getAllowedToCreateAccount();
        sc.submitToSales();
        sc.submitToService();
        sc.createPANL();
        sc.onSaveContractAccountOwner();
        sc.goBackToContract();
        sc.searchAccounts();
        
        sc.cid = UserInfo.getUserId();
        sc.newContractOwnerId = UserInfo.getUserId();
        sc.goBackToContract();
    }
}
