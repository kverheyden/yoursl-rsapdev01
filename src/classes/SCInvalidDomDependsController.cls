/*
 * @(#)SCInvalidDomDependsController.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCInvalidDomDependsController
{
    public List<SCDomainDepend__c> invalidDeps { get; private set; }


    /**
     * Default constructor
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCInvalidDomDependsController()
    {
        System.debug('#### SCInvalidDomDependsController');
        invalidDeps = SCfwDomain.checkForInvalidDomDepends();

    } // SCInvalidDomDependsController
} // class SCInvalidDomDependsController
