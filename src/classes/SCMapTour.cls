/*
 * @(#)SCMapTour.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * A complete liste of items of an engineer's tour (e.g. an order location)
 *
 * @author Norbert Armbruster <narmbruster@gms-online.de>
 */
public class SCMapTour
{
    // Name of the resource (e.g. "Mustermann Max")
    public String resourceName { get; set; }
    // Employee name
    public String resourceNameFull { get; set; }
    // the engineer's resource id
    public ID resourceId { get; set; }
    
    // Communication info (for interative communication)
    public String mobile { get; set; }
    public String phone { get; set; }
    public String email { get; set; }
    public String color {get; set; }

    // items of the tour
    public List<SCMapTourItem> items;
    
    public List<SCMapTourItem> getItems()
    {
        return items;
    }
    

    /**
     * Default Constructor 
     */
    public SCMapTour()
    {
        items = new List<SCMapTourItem>();    
    }
    
    /**
     * Constructor 
     * @param id     the resource id
     * @param name       id
     * @param nameFull   last and firstname of the resource 
     * @param mobile mobile phone number
     * @param phone  office phone number
     * @param email  email address
     * @author Norbert Armbruster <narmbruster@gms-online.de>
     */
    public SCMapTour(ID id, String name, String nameFull, String mobile, String phone, String email)
    {
        this.resourceId = id;
        this.resourceName = name;
        this.resourceNameFull = nameFull;
        this.mobile = mobile;
        this.phone = phone;
        this.email = email;    
    
        items = new List<SCMapTourItem>();    
    }
    
    /**
     * Reads the tours of the resources for a specific day
     * @param mode           LOC1 read the standard engineer locations only (resource assignment)
     *                       TOUR read the engineer tours
     * @param resourceIds    list of resources to be read
     * @param day            the day to be read
     * @author Norbert Armbruster <narmbruster@gms-online.de>
     */
    public static List<SCMapTour> read(String mode, List<Id> resourceIds, Date day)
    {
        System.Debug('###na SCMapTour.read(mode=' + mode + ' ids=' + resourceIds + ' day=' + day + ')' );
    
        Map<Id, SCMapTour> tours = new Map<Id, SCMapTour>();
        
        // prepare the date parameters
        Datetime t1 =  Datetime.newInstance(day.year(), day.month(), day.day(), 0,0,0); 
        Datetime t2 =  Datetime.newInstance(day.year(), day.month(), day.day(), 23,59,0); 

        // mode == 'LOC1'
        // 1. Read the resource start locations (home address)
        List<SCResourceAssignment__c> resources =
                    [SELECT Resource__r.ID, Resource__r.util_url__c, Resource__r.name, Resource__r.FirstName_txt__c, Resource__r.LastName_txt__c, Resource__r.alias_txt__c, Resource__r.Mobile_txt__c, Resource__r.Phone_txt__c, Resource__r.EMail_txt__c,  
                    Country__c, CountryState__c, County__c, PostalCode__c,  City__c, District__c, Street__c, HouseNo__c, Extension__c, 
                    GeoY__c, GeoX__c
                    FROM SCResourceAssignment__c
                    WHERE Resource__c in :resourceIds and 
                    (validfrom__c <= :day and validto__c >= :day) 
                    ];
                    
        Integer idx = 0;            
        for(SCResourceAssignment__c r : resources)
        {
            // add the new tour to the ma
            SCMapTour tour = new SCMapTour(r.Resource__r.ID, r.Resource__r.name, r.Resource__r.firstname_txt__c + ' ' + r.Resource__r.lastname_txt__c, r.Resource__r.Mobile_txt__c, r.Resource__r.Phone_txt__c, r.Resource__r.EMail_txt__c);
            // determine the colour of the tour
            tour.color = getColor(mode, idx);
            tours.put(tour.resourceId, tour);
            // add the location of the engineer as the first item of the tour
            SCMapTourItem item = new SCMapTourItem();
            item.itemtype     = 'ENG';                       // engineer location
            item.GeoX         = r.GeoX__c;                   // location longitude
            item.GeoY         = r.GeoY__c;                   // location latitude
            item.dtStart      = day;                         // planned start and end (internal appointment)
            item.dtEnd        = day;
            //item.status     = '';                          // assignment status   
            //item.orderno    = '';                          // order number 
            //item.orderid    = '';                          // order id
            // information - the location address
            item.infoAddress  = '<DIV>'
                                + r.Resource__r.util_url__c 
                                + '<BR/>' 
                                + r.Resource__r.name + '<BR/>' + SCutilFormatAddress.formatAddress(r)
                                + '<BR/>' 
                                + '<BR/><a href="callto:'+ r.Resource__r.Mobile_txt__c +'">- Call ' + r.Resource__r.Mobile_txt__c + '</a>'
                                + '<BR/><a href="smsto:'+ r.Resource__r.Mobile_txt__c +'">- Send sms to: ' + r.Resource__r.Mobile_txt__c + '</a>'
                                + '<BR/><a href="mailto:'+ r.Resource__r.EMail_txt__c +'">- Send email to: ' + r.Resource__r.EMail_txt__c + '</a>'
                                + '<BR/><a href="/apex/scautoplanningpage?id='+ r.Resource__r.ID +'">- Assign open orders</a>' 
                                + '<BR/><a href="/apex/scmanualplanningpage?id='+ r.Resource__r.ID +'">- Open in planning board</a>' 
                                + '</DIV>'
                                ;
            // information - order details    
            // item.infoOrder = '';
            // information - product details    
            item.infoProduct  = '';
            
            //invalid coordinates 
            if(r.GeoX__c == 0 && r.GeoY__c == 0)            
            {
                continue;
            }

            // add the item 
            tour.items.add(item);
            
            idx++;
        }
        System.Debug('###na SCMapTour.resources' + tours);

        if(mode == 'TOUR')
        {
            // 2. read all appointments for the resources and the currend day (requres 1. mode=LOC1)
            List<SCAppointment__c> appointments = new List<SCAppointment__c>();
            appointments = [SELECT type__c, Description__c,
                            Resource__c, Order__c, OrderItem__c, Assignment__c, AssignmentStatus__c,
                            Start__c, End__c, GeoY__c, GeoX__c, Distance__c, DrivingTime__c, 
                            CustomerPrefStart__c, CustomerPrefEnd__c, CustomerTimewindow__c, CustomerTimewindowStart__c, CustomerTimewindowEnd__c, 
                            Order__r.id, Order__r.name, TOLABEL(Order__r.type__c), TOLABEL(Order__r.status__c)
                           FROM SCAppointment__c 
                           WHERE (Start__c >= :t1 and Start__c <= :t2)
                           and Resource__c  in :resourceIds and type__c in ('7401')
                           order by Resource__c, Start__c];
            // 7401: order  
      
            SCMapTour currentTour = new SCMapTour();
            for(SCAppointment__c app : appointments)
            {
                if(currentTour.resourceId != app.Resource__c)
                {
                    currentTour = tours.get(app.Resource__c);
                    if(currentTour == null)
                    {
                        // invalid state - check resource assigments
                        continue;
                    }
                }
                //invalid coordinates 
                if(app.GeoX__c == 0 && app.GeoY__c == 0)            
                {
                    continue;
                }

                // add the location of the engineer as the first item of the tour
                SCMapTourItem item = new SCMapTourItem();
                item.itemtype     = 'ORD';                   // order location
                item.apptype      = app.type__c;             // appointment type
                item.GeoX         = app.GeoX__c;             // location longitude
                item.GeoY         = app.GeoY__c;             // location latitude
                item.dtStart      = app.Start__c;            // planned start and end (internal appointment)
                item.dtEnd        = app.End__c;
                item.status       = app.AssignmentStatus__c; // assignment status   
                item.orderno      = app.Order__r.name;       // order number 
                item.orderid      = app.Order__c;            // order id
                // information - the location address
                item.infoAddress  = '<DIV>'
                                + '<BR/><a href="/"'+ app.Order__r.id +'>' + app.Order__r.name + '</a>'
                                + '<BR/>' 
                                + app.Order__r.type__c
                                + '<BR/>' 
                                + app.Order__r.status__c
                                + '<BR/>' 
                                + app.Description__c
                                + '<BR/>' 
                                + app.start__c
                                + '<BR/>' 
                                + app.end__c
/*                                + '<BR/>' 
                                + app.Distance__c
                                + '<BR/>' 
                                + app.DrivingTime__c
*/                                + '<BR/>' 
                                + '<BR/><a href="">- Call 02121/123456</a>'
                                + '<BR/><a href="">- Send sms to: 0171/123456</a>'
                                + '<BR/><a href="">- Send email to: mueller@philipmorris.de</a>'
                                + '<BR/><a href="">- Assign engineer manually</a>'
                                + '<BR/><a href="">- Assign engineer automatically</a>'
                                + '<BR/><a href="">- Assign additional engineer</a>'
                                + '<BR/><a href="">- Bundle suitable orders</a>'
                                + '<BR/><a href="">- Open in planning board</a>'
                                + '<BR/><a href="">- Cancel assignment</a>'
//                                + '<BR/><a href="">- </a>'
//                                + '<BR/><a href="callto:"'+ r.Resource__r.Mobile_txt__c +'>- Call ' + r.Resource__r.Mobile_txt__c + '</a>'
//                                + '<BR/><a href="smsto:"'+ r.Resource__r.Mobile_txt__c +'>- Send sms to: ' + r.Resource__r.Mobile_txt__c + '</a>'
//                                + '<BR/><a href="mailto:"'+ r.Resource__r.EMail_txt__c +'>- Send email to: ' + r.Resource__r.EMail_txt__c + '</a>'
//                                + '<BR/><a href=/apex/scautoplanningpage?id="'+ r.Resource__r.ID +'">- Assign open orders</a>' 
//                                + '<BR/><a href=/apex/scmanualplanningpage?id="'+ r.Resource__r.ID +'">- Open in planning board</a>' 
                                + '</DIV>'
                                ;
/*                
                            CustomerPrefStart__c, CustomerPrefEnd__c, CustomerTimewindow__c, CustomerTimewindowStart__c, CustomerTimewindowEnd__c, 
                            Order__r.name, Order__r.type__c, Order__r.status__c
                           FROM SCAppointment__c 
            item.infoAddress  = '<DIV>'
                                + r.Resource__r.util_url__c 
                                + '<BR/>' 
                                + r.Resource__r.name + '<BR/>' + SCutilFormatAddress.formatAddress(r)
                                + '<BR/>' 
                                + '<BR/><a href="callto:"'+ r.Resource__r.Mobile_txt__c +'>- Call ' + r.Resource__r.Mobile_txt__c + '</a>'
                                + '<BR/><a href="smsto:"'+ r.Resource__r.Mobile_txt__c +'>- Send sms to: ' + r.Resource__r.Mobile_txt__c + '</a>'
                                + '<BR/><a href="mailto:"'+ r.Resource__r.EMail_txt__c +'>- Send email to: ' + r.Resource__r.EMail_txt__c + '</a>'
                                + '<BR/><a href=/apex/scautoplanningpage?id="'+ r.Resource__r.ID +'">- Assign open orders</a>' 
                                + '<BR/><a href=/apex/scmanualplanningpage?id="'+ r.Resource__r.ID +'">- Open in planning board</a>' 
                                + '</DIV>'
                                ;
*/                
                
                // information - order details    
                // item.infoOrder = '';
                // information - product details    
                item.infoProduct  = '';
                 
                currentTour.items.add(item);
                //, Order__r.type__c, Order__r.status__c
                
            } // for(SCAppointment__c..
            
            System.Debug('###na SCMapTour.tours' + tours);
            
        } // if(mode == 'TOUR'..
        return tours.values();
    }

    
    private final static String[] clr = new String[]{'40','80','90','0xC0'};
    
    /**
     * Calculate the color
     * @author Norbert Armbruster <narmbruster@gms-online.de>
     */
    public static String getColor(String mode, Integer i)
    {
        integer r = math.mod(i + 0, 4);
        integer g = math.mod(i + 1, 4);
        integer b = math.mod(i + 2, 4);
        
        return clr[r] + clr[g] + clr[b];
    }
   
     
    /**
     * Reads the schedulable resources for the current day of a country.
     * @param country     country code DE, GB, NL, ....
     * @param day         the day to query (for resource assignment)
     * @return            a list of recources
     * @author Norbert Armbruster <narmbruster@gms-online.de>
     */
    public static List<SCResource__c> readResources(String country, Date day)
    {
        List<SCResource__c> result = [SELECT id, name, EMail_txt__c, Employee__c, lastname_txt__c, firstname_txt__c, Phone_txt__c, Type__c FROM SCResource__c where enablescheduling__c = true and isDeleted = false and id in (select resource__c from SCResourceAssignment__c where country__c = :country and enablescheduling__c = true and (validfrom__c <= :day and validto__c >= :day)) order by lastname_txt__c, name]; 
        return result;
    }
   
    
}
