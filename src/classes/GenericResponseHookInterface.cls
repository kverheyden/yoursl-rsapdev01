/*
 * @(#)GenericResponseHookInterface.cls 
 * 
 * Copyright 2014 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * It is the interface for hook for the CCWSGenericResponse class to be used by YourSL team
 *   
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */

public interface GenericResponseHookInterface 
{
	 /**
	  *	processes the whole input structure that is sent by SAP to Clock Port.
	  *
	  */
     void process(CCWSGenericResponse.GenericServiceResponseMessageClass GenericServiceResponseMessage);
}
