/**
* @author		Austen Buennemann
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				a.buennemann@yoursl.de
*
* @description	Handler class for CCWCCustomerRegistration webservice
*
* @date			23.10.2014
*
* Timeline:
* Name               DateTime                  Description
* Austen Buennemann  23.10.2014 10:59          Class created
* Austen Buennemann  12.11.2014 09:00          Class modified.
*/

public with sharing class CCWCCustomerRegistrationHandler {

	/**
	* Prepare callout for CustomerRegistrations
	*
	* @param requests CokeIDRequest__c IDs
	* 
	*/

	public void prepareRegistration(List<CokeIDRequest__c> requests){
		
		boolean execute = false;
		Set<Id> accountIDs = new Set<Id>();
		List<CokeIDRequest__c> selectRequest = new List<CokeIDRequest__c>();
		for(CokeIDRequest__c req : requests){
			if(req.CokeIdUserId__c != null && req.Status__c == 'created'){
				execute = true;
				if(req.Account__c != null) accountIDs.add(req.Account__c);
				selectRequest.add(req);
			}
		}
		
		system.debug('\n\n  <<< CCWCCustomerRegistrationHandler >>>\n' + 'Relevant Account IDs: ' + accountIDs + '\n' );
		
		
		if(execute){
			Map<Id, Account> regAccount = new Map<Id, Account>([SELECT Id, AccountNumber FROM Account WHERE Id IN: accountIDs]);
			Map<Id, CCAccountRole__c> soldRoles = new Map<Id, CCAccountRole__c>();
			Set<Id> masterAGs	 = new Set<Id>();
			Map<Id, List<CCAccountRole__c>> shipRoles = new Map<Id, List<CCAccountRole__c>>();
			String query = 'SELECT Id, AccountRole__c, SlaveAccount__c, SlaveAccount__r.AccountNumber, MasterAccount__c, MasterAccount__r.AccountNumber FROM CCAccountRole__c WHERE (Status__c != \'Deleted\' AND AccountRole__c IN (\'WE\', \'AG\')) AND (SlaveAccount__c IN: accountIDs OR MasterAccount__c IN: accountIDs)';
			system.debug('\n\n  <<< CCWCCustomerRegistrationHandler >>>\n' + 'Account Roles: ' + Database.query(query) + '\n' );			
			for(CCAccountRole__c role :  Database.query(query)){ 
												
				//map AG roles
				if(role.AccountRole__c == 'AG'){					
					soldRoles.put(role.MasterAccount__c, role);
				}
				
				//map WE roles
				else if(role.AccountRole__c == 'WE'){
					
					//all WE roles, key is Slave
					List<CCAccountRole__c> entitiesForKey = shipRoles.get(role.SlaveAccount__c);
					if(entitiesForKey == null){
						entitiesForKey = new List<CCAccountRole__c>();
						shipRoles.put(role.SlaveAccount__c, entitiesForKey);
					}
					entitiesForKey.add(role);
					
					//all WE roles , key is Master
					List<CCAccountRole__c> entitiesForKey2 = shipRoles.get(role.MasterAccount__c);
					if(entitiesForKey2 == null){
						entitiesForKey2 = new List<CCAccountRole__c>();
						shipRoles.put(role.MasterAccount__c, entitiesForKey2);
					}
					entitiesForKey2.add(role);
					
					masterAGs.add(role.MasterAccount__c);
				}				
			}
			
			Map<Id, CCAccountRole__c> soldRoles2 = new Map<Id, CCAccountRole__c>();		
			query = 'SELECT Id, AccountRole__c, SlaveAccount__c, SlaveAccount__r.AccountNumber, MasterAccount__c, MasterAccount__r.AccountNumber FROM CCAccountRole__c WHERE Status__c != \'Deleted\' AND AccountRole__c = \'AG\' AND (MasterAccount__c IN: masterAGs)';
			system.debug('\n\n  <<< CCWCCustomerRegistrationHandler >>>\n' + 'Select AGs of related WEs to verification: ' + Database.query(query) + '\n' );			
			for(CCAccountRole__c role :  Database.query(query)){ 
				
				//map AGs of related WEs to verification
				if(role.SlaveAccount__c == role.MasterAccount__c && role.AccountRole__c == 'AG'){
					soldRoles2.put(role.MasterAccount__c, role);
				}
				
			}
					
			system.debug('\n\n  <<< CCWCCustomerRegistrationHandler >>>\n' + 'SoldTo roles: ' + soldRoles + '\n'  + 'verify SoldTo roles: ' + soldRoles2 + '\n' + 'ShipTo roles: ' + shipRoles + '\n');
														
			for(CokeIDRequest__c req : selectRequest){
				String regUserID = req.CokeIdUserId__c ;
			    String regCustomerPortalID = req.CokeIdUserId__c;
			    String regAccNumberRegUser = regAccount.get(req.Account__c).AccountNumber;
			    String regComment = 'SFDC ID: ' + req.Id + ', SFDC Name: ' + req.Name;
			    
			    String regAccountNumberSoldTo;
			    Set<String> regAccountNumberShipTo = new Set<String>();
			    
		     	//Case: Account is a SoldTo	     	
		     	if(soldRoles.containsKey(req.Account__c)){
		     		//verify Master AG roles
			     	if(soldRoles.get(req.Account__c).MasterAccount__c == req.Account__c){
			     		//write SoldTo
				     	regAccountNumberSoldTo = regAccount.get(req.Account__c).AccountNumber;
				     	//verify ShipTos
				     	if(shipRoles.containsKey(req.Account__c)){
				     		//write ShipTos
				     		for (CCAccountRole__c entry : shipRoles.get(req.Account__c)){
				     			if(entry != null && entry.SlaveAccount__c != null && !regAccountNumberShipTo.contains(entry.SlaveAccount__r.AccountNumber)){
				     				regAccountNumberShipTo.add(entry.SlaveAccount__r.AccountNumber);
				     			}
				     		}
				     	} 
			     	}
		     	}
		     	
		     	//Case: Account is a ShipTo
		     	//verify WE Slave
				else if(shipRoles.containsKey(req.Account__c)){
		     		for (CCAccountRole__c entry : shipRoles.get(req.Account__c)){
		     			if(entry != null && entry.MasterAccount__c != null){
		     				//verify WE Master as AG
		     				if(soldRoles2.containsKey(entry.MasterAccount__c)){
		     					//write SoldTo
		     					regAccountNumberSoldTo = soldRoles2.get(entry.MasterAccount__c).MasterAccount__r.AccountNumber;
		     				}
		     			}
		     		}	
					//write WE Slave as ShipTo
					if(!regAccountNumberShipTo.contains(regAccount.get(req.Account__c).AccountNumber)){
		     			regAccountNumberShipTo.add(regAccount.get(req.Account__c).AccountNumber);
					}
		     	}
			    
			    if(regAccountNumberSoldTo == null && regAccountNumberShipTo.isEmpty()) {	
			    	system.debug('\n\n  <<< CCWCCustomerRegistrationHandler >>>\n' + 'ShipTo and SoldTo Accounts not found.' + '\n');
			    }else{
				    //serialize CokeIDRequest__c object
				    String accString = JSON.serialize(req);
				    
				    //send webservice call
				    system.debug('\n\n  <<< CCWCCustomerRegistrationHandler >>>\n' + 'CALL CCWCCustomerRegistration.executecallout: ' 
				    	+ accString + '; ' + regUserID + '; ' + regCustomerPortalID + '; ' + regComment +
				    	'; ' + regAccountNumberSoldTo + '; ' + regAccNumberRegUser + '; ' + regAccountNumberShipTo + '\n' );
				    
					CCWCCustomerRegistration.executecallout(accString, regUserID, regCustomerPortalID, regComment, regAccountNumberSoldTo, 
															regAccNumberRegUser, new List<String>(regAccountNumberShipTo), false);
			    }	     	
			}
		}		
	}
		
	/**
	* Prepare single callout for CustomerRegistration retry operation
	*
	* @param cid 	CokeIDRequest__c Id
	* @param test	Testmode
	* 
	*/
	public void callout(String cid, boolean test){
		CCWCCustomerRegistrationHandler send = new CCWCCustomerRegistrationHandler();
		List<CokeIDRequest__c> requests = new List<CokeIDRequest__c>([Select Id, Status__c, Name, CokeIdUserId__c, Account__c From CokeIDRequest__c  Where Id =: cid]);
		send.prepareRegistration(requests);
	}
	
}
