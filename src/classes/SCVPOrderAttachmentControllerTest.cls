/*
* @(#)SCVPOrderAttachmentControllerTest.cls
* 
* Copyright 2014 by GMS Development GmbH, Karl-Schurz-Strasse 29, 
* DE-33100 Paderborn. All rights reserved.
*
* This software is the confidential and proprietary information
* of GMS Development GmbH. ("Confidential Information").  You
* shall not disclose such Confidential Information and shall use
* it only in accordance with the terms of the license agreement
* you entered into with GMS.
* 
* # Test class for SCVPOrderAttachmentController
*
* @author Marc Sälzler <msaelzler@gms-online.de>
*
* @history	
* 2014-11-05 GMSmae created
* 
* @review
*
*/

@isTest
public class SCVPOrderAttachmentControllerTest
{
	@isTest
	public static void SCVPOrderAttachmentControllerTest()
	{
		SCVPOrderAttachmentController t_SCVPOrderAttachmentController;
		
		try
		{
			t_SCVPOrderAttachmentController = new SCVPOrderAttachmentController();
		}
		catch(Exception e)
		{
			
		}
		
		System.assert(t_SCVPOrderAttachmentController.a_errorText != '',  'Expected an error text. String is empty.');
		
		SCOrder__c t_order = new SCOrder__c(Closed__c        = DateTime.now(),
                                          Followup1__c       = SCfwConstants.DOMVAL_FOLLOWUP1_NONE,
                                          Followup2__c       = SCfwConstants.DOMVAL_FOLLOWUP2_NONE,
                                          Status__c          = SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL,
                                          Type__c            = SCfwConstants.DOMVAL_ORDERTYPE_INHOUSE,
                                          ID2__c             = 'Order_ClosedToday');
        Database.insert(t_order);
		
		
		PageReference t_pageRef = Page.SCVPOrderAttachment;
		
		t_pageRef.getParameters().put('id', t_order.Id);
		
		Test.setCurrentPage(t_pageRef);
		
		t_SCVPOrderAttachmentController= new SCVPOrderAttachmentController();
		
		String t_errorText = t_SCVPOrderAttachmentController.a_errorText;
		String t_errorTextAtt = t_SCVPOrderAttachmentController.a_errorTextAtt;
		Boolean t_success = t_SCVPOrderAttachmentController.a_success;
		
		System.assertNotEquals(t_SCVPOrderAttachmentController.x_orderId, null, 'Expected an id. Received: ' + t_SCVPOrderAttachmentController.x_orderId);
		
		try
		{
			t_SCVPOrderAttachmentController.CreateAttachment();
		}
		catch(Exception e)
		{
			
		}
		
		Attachment t_attachment = t_SCVPOrderAttachmentController.a_attachment;
		
		// Name too long.
		t_attachment.Name = 'testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest';
		t_attachment.Body = Blob.valueOf('test');
		
		try
		{
			t_SCVPOrderAttachmentController.CreateAttachment();
		}
		catch(Exception e)
		{
			
		}
		
		System.assert(t_SCVPOrderAttachmentController.a_errorTextAtt != '',  'Expected an error text. String is empty.');
		
		t_attachment.Name = 'test';
		
		t_SCVPOrderAttachmentController.CreateAttachment();
		
		System.assertEquals(true, t_SCVPOrderAttachmentController.a_success);
	}
}
