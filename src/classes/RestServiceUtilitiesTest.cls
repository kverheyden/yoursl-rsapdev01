/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RestServiceUtilitiesTest {

    static testMethod void testInsertNewLog() {

        enableDebugging();

        RestRequest req = new RestRequest();
        DateTime reqStart = System.now();
        RestResponse res = new RestResponse();

        req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/SomeService';
        req.addParameter('param1', 'value1');
        req.requestBody = Blob.valueOf( 'RequestBody' );
        
        System.RestContext.request = new RestRequest();
        RestContext.request.requestURI = req.requestURI;
        RestContext.request.RequestBody = req.requestBody;
        System.RestContext.response = new RestResponse();
        RestContext.response.ResponseBody = req.requestBody;

        RestServiceUtilities.WSLog LO_WSLog = new RestServiceUtilities.WSLog();
        LO_WSLog.setStart( reqStart );
        LO_WSLog.setStatus( RestServiceUtilities.WSStatus.Success );
        LO_WSLog.setJSONRequest( 'Request Body' );
        LO_WSLog.insertLog();

        Test.startTest();

        RestServiceUtilities.insertNewLog(reqStart, null, true);
        RestServiceUtilities.insertNewLog(reqStart, true);

        Test.stopTest();            

        List< WSLogs__c > LL_WSLogs = [ SELECT Id FROM WSLogs__c ];
        System.assertEquals( 3, LL_WSLogs.size() );
    }

    //Enable Debugging
    public static void enableDebugging(){
        WSDebugSettings__c LO_WSDebugSettingEnabled = new WSDebugSettings__c(
                Name        = 'DebugIsActive',
                Value__c    = '1'
            );
        insert( LO_WSDebugSettingEnabled );
    }
}
