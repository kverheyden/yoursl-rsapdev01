@isTest
private class cc_cceag_dao_test_User 
{
    static testMethod void testGetFlowUserWithoutCSR() 
    {
		PageReference page = new PageReference('/');
        Test.setCurrentPage(page);
        
        Test.startTest();
        
        String userId = UserInfo.getUserId();
        System.assertEquals(userId, cc_cceag_dao_User.getFlowUser());
        
        Test.stopTest();
    }
    
    static testMethod void testGetFlowUserWithCSR() 
    {
        PageReference page = new PageReference('/');
        page.getParameters().put('portalUser','1234');
        Test.setCurrentPage(page);
        
        Test.startTest();
        
        System.assertEquals('1234', cc_cceag_dao_User.getFlowUser());
        
        Test.stopTest();
    }    
    
    static testMethod void testGetFlowUserWithContext()
    {
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        ctx.portalUserId = '5678';
        
        Test.startTest();
        System.assertEquals('5678', cc_cceag_dao_User.getFlowUserWithContext(ctx));
        Test.stopTest();
    }
    
    static testMethod void testGetFlowUserWithEmptyContext()
    {
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        
        Test.startTest();
        System.assertEquals(UserInfo.getUserId(), cc_cceag_dao_User.getFlowUserWithContext(ctx));
        Test.stopTest();
    }    
}
