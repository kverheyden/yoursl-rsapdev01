global with sharing class cc_cceag_ctrl_MyAccountAddressBook {

    private static final String[] recipients = new String[] {
        ecomSettings__c.getInstance('editContactDataEmailRecipient').value__c
    };
    
    private static final String SUBJECT = 'CustomerPortal: Anfrage Adressänderungen für {Name} (AccountNumber)';
    
    private static final String BODY_TEMPLATE = 
'Anfrage zur Änderung von Addressinformationen\n' +
'==============================================\n' +
'\n' +
'AccountNumber:		{AccountNumber}\n' +
'Name:				{Name}\n' +
'\n' +
'Geänderte Daten\n' +
'===============\n' +
'\n';
    

    @RemoteAction
    global static ccrz.cc_RemoteActionResult saveData(ccrz.cc_RemoteActionContext ctx, Map<String, String> data) {

        ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
        
        result.inputContext = ctx;
        result.success = false;

        try {
			ccrz.cc_CallContext.initRemoteContext(ctx);
            String userId = cc_cceag_dao_User.getFlowUser();

            sendEmail(data);

            Map<String, String> responseData = new Map<String, String>();
            responseData.put('emailSent', 'true');

            result.data = responseData;
            result.success = true;

        } catch (Exception e) { handleException(result, e); }

        return result;
    }


    private static void sendEmail (Map<String, String> newData) 
    {
    	String id = newData.get('id');
		Account account = [SELECT Id, AccountNumber, Name, BillingStreet, BillingCity, BillingPostalCode FROM Account WHERE Id=:id];
		Map<String,String> oldData = new Map<String,String>();
		oldData.put('companyName',account.Name);
		oldData.put('street',account.BillingStreet);
		oldData.put('zip',account.BillingPostalCode);
		oldData.put('city',account.BillingCity);
		oldData.put('accountNumber',account.AccountNumber);
		
        String replacedSubject = SUBJECT.replace('{AccountNumber}', account.AccountNumber).replace('{Name}', account.Name);
        cc_cceag_Mailer.sendEmail(recipients, SUBJECT, buildEmailBody(newData, oldData));
    }
    
    
    private static String buildEmailBody (Map<String, String> data, Map<String,String> oldData) {
        
        Map<String, String> fields = new Map<String, String>();
		fields.put('companyName','Name');
        fields.put('street', 'Straße');
        fields.put('zip', 'Postleitzahl');
        fields.put('city', 'Stadt');
        
        String body = BODY_TEMPLATE.replace('{AccountNumber}', oldData.get('accountNumber')).replace('{Name}', oldData.get('companyName'));
        
        for (String fieldName : fields.keySet()) {
            String oldValue = oldData.get(fieldName);
            String newValue = data.get(fieldName);
            String label = fields.get(fieldName);
            if (string.isNotBlank(newValue) && oldValue!=newValue) {
                body = body + label + ': ' + oldValue + ' --> ' + newValue + '\n';
            }
        }
        
        return body;
    }    
    
    without sharing class PriviledgedRetriever
    {
        public List<Account> getAccounts(List<Id> accountIds)
        {   
            return [
                SELECT
                    AccountNumber
                    , BillingAddress__c
                    , BillingStreet
                    , BillingCity
                    , BillingCountry
                    , BillingPostalCode
                    , BillingState
                    , DeliveringPlant__c
                    , Id
                    , Name
                FROM
                    Account
                WHERE
                    Id IN :accountIds
            ];
        }
    }

    static Map<String,Object> accountToMap(Account account, Map<String,String> extras)
    {
        Map<String,Object> data = new Map<String,Object>();
        
        if (extras!=null) {
            for (String key : extras.keySet()) {
                data.put(key, extras.get(key));
            }
        }
        
        cc_cceag_dao_Account.ensureFieldsForAccount(account, 'Name,BillingStreet,BillingCity,BillingPostalCode');
        data.put('companyName', account.Name);
        data.put('address',account.BillingAddress__c);
        data.put('accountNumber',account.AccountNumber);
        data.put('zip',account.BillingPostalCode);
        data.put('city',account.BillingCity);
        data.put('street',account.BillingStreet);
        data.put('id',account.Id);
        
        return data;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult fetchData (ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
        result.inputContext = ctx;
        result.success = false;

        try {
            ccrz.cc_CallContext.initRemoteContext(ctx);
            System.debug(ctx);
            
            String userId = cc_cceag_dao_User.getFlowUserWithContext(ctx);
            
            String accountId = cc_cceag_dao_Account.fetchAccountId(userId);
            result.data = accountId;
            List<Map<String, Object>> addresses = new List<Map<String, Object>>();
            
            Account account = [SELECT Id,Name,BillingAddress__c FROM Account WHERE Id=:accountId];
            //addresses.add(accountToMap(account,new Map<String,String> { 'type' => 'Self' }));
            if (cc_cceag_dao_Account.isAGAccount(accountId)) {
                Account invoiceRecipient = cc_cceag_dao_Account.fetchInvoiceRecipientAccount(account);
                if (invoiceRecipient!=null) {
                    addresses.add(accountToMap(invoiceRecipient,new Map<String,String> { 'type' => 'Rechnungsadresse' }));
                }
                
            	List<CCAccountRole__c> shipToRoles = cc_cceag_dao_Account.fetchRelatedAccounts(accountId, 'WE');
                for (CCAccountRole__c role : shipToRoles) {
                    addresses.add(accountToMap(role.SlaveAccount__r, new Map<String,String> {'type' => 'Lieferadresse'}));
                }
            }           
            
            result.data = addresses;            
            result.success = true;
            
        } catch (Exception e) { handleException(result, e); }
        
        return result;
    }
    
    
    public static void handleException (ccrz.cc_RemoteActionResult result, Exception e)
    {
        System.debug(System.LoggingLevel.ERROR, e.getMessage());
        System.debug(System.LoggingLevel.ERROR, e.getStackTraceString());
        
        ccrz.cc_bean_Message msg = new ccrz.cc_bean_Message();
        msg.type = ccrz.cc_bean_Message.MessageType.CUSTOM;
        msg.classToAppend = 'messagingSection-Error';
        msg.message = e.getMessage() + ': ' + e.getStackTraceString();
        msg.severity = ccrz.cc_bean_Message.MessageSeverity.ERROR;
        
        result.messages.add(msg);
    }
    
    
   	// change to public for unit test 
    public static List<Account> getAccountsWithAddresses(Id accountId)
    {
        List<Id> accountIds = new List<Id>();
        
        if (cc_cceag_dao_Account.isWEAccount(accountId)) {
                        
        } else {
            // get all WE for this account
            accountIds.add(accountId);
            List<CCAccountRole__c> roles = [SELECT SlaveAccount__c FROM CCAccountRole__c WHERE MasterAccount__c=:accountId AND AccountRole__c='WE'];
            for (CCAccountRole__c role : roles) {
                accountIds.add(role.SlaveAccount__c);
            }
        }
        
        PriviledgedRetriever retriever = new PriviledgedRetriever();
        
        return retriever.getAccounts(accountIds);
    }
}
