/**********************************************************************
Name:  CCWSValUserGetDataTest
======================================================
Purpose:                                                            
Test class for CCWSValUserGetData 
======================================================
History
-------                                                            
Date  		AUTHOR			DETAIL 
01/31/2014 	Bernd Werner	<b.werner@yoursl.de> 
12/09/2014  Duc Nguyen Tien	Will not be deployable for a fresh orgs, even with SeeAllData. Class uses permission sets (will not exists on fresh prod). The same with custom setting that is used in code.
***********************************************************************/
@isTest//(SeeAllData = true)
private class CCWSValUserGetDataTest {

    static testMethod void UnitTest() {
        PermissionSet permset = new PermissionSet(Label = 'FSFA Permission Access', Name='FSFA_Permission');
        insert permset;
		
		
		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
      	User u 					= new User();
      	u.Alias 				= 'standt';
      	u.Email					= 'standarduser@testorg.com'; 
      	u.EmailEncodingKey		= 'UTF-8';
      	u.LastName				= 'Testing';
      	u.LanguageLocaleKey		= 'en_US'; 
      	u.LocaleSidKey			= 'en_US';
      	u.ProfileId 			= p.Id; 
      	u.TimeZoneSidKey		= 'America/Los_Angeles';
      	u.UserName				= 'yoursl_standarduser@cceag.com';
      	
      	System.runAs(u){
            Account testAccount 			= new Account();
            testAccount.Name 				= 'Max Mustermann';
            testAccount.BillingCountry__c 	= 'DE';
            testAccount.ID2__c 				= '12345678';
            
            insert testAccount;
            
            PermissionSetAssignment permSetAssig = new PermissionSetAssignment(PermissionSetID = permset.ID, AssigneeID = u.Id );
        	insert permSetAssig;
            SalesAppSettings__c setting = new SalesAppSettings__c(Name = 'PermissionSetId', value__c = permSetAssig.PermissionSetID);
            insert setting;
            
            
            Integer iPerAss =[Select count() From PermissionSetAssignment WHERE Assignee.Id = :u.ID AND PermissionSetId =: setting.value__c];
            system.assertNotEquals(0, iPerAss);
            
            
            CCWSValUserGetData.getDebitor(testAccount.Id);
            CCWSValUserGetData.getDebitor('1234');
            CCWSValUserGetData.getDebitor('');

      	}
    }
}
