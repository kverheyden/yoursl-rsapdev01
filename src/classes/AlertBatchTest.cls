@isTest
private class AlertBatchTest {

	static testMethod void testScheduler() {
		Test.startTest();	
		AlertBatchScheduler sh1 = new AlertBatchScheduler();
		String sch = '0 0 23 * * ?'; 
		system.schedule('Test Territory Check', sch, sh1); 
		Test.stopTest(); 
	} 
	
    static testMethod void myUnitTest() {
    	List<PictureOfSuccess__c> listPofs	= new List<PictureOfSuccess__c>();
    	date validToDate				= date.today()+14; 
        PictureOfSuccess__c pofs 		= new PictureOfSuccess__c();
        pofs.Bezeichnung__c				= 'Test 1';
        pofs.ValidFrom__c				= date.today();
        pofs.ValidUntil__c				= validToDate;
        pofs.Subtradechannel__c			= 'xx';
        pofs.RedWeightingActivation__c	= 20;
        pofs.RedWeightingArea__c		= 20;
        pofs.RedWeightingAssortment__c	= 20;
        pofs.RedWeightingEquipment__c	= 40;
        listPofs.add(pofs);
        
        pofs					 		= new PictureOfSuccess__c();
        pofs.Bezeichnung__c				= 'Test 2';
        pofs.ValidFrom__c				= date.today();
        pofs.ValidUntil__c				= validToDate;
        pofs.Subtradechannel__c			= 'yy';
        pofs.RedWeightingActivation__c	= 20;
        pofs.RedWeightingArea__c		= 20;
        pofs.RedWeightingAssortment__c	= 20;
        pofs.RedWeightingEquipment__c	= 40;
        listPofs.add(pofs);
        
        insert listPofs;
        
        Test.StartTest();
		AlertBatch batch = new AlertBatch();
		Id batchId = Database.executeBatch(batch);
		// System.abortJob(batchId);
		Test.StopTest();
    }
}
