/**
 * @(#)SCInterfaceClearing
 * Copyright 2013 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

 /**
 * @author Eugen Tiessen <etiessen@gms-online.de>
 * @version $Revision$, $Date$
 */
 
@RestResource(urlMapping='/SCHtml2Pdf/*')
global with sharing class SCHtml2PdfController 
{
    
    private List<Attachment> tempAttachments= new List<Attachment>(); 
    
    public String html { get; private set; } 
    public Attachment att { get; private set; }

	
    public SCHtml2PdfController(ApexPages.StandardController controller)
    {
        this.att=(Attachment) controller.getRecord();
    }
    
    public SCHtml2PdfController(Id attachmentId)
    {
        this.att = [ SELECT Id, Body, ParentId FROM Attachment WHERE Id =:attachmentId LIMIT 1];
    }
    
    /**
     * Extract all embedded pictures, create attachments of the pictures and replace the base64 strings with 
     * the donwnload url of the inserted attachments.
     */
    public void generateHtml()
    {
        Attachment att2 = 
        [ 
        	SELECT 
        		Body,ParentId 
        	FROM 
        		Attachment 
        	WHERE 
        		Id =:att.Id LIMIT 1
        ];
        
        String origHtml = att2.Body.toString();
        
        //TODO: optimization. Exception handling
        //Quick and dirty HTML replacement.
        while(true)
        {
            Integer imgStart = origHtml.indexOf('<img src="data:');
            if(imgStart == -1)
            {
                //No images found
                break;
            }
            
            Integer imgEnd = origHtml.indexOf('">',imgStart);        
    
            String theImage = origHtml.substring(imgStart,imgEnd + 2 );
            
            //Parsing meta data of the embedded image
            //e.g. <img src="data:image/png;base64, iVBORw....hgsa==">
            //[0] content-type etc.
            //[1] base64 string of the image
            String[] params = theImage.split(',');
            
            String contentType= params[0].split(';')[0].split(':')[1];
            
            //TODO: the attachment must be deleted
            Attachment a = new Attachment
            (
            	Name='$tempPicture', 
            	parentId = att2.ParentId, 
            	contenttype=contentType, 
            	Body=EncodingUtil.base64Decode(params[1].substring(0,params[1].length() -2))
            );
            
            insert a;
            
            tempAttachments.add(a);
              
            html = origHtml.substring(0,imgStart);
            
            //The new image location
            String image = '/servlet/servlet.FileDownload?file=' + a.id;
            html += '<img src="' + image; 
            html += origHtml.substring(imgEnd,origHtml.length());
            
            origHtml = html;
        }
        
    }
    
    public String getHtml()
    {
    	return html;
    }
    
    /**
     * Deletes all temporary attachments assigned to the parentId
     * @param parentId 
     */
    public static void deleteTempAttachments(Id parentId)
    {
        List<Attachment> tempAtts = 
        [ 
        	SELECT 
        		Id 
        	FROM 
        		Attachment 
        	WHERE 
        		parentId =:parentId 
        		AND 
        		Name LIKE '$tempPicture%'
        ];
        
        delete tempAtts;
        
    }
    
    /**
     * Generate a PDF from the attachment (html code) and store the pdf to the same parent.
     * The original attachment will not be removed
     * 
     * @param attachmentId
     */
    public static void generatePdfFromHtml(Id attachmentId)
    {
    	generatePdfFromHtml(attachmentId, false);
    }
    
    /**
     * Generate a PDF from the attachment (html code) and store the pdf to the same parent 
     * @param attachmentId
     * @param removeOriginalHtml if true the original html attachment will be removed
     */ 
	public static void generatePdfFromHtml(Id attachmentId, Boolean removeOriginalHtml)
    {
    	SCHtml2PdfController pdf = new SCHtml2PdfController(attachmentId);
    	Pagereference pageRef =  Page.SCHtml2Pdf;
    	pageRef.getParameters().put('id',attachmentId);
    	
    	Attachment htmlAttachment = 
    	[ 
    		SELECT 
    			Id, ParentId, Name 
    		FROM 
    			Attachment 
    		WHERE 
    			Id =:attachmentId LIMIT 1
    	];
    	
    	Attachment att = new Attachment
    	(	
    	
    		Name = htmlAttachment.Name.substring(0,htmlAttachment.Name.lastIndexOf('.')) + '.pdf', 
    		Body = getPdfFromHtml(htmlAttachment), 
    		ParentId = htmlAttachment.parentId,
    		ContentType = 'application/pdf'
    	);
		insert att;

		//Delete helper attachments
		deleteTempAttachments(htmlAttachment.parentId);
		
		if(removeOriginalHtml)
		{
			delete htmlAttachment;
		}
		//return att.Id;
    }
    
    /**
     * Generate a PDF from the attachment (html code) 
     * @param attachmentId
     * @return the pdf as blob
     */ 
	public static Blob getPdfFromHtml(Id attachmentId)
    {
    	Attachment htmlAttachment = 
    	[ 
    		SELECT 
    			Id, ParentId 
    		FROM 
    			Attachment 
    		WHERE 
    			Id =:attachmentId LIMIT 1
    	];
    	
    	return getPdfFromHtml(htmlAttachment);
    }
    
    /**
     * Generate a PDF from the attachment (html code) 
     * @param htmlAttachment
     * @return the pdf as blob
     */ 
	public static Blob getPdfFromHtml(Attachment htmlAttachment)
    {
    	Pagereference pageRef =  Page.SCHtml2Pdf;
    	pageRef.getParameters().put('id', htmlAttachment.Id);
    	Blob pdf = pageRef.getContent(); 
    	
		//Delete helper attachments created by 
		deleteTempAttachments(htmlAttachment.ParentId);
		
    	return pdf;
    }
    
   	
   	/**
   	 * Helper function to replace a html with pdf because pdf creation is forbidden in triggers or future calls
   	 * The original html attachment will be deleted
   	 * Should be executed by rest callout 
   	 */
   	@HttpGet
   	global static void doGet()
   	{
   		RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String attachmentId = req.requestURI.substring(req.requestURI.lastIndexOf('/') + 1);
        generatePdfFromHtml(attachmentId,true);
   		//return generatePdfFromHtml(attachmentId);
   	}
   	
   	/**
   	 * Helper function to replace a html with pdf because pdf creation is forbidden in triggers or future calls
   	 * The original html attachment will be deleted
   	 * Should be executed by rest callout 
   	 */
   	@HttpPost
   	global static void doPost(String[] attachmentIds)
   	{
   		system.debug(attachmentIds);
   		RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        for(String attachmentId : attachmentIds)
        {
        	generatePdfFromHtml(attachmentId,true);
        }
   		//return 'result: ' + attachmentIds;
   	}
   	
}
