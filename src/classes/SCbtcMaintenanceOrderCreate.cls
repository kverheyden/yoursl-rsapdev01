/*
 * @(#)SCbtcMaintenanceOrderCreate.cls 
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Calcualtes next maintenance proposal in the object SCMaintenance__c
 *
 */

global with sharing class SCbtcMaintenanceOrderCreate extends SCbtcBase 
       implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts
{
    private static final Integer batchSize = 1;
    private ID batchprocessid = null;
    public ID getBatchProcessId()
    {
        return batchprocessid;
    }
    public Boolean synchron = false;
    String mode = 'productive';
    Integer max_cycles = 0;
    public String extraCondition = null;

    public Id maintenanceId = null;
    
    private Boolean autoCreated = true;
    private String query; // Query for batch job 

    private ID orderItemRecordTypeId = null;        
    // Object for application settings
    public static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();


    public static ID asyncCreateAll(Integer max_cycles, String mode)
    {
        System.debug('###mode: ' + mode);
        SCbtcMaintenanceOrderCreate btc = new SCbtcMaintenanceOrderCreate(max_cycles, mode);

        btc.batchprocessid  = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    }  

    public static ID asyncCreateAll(Integer max_cycles, String mode, String extraCondition)
    {
        System.debug('###mode: ' + mode);
        SCbtcMaintenanceOrderCreate btc = new SCbtcMaintenanceOrderCreate(max_cycles, mode, extraCondition);

        btc.batchprocessid  = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    }  

    public static ID asyncCreate(ID maintenanceID, Integer max_cycles, String mode)
    {
        System.debug('###mode: ' + mode);
        SCbtcMaintenanceOrderCreate btc = new SCbtcMaintenanceOrderCreate(maintenanceId, max_cycles, mode);

        btc.batchprocessid  = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    }  

    public static List<SCOrder__c> syncCreate(ID maintenanceID, Integer max_cycles, String mode)
    {
        System.debug('###mode: ' + mode);
        SCbtcMaintenanceOrderCreate btc = new SCbtcMaintenanceOrderCreate(maintenanceId, max_cycles, mode);
        btc.synchron = true;
        Boolean startCoreReturnsRetValue = false;
        Boolean aborted = false;        
        btc.startCore(startCoreReturnsRetValue, aborted); // calculates the query 
        List<SObject> mList = Database.query(btc.query); // runs the query        
        List<SCOrder__c> retValue = btc.executeCore(mList);
        return retValue;
    }  

/*
    Webservice static String canCreateOrderList(List<String> maintenanceIdList)
    {
        Boolean ret = false;
        if(maintenanceIdList != null && maintenanceIdList.size() > 0)
        {
            List<SCMaintenance__c> maintenanceList = [select ID 
                                                from SCMaintenance__c 
                                                where  
                                                id in :maintenanceIdList 
                                                and Order__c = null
                                                and MaintenanceFirstDate__c <> null 
                                                and Status__c in('active')
                                                and InstalledBase__r.MaintenanceEnabled__c = '1'
                                                and MaintenanceDateProposal__c <> null
                                                and InstalledBase__r.ShipTo__c <> null];
           if(maintenanceList.size() > 0)
           {
               return 'OK';
           }
        } 
        return System.Label.SC_msg_MaintenanceCreateOrder;       
    }

    Webservice static void syncCreateOrderList(List<String> maintenanceIdList)
    {
        if(maintenanceIdList == null || maintenanceIdList.size() == 0)
        {
            return;
        }   
        Integer max_cycles = 1;
        String mode = 'trace';
        for(String id: maintenanceIdList)
        {
            SCbtcMaintenanceOrderCreate.syncCreate(id, max_cycles, mode);
        }   
    }
*/
    Webservice static String canCreateOrder(String maintenanceId)
    {
        Boolean ret = false;
        if(maintenanceId != null)
        {
            List<SCMaintenance__c> maintenanceList = [select ID 
                                                from SCMaintenance__c 
                                                where  
                                                id  = :maintenanceId 
                                                and Order__c = null
                                                and MaintenanceFirstDate__c <> null 
                                                and Status__c in('active')
                                                and InstalledBase__r.MaintenanceEnabled__c = '1'
                                                and MaintenanceDateProposal__c <> null
                                                and InstalledBase__r.ShipTo__c <> null];
           if(maintenanceList.size() > 0)
           {
               return 'OK';
           }
        } 
        return System.Label.SC_msg_MaintenanceCreateOrder;       
    }

    Webservice static List<SCOrder__c> syncCreateOrder(String maintenanceId)
    {
        if(maintenanceId == null)
        {
            return null;
        }   
        Integer max_cycles = 1;
        String mode = 'trace';
        List<SCOrder__c> retValue = SCbtcMaintenanceOrderCreate.syncCreate(maintenanceId, max_cycles, mode);
        return retValue;
    }

   /*
    * Called by the framework when the batch job is started. We construct the soql statement
    * @param BC the batch context
    * @return the query locator with the selected mainenances
    */
    global override Database.QueryLocator start(Database.BatchableContext BC)
    {
        debug('start');
        Boolean aborted = false;
        if(abortOneOfTwoSameJobsRunning(BC.getJobId(), 'SCbtcMaintenanceOrderCreate', 'start'))
        {
            aborted = true;
        }
        
        Boolean startCoreReturnsRetValue = true;
        return startCore(startCoreReturnsRetValue, aborted);
    } // start

    public Database.QueryLocator startCore(Boolean returnsRetValue, Boolean aborted)
    {
        // one order will be created from one SContractVisit__c record 
        // one order will be created from one SContractVisit__c record 
        
        /* To check the syntax of SOQL statement */
        List<SCMaintenance__c> maintenanceList = [select EndDate__c, StartDate__c, PriceList__c, MaintenanceDateProposal__c,
                                                    InstalledBase__r.Brand__c,
                                                    MaintenancePlan__r.OrderPrio__c,
                                                    MaintenancePlan__r.OrderType__c,
                                                    MaintenancePlan__r.MaintenanceActionDesc__c,
                                                    MaintenancePlan__r.Description__c,
                                                    MaintenancePlan__r.Duration__c, 
                                                    InstalledBase__r.Id,
                                                    InstalledBase__r.Stock__r.Plant__r.Name,
                                                    InstalledBase__r.ShipTo__c, 
                                                    InstalledBase__r.ShipTo__r.Id,
                                                    InstalledBase__r.SoldTo__c,
                                                    InstalledBase__r.SoldTo__r.Id,
                                                    id
                                                    from SCMaintenance__c 
                                                    where InstalledBase__r.ShipTo__c <> null 
                                                    and MaintenanceDateProposal__c <> null 
                                                    and MaintenanceFirstDate__c <> null 
                                                    and Order__c = null 
                                                    and InstalledBase__r.MaintenanceEnabled__c = '1' 
                                                    and Status__c = 'active'
                                                    and OrderCreationDate__c < TODAY
                                                    limit 1];
        
        debug('start: after checking statement');
        /**/
        if(maintenanceId == null)
        {
            query = ' select '
                    + ' EndDate__c, StartDate__c, PriceList__c, MaintenanceDateProposal__c, '
                    + ' InstalledBase__r.Brand__c, '
                    + ' MaintenancePlan__r.OrderPrio__c, '
                    + ' MaintenancePlan__r.OrderType__c, '
                    + ' MaintenancePlan__r.MaintenanceActionDesc__c, '
                    + ' MaintenancePlan__r.Description__c, '
                    + ' MaintenancePlan__r.Duration__c, ' 
                    + ' InstalledBase__r.Id, '
                    + ' InstalledBase__r.Stock__r.Plant__r.Name, InstalledBase__r.Plant__c, InstalledBase__r.ProductGroup__c, InstalledBase__r.ProductUnitClass__c, InstalledBase__r.ProductUnitType__c, '
                    + ' InstalledBase__r.ProductSkill__c, InstalledBase__r.Brand__r.Name, '
                    + ' InstalledBase__r.ShipTo__c, ' 
                    + ' InstalledBase__r.SoldTo__c, '
                    + ' InstalledBase__r.ShipTo__r.Id, '
                    + ' InstalledBase__r.SoldTo__r.Id, '
                    + ' InstalledBase__r.Description__c, '
                    + ' InstalledBase__r.LocationName__c, '
                    + ' InstalledBase__r.Building__c, '
                    + ' InstalledBase__r.Floor__c, '
                    + ' InstalledBase__r.Room__c, '
                    + ' InstalledBase__r.Phone__c, '
                    + ' InstalledBase__r.ProductModel__c, '
                    + ' InstalledBase__r.SerialNo__c, '
                    + ' InstalledBase__r.IdInt__c, '
                    + ' InstalledBase__r.IdExt__c, '
                    + ' InstalledBase__r.InstallationDate__c, '
                    + ' InstalledBase__r.AuditLast__c, '
                    + ' InstalledBase__r.AuditHint__c, '
                    + ' InstalledBase__r.AcquisitionValue__c, '
                    + ' InstalledBase__r.AcquisitionCurrency__c, '
                    + ' InstalledBase__r.BookValue__c, '
                    + ' InstalledBase__r.BookValueCurrency__c, '  
                    + ' InstalledBase__r.InstalledBaseLocation__r.PostalCode__c, '
                    + ' InstalledBase__r.InstalledBaseLocation__r.Country__c, '
                    + ' id '            
                    + ' from SCMaintenance__c where InstalledBase__r.ShipTo__c <> null and MaintenanceDateProposal__c <> null '
                + ' and MaintenanceFirstDate__c <> null and Order__c = null '
                + ' and OrderCreationDate__c < TODAY'
                + ' and InstalledBase__r.MaintenanceEnabled__c = \'1\' and Status__c = \'active\'';

            if(extraCondition != null)
            {
                query += ' and (' + extraCondition + ')';
            }                   
             
            if(aborted)
            {
                query += ' limit 0';
            }
            else
            {
                if(max_cycles > 0)
                {
                    query += ' limit ' + max_cycles;
                }
            }               
        }
        else
        {
            query = ' select '
                    + ' EndDate__c, StartDate__c, PriceList__c, MaintenanceDateProposal__c, '
                    + ' InstalledBase__r.Brand__c, '
                    + ' MaintenancePlan__r.OrderPrio__c, '
                    + ' MaintenancePlan__r.OrderType__c, '
                    + ' MaintenancePlan__r.MaintenanceActionDesc__c, '
                    + ' MaintenancePlan__r.Description__c, '
                    + ' MaintenancePlan__r.Duration__c, ' 
                    + ' InstalledBase__r.Id, '
                    + ' InstalledBase__r.Stock__r.Plant__r.Name, InstalledBase__r.Plant__c, InstalledBase__r.ProductGroup__c, InstalledBase__r.ProductUnitClass__c, InstalledBase__r.ProductUnitType__c, '
                    + ' InstalledBase__r.ProductSkill__c, InstalledBase__r.Brand__r.Name, '
                    + ' InstalledBase__r.ShipTo__c, ' 
                    + ' InstalledBase__r.SoldTo__c, '
                    + ' InstalledBase__r.ShipTo__r.Id, '
                    + ' InstalledBase__r.SoldTo__r.Id, '
                    + ' InstalledBase__r.Description__c, '
                    + ' InstalledBase__r.LocationName__c, '
                    + ' InstalledBase__r.Building__c, '
                    + ' InstalledBase__r.Floor__c, '
                    + ' InstalledBase__r.Room__c, '
                    + ' InstalledBase__r.Phone__c, '
                    + ' InstalledBase__r.ProductModel__c, '
                    + ' InstalledBase__r.SerialNo__c, '
                    + ' InstalledBase__r.IdInt__c, '
                    + ' InstalledBase__r.IdExt__c, '
                    + ' InstalledBase__r.InstallationDate__c, '
                    + ' InstalledBase__r.AuditLast__c, '
                    + ' InstalledBase__r.AuditHint__c, '
                    + ' InstalledBase__r.AcquisitionValue__c, '
                    + ' InstalledBase__r.AcquisitionCurrency__c, '
                    + ' InstalledBase__r.BookValue__c, '
                    + ' InstalledBase__r.BookValueCurrency__c, '
                    + ' InstalledBase__r.InstalledBaseLocation__r.PostalCode__c, '
                    + ' InstalledBase__r.InstalledBaseLocation__r.Country__c, '
                    + ' id '            
                + ' from SCMaintenance__c where InstalledBase__r.ShipTo__c <> null and MaintenanceDateProposal__c <> null'
                + ' and MaintenanceFirstDate__c <> null and Order__c = null'
                + ' and InstalledBase__r.MaintenanceEnabled__c = \'1\' and Status__c = \'active\''
                + ' and ID = \'' + maintenanceId + '\'';
            if(aborted)
            {
                query += ' limit 0';
            }
            else
            {
                if(max_cycles > 0)
                {
                    query += ' limit ' + max_cycles;
                }
            }
        }
        debug('query: ' + query);
        Database.QueryLocator retValue = null;
        if(returnsRetValue)
        {
            retValue = Database.getQueryLocator(query);
        }
        return retValue;
    }

   /*
    * Called for each batch of records to process.
    * @param BC the batch context
    * @param scope the list records to be processed
    */
    global override void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        debug('execute');
        if(abortOneOfTwoSameJobsRunning(BC.getJobId(), 'SCbtcMaintenanceOrderCreate', 'execute'))
        {
            return;
        }
        executeCore(scope);
    } // execute

    public List<SCOrder__c> executeCore(List<sObject> scope)
    {
        List<SCOrder__c> retValue = new List<SCOrder__c>();
        List<SCMaintenance__c> maintenanceUpdateList = new List<SCMaintenance__c>();
        
        for(sObject rec: scope)
        {
            debug('sObject: ' + rec);
            SCMaintenance__c m = (SCMaintenance__c)rec;
            // create an order tree
            SCboOrder boOrder = createOrderTree(m, maintenanceUpdateList);
            if(boOrder != null && boOrder.order != null)
            {
                retValue.add(boOrder.order);
            }   
        }
        debug('mainenanceUpdateList: ' + maintenanceUpdateList);
        update maintenanceUpdateList;
        return retValue;
    }
   /*
    * Called by the framework when the batch job has been completed. 
    * We send an e-mail notification about the status
    * @param BC the batch context
    */
    global override void finish(Database.BatchableContext BC)
    {
    } // finish

    /**
     * Constructor
     * @param mode could be 'test' or 'productive'
     */
    public SCbtcMaintenanceOrderCreate(Integer max_cycles, String mode)
    {
        this(null, max_cycles, mode, null);
    }

    public SCbtcMaintenanceOrderCreate(Integer max_cycles, String mode, String extraCondition)
    {
        this(null, max_cycles, mode, extraCondition);
    }

    public SCbtcMaintenanceOrderCreate(Id maintenanceId, Integer max_cycles, String mode)
    {
        this(maintenanceId, max_cycles, mode, null);
    }
    /**
     * Constructor
     * @param contractID
     * @param mode could be 'test' or 'productive'
     */
    public SCbtcMaintenanceOrderCreate(Id maintenanceId, Integer max_cycles, String mode,
            String extraCondition)
    {
        this.maintenanceId = maintenanceId;
        this.max_cycles = max_cycles;
        this.mode = mode;
        this.extraCondition = extraCondition;
        this.orderItemRecordTypeId = getOrderItemRecordType ();
    }

    public String getQuery()
    {
        return query;
    }    

    public static final String RESULT_INFO = 'RESULT_INFO';
    public static final String DATA = 'DATA';
    public static final String IMAGINARY_DUE_DATE = 'IMAGINARY_DUE_DATE';
    public static final String FM_IN_SUSPENSION = 'FM_IN_SUSPENSION';
    
    public SCboOrder createOrderTree(SCMaintenance__c m, List<SCMaintenance__c> maintenanceUpdateList)
    {
        debug('createOrderTree');
        debug('maintenance: ' + m);
        SCboOrder boOrder = null;
        DateTime start = Datetime.now();
        Boolean thrownException = false;
        String resultInfo = '';
        Integer count = 0;
        String step = '';
        Savepoint sp = Database.setSavepoint();
        ID orderId = null;
        try
        {
            step = 'read user';
            User user = getUser();
            step = 'read accountInfo';
            ID accountID = m.InstalledBase__r.ShipTo__c;
            SCAccountInfo__c accountInfo = getAccountInfo(accountID);
            step = 'create order';
            debug(step);
            boOrder = createOrder(m, user, accountInfo);
            
            step = 'create order roles';
            debug(step);
            createOrderRoles(boOrder, m);
            
            step = 'create an order item';
            debug(step);
            createOrderItem(boOrder, m);

            step = 'save';
            debug(step);
            // roles, items, a line and qualification are save alltogether
            boOrder.save();
            count = 1;
            orderId = boOrder.order.Id;
            m.Order__c = orderId;
            m.OrderStatus__c = 'created';
            debug('order created successfully');       
            
            if(synchron)
            {
                // CCEAG --> 
                // for asynchronous calls there is an extra job SCbtcMaintenanceOrderToSAP
                // as we have no way to call a future call in a batch job / future method
                
                // This function is only called when the maintenance order is created 
                // manually by pressing the button [Create order]
                // now call the SAP interface to create the SAP order (asynchronously)
                // the Trigger SCOrder_AI_CallSapWebService is suppressing the async callout !
                Boolean async = true; // we call the web service asynchronously 
                CCWCOrderCreate.callout(orderId, async, false);
                // CCEAG <-- 
            }
            debug('succesful end of execute');       
            maintenanceUpdateList.add(m);
            count++;

    

        }
        catch(Exception e)
        {
            thrownException = true;
            resultInfo = 'step: ' + step + ', exception: '  + SCfwException.getExceptionInfo(e);
            String prevMode = mode;
            mode = 'test';
            debug('resultInfo:' + resultInfo);
            mode = prevMode;
            Database.rollback(sp);
        }
        finally
        {
            String resultCode = 'E000';
            if(thrownException)
            {
                resultCode = 'E101';
            }
            SCInterfaceLog.logBatchInternalExt('MAINTENANCE_ORDER', 'SCbtcMaintenanceOrderCreate',
                                m.Id, orderId, resultCode, 
                                resultInfo, null, start, count); 
        }

        return boOrder;
    }
   
    private SCboOrder createOrder(SCMaintenance__c m, User user, SCAccountInfo__c ai)
    {
        SCboOrder ret = null;
        SCOrder__c o = new SCOrder__c();
        CCWSUtil u = new CCWSUtil();
        o.Brand__c = m.InstalledBase__r.Brand__c;
            
        o.cce_workcenter__c = user.Workcenter__c;
                
        o.Channel__c = SCfwConstants.DOMVAL_CHANNEL_FROM_CONTRACT_OR_MAINTENANCE;

        o.Country__c = appSettings.DEFAULT_COUNTRY__c; //   DE

        o.CustomerPrefEnd__c = m.EndDate__c;
        o.CustomerPrefStart__c = m.StartDate__c;
        o.CustomerPriority__c = m.MaintenancePlan__r.OrderPrio__c; //2101

        String defCustomerTimeWindow = appSettings.DEFAULT_CUSTOMERTIMEWINDOW__c;
        o.CustomerTimewindow__c = defCustomerTimeWindow; //'12404';    // AM till 13:00 = 12404, All day = 12401
        if(o.CustomerTimewindow__c == null)
        {
            o.CustomerTimewindow__c = '12401';
        }

        o.DepartmentCurrent__c = getDepartment(m.InstalledBase__r.Stock__r.Plant__r.name);
         
        o.DepartmentResponsible__c = o.DepartmentCurrent__c;
        o.DepartmentTurnover__c = o.DepartmentCurrent__c;

        o.Description__c = m.MaintenancePlan__r.Description__c + '\n' + m.MaintenancePlan__r.MaintenanceActionDesc__c;  
        if(o.Description__c != null && o.Description__c.length() > 2000)
        {
            o.Description__c = o.Description__c.substring(0, 1998);
        }

        o.CompanyCode__c = ai.CompanyCode__c;
        o.SalesArea__c = ai.SalesArea__c;
        o.SalesGroup__c = ai.SalesGroup__c; 
        o.SalesOffice__c = ai.SalesOffice__c;
        o.ServiceArea__c = ai.ServiceArea__c;
        o.Info__c = o.DistributionChannel__c = ai.DistributionChannel__c;
        o.Division__c = ai.Division__c; 

        o.DurationUnit__c = SCfwConstants.DOMVAL_INTERVALLTIME_MINUTE;
        o.Duration__c = m.MaintenancePlan__r.Duration__c;
        Decimal duration = 60;
        
        if(o.Duration__c == null && appSettings.DEFAULT_ORDERITEM_DURATION__c != null && appSettings.DEFAULT_ORDERITEM_DURATION__c > 0)
        {
            o.Duration__c = appSettings.DEFAULT_ORDERITEM_DURATION__c;
        }

        o.InvoicingSubtype__c = SCfwConstants.DOMVAL_INVOICINGSUBTYPE_DEFAULT;
        o.Type__c = m.MaintenancePlan__r.OrderType__c; 

        o.PriceList__c  = m.Pricelist__c;
        o.InvoicingType__c  = getInvoicingType(o.PriceList__c, o.Type__c);

        o.Maintenance__c = m.id;
        o.PaymentType__c = SCfwConstants.DOMVAL_PAYMENTTYPE_INVOICE;
        
        o.PlannerGroup__c = user.ERPPlannerGroup__c;    
        o.Status__c = SCfwConstants.DOMVAL_ORDERSTATUS_OPEN;
        o.UsedBy__c = 'Service';

        ret = new SCboOrder(o);
        debug('boOrder: ' + ret);
        return ret;
    }// createOrder
   
    /**
     * Creates order roles LE, AG, RE, RG
     */
    public void createOrderRoles(SCboOrder boOrder, SCMaintenance__c m)
    {
        ID leId = m.InstalledBase__r.ShipTo__r.Id;
        ID agId = m.InstalledBase__r.SoldTo__r.Id;
        if(agId == null)
        {
            agId = leId;    
        }
        boOrder.setRole(leId, SCfwConstants.DOMVAL_ORDERROLE_LE, false);
        boOrder.setRole(agId, SCfwConstants.DOMVAL_ORDERROLE_AG, false);
        boOrder.setRole(agId, SCfwConstants.DOMVAL_ORDERROLE_RE, false);
        boOrder.setRole(agId, SCfwConstants.DOMVAL_ORDERROLE_RG, false);
        debug('boOrder.allRole Default:  ' + boOrder.allRole);
    }// createOrderRoles

    public SCOrderItem__c createOrderItem(SCboOrder boOrder, SCMaintenance__c m)
    {
        Double duration = 0;

        Id orderBrandId = boOrder.order.Brand__r.Id;
        SCOrderItem__c orderItem = new SCOrderItem__c();
        orderItem.RecordTypeId = orderItemRecordTypeId;
        orderItem.Order__c = boOrder.order.Id;
        orderItem.Order__r = boOrder.order;
        orderItem.InstalledBase__c = m.InstalledBase__r.Id;
        orderItem.Duration__c = boOrder.order.Duration__c;
        orderItem.DurationUnit__c = boOrder.order.DurationUnit__c;
        orderItem.CompletionStatus__c = SCfwConstants.DOMVAL_COMPLETIONSTATUS_DEFAULT;
        orderItem.Completed__c =  null;
        // add boOrderItem for Qualifications
        SCOrderRole__c curRole = boOrder.getRole(SCfwConstants.DOMVAL_ORDERROLE_LE);
        String language = 'DE';
        if (null != curRole)
        {
            language = curRole.LocaleSidKey__c;
        }
        orderItem.ErrorText__c = boOrder.order.Description__c;
        orderItem.InstalledBase__r = m.InstalledBase__r;

        SCboOrderItem boOrderItem = new SCboOrderItem(orderItem);
        boOrderItem.initQualifications(language);
        boOrderItem.copyDataFromInstBase();
        boOrder.boOrderItems.add(boOrderItem);
        return orderItem;
    } // createOrderItem




    public User getUser()
    {
        User retValue = null;
        ID userId = UserInfo.getUserId();
        if(userId != null)
        {
            List<User> ul = [select WorkCenter__c, ERPPlannerGroup__c, ERPWorkCenter__c, ERPPurchaseGroup__c, ERPPurchaseOrg__c, DefaultBusinessUnit__c from User where id = : userId];
            if(ul.size() > 0)
            {
                retValue = ul[0];
            }
        }    
        return retValue;
    }  

    public SCAccountInfo__c getAccountInfo(ID accountID)
    {
        SCAccountInfo__c retValue = null;
        if(accountId != null)
        {
            List<SCAccountInfo__c> aiList = [SELECT Account__c,CompanyCode__c,DistributionChannel__c,Division__c,GeoX__c,GeoY__c,Id,ID2__c,Name,SalesArea__c,SalesGroup__c,SalesOffice__c,ServiceArea__c,Status__c  
                                        FROM SCAccountInfo__c where Account__c = : accountID];
            if(aiList.size() > 0)
            {
                retValue = aiList[0];
            }                                       
        }                                       
        return retValue;
    }

   
    public static ID getDepartment(String plantName)
    {
        ID retValue = null;
        if(plantName != null && plantName != '')
        {
            List<SCBusinessUnit__c> items = [Select ID, Name from SCBusinessUnit__c  where ID2__c =: plantName or name =: plantName order by name];
            if(items.size() > 0)
            {
                retValue = items[0].Id;
            }
        }
        return retValue;
    }

    public String getInvoicingType(ID priceListId, String orderType)
    {
        String retValue = null;
        List<CCBillingIndicator__c> biList = [SELECT InvoicingType__c, OrderType__c,Pricelist__c FROM CCBillingIndicator__c
                                             where OrderType__c = : orderType and PriceList__c = : priceListId];
        if(biList.size() > 0)
        {
            retValue = biList[0].InvoicingType__c;
        }                                            
        return retValue;
    }


    public ID getOrderItemRecordType ()
    {
        ID retValue = null;
        List<RecordType> rtList = [SELECT Id  FROM RecordType where SobjectType = 'SCOrderItem__c' and DeveloperName = 'Equipment'];
        if(rtList.size() > 0)
        {
            retValue = rtList[0].id;
        }
        return retValue;
    }   

    private void debug(String text)
    {
        if(mode.equalsIgnoreCase('test')
           || mode.equalsIgnoreCase('trace'))
        {
            System.debug('###...................' + text);
        }
    }
}
