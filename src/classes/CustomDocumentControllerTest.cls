/*************************
!!! NO ASSERTS !!! - NO DEPLOYMENT!!!
*************************/

@isTest
private class CustomDocumentControllerTest {
 static testMethod void myUnitTest() {
        // create Account
        List<Account> newAccounts = createAccount();

        //create CustomDocument
        CustomDocument__c newDoc = createDoc(newAccounts);
        
/*****************************************************************
    PART 1 - MAIN TEST
*****************************************************************/      
        
        //PageReference pageRef = Page.CokeConnectBibliotheken;
        PageReference pageRef = Page.CustomDocumentAction;
        pageRef.getParameters().put('id',newDoc.Id);
        pageRef.getParameters().put('action','show');
        
        Test.setCurrentPageReference(pageRef);
        Test.setCurrentPage(pageRef);     
           
        CustomDocument__c tmpDoc = new CustomDocument__c();
        Attachment tmpAtt = new Attachment();
        ApexPages.StandardController stdDoc = new ApexPages.StandardController(tmpDoc);
        CustomDocumentController CCBE = new CustomDocumentController(stdDoc);
        //Test.startTest();
        
        system.debug('### newDoc Id:' + ApexPages.currentPage().getParameters().get('id'));
        system.debug('### newDoc CCBE.myCDoc:' + CCBE.myCDoc);
        CCBE.myfile = tmpAtt;
        CCBE.editDocument();
        CCBE.saveDocument();
        
        CCBE.showList();
        
        // CREATE NEW DOC
        CustomDocument__c new2Doc = new CustomDocument__c();
        new2Doc.Title__c = 'Doc 2';
        //newDoc.SalesArea__c = '';
        new2Doc.ValidFrom__c = date.today();
        //new2Doc.ValidKeyAccountNumber__c = newAccounts[0].Id;
        
        tmpAtt.Name = 'File 1';
        tmpAtt.Body = Blob.valueOf('Unit Test Attachment Body');
        
        CCBE.newDocument();
        CCBE.myCDoc = new2Doc;
        CCBE.myfile = tmpAtt;
        CCBE.saveDocument();
        CCBE.newDocument();
        CCBE.cancel();
        CCBE.showList();

        CCBE.getmyfile();
        CCBE.deleteAttachment();

        CCBE.myCDoc = new2Doc;
        CCBE.myfile = tmpAtt;
        CCBE.saveDocument();
        CCBE.deleteAttachment();

/*****************************************************************
    PART 2 - ID +  edit
*****************************************************************/
        //PageReference pageRef2 = Page.CokeConnectBibliotheken;
        PageReference pageRef2 = Page.CustomDocumentAction;
        pageRef2.getParameters().put('id',new2Doc.Id);
        pageRef2.getParameters().put('action','edit');
        
        Test.setCurrentPageReference(pageRef2);
        Test.setCurrentPage(pageRef2);
        
        ApexPages.StandardController stdDoc2 = new ApexPages.StandardController(tmpDoc);
        CustomDocumentController CCBE2 = new CustomDocumentController(stdDoc2);
        
        CCBE2.cancel();
   
/*****************************************************************
    PART 3 - only ID
*****************************************************************/
        //PageReference pageRef3 = Page.CokeConnectBibliotheken;
        PageReference pageRef3 = Page.CustomDocumentAction;
        pageRef3.getParameters().put('id',new2Doc.Id);

        
        Test.setCurrentPageReference(pageRef3);
        Test.setCurrentPage(pageRef3);
        
        ApexPages.StandardController stdDoc3 = new ApexPages.StandardController(tmpDoc);
        CustomDocumentController CCBE3 = new CustomDocumentController(stdDoc3);       
        
/*****************************************************************
    PART 4 - NO ID
*****************************************************************/
        //PageReference pageRef4 = Page.CokeConnectBibliotheken;
        PageReference pageRef4 = Page.CustomDocumentAction;
        Test.setCurrentPageReference(pageRef4);
        Test.setCurrentPage(pageRef4);

        ApexPages.StandardController stdDoc4 = new ApexPages.StandardController(tmpDoc);
        CustomDocumentController CCBE4 = new CustomDocumentController(stdDoc4);           

/*****************************************************************
    PART 5 - only action=new
*****************************************************************/
        //PageReference pageRef5 = Page.CokeConnectBibliotheken;
        PageReference pageRef5 = Page.CustomDocumentAction;
        pageRef5.getParameters().put('action','new');
        
        Test.setCurrentPageReference(pageRef5);
        Test.setCurrentPage(pageRef5);

        ApexPages.StandardController stdDoc5 = new ApexPages.StandardController(tmpDoc);
        CustomDocumentController CCBE5 = new CustomDocumentController(stdDoc5);
        
        CCBE5.cancel();      
        
    }
    
    // create test Account
    public static List<Account> createAccount()
    {
        List<Account> newAccounts = new List<Account>();    
        Account acc1 = new Account();
        acc1.Name = 'Sector 7';
        //acc1.OwnerId = newUsers[0].Id;
        newAccounts.add(acc1);
        
        insert newAccounts;
        return newAccounts;
    }
    
    // create test FAQ
    public static CustomDocument__c createDoc(List<Account> newAccounts)
    {
        // create FAQ
        CustomDocument__c newDoc = new CustomDocument__c();  
        newDoc.Title__c = 'Doc 1';
        //newDoc.SalesArea__c = '';
        newDoc.ValidFrom__c = date.today();
        newDoc.ValidTo__c = date.today().addDays(1);
        //newDoc.ValidKeyAccountNumber__c = newAccounts[0].Id;
    
        insert newDoc;      
        return newDoc;
    }
}
