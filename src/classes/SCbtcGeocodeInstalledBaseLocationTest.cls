/*
 * @(#)SCbtcGeocodeInstalledBaseLocationTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCbtcGeocodeInstalledBaseLocationTest
{
    static testMethod void testDEPositive()
    {
        Test.StartTest();
        List<SCInstalledBaseLocation__c> la = [Select id from SCInstalledBaseLocation__c where Country__c = 'DE' limit 1];
        if(la.size() > 0)
        {
            Id locationId = la[0].Id;
            String strId = locationId + '';
            SCbtcGeocodeInstalledBaseLocation.syncGeocodeLocation(strId);
            List<String> idList = new List<String>();
            idList.add(strId);
            SCbtcGeocodeInstalledBaseLocation.syncGeocodeLocations(idList);
        }
        SCbtcGeocodeInstalledBaseLocation.asyncGeocodeAll('ALL', 'DE', null,  1, 'trace');    
        Test.StopTest();
    }

    static testMethod void testDEPositive2()
    {
        Test.StartTest();
        List<SCInstalledBaseLocation__c> la = [Select id from SCInstalledBaseLocation__c where Country__c = 'DE' limit 1];
        if(la.size() > 0)
        {
            Id locationId = la[0].Id;
            String strId = locationId + '';
            SCbtcGeocodeInstalledBaseLocation.asyncGeocodeLocation(strId, 'trace');
            List<String> idList = new List<String>();
            idList.add(strId);
            SCbtcGeocodeInstalledBaseLocation.asyncGeocodeLocations(idList);
        }
        SCbtcGeocodeInstalledBaseLocation.asyncGeocodeAll('ALL', 'DE', null,  1, 'trace');    
        Test.StopTest();
    }


    static testMethod void testNLPositive()
    {
        Test.StartTest();
        List<SCInstalledBaseLocation__c> la = [Select id from SCInstalledBaseLocation__c where Country__c = 'NL' limit 1];
        if(la.size() > 0)
        {
            Id locationId = la[0].Id;
            String strId = locationId + '';
            SCbtcGeocodeInstalledBaseLocation.syncGeocodeLocation(strId);
            List<String> idList = new List<String>();
            idList.add(strId);
            SCbtcGeocodeInstalledBaseLocation.syncGeocodeLocations(idList); 
        }
        if(la.size() > 0)
        {
            Id locationId = la[0].Id;
            String strId = locationId + '';
            SCbtcGeocodeInstalledBaseLocation.asyncGeocodeLocation(strId, 'trace');
            List<String> idList = new List<String>();
            idList.add(strId);
            SCbtcGeocodeInstalledBaseLocation.asyncGeocodeLocations(idList);
        }
        SCbtcGeocodeInstalledBaseLocation.asyncGeocodeAll('ALL', 'NL', null,  1, 'trace');    
        Test.StopTest();
    }
    
    //@author GMSS
    static testMethod void testCodeCoverageA()
    {
        try { SCHelperTestClass3.createCustomSettings('DE',true); } catch(Exception e) {}
        try { SCHelperTestClass.createDomsForOrderCreation(); } catch(Exception e) {}
        
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        
        SCInstalledBaseLocation__c location = new SCInstalledBaseLocation__c();
        Database.insert(location);
        
        
        Test.startTest();
        
        
        try { SCbtcGeocodeInstalledBaseLocation.asyncGeocodeLocation(location.Id, 'trace'); } catch(Exception e) {}
        try { SCbtcGeocodeInstalledBaseLocation.asyncGeocodeLocation(null, 'trace'); } catch(Exception e) {}
        try { SCbtcGeocodeInstalledBaseLocation.asyncGeocodeLocations(new List<String>{location.Id}); } catch(Exception e) {}
        try { SCbtcGeocodeInstalledBaseLocation.asyncGeocodeLocations(new List<String>()); } catch(Exception e) {}
        try { SCbtcGeocodeInstalledBaseLocation.syncGeocodeLocations(new List<String>{location.Id}); } catch(Exception e) {}
        try { SCbtcGeocodeInstalledBaseLocation.syncGeocodeLocations(new List<String>()); } catch(Exception e) {}
        try { SCbtcGeocodeInstalledBaseLocation.syncGeocodeLocation(location.Id); } catch(Exception e) {}
        try { SCbtcGeocodeInstalledBaseLocation.syncGeocodeLocation(null); } catch(Exception e) {}
        
        SCbtcGeocodeInstalledBaseLocation obj = new SCbtcGeocodeInstalledBaseLocation(new List<String>{location.Id}, 'trace');
        AvsAddress add = obj.getAddr(location);
        
        try { obj.execute(null, new List<sObject>{location}); } catch(Exception e) {}
        try { obj.geocode(location, true, new List<SCInterfaceLog__c>()); } catch(Exception e) {}
        try { obj.geocode(location, false, new List<SCInterfaceLog__c>()); } catch(Exception e) {}
        try { obj.setGeoCode(false, false, add, null, null, location); } catch(Exception e) {}
        
        
        Test.stopTest();
    }  

}
