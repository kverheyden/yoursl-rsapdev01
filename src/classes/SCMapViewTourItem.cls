public with sharing class SCMapViewTourItem {
		
	
	// ENG location of an engineer
    // ORD location of an order
    public String itemtype { get; set; }
    
    // the item name
    public String name {get; set;}
    
    // location longitude
    public double GeoX { get; set; }
    
    // location latitude
    public double GeoY { get; set; }
    
    // planned start and end (internal appointment)
    public String dateString {get; set; }
    public String startTime { get; set; }
    public String endTime { get; set; }
    
    public String prefStart { get; set; }
    public String prefEnd { get; set; }
    //only for calculation
    public DateTime endTimeNotFormatted {get; set; }
    public DateTime startTimeNotFormatted {get; set; }
    
    // street
    public String street { get; set; }
    
    //House No
    public String houseNo { get; set; }
    
    //city
    public String city { get; set; }
    
    //postal code
    public String postalCode { get; set; }
    
    public String formattedAddress { get; set; }
    
    // failure type
    public String failure { get; set; }
    
    // order description 
    public String description {get; set;}
     
    //the order position
    public Integer orderIndex { get; set; }
    
    //duration
    public String duration { get; set; }
    
    //distance
    public String distance {get; set; }
    
    //duration
    public Integer durationInteger { get; set; }
    
    //distance
    public Integer distanceInteger {get; set; }
    
    //order id
    public String orderId {get; set;}
    
    //order reason
    
    
    public List<SCOrderRole__c> orderRoles {get; set;} 
    
    //engineer id
    public String engineerId {get; set;}
    
    //product name
    public String productName {get; set;}
    
    //resource name
    public String resourceFullName {get; set;}
    
    //resource id
    public String resourceId {get; set;}
    
    //Temp values for replacing old SCMapTourItem2
    public String resourceUrl {get; set;}
    public String resourceName {get; set;}
    public String resourceAddress {get; set;}
    public String resourceMobile {get; set;}
    public String resourcePhone {get; set;}
    public String resourceEmail {get; set;}
	public String objectId {get; set;}
	
	
	
	public String orderno {get; set;}
	public String apptype {get; set;}
	public String status {get; set;}
	public String orderDate {get; set;}
	public DateTime dtEnd {get; set;}
	public String priority {get; set;}
	public String orderFailureType {get; set;}
	public String orderRecipient {get; set;}
	public String infoAddress {get; set;}
	
	
	
            		
             
             
             
    
}
