@isTest
private class T_RedSurveyResultAfterInsertTest {
	
	static testMethod void testInsertEmptyRedSurveyResult() {		
		
		RedSurveyResult__c rsr = new RedSurveyResult__c();		
		insert rsr;
		
		List<RedSurveyResult__c> redSurveys = [SELECT Id FROM RedSurveyResult__c];
		System.assertEquals(1, redSurveys.size());
	}
	
	static testMethod void testInsertRedSurveyToAccount() {
		Account a = new Account();
		a.Name = 'Test account';
		insert a;
		
		RedSurveyResult__c rsr = new RedSurveyResult__c();
		rsr.Source__c = 'SalesApp';
		rsr.Account__c = a.Id;
		insert rsr;
		
	}
	
    static testMethod void testVisUpdate() {
    	
        Account testAcc = new Account();
		testAcc.Name = 'Test Account';
		testAcc.BillingCountry__c = 'DE';
		insert testAcc;
		
		Visitation__c testVis = new Visitation__c();
		testVis.Account__c = testAcc.Id;
		testVis.Type__c = 'SRP';
		testVis.Time__c = DateTime.valueOf('2014-01-01 12:15:30.000');
		insert testVis;
		
		// valid RedSurveyResult__c
		List<RedSurveyResult__c> testRedSR = new List<RedSurveyResult__c>();
		RedSurveyResult__c testRedSR_1 = new RedSurveyResult__c();
		testRedSR_1.Account__c = testAcc.Id;
		testRedSR_1.VisitationId__c = testVis.Id;
		testRedSR.add(testRedSR_1);

		// empty VisitationId__c
		RedSurveyResult__c testRedSR_2 = new RedSurveyResult__c();
		testRedSR_2.Account__c = testAcc.Id;
		testRedSR.add(testRedSR_2);
		
		insert testRedSR;
		
		List<Visitation__c> visitaionList = [SELECT Id, Type__c, Time__c FROM Visitation__c];
		List<Account> accountList = [SELECT Id, SalesLastVisit__c, RedActivation__c, RedArea__c, RedAssortment__c, 
									RedCooler__c, RedScore__c, REDScoreTrend__c, RedScoreDate__c, LastRedSurvey__c FROM Account];
	}	
}
