/**********************************************************************
Name:  CCWSCheckPromotionArticlesTest 

Test Class for CCWSCheckPromotionArticles

History                                                            
-------                                                            
Date  		AUTHOR								DETAIL 
02/14/2014 	Bernd Werner <b.werner@yoursl.de> 	creation of this class      
     
***********************************************************************/
@isTest
private class CCWSCheckPromotionArticlesTest {
 
    static testMethod void myUnitTest() {
    	List<Product2> lprodList	= new List<Product2>();
    	
        // Create a couple of Products
        Product2 oprod 		= new Product2();
        oprod.Name			= 'Test';
        oprod.IsActive		= true;
        lprodList.add(oprod);
        
        Product2 oprod2 	= new Product2();
        oprod2.Name			= 'Test';
        oprod2.IsActive		= true;
        lprodList.add(oprod2);
        
        Product2 oprod3 	= new Product2();
        oprod3.Name			= 'Test';
        oprod3.IsActive		= true;
        lprodList.add(oprod3);
        
        Product2 oprod4 	= new Product2();
        oprod4.Name			= 'Test';
        oprod4.IsActive		= true;
        lprodList.add(oprod4);
        
        insert lprodList;
        
        // Add a promotion
         // Create a Testpromotion
	    Promotion__c oTestPromotion		= new Promotion__c();
	    oTestPromotion.Name				= 'Testpromotion';
	    oTestPromotion.PromotionID__c	= '54321';
	    insert oTestPromotion;
	    
	    // Relate PRoducts to Promotion
	    List<PromotionArticle__c> lPromoArtList	= new List<PromotionArticle__c>();
	    PromotionArticle__c opa = new PromotionArticle__c();
	    opa.Article__c			= oprod.Id;
	    opa.Promotion__c		= oTestPromotion.Id;
	    opa.ArticleQuantity__c	= 1;
	    opa.Status__c			= 'Active';
	    lPromoArtList.add(opa);
	    
	    PromotionArticle__c opa2	= new PromotionArticle__c();
	    opa2.Article__c				= oprod2.Id;
	    opa2.Promotion__c			= oTestPromotion.Id;
	    opa2.ArticleQuantity__c		= 1;
	    opa2.Status__c				= 'Active';
	    lPromoArtList.add(opa2);
	    
	    PromotionArticle__c opa3	= new PromotionArticle__c();
	    opa3.Article__c				= oprod3.Id;
	    opa3.Promotion__c			= oTestPromotion.Id;
	    opa3.ArticleQuantity__c		= 1;
	    opa3.Status__c				= 'Active';
	    lPromoArtList.add(opa3);
	    
	    PromotionArticle__c opa4	= new PromotionArticle__c();
	    opa4.Article__c				= oprod4.Id;
	    opa4.Promotion__c			= oTestPromotion.Id;
	    opa4.ArticleQuantity__c		= 1;
	    opa4.Status__c				= 'Active';
	    lPromoArtList.add(opa4);
	    insert lPromoArtList;
	    
	    // Test no Promotion Id
	    CCWSCheckPromotionArticles.checkPromotionArticles('');
	   	// Test normal
	   	CCWSCheckPromotionArticles.checkPromotionArticles('54321'); 
	    
	    // Create a second Testpromotion and Test more than one promotion found
	    Promotion__c oTestPromotion2		= new Promotion__c();
	    oTestPromotion2.Name				= 'Testpromotion';
	    oTestPromotion2.PromotionID__c		= '54321';
	    insert oTestPromotion2;
	    
	    // Test two Promotions found
	    CCWSCheckPromotionArticles.checkPromotionArticles('54321'); 
	    
	    // Test no Promotion found
	    CCWSCheckPromotionArticles.checkPromotionArticles('54321test'); 
        
    }
}
