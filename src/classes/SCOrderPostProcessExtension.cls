/*
 * @(#)SCOrderPostProcessExtension.cls    
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.   
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/** 
 * This is an extension for the SCOrder__c controller and is used
 * by the SCOrderPostProcess.page. It's purpose is to enable users
 * to input the timerecords of an assignment without using a mobile 
 * device. 
 * 
 * <h2> Underlying Scenario: </h2>
 * <p> 
 * The technician visited the customer and performed repairs, but for
 * some reason was unable to input his timerecord data into his mobile
 * device (broken device, device offline, not enough devices, etc.pp).
 * This extension enables a technician/dispatcher to manually input 
 * timerecord data without having the technician ever scheduled by the 
 * ASE. This is done by creating the SCAssignment and directly setting
 * it's status to (TODO: wicht status?). From the entered data, this
 * controller creates the correct timereports and attaches them to the 
 * corresponding SCOrder.
 * </p>
 *
 * PMS #34819 (Order Postprocessing without Mobile Client)
 *
 * @author Marcus Autenrieth <mautenrieth@gms-online.de>
 */
public class SCOrderPostProcessExtension 
{
    // Error Codes for easier fuzz/negative-testing  
    public enum ErrorCode {
        SUCCESS,
        ORDER_NULL,
        ORDER_TYPE_WRONG,
        ORDER_STATUS_WRONG,
        ENGINEER_NULL,
        TIMEREPORT_END_NULL,
        TIMEREPORT_END_TOO_FAR_IN_PAST,
        TIMEREPORT_END_IN_FUTURE,
        TIMEREPORT_LABOUR_DURATION_NULL,
        TIMEREPORT_LABOUR_DURATION_NOT_IN_RANGE,
        TIMEREPORT_TRAVEL_DURATION_NOT_IN_RANGE,
        TIMEREPROT_TRAVEL_DISTANCE_NULL,
        TIMEREPORT_TRAVEL_DISTANCE_NOT_IN_RANGE,
        TIMEREPORT_TRAVEL_DURATION_NULL,
        FAIL
    }
    
    // The StandardController which this class is extending (Order)
    private ApexPages.Standardcontroller orderController = null;
    
    // The Id of the Order we are postprocessing
    
    public SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance(); 
    
    // The new Assignment of the engineer visit we are going to generate.
    public SCOrder__c ord {get; set;}
    public SCAssignment__c assignment {get; set;}
    public SCAppointment__c appointment {get; set;}

    // List of Technicians to select from 
    public List<SCResource__c> engineersList { get; set; }
    public String engineerSearchString { get; set; }
    
    // Stores the ID of the SCResource
    public String selectedEmployee { get; set; }

    public Boolean canEnterData { get; set; }
    
    // Only for testing and debuggin purposes
    public ErrorCode errno = ErrorCode.FAIL; // There's no such thing as guaranteed success 
    
    public SCOrderPostProcessExtension(ApexPages.StandardController controller) 
    {
        orderController = controller;
        
        // select some order data that is required for filling in the data to write 
        Id orderId = controller.getRecord().Id;
        
        ord = [select id, name, Type__c, Status__c, CustomerTimewindow__c, CustomerPrefStart__c, CustomerPrefEnd__c,
               (select id, installedbase__r.ProductNameCalc__c, installedbase__r.serialno__c from OrderItem__r where RecordType.DeveloperName = 'Equipment' limit 1) 
               from SCOrder__c where id = :orderId];
        
        assignment = new SCAssignment__c();
        appointment = new SCAppointment__c();
        
        // Populated by getEngineers
        engineersList = new List<SCResource__c>();
        
        selectedEmployee = null;
        
        // do a pre-validation
        onValidate();
        errno = ErrorCode.FAIL; // Reset error code to unspecific.
    }
    
    /**
     * Saves the data of the assignment the technician has worked on:
     *   - the new SCAssignment
     *   - the two SCTimeReports generated from it
     *
     * Then redirects back to the Order Overview
     */
    public Pagereference onSave() 
    {   
        // check the input data
        if(onValidate())
        {
            saveData();

            // If everything is fine, return back to OrderOverview
            return orderController.view();
        }
        return null;
    }
    

    /**
     * Before saving the data this method validates the entered data:
     * 1. The visit date must have been entered
     * 2. The order duration must have been entered  
     * The error message is added if required
     *
     * @param  assigment... time report fields are evaluated
     *
     * @return true if all required data exists
     */
    public Boolean onValidate() 
    {   
        canEnterData = false;
    
        ApexPages.getMessages().clear();
        String msg = null;
        
        if(ord == null)
        {
            msg = 'Der Auftrag kann nicht nacherfasst werden.';
            errno = ErrorCode.ORDER_NULL;
        } 
        else if(ord.Type__c == SCfwConstants.DOMVAL_ORDERTYPE_INHOUSE)
        {
            msg = 'Diese Auftragsart kann nicht nacherfasst werden.';
            errno = ErrorCode.ORDER_TYPE_WRONG;
        }
        else if(ord.Status__c == SCfwConstants.DOMVAL_ORDERSTATUS_CANCELLED ||
                ord.Status__c == SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL)
        {
            msg = 'Der Auftrag kann in diesem Status nicht mehr nacherfasst werden.';
            errno = ErrorCode.ORDER_STATUS_WRONG;
        }
        else if(ord.Status__c != SCfwConstants.DOMVAL_ORDERSTATUS_OPEN 
                && ord.Status__c != SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED)
        {
            msg = 'Nur neu angelegte Aufträge können zur Nacherfassung genutzt werden.';
            errno = ErrorCode.ORDER_STATUS_WRONG;
        }
        //else if(selectedEmployee == null) GMSSU 18.11.2013 PMS 36423
        else if(String.isBlank(selectedEmployee))
        {
            msg = 'Bitte wählen Sie den Techniker, für den nacherfasst werden soll.';
            canEnterData = true;
            errno = ErrorCode.ENGINEER_NULL;
        } 
        else if(assignment.TimereportEnd__c == null)
        {
            msg = 'Bitte geben Sie den Auftragsabschlusszeitpunkt an.';
            canEnterData = true;
            errno = ErrorCode.TIMEREPORT_END_NULL;
        }
        else if(assignment.TimereportEnd__c < DateTime.now().addDays(-180))
        {
            msg = 'Das eingegebene Datum liegt zu weit in der Vergangenzeit.';
            canEnterData = true;
            errno = ErrorCode.TIMEREPORT_END_TOO_FAR_IN_PAST;
        }
        else if(assignment.TimereportEnd__c > DateTime.now())
        {
            msg = 'Das eingegebene Datum muss in der Vergangenheit liegen.';
            canEnterData = true;
            errno = ErrorCode.TIMEREPORT_END_IN_FUTURE;
        }
        else if(assignment.TimereportLabourDuration__c == null)
        {
            msg = 'Bitte geben Sie die Arbeitszeit in Minuten an.';
            canEnterData = true;
            errno = ErrorCode.TIMEREPORT_LABOUR_DURATION_NULL;
        }
        else if(assignment.TimereportLabourDuration__c < 5 || assignment.TimereportLabourDuration__c > 60 * 10)
        {
            msg = 'Bitte geben Sie eine Arbeitszeit > 5 Minuten und < 600 Minuten (10 Stunden ) an.';
            canEnterData = true;
            errno = ErrorCode.TIMEREPORT_LABOUR_DURATION_NOT_IN_RANGE;
        }
        else if(assignment.TimereportTravelDuration__c != null)
        {
            canEnterData = true;
            if(assignment.TimereportTravelDuration__c < 5 || assignment.TimereportTravelDuration__c > 60 * 10)
            {
                msg = 'Bitte geben Sie eine Fahrtzeit > 5 Minuten und < 600 Minuten (10 Stunden ) an.';
                errno = ErrorCode.TIMEREPORT_TRAVEL_DURATION_NOT_IN_RANGE;
            }
            else if(assignment.TimereportTravelDistance__c == null)
            {
                msg = 'Bitte geben Sie eine Fahrtstrecke in km an.';
                errno = ErrorCode.TIMEREPROT_TRAVEL_DISTANCE_NULL;
            }
            else if(assignment.TimereportTravelDistance__c < 1 || assignment.TimereportTravelDistance__c > 1000)
            {
                msg = 'Bitte geben Sie eine Fahrtstrecke > 1 km und < 1000 km an.';
                errno = ErrorCode.TIMEREPORT_TRAVEL_DISTANCE_NOT_IN_RANGE;
            }
        }
        else if (assignment.TimereportTravelDuration__c == null)
        {
            canEnterData = true;
            msg = 'Bitte geben Sie eine Fahrtzeit in Minuten an.';
            errno = ErrorCode.TIMEREPORT_TRAVEL_DURATION_NULL;
        }
        // all validation rules have been passed         
        if(msg != null)
        {    
            ApexPages.Message m = new ApexPages.Message(ApexPages.Severity.INFO, msg);
            ApexPages.addMessage(m);
            return false;
        }
        errno = ErrorCode.SUCCESS; 
        return true;
    }
    
    
   /*
    * After the validation transfer the data into the 
    * - assignment
    * - appointment 
    * - time report
    * 
    */
    private void saveData()
    {
        String orderitemid;
        if(ord.OrderItem__r != null && ord.OrderItem__r.size() > 0)
        {
          orderitemid = ord.OrderItem__r[0].id;
        }
    
        //---<1. appointment>--------------------------------------------------
        // Fill the appointment details
        appointment.Order__c      = ord.Id;
        appointment.OrderItem__c  = orderitemid; 
        appointment.Resource__c   = selectedEmployee;
        // appointment.Assignment__c = assignment.id; Let the trigger do this for us
        appointment.AssignmentStatus__c = SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED;
    
        // the actual appointment start / end is determined from the time report 
        // (overlapping with existing appointments are ignored)
        Integer traveldura = 0;
        Integer traveldistance = 0;
        if(assignment.TimereportTravelDuration__c != null && assignment.TimereportTravelDistance__c != null)
        {
            traveldura = assignment.TimereportTravelDuration__c.intvalue();
            traveldistance = assignment.TimereportTravelDistance__c.intvalue();
        }
        
        Integer labourdura = 0;
        if(assignment.TimereportLabourDuration__c != null)
        {
            labourdura = assignment.TimereportLabourDuration__c.intvalue();
        }
        
        appointment.Start__c      = assignment.TimereportEnd__c.addMinutes(-labourdura);
        appointment.End__c        = assignment.TimereportEnd__c;
        
        // the timewindow is narrowed to the actual start / end (and can be ignored)
        appointment.CustomerTimewindowStart__c = appointment.Start__c;
        appointment.CustomerTimewindowEnd__c   = appointment.End__c;

        // The due date time window is copied from the order (as when scheduling automatically)
        appointment.CustomerPrefStart__c       = ord.CustomerPrefStart__c; 
        appointment.CustomerPrefEnd__c         = ord.CustomerPrefEnd__c;
        appointment.CustomerTimewindow__c      = ord.CustomerTimewindow__c;

        // set some marker so that we can see that this is not an ASE appointment        
        appointment.PlanningType__c      = SCfwConstants.DOMVAL_APPOINTMENTPLANNINGTYPE_GENERATED;
        appointment.Description__c       = 'aus Nacherfassung angelegt'; // TODO translate 
        appointment.PreviousLocation__c  = null;

        // set standard fields
        appointment.Class__c = SCfwConstants.DOMVAL_APPOINTMENTCLASS_ORDER;            
        appointment.Type__c  = SCfwConstants.DOMVAL_APPOINTMENTTYP_JOB;
        appointment.FixedResource__c = true;
        appointment.Fixed__c = true;
        appointment.Completed__c = true;

        // the planned driving time is not available 
        appointment.Distance__c = 0;
        appointment.DrivingTime__c = 0;

        // we have no coordinates here
        appointment.GeoY__c = 0;
        appointment.GeoX__c = 0;

        insert appointment;
        
        // !!! TRIGGER-WARNING !!!
        // By now, an-insert-trigger should have generated an SCAssignment__c for us, 
        // whose ID should be accessible via appointment.Assignment__c
        
        // ReFetch the appointment to retrieve the Id of the generated Assignment
        System.assertNotEquals(null, appointment.Id);
        List<SCAppointment__c> apps = [SELECT Id, Assignment__c, End__c
                                       FROM SCAppointment__c
                                       WHERE Id = :appointment.Id];
        System.assertEquals(1, apps.size());
        appointment = apps.get(0);
                                       
        // Now fetch the generated assignment (there should be exactly one)    
        List<SCAssignment__c> generatedAssignments = [SELECT Id, Order__c, Resource__c, Status__c, Followup1__c, Followup2__c,
                                                              TimereportLabourDuration__c, TimereportTravelDuration__c, 
                                                              TimereportTravelDistance__c 
                                                       FROM SCAssignment__c
                                                       WHERE Order__c = :ord.id 
                                                         // Find the assignment the insert-trigger of Appointment created for us
                                                         AND Id = :appointment.Assignment__c];
        System.assertEquals(1, generatedAssignments.size());
                                 
        //---<2. assignment>---------------------------------------------------
         
        // Fill in the generated assignment
        SCAssignment__c genAssignment = generatedAssignments.get(0);
            
        // Copy the Timereport-Data from the old assignment
        genAssignment.TimereportEnd__c = assignment.TimereportEnd__c;
        genAssignment.TimereportLabourDuration__c = assignment.TimereportLabourDuration__c;
        genAssignment.TimereportTravelDistance__c = assignment.TimereportTravelDistance__c;
        genAssignment.TimereportTravelDuration__c = assignment.TimereportTravelDuration__c;
            
        // The trigger should have prefilled this for us.
        // genAssignment.Order__c     = ord.Id;    
        
        // Fill the mandatory assignment fields
        genAssignment.Resource__c  = selectedEmployee;
        genAssignment.Status__c    = SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED;
        genAssignment.Followup1__c = SCfwConstants.DOMVAL_FOLLOWUP1_NONE;
        genAssignment.Followup2__c = SCfwConstants.DOMVAL_FOLLOWUP2_NONE;
        
        // Don't insert the assignment, only update the generated one.
        update genAssignment;
        
        // Copy for testing purposes
        assignment = genAssignment;
        
       
        //---<3. timereport>--------------------------------------------------
        
        // Generate TimeReports
        List<SCTimeReport__c> travelAndWorkReport = SCboTimeReport.generateTimeReportsLight(genAssignment);

        upsert travelAndWorkReport;

        //---<4. order Log>--------------------------------------------------
        SCOrderLog__c log = new SCOrderLog__c();
        
        log.SCOrderId__c      = ord.Id;
        log.CreatedDate__c    = DateTime.now(); 
        log.UserName__c       = UserInfo.getName();
        log.Type__c           = '11719'; // post processed
        log.Info__c           = 'Auftrag nacherfasst ';
        
        String engname = '';
        
        for(SCResource__c eng : engineersList)
        {
            if(selectedEmployee == eng.id)
            {
                engname = eng.LastName_txt__c + ',' + eng.FirstName_txt__c;
                log.Info__c += 'für Techniker ' + engname;
                break;
            }
        }
        
        log.Info__c += ' ausgeführt am: ' + appointment.End__c.format('dd.MM.yyyy HH:mm')  + ' Arbeitszeit:' + labourdura;
        if(traveldura > 0)
        {
            log.Info__c +=  '  Fahrtzeit:' + labourdura  + '  Entfernung: ' + traveldistance + ' km';
        }

        insert log;
        
        //---<5. update order>--------------------------------------------------
        
        // use a copy to update only the desired fields
        // set the cannel to "post processed"
        SCOrder__c o = new SCOrder__c();
        o.id = ord.Id;
        o.channel__c = SCfwConstants.DOMVAL_CHANNEL_POSTPROCESSED;
        o.Closed__c = appointment.End__c;
        o.ClosedByInfo__c = engname;
        
        //PMS 36697/INC0112994: Nacherfassung setzt den Auftrag sofort auf "Rechnung freigegeben"
    	// set InvoicingReleased__c to false
   		o.InvoicingReleased__c = false;
        
        update o;
        
    }   

    
    /**
     * Navigates back to the previous view, i.e. the SCOrder__c overview.
     */
    public Pagereference onCancel()
    {
        return this.orderController.view();
    }

    
   /**
    * Read all engineers
    *
    * FIXME: Shamelessly copied from SCContractSearchController.cls! Should be generified soon!
    *
    * @return  list of all engineers
    */ 
    public PageReference getEngineers()
    {
        engineersList = new List<SCResource__c>();
        
        String query =  'Select alias_txt__c, FirstName_txt__c, LastName_txt__c, Employee__c, Mobile_txt__c, DefaultDepartment__r.Name ';
               query += 'From SCResource__c ';
               query += 'Where enablescheduling__c = true ';
        
        if(engineerSearchString != null && engineerSearchString.trim() != '' && engineerSearchString.trim() != '*')
        {   
            String tmpString = engineerSearchString;
            engineerSearchString = String.escapeSingleQuotes(engineerSearchString.trim()) + '%';
            
            query += 'AND (FirstName_txt__c LIKE \'' + engineerSearchString + '\' ';
            query += 'OR LastName_txt__c LIKE \'' + engineerSearchString + '\' ';
            query += 'OR alias_txt__c LIKE \'' + engineerSearchString + '\' ';
            query += 'OR Mobile_txt__c LIKE \'' + engineerSearchString + '\' ) ';
            
            engineerSearchString = tmpString;
        }
        
        query += 'Order By LastName_txt__c asc';
        
        engineersList = Database.query(query);
        
        return null;
    }

}
