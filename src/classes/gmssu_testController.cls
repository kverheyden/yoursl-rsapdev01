public with sharing class gmssu_testController {

    public List<SCOrder__c> orders {get; set; }
    
    public transient Object buttons { get; set; }
    public transient Component.Apex.CommandButton button;
    
    public List<Map<String,String>> buttonsList { get; set; }
    public Object buttonsMapObject { get; set; }
    
    public String selectedIDs { get; set; }
    
    public Account testAcc { get; set; }

    public gmssu_testController()
    {
        testAcc = [Select BillingCountry__c, BillingStreet, PersonBirthdate__c, DefaultPriceList__c, GeoApprox__c
                     From Account
                     Limit 1];
    
        selectedIDs = '';
        
        appointmentTypes = new Map<String, String>();

        // Creating buttons as dynamic components
        // This is not working (not serializable error)
        /*
        button = new Component.Apex.CommandButton();
        button.value = 'My Button 1';
        
        buttons = new List<Object>();
        buttons = button;
        */
        
        // Creating a MAP with buttons as strings
        buttonsList = new List<Map<String,String>>();
        
        // Button 1
        Map<String,String> buttonValues = new Map<String,String>();
        buttonValues.put('value','Button 1');
        buttonValues.put('onclick','alert(\'It works;\'); return false;');
        buttonsList.add(buttonValues);
        
        // Button 2
        buttonValues = new Map<String,String>();
        buttonValues.put('value','Button 2');
        buttonValues.put('onclick','alert(\'It works again;\'); return false;');
        buttonsList.add(buttonValues);
        
        // Button 2
        buttonValues = new Map<String,String>();
        buttonValues.put('value','Save');
        buttonValues.put('onclick','alert(\'It works again;\'); return false;');
        buttonsList.add(buttonValues);
        
        buttonsMapObject = buttonsList; 
        
                        
        orders = [select Id, Name, Type__c, Status__c, DepartmentCurrent__c, CreatedDate From SCOrder__c Order By Status__c Limit 15];
    }
    
    
    //-----New Order/App Infos------------------------------------------
    
    public class AppointmentInfoPerDay
    {
        public Boolean isOrder   { get; set; }
        public SCOrder__c order  { get; set; }
        public String appId      { get; set; }
        
        AppointmentInfoPerDay(Boolean isOrder, Id orderId, String appId)
        {
            this.isOrder = isOrder;
            
            if(isOrder && orderId != null)
            {
                this.order = new SCORder__c(id = orderId);
            }
            
            if(isOrder && orderId == null)
            {
                this.order = new SCORder__c();
            }
            
            if(!isOrder && appId != null)
            {
                this.appId = appId;
            }
        }
    }

    @RemoteAction
    public static List<AppointmentInfoPerDay> getAppInfoPerDay(String resId, Long givenDate)
    {
        if(String.isNotBlank(resId) && givenDate != null)
        {
            Date startDate = DateTime.newInstance(givenDate).date();
            List<AppointmentInfoPerDay> appInfos = new List<AppointmentInfoPerDay>();
            List<SCOrder__c> orders = new List<SCOrder__c>();
            Set<Id> orderIds = new Set<Id>();
            
            for(SCAppointment__c a : [Select Id, Order__c, Type__c
                                      From SCAppointment__c 
                                      Where Resource__c = :resId 
                                      And DAY_ONLY(Start__c) = :startDate
                                      Order By Start__c asc])
            {
                // Order
                if(a.Order__c != null)
                {
                    orderIds.add(a.Order__c);
                    appInfos.add(new AppointmentInfoPerDay(true, a.Order__c, null));
                }
                
                // Appointment
                if(a.Order__c == null)
                {
                    appInfos.add(new AppointmentInfoPerDay(false, null, a.Id));
                }
            } 
            
            if(!orderIds.isEmpty())
            {
                for(SCOrder__c o : [Select Id, Name, Status__c, CustomerTimewindow__c, FailureType__c,
                                             (Select Account__c, Account__r.Name, AddressFull__c, Phone__c, MobilePhone__c
                                              From OrderRole__r
                                              Where OrderRole__c = '50301'
                                              Limit 1),
                                             (Select Id, ProductNameCalc__c, IBSerialNo__c
                                              From OrderItem__r
                                              Order By CreatedDate desc
                                              Limit 1),
                                             (Select Id, Start__c, End__c, Distance__c, Resource__c
                                              From Appointments__r
                                              Where Order__c IN : orderIds
                                              Order by CreatedDate desc
                                              Limit 1)
                                       From SCORder__c
                                       Where Id = : orderIds
                                       Order By CreatedDate asc
                                       Limit 100])
                {
                    for(AppointmentInfoPerDay app: appInfos)
                    {
                        if(app.order != null && app.order.id == o.id)
                        {
                            app.order = o;
                        }
                    }
                }
            }
            
            return appInfos;
        }
        
        return null;
    }    
    
    //---------------------------------------------------------------
    
    
    
    @RemoteAction
    public static List<SCOrder__c> getInfos(String oids)
    {
        if(String.isNotBlank(oids))
        {
            List<String> idsList = oids.split(',');
            
            List<SCOrder__c> orders = [Select Id, Name, Status__c, CustomerTimewindow__c, FailureType__c,
                                             (Select Account__c, Account__r.Name, AddressFull__c, Phone__c, MobilePhone__c
                                              From OrderRole__r
                                              Where OrderRole__c = '50301'
                                              Limit 1),
                                             (Select Id, ProductNameCalc__c, IBSerialNo__c
                                              From OrderItem__r
                                              Order By CreatedDate desc
                                              Limit 1),
                                             (Select Id, Start__c, End__c, Distance__c, Resource__c
                                              From Appointments__r
                                              Where Order__c IN : idsList
                                              Order by CreatedDate desc
                                              Limit 1)
                                       From SCORder__c
                                       Where Id = : idsList
                                       Order By CreatedDate asc
                                       Limit 100];
            return orders;
        }
        
        return null;
    }

    
    /*
    
    Infor für die Info-Bereich der einzelnen Appointments / Resources:
    
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
    Resource:    Name, Type, Adresse, Geokooordianten, Plant, Worktime profile  Id, Name, Type__c, LastName__c, FirstName__c, DefaultDepartment__c
    +Order:       Name, "All Day", "Transferred", "No Failure"                   Id, Name, Status__c, CustomerTimewindow__c, FailureType__c
    +Order Role:                                                                 Account__c, AddressFull__c, Phone__c, MobilePhone__c
    +Order Item:                                                                 Id, ProductNameCalc__c, IBSerialNo__c
    Appointment: Name, Description, Driving time, Distance                      Id, Start__c, End__c, Distance__c
    
    */
    
   /**
    * This function sets a new start/end dates for the given appointment
    * If appointmnt was moved to another group (engineer) - we need to create 
    * a new assignemnt and assign it to the appontment.
    */
    @RemoteAction
    public static Boolean editAppointment(String appId, Long startDateTime, Long endDateTime, Boolean newGroup, String newEngineerId)
    {
        // Appointment was moved and stays in the same group (assigned to the same engineer)

        Datetime dt1 = DateTime.newInstance(startDateTime);
        Datetime dt2 = DateTime.newInstance(endDateTime);

        if(newGroup == null || newGroup == false)
        {
            System.debug('#### startDateTime: ' +  startDateTime);
            System.debug('#### endDateTime: ' +  endDateTime);
            
            System.debug('#### parsed dt1: ' +  dt1);
            System.debug('#### parsed dt2: ' +  dt2);
        
            if(String.isNotBlank(appId) && startDateTime != null && endDateTime != null)
            {
                SCAppointment__c app = [Select Id, Name, Start__c, End__c
                                        From SCAppointment__c
                                        Where Id = :appId];

                app.Start__c = dt1;
                app.End__c   = dt2;
                
                update app;
                
                return true;
            }
        }
        // Appointment was moved to a new froup (new engineer assignemnt should be created)
        else
        {
            // Create a new aggignment with a reference to the given engineer and assign it to the given appointment
            // NICHT VERGERSSEN den alten Assignment aktualisieren: entfernen Appointment/Resource Referenzen
            
            if(String.isNotBlank(appId) && startDateTime != null && endDateTime != null && String.isNotBlank(newEngineerId))
            {
                SCAppointment__c app = [Select Id, Name, Start__c, End__c, Order__c, Resource__c, Assignment__c
                                        From SCAppointment__c
                                        Where Id = :appId];

                // Appointment
                app.Start__c = dt1;
                app.End__c   = dt2;
                app.Resource__c = newEngineerId;
                
                //SCAssignment__c assignmentOld = [Select Id, Name, Order__c, Status__c, Resource__c From SCAssignment__c Where Id = : app.Assignment__c];
                
                /*
                // Assignment (new)
                SCAssignment__c assignment = new SCAssignment__c();
                assignment.Resource__c = newEngineerId;
                assignment.Order__c    = assignmentOld.Order__c;
                assignment.Status__c   = assignmentOld.Status__c;
                insert assignment;
                System.debug('#### inserting');
                */
                
                // Assignment (old)
                /*
                assignmentOld.Resource__c = null;
                assignmentOld.Status__c   = '5507'; // Cancelled
                update assignmentOld;
                */
                
                
                //app.Assignment__c = assignment.id;
                update app;
                
                
                return true;
            }
            
        }
        
        return false;
    }
    
    @RemoteAction
    public static Boolean deleteSimpleAppointment(String appId)
    {
        if(String.isNotBlank(appId))
        {
            Id aId = ID.valueOf(appId);
            Database.delete(aId);
            
            return true;
        }
        return false;
    }
    
    @RemoteAction
    public static Boolean updateAppointment(String appId, Long startDateTime, Long endDateTime, String type, String description)
    {
        if(String.isNotBlank(appId))
        {
            SCAppointment__c app = new SCAppointment__c(Id = Id.valueOf(appId));
            
            if(startDateTime != null)
            {
                Datetime dStart = DateTime.newInstance(startDateTime);
                app.Start__c    = dStart;
            }
            
            if(endDateTime != null)
            {
                Datetime dEnd   = DateTime.newInstance(endDateTime);
                app.End__c      = dEnd;
            }
            
            if(String.isNotBlank(type))
            {
                app.Type__c     = type;
            }
            
            if(String.isNotBlank(description))
            {
                app.Description__c = description;
            }
            
            try
            {
                update app;
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }
        
        return false;
    }
    
    @RemoteAction
    public static SCAppointment__c createSimpleAppointment(Long startDateTime, Long endDateTime, String type, String description, String resId)
    {
        if(startDateTime != null && endDateTime != null && String.isNotBlank(type) && String.isNotBlank(resId))
        {
            Datetime dStart = DateTime.newInstance(startDateTime);
            Datetime dEnd   = DateTime.newInstance(endDateTime);
        
            SCAppointment__c app = new SCAppointment__c();
            
            app.Start__c    = dStart;
            app.End__c      = dEnd;
            app.Type__c     = type;
            app.Resource__c = resId;
            
            if(String.isNotBlank(description))
            {
                app.Description__c = description;
            }
            
            try
            {
                insert app;
                
                app = [Select Id, Name, Type__c, Start__c, End__c, Order__c, Resource__c
                       From SCAppointment__c 
                       Where Id = :app.id];
                
                return app;
            }
            catch(Exception e)
            {
                return null;
            }
            
        }
        
        return null;
    }
    
    @RemoteAction
    public static List<SCResource__c> getResources()
    {
        List<id> rids = new list<id>();
        
        for(AggregateResult ar : [Select Resource__c
                                  From SCAppointment__c 
                                  Where Start__c >= LAST_N_DAYS:15 
                                  And   End__c   <= NEXT_N_DAYS:15
                                  And   Resource__r.EnableScheduling__c = true
                                  Group By Resource__c])
        {
            rids.add((ID)ar.get('Resource__c'));
        }
        
       /*
        * DO NOT USE SUCH LOOPS - THEY ARE CAUSE VERY BIG AMOUNT OF SCRIPT EXECUTION TIME/SIZE (this one is 1.75MB !!!)
        *
        for(SCAppointment__c a : [Select Resource__c, Order__c 
                                  From SCAppointment__c 
                                  Where Start__c >= LAST_MONTH AND End__c <= NEXT_MONTH])
        {
            rids.add(a.Resource__c);
        }
        */
        
        return [Select Id, Name, toLabel(Type__c), LastName__c, FirstName__c, DefaultDepartment__r.Name, 
                      (Select Id, name, ArrivalTimeFrom__c, ArrivalTimeTo__c, Distance__c, DrivingTime__c, End__c, FixedResource__c, 
                              Fixed__c, GeoX__c, GeoY__c, Start__c, Type__c,
                              Order__c, Order__r.Name, Resource__c, Description__c
                       From Appointments__r
                       Where Start__c >= LAST_N_DAYS:15 
                       AND End__c   <= NEXT_N_DAYS:15)
                       //And Order__c != null) 
                From SCResource__c
                Where Id = :rids
                Order By LastName__c asc
                Limit 50];
    }
    
    
    
    public static Map<String, String> appointmentTypes { get; set; }
    
    public Map<String, String> getAppointmentType()
    {
        if(appointmentTypes.isEmpty())
        {
            appointmentTypes = new Map<String, String>();
            
            Schema.DescribeFieldResult fieldResult = SCAppointment__c.Type__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

            for(Schema.PicklistEntry f : ple)
            {
                if(f.isActive())
                {
                    appointmentTypes.put(f.getValue(), f.getLabel());
                }
            }
        }
        
        return appointmentTypes;
    }
    


}
