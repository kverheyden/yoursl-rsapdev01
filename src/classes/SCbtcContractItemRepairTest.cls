/*
 * @(#)SCbtcContractItemRepairTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCbtcContractItemRepairTest
{
    static testMethod void testContractItemValidatePositive()
    {
        SCContractTestHelper cth = new SCContractTestHelper();
        String contractID = cth.prepareForBtcContractVisitCreateTest();
        System.debug('###Test contract: ' + cth.contract);
        System.debug('###ContractId: ' + contractID);
        SCbtcContractVisitCreate.syncCreateContractVisits(contractID);
        
        SCbtcContractItemRepair.repairAll('NL', 1, 'trace');
        Id id = (Id) contractID;
        SCbtcContractItemRepair.repairContract(Id, 'NL', 0, 'trace');
        List<SCContract__c> cList = [Select Name from SCContract__c where Id = :id];
        if(cList.size() > 0)
        {
            String contractName = cList[0].Name;
            SCbtcContractItemRepair.repairContract(contractName, 'NL', 0, 'trace');
        }
        List<SCInterfaceLog__c> interfaceLogList = new List<SCInterfaceLog__c>();
        SCbtcContractItemRepair.logBatchInternal(interfaceLogList, 'CONTRACT_ITEM_REPAIR', 'interfaceHandler',
                null, null, 'resultCode','resultInfo', 'data', Datetime.now(), 1);

        Test.StartTest();
        List<SCContractVisit__c> visitList = [Select Id, Status__c from SCContractVisit__c where Contract__c = :contractID];
        System.debug('###Test visitList: ' + visitList);
        List<String> visitIdList = new List<String>();
        for(SCContractVisit__c visit: visitList)
        {
            visitIdList.add(visit.Id);
            //break;
        } 
        update visitList;

        SCbtcContractOrderCreate.syncCreateOrders(visitIdList);
        List<SCOrder__c> orderList = [Select Id, Status__c from SCOrder__c where Contract__c = : id];
        System.debug('###Test orderList: ' + orderList);
        Integer cnt = 0;
        for(SCOrder__c order: orderList)
        {
            cnt++;
            if(cnt == 1)
            {
                System.debug('###Test order status set from: ' + order.Status__c + ' to : 5506'); 
                order.Status__c = '5506';   
            }
            else
            {
                System.debug('###Test order status set from: ' + order.Status__c + ' to : 5507'); 
                order.Status__c = '5507';   
            }
            order.Closed__c = Date.today().addDays(10);
            
        }
        update orderList;
        // set status to completed
        for(SCContractVisit__c visit: visitList)
        {
            visit.Status__c = 'completed';
            visitIdList.add(visit.Id);
            //break;
        } 
        update visitList;
        List<SCContractItem__c> ciList = [Select id, MaintenanceLastDate__c from SCContractItem__c 
                                         where Contract__c = :id];
        debug('ciList: ' + ciList);       
        for(SCContractItem__c ci: ciList)
        {
            ci.MaintenanceLastDate__c = Date.today();
        }                                  
        update ciList;

        SCbtcContractItemRepair.repairContract(Id, 'NL', 0, 'trace');

        Test.StopTest();
    }

    private static void debug(String msg)
    {
        System.debug('###Test..........' + msg);
    }
}
