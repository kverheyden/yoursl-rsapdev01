/*
 * @(#)SCutilSorterTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCutilSorterTest
{
    private static SCutilSorter us;
    
    private static void testSortingContacts(string fieldName, Boolean isAscending) 
    {
        testSortingContacts(null, fieldName, isAscending);
    }
    
    private static SCutilSorter testSortingContacts(SCutilSorter sorter, string fieldName, Boolean isAscending) 
    {
        // If sorted is null,create it.        
        if (sorter == null) 
        {
            sorter = new SCutilSorter();
            sorter.originalList = [SELECT Name, Phone, Account.Name FROM Contact LIMIT 50];
        }
        
        // Sort list
        List<Contact> sortedList = (List<Contact>) sorter.getSortedList(fieldName, isAscending);        
        
        // Test sort order
        testOrderedList(sortedList, fieldName, isAscending);
        
        return sorter;        
    }
    
    
    private static void testOrderedList(List<sObject> sortedList, string fieldName, Boolean isAscending) 
    {
        us = new SCutilSorter();
        String lastValue = null;
        String currentValue = null;        
        
        for (sObject sObj : sortedList) 
        {
            currentValue = getValue(sObj, fieldName);
            if ((lastValue != null) && (currentValue != null)) 
            {
            
                String strDebug = '';
                strDebug += '\n--------------------------------------------------------------';
                strDebug += '\nSTART';
                strDebug += '\n--------------------------------------------------------------';
                strDebug += '\n[Ascending:'+isAscending+']';
                strDebug += '\n[Previous:'+lastValue+'] [IsNull():'+(lastValue==null)+']';
                strDebug += '\n[Current:'+currentValue+'] [IsNull():'+(currentValue==null)+']';
                strDebug += '\n[CompareTo:'+(currentValue.compareTo(lastValue))+']';
                strDebug += '\n--------------------------------------------------------------';
                strDebug += '\nEND';
                strDebug += '\n--------------------------------------------------------------';
                System.debug(strDebug);
                
                if (isAscending) 
                {
                    System.assertEquals(currentValue.compareTo(lastValue)>=0, true);
                } 
                else 
                {
                    System.assertEquals(currentValue.compareTo(lastValue)<=0, true);
                }
            }
            
            lastValue = currentValue;
        }
    }
    
    private static String getValue(sObject sObj, String fieldName) {
        // This returns the sObject desired in case the fieldName refers to a linked object.
        Integer pieceCount;
        String[] fieldNamePieces;
        
        fieldNamePieces = fieldName.split('\\.');
        pieceCount = fieldNamePieces.size();
        
        for (Integer i = 0; i < (pieceCount-1); i++) 
        {
            sObj = sObj.getSObject(fieldNamePieces[i]);
        }
        if(sobj != null && fieldNamePieces != null)
        {        
            return String.valueOf(sObj.get(fieldNamePieces[pieceCount-1]));
        }
        return '';
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    static testMethod void testSimpleField_AscendingPositiv1() {
        Test.startTest();
        testSortingContacts('Name', true);
        Test.stopTest();
    }
    
    static testMethod void testSimpleField_DescendingPositiv2() {
        Test.startTest();
        testSortingContacts('Name', False);
        Test.stopTest();
    }
    
    static testMethod void testLookupField_AscendingPositiv3() {
        Test.startTest();
        testSortingContacts('Account.Name', True);
        Test.stopTest();
    }
    
    static testMethod void testLookupField_DecendingPositiv4() {
        Test.startTest();
        testSortingContacts('Account.Name', False);
        Test.stopTest();
    }
    
    static testMethod void testMultipleCallsPositiv5() {
        Test.startTest();
        SCutilSorter sorter;
        sorter = testSortingContacts(null, 'Name', true);
        testSortingContacts(sorter, 'Name', False);
        testSortingContacts(sorter, 'Account.Name', True);
        testSortingContacts(sorter, 'Account.Name', False);
        Test.stopTest();
    }
    
    
    static testMethod void testForgotOriginalListPositiv6() 
    {
        Test.startTest();
        Boolean exceptionDetected = false;
        us = new SCutilSorter();
    
        try 
        {
            us.getSortedList('Name', true);
        } 
        catch (NullPointerException e) 
        {
            exceptionDetected = true;
        }
        System.assert(exceptionDetected);
        Test.stopTest();
    }
    
    
    static testMethod void testPassingListPositiv7() 
    {
        Test.startTest();
        us = new SCutilSorter();
        List<Contact> contacts = [SELECT Name, Phone, Account.Name FROM Contact LIMIT 50];
        List<Contact> sortedList = (List<Contact>) us.getSortedList(contacts, 'Name', true);
        testOrderedList(sortedList, 'Name', true);
        Test.stopTest();
    }
    
    
}
