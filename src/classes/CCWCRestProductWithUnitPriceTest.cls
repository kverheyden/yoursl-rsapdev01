/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class CCWCRestProductWithUnitPriceTest {

 	static {
		
		
        
	}
	static testMethod void testDoGet() {
        //create Pricebook
		Pricebook2 standardPB = [Select Id, Name, IsActive From Pricebook2 where IsStandard = true LIMIT 1];
		if (!standardPB.isActive) {
            standardPB.isActive = true;
            update standardPB;
        }
		Pricebook2 pricebook = new Pricebook2();
		pricebook.Name = 'TestPricebook';
		pricebook.DeliveryType__c = 'TestDeliveryType';
		pricebook.Type__c = 'TestType';
		insert pricebook; 
		        
        //create Product
        Product2 product = new Product2();
        product.Name = 'Cola';
        product.ProductCode = 'Cola100';
        product.IsSalesAppRelevant__c = true;
        product.IsShopRelevant__c = true;
        product.Type__c = 'ZEMP';
        insert product;
        List<Product2> prods = [Select ID,IsSalesAppRelevant__c , Type__c from Product2 where ID =: product.ID];
        system.assertEquals(1, prods.size());
        system.assertEquals(true, prods.get(0).IsSalesAppRelevant__c);
        system.assertEquals('ZEMP', prods.get(0).Type__c);
        
        List<Product2> assertProds = [SELECT ID FROM Product2 WHERE IsSalesAppRelevant__c = true ];
        system.assertNotEquals(0, assertProds.size());
        //create PricebookEntries
		PricebookEntry standardPrice = new PricebookEntry(
		Pricebook2Id = standardPB.Id, 
		Product2Id = product.Id, 
		UnitPrice = 0.70, 
		IsActive = true, 
		UseStandardPrice = false);
        insert standardPrice;   
             
        PricebookEntry bookentry = new PricebookEntry();
        bookentry.Pricebook2Id =  pricebook.Id;
        bookentry.Product2Id = product.Id;
        bookentry.UnitPrice = 0.75;
        bookentry.IsActive = true;
        insert bookentry;
        
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/ProductWithUnitPrice/offsetId=1&timestamp=1410561847';
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;
		
		CCWCRestProductWithUnitPrice.doGet();
    }
}
