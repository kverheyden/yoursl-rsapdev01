/*
 * @(#)SCFixAppointmentController.cls SCCloud    hs 18.02.2011
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * @author HS <hschroeder@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCFixAppointmentController 
{
    // the description entered by the technician
    public String description { get; set; }
    // the result message from the webservice
    private String message;
    // a method called after the fix has been done
    public String callback { get; set; }
    // set to true to allow overlapping appointments
    public Boolean allowOverlap { get; set; }

    // the ID of the appointment to change
    public String appointmentID 
    { 
        get;
        
        set
        {
            System.debug('AppointmentID: ' + value);
            appointmentID = value;
            
            // reinit
            appointment = null;
            start = null;
            startTime = null;
            startTimeID = null;
            title = null;
            duration = null;
            allowOverlap = false;
            
            // trace reinit values
            System.debug('Init: start: ' + start + ', startTime: ' + startTime + ', startTimeID: ' + startTimeID +
                ', title: ' + title + ', duration: ' + duration);
        } 
    }
    
    /**
     * Returns true if a message is pending
     * @author HS <hschroeder@gms-online.de>
     */
    public Boolean hasMessage
    {
        get
        {
            return message != null && message.length() > 0;
        }
        set;
    }
    
    /**
     * The appointment that is identified by the appointment ID
     * @author HS <hschroeder@gms-online.de>
     */
    public SCAppointment__c appointment 
    { 
        get
        {
            if (appointment == null && appointmentID != null)
            {
                appointment = [ 
                    SELECT id, Start__c, End__c, CustomerTimewindow__c, ID2__c, Order__r.name,
                         OrderItem__r.Address__c,
                         OrderItem__r.InstalledBase__r.InstalledBaseLocation__r.LocName__c
                    FROM SCAppointment__c 
                    WHERE id = :appointmentID 
                ];
            }
            System.debug('Appointment: ' + appointment);
            return appointment;
        } 
        set;
    }

    /**
     * The current start time of the appointment
     * @author HS <hschroeder@gms-online.de>
     */ 
    public Datetime start
    {
        get
        {
            if (start == null && appointment != null)
            {
                start = appointment.start__c;
                System.debug('Start: ' + start);
            }
            return start;
        }
        set;
    }        

    /**
     * The formatted start time of the appointment
     * @author HS <hschroeder@gms-online.de>
     */ 
    public String startTime 
    {
        get
        {
            if (startTime == null && start != null)
            {
                startTime = start.format();
                System.debug('StartTime: ' + startTime);
            }
            return startTime;
        }
        set;
    }

    /**
     * Returns the title
     * @author HS <hschroeder@gms-online.de>
     */
    public string title
    {
        get 
        {
            if (title == null && appointment != null)
            {
                title = '[' + appointment.OrderItem__r.InstalledBase__r.InstalledBaseLocation__r.LocName__c + ', ' +
                    appointment.OrderItem__r.Address__c + '],[' + appointment.Order__r.name + ']';
            }
            System.debug('Title: ' + title);
            return title;
        }
        set;
    }
    
    /**
     * Contructor, reads the appointment ID when running on a stand-alone page
     * @author HS <hschroeder@gms-online.de>
     */
    public SCFixAppointmentController()
    {
        appointmentID = ApexPages.currentPage().getParameters().get('id');
    }
    
    /**
     * Formats the specified date/time as a time field
     *
     * @param dt the date time value to format
     * @author HS <hschroeder@gms-online.de>
     */
    public String getTime(Datetime dt)
    {
        //Integer off = Integer.valueOf(dt.format('Z').replace('0', '').replace('+', ''));
        if (dt != null)
        {
            return dt.format('HH:mm');
        }
        return '--:--';
    }

    /**
     * Sets the selected start time
     *
     * @param String time value
     */
    public Datetime setTime(String value)
    {
        System.debug('SetTime: ' + value);
        
        if (value == null)
        {
            return null;
        }
        
        String[] elems = value.split(':');
        Datetime now = Datetime.now();
        Datetime newStart = Datetime.newInstance(now.year(), now.month(), now.day(), 
                                Integer.valueOf(elems[0]), Integer.valueOf(elems[1]), 0);   
        return newStart;
    }

        
    /**
     * Calculates the appointment duration as minutes
     * @author HS <hschroeder@gms-online.de>
     */
    public String duration
    {
        get
        {
            if (appointment != null)
            {
                Long diff = (appointment.End__c).getTime() - (appointment.Start__c).getTime();
                duration = diff / 60000 + ' min';
            }
            System.debug('Duration: ' + duration);
            return duration;
        }
        set;
    }
        
    /**
     * Getter/Setter for the selected start time
     * @author HS <hschroeder@gms-online.de>
     */
    public String startTimeID 
    {
        get
        {
            startTimeID = getTime(start);
            System.debug('GetStartTimeID: ' + startTimeID);
            return startTimeID;
        }
        
        set
        {
           startTimeID = value;
           
           if (value != null)
           {
               start = setTime(value);
           }
           System.debug('SetStartTimeID: ' + value);
        }
    }


    /**
     * Returns the startimes as a list of select options
     * @author HS <hschroeder@gms-online.de>
     */
    public List<SelectOption> getStartTimes()
    {
        List<SelectOption> entry = new List<SelectOption>();
        
        if (start == null)
            return entry;

        Integer step = 30;

        boolean addAdditionalEntry = true;

        Datetime rounded = start;
        if (0 < start.minute() && start.minute() < 30)
        {
           rounded = start.addMinutes( -start.minute());
        }
        else if (30 < start.minute() && start.minute() < 60) 
        {
           rounded = start.addMinutes(60 - start.minute());
        }
        else
        {
            addAdditionalEntry = false;
        }
        
        // calculated time range start +/- 4 hours
        Datetime startRange = rounded.addMinutes(-240);
        Datetime endRange   = rounded.addMinutes(+240);
        
        // fixed range from 7:00 to 18:00
        Datetime now = Datetime.now();
        startRange = Datetime.newInstance(rounded.year(), rounded.month(), rounded.day(), 7, 0, 0);
        endRange = Datetime.newInstance(rounded.year(), rounded.month(), rounded.day(), 18, 0, 0);
        
        // Underflow?
        if (!startRange.isSameDay(rounded))
        {
            startRange = DateTime.newInstance(rounded.year(), rounded.month(), rounded.day());
        }
        
        // Overflow?
        if (!endRange.isSameDay(rounded))
        {
            endRange = DateTime.newInstance(rounded.year(), rounded.month(), rounded.day(), 23, 59, 0);
        }
                
        Datetime current = startRange;
        
        while(current <= endRange)
        {
            if(addAdditionalEntry && current > start)
            {
                entry.add(new SelectOption(getTime(start), '* ' + getTime(start)));
                addAdditionalEntry = false;
            }
            String marker = '';
            if(current == start)
            {
               marker  ='* ';
            }   
            
            entry.add(new SelectOption(getTime(current), marker + getTime(current)));
            current = current.addMinutes(step);
        }
        System.debug('Entries: ' + entry);
        return entry;
    }
    
    /**
     * Fixes the appointment by calling the webservice
     * @author HS <hschroeder@gms-online.de>
     */
    public PageReference fixAppointment() 
    {
        String key = 'datstart';
        // delete old message
        message = '';
        
        try
        {
            System.debug('Fixing appoinment (' + appointment.ID2__c + ') to: ' + start);
            AseSetValue.callout(appointment.ID2__c, key, SCBase.formatDBaseDate(start) + ';' + allowOverlap);

            // also update the database
            updateAppointment();
        }
        catch (CalloutException e)
        {
            // handle multiple exceptions
            SCAppointmentException[] ex = SCAppointmentException.getExceptions(e);
            message = ex[0].getMessage();
            System.debug('Error fixing appointment: ' + ex[0]);
            ApexPages.addMessages(ex[0]);
        }
        return null;
    }

    /**
     * Updates appointment in the database
     * @author HS <hschroeder@gms-online.de>
     */
    public void updateAppointment()
    {
        // calc duration
        Long diff = (appointment.End__c).getTime() - (appointment.Start__c).getTime();
        // set new start
        appointment.Start__c = start;
        // set new end to start plus duration
        appointment.End__c = Datetime.newInstance(start.getTime() + diff);
        System.debug('Update appointment: ' + appointment);
        
        // update appointment
        update appointment;
    
        // update order info
        if (description != null)
        {
            SCOrder__c order = [ 
                SELECT id, info__c
                FROM SCOrder__c
                WHERE id = :appointment.Order__r.id
            ];
            order.info__c = description;
            System.debug('Update order: ' + order);
            update order;
        }
        startTime = null;
        allowOverlap = false;
    }
    
    /**
     * Returns the result message from the last callout. 
     * It is empty, if everything went ok
     * @author HS <hschroeder@gms-online.de>
     */
    public String getMessage() 
    {
        System.debug('Message: ' + message);
        return message;
    }
}
