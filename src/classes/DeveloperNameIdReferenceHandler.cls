public with sharing class DeveloperNameIdReferenceHandler {
	public SObjectType soType { get; set; }
	public SObjectField soIdTextField { get; set; }
	public SObjectField soDeveloperNameField { get; set; }


	public DeveloperNameIdReferenceHandler(SObjectType soType, SObjectField soDeveloperNameField, SObjectField soIdTextField) {
		this.soType = soType;
		this.soDeveloperNameField = soDeveloperNameField;
		this.soIdTextField = soIdTextField;
	}


	public void setIdTextByDeveloperName(Map<Id, SObject> oldObjects, Map<id, SObject> newObjects) {
		List<SObject> changedObjects = new List<SObject>();

		for (SObject objectNew : newObjects.values()) {
			SObject objectOld = oldObjects.get(objectNew.Id);
			if (objectOld.get(this.soDeveloperNameField) != objectNew.get(this.soDeveloperNameField))
				changedObjects.add(objectNew);
		}

		setIdTextByDeveloperName(changedObjects);
	}

	private void setIdTextByDeveloperName(List<SObject> sObjects) {
		if (sObjects.isEmpty())
			return;

		Set<String> documentDeveloperNames = getDocumentDeveloperNames(sObjects);
		if (documentDeveloperNames.isEmpty())
			return;

		List<Document> documents = [SELECT Id, DeveloperName FROM Document WHERE DeveloperName IN: documentDeveloperNames];
		if (documents.isEmpty())
			return;

		Map<String, Id> developerNamesWithId = new Map<String, Id>();
		for (Document d : documents)
			developerNamesWithId.put(d.DeveloperName, d.Id);

		for (SObject so : sObjects) {
			Object o = so.get(soDeveloperNameField);
			if (o != null) {
				Id documentId = developerNamesWithId.get((String)o);
				so.put(soIdTextField, documentId);
			}
		}
	}

	private Set<String> getDocumentDeveloperNames(List<SObject> sObjects) {
		Set<String> devNames = new Set<String>();
		for (SObject so : sObjects) {
			
			if (so.getSObjectType() != this.soType)
				continue;

			Object o = so.get(soDeveloperNameField);
			if (o != null)
				devNames.add((String)o);
		}
		return devNames;
	}

	public void setDeveloperNamesByIdText(Map<Id, SObject> oldObjects, Map<Id, SObject> newObjects) {
		List<SObject> changedObjects = new List<SObject>();

		for (SObject objectNew : newObjects.values()) {
			SObject objectOld = oldObjects.get(objectNew.Id);
			if (objectOld.get(this.soDeveloperNameField) == objectNew.get(this.soDeveloperNameField) && 
				objectOld.get(this.soIdTextField) != objectNew.get(this.soIdTextField))
				changedObjects.add(objectNew);
		}

	}

	public void setDeveloperNamesByIdText(List<SObject> sObjects) {
		if (sObjects.isEmpty())
			return;

		Set<Id> documentIds = getDocumentIds(sObjects);
		if (documentIds.isEmpty())
			return;	

		List<Document> documents = [SELECT Id, DeveloperName FROM Document WHERE Id IN: documentIds];

		Map<Id, String> IdWithDeveloperNames = new Map<Id, String>();
		for (Document d : documents)
			IdWithDeveloperNames.put(d.Id, d.DeveloperName);

		for (SObject so : sObjects) {
			Object o = so.get(soIdTextField);
			if (o != null) {
				String devName = IdWithDeveloperNames.get((Id)o);
				so.put(soDeveloperNameField, devName);
			}
		}
	}

	private Set<Id> getDocumentIds(List<SObject> sObjects) {
		Set<Id> ids = new Set<Id>();
		for (SObject so : sObjects) {

			if (so.getSObjectType() != this.soType)
				continue;

			Object o = so.get(soIdTextField);
			if (o != null)
				ids.add((Id)o);
		}
		return ids;
	}

}
