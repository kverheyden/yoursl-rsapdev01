/*
 * @(#)SCMap.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

public with sharing class SCMapData
{
    /*
     * Reads the schedulable resources for the current day of a country.
     */
    public static List<SCResource__c> readResources(String country, Date day)
    {
        List<SCResource__c> result = [SELECT id, name, EMail_txt__c, Employee__c, lastname_txt__c, firstname_txt__c, Phone_txt__c, Type__c FROM SCResource__c where enablescheduling__c = true and isDeleted = false and id in (select resource__c from SCResourceAssignment__c where country__c = :country and enablescheduling__c = true and (validfrom__c <= :day and validto__c >= :day)) order by Employee__r.lastname, name]; 
        return result;
    }
    /*    
    public static Map<Id, List<SCTour>> readTour(List<Id> EmplId, Date day)
    {
        Map<Id, List<SCTour>> tour = new Map<Id, List<SCTour>>();
        
        Datetime t1 =  Datetime.newInstance(day.year(), day.month(), day.day(), 0,0,0); 
        Datetime t2 =  Datetime.newInstance(day.year(), day.month(), day.day(), 23,59,0); 

        List<SCAppointment__c> appointments = new List<SCAppointment__c>();
        appointments = [select Start__c, End__c, Resource__c, AssignmentStatus__c, GeoY__c, GeoX__c 
                       from SCAppointment__c 
                       where (Start__c >= : t1 and Start__c <= : t2)
                       and Resource__c  in : EmplId
                       order by Resource__c,  Start__c];
  
        Id res = null;
        List<SCTour> tours = new List<SCTour>();
        for(SCAppointment__c app : appointments)
        {
            if(res != app.Resource__c)
            {
                tours = new List<SCTour>();
                tour.put(app.Resource__c, tours);           
                res = app.Resource__c;
            }
            SCTour tourItem = new SCTour();
            tourItem.dtStart = app.Start__c;
            tourItem.dtEnd = app.End__c;
            tourItem.day = app.Start__c.date();         
            tourItem.resource = app.Resource__c;
            tourItem.GeoX = app.GeoX__c;
            tourItem.GeoY = app.GeoY__c;
            tourItem.Address = '';
            tourItem.Status = app.AssignmentStatus__c; 
            tours.add(tourItem);
        }
        return tour;
    }
    */
}
