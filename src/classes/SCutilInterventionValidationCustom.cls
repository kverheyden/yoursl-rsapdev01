/*
 * @(#)SCutilInterventionValidationCustom.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCutilInterventionValidationCustom extends SCutilInterventionValidation
{
 
    /**
     * Do the actual validation in a defined order
     *
     * @throws SCutilInterventionValidationException
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public override void validate(SCboOrder boOrder, SCStock__c stock)
    {
        for (SCboOrderItem item : boOrder.boOrderItems)
        {
            validateSerialNumber(item.orderItem.InstalledBase__r);
            validateProduct(item.orderItem.InstalledBase__r);
            validateInstalledBase(item.orderItem.InstalledBase__r);
            validateSafetyStatus(item.orderItem.InstalledBase__r);
//            validateTimes()
        }
        
        validateRepairCodes(boOrder);
        validateOrderLines(boOrder);
        validateMaterialManagement(boOrder, stock);
    }
}
