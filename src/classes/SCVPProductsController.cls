/*
* @(#)SCVPProductsController.cls
* 
* Copyright 2014 by GMS Development GmbH, Karl-Schurz-Strasse 29, 
* DE-33100 Paderborn. All rights reserved.
* 
* Controller for the Vendor Portal Product Page
*
* @history	
* 2014-01-20  GMSxx created
* 
* @review 
* 2014-01-20  GMSxx 
*/

public with sharing class SCVPProductsController extends SCPortal
{
   //Inner Class for the Stock  List
   public class StockHolder
   {
   	   //Main Controller Viable used to call a Action from the Main Controller
   	   private SCVPProductsController x_MainController;
   	   //Hold the Stock sObject
   	   public SCStock__c x_stockItem {get;set;}
   	   
   	   public StockHolder(SCStock__c p_StockItem,SCVPProductsController p_MainController)
   	   {
   	   	 x_stockItem = p_StockItem;
   	   	 x_MainController = p_MainController;
   	   }
   	   //Method Calls the Main controller to show the Equipment List of this Stock
   	   public void showEquipmentForThisStock()
   	   {
   	   		x_MainController.ShowEquipment(x_stockItem);
   	   }
   }
   
   //Inner class for the Equipment List
   public class EquipmentHolder
   {
   		public String x_Lagerort 		{get;set;}
   		public String x_MatNr 	 		{get;set;}
   		public String x_MatKurztext 	{get;set;}
   		public String x_Branding		{get;set;}
   		public String x_Anzahl			{get;set;}
   		public String x_LagerortId		{get;set;}
   		public String x_Preview			{get;set;}
   		
   		//Takes the AggregateResult and sorts it in to variables
   		public EquipmentHolder(AggregateResult p_result)
   		{
   			x_Lagerort = (String)p_result.get('expr3');
   			x_MatNr = (String)p_result.get('expr1');
   			x_MatKurztext = (String)p_result.get('expr0');
   			x_Branding = (String)p_result.get('expr2');
   			x_Anzahl = String.valueOf(p_result.get('expr4'));
   			x_LagerortId = String.valueOf(p_result.get('expr5'));
   			x_Preview = String.valueOf(p_result.get('expr6'));
   		}
   }
  
   //Equipment List for the Pageination
   private List<List<EquipmentHolder>> x_EquipmentList;
   //List of relevant Stock Types for the Page
   private List<String> x_StockTypeList;
   //Count for Equipment Items shown on the Page
   private static Integer PAGE_COUNT = 20;
   //current selected Stock
   private SCStock__c x_currentStock;
   
   public String  sortField 			{ get; set; }
   public String  sortOrder 			{ get; set; }
   public String  sortFieldTmp = '';
   
   //Query Equipment Again
   public Boolean filterQuery { get; set; }
   //List to Show the Equipment on Page Max size  = PAGE_COUNT
   public List<EquipmentHolder> x_EquipmentListforPage {get;set;}
   //List to Show the Stocks on the Page
   public List<StockHolder> x_StockListForPage {get;set;}
   //Page Counter
   public Integer x_PageCounter {get;set;}
   //Page Counter +1 for the visual Input
   public Integer x_PageCounterInput {get;set;}
   
   //Helper variables for the Filter
   public SCInstalledBase__c x_FilterHelper {get;set;}
   public SCStock__c x_FilterHelperVendor {get;set;}
   public String FilterMatNrFilter {get;set;}
   public String MatKurztextFilter {get;set;}
   
   //Variables and Methods for the Pageination
   public void Next()
   {
   		if(gethasNext())
   		{
   			x_PageCounter++;
   			x_PageCounterInput ++;
   			x_EquipmentListforPage = x_EquipmentList.get(x_PageCounter);
   		}
   }
   
   public void Prev()
   {
   		if(gethasPrevious())
   		{
   			x_PageCounter--;
   			x_PageCounterInput --;
   			x_EquipmentListforPage = x_EquipmentList.get(x_PageCounter);
   		}
   }
   public void FirstPage()
   {
   		x_PageCounter = 0;
   		x_PageCounterInput = 1;
   		x_EquipmentListforPage = x_EquipmentList.get(x_PageCounter);
   }
   public void setPage()
   {
   		if(x_PageCounterInput-1 < getIntReturnListSize() && x_PageCounterInput >= 1)
   		{
   			x_PageCounter = x_PageCounterInput-1;
   			x_EquipmentListforPage = x_EquipmentList.get(x_PageCounter);
   		}
   		else
   		{
   			x_PageCounterInput = x_PageCounter+1;
   		}
   }
   public void LastPage()
   {
   		x_PageCounter = getIntReturnListSize() -1;
   		x_PageCounterInput = x_PageCounter+1;
   		x_EquipmentListforPage = x_EquipmentList.get(x_PageCounter);
   }
   public boolean gethasPrevious()
   {
   	  if(x_PageCounter != 0)
   	  {
   	  	 return true;
   	  }
   	  return false;
   }
   public boolean gethasNext()
   {
   	  if(getIntReturnListSize() > x_PageCounter+1)
   	  {
   	  	 return true;
   	  }
   	  return false;
   }
   
   public Integer getIntReturnListSize()
   {
      if(x_EquipmentList != null)
      {
	   	  return x_EquipmentList.size();
      }
      else
      {
      	 return 1;
      }
   }
   //Variables and Methods for the Pageination end
   
   public SCVPProductsController()
   {
   	  x_PageCounter = 0;
   	  if(x_FilterHelper == null)
   	  {
   	  	x_FilterHelper = new SCInstalledBase__c();
   	  }
   	  if(x_FilterHelperVendor == null)
   	  {
		  x_FilterHelperVendor = new SCStock__c();
   	  }
	  if(FilterMatNrFilter == null)
	  {
	  	FilterMatNrFilter = '';
	  }
	  if(MatKurztextFilter == null)
	  {
	  	MatKurztextFilter = '';
	  }
	  filterObjectType = 'SCInstalledBase__c';
	  filterGroupId    = '003';
   	  x_PageCounterInput = 1;
   	  filterQuery = false;
   	  readUser();
   	  readVendor();
   	  FillTypForStock();
   	  readFilters();  	
   	  sortField = 'MAX(ProductModel__r.Name)';
	  sortOrder = 'DESC';
   	  fillStocks();
   } 
   //Fills out relevant Stock Types
   public void FillTypForStock()
   {
   	 x_StockTypeList = new List<String>();
   	 x_StockTypeList.add('EQReadyForRefurbishment');
	 x_StockTypeList.add('EQReadyForScrapping');
	 x_StockTypeList.add('EQReadyToMarket');
   }
   
   //Querys the Stocks, depented if Vendor = True 
   public void fillStocks()
   {
   	  x_StockListForPage = new List<StockHolder>();
   	  if(isVendor == true)
   	  {
   	  	 for(SCStock__c st:[Select   s.Plant__r.Name,s.Info__c,s.Vendor__c, s.Type__c, s.Name__c, s.Name From SCStock__c s where Type__c IN:x_StockTypeList AND Vendor__c = :vendor.Id])
   	  	 {
   	  	 	 StockHolder tmp_stHolderItem = new StockHolder(st,this);
   	  	 	 x_StockListForPage.add(tmp_stHolderItem);
   	  	 }
   	  }
   	  else
   	  {
   	  	 if(x_FilterHelperVendor.Vendor__c != null)
   	  	 {
   	  	 	 for(SCStock__c st:[Select s.Plant__r.Name,s.Info__c,s.Vendor__c, s.Type__c, s.Name__c, s.Name From SCStock__c s where Type__c IN:x_StockTypeList AND Vendor__c =:x_FilterHelperVendor.Vendor__c])
	   	  	 {
	   	  	 	 StockHolder tmp_stHolderItem = new StockHolder(st,this);
	   	  	 	 x_StockListForPage.add(tmp_stHolderItem);
	   	  	 }
	   	  	 if(x_currentStock != null && x_currentStock.Vendor__c != x_FilterHelperVendor.Vendor__c)
	   	  	 {
	   	  	 	x_currentStock = null;
	   	  	 }
   	  	 }
   	  	 else
   	  	 {
	   	  	 for(SCStock__c st:[Select s.Plant__r.Name,s.Info__c,s.Vendor__c, s.Type__c, s.Name__c, s.Name From SCStock__c s where Type__c IN:x_StockTypeList])
	   	  	 {
	   	  	 	 StockHolder tmp_stHolderItem = new StockHolder(st,this);
	   	  	 	 x_StockListForPage.add(tmp_stHolderItem);
	   	  	 }
   	  	 }
   	   }
   }
   //Dos Refresh the Stock and Equipment List
   public void RefreshEquipment()
   {
   	
   		filterUpdate();
   		fillStocks();
   		if(x_currentStock != null)
   		{
   			ShowEquipment(x_currentStock);
   		}
   		else
   		{
   			x_EquipmentList = new List<List<EquipmentHolder>>();
   			x_EquipmentListforPage = new List<EquipmentHolder>();
   		}
   }
   public void switchFilterForProducts()
   {
   		switchFilter();
   }
   
   //Dos Query the Equipments and filters them
   public void ShowEquipment(SCStock__c p_stockItem)
   {	
	   		x_EquipmentList = new List<List<EquipmentHolder>>();
	   		x_currentStock = p_stockItem;
	   		Id StockItemId = p_stockItem.Id;
	   		String tmp_query = 'Select MAX(ProductModel__r.ProductNameCalc__c),MAX(ProductModel__r.Name),MAX(Brand__r.Name),MAX(Stock__r.Name),COUNT(Id),MAX(Stock__c) , MAX(ProductModel__r.Preview__c),ProductModel__c, Brand__c From SCInstalledBase__c where Stock__c = :StockItemId';
	   		
	   		//Filter Options
	   		if(x_FilterHelper.Brand__c != null)
	   		{
	   			Id BrandId = x_FilterHelper.Brand__c;
	   			tmp_query = tmp_query + ' AND Brand__c = :BrandId';
	   		}
	   	    if(FilterMatNrFilter != null && FilterMatNrFilter != '')
	   	    {
	   	    	tmp_query = tmp_query + ' AND ProductModel__r.Name LIKE \'%' + FilterMatNrFilter + '%\'';
	   	    }
	   	    if(MatKurztextFilter != null && MatKurztextFilter != '')
	   	    {
	   	    	tmp_query = tmp_query + ' AND ProductModel__r.ProductNameCalc__c LIKE \'%' + MatKurztextFilter + '%\'';
	   	    }
	   	    
	   		tmp_query = tmp_query + ' GROUP BY  ProductModel__c, Brand__c';
	   		


		    if(sortFieldTmp != sortField)
		    {
		           sortOrder = 'DESC';
		    }
		    else
		    {
			    if(sortOrder == 'DESC')
			    {
			            sortOrder = 'ASC';
			    }
			    else
			    {
			            sortOrder = 'DESC';
			    }
		    }
		    
		    tmp_query += ' Order By ' +  sortField + ' ' +sortOrder;
		    sortFieldTmp = sortField;
		    
	   		System.debug('### Query :' + tmp_query);
	   		
	   		AggregateResult[] tmp_groupedResults = Database.query(tmp_query);
	   		
	   		List<EquipmentHolder> tmp_eqList = new List<EquipmentHolder>();
	   		for (AggregateResult ar : tmp_groupedResults)  
	   		{
	   			System.debug('### AggregateResult :' + ar);
	   			EquipmentHolder tmp_EQHolder = new EquipmentHolder(ar);
	   			tmp_eqList.add(tmp_EQHolder);
	   			if(tmp_eqList.size() >= SCVPProductsController.PAGE_COUNT)
	   			{
	   				x_EquipmentList.add(tmp_eqList);
	   				tmp_eqList = new List<EquipmentHolder>();
	   			}
	   		}
	   		if(!tmp_eqList.isEmpty() && tmp_eqList.size() < SCVPProductsController.PAGE_COUNT)
	   		{
	   			x_EquipmentList.add(tmp_eqList);
	   		}
	   		x_EquipmentListforPage = new List<EquipmentHolder>();
	   		if(!x_EquipmentList.isEmpty())
	   		{
	   			x_EquipmentListforPage = x_EquipmentList.get(0);
	   			System.debug('### x_EquipmentListforPage :' + x_EquipmentListforPage);
	   		}
   }
   
   /*
    * Assign selected filter values to the variables
  	*/
	public override void assignFilter(SCSearchFilter__c filter)
	{
    	if(filter.Filter__c != null)
    	{
   			Map<String,String> mapOfFilters = (Map<String,String>)JSON.deserialize(filter.Filter__c, Map<String,String>.class);
        	 
        	if(mapOfFilters.get('x_FilterHelper') != null)
        	{
        		x_FilterHelper.Brand__c = (Id)mapOfFilters.get('x_FilterHelper');
        	}
        	else
        	{
        		x_FilterHelper.Brand__c = null;
        	}
        	if(mapOfFilters.get('x_FilterHelperVendor') != null)
        	{
        		x_FilterHelperVendor.Vendor__c = (Id)mapOfFilters.get('x_FilterHelperVendor');
        	}
        	else
        	{
        		x_FilterHelperVendor.Vendor__c = null;
        	}			            	
        	if(mapOfFilters.get('FilterMatNrFilter') != null)
        	{
        		FilterMatNrFilter = (String)mapOfFilters.get('FilterMatNrFilter');
        	}
        	else
        	{
        		FilterMatNrFilter = '';
        	}
        	
        	if(mapOfFilters.get('MatKurztextFilter') != null)
        	{
        		MatKurztextFilter = (String)mapOfFilters.get('MatKurztextFilter');
        	}
        	else
        	{
        		MatKurztextFilter = '';
        	}
        	
    	}
    	
    	selectedFilter = filter.Id;
	}
	
   /*
    * Generates a JSON string from all filter fields.
    * This string will be saved to the Filter__c field of the SCSearchFilter_c object
    * @return JSON string
  	*/
	public override String generateJSONFilter()
	{
		Map<String,String> params = new Map<String, String>();
		
		if(x_FilterHelper != null && x_FilterHelper.Brand__c != null) 
		{
			params.put('x_FilterHelper',x_FilterHelper.Brand__c);
		}
		if(x_FilterHelperVendor != null && x_FilterHelperVendor.Vendor__c != null) 
		{
			params.put('x_FilterHelperVendor',x_FilterHelperVendor.Vendor__c);
		}
		if(FilterMatNrFilter != null && FilterMatNrFilter != '')
		{
			params.put('FilterMatNrFilter',FilterMatNrFilter);
		}
		if(MatKurztextFilter != null && MatKurztextFilter != '')
		{
			params.put('MatKurztextFilter',MatKurztextFilter);
		}
		
		return JSON.serialize(params);
	}
}
