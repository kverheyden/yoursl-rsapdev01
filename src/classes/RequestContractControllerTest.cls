/**********************************************************************

Name: fsFA_PofS_ImageComponentController

======================================================

Purpose: 
 
This is the test class for RequestContractController

======================================================

History 

------- 

Date            AUTHOR              DETAIL

09/12/2014      Oliver Preuschl     INITIAL DEVELOPMENT

***********************************************************************/@isTest
private class RequestContractControllerTest {
    
    @isTest static void test_method_one() {
        //Load Accounts
        List< Account > LL_Accounts = loadAccounts();
        insert( LL_Accounts );

        //Load Request
        List< Request__c > LL_Requests = loadRequests();
        insert( LL_Requests );

        //Load RequestAttachment
        List< RequestAttachment__c > LL_RequestAttachments = loadRequestAttachments();
        for( RequestAttachment__c LO_RequestAttachment: LL_RequestAttachments ){
          LO_RequestAttachment.Request__c = LL_Requests.get( 0 ).Id;
        }
        insert( LL_RequestAttachments );

        Attachment LO_Attachment = new Attachment(
                Name            = 'Attachment',
                Body            = Blob.valueOf( 'Body' ),
                parentId        =LL_RequestAttachments.get( 0 ).Id
            );
        insert( LO_Attachment );

        //Load CDEOrders
        List< CDEOrder__c > LL_CDEOrders = loadCDEOrders();
        for( CDEOrder__c LO_CDEOrder: LL_CDEOrders ){
          LO_CDEOrder.Account__c = LL_Accounts.get( 0 ).Id;
        }
        insert( LL_CDEOrders );

        //Load CDEOrderItems
        List< SCProductModel__c > LL_ProductModels = loadProductModels();
        insert( LL_ProductModels );

        //Load CDEOrderItems
        List< CDEOrderItem__c > LL_CDEOrderItems = loadCDEOrderItems();
        for( CDEOrderItem__c LO_CDEOrderItem: LL_CDEOrderItems ){
          LO_CDEOrderItem.CDEOrder__c = LL_CDEOrders.get( 0 ).Id;
          LO_CDEOrderItem.ProductModel__c = LL_ProductModels.get( 0 ).Id;
        }
        insert( LL_CDEOrderItems );
        
        Test.startTest();

        Pagereference LO_Page = Page.SEPAContractForCustomerRequest;
        Test.setCurrentPage( LO_Page );

        ApexPages.currentPage().getParameters().put( 'parentId', LL_RequestAttachments.get( 0 ).Id );
        ApexPages.currentPage().getParameters().put( 'attachmentId', LO_Attachment.Id );

        RequestContractController LO_RequestContractController = new RequestContractController();

        Test.stopTest();

    }

  //Load Account Testdata
  private static List< Account > loadAccounts(){
    List< Account > LL_Accounts = new List< Account >();
    Account LO_Account = new Account(
        Name        =  'TestAccount 1',
        AccountNumber    =  '1'
      );
    LL_Accounts.add( LO_Account );
    return LL_Accounts;
  }

  //Load CDEOrder Testdata
  private static List< CDEOrder__c > loadCDEOrders(){
    List< CDEOrder__c > LL_CDEOrders = new List< CDEOrder__c >();
    CDEOrder__c LO_CDEOrder = new CDEOrder__c();
    LL_CDEOrders.add( LO_CDEOrder );
    return LL_CDEOrders;
  }

  //Load CDEOrderItem Testdata
  private static List< CDEOrderItem__c > loadCDEOrderItems(){
    List< CDEOrderItem__c > LL_CDEOrderItems = new List< CDEOrderItem__c >();
    for( Integer i = 0; i < 10; i++ ){
      CDEOrderItem__c LO_CDEOrderItem = new CDEOrderItem__c(
          Quantity__c        =  10
        );
      LL_CDEOrderItems.add( LO_CDEOrderItem );
    }
    return LL_CDEOrderItems;
  }

  //Load ProductModel Testdata
  private static List< SCProductModel__c > loadProductModels(){
    List< SCProductModel__c > LL_ProductModels = new List< SCProductModel__c >();
    SCProductModel__c LO_ProductModel = new SCProductModel__c(
        Name          =  'Cooler',
        ID2__c          =  'D036900_4712'
      );
    LL_ProductModels.add( LO_ProductModel );
    return LL_ProductModels;
  }

  //Load CDEOrder Testdata
  private static List< Request__c > loadRequests(){
    List< Request__c > LL_Requests = new List< Request__c >();
    Request__c LO_Request = new Request__c();
    LL_Requests.add( LO_Request );
    return LL_Requests;
  }

  //Load CDEOrder Testdata
  private static List< RequestAttachment__c > loadRequestAttachments(){
    List< RequestAttachment__c > LL_RequestAttachments = new List< RequestAttachment__c >();
    RequestAttachment__c LO_RequestAttachment = new RequestAttachment__c();
    LL_RequestAttachments.add( LO_RequestAttachment );
    return LL_RequestAttachments;
  }
    
    
}
