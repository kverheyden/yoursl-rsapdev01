/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public with sharing class cc_cceag_test_hk_Promotion {

	static testMethod void myTest() {
		//Create the account group
		ccrz__E_AccountGroup__c agA = new ccrz__E_AccountGroup__c(Name='A');
		insert agA;
		
		
		ccrz__E_Product__c produ = new ccrz__E_Product__c(Name='TestProduct');
		produ.ccrz__SKU__c = '1234567890';
		produ.NumberOfSalesUnitsPerPallet__c = 10;
		insert produ;
		
		ccrz__E_Category__c cate = new ccrz__E_Category__c();
		cate.Name = 'testCategoryName';
		cate.ccrz__CategoryID__c = 'testCategoryId';
		cate.ccrz__Sequence__c = 500;
		cate.ccrz__EndDate__c = Date.today().addDays(1);
		cate.ccrz__StartDate__c = Date.today().addDays(-1);
		insert cate;
		
		//create the promo
		ccrz__E_Promo__c nonFiltered = new ccrz__E_Promo__c(
            Name = 'testFiltered',
            ccrz__LongDesc__c = 'nonfiltered',
            ccrz__ShortDesc__c = 'nonfiltered',
            ccrz__PageLocation__c = 'Landing Page',
            ccrz__LocationType__c = 'Left Nav',
            ccrz__StorefrontMS__c='DefaultStore',
            ccrz__Enabled__c = true,
            ccrz__StartDate__c = Date.today().addDays(-1),
            ccrz__EndDate__c = Date.today().addDays(1),
            ccrz__Sequence__c = 1
        );
		nonFiltered.ccrz__Product__c = produ.Id;
		nonFiltered.ccrz__ExternalLink__c = 'noLink';
		nonFiltered.ccrz__ImageURI__c = 'none';
		nonFiltered.ccrz__ImageSource__c = 'none';
		nonFiltered.ccrz__NewWindow__c = false;
		nonFiltered.ccrz__Category__c = cate.Id;
		nonFiltered.ccrz__product__r = produ;
		insert nonFiltered;
       	
       	E_PromotionProduct__c promoProd = new E_PromotionProduct__c();
		promoProd.Product__c = produ.Id;
		promoProd.Promotion__c = nonFiltered.Id;
		insert promoProd;
        
        
        
        //create an account with the account group
        Account acc = new Account();
     	acc.Name = 'daoTestAccount';
     	acc.AccountNumber = '123456';
     	acc.Subtradechannel__c = '0';
     	acc.ccrz__E_AccountGroup__c = agA.Id;
     	insert acc;
     	
     	//put the promo in a filter
        ccrz__E_PromotionAccountGroupFilter__c groupFilter =  new ccrz__E_PromotionAccountGroupFilter__c(ccrz__CC_Promotion__c = nonFiltered.Id,
        	ccrz__CC_AccountGroup__c = agA.Id,ccrz__FilterType__c = 'Inclusive', CCAccount__c=acc.id);
        insert groupFilter;
        
        //create the billTo and shipTo contacts including the partnerId for the account
        ccrz__E_ContactAddr__c billTo = new ccrz__E_ContactAddr__c(
			ccrz__AddressFirstline__c='100 Pine Street', ccrz__City__c='SmallTown', ccrz__DaytimePhone__c='(847) 555-1212',
			ccrz__FirstName__c='Test', ccrz__HomePhone__c='(847) 555-1212', ccrz__LastName__c='User',
			ccrz__PostalCode__c='60601', ccrz__State__c='Idaho', ccrz__StateISOCode__c='ID',
			ccrz__CountryISOCode__c='USA');
		ccrz__E_ContactAddr__c shipTo = new ccrz__E_ContactAddr__c(
			ccrz__AddressFirstline__c='100 Pine Street', ccrz__City__c='SmallTown', ccrz__DaytimePhone__c='(847) 555-1212',
			ccrz__FirstName__c='Craig', ccrz__HomePhone__c='(847) 555-1212', ccrz__LastName__c='Traxler',
			ccrz__PostalCode__c='60601', ccrz__State__c='Idaho', ccrz__StateISOCode__c='ID',
			ccrz__CountryISOCode__c='USA', ccrz__Partner_Id__c=acc.AccountNumber);
		insert new List<ccrz__E_ContactAddr__c> {billTo, shipTo};
		String testName = 'testcart';
		String testEmail = 'myemail@test.com';
		String testFirstName = 'Test';
		String testLastName = 'User';
		String testCompanyName = 'MyCo';
		String testPhone = '(800) 555-1111';
		String testMobilePhone = '(800) 555-2222';
		String testPaymentMethod = 'PO';
		String testPONumber = '12345';
		
		//create the cart using those contacts
		ccrz__E_Cart__c cart = new ccrz__E_Cart__c(
			ccrz__Name__c = testName,
			ccrz__BillTo__c = billTo.Id,
			ccrz__ShipTo__c = shipTo.Id,
			ccrz__BuyerEmail__c = testEmail,
			ccrz__BuyerFirstName__c = testFirstName,
			ccrz__BuyerLastName__c = testLastName,
			ccrz__BuyerCompanyName__c = testCompanyName,
			ccrz__BuyerPhone__c = testPhone,
			ccrz__BuyerMobilePhone__c = testMobilePhone,
			ccrz__PaymentMethod__c = testPaymentMethod,
			ccrz__PONumber__c = testPONumber,
			ccrz__SessionID__c = 'test session',
			ccrz__storefront__c='DefaultStore',
			ccrz__RequestDate__c = Date.today(),
			ccrz__ActiveCart__c = true,
			ccrz__CartStatus__c = 'Open',
			ccrz__CartType__c = 'Cart'
		);
		
		insert cart;
        
        ccrz__E_Promo__c promoId = [SELECT Id, Name, ccrz__ProductRelated__c,ccrz__NewWindow__c,ccrz__Category__c, ccrz__ShortDesc__c, ccrz__PageLocation__c, ccrz__LocationType__c, ccrz__Sequence__c, ccrz__ExternalLink__c,ccrz__ImageURI__c,ccrz__ImageSource__c,ccrz__product__r.ccrz__sku__c FROM ccrz__E_Promo__c WHERE Name='testFiltered'];
        
        ccrz.cc_bean_promoRD beanPromoRd = new ccrz.cc_bean_promoRD(promoId);
        cc_cceag_hk_Promotion.cc_cceag_bean_promo promotionBean = new cc_cceag_hk_Promotion.cc_cceag_bean_promo(beanPromoRd);
        system.assertEquals(beanPromoRd.sku, promotionBean.sku);
        /*
        beanPromoRd.sfid = promoId.Id;
        beanPromoRd.name = promoId.Name;
		beanPromoRd.shortDecription = promoId.ccrz__ShortDesc__c;
		beanPromoRd.pageLocation = promoId.ccrz__PageLocation__c;
		beanPromoRd.locationType = promoId.ccrz__LocationType__c;
		beanPromoRd.sequence = Integer.valueOf(promoId.ccrz__Sequence__c);*/
        
        Map<String,Object> inpData = new Map<String,Object>();
        List<ccrz.cc_bean_promoRD> promos = new List<ccrz.cc_bean_promoRD>();
        promos.add(beanPromoRd);
        Map<String, List<ccrz.cc_bean_promoRD>> promoData = new Map<String, List<ccrz.cc_bean_promoRD>>{
        	'promo1'=>promos
        };
        inpData.put('PROMOS', promoData);
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        cart = [select id, ccrz__EncryptedId__c from ccrz__E_Cart__c where id = :cart.Id];
        ctx.currentCartID = cart.ccrz__EncryptedId__c;
        ccrz.cc_CallContext.initRemoteContext(ctx);
		cc_cceag_hk_Promotion hkPromo = new cc_cceag_hk_Promotion();
		
		Map<String,Object> result = hkPromo.filter(inpData);
		Map<String, List<cc_cceag_hk_Promotion.cc_cceag_bean_promo>> resultPromoData =  (Map<String, List<cc_cceag_hk_Promotion.cc_cceag_bean_promo>>) result.get('PROMOS');
		List<cc_cceag_hk_Promotion.cc_cceag_bean_promo> resultPromos = (List<cc_cceag_hk_Promotion.cc_cceag_bean_promo>) resultPromoData.get('promo1');
		system.assertEquals(1, result.size());
		system.assertEquals('1234567890', resultPromos[0].sku);
	}
}
