/*
* @(#)SCDynamicTableController.cls
* 
* Copyright 2014 by GMS Development GmbH, Karl-Schurz-Strasse 29, 
* DE-33100 Paderborn. All rights reserved.
* 
* This controller creates an dynamic table for the passed data.
* The data could be a list of sObjects.
* 
* @history 
* 2014-09-15 GMSSU created
* 
* @review 
* 
*/
public with sharing class SCDynamicTableController {

    public String               dataType        { get; set; }
    public List<DataContainer>  dataList        { get; set; }
    public List<String>         sObjectFields   { get; set; }
    Set<String>                 fields              = new Set<String>();
    public static final Set<String> standardFields  = new Set<String>{'CreatedDate', 'CurrencyIsoCode', 'attributes', 'Id'};
    public Set<String>          fieldKeys           = new Set<String>();
    public String 				sObjectName		{ get; set; }
    
    public boolean showButtons { get; set; }

   /*  
    * Constructor 
    */ 
    public SCDynamicTableController()
    {   
        allSelected = '';
        sObjectFields = new List<String>();
        showButtons = false;
    }

   /*  
    * Initializing variables 
    */ 
    public void init()
    {
        if(customButtons != null)
        {
            showButtons = true;
        }
    }
    
   /*  
    * Sets visibility of the loading icon for the buttons depending on the attribute 
    */ 
    public Boolean getButtonsPassed()
    {
        return (customButtons != null);
    }

   /*
    * Dynamic components are not serializable. This means we can pass it to the 
    * Visualforce component via attribute but can not display it on the page
    * because the variable should be set to transient. Without that an error
    * message displayed: "Not Serializable: Component.apex.commandbutton"
    * The workaround: pass buttons as list of maps (every map contains keys/values for the button).
    */
    public Object customButtons { get; set; }
    transient Component.Apex.OutputPanel dynamicPanelTransient;
    public Component.Apex.OutputPanel getDynamicPanel()
    {
        if(customButtons != null)
        {
            createDynamicButtons(customButtons);
        }

        return dynamicPanelTransient;
    }
  
   /*  
    * Creates buttons for the dynamic component depending on ther passed map. 
    */ 
    public void createDynamicButtons(Object buttonsList)
    {        
        dynamicPanelTransient = new Component.Apex.OutputPanel();

        for(Map<String,String> m : (List<Map<String,String>>)buttonsList)
        {
            Component.Apex.CommandButton button = new Component.Apex.CommandButton();
            
            for(String key : m.keySet())
            {
                if(key == 'value')
                {
                    button.value = m.get(key);
                }
                if(key == 'onclick')
                {
                    button.onclick = m.get(key);
                }
                if(key == 'action')
                {
                    button.expressions.action = m.get(key);
                }
                if(key == 'rerender')
                {
                    button.expressions.rerender = m.get(key);
                }
            }

            dynamicPanelTransient.childComponents.add(button);
        }
    }
    
   /*  
    * Parameter variable. The main data object passed into component. 
    */ 
    public Object dataRaw
    { 
        get
        {
            return dataRaw;
        }
        set
        { 
            dataRaw = value;
            createDataList(dataRaw);
        } 
    }

   /*  
    * Creates a list with all passed objects to be shown on the page 
    */          
    public void createDataList(Object data)
    {
        /*
        Important!!!
        Always do this check, otherwise the list will be initialized every time by every reRender action on the page.
        As a side effect the selected entries could not be selected because isSelect flag is gone by the initialization.
        Additionaly some amount of the view state could be saved that increases a page loading/rerender time.
        */
        if(dataType == null)
        {       	
            dataList = new List<DataContainer>(); 
            String dataType = getDataType(data);
                            
            if(dataType == 'List<sObject>')
            {       
                List<sObject> rawList = (List<sObject>)data;
                
                if(String.isBlank(sObjectName))
                {
	                Schema.SObjectType t = rawList[0].getSObjectType();
	                Schema.DescribeSObjectResult d = t.getDescribe();
	                sObjectName = d.getName();   
                }
                  
                for(sObject s : rawList)
                {
                    Map<String, Object> queriedFieldValues = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(s));
                    
                    /*
                    Need to proceed every entry because some of them can contain fields that other entries are not containing.
                    For example if the query was "Select Id, Name, Article__c ..." and the field Article__c is not empty in the
                    first entry, then this field could be found in the sObject's serialization, otherwise not.
                    We need to know all not-empty-fields from all entries (sObjects) to be able to create a field map for the page.
                    */
                    getObjectFields('', queriedFieldValues);

                    dataList.add(new DataContainer(s, 'sObject'));
                }
                
                Set<String> keysToRemove = new Set<String>();
                
                for(String key : fields)
                {
                    if(key.right(3) == '__c')
                    {
                        for(String key2 : fields)
                        {
                            // FieldReference__r.Name contains FieldReference__ (c was removed) 
                            if(key2 != key && key2.containsIgnoreCase('__r.') && key2.containsIgnoreCase(key.removeEndIgnoreCase('c')))
                            {
                                keysToRemove.add(key);
                            }
                        }
                    }
                }
                
                fields.removeAll(keysToRemove);
                
                sObjectFields.addAll(fields);
            }
        }
    }
    
   /*  
    * Collects fields from every passed sObject 
    */  
    public void getObjectFields(String relatedField, Map<String, Object> queriedFieldValues)
    {       
        queriedFieldValues.remove('attributes');
                
        for(String queriedFieldName : queriedFieldValues.keySet())
        {
            // Check if the queried value represents a related field reference?
            Object queriedFieldValue = queriedFieldValues.get(queriedFieldName);
            
            if(queriedFieldValue instanceof Map<String,Object>)
            {               
                getObjectFields(queriedFieldName + '.', (Map<String, Object>) queriedFieldValue);
            }
            else
            {
                if(!fields.contains(relatedField + queriedFieldName) && !standardFields.contains(queriedFieldName))
                {
                    fields.add(relatedField + queriedFieldName);
                }
            }
        }
    }
    
   /*  
    * Main container class for passed sObjects
    */    
    public class DataContainer
    {
        public sObject sObj             { get; set; }
        public Boolean isSelect         { get; set; }
        public String  val              { get; set; }
               
        public DataContainer(sObject dataRaw, String entryType)
        {           
            this.sObj     = dataRaw;
            isSelect      = false;
            this.val      = dataRaw.Id;
        }
    }
    
    public String allSelected { get; set; }
    
   /*  
    * Recognizes the selected entries and returns a string with IDs
    */
    public void getAllSelectedValues()
    {
        allSelected = '';
    
        for(DataContainer d : dataList)
        {
            if(d.isSelect)
            {
                allSelected += d.sObj.Id + ', ';
            }
        }
    }
    
   /*  
    * Recognizes the passed object type
    */
    public String getDataType(Object dataRaw)
    {
        Type t;
        
        if(dataRaw instanceOf List<String>)
        {
            return 'List<String>';
        }
        else if(dataRaw instanceOf List<Integer>)
        {
            return 'List<Integer>';
        }
        else if(dataRaw instanceOf List<sObject>)
        {
            return 'List<sObject>';
        }  
        else if(dataRaw instanceOf Set<String>)
        {
            return 'Set<String>';
        }
        else if(dataRaw instanceOf Set<Id>)
        {
            return 'Set<Id>';
        }
        else if(dataRaw instanceOf Set<sObject>)
        {
            return 'Set<sObject>';
        }
        else if(dataRaw instanceOf Map<String, String>)
        {
            return 'Map<String, String>';
        }
        else if(dataRaw instanceOf Map<String, sObject>)
        {
            return 'Map<String, sObject>';
        }
        else if(dataRaw instanceOf Map<Id, String>)
        {
            return 'Map<Id, String>';
        }
        else if(dataRaw instanceOf Map<Id, sObject>)
        {
            return 'Map<Id, sObject>';
        }
        else if(dataRaw instanceOf Map<Id, List<sObject>>)
        {
            return 'Map<Id, List<sObject>>';
        }
        else if(dataRaw instanceOf Map<String, List<sObject>>)
        {
            return 'Map<String, List<sObject>>';
        }
        else if(dataRaw instanceOf Map<Id, Set<sObject>>)
        {
            return 'Map<Id, Set<sObject>>';
        }
        else if(dataRaw instanceOf Map<String, Set<sObject>>)
        {
            return 'Map<String, Set<sObject>>';
        }
        else if(dataRaw instanceOf Map<Id, List<String>>)
        {
            return 'Map<Id, List<String>>';
        }
        else if(dataRaw instanceOf Map<String, List<String>>)
        {
            return 'Map<String, List<String>>';
        }
        else if(dataRaw instanceOf Map<Id, Set<String>>)
        {
            return 'Map<Id, Set<String>>';
        }
        else if(dataRaw instanceOf Map<String, Set<String>>)
        {
            return 'Map<String, Set<String>>';
        }
        else if(dataRaw instanceOf ApexPages.StandardSetController)
        {
            return 'ApexPages.StandardSetController';
        }
        
        return 'NOTYPE';
    }

}
