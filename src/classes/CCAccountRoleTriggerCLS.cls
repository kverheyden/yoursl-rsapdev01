/*******************************************************
created 2014-05-15
Update Account Owner based on the CCAccountRole
*******************************************************/
public with sharing class CCAccountRoleTriggerCLS {
	// listUpdateAccount und mapUpdateAccount
	public MAP<string,User> mapValidUsers = new MAP<string,User>();
	public Map<Id,Account> mapUpdateAccount = new Map<Id,Account>();
	public string ID2SUFFIX = '@cc-eag.de';
	
	// tml List of EmployeeDeNumber?s
	public list<string> listUserId2 = new list<string>();
	
	// selecting all necessary Users
	public void SelectUser()
	{
        String AccountOwnerLicenceKeys;
        try
        {
			 AccountOwnerLicenceKeys = CCSettingFlat__c.getValues('AccountOwnerLicenceKeys').Value__c;  
        }
        catch(Exception e)
        {
            String prevMsg = e.getMessage();
            String newMsg = 'Description' + prevMsg;
            system.debug(newMsg);
            e.setMessage(newMsg);
            // throw e;
            return;
        }
        
        if(AccountOwnerLicenceKeys!=null && AccountOwnerLicenceKeys.length()>0){
			 List<String> validLicenses = AccountOwnerLicenceKeys.split(',');
			
			String tmp_user;
			for(User tU: [SELECT Id, Name, IsActive, ID2__c FROM User WHERE IsActive=true AND ID2__c IN:listUserId2 AND Profile.UserLicense.LicenseDefinitionKey IN:validLicenses])
			{
				if(tU.ID2__c != NULL)
				{
					tmp_user = tU.ID2__c.toLowerCase();
					mapValidUsers.put(tmp_user.replace('@cc-eag.de',''),tU);
				}
			}
		 }
	}
	
	// prepare the Account Update based on the CCAccountRole
	public void prepareAccountUpdate(list<CCAccountRole__c> listAR)
	{
		
		// loop all AR?s
		for(CCAccountRole__c AR:listAR)
		{
			
			// check if it is a valid AccountRole == ?ZR? && MasterAccount__c != null // required for Query!!!!!!!!!!!!!
			if(AR.AccountRole__c =='ZR' && AR.MasterAccount__c != NULL)
			{
				listUserId2.add(AR.EmployeeDeNumber__c + '@cc-eag.de');
			}
		}
		
		// if the list is not empty select the Users Info?s
		if(listUserId2.size()>0)
		{
			SelectUser();
		}
		
		
		// prepare Account Update
		for(CCAccountRole__c AR:listAR)
		{
			Account tmpAccount = new Account();
			
			// check if it is a valid AccountRole == ?ZR? && MasterAccount__c != null
			if(AR.AccountRole__c =='ZR' && AR.MasterAccount__c != NULL)
			{
				if(mapValidUsers.get(AR.EmployeeDeNumber__c.toLowerCase())!=NULL && !mapUpdateAccount.containsKey(AR.MasterAccount__c))
				{
					tmpAccount.Id = AR.MasterAccount__c;
					tmpAccount.OwnerId = mapValidUsers.get(AR.EmployeeDeNumber__c.toLowerCase()).Id;
					//AR.EmployeeDeNumber__c
					//mapValidUsers.get();
					// listUpdateAccount.add(tmpAccount);
					mapUpdateAccount.put(tmpAccount.Id,tmpAccount);
				}
			}
		}
		
		// update the Account?s if the list is not empty
		if(mapUpdateAccount.size()>0)
		{
			update mapUpdateAccount.values();
		}
		
		system.debug('### mapValidUsers ' + mapValidUsers);
		system.debug('### listUpdateAccount ' + mapUpdateAccount);
	}

}
