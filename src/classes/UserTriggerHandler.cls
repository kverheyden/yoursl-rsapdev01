public with sharing class UserTriggerHandler {

	public static final String MANAGERAPP_GROUP_NAME = 'ManagerAppUser';
	public static final String MANAGERAPP_USERTYPE = 'Manager';
	public static final String MANAGERAPP_PERMISSIONSET_SPECIFIC = 'ManagerAppSpecificPermissions';

	public static final String SALESAPP_GROUP_NAME = 'SalesAppUser';
	public static final String SALESAPP_USERTYPE = 'Sales Representative';
	public static final String SALESAPP_PERMISSIONSET_SPECIFIC = 'SalesAppSpecificPermissions';

	public static final String SALESAPP_PERMISSIONSET_GENERIC = 'SalesAppUserGenericPermissions';

	public class UserAIHandler extends TriggerHandlerBase {
		public override void mainEntry(TriggerParameters tp) {
			
			Map<Id, User> managerAppUsers = new Map<Id, User>();
			Map<Id, User> salesAppUsers = new Map<Id, User>();

			for(User u : (List<User>)tp.newList) {
				if (u.SalesAppUserType__c == MANAGERAPP_USERTYPE)
					managerAppUsers.put(u.Id, u);
				else if (u.SalesAppUserType__c == SALESAPP_USERTYPE)
					salesAppUsers.put(u.Id, u);
			}

			UserPermissionAssignments.assignUserToPublicGroup(managerAppUsers, MANAGERAPP_GROUP_NAME);
			UserPermissionAssignments.assignUserToPublicGroup(salesAppUsers, SALESAPP_GROUP_NAME);
			Map<Id, User> permissionSetUser = new Map<Id, User>();
			permissionSetUser.putAll(managerAppUsers);
			permissionSetUser.putAll(salesAppUsers);

			UserPermissionAssignments.assignPermissionSet(permissionSetUser, SALESAPP_PERMISSIONSET_GENERIC);
			UserPermissionAssignments.assignPermissionSet(managerAppUsers, MANAGERAPP_PERMISSIONSET_SPECIFIC);
			UserPermissionAssignments.assignPermissionSet(salesAppUsers, SALESAPP_PERMISSIONSET_SPECIFIC);

		}
	}

	public class UserAUHandler extends TriggerHandlerBase {
		public override void mainEntry(TriggerParameters tp) {
			
			Map<Id, User> managerAppUsers = new Map<Id, User>();
			Map<Id, User> salesAppUsers = new Map<Id, User>();

			Map<Id, User> oldManagerAppUsers = new Map<Id, User>();
			Map<Id, User> oldSalesAppUsers = new Map<Id, User>();

			Map<Id, User> salesAppGenericPermissionUsers = new Map<Id, User>();
			Map<Id, User> oldSalesAppGenericPermissionUsers = new Map<Id, User>();

			for (User u : (List<User>)tp.newList) {
				User oldUser = (User)tp.oldMap.get(u.Id);
				
				if (u.SalesAppUserType__c == oldUser.SalesAppUserType__c)
					continue;

				if (u.SalesAppUserType__c != SALESAPP_USERTYPE && u.SalesAppUserType__c != MANAGERAPP_USERTYPE)
					oldSalesAppGenericPermissionUsers.put(u.Id, u);

				if (u.SalesAppUserType__c == SALESAPP_USERTYPE || u.SalesAppUserType__c == MANAGERAPP_USERTYPE)
					salesAppGenericPermissionUsers.put(u.Id, u);

				if (u.SalesAppUserType__c == SALESAPP_USERTYPE)
					salesAppUsers.put(u.Id, u);

				if (u.SalesAppUserType__c == MANAGERAPP_USERTYPE)
					managerAppUsers.put(u.Id, u);

				if (oldUser.SalesAppUserType__c == SALESAPP_USERTYPE)
					oldSalesAppUsers.put(u.Id, u);
 
				if (oldUser.SalesAppUserType__c == MANAGERAPP_USERTYPE)
					oldManagerAppUsers.put(u.Id, u);
				 
			}
			UserPermissionAssignments.removeUserFromPublicGroup(oldSalesAppUsers, SALESAPP_GROUP_NAME);
			UserPermissionAssignments.removeUserFromPublicGroup(oldManagerAppUsers, MANAGERAPP_GROUP_NAME);

			UserPermissionAssignments.assignUserToPublicGroup(salesAppUsers, SALESAPP_GROUP_NAME);
			UserPermissionAssignments.assignUserToPublicGroup(managerAppUsers, MANAGERAPP_GROUP_NAME);

			UserPermissionAssignments.assignPermissionSet(salesAppGenericPermissionUsers, SALESAPP_PERMISSIONSET_GENERIC);
			UserPermissionAssignments.assignPermissionSet(managerAppUsers, MANAGERAPP_PERMISSIONSET_SPECIFIC);
			UserPermissionAssignments.assignPermissionSet(salesAppUsers, SALESAPP_PERMISSIONSET_SPECIFIC);
			
			UserPermissionAssignments.deassignPermissionSet(oldSalesAppGenericPermissionUsers, SALESAPP_PERMISSIONSET_GENERIC);	
			UserPermissionAssignments.deassignPermissionSet(oldManagerAppUsers, MANAGERAPP_PERMISSIONSET_SPECIFIC);
			UserPermissionAssignments.deassignPermissionSet(oldSalesAppUsers, SALESAPP_PERMISSIONSET_SPECIFIC);
		}
	}
}
