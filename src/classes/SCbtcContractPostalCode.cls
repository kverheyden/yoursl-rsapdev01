/*
 * @(#)SCbtcContractPostalCode.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Batch job to Set the postal code into the contracts
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
global with sharing class SCbtcContractPostalCode extends SCbtcBase 
       implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts
{
    private ID batchprocessid = null;
    String mode = 'productive';
    String country = 'DE';
    String department = null;
    Integer max_cycles = 0;
    List<String> contractIdList = null;    // contractIdList = null, normal batch call

    /**
     * if the contract id is null the batch job for all contract visit dates is started
     * if the contract id is not null the batch job for the given contract is started
     */
    private Id contractId = null;

    // Object for application settings
    private static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();

    // the intern contract object
    public SCContract__c contract { get; private set; }

   /**
     * API entry for starting the batch job for contracts
     * @param country    the country code (e.g. DE, GB)
     * @param max_cycles ###
     * @param mode       (reserved) 
     */
    WebService static ID asyncPostalcodeAll(String country, Integer max_cycles, String mode)
    {
        System.debug('###mode: ' + mode);
        String department = null;
        SCbtcContractPostalCode btc = new SCbtcContractPostalCode(country, department, max_cycles, mode);

        // only one contract in one call to execute
        btc.batchprocessid  = btc.executeBatch();
        return btc.batchprocessid;
    } // asyncPostalcodeAll

    WebService static ID asyncPostalcodeDepartmentAll(String country, String department, Integer max_cycles, String mode)
    {
        System.debug('###mode: ' + mode);
        SCbtcContractPostalCode btc = new SCbtcContractPostalCode(country, department, max_cycles, mode);

        // only one contract in one call to execute
        btc.batchprocessid  = btc.executeBatch();
        return btc.batchprocessid;
    } // asyncPostalcodeAll

    /**
     * Postalcode asynchronically one contract found on the base of contract id.
     * If the contract id is not null the batch job for the given contract is started.
     * @param ###JP
     */
    public static ID asyncPostalcodeContract(Id contractId, String mode)
    {
        String country = 'XX'; // dummy only - will be determined from the contract
        if(contractId != null)
        {
            List<SCContract__c> cList = [Select Country__c from SCContract__c where Id = :contractId];
            if(cList.size() > 0)
            {
                country = cList[0].Country__c;
                Integer max_cycles = 1;
                String department = null;
                SCbtcContractPostalCode btc = new SCbtcContractPostalCode(contractId, department, country, max_cycles, mode);
                // only one order in one call to execute
                btc.batchprocessid = btc.executeBatch(1);
                return btc.batchprocessid;
            }    
        }
        return null;
    } // asyncPostalcodeContract

    /**
     * Postalcodes a contract based on the contract number. 
     * if the contract Name is not null the batch job for the given contract is started
     */
    WebService static ID asyncPostalcodeContract(String contractName, String mode)
    {
        Id contractId = null;
        System.debug('###mode: ' + mode);
        System.debug('###contractName: ' + contractName);
        if(contractName != null)
        {
            List<SCContract__c> contractList = [Select Id from SCContract__c where Name = :contractName];
            if(contractList.size() > 0)
            {
                contractId = contractList[0].Id;
                System.debug('###contractId: ' + contractId);
                String withoutMeaningCountry = 'DE';
                Integer max_cycles = 1;
                String department = null;
                SCbtcContractPostalCode btc = new SCbtcContractPostalCode(contractId, withoutMeaningCountry, department, max_cycles, mode);
                // only one order in one call to execute
                btc.batchprocessid = btc.executeBatch(1);
                return btc.batchprocessid;
            }
        }
        return null;
    } // asyncPostalcodeContract

    /**
     * postalcodes asynchronically on the base of list of contract
     */
    WebService static ID asyncPostalcodeContracts(List<String> contractIdList)
    {
        if(contractIdList == null || contractIdList.size() == 0)
        {
            return null;
        }   
        SCbtcContractPostalCode btc = new SCbtcContractPostalCode(contractIdList, 'trace');
        // only one order in one call to execute
        btc.batchprocessid  = btc.executeBatch(1);
        return btc.batchprocessid;
    } // asyncPostalcodeContract

 
    /**
     * Synchronically calls validation of the contract list
     */ 
    Webservice static void syncPostalcodeContracts(List<String> contractIdList)
    {
        if(contractIdList == null || contractIdList.size() == 0)
        {
            return;
        }   
        System.debug('###......contractIdList: ' + contractIdList);
        Integer max_cycles = 1;
        SCbtcContractPostalCode btc = new SCbtcContractPostalCode(contractIdList, 'test');        
            
        List<SCContract__c> contractList = [select ID,  (Select Contract__r.Id ,
                                                         InstalledBase__r.InstalledBaseLocation__r.PostalCode__c 
                                                         From Service_Contract_Items__r
                                                         where InstalledBase__r.InstalledBaseLocation__r.PostalCode__c <> null limit 1) 
                                                         From SCContract__c
                                            where  
                                            PostalCode__c = null
                                            and Id in :contractIdList];
        
        List<SCInterfaceLog__c> interfaceLogList = new List<SCInterfaceLog__c>();
        for(SCContract__c c: contractList)
        {
            // postalcode a contract
            btc.debug('contract id: ' + c.Id);
            btc.postalcodeContract(c, interfaceLogList);
        }
        upsert contractList;
        insert interfaceLogList;
    }
    
    /**
     * Synchronically calls validation of the contract
     */ 
    Webservice static void syncPostalcodeContract(String contractId)
    {
        if(contractId == null || contractId == '')
        {
            return;
        }   
        System.debug('###......contractId: ' + contractId);
        String withoutMeaningCountry = 'XX'; // dummy - determined from the contract
        Integer max_cycles = 1;
        String department = null;
        SCbtcContractPostalCode btc = new SCbtcContractPostalCode(contractId, withoutMeaningCountry, department, max_cycles, 'test');        
            
        List<SCContract__c> contractList = [select ID,  (Select Contract__r.Id ,
                                                        InstalledBase__r.InstalledBaseLocation__r.PostalCode__c 
                                                        From Service_Contract_Items__r
                                                        where InstalledBase__r.InstalledBaseLocation__r.PostalCode__c <> null limit 1) 
                                                        From SCContract__c
                                            where Id = :contractId];
        
        List<SCInterfaceLog__c> interfaceLogList = new List<SCInterfaceLog__c>();
        for(SCContract__c c: contractList)
        {
            // postalcode a contract
            btc.debug('contract id: ' + c.Id);
            btc.postalcodeContract(c, interfaceLogList);
        }
        upsert contractList;
        insert interfaceLogList;
    }
 




    public SCbtcContractPostalCode(String country, Integer max_cycles, String mode)
    {
        this(null, country, null, max_cycles, mode);
    }

    public SCbtcContractPostalCode(String country, String department, Integer max_cycles, String mode)
    {
        this(null, country, department, max_cycles, mode);
    }

    public SCbtcContractPostalCode(List<String> contractIdList, String mode)
    {
        // in case of this call the country is not regarded
        this(null, 'XX', null, 0, mode);
        this.contractIdList = contractIdList;
    }


    /**
     * Constructor 
     * @param contractId set for working out the specified contract
     * @param country
     * @param max_cycles
     * @param mode
     */
    public SCbtcContractPostalCode(Id contractId, String country, String department, Integer max_cycles, String mode)
    {    
        this.contractId = contractId;
        this.country = country;
        this.department = department;
        this.max_cycles = max_cycles;
        this.mode = mode;
        System.debug('###mode: ' + mode);
        debug('mode: ' + mode);
    }

   /*
    * Called by the framework when the batch job is started. We construct the soql statement
    * for reading the resource assignments.
    * @param BC the batch context
    * @return the query locator with the selected material movements (up to 50 mio records possible)
    */
    global override Database.QueryLocator start(Database.BatchableContext BC)
    {
        debug('start');
        // one order will be created from one SContractVisit__c record 
        /* To check the syntax of SOQL statement */
        List<SCContract__c> contractList = [select ID,  (Select Contract__r.Id ,
                                           InstalledBase__r.InstalledBaseLocation__r.PostalCode__c 
                                           From Service_Contract_Items__r 
                                           where InstalledBase__r.InstalledBaseLocation__r.PostalCode__c <> null limit 1) 
                                           From SCContract__c
                                           where  PostalCode__c = null
                                           AND RecordType.DeveloperName = 'Contract'  limit 1 ];
        debug('start: after checking statement');
        /**/
        String query = null;
        if(contractId == null)
        {
            query = ' select ID, '
                + ' (Select Contract__r.Id , InstalledBase__r.InstalledBaseLocation__r.PostalCode__c '
                        + ' From Service_Contract_Items__r '
                + ' where InstalledBase__r.InstalledBaseLocation__r.PostalCode__c <> null limit 1 )  '
                + ' from SCContract__c v'
                + ' where  '
                + ' Country__c = \'' + country + '\''
                + ' RecordType.DeveloperName = \'Contract\' ';
        if(department != null)
        {        
            query += ' and DepartmentCurrent__c = \'' + department + '\'';
        }    
          //query += ' and PostalCode__c = null and (status__c = \'Active\' or status__c = \'Created\')'
          //    + ' and Template__r.Status__c = \'Active\' '
          query += ' order by StartDate__c, EndDate__c';
                if(max_cycles > 0 )
                {
                    query += ' limit ' + max_cycles;
                }
        }
        else
        {
            // specified contract
            query = ' select ID, '
                + ' (Select Contract__r.Id , InstalledBase__r.InstalledBaseLocation__r.PostalCode__c '
                        + ' From Service_Contract_Items__r '
                + ' where InstalledBase__r.InstalledBaseLocation__r.PostalCode__c <> null limit 1)  '
                + ' from SCContract__c v'
                + ' where  '
               // + ' (status__c = \'Active\' or status__c = \'Created\')'
                + ' and id = \'' + contractId + '\''
                + ' RecordType.DeveloperName = \'Contract\' '
               // + ' and Template__r.Status__c = \'Active\' '
                + ' order by StartDate__c, EndDate__c';
                if(max_cycles > 0 )
                {
                    query += ' limit ' + max_cycles;
                }
        }        
        if(contractIdList != null)
        {
            debug('contractIdList: ' + contractIdList);
            return Database.getQueryLocator([select ID, Status__c, Template__r.Status__c, (Select Contract__r.Id ,
                                             InstalledBase__r.InstalledBaseLocation__r.PostalCode__c 
                                             From Service_Contract_Items__r 
                                             where InstalledBase__r.InstalledBaseLocation__r.PostalCode__c <> null limit 1) 
                                             From SCContract__c
                                            where PostalCode__c = null // And Country__c = :country
                                            AND RecordType.DeveloperName = 'Contract' 
                                            and Id in :contractIdList]);
        }
        debug('query: ' + query);
        return Database.getQueryLocator(query);
    } // start
   /*
    * Called for each batch of records to process.
    * @param BC the batch context
    * @param scope the list records to be processed
    */
    global override void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        debug('execute');

        // loop over the scope
        List<SCInterfaceLog__c> interfaceLogList = new List<SCInterfaceLog__c>();
        for(sObject res: scope)
        {
            debug('sObject: ' + res);
            SCContract__c c = (SCContract__c)res;

            // postalcode contract
            postalcodeContract(c, interfaceLogList);
        }
        upsert scope;
        insert interfaceLogList;
    } // execute

   /*
    * Called by the framework when the batch job has been completed. 
    * We send an e-mail notification about the status
    * @param BC the batch context
    */
    global override void finish(Database.BatchableContext BC)
    {
    } // finish


    public void postalcodeContract(SCContract__c c, List<SCInterfaceLog__c> interfaceLogList)
    {

        DateTime start = Datetime.now();
        Boolean thrownException = false;
        String resultInfo = '';
        Integer count = 0;
        String postalCode = null;
        debug('contract: ' + c);
        try
        {
            if(c.Service_Contract_Items__r != null && c.Service_Contract_Items__r.size() > 0)
            {
                SCInstalledBase__c ib = c.Service_Contract_Items__r[0].InstalledBase__r;
                if(ib != null && ib.InstalledBaseLocation__r != null)
                {
                   postalCode =  ib.InstalledBaseLocation__r.PostalCode__c;
                }
            }
            //c.PostalCode__c = c.Service_Contract_Items__r.InstalledBase__r.InstalledBaseLocation__r.PostalCode__c;
            c.PostalCode__c = postalCode;
            
            count = 1;    
            if(c.PostalCode__c != null)
            {
                resultInfo = 'PostalCode__c = ' + postalCode;
            }
            else
            {
                resultInfo = 'There are no contract items with the postal code set!';
            }

            debug('resultInfo: ' + resultInfo);
        }
        catch(Exception e)
        {
            System.debug('#### ERROR: ' + e + ' | Contract: ' + c.id);
            thrownException = true;
            resultInfo += 'Exception: ' + e.getMessage();
            String prevMode = mode;
            mode = 'test';
            debug('resultInfo:' + resultInfo);
            mode = prevMode;
        }
        finally
        {
            /**
            E000 Success
            E001 Success with Info
            E100 Service not reachable
            E101 Service processing failed
            */
            String resultCode = 'E000';
            if(thrownException)
            {
                resultCode = 'E101';
            }
            // protocoll only the events that something happend
            if(contractId != null || thrownException)
            {
                logBatchInternal(interfaceLogList, 'CONTRACT_POSTALCODE', 'SCbtcContractPostalCode',
                                c.Id, null, resultCode, 
                                resultInfo, null, start, count); 
            }                    
        }
    }

    public static void logBatchInternal(List<SCInterfaceLog__c> interfaceLogList, String interfaceName, String interfaceHandler,
                                ID referenceID, ID referenceID2, String resultCode, 
                                String resultInfo, String data, Datetime start, Integer count)
    {
        SCInterfaceLog__c ic = new SCInterfaceLog__c();
        interfaceLogList.add(ic);
        ic.Interface__c = interfaceName;
        ic.InterfaceHandler__c = interfaceHandler;
        ic.Type__c = 'BATCH';
        ic.Count__c = count;
        ic.ReferenceID__c = referenceID;
        ic.ReferenceID2__c = referenceID2;
        ic.ResultCode__c = '';
        ic.ResultCode__c = resultCode;
        ic.ResultInfo__c = resultInfo;
        ic.Data__c = data;
        ic.Direction__c = 'internal';
        ic.Start__c = start;
        ic.Stop__c = Datetime.now();
        Long duration = ic.Stop__c.getTime() - ic.Start__c.getTime();
        ic.Duration__c = duration; 
        if(interfaceName == 'CONTRACT_VISIT'
            || interfaceName == 'CONTRACT_ORDER'
            || interfaceName == 'CONTRACT_POSTALCODE')
        {
            ic.Contract__c = referenceID;
        }
    }       

    private void debug(String text)
    {
        if(mode.equalsIgnoreCase('test')
            || mode.equalsIgnoreCase('trace'))
        {
            System.debug('###....................' + text);
        }
    }

}
