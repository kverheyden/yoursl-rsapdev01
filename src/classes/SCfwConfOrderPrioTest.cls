/*
 * @(#)SCfwConfOrderPrioTest.cls
 *
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCfwConfOrderPrioTest
{
    /**
     * Test all methodes in the class SCConfOrderPrio
     */
    static testMethod void testConfOrderPrio()
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass2.createConfOrderPrio(true);
        
        SCboOrder boOrder = new SCboOrder();
        boOrder.readById(SCHelperTestClass.order.id);

        Test.startTest();
        
        SCfwConfOrderPrio confOrderPrio = new SCfwConfOrderPrio('XX');
        confOrderPrio.isDebug = true;
        confOrderPrio.generateLists();
        System.assertEquals(3, confOrderPrio.partnerObjList.size());
        System.assertEquals(5, confOrderPrio.situationObjList.size());
        System.assertEquals(5, confOrderPrio.failureTypeObjList.size());
        System.assertEquals(true, confOrderPrio.partnerObjList[0].getIsTitle());
        System.assertEquals(true, confOrderPrio.partnerObjList[1].getIsSubTitle());
        System.assertEquals(true, confOrderPrio.partnerObjList[2].getIsSingleSelect());
        System.assertEquals(true, confOrderPrio.situationObjList[2].getIsSeparator());
        System.assertEquals(true, confOrderPrio.failureTypeObjList[4].getIsMultipleSelect());

        confOrderPrio.partnerObjList[2].optValue = '2';
        confOrderPrio.situationObjList[1].optValue = '3';
        confOrderPrio.calculateScore();

        System.assertEquals('8.0', confOrderPrio.getKoPartner());
        System.assertEquals('6.0', confOrderPrio.getKoSituation());
        System.assertEquals('0', confOrderPrio.getKoFailureType());
        System.assertEquals('8.0', confOrderPrio.getNormalPartner());
        System.assertEquals('6.0', confOrderPrio.getNormalSituation());
        System.assertEquals('0', confOrderPrio.getNormalFailureType());
        System.assertEquals('8.0', confOrderPrio.getTotalPartner());
        System.assertEquals('6.0', confOrderPrio.getTotalSituation());
        System.assertEquals('0', confOrderPrio.getTotalFailureType());
        System.assertEquals('0.00', confOrderPrio.getPrioValue());

        confOrderPrio.setOrderData(boOrder, 7);

        Date today = Date.today();
        System.assertEquals('2101', boOrder.order.CustomerPriority__c);
        // System.assertEquals(today, boOrder.order.CustomerPrefStart__c);
        // System.assertEquals(today.addDays(7), boOrder.order.CustomerPrefEnd__c);
        Test.stopTest();
    }
    
    /**
     * Test all methodes in the class SCConfOrderPrio
     */
    static testMethod void testConfOrderPrioWithPreselection()
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass2.createConfOrderPrio(true);
        
        SCboOrder boOrder = new SCboOrder();
        boOrder.readById(SCHelperTestClass.order.id);
        boOrder.order.FailureType__c = '01';
        boOrder.boOrderItems.get(0).orderItem.ErrorSymptom1__c = '01';
        boOrder.boOrderItems.get(0).orderItem.ErrorCondition1__c = '01';

        Test.startTest();
        
        SCfwConfOrderPrio confOrderPrio = new SCfwConfOrderPrio('XX');
        confOrderPrio.isDebug = true;
        confOrderPrio.generateLists();
        System.assertEquals(3, confOrderPrio.partnerObjList.size());
        System.assertEquals(5, confOrderPrio.situationObjList.size());
        System.assertEquals(5, confOrderPrio.failureTypeObjList.size());
        System.assertEquals(true, confOrderPrio.partnerObjList[0].getIsTitle());
        System.assertEquals(true, confOrderPrio.partnerObjList[1].getIsSubTitle());
        System.assertEquals(true, confOrderPrio.partnerObjList[2].getIsSingleSelect());
        System.assertEquals(true, confOrderPrio.situationObjList[2].getIsSeparator());
        System.assertEquals(true, confOrderPrio.failureTypeObjList[4].getIsMultipleSelect());

        confOrderPrio.checkPreselection(boOrder);
        confOrderPrio.calculateScore();

        System.assertEquals('10', confOrderPrio.getKoPartner());
        System.assertEquals('10', confOrderPrio.getKoSituation());
        System.assertEquals('10', confOrderPrio.getKoFailureType());
        System.assertEquals('10.0', confOrderPrio.getNormalPartner());
        System.assertEquals('7.80', confOrderPrio.getNormalSituation());
        System.assertEquals('10.0', confOrderPrio.getNormalFailureType());
        System.assertEquals('10.0', confOrderPrio.getTotalPartner());
        System.assertEquals('10', confOrderPrio.getTotalSituation());
        System.assertEquals('10.0', confOrderPrio.getTotalFailureType());
        System.assertEquals('1000.00', confOrderPrio.getPrioValue());

        confOrderPrio.setOrderData(boOrder, 7);

        Date today = Date.today();
        System.assertEquals('2103', boOrder.order.CustomerPriority__c);
        // System.assertEquals(today, boOrder.order.CustomerPrefStart__c);
        // System.assertEquals(today.addDays(7), boOrder.order.CustomerPrefEnd__c);
        Test.stopTest();
    }
}
