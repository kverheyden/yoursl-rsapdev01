public with sharing class PofSClickAreaTriggerDispatcher extends TriggerDispatcherBase{
	public override void beforeInsert(TriggerParameters tp) {
		execute(new PofSClickAreaTriggerHandler.PofSClickAreaBIHandler(), tp, TriggerParameters.TriggerEvent.beforeInsert);
	}

	public override void beforeUpdate(TriggerParameters tp) {
		execute(new PofSClickAreaTriggerHandler.PofSClickAreaBUHandler(), tp, TriggerParameters.TriggerEvent.beforeUpdate);
	}
}
