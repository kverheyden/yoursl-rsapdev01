/*
 * @(#)CCWCArchiveDocumentInsertTest.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 /**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Note !!!
// ========
//@isTest(SeeAllData=true) not allowed by tests creating an order. Because a trigger SCOrder_AI_CallSapWebService calls the Web Service
//====================================
// CCWCOrderCreate if IFEnableTriggerOrderCreate__c of CCSetting is greater than 0. By production and qa data it is the case.
// Calling of Web services while testing causes an exception that aborts the test. So we will not get the test running over the 75 procent of 
// the code
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@isTest
private class CCWCArchiveDocumentInsertTest
{
    private static Boolean seeAllData = CCWCTestBase.isSeeingAllData();
    static testMethod void positiveTestOrderCase1()
    {
        Test.setMock(WebServiceMock.class, new CCWCCallOutMock());

        CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        CCWCTestBase.setAppSettingsForOrder(appSettings, seeAllData);
        CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
            ccSettings = CCSettings__c.getInstance();
        }
        CCWCTestBase.setCCSettings(ccSettings, seeAllData);
        
        CCWCTestBase.createDomsForOrderCreation(seeAllData);
        Boolean doUpsert = true;
        SCOrder__c order = CCWCTestBase.createTestOrder(appSettings, doUpsert);
        Attachment attachment = [select id from Attachment where ParentId = : order.id];
        Id attachmentId = attachment.Id;
        
        Test.StartTest();

        // call service
        Boolean async = false;
        Boolean testMode = true;
        String requestMessageID = CCWCArchiveDocumentInsert.callout(attachmentId, async, testMode);
        debug('requestMessageID: ' + requestMessageID);       
        
        // make assertion after web call
        List<SCOrder__c> orderList1 = [select id, name, ERPStatusArchiveDocumentInsert__c from SCOrder__c where id = : order.Id];
        System.assertEquals(orderList1.size(), 1);
        System.assertEquals('pending', orderList1[0].ERPStatusArchiveDocumentInsert__c); 

        List<SCInterfaceLog__c> ilList = [select id, MessageId__c, Order__c, ResultCode__c from SCInterfaceLog__c where Order__c = :order.id ];        
        debug('request interfaceLog list: ' + ilList);
        
        // fill response structure
        CCWSGenericResponse.GenericServiceResponseMessageClass gr = new CCWSGenericResponse.GenericServiceResponseMessageClass();
        String messageId = u.getMessageID();
        String messageUUID = u.getMessageID();
        String operation = 'ArchiveDocument';
        String externalID = attachmentId;
        debug('externalID: ' + externalID);

        CCWCTestBase.prepareWCGenericResponse(gr, messageId, messageUUID, requestMessageID, operation, externalID);
        // add reference        
        String idType = 'SAP ArchiveLink: Document ID';
        String sapOrderNumber = 'SAP-OrderNo-1';
        String referenceID = sapOrderNumber;
        CCWCTestBase.addReferenceItem (gr, idType, null, referenceId); //externalID

        // add log
        String typeId = 'TypeID';
        Integer severityCode = 1; 
        String msg = 'Order document created';
        
        CCWCTestBase.addLogEntry (gr, typeId, externalID, severityCode, msg);       
        // call the response service
        CCWSGenericResponse.transmit(gr);

        Test.StopTest();

        // make assertions 
        // read Order
        List<SCOrder__c> orderList2 = [select id, name, ERPStatusArchiveDocumentInsert__c, ERPOrderNo__c, ERPResultDate__c from SCOrder__c 
                            where id = : order.Id];
        System.assertEquals(orderList2.size(), 1);
        System.assertEquals('ok', orderList2[0].ERPStatusArchiveDocumentInsert__c); 
        Date today = Date.today();
        System.assertEquals(today, orderList2[0].ERPResultDate__c.date()); 

    }

    static testMethod void missingReferencesInResponse()
    {
        Test.setMock(WebServiceMock.class, new CCWCCallOutMock());

        CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        CCWCTestBase.setAppSettingsForOrder(appSettings, seeAllData);
        CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
            ccSettings = CCSettings__c.getInstance();
        }
        CCWCTestBase.setCCSettings(ccSettings, seeAllData);
        
        CCWCTestBase.createDomsForOrderCreation(seeAllData);
        Boolean doUpsert = true;
        SCOrder__c order = CCWCTestBase.createTestOrder(appSettings, doUpsert);
        Attachment attachment = [select id from Attachment where ParentId = : order.id];
        Id attachmentId = attachment.Id;
        
        Test.StartTest();
        
        // call service
        Boolean async = false;
        Boolean testMode = true;
        String requestMessageID = CCWCArchiveDocumentInsert.callout(attachmentId, async, testMode);
        debug('requestMessageID: ' + requestMessageID);       
        
        // make assertion after web call
        List<SCOrder__c> orderList1 = [select id, name, ERPStatusArchiveDocumentInsert__c from SCOrder__c where id = : order.Id];
        System.assertEquals(orderList1.size(), 1);
        System.assertEquals('pending', orderList1[0].ERPStatusArchiveDocumentInsert__c); 

        List<SCInterfaceLog__c> ilList = [select id, MessageId__c, Order__c, ResultCode__c from SCInterfaceLog__c where Order__c = :order.id ];        
        debug('request interfaceLog list: ' + ilList);
        
        // fill response structure
        CCWSGenericResponse.GenericServiceResponseMessageClass gr = new CCWSGenericResponse.GenericServiceResponseMessageClass();
        String messageId = u.getMessageID();
        String messageUUID = u.getMessageID();
        String operation = 'ArchiveDocument';
        String externalID = attachmentId;
        debug('externalID: ' + externalID);

        // no references
        CCWCTestBase.prepareWCGenericResponse(gr, messageId, messageUUID, requestMessageID, operation, externalID);
        gr.References.References = null;
        
        // add log
        String typeId = 'TypeID';
        Integer severityCode = 1; 
        String msg = 'Order document created';
        
        CCWCTestBase.addLogEntry (gr, typeId, externalID, severityCode, msg);       
        // call the response service
        CCWSGenericResponse.transmit(gr);


        Test.StopTest();
        // make assertions 

        // read Order
        List<SCOrder__c> orderList2 = [select id, name, ERPStatusArchiveDocumentInsert__c, ERPOrderNo__c, ERPResultDate__c from SCOrder__c 
                            where id = : order.Id];
        System.assertEquals(orderList2.size(), 1);
        System.assertEquals('ok', orderList2[0].ERPStatusArchiveDocumentInsert__c); 
        Date today = Date.today();
        System.assertEquals(today, orderList2[0].ERPResultDate__c.date()); 

    }

    static testMethod void negativeTestOrderCase1()
    {
//      Boolean seeAllData = CCWCTestBase.isSeeingAllData();
        CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        CCWCTestBase.setAppSettingsForOrder(appSettings, seeAllData);

        CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
            ccSettings = CCSettings__c.getInstance();
        }
        CCWCTestBase.setCCSettings(ccSettings, seeAllData);
        
        CCWCTestBase.createDomsForOrderCreation(seeAllData);
        Boolean doUpsert = true;
        SCOrder__c order = CCWCTestBase.createTestOrder(appSettings, doUpsert);
        
        // determine the attachment for an order item
        Attachment attachment = [select id from Attachment where ParentId = : order.id];
        Id attachmentId = attachment.Id;


        Test.StartTest();
        
        // call service
        Boolean async = false;
        Boolean testMode = true;
        String requestMessageID = CCWCArchiveDocumentInsert.callout(attachmentId, async, testMode);       

        // make assertion after web call
        List<SCOrder__c> orderList1 = [select id, name, ERPStatusArchiveDocumentInsert__c from SCOrder__c where id = : order.Id];
        System.assertEquals(orderList1.size(), 1);
        System.assertEquals('pending', orderList1[0].ERPStatusArchiveDocumentInsert__c); 
        
        List<SCInterfaceLog__c> ilList = [select id, MessageId__c, Order__c, ResultCode__c from SCInterfaceLog__c where Order__c = :order.id ];        
        debug('request interfaceLog list: ' + ilList);
       
        // fill response structure
        CCWSGenericResponse.GenericServiceResponseMessageClass gr = new CCWSGenericResponse.GenericServiceResponseMessageClass();
        String messageId = u.getMessageID();
        String messageUUID = u.getMessageID();
        String operation = 'ArchiveDocument';
        String externalID = attachmentId;
        debug('externalID: ' + externalID);

        CCWCTestBase.prepareWCGenericResponse(gr, messageId, messageUUID, requestMessageID, operation, externalID);
        // add reference        
        String idType = 'SAP ArchiveLink: Document ID';
        String sapOrderNumber = 'SAP-OrderNo-1';
        String referenceID = sapOrderNumber;
        CCWCTestBase.addReferenceItem (gr, idType, externalID, referenceId);

        // add log with an error
        String typeId = 'TypeID';
        Integer severityCode = 3; 
        String msg = 'Order document not created';
        
        CCWCTestBase.addLogEntry (gr, typeId, externalID, severityCode, msg);       
        // call the response service
        CCWSGenericResponse.transmit(gr);

        Test.StopTest();

        // make assertions 
        // read Order
        List<SCOrder__c> orderList2 = [select id, name, ERPStatusArchiveDocumentInsert__c, ERPOrderNo__c, ERPResultDate__c from SCOrder__c 
                            where id = : order.Id];
        System.assertEquals(orderList2.size(), 1);
        System.assertEquals('error', orderList2[0].ERPStatusArchiveDocumentInsert__c);

    }

    static testMethod void positiveTestInventoryCase1() 
    {
//      Boolean seeAllData = CCWCTestBase.isSeeingAllData();
        CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        CCWCTestBase.setAppSettingsForOrder(appSettings, seeAllData);

        CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
            ccSettings = CCSettings__c.getInstance();
        }
        CCWCTestBase.setCCSettings(ccSettings, seeAllData);
        
        // create stock
        Boolean doUpsert = true;
        SCPlant__c plant = CCWCTestBase.createTestPlant(doUpsert);
        List<SCStock__c> stockList = CCWCTestBase.createTestStocks(plant.Id, doUpsert);
        System.assertEquals(true, stockList.size() > 0);

        ID stockId = stockList[0].id;
        System.assertNotEquals(null, stockID);

        List<SCArticle__c> articleList = CCWCTestBase.createTestArticles(appSettings, doUpsert);

        // create stock items
        List<SCStockItem__c> stockItemList = CCWCTestBase.createTestStockItems(articleList, stockList[0].id, stockList[1].id, doUpsert);
        System.assertEquals(true, stockItemList.size() > 0);


        // create inventory
        
        String description = 'Description';
        String fiscalYear = '2012';
        Boolean full = true;
        Date plannedCountDate = Date.today();
        ID inventoryID = SCboInventory.createInventory(stockId, description, fiscalYear, full, plannedCountDate);
        System.assertNotEquals(null, inventoryId);

        List<SCInventory__c> invList = [select id, name, ERPStatus__c from SCInventory__c where id = : inventoryId];
        System.assertEquals(true, invList.size() > 0);
        invList[0].Status__c = 'created';
        update invList;
        
        String inventoryName = invList[0].name;
        String InventoryCountNumber = '123';
        System.assertNotEquals(null, inventoryName);


        Attachment attachment = CCWCTestBase.createAttachment(invList[0]);
        Id attachmentId = attachment.Id;
        
        
        Test.StartTest();
        
        // call service
        Boolean async = false;
        Boolean testMode = true;
        String requestMessageID = CCWCArchiveDocumentInsert.callout(attachmentId, CCWCArchiveDocumentInsert.MODE_FOR_INVENTORY, async, testMode);
        debug('requestMessageID: ' + requestMessageID);       
        
        // make assertion after web call
        List<SCInventory__c> inventoryList1 = [select id, name, ERPStatusArchiveDocumentInsert__c from SCInventory__c where id = : inventoryId];
        System.assertEquals(inventoryList1.size(), 1);
        System.assertEquals('pending', inventoryList1[0].ERPStatusArchiveDocumentInsert__c); 
        
        List<SCInterfaceLog__c> ilList = [select id, MessageId__c, Order__c, Inventory__c, ResultCode__c from SCInterfaceLog__c where Inventory__c = :inventoryId ];        
        debug('request interfaceLog list: ' + ilList);
        
        // fill response structure
        CCWSGenericResponse.GenericServiceResponseMessageClass gr = new CCWSGenericResponse.GenericServiceResponseMessageClass();
        String messageId = u.getMessageID();
        String messageUUID = u.getMessageID();
        String operation = 'ArchiveDocument';
        String externalID = attachmentId;
        debug('externalID: ' + externalID);

        CCWCTestBase.prepareWCGenericResponse(gr, messageId, messageUUID, requestMessageID, operation, externalID);
        // add reference        
        String idType = 'SAP ArchiveLink: Document ID';
        String sapOrderNumber = 'SAP-OrderNo-1';
        String referenceID = sapOrderNumber;
        CCWCTestBase.addReferenceItem (gr, idType, externalID, referenceId);

        // add log
        String typeId = 'TypeID';
        Integer severityCode = 1; 
        String msg = 'Inventory document created';
        
        CCWCTestBase.addLogEntry (gr, typeId, externalID, severityCode, msg);       
        // call the response service
        CCWSGenericResponse.transmit(gr);

        Test.StopTest();

        // make assertions 
        // read Order
        List<SCInventory__c> inventoryList2 = [select id, name, ERPStatusArchiveDocumentInsert__c, ERPResultDate__c from SCInventory__c 
                            where id = : inventoryId];
        System.assertEquals(inventoryList2.size(), 1);
        System.assertEquals('ok', inventoryList2[0].ERPStatusArchiveDocumentInsert__c); 
        Date today = Date.today();
        System.assertEquals(today, inventoryList2[0].ERPResultDate__c.date()); 
        
    }
    static testMethod void negativeTestInventoryCase1() 
    {
//      Boolean seeAllData = CCWCTestBase.isSeeingAllData();
        CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        CCWCTestBase.setAppSettingsForOrder(appSettings, seeAllData);

        CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
            ccSettings = CCSettings__c.getInstance();
        }
        CCWCTestBase.setCCSettings(ccSettings, seeAllData);
        
        // create stock
        Boolean doUpsert = true;
        SCPlant__c plant = CCWCTestBase.createTestPlant(doUpsert);
        List<SCStock__c> stockList = CCWCTestBase.createTestStocks(plant.Id, doUpsert);
        System.assertEquals(true, stockList.size() > 0);

        ID stockId = stockList[0].id;
        System.assertNotEquals(null, stockID);

        List<SCArticle__c> articleList = CCWCTestBase.createTestArticles(appSettings, doUpsert);

        // create stock items
        List<SCStockItem__c> stockItemList = CCWCTestBase.createTestStockItems(articleList, stockList[0].id, stockList[1].id, doUpsert);
        System.assertEquals(true, stockItemList.size() > 0);

        // create inventory
        
        String description = 'Description';
        String fiscalYear = '2012';
        Boolean full = true;
        Date plannedCountDate = Date.today();
        ID inventoryID = SCboInventory.createInventory(stockId, description, fiscalYear, full, plannedCountDate);
        System.assertNotEquals(null, inventoryId);

        List<SCInventory__c> invList = [select id, name, ERPStatus__c from SCInventory__c where id = : inventoryId];
        System.assertEquals(true, invList.size() > 0);
        invList[0].Status__c = 'created';
        update invList;
        
        String inventoryName = invList[0].name;
        String InventoryCountNumber = '123';
        System.assertNotEquals(null, inventoryName);


        Attachment attachment = CCWCTestBase.createAttachment(invList[0]);
        Id attachmentId = attachment.Id;
        
        
        Test.StartTest();
        
        // call service
        Boolean async = false;
        Boolean testMode = true;
        String requestMessageID = CCWCArchiveDocumentInsert.callout(attachmentId, CCWCArchiveDocumentInsert.MODE_FOR_INVENTORY, async, testMode);
        debug('requestMessageID: ' + requestMessageID);       
        
        // make assertion after web call
        List<SCInventory__c> inventoryList1 = [select id, name, ERPStatusArchiveDocumentInsert__c from SCInventory__c where id = : inventoryId];
        System.assertEquals(inventoryList1.size(), 1);
        System.assertEquals('pending', inventoryList1[0].ERPStatusArchiveDocumentInsert__c); 

        List<SCInterfaceLog__c> ilList = [select id, MessageId__c, Order__c, Inventory__c, ResultCode__c from SCInterfaceLog__c where Inventory__c = :inventoryId ];        
        debug('request interfaceLog list: ' + ilList);
        
        
        // fill response structure
        CCWSGenericResponse.GenericServiceResponseMessageClass gr = new CCWSGenericResponse.GenericServiceResponseMessageClass();
        String messageId = u.getMessageID();
        String messageUUID = u.getMessageID();
        String operation = 'ArchiveDocument';
        String externalID = attachmentId;
        debug('externalID: ' + externalID);

        CCWCTestBase.prepareWCGenericResponse(gr, messageId, messageUUID, requestMessageID, operation, externalID);
        // add reference        
        String idType = 'SAP ArchiveLink: Document ID';
        String sapOrderNumber = 'SAP-OrderNo-1';
        String referenceID = sapOrderNumber;
        CCWCTestBase.addReferenceItem (gr, idType, externalID, referenceId);

        // add log
        String typeId = 'TypeID';
        Integer severityCode = 3; 
        String msg = 'Inventory document not created';
        
        CCWCTestBase.addLogEntry (gr, typeId, externalID, severityCode, msg);       
        // call the response service
        CCWSGenericResponse.transmit(gr);

        Test.StopTest();

        // make assertions 
        // read Order
        List<SCInventory__c> inventoryList2 = [select id, name, ERPStatusArchiveDocumentInsert__c, ERPResultDate__c from SCInventory__c 
                            where id = : inventoryId];
        System.assertEquals(inventoryList2.size(), 1);
        System.assertEquals('error', inventoryList2[0].ERPStatusArchiveDocumentInsert__c); 
        Date today = Date.today();
        System.assertEquals(today, inventoryList2[0].ERPResultDate__c.date()); 
        
    }

/* not possible by running of tests
    static testMethod void testOrderCaseFor_piCceagDeSfdc1()
    {
        Test.StartTest();
//      Boolean seeAllData = CCWCTestBase.isSeeingAllData();
        debug('piCceagDeSfdc1');
        CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        CCWCTestBase.setAppSettingsForOrder(appSettings, seeAllData);

        CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
            ccSettings = CCSettings__c.getInstance();
            debug('ccSettings.IFEnableTriggerOrderCreate__c: ' + ccSettings.IFEnableTriggerOrderCreate__c);
            ccSettings.IFEnableTriggerOrderCreate__c = 0;           
        }
        CCWCTestBase.setCCSettings(ccSettings, seeAllData);
        
        CCWCTestBase.createDomsForOrderCreation(seeAllData);
        Boolean doUpsert = true;
        SCOrder__c order = CCWCTestBase.createTestOrder(appSettings, doUpsert);
        Attachment attachment = [select id from Attachment where ParentId = : order.id];
        Id attachmentId = attachment.Id;
        
        // call service
        Boolean async = false;
        Boolean testMode = false;
        try
        {
            String requestMessageID = CCWCArchiveDocumentInsert.callout(attachmentId, async, testMode);
            debug('requestMessageID: ' + requestMessageID);       
        }
        catch(Exception e)
        {
        }        
        Test.StopTest();
    }
*/

/*
    static testMethod void musterTest()
    {
        Test.StartTest();
        //Test.setMock(WebServiceMock.class, new CCWCOrderCloseTestMock()); 
        Test.StopTest();
    }   
*/
    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }

}
