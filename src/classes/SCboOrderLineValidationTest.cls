/*
 * @(#)SCboOrderLineValidationTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This class contains the test code for SCboOrderLineValidation.
 *
 * @author Alexander Wagner <awagner@gms-online.de>
*/ 
@isTest
private class SCboOrderLineValidationTest
{
    /*
     * Test: Validation of order line data
     */
    static testMethod void validateOrderLineDataTest() 
    {
        SCHelperTestClass.createTestArticles(true);        
        SCHelperTestClass.createOrder(true);        

        SCboOrderLine boOrderLine = new SCboOrderLine();
        boOrderLine.orderLine.Order__c            = SCHelperTestClass.order.Id;
        boOrderLine.orderLine.Article__c          = SCHelperTestClass.articles[0].Id;
        boOrderLine.orderLine.Qty__c              = 1;
        boOrderLine.orderLine.Type__c             = SCfwConstants.DOMVAL_ORDERLINETYPE_INV;
        boOrderLine.orderLine.MaterialStatus__c   = SCfwConstants.DOMVAL_MATSTAT_NOMATERIAL;
        boOrderLine.orderLine.PriceType__c        = SCfwConstants.DOMVAL_CONDPRICETYPE_PRICE;
        boOrderLine.orderLine.PositionPrice__c    = 50;
        boOrderLine.orderLine.Price__c            = 50;
        boOrderLine.orderLine.UnitPrice__c        = 50;
        boOrderLine.orderLine.UnitNetPrice__c     = 50;
        boOrderLine.orderLine.Tax__c              = 10;
        boOrderLine.orderLine.TaxRate__c          = 10;
        boOrderLine.orderLine.InvoicingType__c    = SCfwConstants.DOMVAL_INVOICINGTYPE_DEFAULT;
        boOrderLine.orderLine.InvoicingSubType__c = SCfwConstants.DOMVAL_INVOICINGSUBTYPE_DEFAULT;
        boOrderLine.orderLine.ContractRelated__c  = false;

        Test.startTest();

        // activate the validation of the picklist values
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        String oldValue = appSettings.VALIDATE_PICKLIST_VALUES__c;
        appSettings.VALIDATE_PICKLIST_VALUES__c = '1';
        update appSettings;

        // Test 1: no order relation
        boOrderLine.orderLine.Order__c = null;
        try
        {
            SCboOrderLineValidation.validate(boOrderLine);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### validateOrderLineDataTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2500 - '));
        }

        // Test 2: no article relation
        boOrderLine.orderLine.Order__c   = SCHelperTestClass.order.Id;
        boOrderLine.orderLine.Article__c = null;
        try
        {
            SCboOrderLineValidation.validate(boOrderLine);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### validateOrderLineDataTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2501 - '));
        }

        // Test 3: no quantity
        boOrderLine.orderLine.Article__c = SCHelperTestClass.articles[0].Id;
        boOrderLine.orderLine.Qty__c     = null;
        try
        {
            SCboOrderLineValidation.validate(boOrderLine);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### validateOrderLineDataTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2502 - '));
        }

        // Test 4: no type
        boOrderLine.orderLine.Qty__c  = 1;
        boOrderLine.orderLine.Type__c = null;
        try
        {
            SCboOrderLineValidation.validate(boOrderLine);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### validateOrderLineDataTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2503 - '));
        }

        // Test 5: no material status
        boOrderLine.orderLine.Type__c           = SCfwConstants.DOMVAL_ORDERLINETYPE_INV;
        boOrderLine.orderLine.MaterialStatus__c = null;
        try
        {
            SCboOrderLineValidation.validate(boOrderLine);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### validateOrderLineDataTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2504 - '));
        }

        // Test 6: no price type
        boOrderLine.orderLine.MaterialStatus__c = SCfwConstants.DOMVAL_MATSTAT_NOMATERIAL;
        boOrderLine.orderLine.PriceType__c      = null;
        try
        {
            SCboOrderLineValidation.validate(boOrderLine);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### validateOrderLineDataTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2505 - '));
        }

        // Test 7: no position price
        boOrderLine.orderLine.PriceType__c     = SCfwConstants.DOMVAL_CONDPRICETYPE_PRICE;
        boOrderLine.orderLine.PositionPrice__c = null;
        try
        {
            SCboOrderLineValidation.validate(boOrderLine);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### validateOrderLineDataTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2506 - '));
        }

        // Test 8: no price
        boOrderLine.orderLine.PositionPrice__c = 50;
        boOrderLine.orderLine.Price__c         = null;
        try
        {
            SCboOrderLineValidation.validate(boOrderLine);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### validateOrderLineDataTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2507 - '));
        }

        // Test 9: no unit price
        boOrderLine.orderLine.Price__c     = 50;
        boOrderLine.orderLine.UnitPrice__c = null;
        try
        {
            SCboOrderLineValidation.validate(boOrderLine);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### validateOrderLineDataTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2508 - '));
        }

        // Test 10: no unit net price
        boOrderLine.orderLine.UnitPrice__c    = 50;
        boOrderLine.orderLine.UnitNetPrice__c = null;
        try
        {
            SCboOrderLineValidation.validate(boOrderLine);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### validateOrderLineDataTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2509 - '));
        }

        // Test 11: no tax
        boOrderLine.orderLine.UnitNetPrice__c = 50;
        boOrderLine.orderLine.Tax__c          = null;
        try
        {
            SCboOrderLineValidation.validate(boOrderLine);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### validateOrderLineDataTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2510 - '));
        }

        // Test 12: no tax rate
        boOrderLine.orderLine.Tax__c     = 10;
        boOrderLine.orderLine.TaxRate__c = null;
        try
        {
            SCboOrderLineValidation.validate(boOrderLine);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### validateOrderLineDataTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2511 - '));
        }

        // Test 13: no invoicing type
        boOrderLine.orderLine.TaxRate__c       = 10;
        boOrderLine.orderLine.InvoicingType__c = null;
        try
        {
            SCboOrderLineValidation.validate(boOrderLine);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### validateOrderLineDataTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2512 - '));
        }

        // Test 14: no invoicing subtype
        boOrderLine.orderLine.InvoicingType__c    = SCfwConstants.DOMVAL_INVOICINGTYPE_DEFAULT;
        boOrderLine.orderLine.InvoicingSubType__c = null;
        try
        {
            SCboOrderLineValidation.validate(boOrderLine);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### validateOrderLineDataTest(): Exception -> ' + e);
            //TODO System.assert(e.getMessage().startsWith('E2513 - '));
        }

        // Test 15: no contract related
        // it seems that boOrderLine.orderLine.ContractRelated__c has always a value
        // so test doesn't work 
        /*
        boOrderLine.orderLine.InvoicingSubType__c = SCfwConstants.DOMVAL_INVOICINGSUBTYPE_DEFAULT;
        boOrderLine.orderLine.ContractRelated__c  = null;
        try
        {
            SCboOrderLineValidation.validate(boOrderLine);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### validateOrderLineDataTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2514 - '));
        }
        */

        // Test 16: no list price
        boOrderLine.orderLine.InvoicingSubType__c = SCfwConstants.DOMVAL_INVOICINGSUBTYPE_DEFAULT;
        boOrderLine.orderLine.ListPrice__c  = null;
        try
        {
            SCboOrderLineValidation.validate(boOrderLine);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### validateOrderLineDataTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2516 - '));
        }
        
        // Test 17: wrong auto pos
        boOrderLine.orderLine.ListPrice__c       = 0;
        boOrderLine.orderLine.ContractRelated__c = false;
        boOrderLine.orderline.AutoPos__c = 'XY';
        try
        {
            SCboOrderLineValidation.validate(boOrderLine);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### validateOrderLineDataTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 18: wrong country
        boOrderLine.orderline.AutoPos__c = '';
        boOrderLine.orderline.Country__c = 'XY';
        try
        {
            SCboOrderLineValidation.validate(boOrderLine);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### validateOrderLineDataTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 19: wrong invoicing type
        boOrderLine.orderline.Country__c = 'DE';
        boOrderLine.orderline.InvoicingType__c = 'XY';
        try
        {
            SCboOrderLineValidation.validate(boOrderLine);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### validateOrderLineDataTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 20: wrong invoicing subtype
        boOrderLine.orderline.InvoicingType__c = SCfwConstants.DOMVAL_INVOICINGTYPE_DEFAULT;
        boOrderLine.orderline.InvoicingSubType__c = 'XY';
        try
        {
            SCboOrderLineValidation.validate(boOrderLine);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### validateOrderLineDataTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 21: wrong material status
        boOrderLine.orderline.InvoicingSubType__c = SCfwConstants.DOMVAL_INVOICINGSUBTYPE_DEFAULT;
        boOrderLine.orderline.MaterialStatus__c = 'XY';
        try
        {
            SCboOrderLineValidation.validate(boOrderLine);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### validateOrderLineDataTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 22: wrong price type
        boOrderLine.orderline.MaterialStatus__c = SCfwConstants.DOMVAL_MATSTAT_NOMATERIAL;
        boOrderLine.orderline.PriceType__c = 'XY';
        try
        {
            SCboOrderLineValidation.validate(boOrderLine);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### validateOrderLineDataTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 22: wrong type
        boOrderLine.orderline.PriceType__c = SCfwConstants.DOMVAL_CONDPRICETYPE_PRICE;
        boOrderLine.orderline.Type__c = 'XY';
        try
        {
            SCboOrderLineValidation.validate(boOrderLine);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### validateOrderLineDataTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 23: no errors
        boOrderLine.orderline.Type__c = SCfwConstants.DOMVAL_ORDERLINETYPE_INV;
        try
        {
            Boolean ret = SCboOrderLineValidation.validate(boOrderLine);
            System.assertEquals(true, ret);
        }
        catch(Exception e)
        {
            System.assert(false);
        }

        // set the old value for the validation of the picklist values
        appSettings.VALIDATE_PICKLIST_VALUES__c = oldValue;
        update appSettings;
        
        Test.stopTest();
    } // validateOrderLineDataTest
} // SCboOrderLineValidationTest
