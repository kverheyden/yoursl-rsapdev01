global virtual class SCCokeOpenIDUserRegistrationHandler implements Auth.Registrationhandler
{
  /**
   * interface Auth.RegistrationHandler - returns a user based on given data
   */
  global User createUser(Id portalId, Auth.UserData data)
  {
    
    System.debug('createUser');
    
    System.debug(data);   
    User user = [SELECT Id, email, isactive, lastname From User WHERE isactive = true AND lastname = :data.lastname AND email=:data.email Limit 1];   
    user.CokeID__c = data.attributeMap.get('id');
    update user;
    return user;
   
  }   
  
  /**
   * interface Auth.RegistrationHandler - updates a user with a given userId
   */
  global void updateUser(Id UserID, Id portalId, Auth.UserData data)
  {
    System.debug('updateUser');
    String CokeIDUserId =  data.attributeMap.get('id'); 
    User user = [SELECT Id, email, isactive, lastname, CokeID__c From User WHERE isactive = true AND CokeID__c =:CokeIDUserId Limit 1];
    System.debug(user);
 
  }
  
  /**
   * updates a user with generic (user unspecific data) like currency etc
   */
  public virtual void setGeneralUserData(User user, Auth.UserData data)
  {

  }  
}
