/*
 * @(#)SCMaterialAvailabilityTest.cls    aw  29.05.2012
 *  
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
@isTest
private class SCMaterialAvailabilityTest
{
    static testMethod void testTestMode() 
    {
        SCHelperTestClass3.createCustomSettings('XX', true);

        SCMaterialAvailabilityData.SCServiceData serviceData = new SCMaterialAvailabilityData.SCServiceData();
        serviceData.mode = SCMaterialAvailabilityData.MODE_TEST;
                
        SCMaterialAvailabilityData.SCResult result = SCMaterialAvailability.call(serviceData);
        System.assertEquals(true, result.success);
        System.assertEquals(2, result.results.size());
    }

    static testMethod void testProdMode() 
    {
        SCHelperTestClass3.createCustomSettings('XX', true);

        SCMaterialAvailabilityData.SCServiceData serviceData = new SCMaterialAvailabilityData.SCServiceData();
        serviceData.mode = SCMaterialAvailabilityData.MODE_PROD;
                
        //SCMaterialAvailabilityData.SCResult result = SCMaterialAvailability.call(serviceData);
        //System.assertEquals(false, result.success);
    }
}
