/*
 * @(#)SCTestTriggerAccount.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * @author SS <sschrage@gms-online.de>
 */
@isTest 
private class SCTriggerAccountTest
{
    public static testMethod void AccountBeforeInsertandUpdateFirst()
    {
        Account acc = new Account(Name = 'Test', LastName__c = 'TestLastName', FirstName__c = 'TestFirstName',
                                                 BillingState = 'FL',    BillingCountryState__c = 'NY',
                                                 BillingCountry = 'GB',  BillingCountry__c = 'DE',
                                                 ShippingState = 'FL',   ShippingCountryState__c = 'NY',
                                                 ShippingCountry = 'GB', ShippingCountry__c = 'DE');
        
        Database.insert(acc);
        
        System.assertEquals(1, [SELECT count() FROM Account WHERE BillingState = 'NY'  AND BillingCountry = 'DE'
                                                              AND ShippingState = 'NY' AND ShippingCountry = 'DE'
                                                              AND Name = 'TestFirstName TestLastName']);
        acc.BillingState = 'FL';
        acc.BillingCountry = 'GB';
        acc.ShippingState = 'FL';
        acc.ShippingCountry = 'GB';
        
        Database.update(acc);
        
        System.assertEquals(1, [SELECT count() FROM Account WHERE BillingCountryState__c = 'FL'  AND BillingCountry__c = 'GB'
                                                              AND ShippingCountryState__c = 'FL' AND ShippingCountry__c = 'GB'
                                                              AND Name = 'TestFirstName TestLastName']);
        acc.BillingCountryState__c = 'NY';
        acc.BillingCountry__c = 'DE';
        acc.ShippingCountryState__c = 'NY';
        acc.ShippingCountry__c = 'DE';
        
        Database.update(acc);
        
        System.assertEquals(1, [SELECT count() FROM Account WHERE BillingState = 'NY'  AND BillingCountry = 'DE'
                                                              AND ShippingState = 'NY' AND ShippingCountry = 'DE'
                                                              AND Name = 'TestFirstName TestLastName']);
    }
    
    public static testMethod void AccountBeforeInsertandUpdateSecond()
    {
        Account acc = new Account(Name = 'Test', LastName__c = 'TestLastName',
                                                 BillingState = 'FL',    BillingCountryState__c = 'NY',
                                                 BillingCountry = 'GB',  BillingCountry__c = 'DE',
                                                 ShippingState = 'FL',   ShippingCountryState__c = 'NY',
                                                 ShippingCountry = 'GB', ShippingCountry__c = 'DE');
        
        Database.insert(acc);
        
        System.assertEquals(1, [SELECT count() FROM Account WHERE BillingState = 'NY'  AND BillingCountry = 'DE'
                                                              AND ShippingState = 'NY' AND ShippingCountry = 'DE'
                                                              AND Name = 'TestLastName']);
        acc.BillingState = 'FL';
        acc.BillingCountry = 'GB';
        acc.ShippingState = 'FL';
        acc.ShippingCountry = 'GB';
        
        Database.update(acc);
        
        System.assertEquals(1, [SELECT count() FROM Account WHERE BillingCountryState__c = 'FL'  AND BillingCountry__c = 'GB'
                                                              AND ShippingCountryState__c = 'FL' AND ShippingCountry__c = 'GB'
                                                              AND Name = 'TestLastName']);
        acc.BillingCountryState__c = 'NY';
        acc.BillingCountry__c = 'DE';
        acc.ShippingCountryState__c = 'NY';
        acc.ShippingCountry__c = 'DE';
        
        Database.update(acc);
        
        System.assertEquals(1, [SELECT count() FROM Account WHERE BillingState = 'NY'  AND BillingCountry = 'DE'
                                                              AND ShippingState = 'NY' AND ShippingCountry = 'DE'
                                                              AND Name = 'TestLastName']);
    }
}
