/*
 * @(#)SCInventoryAbortController.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *  
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This class cancells or closes an inventory. 
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCInventoryAbortController
{
    public String oid = '';
    public String errorMsg { get; set; }
    public Boolean ok { get; set; }
    
    public SCInventoryAbortController()
    {
        errorMsg = '';
        ok = false;
    }
    
   /*
    * Init method to initialize the parameter
    * @return  null
    */
    public PageReference init()
    {
        // First we need an inventory ID
        if(ApexPages.currentPage().getParameters().containsKey('oid') && ApexPages.currentPage().getParameters().get('oid') != '')
        {
            oid = ApexPages.currentPage().getParameters().get('oid');
            
            // Inventory need to be closed
            if(ApexPages.currentPage().getParameters().containsKey('close') && ApexPages.currentPage().getParameters().get('close') == 'true')
            {
                close(oid);
            }
            
            // Inventory need to be cancelled
            if(ApexPages.currentPage().getParameters().containsKey('cancel') && ApexPages.currentPage().getParameters().get('cancel') == 'true')
            {
                cancel(oid);
            }
        }
        
        return null;
    }
    
   /*
    * Closes the inventory
    * @param   oid              id of the SCInventory object to close
    * @return  null
    */
    public PageReference close(String oid)
    {
        SCboInventory boInventory = new SCboInventory();
        String returnedValue = boInventory.close(oid, true, Date.today());
        
        if(returnedValue == 'OK')
        {
            ok = true;
            return null;
        }
        else
        {
            errorMsg = returnedValue;
        }
        
        return null;
    }
    
   /*
    * Cancells the inventory
    * @param   oid              id of the SCInventory object to close
    * @return  null
    */
    public PageReference cancel(String oid)
    {
        SCboInventory boInventory = new SCboInventory();
        String returnedValue = boInventory.cancel(oid);
        
        if(returnedValue == 'OK')
        {
            ok = true;
            return null;
        }
        else
        {
            errorMsg = returnedValue;
        }
        
        return null;
    }
    
   /*
    * Cancells the inventory
    * @param   oid              id of the SCInventory object to close
    * @return  null if error | redirect to the inventory page
    */
    public PageReference goBack()
    {
        if(String.isBlank(errorMsg))
        {            
            PageReference p = new PageReference('/' + oid);
            return p;
        }
        else
        {
            return null;
        }
        
        return null;
    }
}
