@RestResource(urlMapping='/CdeOrder/*') 
global with sharing class CCWCRestCdeOrder{
	
	global class CdeOrderWrapper{
		public String AccountId { get; set; }
		public String RequestId { get; set; }
		public String Contact { get; set; }
		public String EarliestDeliveryDate { get; set; }
		public String Email { get; set; }
		public String Mobile { get; set; }
		public String Phone { get; set; }
		public String RequestedDeliveryDate { get; set; }
		public String Street { get; set; }
		public List<CdeOrderItemWrapper> CdeOrderItems { get; set; }
		public String AppIdentifierKey { get; set; }
		public String Comment { get; set; }
    }
	
	global class CdeOrderItemWrapper {
		public String Building { get; set; }
		public String Floor { get; set; }
		public String IsLiftAvailable { get; set; }
		public String WithLockingSystem { get; set; }
		public String ProductModel { get; set; }
		public String Quantity { get; set; }
		public String Room { get; set; }
		public String HasSeparateCircuit { get; set; }
		public String Steps { get; set; }
	}
	
	global class ResponseWrapper{
        public String StatusCode;
        public String StatusMessage;
		public String Stacktrace;
		public String AppIdentifierKey;
        public CdeOrder__c CdeOrder;
    } 
	
	@HttpPost
    global static void doPost(CdeOrderWrapper CdeOrder){
		Datetime requestStart = System.now();
		ResponseWrapper response = new ResponseWrapper();
		
		Database.DMLOptions dml = new Database.DMLOptions();
		dml.allowFieldTruncation = true;
		
		List<SCProductModel__c> ProductModelList = new List<SCProductModel__c>();
		ProductModelList = [SELECT Id, ProductNameCalc__c, Name FROM SCProductModel__c];
        String OrderListItemSummary = '';
        
		CdeOrder__c newOrder = new CdeOrder__c();
        try{
			if (String.isNotBlank(CdeOrder.AccountId))
				newOrder.Account__c = Id.valueOf(CdeOrder.AccountId);
			if (String.isNotBlank(CdeOrder.RequestId))
				newOrder.Request__c = Id.valueOf(CdeOrder.RequestId);
			if (String.isNotBlank(CdeOrder.Contact))
				newOrder.Contact__c = CdeOrder.Contact;
			if (String.isNotBlank(CdeOrder.Email))
				newOrder.Email__c = CdeOrder.Email;
			if (String.isNotBlank(CdeOrder.Mobile))
				newOrder.Mobile__c = CdeOrder.Mobile;
			if (String.isNotBlank(CdeOrder.Phone))
				newOrder.Phone__c = CdeOrder.Phone;
			if (String.isNotBlank(CdeOrder.Street))
				newOrder.Street__c = CdeOrder.Street;
			if (String.isNotBlank(CdeOrder.EarliestDeliveryDate))
				newOrder.EarliestDeliveryDate__c = Date.valueOf(CdeOrder.EarliestDeliveryDate);
			if (String.isNotBlank(CdeOrder.RequestedDeliveryDate))
				newOrder.RequestedDeliveryDate__c = Date.valueOf(CdeOrder.RequestedDeliveryDate);
            if (String.isNotBlank(CdeOrder.Comment))
				newOrder.Comment__c = CdeOrder.Comment;
			
			insert newOrder;     
			
			if (CdeOrder.CdeOrderItems != null) {
				List<CdeOrderItem__c> newOrderItems = new List<CdeOrderItem__c>();
				
				for(CdeOrderItemWrapper item : CdeOrder.CdeOrderItems) {
					CdeOrderItem__c newItem = new CdeOrderItem__c();
					newItem.CdeOrder__c = newOrder.Id;
					
					newItem.LiftAvailable__c = String.isNotBlank(item.IsLiftAvailable);
					newItem.LockingSystem__c = String.isNotBlank(item.WithLockingSystem);
					newItem.SeparateCircuit__c = String.isNotBlank(item.HasSeparateCircuit);
					if (String.isNotBlank(item.Building))
						newItem.Building__c = item.Building;
					if (String.isNotBlank(item.Floor))
						newItem.Floor__c = item.Floor;
					if (String.isNotBlank(item.Quantity)) {
						newItem.Quantity__c = Double.valueOf(item.Quantity);
						OrderListItemSummary = String.valueOf(newItem.Quantity__c) + ' ';
					}
					if (String.isNotBlank(item.ProductModel)) {
						newItem.ProductModel__c = Id.valueOf(item.ProductModel);
						for (SCProductModel__c ProductModel : ProductModelList) {
							if (ProductModel.Id == newItem.ProductModel__c)
							  OrderListItemSummary = OrderListItemSummary + ProductModel.ProductNameCalc__c + ' (' + 
							    ProductModel.Name + ')\r\n';
						}
					}
					if (String.isNotBlank(item.Room))
						newItem.Room__c = item.Room;
					if (String.isNotBlank(item.Steps))
						newItem.Steps__c = Double.valueOf(item.Steps);
					newItem.setOptions(dml);			
					newOrderItems.add(newItem);
				}
				insert newOrderItems;
				
				newOrder.OrderItemListSummary__c = OrderListItemSummary;
				newOrder.IsReadyToSendToSAP__c = true;
				update newOrder;
			}
        } catch(Exception e){
            response.StatusCode = 'Error';
            response.StatusMessage = e.getTypeName() + ': ' + e.getMessage();
			response.Stacktrace = e.getStackTraceString();
            RestContext.response.addHeader('Content-Type', 'application/json');     
			RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
			RestServiceUtilities.insertNewLog(requestStart, false);
			return;
        }
        response.StatusCode = 'Done';
        response.StatusMessage = 'Success';
		response.AppIdentifierKey = CdeOrder.AppIdentifierKey;
        response.CdeOrder = newOrder;
        
		RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
		RestServiceUtilities.insertNewLog(requestStart, CdeOrder, true);
	}
}
