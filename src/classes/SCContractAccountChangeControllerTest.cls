/*
 * @(#)SCContractAccountChangeControllerTest.cls
 *
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Testclass for the "Change Order Role Controller"
 * 
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCContractAccountChangeControllerTest
{
    private static SCContractAccountChangeController cac;
    
    static testMethod void contractAccountChangeControllerPositiv1()
    {   
        // Prepare the main data for the test
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.createInstalledBaseLocation(true);
        SCHelperTestClass.createInstalledBase(true);
        SCHelperTestClass.createProductModel(true);
        
        SCHelperTestClass2.createContractTemplates(true);
        SCHelperTestClass2.createContract('Contract', SCHelperTestClass2.insuranceTemplate.Id, SCHelperTestClass.account.Id, SCHelperTestClass.installedBaseSingle.Id, true);
    
        ApexPages.currentPage().getParameters().put('id', SCHelperTestClass2.contracts.get(0).Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(SCHelperTestClass2.contracts.get(0));
        
        // Starting the test
        Test.startTest();
        
        cac = new SCContractAccountChangeController(sc);
        
        cac.selAccountId = SCHelperTestClass.account.Id;
        cac.selAccountOwnerId = SCHelperTestClass.account.Id;

        cac.onLoadNewAccount();
        cac.onLoadNewAccountOwner();
        cac.getAccountIds();
        cac.onSaveNewAccounts();
        cac.goBack();
        
        Test.stopTest();
    }
}
