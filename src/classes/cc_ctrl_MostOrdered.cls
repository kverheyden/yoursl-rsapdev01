global with sharing class cc_ctrl_MostOrdered {
    global cc_ctrl_MostOrdered() {
        
    }

    @RemoteAction
    global static List<MostOrderedProductBean> getMostOrderedProducts(){
        List<Id> productIdList = new List<Id>();
        Map<Id, Decimal> productIdQuantityMap = new Map<Id, Decimal>();
        for(AggregateResult ag : [SELECT ccrz__Product__c, 
                                                SUM(ccrz__Quantity__c)
                                            FROM ccrz__E_OrderItem__c
                                            GROUP BY ccrz__Product__c
                                            ORDER BY SUM(ccrz__Quantity__c) DESC]){

            productIdList.add((Id) ag.get('ccrz__Product__c'));
            productIdQuantityMap.put((Id) ag.get('ccrz__Product__c'), (Decimal) ag.get('expr0'));
        }

        Map<Id, ccrz__E_Product__c> productMap = new Map<Id, ccrz__E_Product__c>([
            SELECT Id, ccrz__SKU__c, Name FROM ccrz__E_Product__c WHERE Id IN :productIdList
        ]);

        List<MostOrderedProductBean> beanList = new List<MostOrderedProductBean>();
        for(Id prodId : productIdList){
            if(productMap.get(prodId) != null)
                beanList.add(new MostOrderedProductBean(productMap.get(prodId), productIdQuantityMap.get(prodId)));
        }

        return beanList;
    }



    global class MostOrderedProductBean{

        public String sku{get;set;}
        public String sfid{get;set;}
        public Integer ranking{get;set;}
        public String name{get;set;}
        public Decimal price { get; set; }

        public MostOrderedProductBean(ccrz__E_Product__c prod, Decimal quantity){
            this.sku = prod.ccrz__SKU__c;
            this.sfid = prod.Id;
            this.name = prod.Name;
            this.ranking = quantity.intValue();
            this.price = quantity;
        }

    }

}
