/*
 * @(#)SCboMaterialMovement.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Books material movements in stock items. Used in the material 
 * management processes to increase or reduce the stock item quantities.
 * See: SCbtcMaterialMovement for including material booking in web services
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCboMaterialMovement
{
    private static final String SOURCE_MOBILE = 'mobile';
    private static final String SOURCE_BACKEND = 'backend';
    
    private Boolean allowNegativStockQty;
    private SCfwDomain domMatMove;
    public Map<String, String> articleWithNegativStockQty = new Map<String, String>();
    public List<SCMaterialMovement__c> matMovesForInsert = new List<SCMaterialMovement__c>();
    
    /**
     * Default constructor
     *
     * @param domMatMove Holds the material movement types and their control parameter
     */
    public SCboMaterialMovement(SCfwDomain domMatMove)
    {
        // System.debug('##### SCboMaterialMovement');
        this.domMatMove = domMatMove;
        this.allowNegativStockQty = SCApplicationSettings__c.getInstance().STOCK_ALLOWNEGATIVESTOCK__c;
    } // SCboMaterialMovement

    /**
     * Creates the entry in SCMaterialMovement for the given stock and article.
     * Depending on the material movement type the quantity will be added or subtraced.
     * @param stockId          stock 
     * @param article          article 
     * @param valutationType   valutationType
     * @param qty              quantity
     * @param matStatus        material status (see DOM_MATSTAT)
     * @param matMoveType      material movement type (see DOM_MATMOVETYPE)
     * @param requestDate      requisition date (for material requests)
     * @param deliveryStockId  the delivering stock 
     * @param orderId          reference to the order (optional)
     * @param orderLineId      reference to the order line (optional)
     * @param bookNow          if false, the matMoves are stored in a List. With the method "insertMatMoves" one can insert the matMoves in one Step
     * @return true for success else false
     */
    public Boolean createMatMove(Id stockId, Id article, Decimal qty, 
                                 String matStatus, String matMoveType, 
                                 Date requestDate, Id deliveryStockId, 
                                 Id orderId, Id orderLineId)
    {
        return createMatMove(stockId, article, null, qty, matStatus, matMoveType, requestDate, deliveryStockId, orderId, orderLineId, true);
    }
    public Boolean createMatMove(Id stockId, Id article, String valuationType, Decimal qty, 
                                 String matStatus, String matMoveType, 
                                 Date requestDate, Id deliveryStockId, 
                                 Id orderId, Id orderLineId)
    {
        return createMatMove(stockId, article, valuationType, qty, matStatus, matMoveType, requestDate, deliveryStockId, orderId, orderLineId, true);
    }
    public Boolean createMatMove(Id stockId, Id article, String valuationType, Decimal qty, 
                                 String matStatus, String matMoveType, 
                                 Date requestDate, Id deliveryStockId, 
                                 Id orderId, Id orderLineId,
                                 Boolean bookNow)
    {
        // System.debug('#### createMatMove(): StockId       -> ' + stockId);
        // System.debug('#### createMatMove(): Article       -> ' + article);
        // System.debug('#### createMatMove(): ValuationType -> ' + valuationType);
        // System.debug('#### createMatMove(): Quantity      -> ' + qty);
        
        if (matMoveType.equals(SCfwConstants.DOMVAL_MATMOVETYP_MATREQUEST_CUST) || 
            matMoveType.equals(SCfwConstants.DOMVAL_MATMOVETYP_MATREQUEST_EMPL))
        {
            matStatus = SCfwConstants.DOMVAL_MATSTAT_ORDER;
        } 
        
        // Create a new material movement object 
        SCMaterialMovement__c newMatMove = new SCMaterialMovement__c();
        newMatMove.Stock__c = stockId;
        newMatMove.Article__c = article;
        newMatMove.ValuationType__c = valuationType;
        newMatMove.Qty__c = qty;
        newMatMove.Status__c = matStatus;
        newMatMove.Type__c = matMoveType;
        if (null != requestDate)
        {
            newMatMove.RequisitionDate__c = requestDate;
        } 
        if (null != deliveryStockId)
        {
            newMatMove.DeliveryStock__c = deliveryStockId;
        } 
        if (null != orderId)
        {
            newMatMove.Order__c = orderId;
        } 
        if (null != orderLineId)
        {
            newMatMove.OrderLine__c = orderLineId;
        } 

        try
        {
            // save the material movement
            if ((bookNow != null) && (bookNow == false))
            {
                matMovesForInsert.add(newMatMove);
            }
            else
            {
                insert newMatMove;
            }
            
            return true;
        }
        catch (DmlException e)
        {
            ApexPages.addMessages(e);
            
            return false;
        }
    } // createMatMove
    
    /**
     * Inserts all MaterialMovements, which are stored in the list matMovesForInsert
     */
    public Boolean insertMatMoves() 
    {
        try
        {
            // save the material movement
            if ((matMovesForInsert != null) && (matMovesForInsert.size() > 0))
            {
                Database.insert(matMovesForInsert);
            }
            
            matMovesForInsert = new List<SCMaterialMovement__c>();
            return true;
        }
        catch (DmlException e)
        {
            ApexPages.addMessages(e);
            
            return false;
        }
    } // insertMatMoves

    /**
     * Process all entries in SCMaterialMovement for the given stock and/or order
     * @param stockId  the stock 
     * @param orderId  the order (optional to book only for the stock and order)
     */
    public Boolean bookMaterial(Id stockId, Id orderId)
    {
        // Create a list with all material movements for the given stock
        List<SCMaterialMovement__c> matMovements = null;
        if (null == orderId)
        {
            matMovements = [select Id, Name, Stock__c, Stock__r.Name, Stock__r.ValuationType__c, Article__c, 
                                   Article__r.Name, ValuationType__c, Type__c, Status__c, Qty__c, Source__c, Head__c 
                              from SCMaterialMovement__c 
                             where Stock__c = :stockId 
                               and Status__c = :SCfwConstants.DOMVAL_MATSTAT_BOOK];
        } 
        else
        {
            matMovements = [select Id, Name, Stock__c, Stock__r.Name, Article__c, Stock__r.ValuationType__c,
                                   Article__r.Name, ValuationType__c, Type__c, Status__c, Qty__c,
                                   Source__c, Head__c
                              from SCMaterialMovement__c 
                             where Stock__c = :stockId 
                               and Order__c = :orderId 
                               and Status__c = :SCfwConstants.DOMVAL_MATSTAT_BOOK];
        } 
        return bookMaterialMovements(matMovements);   
    } // bookMaterial    
    
    /**
     * Process all entries in SCMaterialMovement for the given stock and/or order
     * @param stockId      the stock 
     * @param orderId      the order (optional to book only for the stock and order)
     * @param allowNegativ the qty of the stockItems can be negative
     */
    public Boolean bookMaterialWithAllowNegativ(Id stockId, Id orderId, Boolean allowNegativ)   
    {
        if (allowNegativ)
        {
            Boolean tmpAllowNegativStockQty = allowNegativStockQty;
            allowNegativStockQty = true;
            
            Boolean ret = bookMaterial(stockId, orderId);
            
            allowNegativStockQty = tmpAllowNegativStockQty;
            
            return ret;
        }
        
        return bookMaterial(stockId, orderId);
    }     
    
   /*
    * Books all material movements passed in to this method.
    * Use a query with the following fields to collect the material movements.
    * Please ensure to pass the status, too.
    * select Id, Name, Stock__c, Stock__r.Name, Article__c, Article__r.Name, Type__c, Status__c, Qty__c 
    *    from SCMaterialMovement__c where Status__c = \'' + SCfwConstants.DOMVAL_MATSTAT_BOOK + '\'' ....;
    *
    * This method is also used by the batch processing component SCboMaterialMovement.
    *
    * @param matMovements  List of material movements. All fields above are required. 
    * @return true success else false
    */
    public Boolean bookMaterialMovements(List<SCMaterialMovement__c> matMovements)
    {
                
        articleWithNegativStockQty.clear();
        
        // determine the relevant stocks and included aricles
        Set<Id> stockList = new Set<Id>();
        Set<Id> articleList = new Set<Id>();
        for (SCMaterialMovement__c matMove: matMovements)
        {
            articleList.add(matMove.Article__c);
            stockList.add(matMove.stock__c);
        } 
        
        // SOQL1: read all existing stock items for the relevant stocks / articles
        // stock -> article --> stockitem

        Map<String, SCStockItem__c> stockItems = new Map<String, SCStockItem__c>();
        
        // fill the map
        for (SCStockItem__c stockItem: [Select Name, Qty__c, Article__c, ValuationType__c, stock__c, stock__r.ValuationType__c
                                          from SCStockItem__c 
                                         where Stock__c in :stockList
                                           and Article__c in :articleList])
        {
            //GMSNA 2803.2013 do not modify the material movements !!!
            // if (stockItem.stock__r.ValuationType__c == false)
            // {
            //     stockItem.ValuationType__c = null;
            // }
            stockItems.put(getKey(stockItem), stockItem);
        } 
        
        List<SCMaterialMovement__c> delMatMoveList = new List<SCMaterialMovement__c>();
        List<SCMaterialMovement__c> updateMatMoveList = new List<SCMaterialMovement__c>();
        // iterate over all material movements
        for (SCMaterialMovement__c matMove: matMovements)
        {
            // System.debug('#### bookMaterial(): MatMove.Id            -> ' + matMove.Id);
            // System.debug('#### bookMaterial(): MatMove.Name          -> ' + matMove.Name);
            // System.debug('#### bookMaterial(): MatMove.Stock         -> ' + matMove.Stock__r.Name);
            // System.debug('#### bookMaterial(): MatMove.Article       -> ' + matMove.Article__r.Name);
            // System.debug('#### bookMaterial(): MatMove.ValuationType -> ' + matMove.ValuationType__c);
            // System.debug('#### bookMaterial(): MatMove.Type          -> ' + matMove.Type__c);
            // System.debug('#### bookMaterial(): MatMove.Quantity      -> ' + matMove.Qty__c);

            // only bookable material movements are allowed
            if(matMove.Status__c != SCfwConstants.DOMVAL_MATSTAT_BOOK)
            {    
                System.debug('#### bookMaterial(): MatMove.Id       -> ' + matMove.Id);
                System.debug('#### bookMaterial(): warning - skipping record as status is ' + 
                              matMove.Status__c + ' expected ' + SCfwConstants.DOMVAL_MATSTAT_BOOK);
                continue;
            }
            
            updateMatMoveList.add(matMove);
            //GMSNA 2803.2013 do not modify the material movements !!!
            // if (matMove.Stock__r.ValuationType__c == false)
            // {
            //     matMove.ValuationType__c = null;
            // }
        
            SCfwDomainValue domVal = domMatMove.getDomainValue(matMove.Type__c, true);
            String param = domVal.getControlParameter('V4');
            
            system.debug('#### controlParameter: ' + param);
            // Check if we have a stock item for the article in this stock
            SCStockItem__c si = stockItems.get(getKey(matMove));
            if(si == null)
            {
                // if the stock item does not exist - add a new one and set the quantity
                System.debug('#### bookMaterial(): StockItem not found - creating a new one');
                // create a new stock item object and fill it
                si = new SCStockItem__c();
                si.Article__c = matMove.Article__c;
                si.ValuationType__c = matMove.ValuationType__c;
                si.Stock__c = matMove.Stock__c;
                si.Qty__c = 0;
                // put the new object to the map
                stockItems.put(getKey(si), si);
            }
            //ET: PMS 33277
            //save qtyold only if param is in (1,2,3)
            if (param.equals('1') || param.equals('2') || param.equals('3') )
            {
                matMove.QtyOld__c = si.Qty__c;
            }
            
            // System.debug('#### bookMaterial(): StockItem                -> ' + si.id);
            // System.debug('#### bookMaterial(): StockItem.Name           -> ' + si.name);
            // System.debug('#### bookMaterial(): StockItem.Stock          -> ' + si.Stock__r.name);
            // System.debug('#### bookMaterial(): StockItem.Article        -> ' + si.Article__r.name);
            // System.debug('#### bookMaterial(): StockItem.ValuationType  -> ' + si.ValuationType__c);
            // System.debug('#### bookMaterial(): StockItem.Quantity (old) -> ' + si.Qty__c);

            // Evaluate the material movement type
            
            // 1 decrease the quantity
            if (param.equals('1'))
            {
                if (!allowNegativStockQty && ((si.Qty__c - matMove.Qty__c) < 0.0))
                {
                    if(si.ValuationType__c == null)
                    {
                        articleWithNegativStockQty.put(si.Article__c, matMove.Article__r.Name);
                    }
                    else
                    {
                        String value = '' + si.Article__c;
                        value += '-' + si.ValuationType__c;
                        articleWithNegativStockQty.put(value, matMove.Article__r.Name);
                    }
                    delMatMoveList.add(matMove);
                }
                else
                {
                    si.Qty__c -= matMove.Qty__c;
            
                    // Set the status of the material movement 
                    matMove.Status__c = SCfwConstants.DOMVAL_MATSTAT_BOOKED;
                }
            } 
            // 2 increase the quantity
            else if (param.equals('2'))
            {
                si.Qty__c += matMove.Qty__c;
            
                // Set the status of the material movement 
                matMove.Status__c = SCfwConstants.DOMVAL_MATSTAT_BOOKED;
            } 
            // 3 setting absolute value
            else if (param.equals('3'))
            {
                si.Qty__c = matMove.Qty__c;
            
                // Set the status of the material movement 
                matMove.Status__c = SCfwConstants.DOMVAL_MATSTAT_BOOKED;
            }
            
            //ET: PMS 33277
            //save qtynew only if param is in (1,2,3)
            if (param.equals('1') || param.equals('2') || param.equals('3') )
            {
                matMove.QtyNew__c = si.Qty__c; 
            }
           
            
            System.debug('#### bookMaterial(): StockItem.Quantity (new) -> ' + si.Qty__c);
        } // for (SCMaterialMovement__c..
 
        try
        {
            System.debug('#### bookMaterial(): not booked articles -> ' + articleWithNegativStockQty);
            
            // update or insert the stock items
            upsert stockItems.values();
            
            // update the status of the material movements
            //update matMovements;
            if (updateMatMoveList.size() > 0)
            {
                update updateMatMoveList;
            }
            //ET 13.12.2012
            //Post process material movements after update.
            //e.g.:     - receipt confirmation -> set delivery to deliverd
            //          - transfer (out) -> create transfer (in)  
            postProcess(matMovements);
            
            if (delMatMoveList.size() > 0)
            {
                delete delMatMoveList;
            } // if (delMatMoveList.size() > 0)
            
            return true;
        }
        catch (DmlException e)
        {
            if(Apexpages.currentPage() != null)
            {
                ApexPages.addMessages(e);
            }
            else
            {
                throw e;
            }
            //ET 19.12.2012. Don't add messages if not a VF Page
            //ApexPages.addMessages(e);
        }
        return false;
    } // bookMaterial
    
    
    /**
     * Post process material movements after update.
     * e.g.:    - receipt confirmation -> set delivery to deliverd
     *          - transfer (out) -> create transfer delivery  
     * 
     * @param updatedMaterialMovements the material movements to process
     *
     * @author Eugen Tiessen <etiessen@gms-online.de>
     */
    private void postProcess(List<SCMaterialMovement__c> updatedMaterialMovements)
    {
        system.debug('#### start post process...');
        
        List<SCMaterialMovement__c > movement2UpsertList = new List<SCMaterialMovement__c>();
        
        //unique list
        Set<Id> movementIDs2ConfirmSet = new Set<Id>();
        
        for(SCMaterialMovement__c movement : updatedMaterialMovements)
        {
            //Receipt confirmation
            //Source should be "mobile" 
            if((movement.Type__c == SCfwConstants.DOMVAL_MATMOVETYP_RECEIPT_CONFIRMATION || 
                movement.Type__c == SCfwConstants.DOMVAL_MATMOVETYP_TRANSFER_IN)
                && movement.Source__c == SOURCE_MOBILE
            )
            {
                //Check if head is set
                if(movement.Head__c != null)
                {
                    //Set parent material movements to delivered
                    //SCMaterialMovement__c confirmMovement = new SCMaterialMovement__c(Id = movement.Parent__c, Status__c = SCfwConstants.DOMVAL_MATSTAT_DELIVERED); 
                    //confirmMovement.Status__c = SCfwConstants.DOMVAL_MATSTAT_DELIVERED;
                    movementIDs2ConfirmSet.add(movement.Head__c);
                    system.debug('#### Movement: ' + movement.Head__c + ' should be set to delivered');
                }
                else
                {
                    system.debug('#### Movement: ' + movement + ' has no head to update');
                }
            }
            
            //Transfer out
            //Source should be "mobile" 
            else if(movement.Type__c == SCfwConstants.DOMVAL_MATMOVETYP_TRANSFER_OUT && movement.Source__c == SOURCE_MOBILE)
            {
                if(movement.DeliveryStock__c != null)
                {
                    //Create the transfer (in) material movement
                    SCMaterialMovement__c matMove = movement.clone(false);
                    matMove.DeliveryStock__c = null;
                    matMove.Stock__c = movement.DeliveryStock__c;
                    matMove.Type__c = SCfwConstants.DOMVAL_MATMOVETYP_TRANSFER_DELIVERY;
                    matMove.Status__c = SCfwConstants.DOMVAL_MATSTAT_IN_TRANSIT;
                    matMove.Head__c = (movement.Head__c != null) ? movement.Head__c : movement.Id;
                    matMove.QtyNew__c = null;
                    matMove.QtyOld__c = null;
                    matMove.Source__c = SOURCE_BACKEND;
                    movement2UpsertList.add(matMove);
                    system.debug('#### Movement: ' + matMove + ' created');
                }
                else
                {
                    system.debug('#### Movement: ' + movement + ' has no delivery stock for transfer (in) / transfer delivery');
                }
            }
            else
            {
                system.debug('#### Movement: ' + movement + ' has no post process requirements');
            }
        }
        //Add the material movements to update
        
        List<SCMaterialMovement__c> movement2ConfirmList = 
        [
            SELECT
                Id, Status__c
            FROM
                SCMaterialMovement__c
            WHERE
                (
                    Id IN :movementIDs2ConfirmSet
                    OR
                    Head__c IN :movementIDs2ConfirmSet
                )
                AND
                Status__c =: SCfwConstants.DOMVAL_MATSTAT_IN_TRANSIT
        ];
        
        for(SCMaterialMovement__c mov : movement2ConfirmList)
        {
            mov.Status__c = SCfwConstants.DOMVAL_MATSTAT_DELIVERED;
        }
         
        movement2UpsertList.addAll(movement2ConfirmList);
        
        upsert movement2UpsertList;
        //book material movements after upsert generated material movements
        /*if(movements2ConfirmList.size() > 0)
        {
            bookMaterialMovements(movements2ConfirmList);
        }*/
    }
    
    
   /*
    * Calculates the stock item map key from the stock and article id
    * @param i the stock item
    * @return the stockid + '-' + articleid + '-' + valuationType
    */
    private String getKey(SCStockItem__c i)
    {
        return i.Stock__c + '-' + i.Article__c + '-' + (i.ValuationType__c != null ? i.ValuationType__c : '');
    }  

   /*
    * Calculates the stock item map key from the material movement
    * @param i material movement
    * @return the stockid + '-' + articleid + '-' + valuationType
    */
    private String getKey(SCMaterialMovement__c i)
    {
        return i.Stock__c + '-' + i.Article__c + '-' + (i.ValuationType__c != null ? i.ValuationType__c : '');
    }  
} // SCboMaterialMovement
