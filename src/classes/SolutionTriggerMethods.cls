global with sharing class SolutionTriggerMethods{
	
	public static void doBeforeUpdateInsert(List<Solution> listSolution) { 
		// clear AssignedCategories__c 
		for (Solution sol : listSolution ) {
			sol.AssignedCategories__c = '';
		}
		for (CategoryData CatData :[SELECT RelatedSobjectId, CategoryNodeId 
	                               FROM CategoryData 
	                               WHERE RelatedSobjectId IN : listSolution]) {
			// take CategoryNode from CatData                                   
			CategoryNode CatNode = [SELECT MasterLabel, Id
	                               FROM CategoryNode 
	                               WHERE Id=:CatData.CategoryNodeId];
			CategoryNodeLocalization []CatNodeLocList = [SELECT Language, Value, CategoryNodeId
	                               FROM CategoryNodeLocalization 
	                               WHERE CategoryNodeId=:CatData.CategoryNodeId];
			// Create the string. Therefore also check CategoryNodeLocalization for Translations.
			String AssignedCatString = CatNode.Id + ' ';
			String CatNodeValue = '';
			for (CategoryNodeLocalization CatNodeLoc :CatNodeLocList) {
				if (CatNodeLoc.Language == 'nl_NL') {
					CatNodeValue = CatNodeLoc.Value;
					break ;
				}
			}
			if (CatNodeValue == '') {
				AssignedCatString = AssignedCatString + CatNode.Masterlabel;
			}
			else {
				AssignedCatString = AssignedCatString + CatNodeValue;
			}
			// Write  AssignedCatString in AssignedCategories__c for the right Solution 			
			//Solution sol = Trigger.newMap.get(CatData.RelatedSObjectId);
			Solution sol = null;
			for (Solution s: listSolution)
				if (s.Id == CatData.RelatedSobjectId)
					sol = s;
			
			if (sol.AssignedCategories__c == '') {
				sol.AssignedCategories__c = AssignedCatString;
			}
			else {
				sol.AssignedCategories__c = sol.AssignedCategories__c + ','+ AssignedCatString;
			}
		}
	}
}
