/*
 * @(#)AseRightsTest.cls SCCloud    hs 17.Jun.2010
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author HS <hschroeder@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class AseRightsTest 
{
    /**
     * Class under test
     */
    static testMethod void testAseRight()
    {
        // get current rights
        String access = AseRights.getAccess();
    }
}
