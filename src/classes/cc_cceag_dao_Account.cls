public virtual without sharing class cc_cceag_dao_Account {
	
	public static List<CCAccountRole__c> fetchRelatedAccounts(String accountId, String relationship) {
		return [SELECT 
			SlaveAccount__c,
			SlaveAccount__r.Name, 
			SlaveAccount__r.AccountNumber,
			SlaveAccount__r.BillingHouseNo__c,
			SlaveAccount__r.BillingStreet,
			SlaveAccount__r.BillingCity,
			SlaveAccount__r.BillingState,
			SlaveAccount__r.BillingPostalCode,
			SlaveAccount__r.BillingCountry,
			SlaveAccount__r.BillingAddress__c,
			SlaveAccount__r.DeliveringPlant__c,
			SlaveAccount__r.ShippingTerms__c,
			SlaveAccount__r.NextPossibleDeliveryDate__c,
			SlaveAccount__r.DeliveryWeekdays__c
		FROM
			CCAccountRole__c
		WHERE
			AccountRole__c =: relationship
			AND MasterAccount__c =: accountId
		ORDER BY
			SlaveAccount__r.AccountNumber,
			SlaveAccount__r.Name,
			SlaveAccount__r.BillingStreet,
			SlaveAccount__r.BillingPostalCode,
			SlaveAccount__r.BillingCity];
	}

	public static String fetchAccountId(String userId) {
		List<User> userData = [Select u.UserType, u.Contact.AccountId From User u where u.id = :userId];
		system.debug('userId=' + userId);
		system.debug('userData=' + userData);
		if (userData != null && userData.size() > 0) {
			system.debug('userData contact Id=' + userData[0].Contact);
			system.debug('userData account Id=' + userData[0].Contact.AccountId); 
			return userData[0].Contact.AccountId;
		} else
			return null;
	}

	public static Id fetchUserContactId(String userId) {
		User userData = fetchUser(userId);
		if (userData != null)
			return userData.ContactId;
		else
			return null;
	}

	public static User fetchUser(String userId) {
		List<User> users = [SELECT u.Username, u.UserType, u.UserRoleId, u.Profile.UserLicenseId, u.Profile.Name, u.Profile.Id, 
				u.Email, u.Phone, u.ProfileId, u.Name, u.LanguageLocaleKey, u.LocaleSIDKey, u.Id, u.Contact.Name, 
				u.Contact.FirstName, u.Contact.LastName, u.Contact.Phone, u.Contact.Email, u.ContactId, u.CompanyName, u.AccountId FROM User u WHERE u.id = :userId];
		if (users.size() > 0)
			return users[0];
		return null;
	}

	public static SCAccountInfo__c fetchSCAccountInfo(String shipTo) {
		return fetchSCAccountInfo(shipTo, true);
	}

	public static SCAccountInfo__c fetchSCAccountInfo(String shipTo, boolean filterType) {
		try {
			if (filterType)
				return [SELECT Account__c, Account__r.AccountNumber , CustomerType__c, CustomerPaymentType__c, DistributionChannel__c, Status__c from SCAccountInfo__c WHERE Account__c =: shipTo AND DistributionChannel__c= 'Z1' AND CustomerType__c IN ('14', '15') LIMIT 1 ];
			else
				return [SELECT Account__c, Account__r.AccountNumber , CustomerType__c, CustomerPaymentType__c, DistributionChannel__c, Status__c from SCAccountInfo__c WHERE Account__c =: shipTo AND DistributionChannel__c= 'Z1' LIMIT 1 ];
		}
		catch (Exception e) {
			return null;
		}
	}

	public static SCPlant__c fetchPlant(String plantId) {
		return [SELECT
			Name__c,
			Street__c,
			ZipCode__c,
			City__c
		FROM
			SCPlant__c
		WHERE
			Name =: plantId
		];
	}
	
	public static List<Map<String,Object>> getPromotedProductsForAccount(String accountId)
	{
		List<Map<String,Object>> result = new List<Map<String,Object>>();
		Account account = [SELECT Subtradechannel__c FROM Account WHERE Id=:accountId];
        string stc = account.Subtradechannel__c==null ? '' : account.Subtradechannel__c;
        
        // padding stc with leading zeros.
		while (stc.length()<3) stc = '0'+stc;

		System.debug('fetching stcproducts for account='+accountId+'/stc='+stc);		

		// fetching SubtradechannelProducts
		List<SubtradechannelProduct__c> stcProds = [SELECT Product2__r.ID2__c FROM SubtradechannelProduct__c WHERE Subtradechannel__r.Name=:stc ORDER BY PromotedInEcom__c DESC,Sequence__c ASC LIMIT 5];
		System.debug(stcProds);
		List<String> matNumbers = new List<String>();
		for (SubtradechannelProduct__c stcProd : stcProds) {
			matNumbers.add(stcProd.Product2__r.ID2__c);
		}
		System.debug(matNumbers);

		// fetching the ccProducts
		List<ccrz__E_Product__c> ccProducts = [SELECT ccrz__SKU__c,Id,Name FROM ccrz__E_Product__c WHERE ccrz__ProductId__c IN :matNumbers]; 
		System.debug(ccProducts);
		
		List<Id> ccProductIds = new List<Id>();
		Map<String,ccrz__E_Product__c> ccProductsByMatNr = new Map<String,ccrz__E_Product__c>();
		for (ccrz__E_Product__c p : ccProducts) {
			ccProductsByMatNr.put(p.ccrz__SKU__c,p);
			ccProductIds.add(p.Id);
		}
		System.debug(ccProductsByMatNr);
				
		// fetch images
		List<ccrz__E_ProductMedia__c> productImages = [SELECT Id,ccrz__Product__c, ccrz__URI__c, ccrz__MediaType__c, ccrz__ProductMediaSource__c FROM ccrz__E_ProductMedia__c WHERE ccrz__MediaType__c='Product Search Image' AND ccrz__Product__c IN :ccProductIds ORDER BY ccrz__Product__c,ccrz__Sequence__c];
		System.debug(productImages);			
		Map<Id,Id> mediaToProduct = new Map<Id,Id>();
		Map<Id,String> imageById = new Map<Id,String>();
		List<Id> pmAttachmentIds = new List<Id>(); 
		for (ccrz__E_ProductMedia__c pm : productImages) {
			String imgUrl = null;
			if (pm.ccrz__ProductMediaSource__c=='URI') {
				imgUrl = pm.ccrz__URI__c;
			}
			if (pm.ccrz__ProductMediaSource__c=='Attachment'){
				pmAttachmentIds.add(pm.Id);
				mediaToProduct.put(pm.id,pm.ccrz__Product__c);
			}
			if (imgUrl!=null) {
				imageById.put(pm.ccrz__Product__c,imgUrl);
			}
		}
		// attachment medias
		if (pmAttachmentIds.size()>0) {
			List<Attachment> pmAttachments = [SELECT Id, ParentId FROM Attachment WHERE ParentId IN :pmAttachmentIds];
			System.debug(pmAttachments);	
			for (Attachment a : pmAttachments) {
				String imgUrl = Site.getCurrentSiteUrl()+'servlet/servlet.FileDownload?file='+a.Id;
				imageById.put(mediaToProduct.get(a.ParentId),imgUrl);
			}
		}
		System.debug(imageById);
			
		// now merge all informations for returning					
		String productDetailPageBaseUrl = Site.getCurrentSiteUrl()+Page.ccrz__ProductDetails.getUrl().Substring(1);
		for (String matNr : matNumbers) {
			Map<String,Object> p = new Map<String,Object>();
			ccrz__E_Product__c prod = ccProductsByMatNr.get(matNr);
			if (prod==null) continue;
			String imgUrl = imageById.get(prod.Id);
			
			p.put('name',prod.Name);
			p.put('id',prod.Id);	
			p.put('detailUrl',productDetailPageBaseUrl+'?sku='+prod.ccrz__SKU__c);
			p.put('imageUrl',imgUrl);		
			result.add(p);
		}
		System.debug(result);
				
		return result;
	}
    
    /**
     * gets the soldto account of an account - if account is already a soldto - it will be returned itself!
	 */
    public static Account fetchSoldToAccount(Account account)
    {
        if (isAGAccount(account.Id)) {
            System.debug('given account '+account.Id+' is already a soldto!');
            return account;
        }
        if (isWEAccount(account.Id)) {
            System.debug('given account '+account.Id+' is a shipto');
        	CCAccountRole__c role = [SELECT Id, MasterAccount__c FROM CCAccountRole__c WHERE AccountRole__c='WE' AND SlaveAccount__c=:account.Id AND MasterAccount__c<>:account.Id];
            List<Account> accounts = [SELECT Id,Name,BillingAddress__c,AccountNumber FROM Account WHERE Id=:role.MasterAccount__c];
            return accounts[0];
        }
        System.debug('given account isnt a soldto or shipto - this is not supported yet!');
     	return null;   
    }
    /**
     * gets the invoicerecipient (by priority: first RE, then RG) of an account (which can be a shipto or soldto)
     */
    public static Account fetchInvoiceRecipientAccount(Account account)
    {
        Account soldTo = fetchSoldToAccount(account);
        if (soldTo==null) {
            System.debug('no soldto found for '+account.Id+' - so no invoice recipient available!');
            return null;
        }
        
		List<CCAccountRole__c> roles = [SELECT Id, SlaveAccount__c FROM CCAccountRole__c WHERE AccountRole__c='RE' AND MasterAccount__c=:soldTo.Id];
        if (roles.size()==0) {
            System.debug('no RE role found for account '+soldTo.Id+' - searching for RG!');
            roles = [SELECT Id, SlaveAccount__c FROM CCAccountRole__c WHERE AccountRole__c='RG' AND MasterAccount__c=:soldTo.Id];
            if (roles.size()==0) {
                System.debug('no RG role found for account '+soldTo.Id+' - so no invoice recipient found');
                return null;
            }
        }
        
        CCAccountRole__c role = roles[0];
        List<Account> accounts = [SELECT Id,Name,BillingAddress__c,AccountNumber FROM Account WHERE Id=:role.SlaveAccount__c];
        return accounts[0];
    }
    
	public static Boolean isWEAccount(Id accountId)
	{
		List<CCAccountRole__c> roles = [SELECT Id FROM CCAccountRole__c WHERE AccountRole__c='WE' AND SlaveAccount__c=:accountId AND MasterAccount__c<>:accountId];
		return roles.size()>0;
	}  
    // SoldTO = AG - if current account is master=slave with role=AG
    public static Boolean isAGAccount(Id accountId)
	{
		List<CCAccountRole__c> roles = [SELECT Id FROM CCAccountRole__c WHERE AccountRole__c='AG' AND SlaveAccount__c=:accountId AND MasterAccount__c=:accountId];
		return roles.size()>0;
	}   
    
    /**
     * loads given fields (seperated by ,) into the given account
     */
    public static void ensureFieldsForAccount(Account account, String fieldNames)
    {
    	List<Account> accounts = new List<Account>();
        accounts.add(account);
        ensureFieldsForAccounts(accounts, fieldNames);
    }
    
    /**
     * loads given fields (seperated by ,) into the given accounts
     */
    public static void ensureFieldsForAccounts(List<Account> accounts, String fieldNames)
    {	
        // building query
		List<String> fields = fieldNames.split(',');
        System.debug('FIELDS TO ENSURE:');
        System.debug(fields);
        System.debug('FOR ACCOUNTS: ');
        System.debug(accounts);
        
		String idsStr = '';
        for (Account account : accounts) {
            if (idsStr.length()>0) idsStr = idsStr + ',';
            idsStr = idsStr + '\'' + account.Id + '\'';
        }

        String query = 'SELECT Id';
        for (String fieldName : fields) {
            query = query + ', ' + fieldName;
        }
        query = query + ' FROM Account WHERE Id IN ('+idsStr+')';
        
        // getting requested fields...
        System.debug('Query: '+query);
        Map<Id,SObject> objectsById = new Map<Id,SObject>(Database.query(query));

        // filling new fields into "old" objects...
        for (Account account : accounts) {
            SObject source = objectsById.get(account.Id);
            for (String fieldName : fields) {
                account.put(fieldName,source.get(fieldName));
            }
        }
    }

    
}
