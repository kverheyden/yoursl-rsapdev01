/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class cc_cceag_bean_test_Cart {
    
    static testMethod void myUnitTest() {
        ccrz__E_ContactAddr__c billTo = new ccrz__E_ContactAddr__c(
            ccrz__AddressFirstline__c='100 Pine Street', ccrz__City__c='SmallTown', ccrz__DaytimePhone__c='(847) 555-1212',
            ccrz__FirstName__c='Test', ccrz__HomePhone__c='(847) 555-1212', ccrz__LastName__c='User',
            ccrz__PostalCode__c='60601', ccrz__State__c='Idaho', ccrz__StateISOCode__c='ID',
            ccrz__CountryISOCode__c='USA');
        ccrz__E_ContactAddr__c shipTo = new ccrz__E_ContactAddr__c(
            ccrz__AddressFirstline__c='100 Pine Street', ccrz__City__c='SmallTown', ccrz__DaytimePhone__c='(847) 555-1212',
            ccrz__FirstName__c='Craig', ccrz__HomePhone__c='(847) 555-1212', ccrz__LastName__c='Traxler',
            ccrz__PostalCode__c='60601', ccrz__State__c='Idaho', ccrz__StateISOCode__c='ID',
            ccrz__CountryISOCode__c='USA');
        insert new List<ccrz__E_ContactAddr__c> {billTo, shipTo};
        String testName = 'testcart';
        String testEmail = 'myemail@test.com';
        String testFirstName = 'Test';
        String testLastName = 'User';
        String testCompanyName = 'MyCo';
        String testPhone = '(800) 555-1111';
        String testMobilePhone = '(800) 555-2222';
        String testPaymentMethod = 'PO';
        String testPONumber = '12345';
        
        ccrz__E_Cart__c cart = new ccrz__E_Cart__c(
            ccrz__Name__c = testName,
            //ccrz__Contact__c = testUser.ContactId,
            ccrz__BillTo__c = billTo.Id,
            ccrz__ShipTo__c = shipTo.Id,
            ccrz__BuyerEmail__c = testEmail,
            ccrz__BuyerFirstName__c = testFirstName,
            ccrz__BuyerLastName__c = testLastName,
            ccrz__BuyerCompanyName__c = testCompanyName,
            ccrz__BuyerPhone__c = testPhone,
            ccrz__BuyerMobilePhone__c = testMobilePhone,
            ccrz__PaymentMethod__c = testPaymentMethod,
            ccrz__PONumber__c = testPONumber,
            ccrz__SessionID__c = 'test session',
            ccrz__storefront__c='DefaultStore',
            ccrz__RequestDate__c = Date.today()
        );
        
        insert cart;
        Test.startTest();
        ccrz__E_Cart__c testCart = [Select c.ccrz__ShipTo__r.ccrz__State__c, c.ccrz__ShipTo__r.ccrz__StateISOCode__c, 
            c.ccrz__ShipTo__r.ccrz__ShippingComments__c, c.ccrz__ShipTo__r.ccrz__PostalCode__c, 
            c.ccrz__ShipTo__r.ccrz__Partner_Id__c, c.ccrz__ShipTo__r.ccrz__Country__c, 
            c.ccrz__ShipTo__r.ccrz__CountryISOCode__c, c.ccrz__ShipTo__r.ccrz__CompanyName__c, 
            c.ccrz__ShipTo__r.ccrz__City__c, c.ccrz__ShipTo__r.ccrz__AddressFirstline__c, 
            c.ccrz__ShipTo__r.Id, c.ccrz__ShipTo__c, c.ccrz__ShipMethod__c, c.ccrz__ShipAmount__c, 
            c.ccrz__RequestDate__c, c.ccrz__EncryptedId__c, c.ccrz__CurrencyISOCode__c, c.ccrz__BillTo__r.ccrz__State__c, 
            c.ccrz__BillTo__r.ccrz__StateISOCode__c, c.ccrz__BillTo__r.ccrz__PostalCode__c, 
            c.ccrz__BillTo__r.ccrz__Partner_Id__c, c.ccrz__BillTo__r.ccrz__Country__c, 
            c.ccrz__BillTo__r.ccrz__CountryISOCode__c, c.ccrz__BillTo__r.ccrz__CompanyName__c, 
            c.ccrz__BillTo__r.ccrz__City__c, c.ccrz__BillTo__r.ccrz__AddressFirstline__c, c.ccrz__BillTo__r.Id, 
            c.ccrz__BillTo__c, c.Name, c.Id, c.ccrz__ShipComplete__c From ccrz__E_Cart__c c where id = :cart.id];
        
        cc_cceag_bean_Cart defaultCartBean = new cc_cceag_bean_Cart();
        cc_cceag_bean_Cart cartBean = new cc_cceag_bean_Cart(testCart);
        System.assertEquals(testCart.Id, cartBean.sfid);
        System.assertEquals(testCart.name, cartBean.name);
        Test.stopTest();
    }
}
