@isTest
private class CCWSAddContentVersionToPromotionTest {
	static testMethod void CCWSAddContentVersionToPromotionUnitTest(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
      	User u 					= new User();
      	u.Alias 				= 'standt';
      	u.Email					= 'standarduser@testorg.com'; 
      	u.EmailEncodingKey		= 'UTF-8';
      	u.LastName				= 'Testing';
      	u.LanguageLocaleKey		= 'en_US'; 
      	u.LocaleSidKey			= 'en_US';
      	u.ProfileId 			= p.Id; 
      	u.TimeZoneSidKey		= 'America/Los_Angeles';
      	u.UserName				= 'yoursl_standarduser@cceag.com';
        u.ID2__c				= '1234';
        
      	System.runAs(u){
            Promotion__c promo		= new Promotion__c();
            promo.Name				= 'Testpromotion';
            promo.PromotionID__c	= '54321';
            insert promo;
            
            Blob bl = Blob.valueOf('TestBlob');
            String docName = 'DocName';
            
            //Case with invalid user id2__c
        	CCWSAddContentVersionToPromotion.addFeed('123','Promotion','54321',u.Id2__c, docName, bl);
            List<FeedItem> feedsAssert1 = [Select ID from FeedItem where ParentID = :promo.ID];
            system.assertEquals(0,feedsAssert1.size());
            List<ContentVersion> contentsAssert1 = [Select ID from ContentVersion where OwnerID =: u.ID];
            system.assertEquals(0,contentsAssert1.size());
            
            //Case with invalid promotionID__c
            CCWSAddContentVersionToPromotion.addFeed('1234','Promotion','54',u.Id2__c, docName, bl);
            List<FeedItem> feedsAssert2 = [Select ID from FeedItem where ParentID = :promo.ID];
            system.assertEquals(0,feedsAssert2.size());
            List<ContentVersion> contentsAssert2 = [Select ID from ContentVersion where OwnerID =: u.ID];
            system.assertEquals(0,contentsAssert2.size());
            
            //Case with two promotions with the same ID
            Promotion__c promo2		= new Promotion__c();
            promo2.Name				= 'Testpromotion2';
            promo2.PromotionID__c	= '54321';
            insert promo2;
            CCWSAddContentVersionToPromotion.addFeed('1234','Promotion','54321',u.Id2__c, docName, bl);
            delete promo2;
            List<FeedItem> feedsAssert3 = [Select ID from FeedItem where ParentID = :promo.ID];
            system.assertEquals(0,feedsAssert3.size());
            List<ContentVersion> contentsAssert3 = [Select ID from ContentVersion where OwnerID =: u.ID];
            system.assertEquals(0,contentsAssert3.size());
            
            //Case where ParentType is not a promotion
            CCWSAddContentVersionToPromotion.addFeed('1234','Account','54321',u.Id2__c, docName, bl);
            List<FeedItem> feedsAssert4 = [Select ID from FeedItem where ParentID = :promo.ID];
            system.assertEquals(0,feedsAssert4.size());
            List<ContentVersion> contentsAssert4 = [Select ID from ContentVersion where OwnerID =: u.ID];
            system.assertEquals(0,contentsAssert4.size());
            
            //Case where doc name is not provided
            CCWSAddContentVersionToPromotion.addFeed('1234','Promotion','54321',u.Id2__c, '' , bl);
            List<FeedItem> feedsAssert5 = [Select ID from FeedItem where ParentID = :promo.ID];
            system.assertEquals(0,feedsAssert5.size());
            List<ContentVersion> contentsAssert5 = [Select ID from ContentVersion where OwnerID =: u.ID];
            system.assertEquals(0,contentsAssert5.size());
            
            //Case where body is null
            CCWSAddContentVersionToPromotion.addFeed('1234','Promotion','54321',u.Id2__c, docName , null);
            List<FeedItem> feedsAssert6 = [Select ID from FeedItem where ParentID = :promo.ID];
            system.assertEquals(0,feedsAssert6.size());
            List<ContentVersion> contentsAssert6 = [Select ID from ContentVersion where OwnerID =: u.ID];
            system.assertEquals(0,contentsAssert6.size());
            
            //Proper test
            CCWSAddContentVersionToPromotion.addFeed('1234','Promotion','54321',u.Id2__c, docName , bl);
            List<FeedItem> feedsAssert7 = [Select ID from FeedItem where ParentID = :promo.ID];
            system.assertEquals(1,feedsAssert7.size());
            List<ContentVersion> contentsAssert7 = [Select ID from ContentVersion where OwnerID =: u.ID];
            system.assertEquals(1,contentsAssert7.size());
        }
    }
}
