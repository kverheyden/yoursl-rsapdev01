/**
 * @(#)SCReleaseJob.cls    ASE1.0 hs 12.10.2010
 * 
 * Copyright (c) 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 * 
 * $Id: SCReleaseJob.cls 7692 2010-01-18 16:18:41Z hschroeder $
 * 
 */
global class SCReleaseJob implements Schedulable
{
    // job session
    private String sessionID = 'gwin-0';

    /**
     * job parameters read from custom settings
     */
    SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
     
    private Integer daysToRelease = appSettings.ASE_RELEASEJOB_DAYSTORELEASE__c.intValue();
    private Integer daysToPreview = appSettings.ASE_RELEASEJOB_DAYSTOPREVIEW__c.intValue();
    private String department = appSettings.ASE_RELEASEJOB_DEPARTMENT__c;
    private String calendar = appSettings.ASE_RELEASEJOB_CALENDAR__c;
    private Boolean setLocks = appSettings.ASE_RELEASEJOB_SETLOCKS__c;
    private String ordertypes = appSettings.ASE_RELEASEJOB_ORDERTYPES__c;
         
    /**
     * Context meta data
     *
     * @author HS <hschroeder@gms-online.de>
     */
    private String[] metaContext = new String[]
    {
        'id',
        'userid'
    };
    
    /**
     * Job param meta data
     *
     * @author HS <hschroeder@gms-online.de>
     */
    private String[] metaParam = new String[]
    {
        'id',
        'daystorelease',
        'daystopreview',
        'department',
        'calendar', 
        'setlocks', 
        'ordertypes' 
    };
    
    global void execute(SchedulableContext context)
    {
        System.debug('SCReleaseJob context: ' + context);
        callout();
    }
    
    /**
     * Make the webservice call out
     *
     * @author HS <hschroeder@gms-online.de>
     */
     @Future(callout=true)
    public static void callout()
    {
        SCReleaseJob job = new SCReleaseJob();
        
        String tenant = UserInfo.getOrganizationID().toLowerCase();

        AseService.aseDataEntry dataEntry = new AseService.aseDataEntry();
        AseService.aseKeyValueType keyValue = new AseService.aseKeyValueType();
        
        // prepare context
        AseService.aseDataType contextParam = new AseService.aseDataType();
        dataEntry = new AseService.aseDataEntry();
        dataEntry.keyValues = new AseService.aseKeyValueType[0]; 

        for (String key : job.metaContext)
        {
            keyValue = new AseService.aseKeyValueType();
            keyValue.key = key;
            keyValue.value = job.getValue(key) != null ? job.getValue(key) : '';
            System.debug('Context: ' + key + ' = ' + keyValue.value);
            dataEntry.keyValues.add(keyValue);
        }

        contextParam.dataEntries = new AseService.aseDataEntry[0];
        contextParam.dataEntries.add(dataEntry);
        contextParam.type_x = AseCalloutConstants.ASE_TYPE_CONTEXT_STR;
        
        // job params
        AseService.aseDataType jobParam = new AseService.aseDataType();
        dataEntry = new AseService.aseDataEntry();
        dataEntry.keyValues = new AseService.aseKeyValueType[0]; 

        for (String key : job.metaParam)
        {
            keyValue = new AseService.aseKeyValueType();
            keyValue.key = key;
            keyValue.value = job.getValue(key) != null ? job.getValue(key) : '';
            System.debug('Job param: ' + key + ' = ' + keyValue.value);
            dataEntry.keyValues.add(keyValue);
        }

        jobParam.dataEntries = new AseService.aseDataEntry[0];
        jobParam.dataEntries.add(dataEntry);
        jobParam.type_x = AseCalloutConstants.ASE_TYPE_RELJOB_STR;
        
        AseService.aseDataType[] params = new AseService.aseDataType[]
        { 
            contextParam, jobParam
        };
        
        AseService.aseSOAP aseSoap = AseCalloutUtils.getAseSoap();
        aseSoap.endpoint_x = AseCalloutConstants.ENDPOINT;
        aseSoap.set(tenant, params);
    }
    
    /**
     * Returns the calendar id of the specified calendar
     *
     * @param calendar the name of the calendar
     * @return the calendar id of the specified calendar
     * @author HS <hschroeder@gms-online.de>
     */
    public String getCalendarID(String calendarName)
    {
        SCCalendar__c calendar = [ select id from SCCalendar__c where name = :calendarName limit 1 ];
        return calendar.id;
    }
    
    /**
     * Initialize the demand with the current user
     *
     * @author HS <hschroeder@gms-online.de>
     */
    private String getUserID()
    {
        String userID = UserInfo.getUserId();
        User user = [ select id, alias from user where user.id = :userID limit 1 ];
        return user.alias;
    }
   
    /**
     * Get a named value
     *
     * @param key the attribute name
     * @return the translated value
     * @author HS <hschroeder@gms-online.de>
     */
    public String getValue(String key)
    {
        // meta key
        if (key == 'id') return sessionID;
        if (key == 'userid') return getUserID();

        // param key        
        if (key == 'daystorelease') return '' + daysToRelease;
        if (key == 'daystopreview') return '' + daysToPreview;
        if (key == 'department') return department;
        if (key == 'calendar') return getCalendarID(calendar);
        if (key == 'setlocks') return setLocks ? '1' : '0';
        if (key == 'ordertypes') return ordertypes;
      
        return null;
    }         
}
