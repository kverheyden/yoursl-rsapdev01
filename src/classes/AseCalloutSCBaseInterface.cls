/**
 * AseCalloutSCBaseInterface.cls	mt	28.09.2009
 * 
 * Copyright (c) 2009 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 /**
 * AseCalloutSCBaseInterface is the interface which all classes
 * has to implement when they are used by the AseServiceManager
 * 
 * @author mt
 * @version $Revision$, $Date$
 */
public interface AseCalloutSCBaseInterface {

 	/**
	 * sets the flag isTest
	 * 
	 * @param isTest the value to be set
	 */
	void setIsTest(boolean isTest);


 	/**
	 * @return true if the class is called in test-mode, false otherwise
	 */
	boolean getIsTest();

	/**
	 * add the types which has to be removed to the given array
	 *
	 * @param strDataTypes the array to which the types to be
	 *        removed were added
	 */
    void addRemoveAllTypes(String[] strDataTypes);

	/**
	 * this method adds the parameter strings (these are the keys of the records
	 * wichig should be selected) to the aseDataType-Array
	 * 
	 * @param dTypes the array with the previous datatypes
	 * @param paras the keys as String-Array
	 */
    void addSetAseDataTypes(AseService.aseDataType[] dTypes, String[] parameter);

	/**
	 * this method queries the ASEWebservice fro all data for the
	 * intDataTypes an print them on the DebugConsole
	 * 
	 * @param intDataTypes the dataTypes which should be printed
	 */
    void addPrintDebugGetAllTypes(integer[] intDataTypes);
  
}
