public with sharing class SCInstalledBaseOrderHandler 
{
 	Map<id, SCInstalledBase__c> g_newlistSCInstalledBase;
 	Map<id, SCOrderItem__c> g_OrderItemMap;
 	Map<id, SCOrder__c> g_OrderMap;
  
   /*
    * Look up additional fields from equipment and after sorting them for being moved to refurbishment stock,
    * look for the orderitems and the connected order, that is external ready
    * @param oldlistSCInstalledBase & newlistSCInstalledBase	NEW and OLD Map from Installbase (Equipment) Trigger
  	*/
	public void CheckForAppropriateRecords(Map<id, SCInstalledBase__c> oldlistSCInstalledBase, Map<id, SCInstalledBase__c> newlistSCInstalledBase)
	{
 		g_newlistSCInstalledBase = newlistSCInstalledBase; 
 		
 		Map<id, SCInstalledBase__c> enrichedEquips = new Map<id, SCInstalledBase__c>();
 		 		
 		for(SCInstalledBase__c enrEq : [SELECT id, Stock__c, Stock__r.Type__c FROM SCInstalledBase__c WHERE id IN :newlistSCInstalledBase.keyset()])
 		{
 			enrichedEquips.put(enrEq.id, enrEq);		
 		}
 		
 		Set<id> matchEqIds = new Set<id>();
 		
 		for(SCInstalledBase__c eq : enrichedEquips.values())
 		{
 			System.debug('###SCInstalledBaseOrderHandler - CheckForAppropriateRecords - NewStock: ' + eq.Stock__r.Type__c);
 			if(eq.Stock__r.Type__c == 'EQReadyForRefurbishment' && oldlistSCInstalledBase.get(eq.id).Stock__r.Type__c != 'EQReadyForRefurbishment')
 			{
 				System.debug('###SCInstalledBaseOrderHandler - CheckForAppropriateRecords - IT\'S A MATCH');
 				matchEqIds.add(eq.id);	
 			}
 		}
		
		Set<id> extRdyOrders = new Set<id>();
		
		Map<id, id>OrderEquipMap = new Map<id, id>();
		g_OrderItemMap = new Map<id, SCOrderItem__c>();
		
		
 		for(SCOrderItem__c oi : [SELECT id, InstalledBase__c, Order__c, Order__r.Type__c, Order__r.roleSR__c ,Order__r.ExternalReady__c FROM SCOrderItem__c WHERE InstalledBase__c IN :matchEqIds])
 		{
 			System.debug('###SCInstalledBaseOrderHandler - CheckForAppropriateRecords - oi OrderType: ' + oi.Order__r.Type__c);
 			
 			if(oi.Order__r.ExternalReady__c)
 			{
 				extRdyOrders.add(oi.Order__c);
 				OrderEquipMap.put(oi.Order__c, oi.InstalledBase__c);
 				g_OrderItemMap.put(oi.Order__c, oi);	
 			}

 		}
 		
 		System.debug('###SCInstalledBaseOrderHandler - CheckForAppropriateRecords - extRdyOrders: ' + extRdyOrders);

		List<SCOrder__c> orderList;

 		String query = getQuery('SCOrder__c', getFieldList('SCOrder__c'));
 		
		query += 'id IN :extRdyOrders';
		
		try
		{
			orderList = Database.query(query);	
		}
		catch(exception e){}

 		g_OrderMap = new Map<id, SCOrder__c>();
 		
 		if(orderList != null && !orderList.isEmpty())
 		{
 	 		for(SCOrder__c anOrder : orderList)
	 		{
	 			System.debug('###SCInstalledBaseOrderHandler - CheckForAppropriateRecords - anOrder: ' + anOrder);
				g_OrderMap.put(anOrder.id, anOrder);
	
	 		}			
 		}
		
		
		CreateRefurbishmentOrder(OrderEquipMap);		
		
	}
	
   /*
    * Create the follow up orders based on equipment moved to refurbishment stock
    * and the connected order being closed
    * @param OrderEquipMap	Map of orderID and EquipID
  	*/
	public void CreateRefurbishmentOrder(Map<id, id>OrderEquipMap)
	{
		List<SCOrder__c> 						insertOrders 			= new List<SCOrder__c>();
		List<SCOrderItem__c> 					insertOrderItems 		= new List<SCOrderItem__c>();
		List<SCOrderExternalAssignment__c> 		xAssignmentList 		= new List<SCOrderExternalAssignment__c>();
		List<SCOrderExternalAssignmentItem__c> 	xAssignmentItemList 	= new List<SCOrderExternalAssignmentItem__c>();
		
		Map<SCOrder__c, id> New2OldOrderMap	= new Map<SCOrder__c, id>(); 
		
		SCOrder__c newOrder;
		SCOrderItem__c dataObject;
		
		for(id orderID : OrderEquipMap.keyset())
		{
			newOrder = g_OrderMap.get(orderID).clone(false, true, false, false);
			//newOrder = new SCOrder__c();  
			newOrder.Description__c = 'TRIGGER TEST';
			newOrder.Type__c = '5702';
			insertOrders.add(newOrder);
			New2OldOrderMap.put(newOrder, orderID);
			 	
		}
		
		insert insertOrders;
		System.debug('###SCInstalledBaseOrderHandler - CreateRefurbishmentOrder - insertOrders: ' + insertOrders);
		
		
		SCOrderItem__c newItem;
		SCOrderExternalAssignment__c newXAssignment;
		SCOrderExternalAssignmentItem__c newXAssignmentItem;
		
		for(SCOrder__c inOrder : insertOrders)
		{
			newItem = new SCOrderItem__c();
			newItem.Order__c = inOrder.id;
			newItem.InstalledBase__c = OrderEquipMap.get(New2OldOrderMap.get(inOrder));
			
			insertOrderItems.add(newItem);
			
			newXAssignment = new SCOrderExternalAssignment__c();
			newXAssignment.Order__c = inOrder.id; 
			
			xAssignmentList.add(newXAssignment);		
		} 	

		insert insertOrderItems;
		insert xAssignmentList; 		
		
		for(SCOrderExternalAssignment__c xAssignment : xAssignmentList)
		{
			newXAssignmentItem = new SCOrderExternalAssignmentItem__c();
			newXAssignmentItem.OrderExternalAssignment__c = xAssignment.id;
			
			xAssignmentItemList.add(newXAssignmentItem); 
		}
		
		insert xAssignmentItemList;
		

		
		
		
		
		
		
	}
	
	public List<String> getFieldList (String p_objectName)
	{
        // Get a map of field name and field token
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(p_objectName.toLowerCase()).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();		
        if (fMap != null){
            for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                if (fd.isCreateable()){ // field is creatable
                    selectFields.add(fd.getName());
                }
            }
        }
		
		return selectFields; 

	}
	
	public String getQuery (String p_objectName, List<String> fieldList)
	{
		String selects = '';

 
        if (!fieldList.isEmpty()){
            for (string s:fieldList){
                selects += s + ',';
            }
            if (selects.endsWith(','))
            {
            	selects = selects.substring(0,selects.lastIndexOf(','));
            }
             
        }
         
        return 'SELECT ' + selects + ' FROM ' + p_objectName + ' WHERE ';
	}
	
}
