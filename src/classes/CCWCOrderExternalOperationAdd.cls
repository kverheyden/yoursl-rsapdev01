/*
 * @(#)CCWCOrderExternalOperationAdd.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * The class is used to add an order external assignment in SAP of an existing order
 * 
 * To start the callout:
 *   CCWCOrderExternalOperationAdd.callout(String externalAssignmentId, boolean async, boolean testMode)
 *
 * Methods to override: 
 *      fillAndSendERPData(...)
 *      fillAndSendERPData(...)
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
public without sharing class CCWCOrderExternalOperationAdd extends SCInterfaceExportBase
{

    public CCWCOrderExternalOperationAdd ()
    {
    }
    public CCWCOrderExternalOperationAdd (String interfaceName, String interfaceHandler, String refType, String subclassName)
    {
        super(interfaceName, interfaceHandler, refType, subclassName);
    }
    public CCWCOrderExternalOperationAdd (String interfaceName, String interfaceHandler, String refType)
    {
        //super(interfaceName, interfaceHandler, refType, subclassName);
    }

    /**
     * Prepare to make the webservice callout synchrounously or asynchronously (future)
     *
     * ### Set the interface name, its interface handler and a refType for head id in an interface object!
     *
     * @param oid   order id
     * @param async     true: use a future method to call 
     *                  false: do a synchrounous callout
     * @param testMode true: emulate call
     */
    public static String callout(String externalAssignmentId, boolean async, boolean testMode)
    {
        String interfaceName = 'SAP_ORDER_EXTOP_ADD';
        String interfaceHandler = 'CCWCOrderExternalOperationAdd';
        String refType = 'SCOrder__c';
        CCWCOrderCreate oc = new CCWCOrderCreate(interfaceName, interfaceHandler, refType);
        return oc.calloutCore(externalAssignmentId, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCOrderExternalOperationAdd');
    } // callout   

    /**
     * Reads an order to send
     *
     * @param externalAssignmentId
     * @param testMode if true no call is made, because in the test mode the salesforce framework prevent it.
     *
     */
    public override void readSalesforceData(String externalAssignmentId, Map<String, Object> retValue, Boolean testMode)
    {
        debug('externalAssignmentId: ' + externalAssignmentID);

        List<SCOrderExternalAssignment__c> oeal =  [Select ID, Name, 
        											Order__r.ID, Order__r.ERPOrderNo__c, 
        											Order__r.ERPStatusOrderClose__c, Order__r.ERPStatusOrderCreate__c,
        											Vendor__r.name, Vendor__r.ID2__c, VendorContract__c, VendorContract__r.name, 
                                                  VendorContract__r.ID2__c, VendorContractItem__r.name, VendorContractItem__r.ID2__c,
                                                  VendorContractItem__r.MaterialGroup__c, Order__r.OwnerId,ERPStatusAdd__c,
                                                  (select Qty__c, Unit__c, ServiceNumber__c, Description__c, Price__c
                                                  from OrderExternalAssignmentItems__r order by name)
                                from SCOrderExternalAssignment__c where ID = : externalAssignmentId for update];
                               
        if(oeal.size() > 0)
        {
            retValue.put(ROOT, oeal[0]);
            debug('order: ' + retValue);
            setReferenceID(oeal[0].Order__r.ID);
            setReferenceType('SCOrder__c');
        }
        else
        {
            String msg = 'The OrderExternalAssignment with id: ' + externalAssignmentId + ' could not be found!';
            debug(msg);
            throw new SCfwException(msg);
        }
    }

    /**
     * Fills the send structure and sends the call to SAP
     * 
     * @param dataMap with the order under the key ROOT
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure
     *           message = data sent to ERP
     *           message_v1 = endpoint 
     *           message_v2 = a return value of web service 
     *           message_v3 = a mandatory fields error
     *
     */
    public override Boolean fillAndSendERPData(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs)
    {
        Boolean retValue = true;
     
        SCOrderExternalAssignment__c externalAssignment = (SCOrderExternalAssignment__c)dataMap.get(ROOT);
        
        // Check if we can create the external assignment for the order
        if(externalAssignment.Order__r.ERPStatusOrderClose__c == 'ok')
        {
            rs.message = 'The OrderExternalOperationAdd was skipped as ERPStatusOrderClose__c is already [ok]';
            rs.message_v4 = 'E001';
            return retValue;
        }
        else if (externalAssignment.Order__r.ERPStatusOrderCreate__c != 'ok')
        {
        	rs.message = 'The OrderExternalOperationAdd was skipped as ERPStatusOrderCreate__c was not [ok]';
            rs.message_v4 = 'E001';
            return retValue;
        }
        //PMS 37478/INC0159521: Aufträge Doppelt im Vendor Portal 
        else if (externalAssignment.ERPStatusAdd__c == 'ok' || externalAssignment.ERPStatusAdd__c == 'pending')
        {
        	rs.message = 'The OrderExternalOperationAdd was skipped as ERPStatusAdd__c was ['+externalAssignment.ERPStatusAdd__c+']';
            rs.message_v4 = 'E001';
            return retValue;
        }
        
        List<SCOrderExternalAssignmentItem__c> itemList = (List<SCOrderExternalAssignmentItem__c>) dataMap.get(KEY_LIST);
        // instantiate the client of the web service
        piCceagDeSfdcCSOrderExternalOperationAdd.HTTPS_Port ws = new piCceagDeSfdcCSOrderExternalOperationAdd.HTTPS_Port();

        // sets the basic authorization for the web service
        ws.inputHttpHeaders_x = u.getBasicAuth();

        // sets the endpoint of the web service
        ws.endpoint_x  = u.getEndpoint('OrderExternalOperationAdd');
        debug('endpoint: ' + ws.endpoint_x);
        rs.message_v1 = ws.endpoint_x; 
 		ws.timeout_x = u.getTimeOut();
        
        try
        {
            // instantiate a message header
            piCceagDeSfdcCSOrderExternalOperationAdd.BusinessDocumentMessageHeader messageHeader = new piCceagDeSfdcCSOrderExternalOperationAdd.BusinessDocumentMessageHeader();
            setMessageHeader(messageHeader, externalAssignment, messageID, u, testMode);
            
            // instantiate the body of the call
            piCceagDeSfdcCSOrderExternalOperationAdd.ExternalOperation_element externalOperation = new piCceagDeSfdcCSOrderExternalOperationAdd.ExternalOperation_element();
            
            // set the data to the body
            // rs.message_v3 = 
            setExternalAssignment(externalOperation, externalAssignment, u, testMode);

            String jsonInputMessageHeader = JSON.serialize(messageHeader);
            String fromJSONMapMessageHeader = getDataFromJSON(jsonInputMessageHeader);
            debug('from json Message Header: ' + fromJSONMapMessageHeader);

            String jsonInputOrder = JSON.serialize(externalOperation);
            String fromJSONMapOrder = getDataFromJSON(jsonInputOrder);
            debug('from json Order: ' + fromJSONMapOrder);
            
            rs.message = '\n\nMessageHeader: ' + fromJSONMapMessageHeader + ',\n\nmessageData: ' + fromJSONMapOrder ;
            debug('rs.message: ' + rs.message);
            // check if there are missing mandatory fields
            if(rs.message_v3 == null || rs.message_v3 == '')
            {
                debug('message_v3 ok');
                // All mandatory fields are filled
                // callout
                if(!testMode)
                {
                    debug('real mode');
                    // It is not the test from the test class
                    // go
                    rs.setCounter(1);
                    ws.CustomerServiceOrderAddExternalOperation_Out(messageHeader, externalOperation);
                    rs.Message_v2 = 'void';
                }
            }
            debug('after sending');
        }
        catch(Exception e) 
        {
            throw e;
        }
        return retValue;
    }
    
    /**
     * Sets the ERPStatusOrderCreate in the root object to 'pending'
     * 
     * @param dataMap of obects to dispatch
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure with error messages
     * @param error true if some error has been occured
     */
    public override void setERPStatus(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs, Boolean error)
    {
        if(dataMap != null)
        {
            SCOrderExternalAssignment__c oea = (SCOrderExternalAssignment__c)dataMap.get(ROOT);
            if(oea != null)
            {
                // abort the ERPStatus manipulation, when the order staus is already 'ok'
                //GMSGB PMS 36271: Auslösen von Interface Schnittstellen verhindern, wenn OderCreate noch nicht zurück ist
                if(!testMode &&  (oea.Order__r.ERPStatusOrderClose__c == 'ok' || oea.Order__r.ERPStatusOrderCreate__c != 'ok' ) )
                {
                    return;
                }
                
                if(!testMode && error)
                {
                    oea.ERPStatusAdd__c = 'error';
                }
                else
                {   
                    oea.ERPStatusAdd__c = 'pending';
                }   
                update oea;
                debug('order external assignment after update: ' + oea);
                updateOrder(oea.Order__r.Id, oea.ERPStatusAdd__c);
            }
        }    
    }

    public void updateOrder(ID orderID, String status)
    {
        if(orderID != null)
        {
            List<SCOrder__c> orderList = [Select ID from SCOrder__c where id = :orderID];
            if(orderList != null && orderList.size() > 0)
            {
                for(SCOrder__c o: orderList)
                {
                    if(status != null)
                    {
                        o.ERPStatusExternalAssignmentAdd__c = status;
                    }       
                }
            }
            update orderList;
        }       
    }
    
    /**
     * sets data into the message header
     * messageID, referenceID = order.Name, CreationDateTime, SenderBusinessSystemID, RecipientBusinessSystemID
     *
     * @param mh message header 
     * @param order 
     * @param messageID MS: milliseconds R: random, e.g.    MS:1351254139745-R:0.7486318721270884
     * @param u CCWSUtil instance
     * @param testMode used to prevent call out by calls from CCWCOrderCreateTest class
     *
     */
    public void  setMessageHeader(piCceagDeSfdcCSOrderExternalOperationAdd.BusinessDocumentMessageHeader mh, 
                                         SCOrderExternalAssignment__c externalAssignment, String messageID, CCWSUtil u, Boolean testMode)
    {
        mh.MessageID = messageID;
//      mh.MessageUUID;
        mh.ReferenceID = externalAssignment.Name;
//      mh.ReferenceUUID;
        mh.CreationDateTime = u.getFormatedCreationDateTime();
        if(testMode)
        {
            mh.TestDataIndicator = 'Test';
        }    
        mh.SenderBusinessSystemID = u.getSenderBusinessSystemID();
        mh.RecipientBusinessSystemID = u.getRecipientBusinessSystemID ();
        debug('Message Header: ' + mh);
    }

    /**
     * sets the call out order structure with data form an order
     *
     * @param cso callout order sturcture
     * @param order order with input data
     * @param u instance of the util class CCWSUtil
     * @param testMode used to prevent call out by calls from CCWCOrderCreateTest class
     *
     */
    public String setExternalAssignment(piCceagDeSfdcCSOrderExternalOperationAdd.ExternalOperation_element eoe, 
                                        SCOrderExternalAssignment__c ea, 
                                        CCWSUtil u, Boolean testMode)
    {
        String retValue = '';
        String step = '';
        try
        {
            step = 'OrderNumber';
            eoe.OrderNumber = ea.Order__r.ERPOrderNo__c;
            eoe.Vendor = ea.Vendor__r.name; //### ea.Vendor__r.ID2__c;
            eoe.Contract = ea.VendorContract__r.name;
            if(ea.VendorContractItem__c != null && ea.VendorContractItem__r.name != '')
            {
            	eoe.ContractPosition = ea.VendorContractItem__r.name;
            	eoe.MaterialGroup = ea.VendorContractItem__r.MaterialGroup__c;
            }
            // PMS 36637/INC0109604: Ext. Auftragsvergabe / Freitextbestellung für Vendors ohne ContractPosition wieder freigeben
            //else
            //{
            //	throw new SCfwException('The callout was aborted by the Clockport interface. \n SAP requires an ContractPosition and MaterialGroup.'+
            //	 '\n Therfore, the ExternalAssignment needs a VendorContractItem. \n Please fill the VendorContractItem and retry the operation.');
            //}
            
            if(ea.VendorContract__c == null)//PMS 35140/INC0019690: ORD-0000004027 Error Freitextbestellung: Für dieses Geschäft muss eine Bewertungsklasse angegeben 
            {
            	eoe.MaterialGroup = u.getExternalOperationMaterialGroup();
            }
            
            
            if(u.user.name != 'sapapi')
            {
            	eoe.PurchasingGroup = u.getPurchaseGroup();
            	eoe.PurchasingOrganization = u.getPurchaseOrg();
            }
            else// for the sapapi user we will select the orders owner as source for the Purchasing Group and org.
            {
            	User owner = [select id, ERPPurchaseGroup__c, ERPPurchaseOrg__c from User where id = : ea.Order__r.OwnerID limit 1];
            	if(owner != null)
            	{
            		eoe.PurchasingGroup 		= owner.ERPPurchaseGroup__c;
            		eoe.PurchasingOrganization  = owner.ERPPurchaseOrg__c;
            	}
            	else
            	{
            		debug('ERROR: user is SAPAPI and no owner os the order is given.');
            		
            	}
            	
            }
            
            eoe.UnloadingPoint = ea.Name;
            eoe.ServiceItems = new piCceagDeSfdcCSOrderExternalOperationAdd.ServiceItems_element();
            eoe.ServiceItems.ServiceItem = new List<piCceagDeSfdcCSOrderExternalOperationAdd.ServiceItem_element>();

            List<SCOrderExternalAssignmentItem__c> itemList = ea.OrderExternalAssignmentItems__r;
            for(SCOrderExternalAssignmentItem__c item: itemList)
            {
                piCceagDeSfdcCSOrderExternalOperationAdd.ServiceItem_element el = new piCceagDeSfdcCSOrderExternalOperationAdd.ServiceItem_element();

                el.Quantity = '1';
                
                // if there a vendor contract service exits
                if(item.ServiceNumber__c != null)
                {    
                    // we only have to transfer the following information
                    el.ServiceID = item.ServiceNumber__c;
                    if(item.Qty__c != null)
                    {
                        el.Quantity = '' + item.Qty__c.stripTrailingZeros().toPlainString();        // remove zeros and dezimal places
                    }                   
                    //el.UnitOfMeasure = item.Unit__c;  // determined by SAP
                    el.GrossPrice = item.Price__c;   
                }
                else
                {
                    el.ShortText = item.Description__c;
                    if(item.Qty__c != null)
                    {
                        el.Quantity = '' + item.Qty__c.stripTrailingZeros().toPlainString();        // remove zeros and dezimal places
                    }                   
                    el.UnitOfMeasure = item.Unit__c;
                    el.GrossPrice = item.Price__c;
                }
                
                
                eoe.ServiceItems.ServiceItem.add(el);
            }
        }
        catch(Exception e)
        {
            String prevMsg = e.getMessage();
            String newMsg = 'setExternalAssignment: ' + step + ' ' + prevMsg;
            String stack = SCfwException.getExceptionInfo(e);
            newMsg += stack;
            debug(newMsg);
            e.setMessage(newMsg);
            throw e;
        }

        return retValue;    
    }

    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }
}
