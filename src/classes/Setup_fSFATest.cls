/**********************************************************************

Name: Setup_fSFATest

======================================================

Purpose: 

This is the test class for the setup class

======================================================

History 
 
------- 

Date 			AUTHOR 				DETAIL

09/09/2014 		Falk Wiegand 	INITIAL DEVELOPMENT

***********************************************************************/

@isTest
private class Setup_fSFATest {
    
    static testMethod void testSetup() 
	{	
		Test.startTest();
		try {
			Setup_fSFA s = new Setup_fSFA();
			s.Install( false, false );
		} catch (Exception e) {			
//			System.AssertEquals(expectedExceptionThrown, true);
		}
		
		Test.stopTest();
    }

}
