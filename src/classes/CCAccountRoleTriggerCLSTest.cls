/*******************************************************
created 2014-05-15
Update Account Owner based on the CCAccountRole
*******************************************************/
@isTest
private class CCAccountRoleTriggerCLSTest {

    static testMethod void testAccountRoleTrigger() {
        List<Account> newAcc	= new List<Account>();
        
        // Test without Custom setting
        Account tmpAcc= new Account();
        tmpAcc.Name		= 'Test ART';
        tmpAcc.BillingStreet ='die Stra?e 1';
        tmpAcc.BillingCountry = 'Germany';
        tmpAcc.BillingCity = 'Hamburg';
        tmpAcc.BillingCountry__c = 'DE';
        insert tmpAcc;
        
        User tmpUser = new User();
        tmpUser.FirstName 	= 'J?rgen';
        tmpUser.LastName	= 'Test';
        tmpUser.Email		= 'test@cc-eag.de';
        tmpUser.Username	= 'test@cc-eag.de';
        tmpUser.ProfileId	= '00eD0000001lNP0';
        //tmpUser.UserType	= 'Standard';
        tmpUser.ID2__c		= 'DE123456@cc-eag.de';
        tmpUser.Alias		= 'JT';
        tmpUser.TimeZoneSidKey = 'Europe/Berlin';
        tmpUser.LocaleSidKey = 'de_DE_EURO';
        tmpUser.EmailEncodingKey = 'ISO-8859-1';
        tmpUser.LanguageLocaleKey = 'en_US';
      
        
        insert tmpUser;
        
        
        CCAccountRole__c tmpCCAR = new CCAccountRole__c();
        tmpCCAR.MasterAccount__c	= tmpAcc.Id;
        tmpCCAR.AccountRole__c		= 'ZR';
        tmpCCAR.Status__c			= 'Active';
        tmpCCAR.EmployeeDeNumber__c	= 'DE123456';
        
        insert tmpCCAR;
        
        // Re-Test with custom setting
        delete tmpCCAR;
        
        CCSettingFlat__c ccset 	= new CCSettingFlat__c();
        ccset.Name 				= 'AccountOwnerLicenceKeys';
        ccset.Value__c			= 'SFDC';
        insert ccset;
        
        tmpCCAR 					= new CCAccountRole__c();
        tmpCCAR.MasterAccount__c	= tmpAcc.Id;
        tmpCCAR.AccountRole__c		= 'ZR';
        tmpCCAR.Status__c			= 'Active';
        tmpCCAR.EmployeeDeNumber__c	= 'DE123456';
        insert tmpCCAR;
        
        
        for(Account tmpNewAcc: [Select OwnerId from Account where Id =: tmpAcc.Id]){
        	newAcc.add(tmpNewAcc);
        }
        
        system.assert(newAcc.size()==1);
        system.assert(newAcc[0].OwnerId == tmpUser.Id);
    }
}
