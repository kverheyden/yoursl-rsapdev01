/*
 * @(#)SCboOrderLineTest.cls SCCloud    dh 21.09.2010
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author DH <dherdt@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCboOrderLineTest
{   
    /**
     * SCHelperTestClass for creation test order data
     */
    //private static SCHelperTestClass SCHelperTestClass;
    
    /**
     * SCboOrderLine under negativ test
     */
    static testMethod void testboOrderLineNegativ()
    {
        SCHelperTestClass.createOrderTestSet(true);
        try
        {
            
            SCHelperTestClass.boOrderLine.save();
            SCHelperTestClass.boOrderLine2.save();
            // if order line saved, failure
            // DH: something wrong here --> class changed?
            // System.assert(false); 
        }
            
        catch (SCfwOrderException e)
        {
            // proper exception matched
            System.assert(true);
        }            
    }
    

    static testMethod void SCboOrderLineConstructors()
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createTestArticles(true);
        SCboOrderLine boOrdLine = new SCboOrderLine();

        SCboOrder boOrder = new SCboOrder(SCHelperTestClass.order);
        SCboOrderItem boOrderItem = new SCboOrderItem(SCHelperTestClass.orderItem);

        // Id orderId, Id orderItemId, Id articleId, Id assignmentId
        SCboOrderLine boOrderLine = new SCboOrderLine(SCHelperTestClass.order.Id,
                                                      SCHelperTestClass.orderItem.Id,
                                                      SCHelperTestClass.articles[0].Id,
                                                      SCHelperTestClass.assignmentMap.get('assignmentDispo1').Id 
                                                      );
        
        // Id orderId, Id orderItemId, Id articleId, Id assignmentId
        SCboOrderLine boOrderLine2 = new SCboOrderLine(SCHelperTestClass.order,
                                                      SCHelperTestClass.orderItem.Id,
                                                      SCHelperTestClass.articles[0].Id,
                                                      SCHelperTestClass.assignmentMap.get('assignmentDispo1').Id 
                                                      );
        SCboOrderLine boOrdLine3 = new SCboOrderLine(boOrderLine2.orderline);
    }


    static testMethod void isNotEditable()
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createTestArticles(true);
        SCboOrderLine boOrdLine = new SCboOrderLine();
        Boolean bEdit = boOrdLine.isNotEditable;
    }

    static testMethod void isReadOnly()
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createTestArticles(true);
        SCboOrderLine boOrdLine = new SCboOrderLine();
        Boolean bReadOnly = boOrdLine.isReadOnly;
    }

    static testMethod void isDeletable()
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createTestArticles(true);
        SCboOrderLine boOrdLine = new SCboOrderLine();
        Boolean bDel = boOrdLine.isDeletable;
    }
    
    static testMethod void itemIdentifier()
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createTestArticles(true);
        SCboOrderLine boOrdLine = new SCboOrderLine();
        String identifer = boOrdLine.itemIdentifier;
    }

    static testMethod void symMatStat()
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createTestArticles(true);
        SCboOrderLine boOrdLine = new SCboOrderLine();
        String  matstat = boOrdLine.symMatStat;
    }

    static testMethod void readByOrder()
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createTestArticles(true);
        SCboOrderLine boOrdLine = new SCboOrderLine();
        boOrdLine.readByOrder(SCHelperTestClass.order.Id);
    }

}
