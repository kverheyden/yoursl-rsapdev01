/*
 * @(#)SCDayStartController.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Implements the day start or day end dialog.
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @author Dietrich Herdt <dherdt@gms-online.de>
 * @author Sebastian Schrage <sschrage@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCDayStartController extends SCfwPageControllerBase
{
/*
 * ((SCProcessOrderController)pageController).varInController;
 * ((SCProcessOrderController)pageController).methodInController();
 */
    public Boolean allOk { get; private set; }
    public Boolean showOpenDay { get; private set; }
    public boolean addButtonOn { get; set; }
    public String appId { get; private set; }
    public String startTime { get; set; }
    public String endTime { get; set; }
    public String confirmText { get; private set; }
    public Integer mileage { get; set; }
    public List<SCTimereport__c> timereport { get; set; }
    public SCTimereport__c timereportSingle { get; set; }
    public SCTimereport__c pendingDay { get; set; }
    public String pendingDayDisplay { get; set; }
    public String pendingDayTimeReport { get; set; }
    //public String openDayStartId { get; set; }
    public List <String> openDayStartId { get; set; }
    public String dayStartListString { get; set; }
    
    public String callback {get;set;}
    public String callbackComplete {get;set;}

    public SCTool__c toolData { get; set; }   
    private SCResource__c resource { get; set; }
    public String resourceTimeReport { get; set; }
    private Boolean noActions;
    private String dayOffset;

    protected SCfwDomain domSysParam = new SCfwDomain('DOM_SYSPARAM');
    
    SCApplicationSettings__c globalAppset;
    List<SCResource__c> globalListResource;
    
    /**
     * Default constructor
     */
    public SCDayStartController()
    {
        System.debug('#### SCDayStartController');
        appId = ApexPages.currentPage().getParameters().get('aid');
        dayOffset = ApexPages.currentPage().getParameters().get('off');
        System.debug('#### SCDayStartController: aid -> ' + appId);
        System.debug('#### SCDayStartController: offset -> ' + dayOffset);
        callback = ApexPages.currentPage().getParameters().get('callback');
        callbackComplete = ((callback == null || callback == '') ? '' : 'window.opener.' + callback + '();');
        System.debug('#### SCDayStartController: aid      -> ' + appId);
        System.debug('#### SCDayStartController: offset   -> ' + dayOffset);
        System.debug('#### SCDayStartController: callback -> ' + callback);

        init(ApexPages.currentPage().getParameters().get('uid'));
        
        System.debug('###+###resource: ' + resource);
    }
    
    public String getCurrentDay()
    {
      return Datetime.now().format('dd.MM.yyyy');
    }
    
    /*
     * System-Admins may choose the Resource.
     */
    public Boolean getAllowedToChooseTheResource()
    {
        if (globalAppset.RESOURCE_CHANGE_ALLOWED__c == 1) { return true; }
        return false;
    }
    
    /*
     * Creates a List of SelectedOptions, which is need for the choice of a resource
     */
    public List<SelectOption> getAllResourceList()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        if ((globalListResource == null) || (globalListResource.size() == 0))
        {
            globalListResource = [SELECT Id, Name, Employee__c FROM SCResource__c ORDER BY Name LIMIT 10000];
        }
        
        for (SCResource__c res : globalListResource)
        {
            options.add(new SelectOption(res.Id,res.Name));
        }
        
        return options;
    }
    
    public String getSelectedResource()
    {
        System.debug('###+###resource (getselected): ' + resource);
        if (resource != null) { return resource.Id; }
        return '';
    }
    
    public void setSelectedResource(String ResourceId)
    {
        for (SCResource__c res : globalListResource)
        {
            if (res.Id == ResourceId) { resource = res; }
        }
    }
    
    public PageReference ChangeResource()
    {
        if (resource != null) { init(resource.Id); }
        else                  { init(UserInfo.getUserId()); }
        return null;
    }
    
    /**
     * Set allOk to true.
     *
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference setAllOkTrue()
    {
        allOk = true;
        return null;
    }     

    /**
     * Default constructor
     */
    public SCDayStartController(String aid, String uid)
    {       
        System.debug('#### SCDayStartController: aid -> ' + aid);
        System.debug('#### SCDayStartController: uid -> ' + uid);
        appId = aid;
        init(uid);
    }
    
    /**
     * Initialize all necessary datas.
     * @ userId   #####TODO: ####
     */
    public void init(String userId)
    {
        toolData = new SCTool__c();
        timereportSingle = new SCTimeReport__c();
        timereport = new List <SCTimeReport__c>();
        resourceTimeReport = null;
        
        System.debug('#### init: userId -> ' + userId);
        if ((null == userId) || (userId.length() == 0))
        {
            userId = UserInfo.getUserId();
        }
        try
        {
            if (globalAppset == null) { globalAppset = SCApplicationSettings__c.getInstance(); }
            
            if (globalAppset.RESOURCE_CHANGE_ALLOWED__c == 1)
            {
                try { resource = [select Id, Name from SCResource__c where Employee__r.Id = :userId]; }
                catch (Exception e) { }
                
                if (resource == null)
                {
                    String strTmp = userId;
                    userId = UserInfo.getUserId();
                    
                    try { resource = [select Id, Name from SCResource__c where Id = :strTmp]; }
                    catch (Exception e) { }
                }
            }
            
            if (resource == null)
            {
                resource = [select Id, Name from SCResource__c where Employee__c = :userId or Id = :userId];
            }
            
            resourceTimeReport = resource.Id;
        }
        catch (Exception e)
        {
            //resource.Name = 'GMS+'+e;
            resource = null;
            // no resource for user found, error
            ApexPages.addMessage(
            new ApexPages.Message(ApexPages.Severity.ERROR, 
                                 System.Label.SC_msg_ResourceNotFound));
    
            // GMSAW: i don't know if that is correct
            allOk = false;
            showOpenDay = true;
            return;
        }
        
        // if resource is not null, save the resource id as string
        if (resource != null)
        {
            resourceTimeReport = resource.Id;
        }    
        //System.debug('#### init: resourceId -> ' + resource.Id);
        
        // init date time data for display
        toolData.Start__c = Date.today();
        System.debug('#### init: start day -> ' + toolData.Start__c);
        System.debug('#### init: offset    -> ' + dayOffset);
        if ((null != dayOffset) && (dayOffset.length() > 0))
        {
            toolData.Start__c = toolData.Start__c.addDays(Integer.valueOf(dayOffset));
        } // if ((null != dayOffset) && (dayOffset.length() > 0))
        System.debug('#### init: start day + offset -> ' + toolData.Start__c);
        Datetime now = Datetime.now();
        //startTime = SCboTimereport.roundMinutes(now, 15).format('HH:mm');
        startTime = SCutilDatetime.roundMinutes(now).format('HH:mm');
        endTime = startTime;

        timereportSingle.Start__c = datetime.newInstance (toolData.Start__c.year(),
                                                        toolData.Start__c.month(),
                                                        toolData.Start__c.day(),
                                                        Integer.ValueOf(startTime.substring(0,2)),
                                                        Integer.ValueOf(startTime.substring(3,5)),
                                                        00);
                                                                                                                
        // if resource is not null, read the mileage and compensation type
        if (resource != null)
        {
            // mileage data from last mileage entry
            timereportSingle.Mileage__c = SCboTimereport.getLastMileageEntry(resource.Id);
        
            // Compensation type is standard        
            timereportSingle.CompensationType__c = 
                         getCompensationType(resource.Id, timereportSingle.Start__c);
        }
        allOk = false;
        showOpenDay = true;
    } // init

    /**
     * Checks if there is a day start entry for the given day.
     * If not, a page is opened for open the day.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference checkDayOpen()
    {
        System.debug('#### checkDayOpen()');
        // first check if there are opened days
        if ((null != resource) && checkCloseDay())
        {
            // no open days found
            Datetime now = Datetime.now();
            if ((null != dayOffset) && (dayOffset.length() > 0))
            {
                now = now.addDays(Integer.valueOf(dayOffset));
            } // if ((null != dayOffset) && (dayOffset.length() > 0))
            Datetime startDate = Datetime.newInstance(now.year(), now.month(), now.day(), 0, 0, 0);
            Datetime endDate = Datetime.newInstance(now.year(), now.month(), now.day(), 23, 59, 0);

            //startTime = SCboTimereport.roundMinutes(now, 15).format('HH:mm');
            startTime = SCutilDatetime.roundMinutes(now).format('HH:mm');
    
            SCboTimereport timeRep = new SCboTimereport();
            
            // timereport = timeRep.readAllEmplTimereports2(resource.Id, currentTimeReportId);

            timereport = timeRep.readAllEmplTimereports(resource.Id, startDate, endDate);
            
            System.debug('#### checkDayOpen(): timereport ->' + timereport);
            
            // read all time report for today
            // Status is opened and type daystart            
            for(SCTimereport__c timeRepItem :timereport)
            {
                if ((timeRepItem.Type__c == SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART) &&
                    (timeRepItem.Status__c == SCfwConstants.DOMVAL_TIMEREPORTSTATUS_OPENED))
                {
                    allOk = true;
                    return null;
                }
            } 
        } // if(checkCloseDay)
        return null;
    } // checkDayOpen

    /**
     * Checks if there is an opened day without a close day entry.
     * If yes, a page is opened for close the day.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public Boolean checkCloseDay()
    {
        System.debug('#### checkCloseDay()');
        SCboTimereport boTimereport = new SCboTimereport();
        System.debug('RESOURCE ID is '+ resource.Id);
        
        // set the list of day start ids to null
        dayStartListString = null;
        
        // search for open day and return the start if founded
        pendingDay = boTimereport.readOpenDays2(resource.Id);        
        // pendingDay = timeRep.readOpenDayStart(resource.Id);
        // currentTimeReportId = pendingDay.Id;
        
        System.debug('#### checkCloseDay(): pendingDay before If -> ' + pendingDay);

        if (null != pendingDay)
        {
            //startTime = SCboTimereport.roundMinutes(pendingDay.Start__c, 15).format('HH:mm');
            startTime = SCutilDatetime.roundMinutes(pendingDay.Start__c).format('HH:mm');

            Datetime startDate = Datetime.newInstance(pendingDay.Start__c.year(), 
                                                    pendingDay.Start__c.month(), 
                                                    pendingDay.Start__c.day(), 
                                                    0, 0, 0);
                                                                
            Datetime endDate = Datetime.newInstance(pendingDay.Start__c.year(), 
                                                    pendingDay.Start__c.month(), 
                                                    pendingDay.Start__c.day(), 
                                                    23, 59, 0);
                                                    
            // DH: old query without time report link
            /* timereport = timeRep.readAllEmplTimereports(resource.Id, 
                                                           pendingDay.Start__c, 
                                                           endDate);
            */                                             
            
            // search for all time reports with link to the founded start of open time report
            timereport.add(pendingDay);
            timereport.addAll(boTimereport.readByLink(resource.Id, pendingDay.Id));

            System.debug('#### checkCloseDay(): timereport in If ->' + timereport);
            
            pendingDayDisplay = pendingDay.Start__c.format('dd.MM.yyyy');
            pendingDayTimeReport = pendingDay.Start__c.format('yyyyMMdd');

            try
            {   
                // search all day starts for the day                          
                List <SCTimeReport__c> dayStartList = 
                    boTimereport.readAllDayStart(resourceTimeReport, startDate, endDate);
                
                System.debug('#### OnSelect dayStartList '+dayStartList);
                
                for (SCTimereport__c timeReportStart :dayStartList)
                {
                    //dayStartListString.add(timeReportStart.Id);
                    if (dayStartListString == null)
                    {
                        dayStartListString = timeReportStart.Id;
                    }
                    else
                    {
                        // add to string comma-separated ids
                        dayStartListString = dayStartListString + ',' +
                                             timeReportStart.Id;
                    }
                }

                System.debug('#### OnSelect dayStartListString '+dayStartListString);
            }
            catch (Exception e)
            {
                dayStartListString = null;
            }    
            System.debug('### The day starts are '+dayStartListString);                        
        }
        //System.debug('showOpenDay openDayStartId ####'+openDayStartId);
        
        showOpenDay = (null == pendingDay);
        System.debug('showOpenDay is ####'+showOpenDay);
        return showOpenDay;
    } // checkCloseDay
   
    /**
     * Create a start day entry in the working time object.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference openDay()
    {
        System.debug('#### openDay()');
        SCboTimereport boTimereport = new SCboTimereport();
        
        // help value for validation of input string
        boolean inputTimeIsNotValid = false;
        // first check the format of the start time
        // values of hours and minutes greater than 60 not allow        
        try
        {
            Integer hours = Integer.ValueOf(startTime.substring(0,2));
            Integer minutes = Integer.ValueOf(startTime.substring(3,5));
        }
        catch (Exception e)
        {
            // input string is not a number
            inputTimeIsNotValid = true;
        }
        
        if ((null == startTime) || (startTime.length() < 5) || 
            (startTime.indexOf(':') != 2) || inputTimeIsNotValid)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                                                       System.Label.SC_msg_NoValidTime));
            return null;
        }

        Datetime startTimeEntry = datetime.newInstance (toolData.Start__c.year(),
                                                        toolData.Start__c.month(),
                                                        toolData.Start__c.day(),
                                                        Integer.ValueOf(startTime.substring(0,2)),
                                                        Integer.ValueOf(startTime.substring(3,5)),
                                                        00);
                                                        
        //startTimeEntry  = SCboTimereport.roundMinutes(startTimeEntry, 15);
        startTimeEntry  = SCutilDatetime.roundMinutes(startTimeEntry);
        System.debug('#### openDay startTimeEntry() is: '+startTimeEntry);

        //If set, the time reports must be created in the chronological order.
        SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();

        // validation for chronological order of the time reports
        if (appset.TIMEREPORT_CHRONOLOGICAL__c)
        {
            SCTimeReport__c lastTimeReport = new SCTimeReport__c ();
            try
            {
                lastTimeReport = boTimereport.readLastClosedDay(resourceTimeReport);
            }
            catch (Exception e)
            {
                // not closed time reports found, all ok
                lastTimeReport = null;
            }
            
            if ((lastTimeReport != null) && (startTimeEntry < lastTimeReport.End__c))
            {
                // the new entry is inside of other time report, error
                ApexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.ERROR, 
                                     System.Label.SC_msg_BeforeLastTimeReport));
                                     // + ' '+'lastClosedTimeReport.Start__c' + 
                                     //'.'));
    
                return null;        
            }            
        }
            
        // validation for last closed time report
        SCTimeReport__c lastClosedTimeReport = new SCTimeReport__c ();
        try
        {
            lastClosedTimeReport = boTimereport.readLastClosedDay(resourceTimeReport);
        }
        catch (Exception e)
        {
            // not closed time reports found, all ok
        }
        
        // validation for resource of current user
        if (resource == null)
        {
            ApexPages.addMessage(
            new ApexPages.Message(ApexPages.Severity.ERROR, 
                                 System.Label.SC_msg_ResourceNotFound));        
        
        }
        
        // the new entry is inside of other time report, error      
        if (startTimeEntry < lastClosedTimeReport.Start__c)
        {
            ApexPages.addMessage(
            new ApexPages.Message(ApexPages.Severity.ERROR, 
                                 System.Label.SC_msg_BeforeClosedTimeReport));
                                 // + ' '+'lastClosedTimeReport.Start__c' + 
                                 //'.'));

            return null;        
        }
        
        // the entry have not valid compensation type, error        
        if ((timereportSingle.CompensationType__c == null) || 
            (timereportSingle.CompensationType__c == '') || 
            (timereportSingle.CompensationType__c.length() == 0))
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                                                       System.Label.SC_msg_NoValidCompensationType));
            return null;                        
        }
        
        /** 
         *  Timereport contains time report entries for today.
         *  New time report entry should be checked for first
         *  and last entry of timereport. 
         */ 
        if ((timereport != null) &&
            (timereport.size() > 1) &&
           //(startTimeEntry >= timereport.get(0).Start__c) && 
           (startTimeEntry < timereport.get(timereport.size()-1).End__c))
        {
            
            // the new entry is inside of other time report, error
            ApexPages.addMessage(
            new ApexPages.Message(ApexPages.Severity.ERROR, 
                                 System.Label.SC_msg_OverlappingTimeAfter));
                                 // + ' ' + timereport.get(timereport.size()-1).End__c.format('HH:mm') + 
                                 //'.'));

            return null;                                                                
        }
        
        else
        {
            SCTimereport__c dayOpenEntry = 
                new SCTimereport__c(Start__c = startTimeEntry, 
                                    End__c = startTimeEntry, 
                                    Resource__c = resource.Id, 
                                    Type__c = SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART,
                                    Status__c = SCfwConstants.DOMVAL_TIMEREPORTSTATUS_OPENED,
                                    CompensationType__c = timereportSingle.CompensationType__c,
                                    Mileage__c = timereportSingle.Mileage__c);

            // try to find other start at same day, if exists do nothing
            try
            {
                System.debug('#### openDay(): look for day open - start  -> ' + dayOpenEntry.Start__c);
                System.debug('#### openDay(): look for day open - resId  -> ' + dayOpenEntry.Resource__c);
                System.debug('#### openDay(): look for day open - type   -> ' + dayOpenEntry.Type__c);
                System.debug('#### openDay(): look for day open - status -> ' + dayOpenEntry.Status__c);
                SCTimereport__c dayOpenEntryExists = [select Start__c, 
                                                             End__c, 
                                                             Resource__c, 
                                                             Type__c, 
                                                             Status__c
                                                        from SCTimereport__c
                                                        where Start__c = :dayOpenEntry.Start__c and
                                                              Resource__c = :dayOpenEntry.Resource__c and
                                                              Type__c = :dayOpenEntry.Type__c and
                                                              Status__c = :dayOpenEntry.Status__c];
                                                              
                System.debug('#### openDay(): dayOpenEntryExists -> ' + dayOpenEntryExists);
                if (dayOpenEntryExists != null)
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                // no other day starts found ==> all ok
                insert dayOpenEntry;
                allOk = true;
                return null;
            }
            
            allOk = true;
            return null;
            /*          
            insert dayOpenEntry;
            
            allOk = true;
            return null;
            */
        }
    } // openDay

    /**
     * Get the compensation type of time report entry.
     *
     * @resourceId The resource Id as String.
     * @dateTimeEntry Entered date time
     *
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public String getCompensationType(String resourceId, Datetime datetimeEntry)
    {
        String compensationType  = SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_DEFAULT;
        /*
            public final static String DOMVAL_TIMEREPORTCOMPENSATION_DEFAULT = '7701';
            public final static String DOMVAL_TIMEREPORTCOMPENSATION_STANDBY = '7702';
            public final static String DOMVAL_TIMEREPORTCOMPENSATION_SATURDAY = '7703';
            public final static String DOMVAL_TIMEREPORTCOMPENSATION_SUNDAY = '7704';
            public final static String DOMVAL_TIMEREPORTCOMPENSATION_PUBLIC_HOLIDAY= '7705';        
        */
        
        // day of week
        String dayOfWeek = datetimeEntry.format('E');
        System.debug('#### dayOfWeek: ' + dayOfWeek);
        
        if (dayOfWeek == 'Sat')
        {
           return SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_SATURDAY;
        }
        else if (dayOfWeek == 'Sun')
        {
           return SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_SUNDAY;        
        }
        /*
        else
        {
            String worktimeProfile =
              [Select s.Employee__c, 
                      s.Resource__c, 
                      s.ValidFrom__c, 
                      s.ValidTo__c, 
                      s.WorktimeProfile__c 
               from SCResourceAssignment__c s
               where Resource__c = :resourceId]
        }
        */
        return compensationType;
    }    
}
