/*
 * @(#)SCfwConfOrderDuration.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing virtual class SCfwConfOrderDuration
{
    protected List<SCConfOrderDuration__c> orderDurations;
    protected String userCountry;

    /**
     * Default constructor
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCfwConfOrderDuration()
    {
        System.debug('#### SCfwConfOrderDuration()');
        userCountry = SCBase.getUserCountry(true);
        init();
    } // SCfwConfOrderDuration

    /**
     * Constructor for special country.
     *
     * @param    country        specific country
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCfwConfOrderDuration(String country)
    {
        System.debug('#### SCfwConfOrderDuration(): country -> ' + country);
        userCountry = country;
        init();
    } // SCfwConfOrderDuration

    /**
     * Initialize the intern list with all possible order reasons.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    private void init()
    {
        System.debug('#### SCfwConfOrderDuration.init()');
        
        orderDurations = [Select Id, Name, ID2__c, 
                                 OrderType__c, ProductGroup__c, 
                                 ProductPower__c, ProductUnitClass__c, 
                                 ProductUnitType__c, Article__c, 
                                 Duration__c 
                            from SCConfOrderDuration__c 
                           where Country__c = :userCountry];
        System.debug('#### SCfwConfOrderDuration.init(): durations -> ' + orderDurations.size());
    } // init

    /**
     * Looks in the intern list for all relevant order durations and
     * retrieves the duration or -1 if not found.
     *
     * @param  orderType          the order type
     * @param  productUnitClass   the unit class
     * @param  productUnitType    the unit type
     * @param  productGroup       the product group
     * @param  productPower       the product power
     * @param  article            the article
     *
     * @return   the duration or -1
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public Integer getDuration(String orderType, 
                               String productUnitClass, String productUnitType, 
                               String productGroup, String productPower, 
                               Id article)
    {
        System.debug('#### SCfwConfOrderDuration.getDuration(): orderType        -> ' + orderType);
        System.debug('#### SCfwConfOrderDuration.getDuration(): productUnitClass -> ' + productUnitClass);
        System.debug('#### SCfwConfOrderDuration.getDuration(): productUnitType  -> ' + productUnitType);
        System.debug('#### SCfwConfOrderDuration.getDuration(): productGroup     -> ' + productGroup);
        System.debug('#### SCfwConfOrderDuration.getDuration(): productPower     -> ' + productPower);
        System.debug('#### SCfwConfOrderDuration.getDuration(): article          -> ' + article);
        
        Integer duration = -1;
        for (SCConfOrderDuration__c confDuration :orderDurations)
        {
            Boolean ordTypeEquals = ((null == confDuration.OrderType__c) || 
                                     ((null != orderType) && 
                                      (null != confDuration.OrderType__c) && 
                                      (confDuration.OrderType__c == orderType)));
            Boolean unitClassEquals = ((null == confDuration.ProductUnitClass__c) || 
                                       ((null != productUnitClass) && 
                                        (null != confDuration.ProductUnitClass__c) && 
                                        (confDuration.ProductUnitClass__c == productUnitClass)));
            Boolean unitTypeEquals = ((null == confDuration.ProductUnitType__c) || 
                                      ((null != productUnitType) && 
                                       (null != confDuration.ProductUnitType__c) && 
                                       (confDuration.ProductUnitType__c == productUnitType)));
            Boolean prodGroupEquals = ((null == confDuration.ProductGroup__c) || 
                                       ((null != productGroup) && 
                                        (null != confDuration.ProductGroup__c) && 
                                        (confDuration.ProductGroup__c == productGroup)));
            Boolean prodPowerEquals = ((null == confDuration.ProductPower__c) || 
                                       ((null != productPower) && 
                                        (null != confDuration.ProductPower__c) && 
                                        (confDuration.ProductPower__c == productPower)));
            Boolean articleEquals = ((null == confDuration.Article__c) || 
                                     ((null != article) && 
                                      (null != confDuration.Article__c) && 
                                      (confDuration.Article__c == article)));
            if (ordTypeEquals && unitClassEquals && unitTypeEquals && 
                prodGroupEquals && prodPowerEquals && articleEquals)
            {
                duration = Integer.valueOf(confDuration.Duration__c);
                System.debug('#### SCfwConfOrderDuration.getDuration(): duration -> ' + duration);
                return duration;
            } // if (ordTypeEquals && unitClassEquals && unitTypeEquals && ...
        } // for (SCConfOrderDuration__c confDuration :orderDurations)
        System.debug('#### SCfwConfOrderDuration.getDuration(): duration -> ' + duration);
        return duration;
    } // getDuration

    /**
     * Looks in the database for the relevant order duration or
     * retrieves -1 if not found.
     *
     * @param  orderType          the order type
     * @param  productUnitClass   the unit class
     * @param  productUnitType    the unit type
     * @param  productGroup       the product group
     * @param  productPower       the product power
     * @param  article            the article
     *
     * @return   the duration or -1
     */
    public static Integer getDurationEx(String orderType, 
                                        String productUnitClass, String productUnitType, 
                                        String productGroup, String productPower, 
                                        Id article)
    {
        // System.debug('#### SCfwConfOrderDuration.getDuration(): orderType        -> ' + orderType);
        // System.debug('#### SCfwConfOrderDuration.getDuration(): productUnitClass -> ' + productUnitClass);
        // System.debug('#### SCfwConfOrderDuration.getDuration(): productUnitType  -> ' + productUnitType);
        // System.debug('#### SCfwConfOrderDuration.getDuration(): productGroup     -> ' + productGroup);
        // System.debug('#### SCfwConfOrderDuration.getDuration(): productPower     -> ' + productPower);
        // System.debug('#### SCfwConfOrderDuration.getDuration(): article          -> ' + article);
        
        Integer duration = -1;
        List<SCConfOrderDuration__c> ordDurs = [Select Id, Name, ID2__c, 
                                                       OrderType__c, ProductGroup__c, 
                                                       ProductPower__c, ProductUnitClass__c, 
                                                       ProductUnitType__c, Article__c, 
                                                       Duration__c 
                                                  from SCConfOrderDuration__c 
                                                 where Country__c = :SCBase.getUserCountry(true) 
                                                   and ((OrderType__c = null) or (OrderType__c = :orderType)) 
                                                   and ((ProductGroup__c = null) or (ProductGroup__c = :productGroup)) 
                                                   and ((ProductPower__c = null) or (ProductPower__c = :productPower)) 
                                                   and ((ProductUnitClass__c = null) or (ProductUnitClass__c = :productUnitClass)) 
                                                   and ((ProductUnitType__c = null) or (ProductUnitType__c = :productUnitType)) 
                                                   and ((Article__c = null) or (Article__c = :article))];

        if (ordDurs.size() > 0)
        {
            duration = Integer.valueOf(ordDurs[0].Duration__c);
        } // if (ordDurs.size() > 0)
        // System.debug('#### SCfwConfOrderDuration.getDuration(): duration -> ' + duration);
        return duration;
    } // getDurationEx
} // SCfwConfOrderDuration
