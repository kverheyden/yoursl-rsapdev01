/*
This class is used to prevent the trigger from calling himself. 
*/
public with sharing class ContactAvoidTriggerRecursion {
	
	public static boolean run = true;
    
    public static boolean runOnce(){
    	if(run){
     		run=false;
     		return true;
    	}else{
        	return run;
    	}
    }//END runOnce

}
