/*
 * @(#)CCWCOrderEquipmentUpdate.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 *
 * The class exports an Salesforce order item to SAP
 * 
 * The entry method is: 
 *      callout(String oid, boolean async, boolean testMode)
 *
 * Methods to override: 
 *      fillAndSendERPData(...)
 *      fillAndSendERPData(...)
 *
 * @author Georg Birkenheuer <gbirkenheuer@gms-online.de>
 * @version $Revision$, $Date$
 *
 *
 * Test: a0sD0000001lsRK, a18D0000000V1vE
 * Boolean async = true;
 * Boolean testMode = false;
 * CCWCOrderEquipmentUpdate.callout('a0sD0000001lsRK', async, testMode);
 */
public without sharing class CCWCOrderEquipmentUpdate extends SCInterfaceExportBase
{
    
    
    public CCWCOrderEquipmentUpdate()
    {
    }
    public CCWCOrderEquipmentUpdate(String interfaceName, String interfaceHandler, String refType, String subclassName)
    {
        super(interfaceName, interfaceHandler, refType, subclassName);
    }
    public CCWCOrderEquipmentUpdate(String interfaceName, String interfaceHandler, String refType)
    {
        //super(interfaceName, interfaceHandler, refType, subclassName);
    }
    
    
    /**
     * Prepare to make the web service callout synchrounously or asynchronously (future)
     *
     * ### Set the interface name, its interface handler and a refType for head id in an interface object!
     *
     * @param oid   order id
     * @param async     true: use a future method to call 
     *                  false: do a synchrounous callout
     * @param testMode true: emulate call
     */
    public static String callout(String oid, boolean async, boolean testMode)
    {
        String interfaceName = 'SAP_ORDER_EQUIPMENT_UPDATE';
        String interfaceHandler = 'CCWCOrderEquipmentUpdate';
        String refType = 'SCOrder__c';
        CCWCOrderEquipmentUpdate ou = new CCWCOrderEquipmentUpdate(interfaceName, interfaceHandler, refType);
        return ou.calloutCore(oid, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCOrderEquipmentUpdate');
        //return '';
    } // callout   
    
     /**
     * Reads an order to send
     *
     * @param orderId
     * @param testMode if true no call is made, because in the test mode the salesforce framework prevent it.
     */
    public override void readSalesforceData(String orderID, Map<String, Object> retValue, Boolean testMode)
    {
         /** 
         * Excemplary Query:
         * 
         *
         * Select ID, ID2__c, Name, SalesArea__c, DistributionChannel__c, SalesOffice__c, SalesGroup__c, PriceList__r.ID2__c, 
         *       CompanyCode__c, Division__c,         
         *       (Select ID2__c, Installedbase__r.ProductModel__r.Name, InstalledBase__r.IdExt__c, ErrorText__c  from 
         *       OrderItem__r  where RecordTypeId in (Select Id from RecordType where 
         *       DeveloperName = 'Equipment' and SobjectType = 'SCOrderItem__c')),
         *       (select ID, OrderRole__c, AccountNumber__c, Account__r.AccountNumber  from OrderRole__r 
         *       where OrderRole__c in ('50301', '50303') order by OrderroleNumeric__c asc)
         *       from SCOrder__c where ID = 'a18D0000000V1vE' 
         */
         // GMSGB 04.03.2014 select not only record type 'Equipment' but also 'SubEquipment' 
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        List<SCOrder__c> ol =  [Select ID, ID2__c, Name, ERPOrderNo__c, SalesArea__c, DistributionChannel__c, 
        		SalesOffice__c, SalesGroup__c, PriceList__r.ID2__c, 
                CompanyCode__c, Division__c, isWorkshopOrder__c, ERPStatusOrderClose__c, ERPStatusOrderCreate__c,         
                (Select Installedbase__r.ProductModel__r.Name, InstalledBase__r.SerialNo__c, 
                	InstalledBase__r.ArticleEAN__c, InstalledBase__r.IdExt__c,ErrorText__c, RecordTypeId
                    from OrderItem__r
                   where RecordTypeId in (Select Id from RecordType where DeveloperName in ( 'SubEquipment' , 'Equipment') and SobjectType =:(SCfwConstants.NamespacePrefix + 'SCOrderItem__c'))
                ),
                (select ID, OrderRole__c, AccountNumber__c, Account__r.AccountNumber  from OrderRole__r 
                where OrderRole__c in ('50301', '50303') order by OrderroleNumeric__c asc)
                from SCOrder__c where ID = : orderId];
        if(ol.size() > 0)
        {
            retValue.put('ROOT', ol[0]);
            debug('order: ' + retValue);
        }
        else
        {
            String msg = 'The order with id: ' + orderId + ' could not be found!';
            debug(msg);
            throw new SCfwException(msg);
        }
    }
    
    
    /**
     * Fills the send structure and sends the call to SAP
     * 
     * @param dataMap with the order under the key ROOT
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure
     *           message = data sent to ERP
     *           message_v1 = endpoint 
     *           message_v2 = a return value of web service 
     *           message_v3 = a mandatory fields error
     *
     */
    public override Boolean fillAndSendERPData(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs)
    {
        Boolean retValue = true;
        SCOrder__c order = (SCOrder__c)dataMap.get('ROOT');
        
        // Check if we want to update an DummyEuqipment
        // PMS 37380: EQ-Update unterdrücken, wenn kein EQ da (Aufstellung mit Dummy-EQ im Auftrag)
        if((order.OrderItem__r[0].InstalledBase__r.IdExt__c == null ||
        	order.OrderItem__r[0].InstalledBase__r.IdExt__c == ''  ) &&
        	(order.OrderItem__r[0].InstalledBase__r.ArticleEAN__c == null ||
        	order.OrderItem__r[0].InstalledBase__r.ArticleEAN__c == '' ) && 
        	(order.OrderItem__r[0].InstalledBase__r.SerialNo__c  == null ||
        	order.OrderItem__r[0].InstalledBase__r.SerialNo__c  == '' ) 
        	)
        {
        	// aboard the callout, give a warning
        	rs.message = 'The OrderUpdate was skipped because \'Dummy equipments\' sould NOT be send to SAP!';
            rs.message_v4 = 'E010';
        	
        }
        // Check if we can update the order
        else if(order.ERPStatusOrderClose__c == 'ok')
        {
            rs.message = 'The OrderEquipmentUpdate was skipped as ERPStatusOrderClose__c is already [ok]';
            rs.message_v4 = 'E001';
        }
        // GMSGB PMS 36271: Auslösen von Interface Schnittstellen verhindern, wenn OderCreate noch nicht zurück ist
        else if (order.ERPStatusOrderCreate__c != 'ok')
        {
        	rs.message = 'The OrderEquipmentUpdate was skipped as ERPStatusOrderCreate__c was not [ok]';
            rs.message_v4 = 'E001';
        }
        // Prevent update if an Equipment is missing. (CCE Demand)
        // solution for array out of bound when no OrderItem is set order.OrderItem__r.size() > 0 &&
        else if(order.OrderItem__r.size() > 0 && order.OrderItem__r[0].InstalledBase__c != null)
        {
            
            // instantiate the client of the web service
            piCceagDeSfdcCSOrderEquipmentUpdate.CustomerServiceOrderEquipmentUpdate_OutPort ws = new piCceagDeSfdcCSOrderEquipmentUpdate.CustomerServiceOrderEquipmentUpdate_OutPort();
            
            // sets the basic authorization for the web service
            ws.inputHttpHeaders_x = u.getBasicAuth();
    
            // sets the endpoint of the web service
            ws.endpoint_x  = u.getEndpoint('OrderEquipmentUpdate');
            debug('endpoint: ' + ws.endpoint_x);
            rs.message_v1 = ws.endpoint_x;
			ws.timeout_x = u.getTimeOut();
            String errormessage = '';
            try
            {
                // instantiate a message header
                piCceagDeSfdcCSOrderEquipmentUpdate.BusinessDocumentMessageHeader messageHeader = new piCceagDeSfdcCSOrderEquipmentUpdate.BusinessDocumentMessageHeader();
                setMessageHeader(messageHeader, order, messageID, u, testMode);
                
                // instantiate the body of the call
                piCceagDeSfdcCSOrderEquipmentUpdate.CustomerServiceOrder_element customerServiceOrder = new piCceagDeSfdcCSOrderEquipmentUpdate.CustomerServiceOrder_element();
                
                // set the data to the body
                // rs.message_v3 = 
                setCustomerServiceOrder(customerServiceOrder, order, u, testMode);
    
                String jsonInputMessageHeader = JSON.serialize(messageHeader);
                String fromJSONMapMessageHeader = getDataFromJSON(jsonInputMessageHeader);
                debug('from json Message Header: ' + fromJSONMapMessageHeader);
    
                String jsonInputOrder = JSON.serialize(customerServiceOrder);
                String fromJSONMapOrder = getDataFromJSON(jsonInputOrder);
                debug('from json Order: ' + fromJSONMapOrder);
                
                rs.message = '\n\nMessageHeader: ' + fromJSONMapMessageHeader + ',\n\nmessageData: ' + fromJSONMapOrder ;
                // check if there are missing mandatory fields
                if(rs.message_v3 == null || rs.message_v3 == '')
                {
                    // All mandatory fields are filled
                    // callout
                    if(!testMode)
                    {
                        // It is not the test from the test class
                        // go
                        errormessage = 'Call ws.CustomerServiceOrderEquipmentUpdate_Out: \n\n';
                        rs.setCounter(1);
                        ws.CustomerServiceOrderEquipmentUpdate_Out(MessageHeader, CustomerServiceOrder);
                        rs.Message_v2 = 'void';
                    }
                }
            }
            catch(Exception e)
            {
                String prevMsg = e.getMessage();
                String newMsg = 'fillAndSendERPData: ' + errormessage + ' \n\n ' + prevMsg;
                debug(newMsg);
                e.setMessage(newMsg);
                throw e;
            }
        }
        else
        {
            // Update ignored/prevented because the Order does not contain any Equipment. 
            // SAP cannot handle updates without equipment.
            rs.message = 'The OrderEquipmentUpdate was aborted because the order does not contain any order item with record type Equipment.\n'
            + 'Insert an Equipment before calling the update.';
        }
        return retValue;
    }
    
     /**
     * sets data into the message header
     * messageID, referenceID = order.Name, CreationDateTime, SenderBusinessSystemID, RecipientBusinessSystemID
     *
     * @param mh message header 
     * @param order 
     * @param messageID MS: milliseconds R: random, e.g.    MS:1351254139745-R:0.7486318721270884
     * @param u CCWSUtil instance
     * @param testMode used to prevent call out by calls from CCWCOrderEquipmentUpdateTest class
     *
     */
    public void  setMessageHeader(piCceagDeSfdcCSOrderEquipmentUpdate.BusinessDocumentMessageHeader mh, 
                                  SCOrder__c order, String messageID, CCWSUtil u, Boolean testMode)
    {
        mh.MessageID = messageID;
//      mh.MessageUUID;
        mh.ReferenceID = order.name;
//      mh.ReferenceUUID;
        mh.CreationDateTime = u.getFormatedCreationDateTime();
        if(testMode)
        {
            mh.TestDataIndicator = 'Test';
        }    
        mh.SenderBusinessSystemID = u.getSenderBusinessSystemID();
        mh.RecipientBusinessSystemID = u.getRecipientBusinessSystemID ();
        debug('Message Header: ' + mh);
    }
    
    
     /**
     * sets the call out order structure with data form an order
     *
     * @param cso callout order structure
     * @param order with input data
     * @param u instance of the util class CCWSUtil
     * @param testMode used to prevent call out by calls from CCWCOrderEquipmentUpdateTest class
     * @return ###
     */
    public String setCustomerServiceOrder(piCceagDeSfdcCSOrderEquipmentUpdate.CustomerServiceOrder_element cso, 
                                        SCOrder__c order, CCWSUtil u, Boolean testMode)
    {
        CCSettings__c ccset = CCSettings__c.getInstance();
 	   

    
        String retValue = '';
        String step = '';
        try
        {
            step = 'set Assembly = SCOrderItem__r[0].InstalledBase__r.ProductModel__r.Name';
            cso.Assembly = order.OrderItem__r[0].InstalledBase__r.ProductModel__r.Name;
            retValue = checkMandatory(retValue, cso.Assembly, 'Assembly');
            
            step = 'set EquipmentNumber = SCOrderItem__r.InstalledBase__r.IdExt__c';
            // it is possible that subEquipments are selected, as sub equipments are allowed as MasterEquipment
            if(order.OrderItem__r.size() < 1)
            {
            	throw new SCfwException('The order has no order item!');
            }
            else if(order.OrderItem__r.size() == 1)
            {
            	cso.EquipmentNumber = order.OrderItem__r[0].InstalledBase__r.IdExt__c;
            }
            else // more than one EQ
            {
            	RecordType rt = [Select Id from RecordType where DeveloperName in ('Equipment') and SobjectType =:(SCfwConstants.NamespacePrefix + 'SCOrderItem__c')];
            	SCOrderItem__c mainOi = null; 
            	for (SCOrderItem__c oi : order.OrderItem__r)
            	{
            		
            		if(oi.RecordTypeId == rt.id)
            		{
            			mainOi = oi;
            		}
            	}
            	
            	if( mainOi == null)
            	{
            		mainOi = order.OrderItem__r[0];
            	}
            	cso.EquipmentNumber = mainOi.InstalledBase__r.IdExt__c;
            }
            
            step = 'set OrderNumber = SCOrder__c.ERPOrderNo';
            cso.OrderNumber = order.ERPOrderNo__c;
            cso.ReferenceNumber = order.Name;

            // 27.11.2012 KU transfer the long text during order update too
            cso.LongText = order.OrderItem__r[0].ErrorText__c;
            
            // 27.11.2012 KU do not pass this information
            if(ccset.SAPOrderShortText__c != null)
            {
                cso.ShortText = ccset.SAPOrderShortText__c;
            }

            // For testing, Orders by the WorkshopOrder have no order roles
            if(order.isWorkshopOrder__c != true) 
            {            
                cso.Partners = new piCceagDeSfdcCSOrderEquipmentUpdate.Partners_element();
                cso.Partners.Partner = new piCceagDeSfdcCSOrderEquipmentUpdate.Partner[2];
                cso.Partners.Partner[0] = new piCceagDeSfdcCSOrderEquipmentUpdate.Partner();
    
                step = 'Setting an invoice recipient';
                cso.Partners.Partner[0].PartnerNumber = getInvoiceRecipient(order);
                retValue = checkMandatory(retValue, cso.Partners.Partner[0].PartnerNumber, 'order.OrderRole__r[IR].Account__r.AccountNumber');                
    
                cso.Partners.Partner[0].PartnerFunction = 'AG';
        
                step = 'Setting a service recipient';
                cso.Partners.Partner[1] = new piCceagDeSfdcCSOrderEquipmentUpdate.Partner();
                cso.Partners.Partner[1].PartnerNumber = order.OrderRole__r[0].Account__r.AccountNumber;
                retValue = checkMandatory(retValue, cso.Partners.Partner[1].PartnerNumber, 'order.OrderRole__r[SR].Account__r.AccountNumber');                
    
                cso.Partners.Partner[1].PartnerFunction = 'WE';          
            }
            else
            {// create empty elements
                cso.Partners = new piCceagDeSfdcCSOrderEquipmentUpdate.Partners_element();
                cso.Partners.Partner = new List<piCceagDeSfdcCSOrderEquipmentUpdate.Partner>();
                //cso.Partners.Partner[0] = new piCceagDeSfdcCSOrderCreate.Partner();
                //cso.Partners.Partner[1] = new piCceagDeSfdcCSOrderCreate.Partner();
            }
            
            step = 'set PriceList = SCOrder__c.PriceList__c ';
            cso.PriceList = order.PriceList__r.ID2__c;

            cso.SalesData = new piCceagDeSfdcCSOrderEquipmentUpdate.SalesData();
            step = 'set order SalesData.DistributionChannel = SCOrder__c.DistributionChannel__c';
            cso.SalesData.DistributionChannel = order.DistributionChannel__c; 
            
            step = 'set SalesData.DistributionChannel = SCOrder__c.Division__c';
            cso.SalesData.SalesDivision = order.Division__c; 
            
            step = 'set SalesData.DistributionChannel = SCOrder__c.SalesOffice__c';
            cso.SalesData.SalesOffice = order.SalesOffice__c;
            cso.SalesData.SalesGroup = order.SalesGroup__c;
            
            step = 'set SalesData.DistributionChannel = SCOrder__c.SalesArea__c';
            cso.SalesData.SalesOrg = order.SalesArea__c;

                        
                                
            step = 'get subequipments from the database';
            SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
            List<SCOrderItem__c> li = [select InstalledBase__r.IdExt__c from SCOrderItem__c 
                where RecordTypeId in 
                    (Select Id from RecordType where DeveloperName in ( 'SubEquipment' , 'EquipmentNew')
                    and SobjectType = :(SCfwConstants.NamespacePrefix + 'SCOrderItem__c'))
                    and Order__r.id =: order.Id];      
                
            
            step = 'set the subequipments';
            cso.SubEquipments = new piCceagDeSfdcCSOrderEquipmentUpdate.SubEquipments_element();
            if(li.size() > 0)
            {

                cso.SubEquipments.EquipmentNumber = new String[li.size()];
                
                for(Integer i = 0; i < li.size(); i++ )
                { 
                    cso.SubEquipments.EquipmentNumber[i] = li.get(i).InstalledBase__r.IdExt__c;
                }
            }

        }
        catch(Exception e)
        {
            String prevMsg = e.getMessage();
            String newMsg = 'setCustomerServiceOrder: ' + step + ' ' + prevMsg;
            debug(newMsg);
            e.setMessage(newMsg);
            throw e;
        }
        return retValue;    
    }
    
    /**
     * Sets the ERPStatusEquipmentUpdate__c in the root object to 'pending'
     * 
     * @param dataMap of objects to dispatch
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure with error messages
     * @param error true if some error has occurred
     */
    public override void setERPStatus(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs, Boolean error)
    {
      if(dataMap != null)
      {
              SCOrder__c order = (SCOrder__c)dataMap.get(ROOT);
              if(order != null)
              {
              	     // Check if we want to update an DummyEuqipment
			        // PMS 37380: EQ-Update unterdrücken, wenn kein EQ da (Aufstellung mit Dummy-EQ im Auftrag)
			        if((order.OrderItem__r[0].InstalledBase__r.IdExt__c == null ||
			        	order.OrderItem__r[0].InstalledBase__r.IdExt__c == ''  ) &&
			        	(order.OrderItem__r[0].InstalledBase__r.ArticleEAN__c == null ||
			        	order.OrderItem__r[0].InstalledBase__r.ArticleEAN__c == '' ) && 
			        	(order.OrderItem__r[0].InstalledBase__r.SerialNo__c  == null ||
			        	order.OrderItem__r[0].InstalledBase__r.SerialNo__c  == '' ) 
			        	)
			        {
			        	// aboard the callout, set status to none
						order.ERPStatusEquipmentUpdate__c = 'none';
						update order;
			        	return;
			        }
			        
        
	                // order updates are only allowed if the order is not yet closed and an InstalledBase is selected
	                // solution for array out of bound when no OrderItem is set order.OrderItem__r.size() > 0 && 
	                // GMSGB PMS 36271: Auslösen von Interface Schnittstellen verhindern, wenn OderCreate noch nicht zurück ist
	                if(order.ERPStatusOrderClose__c != 'ok' && order.ERPStatusOrderCreate__c == 'ok' && order.OrderItem__r.size() > 0 && order.OrderItem__r[0].InstalledBase__c != null)
	                {
	                    if(error)
	                    {
	                        order.ERPStatusEquipmentUpdate__c = 'error';
	                    }
	                    else
	                    {   
	                        order.ERPStatusEquipmentUpdate__c = 'pending';
	                    }   
	                    update order;
	                    debug('order after update: ' + order);
	                }
              }
      }        
    }



    /**
     * gets the account number of the invoice recipient. If there is not such the service recipient is returned instead
     *
     * @param order the order tree with data
     *
     * @return the account number of the invoice recipient
     */
    public String getInvoiceRecipient(SCOrder__c order)
    {
        String retValue = order.OrderRole__r[0].Account__r.AccountNumber;
        if(order.OrderRole__r[1] != null)
        {
            retValue = order.OrderRole__r[1].Account__r.AccountNumber;
        }
        return retValue;
    }
    
    
    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }


}
