public class cc_cceag_test_TestUtils {
	

	public static Account createAccount(Boolean doInsert){
		Account a = new Account(
			Name = 'TestAccount',
			AccountNumber = '11111',
			BillingStreet = '123 E. Fake St',
			BillingState = 'Utah',
			BillingPostalCode = '12223',
			BillingCountry = 'United States',
			DeliveringPlant__c = 'S',
			ShippingTerms__c = '72 Stunden',
			NextPossibleDeliveryDate__c = date.newInstance(2012, 2, 17),
			DeliveryWeekdays__c = 'Sa,Do,Mo'
		);
		if(doInsert)
			insert a;

		return a;
	}

	public static ccrz__E_Product__c createProduct(Boolean doInsert){
		ccrz__E_Product__c product = new ccrz__E_Product__c(
			ccrz__SKU__c = '1111',
			NumberOfSalesUnitsPerPallet__c = 5,
			Name = 'TestProduct',
			ccrz__StartDate__c = date.newInstance(2012, 1, 1),
			ccrz__EndDate__c = date.newInstance(2099, 1, 1),
			ccrz__Quantityperunit__c = 1
		);
		if(doInsert)
			insert product;

		return product;
	}

	public static Contact createContact(Id accountId, Boolean doInsert){
		Contact c = new Contact();
    	c.LastName = 'c_last_name';
    	c.AccountId = accountId;
    	
    	if(doInsert)
    		insert c;

    	return c;
	}

	public static User getPortalUser(Account a){
		
		Contact c = createContact(a.Id, true);

		Profile p = [SELECT Id FROM Profile WHERE Name='CloudCraze Customer Community User']; 
      	User u2 = new User(
      		Alias = 'cceagT', 
      		Email='cceag_newuser_test_prod@testorg.com', 
	        EmailEncodingKey='UTF-8', 
	        LastName='test_prod', 
	        LanguageLocaleKey='en_US', 
	        LocaleSidKey='en_US', 
	        ProfileId = p.Id, 
	        ContactId = c.Id,
	        TimeZoneSidKey='America/Los_Angeles', 
	        UserName='cceag_newuser_test_prod@cceag_test_prod.com');

      	return u2;
	}

	public static ccrz__E_ContactAddr__c createAddress(String accountNumber, Boolean doInsert){
		ccrz__E_ContactAddr__c billTo = new ccrz__E_ContactAddr__c(
			ccrz__AddressFirstline__c='100 Pine Street', 
			ccrz__City__c='SmallTown', 
			ccrz__DaytimePhone__c='(847) 555-1212',
			ccrz__FirstName__c='Test', 
			ccrz__HomePhone__c='(847) 555-1212', 
			ccrz__LastName__c='User',
			ccrz__PostalCode__c='60601', 
			ccrz__State__c='Idaho', 
			ccrz__StateISOCode__c='ID',
			ccrz__Partner_Id__c = accountNumber, 
			ccrz__CountryISOCode__c='USA');

		if(doInsert)
			insert billTo;

		return billTo;
	}

	public static ccrz__E_Cart__c createCart(Boolean doInsert){
		return createCart(null, doInsert);
	}

	public static ccrz__E_Cart__c createCart(Account a, Boolean doInsert){

		ccrz__E_ContactAddr__c billTo = createAddress(a.AccountNumber, true);
		ccrz__E_ContactAddr__c shipTo = createAddress(a.AccountNumber, true);

		ccrz__E_Cart__c cart = new ccrz__E_Cart__c(
			ccrz__Name__c = 'my_test_cart',
			//ccrz__Contact__c = testUser.ContactId,
			ccrz__BillTo__c = billTo.Id,
			ccrz__ShipTo__c = shipTo.Id,
			ccrz__BuyerEmail__c = 'anemail@test.com',
			ccrz__BuyerFirstName__c = 'buerFirst',
			ccrz__BuyerLastName__c = 'buerLast',
			ccrz__BuyerCompanyName__c = 'aCompany',
			ccrz__BuyerPhone__c = '3305550125',
			ccrz__BuyerMobilePhone__c = '3305550125',
			ccrz__PaymentMethod__c = 'PO',
			ccrz__PONumber__c = '12345',
			ccrz__SessionID__c = 'test session',
			ccrz__storefront__c='DefaultStore',
			ccrz__ActiveCart__c = true,
			ccrz__RequestDate__c = Date.today()
		);
		if(doInsert)
			insert cart;

		return cart;
	}

}
