global with sharing class cc_cceag_bean_Cart {
    public Id sfid {get;set;}
    public String encryptedId { get; public set; }
    public String name { get; public set; }
    public String shippingMethod { get; public set; }
    public String currencyCode { get; set; }
    public Decimal shippingCharge { get; public set; }
    public String shippingInst { get; public set; }
    public Date requestedDate { get; public set; }
    public String requestedDateStr { get; public set; }
    public String requestedDateMonth { get; public set; }
    public String requestedDateDay { get; public set; }
    public String requestedDateYear { get; public set; }
    public Boolean shipComplete { get; public set; }
    public ccrz.cc_bean_MockContactAddress shippingAddress { get; public set; }
    public ccrz.cc_bean_MockContactAddress billingAddress { get; public set; }
    public Map<String, String> extrinsic { get; set; }
    public String deliveryTime { get; set; }
    public String personalOrderId { get; set; }
    public Boolean newsletterSuscribe { get; set; }
    public String buyerEmail {get;public set;}
    public String buyerFirstName {get; public set;}
    public String buyerLastName {get; public set;}
    public String buyerPhone {get; public set;}

    public List<cc_cceag_bean_CartItem> items { get; set; }

    public cc_cceag_bean_Cart() {
        
    }

    public cc_cceag_bean_Cart(ccrz__E_Cart__c cart) {
        sfid = cart.Id;
        name = cart.Name;
        encryptedId = cart.ccrz__EncryptedId__c;
        currencyCode = cart.ccrz__CurrencyISOCode__c;
        shippingMethod = cart.ccrz__ShipMethod__c;
        shippingCharge = cart.ccrz__ShipAmount__c;
        shippingInst = cart.ccrz__ShipTo__r.ccrz__ShippingComments__c;
        requestedDate = cart.ccrz__RequestDate__c;
        if (requestedDate != null) {
            DateTime dt = DateTime.newInstance(requestedDate, Time.newInstance(0, 0, 0, 0));
            requestedDateStr = requestedDate.format();
        }
        if (cart.ccrz__ShipTo__c != null)
            shippingAddress = buildAddressBean(cart.ccrz__ShipTo__r);
        if (cart.ccrz__BillTo__c != null)
            billingAddress = buildAddressBean(cart.ccrz__BillTo__r);
        shipComplete = cart.ccrz__ShipComplete__c;
        try{buyerEmail = cart.ccrz__BuyerEmail__c;}catch(SObjectException e){}
        try{buyerFirstName = cart.ccrz__BuyerFirstName__c;}catch(SObjectException e){}
        try{buyerLastName = cart.ccrz__BuyerLastName__c;}catch(SObjectException e){}
        try{buyerPhone = cart.ccrz__BuyerPhone__c;}catch(SObjectException e){}
        try {
            items = new List<cc_cceag_bean_CartItem>();
            for (ccrz__E_CartItem__c cartItem: cart.ccrz__E_CartItems__r) {
                items.add(new cc_cceag_bean_CartItem(cartItem));
            }
        }
        catch(SObjectException e){/*System.debug(e);*/}
    }

    public ccrz.cc_bean_MockContactAddress buildAddressBean(ccrz__E_ContactAddr__c addr) {
        ccrz.cc_bean_MockContactAddress addressBean = new ccrz.cc_bean_MockContactAddress();
        addressBean.sfid = addr.Id;
        addressBean.address1 = addr.ccrz__AddressFirstline__c;
        addressBean.city = addr.ccrz__City__c;
        addressBean.country = addr.ccrz__Country__c;
        addressBean.countryCode = addr.ccrz__CountryISOCode__c;
        addressBean.companyName = addr.ccrz__CompanyName__c;
        addressBean.postalCode = addr.ccrz__PostalCode__c;
        addressBean.stateCode = addr.ccrz__StateISOCode__c;
        addressBean.state = addr.ccrz__State__c;
        addressBean.partnerId = addr.ccrz__Partner_Id__c;
        return addressBean;
    }

    global with sharing class cc_cceag_bean_CartItem {
        public ccrz.cc_bean_MockProduct mockProduct { get; set; }
        public Decimal quantity { get; set; }

        public cc_cceag_bean_CartItem(ccrz__E_CartItem__c cartItem) {
            mockProduct = new ccrz.cc_bean_MockProduct(cartItem.ccrz__Product__r);
            quantity = cartItem.ccrz__Quantity__c;
        }
    }

}
