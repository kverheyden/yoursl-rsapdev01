/**
 * @(#)SCInterfaceClearing
 * Copyright 2012 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

 /**
 * Implements a wrapper for the functionaliy that calls the SAP order close. 
 * @author Georg Birkenheuer <gbirkenheuer@gms-online.de>
 */
public with sharing class SCInterfaceClearingSAPCloseUtil {
	
	/*
	* Calls the SAP order close web call.
	* Select the implmented Class for the interface SCInterfaceClearingSAPClose
	* - For Coke this is SCInterfaceClearingSAPCloseCCE
	* Returns true if the WebService call is made, else false
	* @param SCboOrder boOrder
	* 
	*/
	public static Boolean orderSAPClose(SCboOrder boOrder)
	{
        

        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        
        String className = appSettings.INTERFACE_CLEARING__c;
        if(className == null)
        {
             system.debug('#### InterfaceClearing - missing SCApplicationSetting__c.INTERFACE_CLEARING__c - using SCInterfaceClearingSAPCloseCCE as default');
             className = 'SCInterfaceClearingSAPCloseCCE';
        }
        
        Type t = Type.forName(className);
        SCInterfaceClearingSAPClose interfaceClearingClose = (SCInterfaceClearingSAPClose)t.newInstance();
        if(interfaceClearingClose != null)
        {
            // Then call the clearin -> true means successfull / false means failure
            
            system.debug('#### InterfaceClearing - order: ' + boOrder.order.Id);
            
            String closeIfFollowup = appSettings.FOLLOW_UP_ACTION_CLOSE_ORDER__c;
            if(closeIfFollowup != null && boOrder.order.Followup1__c != null && 
               closeIfFollowup.contains(boOrder.order.Followup1__c))
            {
                system.debug('#### InterfaceClearing - close order: ' + boOrder.order.Id);
                interfaceClearingClose.orderSAPClose(boOrder.order.Id);
                return true;
            }
            else
            {
                system.debug('#### InterfaceClearing - order will not be closed: ' + boOrder.order.Id + ' followup = '  + boOrder.order.Followup1__c);
            }
         }
         return false;
	}
}
