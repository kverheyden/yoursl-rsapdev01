/*
 * @(#)SCboTimeReportTest.cls
 *
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author Dietrich Herdt <dherdt@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCboTimeReportTest
{
    /**
     * SCboTimeReport under positiv test 1
     */
    static testMethod void boTimeReportPositiv1()
    {
        SCHelperTestClass2.createTimeReportSet(true);

        SCboTimeReport boTimeReport = new SCboTimeReport();

        Test.startTest();

        boTimeReport.readOpenDays(SCHelperTestClass.resources.get(0).Id);        
        boTimeReport.readOpenDays2(SCHelperTestClass.resources.get(0).Id);
        
        // no open days in the past with NL resource
        boTimeReport.readOpenDays(SCHelperTestClass.resources.get(4).Id);
        boTimeReport.readOpenDays2(SCHelperTestClass.resources.get(4).Id);

        Datetime myDateBegin = datetime.newInstance(2010, 9, 1, 0, 0, 0);
        Datetime myDateEnd = datetime.newInstance(2010, 9, 1, 23, 59, 59);

        boTimeReport.readOpenDayStart(SCHelperTestClass.resources.get(0).Id,
                         myDateBegin, myDateEnd);

        boTimeReport.readAllDayStart(SCHelperTestClass.resources.get(0).Id,
                         myDateBegin, myDateEnd);

        boTimeReport.readAllEmplTimereports(SCHelperTestClass.resources.get(0).Id,
                         myDateBegin, myDateEnd);

        // read all opened time report for one resource Id
        boTimeReport.readAllEmplTimereports3(SCHelperTestClass.resources.get(0).Id,
                         myDateBegin, myDateEnd, SCfwConstants.DOMVAL_TIMEREPORTSTATUS_OPENED);

        Test.stopTest();
    }

    /**
     * SCboTimeReport under positiv test 2
     */
    static testMethod void boTimeReportPositiv2()
    {
        SCHelperTestClass2.createTimeReportSet(true);

        SCboTimeReport boTimeReport = new SCboTimeReport();

        Test.startTest();

        boTimeReport.readByLink(SCHelperTestClass.resources.get(0).Id,
                                SCHelperTestClass2.timeReportDay1.get(0).Id);

        // get last mileage test
        System.assertequals(12987,
        SCboTimeReport.getLastMileageEntry(SCHelperTestClass.resources.get(0).Id));

        Test.stopTest();
    }

    /**
     * SCboTimeReport under positiv test 3
     */
    static testMethod void boTimeReportPositiv3()
    {
        SCHelperTestClass2.createTimeReportSet(true);

        Test.startTest();

        SCboTimeReport boTimeReport = new SCboTimeReport(SCHelperTestClass2.timeReportDay1.get(0));

        Test.stopTest();
    }
    
    /**
     * SCboTimeReport round minutes test
     */
    static testMethod void roundMinutesTest()
    {

        Test.startTest();

        // time round method positiv test
        Datetime myDate = datetime.newInstance(2010, 9, 1, 14, 35, 59);
        Datetime proofDate = datetime.newInstance(2010, 9, 1, 14, 45, 00);
        Datetime proofDateRound61 = datetime.newInstance(2010, 9, 1, 15, 00, 00);        

        System.assertequals(proofDate,
                            SCboTimeReport.roundMinutes(myDate, 15));
        
        // test with minutes greater 60
        System.assertequals(proofDateRound61,
                            SCboTimeReport.roundMinutes(myDate, 61));
                            
                            
        
        Test.stopTest();
    }

    /**
     * SCboTimeReport overlap time report positiv test
     */
    static testMethod void overlapTimeReportPositiv()
    {
        SCHelperTestClass2.createTimeReportSet(true);

        SCboTimeReport boTimeReport = new SCboTimeReport();

        SCTimeReport__c testTimeReportOverlap =
            new SCTimeReport__c(ID2__c = 'testTimeReport-1',  //GMSTest1
              Resource__c = SCHelperTestClass.resources.get(0).Id,
              Start__c = Datetime.newInstance(2010, 9, 1, 7, 30, 0),
              End__c = Datetime.newInstance(2010, 9, 1, 8, 15, 0),
              Type__c = SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYEND,
              //Order__c = '',
              Status__c = SCfwConstants.DOMVAL_TIMEREPORTSTATUS_CLOSED,
              CompensationType__c = SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_DEFAULT,
              Description__c = 'day end',
              ReportLink__c = null,
              Assignment__c = null,
              Calendar__c = null);

        SCTimeReport__c testTimeReportNotOverlap =
            new SCTimeReport__c(ID2__c = 'testTimeReport-2',  //GMSTest1
              Resource__c = SCHelperTestClass.resources.get(0).Id,
              Start__c = Datetime.newInstance(2010, 9, 1, 7, 30, 0),
              End__c = Datetime.newInstance(2010, 9, 1, 8, 0, 0),
              Type__c = SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYEND,
              //Order__c = '',
              Status__c = SCfwConstants.DOMVAL_TIMEREPORTSTATUS_CLOSED,
              CompensationType__c = SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_DEFAULT,
              Description__c = 'day end',
              ReportLink__c = null,
              Assignment__c = null,
              Calendar__c = null);

        //Datetime dateRight = datetime.newInstance(2010, 9, 1, 8, 0, 0);
        String dateRightString = '08:00';

        Test.startTest();

        // overlap with day start
        System.assertequals(dateRightString,
                            SCboTimeReport.overlapTimeReport(testTimeReportOverlap));

        // not overlap with daystart, end = start
        System.assertequals(null,
                            SCboTimeReport.overlapTimeReport(testTimeReportNotOverlap));

        Test.stopTest();
    }

    /**
     * SCboTimeReport under negativ test 1
     */
    static testMethod void boTimeReportNegativ()
    {
        SCHelperTestClass2.createTimeReportSet(true);

        SCboTimeReport boTimeReport = new SCboTimeReport();

        Test.startTest();
        try
        {
            // date is 01.11.1900 ---> no time report at this day!
            Datetime myDateBegin = datetime.newInstance(1900, 11, 1, 0, 0, 0);
            Datetime myDateEnd = datetime.newInstance(1900, 11, 1, 23, 59, 59);

            boTimeReport.readOpenDayStart(SCHelperTestClass.resources.get(0).Id,
                         myDateBegin, myDateEnd);

            // day without time reports!
            System.assert(false);
        }
        catch (Exception e)
        {
            // nothing found, all ok
            System.assert(true);
        }
        Test.stopTest();
    }

    /**
     * SCboTimeReport under positiv test getMileageEntryBefore()
     */
    static testMethod void boTimeReportGetMileageEntryBefore()
    {
        SCHelperTestClass2.createTimeReportSet(true);

        System.debug('### TEST list: '+SCHelperTestClass2.timeReportDay1);

        SCTimeReport__c testTimeReport = [select Id,
                                                 ID2__c,
                                                 Mileage__c,
                                                 Start__c,
                                                 End__c,
                                                 ReportLink__c,
                                                 Resource__c
                                            from SCTimeReport__c
                                          where ID2__c = 'timeReportDay1_5'];

        System.debug ('#### Test time report '+testTimeReport);

        Test.startTest();

        // get mileage before test, before was 12987, current is 12997
        System.assertequals(12997,
        SCboTimeReport.getMileageEntryBefore(testTimeReport));

        Test.stopTest();
    }

    /**
     * SCboTimeReport time report before day start positiv test
     */
    static testMethod void beforeTimeReportBeginPositiv()
    {
        SCHelperTestClass2.createTimeReportSet(true);

        SCTimeReport__c testTimeReportBeforeDayStart =
            new SCTimeReport__c(ID2__c = 'testTimeReport-Test-1',
              Resource__c = SCHelperTestClass.resources.get(0).Id,
              Start__c = Datetime.newInstance(2010, 9, 1, 7, 0, 0),
              End__c = Datetime.newInstance(2010, 9, 1, 7, 30, 0),
              Type__c = SCfwConstants.DOMVAL_TIMEREPORTTYPE_STANDBY,
              //Order__c = '',
              Status__c = SCfwConstants.DOMVAL_TIMEREPORTSTATUS_OPENED,
              CompensationType__c = SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_DEFAULT,
              Description__c = 'drive time before day starts',
              ReportLink__c = SCHelperTestClass2.timeReportDay1.get(0).Id,
              Assignment__c = null,
              Calendar__c = null);

        String dateRightString = '08:00';

        Test.startTest();

        // day start should be after the test time report
        System.assertEquals(dateRightString,
           SCboTimeReport.beforeTimeReportBegin(testTimeReportBeforeDayStart));
        Test.stopTest();
    }

    /**
     * SCboTimeReport under positiv test, method optimize()
     */
    static testMethod void optimizePositiv()
    {

        SCHelperTestClass2.createTimeReportSet(true);
        SCboTimeReport boTimeReport = new SCboTimeReport();

        // optimize mileage by day start Id
        System.debug('### optimizeTest with ID2__c: '+SCHelperTestClass2.timeReportDay1.get(0).ID2__c);
        Test.startTest();
        boTimeReport.optimizeMileage(SCHelperTestClass2.timeReportDay1.get(0).Id);

        SCTimeReport__c optimizedTimeReport = [select Id, ID2__c, Mileage__c from
                                                  SCTimeReport__c
                                               where ID2__c = 'timeReportDay1_3'];
        // the last time report entry should contains the mileage sum 12987+20
        System.assertEquals(13007,
        optimizedTimeReport.Mileage__c.intValue());

        Test.stopTest();
    }

    /**
     * SCboTimeReport under positiv test, method giveTimeReportsByStart()
     */
    static testMethod void giveTimeReportsByStartPositiv()
    {

        SCHelperTestClass2.createTimeReportSet(true);

        SCboTimeReport boTimeReport = new SCboTimeReport();
        List <SCTimeReport__c> testTimeReportList = new List <SCTimeReport__c> ();

        Test.startTest();
        // give all time reports by day start
        testTimeReportList =
        boTimeReport.giveTimeReportsByStart(SCHelperTestClass2.timeReportDay1.get(0).Id);

        // the first test time report containts four time reports
        System.debug('#### giveTimeReportsByStart --> list size: '+testTimeReportList.size());
        //System.assertEquals(4, testTimeReportList.size());

        String lastTimeReportIntoListId = [select Id, ID2__c, Mileage__c from
                                                  SCTimeReport__c
                                               where ID2__c = 'timeReportDay1_3'].Id;

        // last ID2__c should be 'timeReportDay1-3'
        System.assertEquals(lastTimeReportIntoListId,
                            testTimeReportList.get(testTimeReportList.size()-1).Id);

        Test.stopTest();
    }

    /**
     * SCboTimeReport under positiv test, method readAllOpenDays()
     */
    static testMethod void readAllOpenDaysPositiv()
    {

        SCHelperTestClass2.createTimeReportSet(true);

        SCboTimeReport boTimeReport = new SCboTimeReport();
        List <SCTimeReport__c> testTimeReportList = new List <SCTimeReport__c> ();

        Test.startTest();

        // give all time reports by resource
        testTimeReportList =
        boTimeReport.readAllOpenDays(SCHelperTestClass.resources.get(0).Id);

        // only one open day for that resource
        System.debug('#### giveTimeReportsByStart --> list size: '+testTimeReportList.size());
        System.assertEquals(1, testTimeReportList.size());

        Test.stopTest();
    }

    /**
     * SCboTimeReport under positiv test, methods readLastClosedDay() and readLastTimeReport()
     */
    static testMethod void readLastClosedDayPositiv()
    {

        SCHelperTestClass2.createTimeReportSet(true);

        SCboTimeReport boTimeReport = new SCboTimeReport();
        SCTimeReport__c testTimeReport = new SCTimeReport__c ();
        Datetime testEndDateNull = Datetime.newInstance(2010, 9, 1, 11, 30, 0);
        Datetime testEndDateOne = Datetime.newInstance (2010, 9, 2, 9, 30, 0);

        Test.startTest();

        try
        {
            testTimeReport =
            boTimeReport.readLastClosedDay(SCHelperTestClass.resources.get(0).Id);
            // closed day for that resource found --> error
            System.assert(false);
        }
        catch (Exception e)
        {
            // no closed day found, all ok
            System.assert(true);
        }

        testTimeReport =
            boTimeReport.readLastTimeReport(SCHelperTestClass.resources.get(0).Id);
        // last time report end for that resource at testEndDateNull
        System.assertEquals(testEndDateNull, testTimeReport.End__c);

        testTimeReport =
            boTimeReport.readLastClosedDay(SCHelperTestClass.resources.get(1).Id);
        // day end for that resource at testEndDateOne
        System.assertEquals(testEndDateOne, testTimeReport.End__c);

        Test.stopTest();
    }

    /**
     * SCboTimeReport under positiv test, methods timeReportCompare()
     */
    static testMethod void timeReportComparePositiv()
    {

        SCHelperTestClass2.createTimeReportSet(true);

        SCboTimeReport boTimeReport = new SCboTimeReport();

        // test time report is the same as timeReportDay1_7
        SCTimeReport__c testTimeReportTrue = new SCTimeReport__c(ID2__c = 'test one',
                            Resource__c = SCHelperTestClass.resources.get(1).Id,
                            Start__c = Datetime.newInstance(2010, 9, 2, 9, 30, 0),
                            End__c = Datetime.newInstance(2010, 9, 2, 9, 30, 0),
                            Type__c = SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART,
                            Status__c = SCfwConstants.DOMVAL_TIMEREPORTSTATUS_OPENED,
                            CompensationType__c = SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_DEFAULT,
                            Description__c = 'day open',
                            Mileage__c = 12997,
                            Calendar__c = null);

        SCTimeReport__c testTimeReportFalse = new SCTimeReport__c(ID2__c = 'test two',
                            Resource__c = SCHelperTestClass.resources.get(1).Id,
                            Start__c = Datetime.newInstance(2010, 9, 2, 15, 30, 0),
                            End__c = Datetime.newInstance(2010, 9, 2, 17, 30, 0),
                            Type__c = SCfwConstants.DOMVAL_TIMEREPORTTYPE_OTHERS,
                            Status__c = SCfwConstants.DOMVAL_TIMEREPORTSTATUS_OPENED,
                            CompensationType__c = SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_DEFAULT,
                            Description__c = 'break',
                            Mileage__c = 13997,
                            Calendar__c = null);
                            
        Test.startTest();

        boolean compareTimeReports = boTimeReport.timeReportCompare(testTimeReportTrue);
        System.assertEquals(true, compareTimeReports);
        
        compareTimeReports = boTimeReport.timeReportCompare(testTimeReportFalse);
        System.assertEquals(false, compareTimeReports);
        
        Test.stopTest();
    }
    
    /**
     * SCboTimeReport under positiv test, method overlapTimeReportEx()
     */
    static testMethod void overlapTimeReportExPositiv()
    {

        SCHelperTestClass2.createTimeReportSet(true);

        // test time report is the same as timeReportDay1_7        
        SCTimeReport__c testTimeReportTrue = new SCTimeReport__c(ID2__c = 'test one true',
                            Resource__c = SCHelperTestClass.resources.get(0).Id,
                            Start__c = Datetime.newInstance(2010, 9, 1, 11, 00, 0),
                            End__c = Datetime.newInstance(2010, 9, 1, 12, 30, 0),
                            Type__c = SCfwConstants.DOMVAL_TIMEREPORTTYPE_STANDBY,
                            //Order__c = '',
                            Status__c = SCfwConstants.DOMVAL_TIMEREPORTSTATUS_OPENED,
                            CompensationType__c = SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_DEFAULT,
                            Description__c = 'stand by',
                            ReportLink__c = SCHelperTestClass2.timeReportDay1.get(0).Id,
                            Assignment__c = null);
                            //Calendar__c = null);

        SCTimeReport__c testTimeReportFalse = new SCTimeReport__c(ID2__c = 'test one false', 
                            Resource__c = SCHelperTestClass.resources.get(0).Id,
                            Start__c = Datetime.newInstance(2010, 9, 1, 11, 30, 0),
                            End__c = Datetime.newInstance(2010, 9, 1, 12, 30, 0),
                            Type__c = SCfwConstants.DOMVAL_TIMEREPORTTYPE_STANDBY,
                            //Order__c = '',
                            Status__c = SCfwConstants.DOMVAL_TIMEREPORTSTATUS_OPENED,
                            CompensationType__c = SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_DEFAULT,
                            Description__c = 'stand by',
                            ReportLink__c = SCHelperTestClass2.timeReportDay1.get(0).Id,
                            Assignment__c = null);
                            //Calendar__c = null);
                            
        Test.startTest();

        String overlapTimeTrue = SCboTimeReport.overlapTimeReportEx(testTimeReportTrue);
        System.assertEquals('11:30', overlapTimeTrue);
        
        String overlapTimeFalse = SCboTimeReport.overlapTimeReportEx(testTimeReportFalse);
        System.assertEquals(null, overlapTimeFalse);
        
        Test.stopTest();
    }

    /**
     * SCboTimeReport under positiv test, method giveTimeReportAfter()
     */
    static testMethod void giveTimeReportAfterPositiv()
    {

        SCHelperTestClass2.createTimeReportSet(true);

        SCboTimeReport boTimeReport = new SCboTimeReport();

        // test time report is the same as timeReportDay1_7
        SCTimeReport__c testTimeReportTrue = new SCTimeReport__c(ID2__c = 'test one true',
                            Resource__c = SCHelperTestClass.resources.get(1).Id,
                            Start__c = Datetime.newInstance(2010, 9, 2, 6, 0, 0),
                            End__c = Datetime.newInstance(2010, 9, 2, 9, 0, 0),
                            Type__c = SCfwConstants.DOMVAL_TIMEREPORTTYPE_STANDBY,
                            //Order__c = '',
                            Status__c = SCfwConstants.DOMVAL_TIMEREPORTSTATUS_OPENED,
                            CompensationType__c = SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_DEFAULT,
                            Description__c = 'stand by overlaps day begin after',
                            Mileage__c = 12987,
                            Calendar__c = null);
                            
        SCTimeReport__c testTimeReportFalse = new SCTimeReport__c(ID2__c = 'test one false',
                            Resource__c = SCHelperTestClass.resources.get(1).Id,
                            Start__c = Datetime.newInstance(2010, 9, 2, 6, 0, 0),
                            End__c = Datetime.newInstance(2010, 9, 2, 8, 0, 0),
                            Type__c = SCfwConstants.DOMVAL_TIMEREPORTTYPE_STANDBY,
                            //Order__c = '',
                            Status__c = SCfwConstants.DOMVAL_TIMEREPORTSTATUS_OPENED,
                            CompensationType__c = SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_DEFAULT,
                            Description__c = 'stand by no overlaps day begin after',
                            Mileage__c = 12987,
                            Calendar__c = null);                            
        Test.startTest();

        String overlapTimeTrue = SCboTimeReport.giveTimeReportAfter(testTimeReportTrue);
        System.assertEquals('08:00', overlapTimeTrue);
        
        String overlapTimeFalse = SCboTimeReport.giveTimeReportAfter(testTimeReportFalse);
        System.assertEquals(null, overlapTimeFalse);
        
        Test.stopTest();
    }

    /**
     * SCboTimeReport under positiv test, method readAllDayStartsWithLink()
     */
    static testMethod void readAllDayStartsWithLinkPositiv()
    {

        SCHelperTestClass2.createTimeReportSet(true);
        SCboTimeReport boTimeReport = new SCboTimeReport();

        Datetime myDateBegin = datetime.newInstance(2010, 9, 1, 0, 0, 0);
        Datetime myDateEnd = datetime.newInstance(2010, 9, 1, 23, 59, 59);
        
        List <String> timeReportDayBeginIds = new List <String> ();
        List <String> timeReportDayBeginIds2 = new List <String> ();

        
        Test.startTest();
        
        timeReportDayBeginIds = boTimeReport.readAllDayStartsWithLink(SCHelperTestClass.resources.get(0).Id,
                         myDateBegin, myDateEnd);

        // test time report with a report link 
        timeReportDayBeginIds2 = boTimeReport.readAllDayStartsWithLink(SCHelperTestClass.resources.get(1).Id,
                         myDateBegin, myDateEnd);
                         
        System.assertEquals(1, timeReportDayBeginIds.size());
        System.assertEquals(0, timeReportDayBeginIds2.size());
        
        Test.stopTest();
    }   

    /**
     * SCboTimeReport under positiv test, method getCompensationType()
     */
    static testMethod void getCompensationTypeTest()
    {
        Datetime dateMon = datetime.newInstance(2011, 3, 14, 0, 0, 0);
        Datetime dateSat = datetime.newInstance(2011, 3, 19, 0, 0, 0);
        Datetime dateSon = datetime.newInstance(2011, 3, 20, 0, 0, 0);
        
        Test.startTest();
        
        String compType = SCboTimeReport.getCompensationType(dateMon);
        System.assertEquals(SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_DEFAULT, compType);
        compType = SCboTimeReport.getCompensationType(dateSat);
        System.assertEquals(SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_SATURDAY, compType);
        compType = SCboTimeReport.getCompensationType(dateSon);
        System.assertEquals(SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_SUNDAY, compType);
        
        Test.stopTest();
    }   
}
