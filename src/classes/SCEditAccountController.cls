/*
 * @(#)SCEditAccountController.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Implements the dialog for editing and geocoding an account. The [Save] button is only active 
 * for person accounts as the business accounts are to edited in EPS.
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCEditAccountController extends SCfwPageControllerBase
{
   
    // Application settings
    public String defCountry { get; set; }
    
    private Pattern gbPhoneValidator = Pattern.compile('^0[0-9]{10}');

    // The account id passed to the popup as input parameter
    public String accId { get; private set; }
    // The account data
    public Account acc { get; set; }

    // Flag used to close the popup dialog when the save button is pressed
    public Boolean ClosePopupDialog { get; private set; }

    //--<Address Validation>--------------------------------------------- 
    public Boolean isAddressValidation { get; private set; }
    
    public List<SCInstalledBaseLocation__c> locations { get; set; }
    public List<SCInstalledBaseLocation__c> locNames { get; set; }
    
    public String saveLocations { get; set; }
    public String locationInfo { get; set; }
    
    public AvsAddress myaddr {get;set;} 
    
    public Boolean isPhoneValid {get;private set;}
    public Boolean isPhone2Valid {get;private set;}
    public Boolean isMobileValid {get;private set;}
    public Boolean isFaxValid {get;private set;}
    private static SCApplicationSettings__c applicationSettings = SCApplicationSettings__c.getInstance();

    public boolean getIsGeocoded()
    {
        //return acc != null && acc.geox__c <> 0;    

        Boolean isGeoCoded = false;

        if (acc != null && acc.GeoX__c != null && acc.GeoY__c != null && (acc.GeoY__c != 0 || acc.GeoX__c != 0) && (String.valueOf(acc.GeoY__c) != '' && String.valueOf(acc.GeoX__c) != ''))
        {
            isGeoCoded = true;
        }

        System.debug('isGeoCoded -> ' + isGeoCoded);

        return isGeoCoded;

    }
    public void setIsGeocoded(boolean value)
    {   
        System.debug('###na setIsGeocoded ' + value);
        if(acc != null && value == false)
        { 
            acc.geox__c = 0;    
            acc.geoy__c = 0;    
        }
    }
    
    /**
     * Default constructor
     */
    public SCEditAccountController()
    {
        accId = ApexPages.currentPage().getParameters().get('aid');
        init();
    } // SCEditAccountController
    
    /**
     * Default constructor
     */
    public SCEditAccountController(String aid)
    {
        accId = aid;
        init();
    } // SCEditAccountController
    
    /**
     * Initialize all necessary data.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public void init()
    {
        defCountry = applicationSettings.DEFAULT_COUNTRY__c;

        // Keep the dialog open until the validation has been completed
        ClosePopupDialog = false;

        // Address Validation - initialize 
        myaddr = new AvsAddress();
        isAddressValidation = false;
        saveLocations = '0';
        locationInfo = '';
        
        this.isPhoneValid = true;
        this.isPhone2Valid = true;
        this.isMobileValid = true;
        this.isFaxValid = true;
        
        acc = new Account();
        
        try
        {
            acc = [Select Id, Name, Name2__c, IsPersonAccount__c, Salutation__c, PersonTitle__c, FirstName__c, LastName__c, 
                          BillingStreet, BillingHouseNo__c, BillingExtension__c, 
                          BillingPostalCode, BillingCity, BillingCounty__c, 
                          BillingCountryState__c, BillingCountry__c, BillingDistrict__c, 
                          Phone, Fax, Phone2__c, Mobile__c, Email__c, 
                          LockType__c, RiskClass__c, VipLevel__c, Description, 
                          GeoX__c, GeoY__c, GeoApprox__c, Type, BillingFloor__c, BillingFlatNo__c
                          from Account where Id = :accId];

            // Addess Validation - make a copy of the data
            /*
            myaddr.country    = acc.BillingCountry__c;
            myaddr.county     = acc.BillingCounty__c;
            myaddr.postalcode = acc.BillingPostalCode;
            myaddr.city       = acc.BillingCity;
            myaddr.street     = acc.BillingStreet;
            myaddr.housenumber= acc.BillingHouseNo__c;
            */
            
            myaddr.extension   = acc.BillingExtension__c;
            myaddr.country     = acc.BillingCountry__c;
            myaddr.county      = acc.BillingCounty__c;
            myaddr.postalcode  = acc.BillingPostalCode;
            myaddr.city        = acc.BillingCity;
            myaddr.street      = acc.BillingStreet;
            myaddr.housenumber = acc.BillingHouseNo__c;
            
            myaddr.salutation  = acc.Salutation__c;
            myaddr.title       = acc.PersonTitle__c;
            myaddr.organisation= acc.name;
            myaddr.firstname   = acc.FirstName__c;
            myaddr.surname     = acc.LastName__c;
            myaddr.telephoneNumber = acc.phone;
            myaddr.faxNumber   = acc.fax;
            myaddr.type        = acc.Type;
            myaddr.district    = acc.BillingDistrict__c;
            myaddr.mobileNumber = acc.Mobile__c;
            myaddr.telephoneNumber2 = acc.Phone2__c;
            myaddr.email       = acc.Email__c;
            myaddr.name2       = acc.Name2__c;
            myaddr.flatno      = acc.BillingFlatNo__c;
            myaddr.floor       = acc.BillingFloor__c;
            myaddr.geoX        = acc.geox__c;
            myaddr.geoY        = acc.geoy__c;
            myaddr.geoApprox   = acc.GeoApprox__c;
            myaddr.description = acc.Description;
            
            System.debug('#### starting to check the location');
            
            // get all locations for the account
            List<SCInstalledBaseLocation__c> accLocs = SCfwInstalledBase.getInstalledBaseLocationByOwner(accId);
            Integer idx = 1;
            String userLang = UserInfo.getLanguage().substring(0,2);
            List<SCOrder__c> orders;
            // look for all locations with the same address of the account
            locations = new List<SCInstalledBaseLocation__c>();
            locNames = new List<SCInstalledBaseLocation__c>();
            for (SCInstalledBaseLocation__c loc :accLocs)
            {
                System.debug('#### starting to loop');
                
                String locStreet = SCBase.isSet(loc.Street__c) ? 
                                            loc.Street__c.replaceAll(' ', '') : '';
                String locHouseNo = SCBase.isSet(loc.HouseNo__c) ? 
                                            loc.HouseNo__c.replaceAll(' ', '') : '';
                String locCity = SCBase.isSet(loc.City__c) ? 
                                            loc.City__c.replaceAll(' ', '') : '';
                String locPostalCode = SCBase.isSet(loc.PostalCode__c) ? 
                                            loc.PostalCode__c.replaceAll(' ', '') : '';
                
                String accStreet = SCBase.isSet(acc.BillingStreet) ? 
                                            acc.BillingStreet.replaceAll(' ', '') : '';
                String accHouseNo = SCBase.isSet(acc.BillingHouseNo__c) ? 
                                            acc.BillingHouseNo__c.replaceAll(' ', '') : '';
                String accCity = SCBase.isSet(acc.BillingCity) ? 
                                            acc.BillingCity.replaceAll(' ', '') : '';
                String accPostalCode = SCBase.isSet(acc.BillingPostalCode) ? 
                                            acc.BillingPostalCode.replaceAll(' ', '') : '';
                
                System.debug('#### locStreet / locHouseNo / locCity / loc.Country__c / acc.BillingCountry__c / locPostalCode : ' + locStreet + ' / ' + locHouseNo + ' / ' + locCity + ' / ' + loc.Country__c + ' / ' + acc.BillingCountry__c + ' / ' + locPostalCode);
                
                System.debug('#### accStreet / accHouseNo / accCity / accPostalCode : ' + accStreet + ' / ' + accHouseNo + ' / ' + accCity + ' / ' + loc.Country__c + ' / ' + acc.BillingCountry__c + ' / ' + accPostalCode);
                
                // if the address of the location is the same as the one of the account
                if (locStreet.equalsIgnoreCase(accStreet) && 
                    locHouseNo.equalsIgnoreCase(accHouseNo) && 
                    locCity.equalsIgnoreCase(accCity) && 
                    (loc.Country__c == acc.BillingCountry__c) && 
                    locPostalCode.equalsIgnoreCase(accPostalCode))
                {
                    // check for each location if there are orders
                    System.debug('#### SCEditAccountController.init(): locId -> ' + loc.Id);
                    orders = SCboInstalledBase.getInstLocDepOrders(loc.Id);
                    System.debug('#### SCEditAccountController.init(): orders -> ' + orders);
                    System.debug('#### SCEditAccountController.init(): size -> ' + orders.size());
                    // if there are orders, generate an information string, which
                    // will be displayed in the page
                    if (orders.size() > 0)
                    {
                        if (locationInfo.length() != 0)
                        {
                            locationInfo += '\\n\\n';
                        } // if (locationInfo.length() != 0)
                        locationInfo += System.Label.SC_app_InstalledBaseLocation + ': ';
                        locationInfo += loc.LocName__c + '\\n';
                        locationInfo += System.Label.SC_msg_NoLocChgWithDepends;
                        locationInfo += '\\n\\n';
                        for (SCOrder__c order :orders)
                        {
                            locationInfo += order.Name + ' (' + SCBase.getPicklistText(SCOrder__c.Status__c, order.Status__c);
                            locationInfo += ' - ' + SCBase.getPicklistText(SCOrder__c.Type__c, order.Type__c) + ')\\n';
                            idx++;
                            if (idx > 10)
                            {
                                locationInfo += '...';
                                break;
                            } // if (idx > 10)
                        } // for (SCOrder__c order :orders)
                    } // if (orders.size() > 0)
                    
                    locations.add(loc);
                } // if (locStreet.equalsIgnoreCase(accStreet) && ...
                
                String accFirstName = acc.FirstName__c;
                String accLastName  = acc.LastName__c;
                
                if((loc.LocName__c == (accFirstName + ' ' + accLastName).trim()) || 
                   (loc.LocName__c == accLastName) || 
                   (loc.LocName__c == (accLastName + ' ' + accFirstName).trim())
                  )
                {
                    locNames.add(loc);
                }
                
            } // for (SCInstalledBaseLocation__c loc :accLocs)
        }
        catch(Exception e)
        {
            ApexPages.addMessages(e);
        }
        // System.debug('##### init(): ' + acc);
    } // init

    /**
     * Checks if there are installed base locations.
     *
     * @return    true if there are locations
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public Boolean getHasLocations() 
    { 
        return (locations.size() > 0);
    } // getHasLocations
    
    /**
     * Cancels the address validation and displayes the normal account page.
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference OnCancelAddrValidation()
    {
        isAddressValidation = false;
        return null;
    } // cancelAddrValidation

    /**
     * Called when the button [Save] is pressed. Checks if the address 
     * was geocoded and saves the account with the updated data.
     */
    public PageReference OnSave()
    {
        //ToDo remove if block after testing controller. 
        //Keep content of else block
        if(defCountry == 'NL')
        {
            System.debug('#### OnSave(): saveLocations -> ' + saveLocations);
            System.debug('#### OnSave(): setIsGeocoded -> ' + getIsGeocoded());
        
            myaddr = new AvsAddress();
            myaddr.country    = acc.BillingCountry__c;
            myaddr.county     = acc.BillingCounty__c;
            myaddr.postalcode = acc.BillingPostalCode;
            myaddr.city       = acc.BillingCity;
            myaddr.street     = acc.BillingStreet;
            myaddr.housenumber= acc.BillingHouseNo__c;
            myaddr.description = acc.Description;
    
            if(getIsGeocoded())
            {
                try
                {
                    upsert acc;
                    if ((locations.size() > 0) && saveLocations.equals('1'))
                    {
                        for (SCInstalledBaseLocation__c loc :locations)
                        {
                            loc.Street__c = acc.BillingStreet;
                            loc.HouseNo__c = acc.BillingHouseNo__c;
                            loc.City__c = acc.BillingCity;
                            loc.Country__c = acc.BillingCountry__c;
                            loc.PostalCode__c = acc.BillingPostalCode;
                            loc.GeoX__c = acc.GeoX__c;
                            loc.GeoY__c = acc.GeoY__c;
                            loc.GeoApprox__c = acc.GeoApprox__c;
                        } // for (SCInstalledBaseLocation__c loc :locations)
                        upsert locations;
                    } // if ((locations.size() > 0) && saveLocations)
                    ClosePopupDialog = true;
                }
                catch(DMLException e)
                {
                    ApexPages.addMessages(e);
                }
            } 
            return null;
        }
        else
        {
            System.debug('#### OnSave(): saveLocations -> ' + saveLocations);
            System.debug('#### OnSave(): setIsGeocoded -> ' + getIsGeocoded());
        
            myaddr = new AvsAddress();
           
            myaddr.extension   = acc.BillingExtension__c;
            myaddr.country     = acc.BillingCountry__c;
            myaddr.county      = acc.BillingCounty__c;
            myaddr.postalcode  = acc.BillingPostalCode;
            myaddr.city        = acc.BillingCity;
            myaddr.street      = acc.BillingStreet;
            myaddr.housenumber = acc.BillingHouseNo__c;
            
            myaddr.salutation  = acc.Salutation__c;
            myaddr.title       = acc.PersonTitle__c;
            myaddr.organisation= acc.name;
            myaddr.firstname   = acc.FirstName__c;
            myaddr.surname     = acc.LastName__c;
            myaddr.telephoneNumber = acc.phone;
            myaddr.faxNumber   = acc.fax;
            myaddr.type        = acc.Type;
            myaddr.district    = acc.BillingDistrict__c;
            myaddr.mobileNumber = acc.Mobile__c;
            myaddr.email       = acc.Email__c;
            myaddr.name2       = acc.Name2__c;
            myaddr.flatno      = acc.BillingFlatNo__c;
            myaddr.floor       = acc.BillingFloor__c;
            myaddr.geoX        = acc.geox__c;
            myaddr.geoY        = acc.geoy__c;
            myaddr.description = acc.Description;
    
            
            if(getIsGeocoded())
            {
                try
                {
                    //Fax, Phone2__c, Mobile__c
                    //ToDo only for GB
                    system.debug('Account country:' + acc.BillingCountry__c);
                    
                    //Phone number validation for GB Accounts
                    /*
                    if(defCountry == 'GB' && acc.BillingCountry__c == 'GB')
                    {
                    
                        if(acc.Phone!=null && (acc.Phone.length() > 0) && !gbPhoneValidator.matcher(acc.Phone).matches()){
                            this.isPhoneValid = false;
                            //throw new IllegalPhoneNumberException('Phonenumber not valid');
                            //ApexPages.addMessages(new ApexPages.Message()
                            acc.Phone.addError(Label.SC_msg_phoneFormatError);
                        }
                        else
                            this.isPhoneValid = true;
                        
                        if(acc.Phone2__c!=null && (acc.Phone2__c.length() > 0) && !gbPhoneValidator.matcher(acc.Phone2__c).matches()){
                            this.isPhone2Valid = false;
                            //throw new IllegalPhoneNumberException('Phonenumber not valid');
                            //ApexPages.addMessages(new ApexPages.Message()
                            acc.Phone2__c.addError(Label.SC_msg_phoneFormatError);
                        }
                        else
                            this.isPhone2Valid = true;
                            
                        if(acc.Mobile__c!=null && (acc.Mobile__c.length() > 0) && !gbPhoneValidator.matcher(acc.Mobile__c).matches()){
                            this.isMobileValid = false;
                            //throw new IllegalPhoneNumberException('Phonenumber not valid');
                            //ApexPages.addMessages(new ApexPages.Message()
                            acc.Mobile__c.addError(Label.SC_msg_phoneFormatError);
                        }
                        else
                            this.isMobileValid = true;
                            
                        if(acc.Fax!=null && (acc.Fax.length() > 0) && !gbPhoneValidator.matcher(acc.Fax).matches()){
                            this.isFaxValid = false;
                            //throw new IllegalPhoneNumberException('Phonenumber not valid');
                            //ApexPages.addMessages(new ApexPages.Message()
                            acc.Fax.addError(Label.SC_msg_phoneFormatError);
                        }
                        else
                            this.isFaxValid = true;
                        
                        //Explicit validation. Cancel save if at least one field is not valid
                        if(!this.isPhoneValid || !this.isPhone2Valid || !this.isMobileValid || !this.isFaxValid)
                        {
                            return null;
                        }
                    }
                    */
                    upsert acc;
                    
                    System.debug('#### upserting Account');
                    
                    if (locations.size() > 0)
                    {
                        for (SCInstalledBaseLocation__c loc :locations)
                        {
                            loc.Street__c = acc.BillingStreet;
                            loc.HouseNo__c = acc.BillingHouseNo__c;
                            loc.City__c = acc.BillingCity;
                            loc.Country__c = acc.BillingCountry__c;
                            loc.PostalCode__c = acc.BillingPostalCode;
                            loc.GeoX__c = acc.GeoX__c;
                            loc.GeoY__c = acc.GeoY__c;
                            loc.GeoApprox__c = acc.GeoApprox__c;                        
                            loc.Floor__c = acc.BillingFloor__c;
                            loc.FlatNo__c = acc.BillingFlatNo__c;
                            loc.Extension__c = acc.BillingExtension__c;
                            loc.District__c = acc.BillingDistrict__c;
                            loc.County__c = acc.BillingCounty__c;
                            
                            if(acc.GeoX__c != null && acc.GeoY__c != null)
                            {
                                loc.GeoStatus__c = 'Geocoded';
                            }
                            
                            for(SCInstalledBaseLocation__c locName :locNames)
                            {
                                if(loc.Id == locName.Id)
                                {
                                    loc.LocName__c = (acc.FirstName__c + ' ' + acc.LastName__c).trim();
                                }
                            }
                            
                        } // for (SCInstalledBaseLocation__c loc :locations)
                        
                        upsert locations;
                    } // if ((locations.size() > 0) && saveLocations)
                    ClosePopupDialog = true;
                }
                catch(DMLException e)
                {
                    ApexPages.addMessages(e);
                }
                catch(Exception ex)
                {
                    ApexPages.addMessages(ex);
                }
               
            } 
            return null;
        }
    } // save
    
   /** 
    * Called by the component to signal the page about data updates
    * Override this method in derived classed to implement component <-> page communication
    * @param ctx  context information optional context information
    * @param data  a data object 
    */    
    public override String OnUpdateData(String ctx, Object data)
    {
        //ToDo remove if block after testing controller. 
        //Keep content of else block
        if(defCountry == 'NL')
        {           
            AvsAddress result = (AvsAddress)data;   
           
            System.debug('###na ' + result);
    
            acc.BillingCountry__c    = result.country;
            acc.BillingCounty__c     = result.county;
            acc.BillingPostalCode = result.postalcode;
            acc.BillingCity       = result.city;
            acc.BillingStreet     = result.street;
            acc.BillingHouseNo__c    = result.housenumber;
            acc.Geox__c              = result.geox;
            acc.Geoy__c              = result.geoy;
            acc.GeoApprox__c         = result.geoApprox;
            acc.Description          = result.description;
            
            myaddr = new AvsAddress();
            myaddr.country    = acc.BillingCountry__c;
            myaddr.county     = acc.BillingCounty__c;
            myaddr.postalcode = acc.BillingPostalCode;
            myaddr.city       = acc.BillingCity;
            myaddr.street     = acc.BillingStreet;
            myaddr.housenumber= acc.BillingHouseNo__c;
            
            return null;
        }       
        else
        {
            AvsAddress result = (AvsAddress)data;   
           
            System.debug('###na ' + result);
    
            acc.BillingCity       = result.city;
            acc.BillingStreet     = result.street;
            acc.BillingCountry__c    = result.country;
            acc.BillingCounty__c     = result.county;
            acc.BillingPostalCode = result.postalCode;
            acc.BillingHouseNo__c    = result.houseNumber;
            acc.GeoX__c              = result.geoX;
            acc.GeoY__c              = result.geoY;
            acc.GeoApprox__c         = result.geoApprox;
            
            acc.Email__c             = result.email;
            acc.Mobile__c            = result.mobileNumber;
            acc.Fax                  = result.faxNumber;
            acc.Name2__c             = result.name2;
            acc.Type                 = result.type;
            acc.BillingDistrict__c   = result.district;
            acc.Description          = result.description;
            acc.Phone2__c            = result.telephoneNumber2;
            
    
            if(acc.GeoX__c != null && acc.GeoY__c != null)
            {
                acc.GeoStatus__c = 'Geocoded';
            }
    
            
            if(result.title != null)
            {    
                acc.PersonTitle__c          = result.title;
            }
            if(result.salutation != null)
            {    
                acc.Salutation__c           = result.salutation;
            }
            if(result.firstname != null)
            {    
                acc.FirstName__c            = result.firstname;
            }
            if(result.surname != null)
            {    
                acc.LastName__c             = result.surname;
            }
            if(result.telephoneNumber != null)
            {
                acc.phone = result.telephoneNumber;
            }
            if(result.faxNumber != null)
            {
                acc.fax = result.faxNumber;
            }
            if(result.birthdate != null)
            {
                acc.PersonBirthdate__c = result.birthdate;
            }
            if(result.extension != null)
            {
                acc.BillingExtension__c  = result.extension;
            }
            if(result.flatno != null)
            {
                acc.BillingFlatNo__c  = result.flatno;
            }
            if(result.floor != null)
            {
                acc.BillingFloor__c  = result.floor;
            }
    
            OnSave();
    
            return null;
        }
        
    }
   
} // SCEditAccountController
