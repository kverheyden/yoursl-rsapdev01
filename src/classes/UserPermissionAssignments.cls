public with sharing class UserPermissionAssignments {


	public static void removeUserFromPublicGroup(Map<Id, User> userMap, String groupDevName) {
		Id groupId = getGroupId(groupDevName);
		if (userMap == null || userMap.isEmpty() || String.isBlank(groupId))
			return;

		delete [SELECT Id FROM GroupMember WHERE GroupId =: groupId AND UserOrGroupId IN: userMap.keySet()];
	}

	public static void assignUserToPublicGroup(Map<Id, User> userMap, String groupDevName) {
		Id groupId = getGroupId(groupDevName);
		if (userMap == null || userMap.isEmpty() || String.isBlank(groupId))
			return;

		List<GroupMember> groupMembers = [SELECT Id, UserOrGroupId, GroupId FROM GroupMember 
			WHERE GroupId =: groupId AND UserOrGroupId IN: userMap.keySet()];

		Set<Id> userGroupMemberIds = new Set<Id>();
		for (GroupMember gm : groupMembers)
			userGroupMemberIds.add(gm.UserOrGroupId);

		Set<Id> usersToAssignIds = new Set<Id>();

		for (Id userId : userMap.keySet())
			if (!userGroupMemberIds.contains(userId))
				usersToAssignIds.add(userId);

		System.debug('CAUTION! ' + usersToAssignIds.size() + ' will be assigned to ' + groupDevName + ' Group');
		assignUserToGroup(usersToAssignIds, groupId);
	}

	private static Id getGroupId(String groupDevName) {
		try {
			Group g = [SELECT Id FROM Group WHERE DeveloperName =: groupDevName LIMIT 1];
			return g == null ? null : g.Id;
		} catch(Exception e) {
			return null;
		}
	}

	private static void assignUserToGroup(Set<Id> userIds, Id groupId) {
		if (userIds.isEmpty() || String.isBlank(groupId))
			return;

		List<GroupMember> gmList = new List<GroupMember>();
		for (Id userId : userIds) {
			GroupMember gm = new GroupMember();
			gm.UserOrGroupId = userId;
			gm.GroupId = groupId;
			gmList.add(gm);
		}
		insert gmList;

	}

	public static void assignPermissionSet(Map<Id, User> userMap, String permissionSetName) {
		if (userMap.isEmpty() || String.isBlank(permissionSetName))
			return;
		PermissionSet ps = [SELECT Id FROM permissionSet WHERE Name =: permissionSetName];
		if (ps == null || ps.Id == null)
			return;

		List<PermissionSetAssignment> permissionSetAssignments = [SELECT Id, PermissionSetId, AssigneeId FROM PermissionSetAssignment
			WHERE PermissionSetId =: ps.Id AND AssigneeId IN: userMap.keySet()];
		
		Set<Id> assignedUserIds = new Set<Id>();
		for (PermissionSetAssignment psa : permissionSetAssignments) 
			assignedUserIds.add(psa.AssigneeId);
		
		Set<Id> usersToAssignIds = new Set<Id>();

		for (Id userId : userMap.keySet()) {
			if (!assignedUserIds.contains(userId))
				usersToAssignIds.add(userId);
		}
		assignPermissionSetById(usersToAssignIds, ps.Id);
	}

	public static void deassignPermissionSet(Map<Id, User> userMap, String permissionSetName) {
		if (userMap.isEmpty() || String.isBlank(permissionSetName))
			return;

		List<PermissionSetAssignment> assignmentsToRemove = [SELECT Id, PermissionSetId, AssigneeId FROM PermissionSetAssignment
			WHERE PermissionSet.Name =: permissionSetName AND AssigneeId IN: userMap.keySet()];
		if (!assignmentsToRemove.isEmpty())
			delete assignmentsToRemove; 
	}

	public static void assignPermissionSetByCustomSetting(Map<Id, User> userMap) {
		if (userMap.isEmpty())
			return;
		SalesAppSettings__c permissionSetId = SalesAppSettings__c.getInstance('PermissionSetId');
		if (permissionSetId == null || String.isBlank(permissionSetId.Value__c))
			return;

		List<PermissionSetAssignment> permissionSetAssignments = [SELECT Id, PermissionSetId, AssigneeId FROM PermissionSetAssignment
			WHERE PermissionSetId =: permissionSetId.Value__c AND AssigneeId IN: userMap.keySet()];
		
		Set<Id> assignedUserIds = new Set<Id>();
		for (PermissionSetAssignment psa : permissionSetAssignments) 
			assignedUserIds.add(psa.AssigneeId);
		
		Set<Id> usersToAssignIds = new Set<Id>();

		for (Id userId : userMap.keySet()) {
			if (!assignedUserIds.contains(userId))
				usersToAssignIds.add(userId);
		}

		System.debug('USERS TO ASSIGN TO PERMISSIONSET ' + usersToAssignIds);
		assignPermissionSetById(usersToAssignIds, permissionSetId.Value__c);
	}

	private static void assignPermissionSetById(Set<Id> userIds, Id permissionSetId) {
		if (userIds.isEmpty() || String.isBlank(permissionSetId))
			return;

		List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();
		for (Id userId : userIds)
			psaList.add( new PermissionSetAssignment(AssigneeId = userId, PermissionSetId = permissionSetId) );

		insert psaList;
	}

	public static void deassignPermissionSetByCustomSetting(Map<Id, User> userMap) {
		if (userMap.isEmpty()) 
			return;
		SalesAppSettings__c permissionSetId = SalesAppSettings__c.getInstance('PermissionSetId');
		if (permissionSetId == null || String.isBlank(permissionSetId.Value__c))
			return;

		delete [SELECT Id, PermissionSetId, AssigneeId FROM PermissionSetAssignment
			WHERE PermissionSetId =: permissionSetId.Value__c AND AssigneeId IN: userMap.keySet()];
	}

}
