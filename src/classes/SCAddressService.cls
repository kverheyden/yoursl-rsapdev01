/*
 * @(#)SCAddressService.cls 
 * 
 * Copyright 2010 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Basic interface that all address validation services have to implement.
 */
public virtual interface SCAddressService
{
    /**
     * Check the query directly and return the result set
     * 
     * @param addr address object with all needed data
     * @return a list of possible addresses
     */
    AvsResult check(AvsAddress addr);

    /**
     * Determines the longitude and latitude for an address.
     * Only required if the external address validation service 
     * does not deliver the geocodes in the addres validateion.
     * 
     * @param addr address object with all needed data
     * @return the address with the longitude and latitude
     */
    AvsResult geocode(AvsAddress addr);
    
}
