/**
* @author		Oliver Preuschl
*
* @description	Update of the related Account when a CCOrder is created
*
* @date			20.11.2014
*
* Timeline:
* Name               Date                      Description
* Oliver Preuschl    20.11.2014				   Created
*
*/

public with sharing class CCOrderHandler {
	
	
	//Updates the related accounts field "LastOrder" and "HasOrderForNextDeliveryDate"
	public static void updateAccounts( List< ccrz__E_Order__c > listCCOrders ){
		try{
			Map< Id, Id > mapLastOrderPerAccount = getLastOrderPerAccount( listCCOrders );
			Map< Id, List< ccrz__E_Order__c > > mapOrdersPerAccount = getOrdersPerAccount( listCCOrders );
			List< Account > listAccounts = getAccounts( mapLastOrderPerAccount.KeySet() );
			
			listAccounts = updateAccountsLastOrder( listAccounts, mapLastOrderPerAccount );
			listAccounts = updateAccountsHasOrderForNextDeliveryDate( listAccounts, mapOrdersPerAccount );

			Database.update( listAccounts, false );
		}catch( Exception LO_Exception ){
			System.debug( LO_Exception );
		}
	}

	//Returns the last order per AccountId
	private static Map< Id, Id > getLastOrderPerAccount( List< ccrz__E_Order__c > listCCOrders ){
		Map< Id, Id > mapLastOrderPerAccount = new Map< Id, Id >();
		for( ccrz__E_Order__c oCCOrder: listCCOrders ){
				mapLastOrderPerAccount.put( oCCOrder.ccrz__Account__c, oCCOrder.Id );
		}

		return mapLastOrderPerAccount;
	}

	//Returnr all orders per AccountId
	private static Map< Id, List< ccrz__E_Order__c > > getOrdersPerAccount( List< ccrz__E_Order__c > listCCOrders ){
		Map< Id, List< ccrz__E_Order__c > > mapOrdersPerAccount = new Map< Id, List< ccrz__E_Order__c > >();
		for( ccrz__E_Order__c oCCOrder: listCCOrders ){
				if( mapOrdersPerAccount.get( oCCOrder.ccrz__Account__c ) == null ){
					mapOrdersPerAccount.put( oCCOrder.ccrz__Account__c, new List< ccrz__E_Order__c >() );
				}
				mapOrdersPerAccount.get( oCCOrder.ccrz__Account__c ).add( oCCOrder );
		}

		return mapOrdersPerAccount;
	}

	//Returns Accounts for specified AccountIds
	private static List< Account > getAccounts( Set< Id > setAccountIds ){
		List< Account > listAccounts = [ SELECT Id, LastOrder__c, NextPossibleDeliveryDate__c, HasOrderForNextDeliveryDate__c FROM Account WHERE ( Id IN :setAccountIds ) ];
		return listAccounts;
	}

	//Update the field "LastOrder" of the related Account
	private static List< Account > updateAccountsLastOrder( List< Account > listAccounts, Map< Id, Id > mapLastOrderPerAccount ){
		//List< Account > listAccounts = [ SELECT Id, LastOrder__c FROM Account WHERE ( Id = :mapLastOrderPerAccount.KeySet() ) ];
		for( Account oAccount: listAccounts ){
			oAccount.LastOrder__c 			= mapLastOrderPerAccount.get( oAccount.Id );
		}
		//Database.update( listAccounts, false );
		return listAccounts;
	}

	//Updates the field "HasOrderForNextDeliveryDate" if a CCOrder exists with  fitting
	private static List< Account > updateAccountsHasOrderForNextDeliveryDate( List< Account > listAccounts, Map< Id, List< ccrz__E_Order__c > > mapOrdersPerAccount ){
		for( Account oAccount: listAccounts ){
			List< ccrz__E_Order__c > listCCOrders = mapOrdersPerAccount.get( oAccount.Id );
			System.debug( 'listCCOrders Size: ' + listCCOrders );
			for( ccrz__E_Order__c oCCOrder: listCCOrders ){
				System.debug( 'Dates: ' + oAccount.NextPossibleDeliveryDate__c + ', ' + oCCOrder.ccrz__RequestDate__c );
				if( ( oAccount.NextPossibleDeliveryDate__c != null ) && ( oAccount.NextPossibleDeliveryDate__c == oCCOrder.ccrz__RequestDate__c ) ){
					oAccount.HasOrderForNextDeliveryDate__c = true;
				}
			}
		}
		return listAccounts;
	}

	//Updates the  accounts field "HasAorderForNextDeliveryDate" if a CCOrder exists with a fitting request date
	public static void updateAccounts( Map< Id, Account > mapOldAccounts,Map< Id, Account > mapNewAccounts ){
		try{
			Map< Id, Account > mapAccounts = filterAccounts( mapOldAccounts, mapNewAccounts );
			updateHasOrderForNextDeliveryDate( mapAccounts );
		}catch( Exception LO_Exception ){
			System.debug( LO_Exception );
		}
	}

	//Updates the related accounts filed "HasAorderForNextDeliveryDate" if a CCOrder exists with a fittingrequest date
	private static void updateHasOrderForNextDeliveryDate( Map< Id, Account > mapAccounts ){
		Map< Id, Account > mapUpdatedAccounts = new Map< Id, Account >();
		Set< Date > setRequestDates = new Set< Date >();
		for( Account oAccount: mapAccounts.Values() ){
			setRequestDates.add( oAccount.NextPossibleDeliveryDate__c );
			Account oUpdatedAccount = new Account(
				Id 								= oAccount.Id,
				HasOrderForNextDeliveryDate__c 	= false
			);
			mapUpdatedAccounts.put( oAccount.Id, oUpdatedAccount );
		}
		List< Account > listAccounts = new List< Account >();
		for( ccrz__E_Order__c oCCOrder: [ SELECT Id, ccrz__Account__c, ccrz__RequestDate__c FROM ccrz__E_Order__c WHERE ( ( ccrz__Account__c IN :mapAccounts.KeySet() ) AND ( ccrz__RequestDate__c IN :setRequestDates ) ) ] ){
			Account oAccount = mapAccounts.get( oCCOrder.ccrz__Account__c );
			if( oAccount.NextPossibleDeliveryDate__c == oCCOrder.ccrz__RequestDate__c ){
				mapUpdatedAccounts.get( oAccount.Id ).HasOrderForNextDeliveryDate__c = true;
			}
		}
		Database.update( mapUpdatedAccounts.Values(), false );
	}

	//Returns only these accounts for which the field "NextPossibleDeliveryDate" was updated
	private static Map< Id, Account > filterAccounts( Map< Id, Account > mapOldAccounts, Map< Id, Account > mapNewAccounts ){
		Map< Id, Account > mapAccounts = new Map< Id, Account >();
		for( Id iAccountId: mapNewAccounts.KeySet() ){
			Account oOldAccount = mapOldAccounts.get( iAccountId );
			Account oNewAccount = mapNewAccounts.get( iAccountId );
			if( oOldAccount.NextPossibleDeliveryDate__c != oNewAccount.NextPossibleDeliveryDate__c ){
				mapAccounts.put( iAccountId, oNewAccount );
			}
		}
		return mapAccounts;
	}

}
