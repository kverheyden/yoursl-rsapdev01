public with sharing class cc_cceag_ctrl_Logout 
{
	public String cokeIdLogoutUrl {get; set;}
	
	public cc_cceag_ctrl_Logout()
	{		
		CCSettings__c settings = CCSettings__c.getInstance();
		this.cokeIdLogoutUrl = settings.ECOMCokeIDLogoutURL__c;
	}
}
