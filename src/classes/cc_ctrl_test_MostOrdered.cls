/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class cc_ctrl_test_MostOrdered {

    static testMethod void myUnitTest() {
    	map<string, integer> skuQty= new map<string, integer>();
    	List<ccrz__E_Product__c> ps = new List<ccrz__E_Product__c>();
    	for(integer i=1; i <= 50; i++) {
    		string sku = 'sku_'+ string.valueOf(i);
    		ps.add(new ccrz__E_Product__c(Name='Product' + string.valueOf(i), ccrz__Sku__c=sku, ccrz__ProductStatus__c='Released', NumberOfSalesUnitsPerPallet__c=12));
    		skuQty.put(sku, 0);
    	}
		insert ps;
		
    	list<ccrz__E_Order__c> orders = new list<ccrz__E_Order__c>();
        for(integer i=1; i <= 10; i++) {
        	orders.add(new ccrz__E_Order__c(ccrz__storefront__c='DefaultStore', ccrz__OrderId__c='MyTestOrder'+string.valueOf(i)));
        }
        insert orders;
        
        list<ccrz__E_OrderItem__c> orderItems = new list<ccrz__E_OrderItem__c>();
        for(ccrz__E_Order__c o : orders) {
        	for(integer j=1; j <= 5 ; j++) {
        		integer skuNum = math.round((Math.random()*ps.size()));
        		skuNum = integer.valueOf(Math.max(1, skuNum));
        		skuNum = integer.valueOf(Math.min(ps.size(), skuNum));
        		string sku = 'sku_' + string.valueOf( skuNum );
        		integer qty =  math.round((Math.random()*20))+1;
        		integer accumulatedQty = skuQty.get(sku);
        		accumulatedQty += qty;
        		skuQty.put(sku, accumulatedQty);
        		orderItems.add(new ccrz__E_OrderItem__c(ccrz__Quantity__c=qty, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c=sku), ccrz__order__c=o.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0));
        	}
        }
		insert orderItems;
		cc_ctrl_MostOrdered mo = new cc_ctrl_MostOrdered();
		string maxSku = null;
		integer maxQty = 0;
		for(string key : skuQty.keyset()) {
			if(skuQty.get(key) >= maxQty) {
				maxQty = skuQty.get(key);
				maxSku = key;
			}
		}
		List<cc_ctrl_MostOrdered.MostOrderedProductBean> beans = cc_ctrl_MostOrdered.getMostOrderedProducts();
		system.assertEquals(maxSku, beans[0].sku);
		system.assertEquals(maxQty, skuQty.get(beans[0].sku));
    }
}
