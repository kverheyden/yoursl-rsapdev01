/*
 * @(#)CCWCAssetInventoryDataRequest.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 *
 * The class exports an Salesforce order item to SAP
 * 
 * The entry method is: 
 *      callout(String oid, boolean async, boolean testMode)
 *
 * Methods to override: 
 *      fillAndSendERPData(...)
 *      fillAndSendERPData(...)
 *
 * @author Georg Birkenheuer <gbirkenheuer@gms-online.de>
 * @version $Revision$, $Date$
 *
 *
 * Test: 
 * Boolean async = true;
 * Boolean testMode = false;
 * CCWCAssetInventoryDataRequest.callout('a0sM0000002LRrq', async, testMode);
 */
public without sharing class CCWCAssetInventoryDataRequest extends SCInterfaceExportBase{
    
    
    public CCWCAssetInventoryDataRequest()
    {
    }
    public CCWCAssetInventoryDataRequest(String interfaceName, String interfaceHandler, String refType, String subclassName)
    {
        super(interfaceName, interfaceHandler, refType, subclassName);
    }
    public CCWCAssetInventoryDataRequest(String interfaceName, String interfaceHandler, String refType)
    {
        //super(interfaceName, interfaceHandler, refType, subclassName);
    }
    
    
    /**
     * Prepare to make the web service callout synchrounously or asynchronously (future)
     *
     * ### Set the interface name, its interface handler and a refType for head id in an interface object!
     *
     * @param ibid      SCInstalledBase__c id
     * @param async     true: use a future method to call 
     *                  false: do a synchrounous callout
     * @param testMode true: emulate call
     */
    public static String callout(String ibid, boolean async, boolean testMode)
    {
        String interfaceName = 'SAP_ASSET_INVENTORY_DATA_REQUEST';
        String interfaceHandler = 'CCWCAssetInventoryDataRequest';
        String refType = 'SCInstalledBase__c';
        CCWCAssetInventoryDataRequest aidr = new CCWCAssetInventoryDataRequest(interfaceName, interfaceHandler, refType);
        return aidr.calloutCore(ibid, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCAssetInventoryDataRequest');
        
    } // callout   
    
     /**
     * Reads an order to send
     *
     * @param installedBaseID
     * @param testMode if true no call is made, because in the test mode the salesforce framework prevent it.
     */
    public override void readSalesforceData(String installedBaseID, Map<String, Object> retValue, Boolean testMode)
    {
        /* 
         * Excemplary Query:
         * Select ID, ID2__c, IDExt__c, IDInt__c, AuditLast__c, AuditHint__c 
         * from SCInstalledBase__c where ID ='a0sD0000001lsQwIAI'
         */
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        List<SCInstalledBase__c> items =  [Select ID, ID2__c, IDExt__c, IDInt__c, 
                AuditLast__c, AuditHint__c, AssetNumber__c,     AssetSubNumber__c
                from SCInstalledBase__c where ID =: installedBaseID];
        if(items.size() > 0)
        {
            retValue.put('ROOT', items[0]);
            debug('order: ' + retValue);
        }
        else
        {
            String msg = 'The InstalledBase with id: ' + installedBaseID + ' could not be found!';
            debug(msg);
            throw new SCfwException(msg);
        }
    }
    
    
    /**
     * Fills the send structure and sends the call to SAP
     * 
     * @param dataMap with the order under the key ROOT
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure
     *           message = data sent to ERP
     *           message_v1 = endpoint 
     *           message_v2 = a return value of web service 
     *           message_v3 = a mandatory fields error
     *
     */
    public override Boolean fillAndSendERPData(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs)
    {
        Boolean retValue = true;
        SCInstalledBase__c installedBase = (SCInstalledBase__c)dataMap.get('ROOT');
        // instantiate the client of the web service
        piCceagDeSfdcCSAssetInventoryDataChange.AssetInventoryDataChangeRequest_OutPort ws = new piCceagDeSfdcCSAssetInventoryDataChange.AssetInventoryDataChangeRequest_OutPort();
        
        // sets the basic authorization for the web service
        ws.inputHttpHeaders_x = u.getBasicAuth();

        // sets the endpoint of the web service
        ws.endpoint_x  = u.getEndpoint('AssetInventoryDataRequest');
        debug('endpoint: ' + ws.endpoint_x);
        rs.message_v1 = ws.endpoint_x;
		ws.timeout_x = u.getTimeOut();
        String errormessage = '';
        try
        {
            // instantiate a message header
            //### piCceagDeSfdcCSAssetInventoryDataChange.MessageHeader messageHeader = new ppiCceagDeSfdcCSAssetInventoryDataChange.MessageHeader();
            //setMessageHeader(messageHeader, order, messageID, u, testMode);
            
            // instantiate the body of the call
            //piCceagDeSfdcCSAssetInventoryDataChange.SFDCResponse_element sfdcResponse = new piCceagDeSfdcCSAssetInventoryDataChange.SFDCResponse_element();
            piCceagDeSfdcCSAssetInventoryDataChange.AssetHeader_element sfdcAssetHeader = new piCceagDeSfdcCSAssetInventoryDataChange.AssetHeader_element();
            
            List<piCceagDeSfdcCSAssetInventoryDataChange.AssetHeader_element>  assetItems = new List<piCceagDeSfdcCSAssetInventoryDataChange.AssetHeader_element>();
            
            // set the data to the body
            // rs.message_v3 = 
            setAssetInventoryDataChange(sfdcAssetHeader, installedBase, u, testMode);

            assetItems.add(sfdcAssetHeader);

            //String jsonInputMessageHeader = JSON.serialize(messageHeader);
            //String fromJSONMapMessageHeader = getDataFromJSON(jsonInputMessageHeader);
            //debug('from json Message Header: ' + fromJSONMapMessageHeader);

            String jsonSfdcAssetHeader = JSON.serialize(sfdcAssetHeader);
            String fromJSONMapOrder = getDataFromJSON(jsonSfdcAssetHeader);
            debug('from json Order: ' + fromJSONMapOrder);
            
            rs.message = '\n messageData: ' + sfdcAssetHeader ;
            // check if there are missing mandatory fields
            if(rs.message_v3 == null || rs.message_v3 == '')
            {
                // All mandatory fields are filled
                // callout
                if(!testMode)
                {
                    // It is not the test from the test class
                    // go
                    errormessage = 'Call ws.AssetInventoryDataChangeRequest_Out: \n\n';
                    rs.setCounter(1);
                    ws.AssetInventoryDataChangeRequest_Out(assetItems);
                    rs.Message_v2 = 'void';
                }
            }
        }
        catch(Exception e)
        {
            String prevMsg = e.getMessage();
            String newMsg = 'fillAndSendERPData: ' + errormessage + ' \n\n ' + prevMsg;
            debug(newMsg);
            e.setMessage(newMsg);
            throw e;

        }
        return retValue;
    }
    
     /**
     * sets data into the message header
     * messageID, referenceID = order.Name, CreationDateTime, SenderBusinessSystemID, RecipientBusinessSystemID
     *
     * @param mh message header 
     * @param order 
     * @param messageID MS: milliseconds R: random, e.g.    MS:1351254139745-R:0.7486318721270884
     * @param u CCWSUtil instance
     * @param testMode used to prevent call out by calls from CCWCOrderEquipmentUpdateTest class
     *
     */
    /*public void  setMessageHeader(piCceagDeSfdcCSOrderEquipmentUpdate.BusinessDocumentMessageHeader mh, 
                                  SCInstalledBase__c installedBase, String messageID, CCWSUtil u, Boolean testMode)
    {
        mh.MessageID = messageID;
//      mh.MessageUUID;
        mh.ReferenceID = installedBase.ID2__c;
//      mh.ReferenceUUID;
        mh.CreationDateTime = u.getFormatedCreationDateTime();
        if(testMode)
        {
            mh.TestDataIndicator = 'Test';
        }    
        mh.SenderBusinessSystemID = u.getSenderBusinessSystemID();
        mh.RecipientBusinessSystemID = u.getRecipientBusinessSystemID ();
        debug('Message Header: ' + mh);
    }*/
    
    
     /**
     * sets the call out order structure with data form an order
     *
     * @param cso callout order structure
     * @param order with input data
     * @param u instance of the util class CCWSUtil
     * @param testMode used to prevent call out by calls from CCWCOrderEquipmentUpdateTest class
     * @return ###
     */
    public String setAssetInventoryDataChange(piCceagDeSfdcCSAssetInventoryDataChange.AssetHeader_element cso, 
                                        SCInstalledBase__c installedBase, CCWSUtil u, Boolean testMode)
    {
        CCSettings__c ccset = CCSettings__c.getInstance();
    
        String retValue = '';
        String step = '';
        try 
        {
            step = 'set AssetNumber = installedBase.AssetNumber__c';
            cso.AssetNumber = installedBase.AssetNumber__c;
                        
            step = 'set AssetSubNumber = installedBase.AssetSubNumber__c';
            cso.AssetSubNumber = installedBase.AssetSubNumber__c;
            
            step = 'set VerificationDate ';
            cso.VerificationDate = u.formatDate(installedBase.AuditLast__c);

            step = 'set Note = installedBase.AuditHint__c';
            cso.Note = installedBase.AuditHint__c;
            

        }
        catch(Exception e)
        {
            String prevMsg = e.getMessage();
            String newMsg = 'setCustomerServiceOrder: ' + step + ' ' + prevMsg;
            debug(newMsg);
            e.setMessage(newMsg);
            throw e;
        }
        return retValue;    
    }
    
    /**
     * Sets the ERPStatusEquipmentUpdate__c in the root object to 'pending'
     * 
     * @param dataMap of objects to dispatch
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure with error messages
     * @param error true if some error has occurred
     */
    public override void setERPStatus(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs, Boolean error)
    {
      if(dataMap != null)
      {
              SCInstalledBase__c ib = (SCInstalledBase__c)dataMap.get(ROOT);
              if(ib != null)
              {
                    if(error)
                    {
                        //ib.ERPStatusEquipmentUpdate__c = 'error';
                    }
                    else
                    {   
                        //ib.ERPStatusEquipmentUpdate__c = 'pending';
                        
                    }   
                    update ib;
                    debug('InstalledBase after update: ' + ib);
              }
      }        
    }



    /**
     * gets the account number of the invoice recipient. If there is not such the service recipient is returned instead
     *
     * @param order the order tree with data
     *
     * @return the account number of the invoice recipient
     */
    /*public String getInvoiceRecipient(SCOrder__c order)
    {
        String retValue = order.OrderRole__r[0].Account__r.AccountNumber;
        if(order.OrderRole__r[1] != null)
        {
            retValue = order.OrderRole__r[1].Account__r.AccountNumber;
        }
        return retValue;
    }*/
    
    
    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }


}
