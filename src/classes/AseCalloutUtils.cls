/**
 * AseCalloutTestService.cls    mt  28.09.2009
 * 
 * Copyright (c) 2009 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 /**
 * AseCalloutUtils includes several utility methods 
 * 
 * @author mt
 * @version $Revision$, $Date$
 */
public class AseCalloutUtils
{
    
    /**
     * returns the given String-dataType as Integer value
     * 
     * @param str the dataType as String
     */
    public static integer getDataType(String str)
    {
        str = str.toLowerCase().trim();

        if (str.equals(AseCalloutConstants.ASE_TYPE_SCHEDULEPARA_STR.toLowerCase()))
        {
            return AseCalloutConstants.ASE_TYPE_SCHEDULEPARA;
        } else if (str.equals(AseCalloutConstants.ASE_TYPE_CALENDAR_STR.toLowerCase()))
        {
            return AseCalloutConstants.ASE_TYPE_CALENDAR;
        } else if (str.equals(AseCalloutConstants.ASE_TYPE_CALENDAR_ENTRY_STR.toLowerCase()))
        {
            return AseCalloutConstants.ASE_TYPE_CALENDAR_ENTRY;
        } else if (str.equals(AseCalloutConstants.ASE_TYPE_LOCATION_STR.toLowerCase()))
        {
            return AseCalloutConstants.ASE_TYPE_LOCATION;
        } else if (str.equals(AseCalloutConstants.ASE_TYPE_ORGUNIT_STR.toLowerCase()))
        {
            return AseCalloutConstants.ASE_TYPE_ORGUNIT;
        } else if (str.equals(AseCalloutConstants.ASE_TYPE_RESOURCE_STR.toLowerCase()))
        {
            return AseCalloutConstants.ASE_TYPE_RESOURCE;
        } 
        else if (str.equals(AseCalloutConstants.ASE_TYPE_USERLOGIN_STR.toLowerCase()))
        {
            return AseCalloutConstants.ASE_TYPE_USERLOGIN;
        }
        else if (str.equals(AseCalloutConstants.ASE_TYPE_APPOINTMENT_STR.toLowerCase()))
        {
            return AseCalloutConstants.ASE_TYPE_APPOINTMENT;
        }
        else if (str.equals(AseCalloutConstants.ASE_TYPE_SERVICECONTROL_STR.toLowerCase()))
        {
            return AseCalloutConstants.ASE_TYPE_APPOINTMENT;
        }
        else
        {
            return AseCalloutConstants.ASE_TYPE_UNKNOWN;
        }
    }
       
    /**
     * returns the given Integer-dataType as String value
     * 
     * @param dataType the dataType as Integer
     */
    public static String getDataTypeAsString(integer dataType)
    {
        String ret = '';
        if (dataType == AseCalloutConstants.ASE_TYPE_SCHEDULEPARA)
        {
            ret = AseCalloutConstants.ASE_TYPE_SCHEDULEPARA_STR;
        } else if (dataType == AseCalloutConstants.ASE_TYPE_CALENDAR)
        {
            ret = AseCalloutConstants.ASE_TYPE_CALENDAR_STR;
        } else if (dataType == AseCalloutConstants.ASE_TYPE_CALENDAR_ENTRY)
        {
            ret = AseCalloutConstants.ASE_TYPE_CALENDAR_ENTRY_STR;
        } else if (dataType == AseCalloutConstants.ASE_TYPE_LOCATION)
        {
            ret = AseCalloutConstants.ASE_TYPE_LOCATION_STR;
        } else if (dataType == AseCalloutConstants.ASE_TYPE_ORGUNIT)
        {
            ret = AseCalloutConstants.ASE_TYPE_ORGUNIT_STR;
        } else if (dataType == AseCalloutConstants.ASE_TYPE_RESOURCE)
        {
            ret = AseCalloutConstants.ASE_TYPE_RESOURCE_STR;
        } 
        else if (dataType == AseCalloutConstants.ASE_TYPE_USERLOGIN)
        {
            ret = AseCalloutConstants.ASE_TYPE_USERLOGIN_STR;
        } 
        else if (dataType == AseCalloutConstants.ASE_TYPE_APPOINTMENT)
        {
            ret = AseCalloutConstants.ASE_TYPE_APPOINTMENT_STR;
        } 
        else if (dataType == AseCalloutConstants.ASE_TYPE_SERVICECONTROL)
        {
            ret = AseCalloutConstants.ASE_TYPE_SERVICECONTROL_STR;
        } 
        else {
            ret = AseCalloutConstants.ASE_TYPE_UNKNOWN_STR;
        }
        return ret.toLowerCase();
    }


    /**
     * returns the class for the given dataType
     * 
     * @param dataType the dataType as Integer
     * @return the corresponding object to the given dataType 
     */
    public static AseCalloutSCBaseInterface getClassObject(integer dataType)
    {
        AseCalloutSCBaseInterface ret = null;

        if (dataType == AseCalloutConstants.ASE_TYPE_SCHEDULEPARA)
        {
            ret = new AseCalloutSCParameter();
        } else if (dataType == AseCalloutConstants.ASE_TYPE_CALENDAR)
        {
            ret = new AseCalloutSCCalendar();
        } else if (dataType == AseCalloutConstants.ASE_TYPE_CALENDAR_ENTRY)
        {
            ret = new AseCalloutSCCalendar();
        } else if (dataType == AseCalloutConstants.ASE_TYPE_LOCATION)
        {
            ret = null;
        } else if (dataType == AseCalloutConstants.ASE_TYPE_ORGUNIT)
        {
            ret = new AseCalloutSCOrgUnit();
        } else if (dataType == AseCalloutConstants.ASE_TYPE_RESOURCE)
        {
            ret = new AseCalloutSCResource();
        } 
        else if (dataType == AseCalloutConstants.ASE_TYPE_USERLOGIN)
        {
            ret = new AseCalloutSCUserLogin();
        } 
        else if (dataType == AseCalloutConstants.ASE_TYPE_APPOINTMENT)
        {
            ret = new AseCalloutSCAppointment();
        } 
        else 
        {
            ret = null;
        }
        if (ret != null) {
            ret.setIsTest(AseServiceSettings.isTest);
        }

        return ret;        
    }
        
    
    
    //######################## AseService ##########################

    /**
     * returns the the aseSOAP Object. The default parameter as
     * the endpoint are already set.
     * 
     * @return the aseSOAP Object to start callouts 
     */
    public static AseService.aseSOAP getAseSoap()
    { 
        AseService.aseSOAP aseService = new AseService.aseSOAP();
        aseService.endpoint_x = AseCalloutConstants.ENDPOINT;
        return aseService;
    }
    
    //######################## dataEntry Utils ##########################

    /**
     * this method adds the key and value to the given dataEntry 
     * 
     * @param key the key of the keyValue-Pair
     * @param value the value of the keyValue-Pair
     * @param dataEntry the object to which the keyValue-pair is added 
     */
    public static void addStringsAsKeyVal2Entry(String key, String val, AseService.aseDataEntry dataEntry)  
    {  
        if (dataEntry.keyValues == null)
            dataEntry.keyValues = new AseService.aseKeyValueType[0]; 
            
        AseService.aseKeyValueType keyVal = new AseService.aseKeyValueType();
        keyVal.key = key;
        keyVal.value = val;
        dataEntry.keyValues.add(keyVal);   
    }
    
    /**
     * this method adds an empty keyValue-pair to the dataEntry.
     * This is useful if you don't have keyValues, but the specification
     * does'nt allow dataEntries without keyValue-pairs. 
     * 
     * @param dataEntry the object to which the empty keyValue-pair is added 
     */
    public static void addEmptyKeyVal2Entry(AseService.aseDataEntry dataEntry)  
    {  
        if (dataEntry.keyValues == null)
            dataEntry.keyValues = new AseService.aseKeyValueType[0]; 
        AseService.aseKeyValueType keyVal = new AseService.aseKeyValueType();
        dataEntry.keyValues.add(keyVal);   
    }
    
    
    //######################## DateUtils ##########################
    
    public static String convertDate2String(DateTime dt)
    {
        return dt.format(AseCalloutConstants.ASE_DATE_FORMAT);       
    }    

    public static String convertDate2String(Date d)
    {       
        DateTime dt = datetime.newInstance(d, time.newInstance(0, 0, 0, 0));
        return convertDate2String(dt);      
    }    
 
    public static boolean dayAndMonthIsGreaterOrEqual(Date d1, Date d2) 
    {
        return dayAndMonthIsGreaterOrEqual(datetime.newInstance(d1, time.newInstance(0, 0, 0, 0)),datetime.newInstance(d2, time.newInstance(0, 0, 0, 0))); 
    }

    public static boolean dayAndMonthIsLess(Date d1, Date d2) 
    {
        return !dayAndMonthIsGreaterOrEqual(datetime.newInstance(d1, time.newInstance(0, 0, 0, 0)),datetime.newInstance(d2, time.newInstance(0, 0, 0, 0))); 
    }
 
    public static boolean dayAndMonthIsGreaterOrEqual(DateTime dt1, DateTime dt2) 
    {
        return dt1.format('MMdd')>=dt2.format('MMdd');
    }
   
    public static boolean dayAndMonthIsLessOrEqual(Date d1, Date d2) 
    {
        return dayAndMonthIsLessOrEqual(datetime.newInstance(d1, time.newInstance(0, 0, 0, 0)),datetime.newInstance(d2, time.newInstance(0, 0, 0, 0))); 
    }
 
    public static boolean dayAndMonthIsLessOrEqual(DateTime dt1, DateTime dt2) 
    {
        return dt1.format('MMdd')<=dt2.format('MMdd');
    }
   
 }
