/*
 * @(#)SCOrderLineControllerTest.cls SCCloud  
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @version $Revision$, $Date$
 */
@isTest
private class SCOrderLineControllerTest
{
    static testMethod void testOrderLineController1()
    {
        try { SCHelperTestClass.createDomsForOrderCreation(); } catch (Exception e) {}
        try { SCHelperTestClass3.createCustomSettings('de', true); } catch (Exception e) {}
        
        SCHelperTestClass.createOrderTestSet3(true);       

        Test.startTest();

        SCOrderLineController con = new SCOrderLineController();
        
        con.oid = SCHelperTestClass.order.Id;
        SCboOrder boOrder = con.boOrder;
        SCTool__c tool = con.priceTool;
        con.getCanAddSpareParts();
        con.getCanAddServiceCharges();
        con.getCanAddPayment();
        con.getCanDelLine();
        con.getCanSelPriceList();
        con.getCanDoPayment();
        con.getCanChgQty();
        con.getCanChgNetPrice();
        con.getCanChgDiscount();
        con.getCanChgInvType();
        con.getCanChgTax();
        con.getCanOrderMat();
        con.getIsOrderClosed();
        con.getHasStock();

        String ordLineType = con.curOrdLineType;
        con.curOrdLineType = ordLineType;
        con.onCalculateSums();
        Decimal sumTax = con.sumLineTax;
        Decimal sumPriceNet = con.sumLinePriceNet;
        Decimal sumPriceGross = con.sumLinePriceGross;
        con.calculatePricing();
        con.onReCalculatePricing();

        System.assertNotEquals(null, con.getPriceLists());
        System.assertNotEquals(null, con.getBoOrderLinesEx());
        System.assertNotEquals(null, con.getOrderLinePrefix());
        //System.assertNotEquals(null, con.getOrderFieldName());
        //System.assertEquals(false, con.isRebook);

        con.selectedArticleId = SCHelperTestClass.articles[0].Id;
        con.updateArticle();

        ApexPages.currentPage().getParameters().put('currentLineRow', '0.0');
        con.curLineRow = 0.0;
        //TODO con.removeOrderLine();
        Double lineRow = con.curLineRow;
        
        con.onAddPayment();
        con.getPaymentFee();
        con.getHasPaymentFee();
        con.doPaymentService();
        con.onResetPaymentFlag();
        con.onRefreshData();
        con.onSave();
        
        con.getShowValuationTypeColumn();
        
        Test.stopTest();
    }

    static testMethod void testOrderLineController2()
    {
        try { SCHelperTestClass.createDomsForOrderCreation(); } catch (Exception e) {}
        try { SCHelperTestClass3.createCustomSettings('de', true); } catch (Exception e) {}
        
        SCHelperTestClass.createOrderTestSet3(true);       
        SCboOrder boOrder = new SCboOrder();
        boOrder.readbyId(SCHelperTestClass.order.Id);

        Test.startTest();

        SCOrderLineController con = new SCOrderLineController();
        
        con.parboOrder = boOrder;
        SCboOrder boOrder2 = con.boOrder;
        SCTool__c tool = con.priceTool;

        String ordLineType = con.curOrdLineType;
        con.curOrdLineType = ordLineType;
        con.onCalculateSums();
        Decimal sumTax = con.sumLineTax;
        Decimal sumPriceNet = con.sumLinePriceNet;
        Decimal sumPriceGross = con.sumLinePriceGross;
        con.calculatePricing();
        con.onReCalculatePricing();

        System.assertNotEquals(null, con.getPriceLists());
        System.assertNotEquals(null, con.getBoOrderLinesEx());
        System.assertNotEquals(null, con.getOrderLinePrefix());
        //System.assertNotEquals(null, con.getOrderFieldName());

        ApexPages.currentPage().getParameters().put('currentLineRow', '0.0');
        con.curLineRow = 0.0;
        con.removeOrderLine();
        Double lineRow = con.curLineRow;

        con.onAddPayment();
        con.getPaymentFee();
        con.doPaymentService();

        con.getShowValuationTypeColumn();
        
        Test.stopTest();
    }

    static testMethod void testOrderLineController3()
    {
        try { SCHelperTestClass.createDomsForOrderCreation(); } catch (Exception e) {}
        try { SCHelperTestClass3.createCustomSettings('de', true); } catch (Exception e) {}
        
        SCHelperTestClass.createOrderTestSet3(true);
        ApexPages.currentPage().getParameters().put('aid', SCHelperTestClass.account.Id);
        
        Test.startTest();

        SCOrderLineController con = new SCOrderLineController();
        
        
        SCboOrder boOrder = con.boOrder;
        SCTool__c tool = con.priceTool;

        String ordLineType = con.curOrdLineType;
        con.curOrdLineType = ordLineType;
        con.onCalculateSums();
        Decimal sumTax = con.sumLineTax;
        Decimal sumPriceNet = con.sumLinePriceNet;
        Decimal sumPriceGross = con.sumLinePriceGross;
        con.calculatePricing();
        con.onReCalculatePricing();

        System.assertNotEquals(null, con.getPriceLists());
        System.assertNotEquals(null, con.getBoOrderLinesEx());
        System.assertNotEquals(null, con.getOrderLinePrefix());
        //System.assertNotEquals(null, con.getOrderFieldName());

        con.onAddPayment();
        ApexPages.currentPage().getParameters().put('currentLineRow', '0.0');
        con.curLineRow = 0.0;
        con.removeOrderLine();
        Double lineRow = con.curLineRow;

        //con.getPaymentFee();
        con.doPaymentService();

        con.getShowValuationTypeColumn();
        con.getResourceId();
        con.getSelectResourceIds();
        
        Test.stopTest();
    }

    static testMethod void testOrderLineCodeCoverage()
    {
        try { SCHelperTestClass.createDomsForOrderCreation(); } catch (Exception e) {}
        try { SCHelperTestClass3.createCustomSettings('de', true); } catch (Exception e) {}
        
        SCApplicationSettings__c appSettings = [SELECT Id, SHOW_PRICING__c FROM SCApplicationSettings__c LIMIT 1];
        try { appSettings = [SELECT Id, SHOW_PRICING__c FROM SCApplicationSettings__c WHERE SetupOwnerId = :UserInfo.getOrganizationId()]; } catch (Exception e) {}
        appSettings.SHOW_PRICING__c = 1;
        
        Database.update(appSettings);
        
        SCHelperTestClass.createOrderTestSet3(true); 

        Test.startTest();

        {//Part1
            Database.delete([SELECT Id FROM SCResource__c WHERE Employee__c = :UserInfo.getUserId()]);
            
            SCOrderLineController con = new SCOrderLineController();
            System.assertEquals(true, con.getShowPricing());
            
            con.getValuationTypeOptions();
            
            System.assertEquals(null, con.userResourceId);
            System.assertEquals(null, con.userResourceId);
        }//Part1
        
        SCHelperTestClass3.appSettings.SHOW_PRICING__c = null;
        Database.update(appSettings);
            
        {//Part2
            SCOrderLineController con = new SCOrderLineController();
            con.getShowPricing();
            
            
            SCResource__c res = new SCResource__c(Alias_txt__c = 'test',
                                                  Employee__c = UserInfo.getUserId());
            
            Database.insert(res);
            
            System.assertNotEquals(null, con.userResourceId);
        }//Part2
        
    }
} // SCOrderLineControllerTest
