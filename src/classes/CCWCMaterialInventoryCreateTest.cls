/*
 * @(#)CCWCMaterialInventoryCreateTest.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 /**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Note !!!
// ========
//@isTest(SeeAllData=true) not allowed by tests creating an order. Because a trigger SCOrder_AI_CallSapWebService calls the Web Service
//====================================
// CCWCOrderCreate if IFEnableTriggerOrderCreate__c of CCSetting is greater than 0. By production and qa data it is the case.
// Calling of Web services while testing causes an exception that aborts the test. So we will not get the test running over the 75 procent of 
// the code
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@isTest
private class CCWCMaterialInventoryCreateTest
{
	private static Boolean seeAllData = CCWCTestBase.isSeeingAllData();
	static testMethod void positiveTestCase1()
	{
		Test.StartTest();
//		Test.setMock(WebServiceMock.class, new CCWCMaterialInventoryCreateTestMock());
		CCWSUtil u = new CCWSUtil();
		SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
        	appSettings = SCApplicationSettings__c.getInstance();
        }

		CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
        	ccSettings = CCSettings__c.getInstance();
        }
		CCWCTestBase.setCCSettings(ccSettings, seeAllData);
		
		// create stock
		Boolean doUpsert = true;
		SCPlant__c plant = CCWCTestBase.createTestPlant(doUpsert);
		List<SCStock__c> stockList = CCWCTestBase.createTestStocks(plant.Id, doUpsert);
		System.assertEquals(true, stockList.size() > 0);

		ID stockId = stockList[0].id;
		System.assertNotEquals(null, stockID);

   		List<SCArticle__c> articleList = CCWCTestBase.createTestArticles(appSettings, doUpsert);

		// create stock items
		List<SCStockItem__c> stockItemList = CCWCTestBase.createTestStockItems(articleList, stockList[0].id, stockList[1].id, doUpsert);
		System.assertEquals(true, stockItemList.size() > 0);

		// create inventory
		
		String description = 'Description';
		String fiscalYear = '2012';
		Boolean full = true;
		Date plannedCountDate = Date.today();
		ID inventoryID = SCboInventory.createInventory(stockId, description, fiscalYear, full, plannedCountDate);
		System.assertNotEquals(null, inventoryId);

		List<SCInventory__c> invList = [select id, name, ERPStatus__c from SCInventory__c where id = : inventoryId];
		System.assertEquals(true, invList.size() > 0);
		invList[0].Status__c = 'created';
		update invList;
		
		String inventoryName = invList[0].name;
		String InventoryCountNumber = '123';
		System.assertNotEquals(null, inventoryName);

		// call service
		Boolean async = false;
		Boolean testMode = true;

		String requestMessageID = CCWCMaterialInventoryCreate.callout(inventoryId, async, testMode);		
		
		// make assertion after web call
		List<SCInventory__c> invList1 = [select id, name, ERPStatus__c from SCInventory__c where id = : inventoryId];
		System.assertEquals(invList1.size(), 1);
		System.assertEquals('pending', invList1[0].ERPStatus__c); 
		
		
		// fill response structure
		CCWSGenericResponse.GenericServiceResponseMessageClass gr = new CCWSGenericResponse.GenericServiceResponseMessageClass();
		String messageId = u.getMessageID();
		String messageUUID = u.getMessageID();
		String operation = 'PhysicalInventory';
		String externalID = inventoryName;

		CCWCTestBase.prepareWCGenericResponse(gr, messageId, messageUUID, requestMessageID, operation, externalID);
		// add reference		
		String idType = 'Physical Inventory Document';
		String referenceID = '123';
		CCWCTestBase.addReferenceItem (gr, idType, externalID, referenceId);

		// add log
		String typeId = 'TypeID';
    	Integer severityCode = 1; 
    	String msg = 'Inventory created';
		
		CCWCTestBase.addLogEntry (gr, typeId, externalID, severityCode, msg);		
		// call the response service
		CCWSGenericResponse.transmit(gr);
		// make assertions 
		// read inventory
		List<SCInventory__c> invList2 = [select id, name, ERPStatus__c, ERPInventoryCountNumber__c from SCInventory__c where id = : inventoryId];
		System.assertEquals(invList2.size(), 1);
		System.assertEquals('ok', invList2[0].ERPStatus__c); 
		System.assertEquals(InventoryCountNumber, invList2[0].ERPInventoryCountNumber__c); 
		Test.StopTest();
	}

	static testMethod void negativeTestCase1()
	{
		Test.StartTest();
		CCWSUtil u = new CCWSUtil();

		SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
        	appSettings = SCApplicationSettings__c.getInstance();
        }

		CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
        	ccSettings = CCSettings__c.getInstance();
        }
		CCWCTestBase.setCCSettings(ccSettings, seeAllData);
		
		// create stock
		Boolean doUpsert = true;
		SCPlant__c plant = CCWCTestBase.createTestPlant(doUpsert);
		List<SCStock__c> stockList = CCWCTestBase.createTestStocks(plant.Id, doUpsert);
		System.assertEquals(true, stockList.size() > 0);

		ID stockId = stockList[0].id;
		System.assertNotEquals(null, stockID);

   		List<SCArticle__c> articleList = CCWCTestBase.createTestArticles(appSettings, doUpsert);

		// create stock items
		List<SCStockItem__c> stockItemList = CCWCTestBase.createTestStockItems(articleList, stockList[0].id, stockList[1].id, doUpsert);
		System.assertEquals(true, stockItemList.size() > 0);

		// create inventory
		
		String description = 'Description';
		String fiscalYear = '2012';
		Boolean full = true;
		Date plannedCountDate = Date.today();
		ID inventoryID = SCboInventory.createInventory(stockId, description, fiscalYear, full, plannedCountDate);
		System.assertNotEquals(null, inventoryId);

		List<SCInventory__c> invList = [select id, name, ERPStatus__c from SCInventory__c where id = : inventoryId];
		System.assertEquals(true, invList.size() > 0);
		invList[0].Status__c = 'created';
		update invList;
		
		String inventoryName = invList[0].name;
		String InventoryCountNumber = '123';
		System.assertNotEquals(null, inventoryName);

		// call service
		Boolean async = false;
		Boolean testMode = true;

		String requestMessageID = CCWCMaterialInventoryCreate.callout(inventoryId, async, testMode);		
		
		// make assertion after web call
		List<SCInventory__c> invList1 = [select id, name, ERPStatus__c from SCInventory__c where id = : inventoryId];
		System.assertEquals(invList1.size(), 1);
		System.assertEquals('pending', invList1[0].ERPStatus__c); 

		// fill response structure
		CCWSGenericResponse.GenericServiceResponseMessageClass gr = new CCWSGenericResponse.GenericServiceResponseMessageClass();
		String messageId = u.getMessageID();
		String messageUUID = u.getMessageID();
		String operation = 'PhysicalInventory';
		String externalID = inventoryName;

		CCWCTestBase.prepareWCGenericResponse(gr, messageId, messageUUID, requestMessageID, operation, externalID);
		// add reference		
		String idType = 'Physical Inventory Document';
		String referenceID = '123';
		CCWCTestBase.addReferenceItem (gr, idType, externalID, referenceId);

		// add log with an error
		String typeId = 'TypeID';
    	Integer severityCode = 3; 
    	String msg = 'Inventory not created';
		
		CCWCTestBase.addLogEntry (gr, typeId, externalID, severityCode, msg);		
		// call the response service
		CCWSGenericResponse.transmit(gr);
		// make assertions 
		// read inventory
		List<SCInventory__c> invList2 = [select id, name, ERPStatus__c, ERPInventoryCountNumber__c from SCInventory__c where id = : inventoryId];
		System.assertEquals(invList2.size(), 1);
		System.assertEquals('error', invList2[0].ERPStatus__c); 
		Test.StopTest();
	}


/*
	static testMethod void musterTest()
	{
		Test.StartTest();
		Test.StopTest();
	}	
*/
    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }

}
