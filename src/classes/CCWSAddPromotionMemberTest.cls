/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers. 
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed 
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CCWSAddPromotionMemberTest {

    static testMethod void myUnitTest() {
    	List<Account> lAccountList	= new List<Account>();
    	
    	// Create TestAccounts
        Account oTestAccount = new Account();
        oTestAccount.Name = 'Max Mustermann';
	    oTestAccount.BillingCountry__c = 'DE';
	    oTestAccount.ID2__c = '12345678';
	    lAccountList.add(oTestAccount);
	    
	    Account oTestAccount2 = new Account();
        oTestAccount2.Name = 'Max Mustermann';
	    oTestAccount2.BillingCountry__c = 'DE';
	    oTestAccount2.ID2__c = '123456789';
	    lAccountList.add(oTestAccount2);
	    
	    Account oTestAccount3 = new Account();
        oTestAccount3.Name = 'Max Mustermann';
	    oTestAccount3.BillingCountry__c = 'DE';
	    oTestAccount3.ID2__c = '1234567891';
	    lAccountList.add(oTestAccount3);
	    
	    Account oTestAccount4 = new Account();
        oTestAccount4.Name = 'Max Mustermann';
	    oTestAccount4.BillingCountry__c = 'DE';
	    oTestAccount4.ID2__c = '12345678912';
	    lAccountList.add(oTestAccount4);
	    
	    Account oTestAccount5 = new Account();
        oTestAccount5.Name = 'Max Mustermann';
	    oTestAccount5.BillingCountry__c = 'DE';
	    oTestAccount5.ID2__c = '123456789123';
	    lAccountList.add(oTestAccount5);
	    
	    insert lAccountList;
	    
	    // Create a Testpromotion
	    Promotion__c oTestPromotion		= new Promotion__c();
	    oTestPromotion.Name				= 'Testpromotion';
	    oTestPromotion.PromotionID__c	= '54321';
	    insert oTestPromotion;
	    
	    CCWSAddPromotionMember.addPromotionMember('12345678;123456789;1234567891;12345678912;123456789123', '54321', 'test321');
	    CCWSAddPromotionMember.addPromotionMember('12345678;541245', '54321', 'test321');
	    CCWSAddPromotionMember.addPromotionMember('12345678;541245', '54321der', 'test321');
	    // Test no members
	    CCWSAddPromotionMember.addPromotionMember('', '54321', 'test321');
	    
	    String members = '1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;';
	    members += '1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;';
	    members += '1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;';
	    members += '1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;';
	    members += '1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;';
	    //Test List Split
	    CCWSAddPromotionMember.addPromotionMember(members, '54321', 'test321');
	   
	    members += '1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;';
	    members += '1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;';
	    members += '1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;';
	    members += '1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;';
	    members += '1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;';
	    members += '1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;';
	    members += '1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;';
	    members += '1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;1;2;3;4;5;6;7;8;9;10;';
	    // Test To many members
	    CCWSAddPromotionMember.addPromotionMember(members, '54321', 'test321');
	   
	    
	    
	    // Create a Testpromotion
	    Promotion__c oTestPromotion2		= new Promotion__c();
	    oTestPromotion2.Name				= 'Testpromotion';
	    oTestPromotion2.PromotionID__c		= '54321';
	    insert oTestPromotion2;
	    // Test two promotions found
	    CCWSAddPromotionMember.addPromotionMember('12345678,541245', '54321', 'test321');
	    
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
