/* 
 * @(#)SCPrintOrderController.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29, 33100 Paderborn
 * www.gms-online.de 
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of
 * GMS Development GmbH. ("Confidential Information").  You shall not disclose 
 * any confidential information and shall use it only in accordance with the 
 * terms of the license agreement you aggreed with GMS.
 */

/*
 * Implements service report (assigment or order related)
 */ 
global with sharing class SCPrintOrderController 
{
    // the order object with all sub items
    public SCOrder__c order {get; set;}

    // the customer signature attchment 
    public Attachment signature {get; set;}

    public String callmode {get; set;}

    // if multiple assignments are to be exported - we group by assignment
    public List<AssignmentInfo> items {get; set;}   
    
    private static CCSettings__c ccSettings = CCSettings__c.getInstance();
    private static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
    
    
    public SCPrintOrderController() 
    {
        String oid = ApexPages.currentPage().getParameters().get('id');    // order id
        String oaid = ApexPages.currentPage().getParameters().get('oaid'); // order assignment id   
        String mode = ApexPages.currentPage().getParameters().get('mode'); // EMAIL or null
        
        init(oid, oaid, mode);
    }

   /*
    * Standard constructor  - reads order data
    * @param con the standard controller
    */
    public SCPrintOrderController(ApexPages.StandardController con) 
    {
        // read order details - the controller does not contain all data we need for creating the credit note 
        String oid = con.getRecord().id;
        String oaid = ApexPages.currentPage().getParameters().get('oaid'); // order assignment id   
        String mode = ApexPages.currentPage().getParameters().get('mode'); // EMAIL or null
        init(oid, oaid, mode);
    }
 
   /*
    * Reads all required data based on the order or assignment id
    * @param oid   order id or 
    * @param oaid  assignment id 
    */
    public void init(String oid, String oaid, String mode)
    {    
        callmode = mode;
    
        // SCfwConstants
        // 5408  DOMVAL_MATSTAT_BOOKED
        // 5506  Completed
        // 50301 Service Recipient 

        // if the order assignment is defined we try to read the order 
        if(oaid != null)
        {
            // read the order based on the current assignment 
            system.debug('###oaid=' + oaid);
            order = [SELECT id, name, ERPOrderNo__c, type__c, InvoicingText__c, Closed__c, DepartmentCurrent__r.Printout1__c,DepartmentCurrent__r.Printout2__c, 
                      (SELECT id,  Name1__c, Name2__c, PostalCode__c, AccountNumber__c, City__c, Street__c FROM OrderRole__r WHERE OrderRole__c = '50301' LIMIT 1),
                      (SELECT id, name, order__c, ArticleName__c, Article__r.Name, Qty__c, assignment__c  FROM OrderLine__r where assignment__c = :oaid and materialstatus__c in ('5408') order by Article__r.Name),
                      
                      //ET 09.09.2013: PMS 33906/W2: Erweiterungen - ZC16 - Austausch 
                      //(SELECT id, name, IBSerialNo__c, ProductNameCalc__c FROM OrderItem__r where recordtypeid in (SELECT Id FROM RecordType WHERE SObjectType = 'SCOrderItem__c' AND DeveloperName  = 'Equipment') LIMIT 1),
                      (SELECT id, name, IBSerialNo__c, ProductNameCalc__c,RecordType.DeveloperName FROM OrderItem__r where RecordType.DeveloperName = 'Equipment' OR (RecordType.DeveloperName = 'EquipmentNew' AND order__r.type__c = '5711') order by RecordType.DeveloperName) ,
                      
                      (SELECT id, ErrorLocation2__c, assignment__c  FROM OrderRepairCode__r where assignment__c = :oaid order by name), 
                      (SELECT id, Type__c, Duration__c, Distance__c, assignment__c  from TimeReports__r where assignment__c = :oaid order by resource__r.name, type__c ), 
                      (SELECT id, name, Status__c, Resource__r.Alias_txt__c, CustomerSignature__c, timereportEnd__c from OrderAssignment__r WHERE id = :oaid and Status__c = '5506' ORDER BY TimeReportEnd__c DESC) // completed
                      from SCOrder__c where id in (select order__c from SCAssignment__c where id = :oaid)];    

            // Read the attachment that contains the customer's signature as a bitmap
            // The signature is directly attached to the engineer's assignment
            List<Attachment> att = [SELECT id, ContentType FROM Attachment where parentid = :oaid and name like 'sig_%' limit 1];
            if(att != null && att.size() > 0 )
            {
                signature = att[0];    
            }
        }
        else
        {
            system.debug('###order id=' + oid);
            // read by the order id - the last completed assignment is used for further processing
            // order lines, order repair codes and timereports are assignment related
            order = [SELECT id, name, ERPOrderNo__c, type__c, InvoicingText__c, Closed__c, DepartmentCurrent__r.Printout1__c,DepartmentCurrent__r.Printout2__c, 
                      (SELECT id,  Name1__c, Name2__c, PostalCode__c, AccountNumber__c, City__c, Street__c FROM OrderRole__r WHERE OrderRole__c = '50301' LIMIT 1),
                      
                      //ET 09.09.2013: PMS 33906/W2: Erweiterungen - ZC16 - Austausch
                      //(SELECT id, name, IBSerialNo__c, ProductNameCalc__c FROM OrderItem__r where recordtypeid in (SELECT Id FROM RecordType WHERE SObjectType = 'SCOrderItem__c' AND DeveloperName = 'Equipment') LIMIT 1),
                      (SELECT id, name, IBSerialNo__c, ProductNameCalc__c, RecordType.DeveloperName FROM OrderItem__r where RecordType.DeveloperName = 'Equipment' OR (RecordType.DeveloperName = 'EquipmentNew' AND order__r.type__c = '5711') order by RecordType.DeveloperName) ,
                      
                      (SELECT id, name, order__c, ArticleName__c, Article__r.Name, Qty__c, assignment__c FROM OrderLine__r order where materialstatus__c in ('5408') order by assignment__c, Article__r.Name),
                      (SELECT id, ErrorLocation2__c, assignment__c  FROM OrderRepairCode__r order by assignment__c, name), 
                      (SELECT id, Type__c, Duration__c, Distance__c, assignment__c from TimeReports__r order by assignment__c, resource__r.name, type__c), 
                      (SELECT id, name, Status__c, Resource__r.Alias_txt__c, CustomerSignature__c, timereportEnd__c from OrderAssignment__r WHERE Status__c = '5506' ORDER BY id, TimeReportEnd__c DESC)
                      from SCOrder__c where id = :oid];    
        }

        // This implementation supports single and multi assignment printouts
        if(order != null && order.OrderAssignment__r != null && order.OrderAssignment__r .size() > 0)
        {   
            // Now fill the order assignment info object (for grouping the output by the assignment)            
            items = new List<AssignmentInfo>();
            for(SCAssignment__c a : order.OrderAssignment__r)
            {
                AssignmentInfo info = new AssignmentInfo();
                items.add(info);
                // the user visible assignment number (starting with 1)
                info.idx = items.size(); 
                info.assignment = a;    
                // now search all order lines for this assignment
                if(order.OrderLine__r != null)
                {
                    for(SCOrderLine__c ol : order.OrderLine__r)
                    {
                        if(a.id == ol.assignment__c)
                        {
                            info.orderline.add(ol);
                        }
                    }
                }
                // and search all repair codes for this assignment
                if(order.OrderRepairCode__r != null)
                {
                    for(SCOrderRepairCode__c r : order.OrderRepairCode__r)
                    {
                        if(a.id == r.assignment__c)
                        {
                            info.orderrepaircode.add(r);
                        }
                    }
                }
                // and all time reports for this assignment
                if(order.TimeReports__r != null)
                {
                    for(SCTimereport__c ot : order.TimeReports__r)
                    {
                        if(a.id == ot.assignment__c)
                        {
                            info.timereport.add(ot);
                        }
                    }
                }
                
                // now prepare the detailled info
                info.orderlineinfos = prepareOrderLines(info.timereport, info.orderline);

            } // for(SCAssignment..
        } // if(order..
    }

   /*
    * Return the assignment (index 0)
    */
    public SCAssignment__c getLastAssignment()
    {
        if(order != null && order.OrderAssignment__r != null && order.OrderAssignment__r .size() > 0)
        {
            return order.OrderAssignment__r[0];
        }
        return null;
    }

    /*
    * Returns true if there are multiple assignments - used 
    */
    public Boolean getMultiAssignments()
    {
        if(order != null && order.OrderAssignment__r != null && order.OrderAssignment__r .size() > 1)
        {
            return true;
        }
        return false;
    }
    
   /*
    * Add artificially the order lines required for the time report items
    */
    public List<OrderLineInfo > getOrderLines()
    {
        return prepareOrderLines(order.TimeReports__r, order.OrderLine__r);
    }   
    
    public List<OrderLineInfo> prepareOrderLines(List<SCTimeReport__c> timereports, List<SCOrderLine__c> orderlines)
    {
        List<OrderLineInfo> result = new List<OrderLineInfo >();

    
        //### GMSNA 05.04.2013 Requirement that has to be hard coded as confirmed by CCEAG + no translation!
        //### as the CP standard features concerning the automatic orderline creation on the mobile
        //### system based on the time reports had to be disabled - we now have this unlucky solution !
    
        // 1. first add the time report details (article numbers are to be hard coded here (requirement from CCEAG !!!)
        // 300000000 Anfahrtskilometer       50
        // 300000003 Anfahrtszeit (min)      30  
        // 300000001 Arbeitszeit (min)       120
        
        //SELECT id, Type__c, Duration__c, Distance__c from TimeReports__r), 
        for(SCTimeReport__c tr : timereports)
        {
            if(tr.type__c == SCfwConstants.DOMVAL_TIMEREPORTTYPE_WORKTIME)
            {
                OrderLineInfo info = new OrderLineInfo();
                info.articleno       = '300000001'; 
                info.articledescr    = 'Arbeitszeit (min)';
                info.qty             = tr.Duration__c;
                result.add(info);
            }
            else if(tr.type__c == SCfwConstants.DOMVAL_TIMEREPORTTYPE_DRIVETIME)
            {
                OrderLineInfo info = new OrderLineInfo();
                info.articleno       = '300000003'; 
                info.articledescr    = 'Anfahrtszeit (min)';
                info.qty             = tr.Duration__c;
                result.add(info);

                info = new OrderLineInfo();
                info.articleno       = '300000000'; 
                info.articledescr    = 'Anfahrtskilometer';
                info.qty             = tr.Distance__c;
                result.add(info);
            }
        }
        
        // 2. add the article order lines
        for(SCOrderLine__c ol : orderlines)
        {
            OrderLineInfo info = new OrderLineInfo();
            info.articleno       = ol.article__r.Name; 
            info.articledescr    = ol.ArticleName__c;
            info.qty             = ol.Qty__c;
            result.add(info);
        }
        return result;         
    }
    
   /*
    * Return the first order item (equipment) - sub equipments are ignored in the soql query already
    */
    public SCOrderItem__c getOrderItem()
    {
        if(order != null && order.OrderItem__r != null && order.OrderItem__r.size() > 0)
        {
            return order.OrderItem__r[0];
        }
        return null;
    }
    
    /*
    * Return all order items (equipment / equipmentNew) - sub equipments are ignored in the soql query already
    */
    public List<SCOrderItem__c> getOrderItems()
    {
        if(order != null && order.OrderItem__r != null && order.OrderItem__r.size() > 0)
        {
            return order.OrderItem__r;
        }
        return null;
    }
    
   /*
    * Return the service recipient of the order
    */
    public SCOrderRole__c getServiceRecepient()
    {
        if(order != null && order.OrderRole__r != null && order.OrderRole__r.size() > 0)
        {
            return order.OrderRole__r[0];
        }
        return null;
    }

    public static void sendDoc(String attachmentid, String email, String subject, String bodytext) 
    {
        Attachment doc = [select id, name, body, contenttype from Attachment where id = :attachmentid];
        sendDoc(doc, email, subject, bodytext); 
    }
    
    public static void sendDoc(Attachment doc, String email, String subject, String bodytext) 
    {
        Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
        attach.setContentType(doc.contentType);
        attach.setFileName(doc.name + '.pdf');
        attach.setInline(false);
        attach.Body = doc.Body;
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setUseSignature(false);
        mail.setToAddresses(new String[] { email });
        
        // Specify the address used when the recipients reply to the email. 
        mail.setReplyTo('noreply@cceag.de');
        // Specify the name used as the display name.
        mail.setSenderDisplayName('Coca-Cola Cold Drink Service (no reply)');
        
        if(subject != null)
        {
            mail.setSubject(subject);
        }
        if(bodytext != null)
        {
            mail.setHtmlBody(bodytext);
        }
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach }); 
        
        // Send the email
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        
    }
    
    
    /**
     * Send an Email for a closed Service Report by using a Template.
     *      ERROR: If this method throws an error, use the sendDoc-method
     * @author: Sebastian Schrage
     * @param recipientId    -> Id of a contact, lead, or user
     * @param attachmentId   -> Id of the attachment
     * @param assignmentId   -> Id of the assignment
     */
    public static void sendEmailServiceReport(Id recipientId, Id attachmentId, Id assignmentId)
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setUseSignature(false);
        mail.setTargetObjectId(recipientId);
        mail.setWhatId(assignmentId);
        mail.setBccSender(false);
        
        //Get Template
        Id templateId = [SELECT Id
                           FROM EmailTemplate
                          WHERE DeveloperName = :appSettings.EMAIL_SERVICE_REPORT_TEMPLATE__c
                            AND IsActive = true].Id;
        mail.setTemplateId(templateId);
        
        //Attach file
        if (attachmentid != null)
        {
            Attachment doc = [SELECT id, name, body, contenttype FROM Attachment WHERE id = :attachmentid];
            
            Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
            attach.setContentType(doc.contentType);
            attach.setFileName(doc.name + '.pdf');
            attach.setInline(false);
            attach.Body = doc.Body;
            
            mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach }); 
        }
        
        // Specify the address used when the recipients reply to the email. 
        if (appSettings.EMAIL_SERVICE_REPORT_REPLY_TO__c != null)
        {
            mail.setReplyTo(appSettings.EMAIL_SERVICE_REPORT_REPLY_TO__c);
        }
        
        // Specify the name used as the display name.
        if (appSettings.EMAIL_SERVICE_REPORT_SENDERDISPLAYNAME__c != null)
        {
            mail.setSenderDisplayName(appSettings.EMAIL_SERVICE_REPORT_SENDERDISPLAYNAME__c);
        }
        
        // Send the email
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    
    
    /**
     * Creates a timestamp of the current date and time to be shown at PDF / Excel pages
     * @return string timestamp 
     */  
    public String getReadableFileName()
    {
        return '' + order.name + '-' + Datetime.NOW().format('yyyMMddHHmmss');
    }    
    
    
    


    //--<pdf creation by means of a web service helper>---------------------------------------------------------------

   /*
    * This WebService trick is required as we can't create the PDF document (render as PDF) in a future call properly (user ctx is missing)!
    * Asynchronous creation (pass the current (api)user's session id as context information)
    */
    @future (callout=true)
    public static void createPDFAttachmentAsyncWS(String sessionid, String oid, String name) 
    {
        createPDFAttachmentByWebService(sessionid, oid, name); 
    }
 
    /**
     * Synchronous Helper function - used to initiate the printout
     * @param sessionid the user's session id (required for the callout)
     * @param val   order id
     * @param name  used as a filename
     */  
    public static String createPDFAttachmentByWebService(String sessionid, String oid, String name) 
    {
        return createPDFHelper(sessionid, null, oid, null, name);
    }

    public static String createPDFAttachmentByWebServiceByAssignment(String sessionid, String oid, String oaid, String name) 
    {
        return createPDFHelper(sessionid, null, oid, oaid, name);
    }
    

    private static String createPDFHelper(String sessionid, String mode, String oid, String oaid, String name) 
    {
        CCWCPrintOrder.proxy ws = new CCWCPrintOrder.proxy();
        
        String wsUrl = ccSettings.SalesforceWebserviceUrl__c;
        wsUrl = (wsUrl == null || wsUrl == '') ? 'https://cs7-api.salesforce.com' : wsUrl;
        
        ws.endpoint_x = wsUrl + '/services/Soap/class/SCPrintOrderController';
        ws.timeout_x = 90000; // timeout in milliseconds
        ws.SessionHeader = new CCWCPrintOrder.SessionHeader_element();
        ws.SessionHeader.sessionId = sessionid;
        String attachmentid = ws.print(mode, oid, oaid, name);
        return attachmentid;
    }

        
    /**
     * Helper function used for printing the document / attachment from within future / triggers
     * This is required as in an apex job saleforce can't render pdfs poperly
     * @param mode  reserved 
     * @param oid   SCOrder__c.ID (required for attaching the document to the order)
     * @param oaid  SCAssignment__c.ID (optional, pass null to print all assignments)
     * @param name  used as a filename
     * @return the id of the attachment created and attached to the order
     */  
    WebService static String print(String mode, String oid, String oaid, String name)
    {
        Attachment att = createPDFAttachment(mode, oid, oaid, name); 
        return att.id;
    }


   /*
    * Creates the PDF document and attaches it to the order.
    * @param mode   (reserved)
    * @param oid    order id - required for attaching the document to the order
    * @param oaid   order assignment id (optional - if null: all assignments will be printed) 
    */
    public static Attachment createPDFAttachment(String mode, String oid, String oaid, String name) 
    {
        pageReference p = Page.SCPrintOrder;
        if(oaid != null)
        {
            p.getParameters().put('oaid', oaid);
        }
        if(oid != null)
        {
            p.getParameters().put('id', oid);
        }

        // now render as pdf  getContentAsPDF
        Blob body = p.getContent();
    
        Attachment pdf = new Attachment();
        pdf.name = 'servicereport-' + name;
        pdf.ContentType = 'application/pdf';
        pdf.body = body;
        pdf.ParentId = oid;
        pdf.isPrivate = false;
        
        insert pdf;
        
        return pdf;
    }
    


    
    
    // Helper classes
    public class OrderLineInfo
    {
        public String articleno {get; set;}
        public String articledescr {get; set;}
        public Decimal qty {get; set;}
    }
    
    public class AssignmentInfo
    {
        public Integer idx  {get; set;}
        public SCAssignment__c assignment  {get; set;}
        public List<SCOrderLine__c> orderline  {get; set;}
        public List<SCOrderRepairCode__c> orderrepaircode  {get; set;}
        public List<SCTimereport__c> timereport  {get; set;}
        
        public List<OrderLineInfo> orderlineinfos {get; set;}
    
        public AssignmentInfo()
        {
            idx = 0;
            assignment = null;
            orderline = new List<SCOrderLine__c>();
            orderrepaircode = new List<SCOrderRepairCode__c>();
            timereport = new List<SCTimereport__c>();
            orderlineinfos = new List<OrderLineInfo>();
        }
    }
    
    
}
