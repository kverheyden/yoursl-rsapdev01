/*
 * @(#)CCWSGenericResponseInterface.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/*
 * The interface is used by the generic class CCWSInterfaceLog for 
 * logging of the incomming web service and preparing of the return value
 * of the transmit function that is the main function of the web service
 * 
 * Every incomming web service class must define the inner class ResponseClass
 * that implements this interface. The reason for that is that every trasmit function
 * returns the response structure that must be defined anew in every specific class as an inner class
 * to prevent the qualifying field names of the structure with the name of the class 
 * for the return structure if it would be the independent class.
 * 
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */

public interface CCWSGenericResponseInterface 
{
    String getResultCode(Object response);
    String getResultInfoForAll(Object response);
    Integer getLocalMaxSeverityCode(Object response);
}
