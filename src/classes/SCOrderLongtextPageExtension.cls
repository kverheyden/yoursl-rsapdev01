public with sharing class SCOrderLongtextPageExtension {
	
	public SCOrder__c order {get; private set;}
  	//private List<SCOrderItem__c> orderItems;
  	public String text {get; set;}
  	
  	public SCOrderLongtextPageExtension(ApexPages.StandardController controller)
  	{
  		SCOrder__c orderID = (SCOrder__c)controller.getRecord();
    	this.order = [ Select id, name, ERPLongtext__c 
      		from SCOrder__c where id = :orderID.id ]; 
    	system.debug('#### SCOrder__c: ' + order);
    	text = order.ERPLongtext__c;
  	}
  	
  	private List<SCOrderItem__c> selectOrderItems()
  	{
  		List<SCOrderItem__c> items = [select Id, Name, Description__c, 	ErrorText__c from SCOrderItem__c where Order__c = : this.order.id];
  		return items;
  	}
  	
  	public void save()
  	{
    	if(text != order.ERPLongtext__c)
    	{
    		order.ERPLongtext__c = text;
    		
    		List<SCOrderItem__c> items = selectOrderItems();
    		
    		if(items.size() > 0)
    		{
    			for(SCOrderItem__c orderitem : items)
    			{
    				//orderitem.Description__c = text;
    				orderitem.ErrorText__c = text;
    			}
    			
    			update items;
    		}
    		
    		update order;
    		
    		
    	}
  		
  	}
  	

}
