/*
 * @(#)SCConfOrderPrioExtensionTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Implements a simple configuration function for creating
 * SCConfOrderPrio items (used for determining the earliest date). 
 * @author Norbert Armbruster <narmbruster@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest 
private class SCConfOrderPrioExtensionTest
{
   /*
    * A simple test method to get the test coverage.
    */
    public static testMethod void testConfig() 
    {
        String country = 'testonly2';
    
        Test.StartTest();

        // create an empty list (nothing selected)
        List<SCConfOrderPrio__c> items = new List<SCConfOrderPrio__c>();
        ApexPages.StandardSetController setCon = new ApexPages.StandardSetController(items);
        SCConfOrderPrioExtension ext = new SCConfOrderPrioExtension(setCon);

        System.AssertEquals(0, ext.getSelectedCount());

        // set the current country
        ext.CountrySelected = country;
        System.AssertEquals(country, ext.CountrySelected);

        ext.onCreate();
        Integer count = [select count() from SCConfOrderPrio__c where country__c = :country];
        System.AssertEquals(true, count > 0);
        
        // read the configuration for our test country
        List<SCConfOrderPrioExtension.ResultCount> result = ext.getCountryConfig();
        System.AssertEquals(true, ext.countryConfigItemsCount() > 0);
        
        // delete the current items of the country
        ext.onDelete();
        count = [select count() from SCConfOrderPrio__c where country__c = :country];
        System.AssertEquals(0, count);
 
        // Dummy test       
        ext.returl = '/dummy';
        ext.onCancel();
        ext.onDeleteSelected();
        
        Test.StopTest();
   } 
}
