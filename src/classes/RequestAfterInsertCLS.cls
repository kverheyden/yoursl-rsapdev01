/***********************************************************************
Date 			AUTHOR 				DETAIL

2014-07-31		Andre Mergen 		INITIAL DEVELOPMENT
2014-08-31		Bernd Werner		Added Check logic to direct confirm Requests without SAP relevant field changes.

***********************************************************************/
public with sharing class RequestAfterInsertCLS {
	public List<Account> UpdateAccounts 		= new List<Account>();
	public List<Request__c> UpdateRequest		= new List<Request__c>();
	public MAP<string, string> RequestToAccount = new MAP<string, string>();
	
	public Set<String>relevantFieldsForSAP { 
		get{
			Set<String> tmpFieldList = new Set<String>();
			if(relevantFieldsForSAP == null){				
				for (Schema.FieldSetMember f : Schema.SObjectType.Request__c.fieldSets.FieldsToSendToSAPforChange.getFields()){
					system.debug('###FieldName');
					tmpFieldList.add(f.getFieldPath());
				}
			}
			return tmpFieldList;		
		} 
		set; 
	} 
	
	/**
	* Check if the changed or deleted fields of an change request form
	* are relevant for transfering the record to SAP
	*
	* @param fieldString 	String with field names comma separated
	* 
	* 
	*/
	private Boolean checkSAPRelevance (String fieldString){
		if(fieldString==null || fieldString==''){
			return false;
		}
		for(String tmpField : fieldString.split(',')){
			if(relevantFieldsForSAP.contains(tmpField)){
				return true;
			}
		}
		
		return  false;
	} 
	
	/**
	* Queries all pending requests from relevant Accounts and checks
	* if it contains an Account Id from the given List
	*
	* @param checkAccs 	List with Accounts
	* 
	* 
	*/
	private List<Account> markPendingRequests(List<Account> checkAccs){
		Set<String> AccountsWithPending = new Set<String>();
		List<Account> returnAccounts	= new List<Account>();
		for (Request__c tmpCheckReq : [Select Id, Account__c,RelatedAccount__c FROM Request__c where Account__c=null AND RelatedAccount__c in : checkAccs ]){
			if(!AccountsWithPending.contains(tmpCheckReq.RelatedAccount__c)){
				AccountsWithPending.add(tmpCheckReq.RelatedAccount__c);
			}
		}
		for(Account tmpCheckAcc : checkAccs){
			if(AccountsWithPending.contains(tmpCheckAcc.Id)){
				tmpCheckAcc.HasPendingChangeRequest__c = true;
			}
			else{
				tmpCheckAcc.HasPendingChangeRequest__c = false;
			}
			returnAccounts.add(tmpCheckAcc);
		}
		return returnAccounts;
	}
	
	/**
	* Filles a map of Strings with the field mapping between Request__c and Account
	*
	* @param 
	* 
	* 
	*/
	public void fillRequestToAccount()
	{	
		// Request Field, Account Field
		RequestToAccount.put('MainBrandBeer__c', 'MainBrandBeer__c');
		RequestToAccount.put('ContractObligationBeer__c', 'ContractObligationBeer__c');
		RequestToAccount.put('EndDateBeerContractBinding__c', 'EndDateBeerContractBinding__c');
		RequestToAccount.put('MainBrandWheatBeer__c', 'MainBrandWheatBeer__c');
		RequestToAccount.put('MainBrandCola__c', 'MainBrandCola__c');
		RequestToAccount.put('ContractObligationCola__c', 'ContractObligationCola__c');
		RequestToAccount.put('EndDateColaContractBinding__c', 'EndDateColaContractBinding__c');
		RequestToAccount.put('MainBrandWater__c', 'MainBrandWater__c');
		RequestToAccount.put('SecondMainBrandWater__c', 'SecondMainBrandWater__c');
		RequestToAccount.put('ContractObligationWater__c', 'ContractObligationWater__c');
		RequestToAccount.put('EndDateWaterContractBinding__c', 'EndDateWaterContractBinding__c');
		RequestToAccount.put('UnrestrictedDelivery__c', 'UnrestrictedDelivery__c');
		RequestToAccount.put('PricingAnnualOrderDiscount__c', 'PricingAnnualOrderDiscount__c');
		RequestToAccount.put('PricingShipmentQuantityDiscount__c', 'PricingShipmentQuantityDiscount__c');
		RequestToAccount.put('PaymentDirectDebit__c', 'PaymentDirectDebit__c');
		RequestToAccount.put('CentralEffort__c', 'CentralEffort__c');
		RequestToAccount.put('IntroductionCateringBottle__c', 'IntroductionCateringBottle__c');
		RequestToAccount.put('PricingSponsoringDiscount__c', 'PricingSponsoringDiscount__c');
		RequestToAccount.put('PricingCaseCompensation__c', 'PricingCaseCompensation__c');
		RequestToAccount.put('PricingUnitThroughputDiscount__c', 'PricingUnitThroughputDiscount__c');
		RequestToAccount.put('PricingCustomerOwnedUnit__c', 'PricingCustomerOwnedUnit__c');
		RequestToAccount.put('PricingWaterConceptDiscount__c', 'PricingWaterConceptDiscount__c');
		RequestToAccount.put('PricingStartDate__c', 'PricingStartDate__c');
		RequestToAccount.put('IsInLowEmissionZone__c', 'IsInLowEmissionZone__c');
		RequestToAccount.put('TypeOfLowEmissionZone__c', 'LowEmissionZoneType__c');
	}

	/**
	* Creates a List of Strings from a given CSV String
	*
	* @param String CSVField 
	* 
	* 
	*/
	// read the CSV Fields to List<String>
	public List<string> readCSV(string CSVField)
	{
		// List<String> fields = CSVField.split(',');
		List<String> fieldNames = CSVField.split(',');   
		
		List<String> cleanFields = new List<String>();
		//String compositeField;
		Boolean makeCompositeField = false;
		// for(String field : fields) 
		for(String field : fieldNames)
		{
			//if(field.startsWith('"') && field.endsWith('"'))
			if (field!= null && field != '') 
			{
				cleanFields.add(field.replaceAll('DBLQT','"'));
			} 
		}
		return cleanFields;
	}

	
/*
			//mapRequestInformation
				Request__c tmp_req = new Request__c(); 
				tmp_req=mapRequestInformation.get(tmp_Acc.Request__c);
				
				tmp_Acc.MainBrandBeer__c 					= tmp_req.MainBrandBeer__c;
				tmp_Acc.ContractObligationBeer__c 			= tmp_req.ContractObligationBeer__c;
				tmp_Acc.EndDateBeerContractBinding__c 		= tmp_req.EndDateBeerContractBinding__c;
				tmp_Acc.MainBrandWheatBeer__c 				= tmp_req.MainBrandWheatBeer__c;
				tmp_Acc.MainBrandCola__c 					= tmp_req.MainBrandCola__c;
				tmp_Acc.ContractObligationCola__c 			= tmp_req.ContractObligationCola__c;
				tmp_Acc.EndDateColaContractBinding__c 		= tmp_req.EndDateColaContractBinding__c;
				tmp_Acc.MainBrandWater__c 					= tmp_req.MainBrandWater__c;
				tmp_Acc.SecondMainBrandWater__c 			= tmp_req.SecondMainBrandWater__c;
				tmp_Acc.ContractObligationWater__c 			= tmp_req.ContractObligationWater__c;
				tmp_Acc.EndDateWaterContractBinding__c 		= tmp_req.EndDateWaterContractBinding__c;
				tmp_Acc.UnrestrictedDelivery__c 			= tmp_req.UnrestrictedDelivery__c;
				tmp_Acc.PricingAnnualOrderDiscount__c 		= tmp_req.PricingAnnualOrderDiscount__c;
				tmp_Acc.PricingShipmentQuantityDiscount__c 	= tmp_req.PricingShipmentQuantityDiscount__c;
				tmp_Acc.PaymentDirectDebit__c 				= tmp_req.PaymentDirectDebit__c;
				tmp_Acc.CentralEffort__c 					= tmp_req.CentralEffort__c;
				tmp_Acc.IntroductionCateringBottle__c 		= tmp_req.IntroductionCateringBottle__c;
				tmp_Acc.PricingSponsoringDiscount__c 		= tmp_req.PricingSponsoringDiscount__c;
				tmp_Acc.PricingCaseCompensation__c 			= tmp_req.PricingCaseCompensation__c;
				tmp_Acc.PricingUnitThroughputDiscount__c 	= tmp_req.PricingUnitThroughputDiscount__c;
				tmp_Acc.PricingCustomerOwnedUnit__c 		= tmp_req.PricingCustomerOwnedUnit__c;
				tmp_Acc.PricingWaterConceptDiscount__c 		= tmp_req.PricingWaterConceptDiscount__c;
				tmp_Acc.PricingStartDate__c 				= tmp_req.PricingStartDate__c;
*/


	public void prepareRequestAccountUpdate(List<Request__c> tmpListRequests)
	{
		boolean FieldsToUpdate = false;
		boolean isSAPrelevant = true;
		fillRequestToAccount();
		List<string> ListDeletedFields = new List<string>();
		List<string> ListChangedFields = new List<string>();
		Account tmpAcc = new Account();
		sObject tmpsObReq = new Request__c();
		Request__c tmpUpdateRequest	= new Request__c();
		
		//Related Account
		//for(Request__c tmpReq:tmpListRequests)
		for(Request__c tmpReq:tmpListRequests)
		{
			FieldsToUpdate 		= false;
			isSAPrelevant		= false;
			tmpAcc 				= new Account();
			tmpsObReq 			= new Request__c();
			tmpUpdateRequest	= new Request__c();
			tmpUpdateRequest.Id = tmpReq.Id;
			
			ListDeletedFields.clear();
			ListChangedFields.clear();
			// nur Valide Requests bearbeiten
			if(tmpReq.RelatedAccount__c != NULL && tmpReq.RequestProcessed__c==false)
			{
				tmpsObReq = tmpReq;
				// ListDeletedFields setzen
				if(tmpReq.DeletedFieldsCSV__c != NULL && tmpReq.DeletedFieldsCSV__c != '')
				{
					ListDeletedFields 	= readCSV(tmpReq.DeletedFieldsCSV__c);
					isSAPrelevant		= checkSAPRelevance(tmpReq.DeletedFieldsCSV__c);
				}
				
				// ListChangedFields setzen
				if(tmpReq.ChangedFields__c != NULL && tmpReq.ChangedFields__c != '')
				{
					ListChangedFields 	= readCSV(tmpReq.ChangedFields__c);
					isSAPrelevant		= checkSAPRelevance(tmpReq.ChangedFields__c);
				}
				system.debug('### ListChangedFields: ' + ListChangedFields);

				// prepare DeletedFields
				for(string ReqDelField: ListDeletedFields)
				{
					if(RequestToAccount.containsKey(ReqDelField))
					{
						tmpAcc.put(RequestToAccount.get(ReqDelField), null);
						FieldsToUpdate = true;
					}
				}
				
				// prepare ChangedFields        
				for(string ReqField: ListChangedFields)
				{
					if(RequestToAccount.containsKey(ReqField))
					{
						tmpAcc.put(RequestToAccount.get(ReqField), tmpsObReq.get(ReqField));
						FieldsToUpdate = true;
					}
				}
				
				// define Account Id
				tmpAcc.Id = tmpReq.RelatedAccount__c;
				// Add Account to UpdateList
				//if(FieldsToUpdate == true)
				//{
					//tmpAcc.HasPendingChangeRequest__c = true;
				UpdateAccounts.add(tmpAcc);
				//}
				//system.debug('### UpdateAccounts:' + UpdateAccounts);
				if(isSAPrelevant==false){
					tmpUpdateRequest.Account__c			=tmpReq.RelatedAccount__c;
				}
				tmpUpdateRequest.RequestProcessed__c 	= true;
				UpdateRequest.add(tmpUpdateRequest);
							
			} // ENDE tmpReq.RelatedAccount__c != NULL
		}
		// Update Requests
		if (UpdateRequest.size()>0){
			update UpdateRequest;
			// Check if 
		}
		// Update Accounts
		if(UpdateAccounts.size()>0)
		{
			UpdateAccounts=markPendingRequests(UpdateAccounts);
			update UpdateAccounts;
		}
		
	}
	


}
