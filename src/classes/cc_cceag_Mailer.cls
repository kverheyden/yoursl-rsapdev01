public without sharing class cc_cceag_Mailer {

    private final static STRING senderEmailAddress = ecomSettings__c.getInstance('systemEmailAddress').value__c;
    

    public static void sendEmail (String[] recipients, String subject, String plainTextBody) {
    
        Messaging.Singleemailmessage message = new Messaging.Singleemailmessage();
        message.setToAddresses(recipients);
        message.setReplyTo(senderEmailAddress);
        message.setSubject(subject);
        message.setPlainTextBody(plainTextBody);

        Messaging.Sendemailresult[] results = Messaging.sendEmail(new Messaging.Singleemailmessage[] {
            message
        });
        
        if (!results[0].isSuccess()) {
            throw new EmailException('Error sending email "' + subject + '"');
        }
    }
    
    
    class EmailException extends Exception {
    }
}
