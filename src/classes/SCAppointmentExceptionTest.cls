/*
 * @(#)SCAppointmentExceptionTest.cls SCCloud    
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @version $Revision$, $Date$
 */
@isTest
private class SCAppointmentExceptionTest 
{  

    static testMethod void testExceptionTestPositive()
    {
        
        SCAppointmentException e = new SCAppointmentException();
        e.setMessage('errcode(s): 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 22, 223, 224, 225, 226, 227, 228, faultcode');
        SCAppointmentException[]  exceptions = SCAppointmentException.getExceptions(e);
        System.Assert(exceptions.size() > 2 ); 
    }
    static testMethod void testExceptionTestnegativ()
    {
        
        SCAppointmentException e = new SCAppointmentException();
        e.setMessage('123');
        SCAppointmentException[]  exceptions = SCAppointmentException.getExceptions(e);
        System.Assert(exceptions.size() < 2 ); 
    }
}
