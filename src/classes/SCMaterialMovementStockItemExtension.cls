/*
 * @(#)SCMaterialMovementStockItemExtension.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCMaterialMovementStockItemExtension extends SCMatMoveBaseExtension
{
    public SCStockItem__c stockItem;
    public List<SCMaterialMovement__c> matMoves;

    public Id stock 
    { 
        get 
        {
            if (null == this.stock)
            {
                this.stock = stockItem.stock__c;
            } // if (null == this.stock)
            return this.stock;
        }  // get
        set;
    } // stock 

    public Id article 
    { 
        get 
        {
            if (null == this.article)
            {
                this.article = stockItem.article__c;
            } // if (null == this.article)
            return this.article;
        }  // get
        set;
    } // article
    
    /**
     * SCMaterialMovementStockItemExtension
     * ====================================
     *
     * Reads the material movements for one article of a stock.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCMaterialMovementStockItemExtension(ApexPages.StandardController controller)
    {
        this.stockItem = (SCStockItem__c)controller.getRecord();
    } // SCMaterialMovementStockItemExtension

    /**
     * setController
     * =============
     *
     * Initialize setController and return a list of records.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public ApexPages.StandardSetController setController 
    {
        get 
        {
            System.debug('#### setController(): StockItem -> ' + stockItem);
            System.debug('#### setController(): Stock     -> ' + stock);
            System.debug('#### setController(): Article   -> ' + article);
        
            if (null == setController) 
            {
                String query = 'Select Name, Type__c, Status__c, Qty__c, Replenishment__r.Name, ' + 
                                'Replenishment__r.Id, Order__c, OrderLine__c, RequisitionDate__c, ' + 
                                'ProcessDate__c ' + 
                                'from SCMaterialMovement__c where ' + 
                                'Stock__c = :stock and Article__c = :article ' + 
                                'order by Name desc';
        
                setController = new ApexPages.StandardSetController(Database.getQueryLocator(query));

                setController.setPageSize(10);
            } // if (null == setController) 

            return setController;
        } // get 
        set;
    } // setController 

    /**
     * getMatMoves
     * ===========
     *
     * Retrieves the list with the material movements for an article of a stock.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public List<SCMaterialMovement__c> getMatMoves()
    {
        System.debug('#### getMatMoves()');
        
        if (null == setController)
        {
            System.debug('### getMatMoves(): setController is NULL!');
            matMoves = null;
        } // if (null == setController)
        else
        {
            matMoves = (List<SCMaterialMovement__c>)setController.getRecords();
        }

        return matMoves;
    } // getMatMoves
} // SCMaterialMovementStockItemExtension
