/**********************************************************************
Name:  CCWCRestAccountsWithDetailsTest()
======================================================
Purpose:                                                            
Test class for CCWCRestAccountWithDetails.                                              
======================================================
History                                                            
-------                                                              
Date  	AUTHOR	DETAIL 
01/09/2013	Jan Mensfeld
***********************************************************************/
 
@isTest
private class CCWCRestAccountsWithDetailsTest {
	
	static final String BH_FROM_AM = '07:00';
	static final String BH_UNTIL_AM = '11:00';
	
	static final String DH_FROM_AM = '08:00';
	static final String DH_UNTIL_AM = '10:00';
	
	static Account TestAccount { get; set; }
	static AccountFeed TestAccountFeed { get; set; }
	static CCAccountRole__c TestAccountRole { get; set; }
	static Contact TestContact { get; set; }
	static BankAccount__c TestBankAccount { get; set; }
	static Pricebook2 TestPriceBook { get; set; }
	static PricebookEntryDetail__c TestPriceBookEntry { get; set; }
	static PromotionMember__c TestPromotionMember { get; set; }
	static RedSurveyResult__c TestRedSurveyResult { get; set; }
	static SalesDetails__c TestSalesDetails { get; set; }
	static SCAccountInfo__c TestAccountInfo { get; set; }
	static SCInstalledBase__c TestInstalledBase { get; set; }
	static SCInstalledBaseLocation__c TestInstalledBaseLocationInfo { get; set; }
	static SCInstalledBaseRole__c TestInstalledBaseRole { get; set; }
	static Task TestTask { get; set; }
	static User TestUser { get; set; }	
	
    static {
        SalesAppSettings__c setting = new SalesAppSettings__c();
        setting.name = 'AccountsWithDetailsQueryLimit';
        setting.value__c = '200';
        insert setting;
    }
    
	static Account getTestAccount() {
				
		Account testAcc = new Account();
		testAcc.Name = 'Test Account';
		return  testAcc;
	}
	
	static BankAccount__c getTestBankAccount(Account acc) {
		BankAccount__c testBA = new BankAccount__c();
		testBA.Account__c = acc.Id;
		//testBA.CurrencyIsoCode = 'EUR';
		return testBA;
	}

	static SCAccountInfo__c getTestAccountInfo(Account acc, String id2) {
		SCAccountInfo__c testAccInfo = new SCAccountInfo__c();
		testAccInfo.Account__c = acc.Id;
		testAccInfo.ID2__c = 'SCAccInfo' + id2;
		return testAccInfo;
	}
	
	static SalesDetails__c getTestSalesDetails(Account acc) {
		SalesDetails__c testSD = new SalesDetails__c();
		testSD.Account__c = acc.Id;
		//testSD.CurrencyIsoCode = 'EUR';
		return testSD;
	}

	static void insertSalesAppSettings(String name, String value) {
		SalesAppSettings__c appSetting = new SalesAppSettings__c(Name = name, Value__c = value);
		insert appSetting;
	}
	
	static void insertTestPricebookEntryDetails() {
		
	}
	
	static void insertTestPricebook() {
		TestPricebook = new Pricebook2();
		TestPricebook.Name = 'TestPricebook';
		insert TestPricebook;
	}
	
	static void insertTestAccount() {
		TestAccount = new Account();
		TestAccount.Name = 'TestAccount';
		
		if (TestPricebook == null)
			insertTestPricebook();
		TestAccount.Pricebook__c = TestPricebook.Id;
		
		TestAccount.BHMondayAMFrom__c = BH_FROM_AM;
		TestAccount.BHMondayAMUntil__c = BH_UNTIL_AM;
		TestAccount.BHTuesdayAMFrom__c = BH_FROM_AM;
		TestAccount.BHTuesdayAMUntil__c = BH_UNTIL_AM;
		TestAccount.BHWednesdayAMFrom__c = BH_FROM_AM;
		TestAccount.BHWednesdayAMUntil__c = BH_UNTIL_AM;
		TestAccount.BHThursdayAMFrom__c = BH_FROM_AM;
		TestAccount.BHThursdayAMUntil__c = BH_UNTIL_AM;
		TestAccount.BHFridayAMFrom__c = BH_FROM_AM;
		TestAccount.BHFridayAMUntil__c = BH_UNTIL_AM;
		
		TestAccount.DHFridayAMFrom__c = DH_FROM_AM;
		TestAccount.DHFridayAMFrom__c = DH_UNTIL_AM;
		TestAccount.DHMondayAMFrom__c = DH_FROM_AM;
		TestAccount.DHMondayAMFrom__c = DH_UNTIL_AM;
		TestAccount.DHThursdayAMFrom__c = DH_FROM_AM;
		TestAccount.DHThursdayAMFrom__c = DH_UNTIL_AM;
		TestAccount.DHTuesdayAMFrom__c = DH_FROM_AM;
		TestAccount.DHTuesdayAMFrom__c = DH_UNTIL_AM;
		TestAccount.DHWednesdayAMFrom__c = DH_FROM_AM;
		TestAccount.DHWednesdayAMFrom__c = DH_UNTIL_AM;
		
		insert TestAccount;
	}
	
	static void insertTestTask() {
		Task t = new Task();
		t.Description = 'something to do';
		t.Status = 'Open';
		System.debug('######################' + TestAccount);
		t.WhatId = TestAccount.Id;
		insert t;
	}
	
	static void insertTestMarketingAttribute() {
		MarketingAttribute__c ma = new MarketingAttribute__c();
		ma.SetId__c = 'Global';
		ma.Attribute__c = 'Capacity';
		ma.Value__c = 'Beds>250';
		insert ma;
		
		AccountMarketingAttribute__c ama = new AccountMarketingAttribute__c();
		ama.Account__c = TestAccount.Id;
		ama.MarketingAttribute__c = ma.Id;
	}
	
	static void setupTest() {
		insertSalesAppSettings('AccountsWithDetailsQueryLimit', '10');
		insertSalesAppSettings('TaskOpenState', 'Open');
		insertSalesAppSettings('TaskCompletetdState', 'Completed');
		
		insertTestAccount();
		insertTestMarketingAttribute();
		
		TestBankAccount = getTestBankAccount(testAccount);
		insert testBankAccount;
		TestAccountInfo = getTestAccountInfo(testAccount, '001');
		insert testAccountInfo;
		TestSalesDetails = getTestSalesDetails(testAccount);
		insert testSalesDetails;
	}
		
	static testMethod void testGetDateTimeFromTimeStamp() {
		DateTime dt = CCWCRestAccountsWithDetails.getDateTimeFromTimeStamp('2014-03-18T12:00:00.000Z');
		System.assertEquals(dt, DateTime.valueOf('2014-03-18 12:00:00'));
	}	
	
	static testMethod void testCreateErrorObject() {
		//CCWCRestAccountsWithDetails.AccountWithDetails awd = CCWCRestAccountsWithDetails.createAccountWithError('Error');
		//System.assertEquals('Error', awd.Message);
		try {
			Integer i = 1/0;
		}catch(Exception e) {
			CCWCRestAccountsWithDetails.AccountWithDetails awd = CCWCRestAccountsWithDetails.createAccountWithError(e);
			System.assertEquals('System.MathException Divide by 0', awd.Message);
		}
	}
	
    static testMethod void testDoGet() {
		
		setupTest();		
		insertTestTask();
		WSDebugSettings__c debugSettings = new WSDebugSettings__c(Name = 'IsDebugActive', Value__c = '1');
		insert debugSettings;
		
		WSDebugSettings__c debug = [SELECT Value__c FROM WSDebugSettings__c LIMIT 1];
		System.assertNotEquals(debug, null);
				 
        RestRequest req = new RestRequest();
		req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/AccountsWithDetails';
		req.addParameter('accountIds', testAccount.Id);		
		req.addParameter('timestamp', '2014-03-28T16:16:19.000Z');
		req.httpMethod = 'GET';
		
        RestRequest req2 = new RestRequest();
        req2.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/AccountsWithDetails';
		req2.addParameter('accountIds', '000000000000000000');		
		req2.addParameter('timestamp', '2014-03-28T16:16:19.000Z');
		req2.httpMethod = 'GET';
        
		RestContext.request = req;
		RestContext.response = new RestResponse();		
		CCWCRestAccountsWithDetails.doGet();
		
		RestContext.request = req2; 
		RestContext.response = new RestResponse();		
		CCWCRestAccountsWithDetails.doGet();
        
        // Checking negative case where no ids is provided
        RestRequest req3 = new RestRequest();
        req3.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/AccountsWithDetails';
		req3.addParameter('accountIds', '');	
		req3.addParameter('timestamp', '2014-03-28T16:16:19.000Z');
		req3.httpMethod = 'GET';
        
		RestContext.request = req3;
		RestContext.response = new RestResponse();		
		CCWCRestAccountsWithDetails.doGet();
        
		//List<CCWCRestAccountsWithDetails.AccountWithDetails> resultList = CCWCRestAccountsWithDetails.doGet();
		//System.assertEquals(1, resultList.size());
		//System.debug('JM Debug: ' + resultList);
		//System.assertEquals(resultList[0].Account.Id, testAccount.Id);
		//System.assertEquals(resultList[0].Account.Id, testBankAccount.Account__c);
	}
}
