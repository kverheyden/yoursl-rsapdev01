/*
 * @(#)SCQuickCustomerSearchController.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This controller will be used for the quick autocomplete-search of customers
 *
 * @author Sergey Utko <sutko@gms-online.de>
 */
public with sharing class SCQuickCustomerSearchController
{
    public String searchString { get; set; }
    public List<Account> customers { get; set; }
    public Id selectedAccount { get; set; }
    
    /**
     * Constructor
     * 
     */
    public SCQuickCustomerSearchController()
    {
        customers = new List<Account>();        
    }

    /**
     * Seach (SOSL) for the Accounts
     * 
     */
    public PageReference getSearchProducts()
    {
        if(searchString.trim() != '' && searchString != null && searchString.trim().length() > 3)
        {            
            String searchQuery = 'FIND \'*' + String.escapeSingleQuotes(searchString) + '*\' IN ALL FIELDS ';
            searchQuery       += 'RETURNING Account (Id, Name, AccountNumber, ShippingCountry__c, ShippingCity__c, ShippingStreet__c, ShippingHouseNo__c ';
            //searchQuery       += 'Where ShippingCountry__c = \'DE\' ORDER BY Name Limit 100 )';
            searchQuery       += 'ORDER BY Name Limit 100 )';
            
            List<List<SObject>> searchList = search.query(searchQuery);
            
            customers = ((List<Account>)searchList[0]);
        }
        
        return null;
    }
    
    /**
     * Reload method. Neede to reRendering the page blocks
     * 
     */
    public PageReference reloadDetails()
    {
        return null;
    }

}
