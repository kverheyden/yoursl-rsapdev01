/*
 * @(#)CCWCOrderExternalOperationAddTest.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 /**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Note !!!
// ========
//@isTest(SeeAllData=true) not allowed by tests creating an order. Because a trigger SCOrder_AI_CallSapWebService calls the Web Service
//====================================
// CCWCOrderCreate if IFEnableTriggerOrderCreate__c of CCSetting is greater than 0. By production and qa data it is the case.
// Calling of Web services while testing causes an exception that aborts the test. So we will not get the test running over the 75 procent of 
// the code
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@isTest
public with sharing class CCWCOrderExternalOperationAddTest
{
    private static Boolean seeAllData = CCWCTestBase.isSeeingAllData();
    static testMethod void positiveTestCase1()
    {
        CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        CCWCTestBase.setAppSettingsForOrder(appSettings, seeAllData);
        CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
            ccSettings = CCSettings__c.getInstance();
        }
        CCWCTestBase.setCCSettings(ccSettings, seeAllData);
        
        CCWCTestBase.createDomsForOrderCreation(seeAllData);
        Boolean doUpsert = true;
        SCOrder__c order = CCWCTestBase.createTestOrder(appSettings, doUpsert);
        SCOrderExternalAssignment__c oxa = CCWCTestBase.createOrderExternalAssignment(order);
        

        Test.StartTest();
        // call service
        Boolean async = false;
        Boolean testMode = true;
        String requestMessageID = CCWCOrderExternalOperationAdd.callout(oxa.Id, async, testMode);       
        
        // make assertion after web call
        List<SCOrder__c> orderList1 = [select id, name, ERPStatusExternalAssignmentAdd__c from SCOrder__c where id = : order.Id];
        System.assertEquals(orderList1.size(), 1);
        System.assertEquals('pending', orderList1[0].ERPStatusExternalAssignmentAdd__c); 
        
        List<SCOrderExternalAssignment__c> oxaList = [select name, ERPStatusAdd__c, ERPOperationID__c from SCOrderExternalAssignment__c where id = : oxa.Id];
        System.assertEquals(oxaList.size(), 1);
        System.assertEquals('pending', oxaList[0].ERPStatusAdd__c); 

        List<SCInterfaceLog__c> ill = [Select Data__c from SCInterfaceLog__c where MessageID__c = : requestMessageID];
        System.assertEquals(true, ill.size() > 0);
        //String operationMaterialGroup = CCWCTestBase.getFromData(ill[0].Data__c, 'MaterialGroup');
        //System.assertEquals('MaterialGroup', operationMaterialGroup);

        
        // fill response structure
        CCWSGenericResponse.GenericServiceResponseMessageClass gr = new CCWSGenericResponse.GenericServiceResponseMessageClass();
        String messageId = u.getMessageID();
        String messageUUID = u.getMessageID();
        String operation = 'AddExternalOperation';
        String externalID = oxaList[0].Name;
        debug('externalID: ' + externalID);

        CCWCTestBase.prepareWCGenericResponse(gr, messageId, messageUUID, requestMessageID, operation, externalID);
        // add reference        
        String idType = 'Operation/Activity Number';
        String operationID = 'OperationID';
        String referenceID = operationID;
        CCWCTestBase.addReferenceItem (gr, idType, externalID, referenceId);

        // add log
        String typeId = 'TypeID';
        Integer severityCode = 1; 
        String msg = 'External operation added';
        
        CCWCTestBase.addLogEntry (gr, typeId, externalID, severityCode, msg);       
        // call the response service
        CCWSGenericResponse.transmit(gr);
        // make assertions 
        List<SCOrder__c> orderList2 = [select id, name, ERPStatusExternalAssignmentAdd__c, ERPOrderNo__c, ERPResultDate__c from SCOrder__c 
                            where id = : order.Id];
        System.assertEquals(orderList2.size(), 1);
        System.assertEquals('ok', orderList2[0].ERPStatusExternalAssignmentAdd__c); 
        Date today = Date.today();
        System.assertEquals(today, orderList2[0].ERPResultDate__c.date()); 
        
        List<SCOrderExternalAssignment__c> oxaList2 = [select name, ERPStatusAdd__c, ERPOperationID__c, ERPResultDate__c from SCOrderExternalAssignment__c where id = : oxa.Id];
        System.assertEquals(oxaList2.size(), 1);
        System.assertEquals('ok', oxaList2[0].ERPStatusAdd__c); 
        System.assertEquals(today, oxaList2[0].ERPResultDate__c.date()); 
        System.assertEquals(operationID, oxaList2[0].ERPOperationID__c); 

        Test.StopTest();
    }

    static testMethod void negativeTestCase1()
    {
        CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        CCWCTestBase.setAppSettingsForOrder(appSettings, seeAllData);
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
            ccSettings = CCSettings__c.getInstance();
        }
        CCWCTestBase.setCCSettings(ccSettings, seeAllData);
        
        CCWCTestBase.createDomsForOrderCreation(seeAllData);
        Boolean doUpsert = true;
        SCOrder__c order = CCWCTestBase.createTestOrder(appSettings, doUpsert);
        SCOrderExternalAssignment__c oxa = CCWCTestBase.createOrderExternalAssignment(order);
        
        Test.StartTest();
        // call service
        Boolean async = false;
        Boolean testMode = true;
        String requestMessageID = CCWCOrderExternalOperationAdd.callout(oxa.Id, async, testMode);       
        
        // make assertion after web call
        List<SCOrder__c> orderList1 = [select id, name, ERPStatusExternalAssignmentAdd__c from SCOrder__c where id = : order.Id];
        System.assertEquals(orderList1.size(), 1);
        System.assertEquals('pending', orderList1[0].ERPStatusExternalAssignmentAdd__c); 
        
        List<SCOrderExternalAssignment__c> oxaList = [select name, ERPStatusAdd__c, ERPOperationID__c from SCOrderExternalAssignment__c where id = : oxa.Id];
        System.assertEquals(oxaList.size(), 1);
        System.assertEquals('pending', oxaList[0].ERPStatusAdd__c); 
        
        // fill response structure
        CCWSGenericResponse.GenericServiceResponseMessageClass gr = new CCWSGenericResponse.GenericServiceResponseMessageClass();
        String messageId = u.getMessageID();
        String messageUUID = u.getMessageID();
        String operation = 'AddExternalOperation';
        String externalID = oxaList[0].Name;
        debug('externalID: ' + externalID);

        CCWCTestBase.prepareWCGenericResponse(gr, messageId, messageUUID, requestMessageID, operation, externalID);
        // add reference        
        String idType = 'Operation/Activity Number';
        String operationID = 'OperationID';
        String referenceID = operationID;
        CCWCTestBase.addReferenceItem (gr, idType, externalID, referenceId);

        // add log
        String typeId = 'TypeID';
        Integer severityCode = 3; 
        String msg = 'An external operation not added';
        
        CCWCTestBase.addLogEntry (gr, typeId, externalID, severityCode, msg);       
        // call the response service
        CCWSGenericResponse.transmit(gr);
        // make assertions 
        List<SCOrder__c> orderList2 = [select id, name, ERPStatusExternalAssignmentAdd__c, ERPOrderNo__c, ERPResultDate__c from SCOrder__c 
                            where id = : order.Id];
        System.assertEquals(orderList2.size(), 1);
        System.assertEquals('error', orderList2[0].ERPStatusExternalAssignmentAdd__c); 
        Date today = Date.today();
        System.assertEquals(today, orderList2[0].ERPResultDate__c.date()); 
        
        List<SCOrderExternalAssignment__c> oxaList2 = [select name, ERPStatusAdd__c, ERPOperationID__c, ERPResultDate__c from SCOrderExternalAssignment__c where id = : oxa.Id];
        System.assertEquals(oxaList2.size(), 1);
        System.assertEquals('error', oxaList2[0].ERPStatusAdd__c); 
        System.assertEquals(today, oxaList2[0].ERPResultDate__c.date()); 
        Test.StopTest();
    }

    static testMethod void positiveTestCaseWithoutContract()
    {
        CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        CCWCTestBase.setAppSettingsForOrder(appSettings, seeAllData);
        CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
            ccSettings = CCSettings__c.getInstance();
        }
        CCWCTestBase.setCCSettings(ccSettings, seeAllData);
        
        CCWCTestBase.createDomsForOrderCreation(seeAllData);
        Boolean doUpsert = true;
        SCOrder__c order = CCWCTestBase.createTestOrder(appSettings, doUpsert);
        SCOrderExternalAssignment__c oxa = CCWCTestBase.createOrderExternalAssignmentWithoutContract(order);

        Test.StartTest();
        // call service
        Boolean async = false;
        Boolean testMode = true;
        String requestMessageID = CCWCOrderExternalOperationAdd.callout(oxa.Id, async, testMode);       
        
        // make assertion after web call
        List<SCOrder__c> orderList1 = [select id, name, ERPStatusExternalAssignmentAdd__c from SCOrder__c where id = : order.Id];
        System.assertEquals(orderList1.size(), 1);
        System.assertEquals('pending', orderList1[0].ERPStatusExternalAssignmentAdd__c); 
        
        List<SCOrderExternalAssignment__c> oxaList = [select name, ERPStatusAdd__c, ERPOperationID__c from SCOrderExternalAssignment__c where id = : oxa.Id];
        System.assertEquals(oxaList.size(), 1);
        System.assertEquals('pending', oxaList[0].ERPStatusAdd__c); 
        
        List<SCInterfaceLog__c> ill = [Select Data__c from SCInterfaceLog__c where MessageID__c = : requestMessageID];
        System.assertEquals(true, ill.size() > 0);
        //String externalOperationMaterialGroup = CCWCTestBase.getFromData(ill[0].Data__c, 'MaterialGroup');
        //System.assertEquals('ExternalOperationMaterialGroup', externalOperationMaterialGroup);
        
        // fill response structure
        CCWSGenericResponse.GenericServiceResponseMessageClass gr = new CCWSGenericResponse.GenericServiceResponseMessageClass();
        String messageId = u.getMessageID();
        String messageUUID = u.getMessageID();
        String operation = 'AddExternalOperation';
        String externalID = oxaList[0].Name;
        debug('externalID: ' + externalID);

        CCWCTestBase.prepareWCGenericResponse(gr, messageId, messageUUID, requestMessageID, operation, externalID);
        // add reference        
        String idType = 'Operation/Activity Number';
        String operationID = 'OperationID';
        String referenceID = operationID;
        CCWCTestBase.addReferenceItem (gr, idType, externalID, referenceId);

        // add log
        String typeId = 'TypeID';
        Integer severityCode = 1; 
        String msg = 'External operation added';
        
        CCWCTestBase.addLogEntry (gr, typeId, externalID, severityCode, msg);       
        // call the response service
        CCWSGenericResponse.transmit(gr);
        // make assertions 
        List<SCOrder__c> orderList2 = [select id, name, ERPStatusExternalAssignmentAdd__c, ERPOrderNo__c, ERPResultDate__c from SCOrder__c 
                            where id = : order.Id];
        System.assertEquals(orderList2.size(), 1);
        System.assertEquals('ok', orderList2[0].ERPStatusExternalAssignmentAdd__c); 
        Date today = Date.today();
        System.assertEquals(today, orderList2[0].ERPResultDate__c.date()); 
        
        List<SCOrderExternalAssignment__c> oxaList2 = [select name, ERPStatusAdd__c, ERPOperationID__c, ERPResultDate__c from SCOrderExternalAssignment__c where id = : oxa.Id];
        System.assertEquals(oxaList2.size(), 1);
        System.assertEquals('ok', oxaList2[0].ERPStatusAdd__c); 
        System.assertEquals(today, oxaList2[0].ERPResultDate__c.date()); 
        System.assertEquals(operationID, oxaList2[0].ERPOperationID__c); 

        Test.StopTest();
    }


/*
    static testMethod void musterTest()
    {
        Test.StartTest();
        //Test.setMock(WebServiceMock.class, new CCWCOrderCreateTestMock()); 
        Test.StopTest();
    }   
*/
    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }

}
