@IsTest
public class cc_cceag_ctrl_test_SaleConsultant {
    
    /**
     * Default profile name
     */
    private static String communityProfileName = 'CloudCraze Customer Community User';


    /**
     * Returns the profile for the given name
     * @return Profile
     */
    public static Profile getProfile (String profileName) {

        Profile profile = [
            SELECT
                Id
            FROM
                Profile
            WHERE
            Name = :profileName
        ];

        return profile;
    }

    /**
     * Creates a user with its requried fields
     */
    private static Map<String, Object> createFixtureData ()
    {
        Account account = new Account(
            Name = 'Account'
        );
        insert account;
        
        Account slaveAccount = new Account(
            Name = 'SlaveAccount',
            BillingCity = 'BillingCity',
            BillingStreet = 'BillingStreet',
            BillingPostalCode = 'BillingPostalCode'
        );
        insert slaveAccount;
        
        CCAccountRole__c accountRole = new CCAccountRole__c(
            AccountRole__c = 'WE',
            MasterAccount__c = account.Id,
            SlaveAccount__c = slaveAccount.Id
        );
        insert accountRole;
           
        Contact contact = new Contact(
            AccountId = account.Id,
            LastName = 'Lastname'
        );
        insert contact;
        
        Profile profile = getProfile(communityProfileName);

        User user = new User(
            Username = 'user@example.org',
            LastName = 'Lastname',
            Email = 'user@example.org',
            Alias = 'alias',
            CommunityNickname = 'nickname',
            TimeZoneSidKey = 'Europe/Berlin',
            LocaleSidKey = 'de',
            EmailEncodingKey = 'UTF-8',
            ProfileId = profile.Id,
            LanguageLocaleKey = 'de',
            ContactId = contact.Id
        );

        Map<String, Object> fixtures = new Map<String, Object>();
        fixtures.put('user', user);
        fixtures.put('account', account);
        
        return fixtures;
    }
    
    
    public static testMethod void testFetchData() {
        
        Map<String, Object> fixtures = createFixtureData();
        User testUser = (User) fixtures.get('user');
        
        System.runAs(testUser) {
            
            ccrz.cc_RemoteActionContext context = new ccrz.cc_RemoteActionContext();
            
            ccrz.cc_RemoteActionResult result = cc_cceag_ctrl_SaleConsultant.fetchData(context);
    
            System.assertEquals(true, result.success);
            
            List<Map<String, String>> addresses = (List<Map<String, String>>) result.data;
            System.assertEquals(2, addresses.size());
            
            Map<String, String> address2 = addresses.get(1);
            System.assertEquals(true, address2.get('id').length() > 0);
            System.assertEquals('SlaveAccount', address2.get('name'));
            System.assertEquals('BillingStreet<br>BillingCity BillingPostalCode', address2.get('address'));
        }
    }
    
    
    public static testMethod void testPostMessage ()
    {
        Map<String, Object> fixtures = createFixtureData();
        User testUser = (User) fixtures.get('user');
        Account account = (Account) fixtures.get('account');
        
        System.runAs(testUser) {
            
            ccrz.cc_RemoteActionContext context = new ccrz.cc_RemoteActionContext();
            
            String message = 'MessageToConsultant';
            ccrz.cc_RemoteActionResult result = cc_cceag_ctrl_SaleConsultant.postMessage(context, account.Id, message);
    
          	System.assertEquals(true, result.success);
            
            Id feedItemId = (Id) result.data;
            
            FeedItem savedFeedItem = [
                SELECT
                	Body,
                	NetworkScope,
                	ParentId,
                	Type,
                	Visibility
                FROM
                	FeedItem
                WHERE
                	Id = :feedItemId
            ];
            
            System.assertEquals(message, savedFeedItem.Body);
            System.assertEquals('AllNetworks', savedFeedItem.NetworkScope);
            System.assertEquals(account.Id, savedFeedItem.ParentId);
            System.assertEquals('TextPost', savedFeedItem.Type);
            System.assertEquals('AllUsers', savedFeedItem.Visibility);
        }
    }
    
    
    public static testMethod void testHandleException() {
        
        ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
        NullPointerException thrownException = new NullPointerException();
        
        cc_cceag_ctrl_SaleConsultant.handleException(result, thrownException);
               
        List<ccrz.cc_bean_Message> messages = (List<ccrz.cc_bean_Message>) result.messages;
        System.assertEquals(1, messages.size());
        ccrz.cc_bean_Message message = messages.get(0);
        System.assertEquals(ccrz.cc_bean_Message.MessageType.CUSTOM, message.type);
        System.assertEquals('messagingSection-Error', message.classToAppend);
        System.assertEquals(ccrz.cc_bean_Message.MessageSeverity.ERROR, message.severity);
    }
}
