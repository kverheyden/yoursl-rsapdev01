/*
 * @(#)SCOrderConfirmationControllerTest.cls
 *
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author Dietrich Herdt <dherdt@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCOrderConfirmationControllerTest
{
    private static SCOrderConfirmationController orderConfirmationController;

    /**
     * orderComponentController under positiv test 1
     */
    static testMethod void orderConfirmationControllerPositiv1()
    {
        SCHelperTestClass.createOrderTestSet(true);
        ApexPages.currentPage().getParameters().put('oid', SCHelperTestClass.order.Id);
        
        Test.startTest();
        
        // time report can be closed, no errors
        orderConfirmationController = new SCOrderConfirmationController();
        orderConfirmationController.onInit();
        
        Test.stopTest();
        
        /*
        System.assertNotEquals(null, orderConfirmationController.ord);
        System.assertEquals('Person testaccount 00, Moormanweg 6, 9831 NK Aduard', orderConfirmationController.orderRoleRecipient);
        System.assertEquals('Person testaccount 00, Moormanweg 6, 9831 NK Aduard', orderConfirmationController.orderRoleCaller);
        System.assertEquals('VAM climaVAIR [026]', orderConfirmationController.product);
        // the employee is not constant (time can be changed)
        //System.assertEquals('Theo Burgt van de, 2011-03-24 01:00', orderConfirmationController.employee);
        System.assertNotEquals(null, orderConfirmationController.employee);
        */
    }
       
}
