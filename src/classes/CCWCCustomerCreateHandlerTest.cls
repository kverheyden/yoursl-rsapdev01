/**
* @author		Austen Buennemann
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				a.buennemann@yoursl.de
*
* @description	Test class for CCWCCustomerCreateHandler
*
* @date			26.03.2014
*
* Timeline:
* Name               DateTime                  Description
* Austen Buennemann  24.03.2014 10:29          Class created
*/

@isTest
public with sharing class CCWCCustomerCreateHandlerTest {


	@isTest 
	static void test()
	{
		CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        appSettings = SCApplicationSettings__c.getInstance();

		String recTypte_create; 
		String recTypte_update;
		recTypte_create = [Select Id From RecordType Where SobjectType = 'Request__c' AND DeveloperName = 'NewCustomerRegistration' LIMIT 1].Id; 
		recTypte_update = [Select Id From RecordType Where SobjectType = 'Request__c' AND DeveloperName = 'CustomerMasterDataUpdate' LIMIT 1].Id;
		String recTypeDefault = [Select Id From RecordType Where SobjectType = 'Request__c' LIMIT 1].Id;
		                                    
        SalesAppSettings__c settingSalesApp = new SalesAppSettings__c();
        settingSalesApp.Name = 'RequestMasterDataUpdateRecordTypeId';
        settingSalesApp.Value__c = recTypte_update;
        insert settingSalesApp;        
        
        CCSettings__c ccSettings = new CCSettings__c();
        ccSettings.SAPDispBasicAuthUsername__c = 'user';
        ccSettings.SAPDispBasicAuthPassword__c = 'passsword';
        ccSettings.SAPDispEndpointCustomerCreate__c = 'CustomerCreateEndpoint';
        ccSettings.SAPDispServer__c = 'Server';
        insert ccSettings;
        
        AnnualOrderDiscount__c aod				= new AnnualOrderDiscount__c();
        aod.DiscountLevel__c					= 2;
        aod.MinOrderQuantityHL__c				= 25;
        aod.DiscountPercent__c					= 0.7;
        insert aod;
        
        PaymentConditionDiscount__c pcd 		= new PaymentConditionDiscount__c();
        pcd.PaymentType__c						= 'Central Effort';
        insert pcd;
        
        SponsoringDiscount__c spd				= new SponsoringDiscount__c();
        spd.DiscountLevel__c					= 3;
        insert spd;
        
        UnitThroughputDiscount__c utd			= new UnitThroughputDiscount__c();
        utd.DiscountLevel__c					= 7;
        utd.PemPom__c							= 'POM';
        utd.DiscountPerL__c						= 0.75;
        utd.MinimumThroughputHL__c				= 75;
        insert utd;
        
        WaterConceptDiscount__c wcd				= new WaterConceptDiscount__c();
        wcd.DiscountLevel__c					= 4;
        insert wcd;
        
		MarketingAttribute__c maa				= new MarketingAttribute__c();
		maa.Attribute__c						= '123';
		maa.UniqueKey__c						= 'Z001#Z00254789';
		insert maa;
		
		Request__c req 							= new Request__c();
		req.OutletCompanyName__c 				= 'Muster GmbH';
		req.ShipToCity__c 						= 'Musterhausen';
		req.PayerEmail__c 						= 'abc@def.de';
		req.PricingAnnualOrderDiscount__c 		= aod.Id;
		req.PricingPaymentDiscount__c			= pcd.Id;
		// req.PricingSponsoringDiscount__c		= spd.Id;
		req.PricingUnitThroughputDiscount__c	= utd.Id;
		req.PricingWaterConceptDiscount__c		= wcd.Id;
		req.MarketingAttributeIDsCSV__c			= maa.Id;
        req.SponsoringDiscountLevel__c			= 2;
		req.RecordTypeId						= String.isEmpty(recTypte_create) ? recTypeDefault : recTypte_create;
		
		insert req;
		
		RequestAttachment__c attachment = new RequestAttachment__c();
		attachment.Request__c = req.Id;
		attachment.Type__c = 'Test';
		attachment.ValidFrom__c = date.today();
		attachment.ValidTo__c = date.today();
		attachment.DocumentNumber__c = 'A1234';
		insert attachment;
		
		Attachment att = new Attachment();
		att.ParentId = attachment.Id;
 	    att.Name = 'Test Attachment for Parent';  
    	att.Body = Blob.valueOf('Test Data');  
        insert att;  
        
		Request__c req_SendInsert = [Select Send_to_SAP__c, Id From Request__c Where Id =: req.Id];
		req_SendInsert.Send_to_SAP__c = true;
		update req_SendInsert;		        
        
		Account  account = new Account();
        account.LastName__c = 'testaccount 00';
        account.FirstName__c = 'Person';
        account.BillingPostalCode = '33106 PB'; 
        account.BillingCountry__c = 'DE';
        account.CurrencyIsoCode = 'EUR';
        account.BillingStreet = 'Karl-Schurz Str.';
        account.BillingHouseNo__c = '29';
        account.BillingPostalCode = '33100 PB';
        account.BillingCity = 'Padebrorn';
        account.BillingCountry__c = 'PB';
        insert account;    
        
		Request__c req_update 							= new Request__c();
		req_update.OutletCompanyName__c 				= 'MusterDrink GmbH';
		req_update.ShipToCity__c 						= 'Musterhusen';
		req_update.ChangedFields__c						= 'OutletCompanyName__c,ShipToCity__c ';
		req_update.RecordTypeId							= String.isEmpty(recTypte_update) ? recTypeDefault : recTypte_update;
		req_update.Account__c							= account.Id;		
		req_update.RelatedAccount__c					= account.Id;
		insert req_update;
		
		ContactAvoidTriggerRecursion.run = true;
		Request__c req_SendUpdate = [Select Send_to_SAP__c, Id From Request__c Where Id =: req_update.Id];
		req_SendUpdate.Send_to_SAP__c = true;
		update req_SendUpdate;		
		CCWCCustomerCreateHandler send = new CCWCCustomerCreateHandler();
		List<Request__c> requests = new List<Request__c>();
		requests.add(req);	
		List<RequestAttachment__c> attach = new List<RequestAttachment__c>();
		attach.add(attachment);
		List<Attachment> attachm = new List<Attachment>();
		attachm.add(att);
		
		send.sendCallout(requests, true, true);
		send.callout(requests[0].Id, false);
		send.sendRequestCallout(attach, false);
		send.sendRequest(attachm, false);

	}
}
