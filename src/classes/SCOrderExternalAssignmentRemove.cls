/*
 * @(#)SCOrderExternalAssignmentRemove.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This class sets the order external assignment status to 'delete' and calls SAP webservice
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCOrderExternalAssignmentRemove
{
    private SCOrderExternalAssignment__c assignment;
    private String oid;

    public SCOrderExternalAssignmentRemove()
    {
        
    }
    
    public PageReference setStatusDelete()
    {
        PageReference p;
        
        if ( ApexPages.currentPage().getParameters().containsKey('oid') && ApexPages.currentPage().getParameters().get('oid') != '' )
        {
            oid = ApexPages.currentPage().getParameters().get('oid');
             
            p = new PageReference('/' + oid);
                
            assignment = [ Select Id, Name, Status__c From SCOrderExternalAssignment__c Where id = : oid ];
        }
        else
        {
            oid = null;
            assignment = null;
            System.debug('#### Cannot set remove, oid is null!');
            
            return null;
        }
        
        try
        {
            assignment.Status__c = 'deleted';
            
            upsert assignment;
            
            CCWCOrderExternalOperationRem.callout(assignment.id, true, false);
            
            return p;
            
        }
        catch(Exception e)
        {
            System.debug('#### Cannot set remove: ' + e.getMessage());
            
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(myMsg);
        }
        
        return null;
        
    }
}
