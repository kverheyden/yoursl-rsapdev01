/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class cc_cceag_ctrl_test_WishlistEntry {

    static testMethod void myUnitTest() {
        List<ccrz__E_Product__c> ps = new List<ccrz__E_Product__c>{
			new ccrz__E_Product__c(Name='Product1', ccrz__Sku__c='sku1', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12),
			new ccrz__E_Product__c(Name='Product2', ccrz__Sku__c='sku2', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12),
			new ccrz__E_Product__c(Name='Product3', ccrz__Sku__c='sku3', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12),
			new ccrz__E_Product__c(Name='Product4', ccrz__Sku__c='sku4', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12)
		};
		insert ps;
		List<ccrz__E_ProductMedia__c> mediaList = new List<ccrz__E_ProductMedia__c>();
		for(ccrz__E_Product__c thisProd : ps){
			ccrz__E_ProductMedia__c media = new ccrz__E_ProductMedia__c();
			media.ccrz__Product__c = thisProd.id;
			media.ccrz__URI__c = 'media URI Dummy';
			media.ccrz__EndDate__c = System.today();
			media.ccrz__StartDate__c = System.today();
			mediaList.add(media);
			
		}
		insert mediaList;
		
		List<ccrz__E_Promo__c> promoList = new List<ccrz__E_Promo__c>();
		integer index = 1;
		for(ccrz__E_ProductMedia__c media : mediaList){
			ccrz__E_Promo__c promo = new ccrz__E_Promo__c(
	            Name = 'promo-' + string.valueOf(index),
	            ccrz__LongDesc__c = 'longDesc=' + string.valueOf(index),
	            ccrz__ShortDesc__c = 'shortDesc=' + string.valueOf(index),
	            ccrz__PageLocation__c = 'Landing Page',
	            ccrz__LocationType__c = 'Left Nav',
	            ccrz__StorefrontMS__c='DefaultStore',
	            ccrz__product__c = media.ccrz__product__c,
	            ccrz__Enabled__c = true,
	            ccrz__StartDate__c = Date.today().addDays(-1),
	            ccrz__EndDate__c = Date.today().addDays(1),
	            ccrz__Sequence__c = 1
        	);
        	promoList.add(promo);
		}
		insert promoList;
		list<Id> promoIds = new list<Id>();
		for(ccrz__E_Promo__c promo : promoList) {
			promoIds.add(promo.id);
		}
		cc_cceag_api_ProductQuantityRule pqr = new cc_cceag_api_ProductQuantityRule();
		pqr.setStorefrontSettings(null);
		
		cc_cceag_ctrl_WishlistEntry wishListEntry = new cc_cceag_ctrl_WishlistEntry();
		Map<String, cc_cceag_bean_Product> result = cc_cceag_ctrl_WishlistEntry.getPromotionProducts('DefaultStore', null, promoIds);
		system.assert(result != null);
    }
}
