/**
* @author		Austen Buennemann
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				a.buennemann@yoursl.de
*
* @description	Customer Registration webservice to SAP PI
*
* @date			21.10.2014
*
* Timeline:
* Name               DateTime                  Description
* Austen Buennemann  21.09.2014 17:15          Class created
*/

public with sharing class CCWCCustomerRegistration  extends SCInterfaceExportBase{
	private static CokeIDRequest__c regAccount;
	private static String regUserID;
    private static String regCustomerPortalID;
    private static String regComment;
    private static String regAccNumberRegUser;
    private static String regAccountNumberSoldTo;
    private static String [] regAccountNumberShipTo;

    public CCWCCustomerRegistration()
    {
    }  

    public CCWCCustomerRegistration(String interfaceName, String interfaceHandler, String refType, CokeIDRequest__c request, 
    		String userID, String customerPortalID, String comment, String accsoldto, 
    			String userNumber, String [] accshipto)
    {
    	regAccount         				= request;
    	regUserID           			= userID;
    	regCustomerPortalID 			= customerPortalID;
    	regComment         				= comment;
    	regAccNumberRegUser				= userNumber;
    	regAccountNumberSoldTo			= accsoldto;
    	regAccountNumberShipTo			= accshipto;
    }
          
     /**
     * Prepare to make the web service callout
     *
     * @param accString JSON string of CokeIDRequest__c object
     * @param userID User ID
     * @param customerPortalID customer Portal ID
     * @param comment comment
     * @param accsoldto Account Number SoldTo
     * @param userNumber Account Number Registration User
     * @param accshipto List Account Number ShipTos
     * @param testMode true: emulate call
     */
    @future(callout=true)     
	public static void executecallout(String accString, String userID, String customerPortalID, String comment,  
						String accsoldto, String userNumber, String [] accshipto, boolean testMode){			
										
		CokeIDRequest__c acc = (CokeIDRequest__c) JSON.deserialize(accString, CokeIDRequest__c.class);
		CCWCCustomerRegistration.callout(acc.Id, false, testMode, acc, userID, customerPortalID, comment,  
			accsoldto, userNumber, accshipto);
	}
    
     /**
     * Prepare to make the web service callout synchrounously or asynchronously (future)
     *
     * ### Set the interface name, its interface handler and a refType for head id in an interface object!
     *
     * @param CokeIDRequest__c ID
     * @param true if future call
     * @param testMode true: emulate call
     * @param accrequest JSON string of CokeIDRequest__c object
     * @param userID User ID
     * @param customerPortalID customer Portal ID
     * @param comment comment     
     * @param accsoldto Account Number SoldTo
     * @param userNumber Account Number Registration User
     * @param accshipto List Account Number ShipTos
     * @param testMode true: emulate call     
     */    
    public static String callout(String ibid, boolean async, boolean testMode, CokeIDRequest__c accrequest, String userID, 
    				String customerPortalID, String comment, String accsoldto, String userNumber, 
    					String [] accshipto)
    {
        String interfaceName = 'CustomerRegistration';
        String interfaceHandler = 'CCWCCustomerRegistrationHandler';
        String refType = 'CokeIDRequest__c';
        CCWCCustomerRegistration request = new CCWCCustomerRegistration(interfaceName, interfaceHandler, refType, accrequest, 
        									userID, customerPortalID, comment, accsoldto, userNumber, accshipto);
        debug('Request ID: ' + ibid);
        return request.calloutCore(ibid, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCCustomerRegistration'); 
    }  

     /**
     * Reads records to send
     *
     * @param Id CokeIDRequest__c
     * @param retValue Map Object
     * @param testMode if true no call is made, because in the test mode the salesforce framework prevent it.
     */
    public override void readSalesforceData(String Id, Map<String, Object> retValue, Boolean testMode)
    {    	
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();                
        retValue.put('ROOT', regAccount);
        debug('CokeIDRequest__c: ' + retValue);
    }

    /**
     * Fills the send structure and sends the call to SAP
     * 
     * @param dataMap with the Account Item under the key ROOT
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure
     *           message = data sent to ERP
     *           message_v1 = endpoint 
     *           message_v2 = a return value of web service 
     *           message_v3 = a mandatory fields error
     *
     */
      
      
    public override Boolean fillAndSendERPData(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs)
    {
    	
        Boolean retValue = true;
        CokeIDRequest__c acc = (CokeIDRequest__c)dataMap.get('ROOT');
        
        // instantiate the client of the web service
        piCceagDeSfdc_CustomerRegistration.HTTPS_Port ws = new piCceagDeSfdc_CustomerRegistration.HTTPS_Port();
        
        // sets the basic authorization for the web service
        ws.inputHttpHeaders_x = u.getBasicAuth();
        
        //Test
        //Map<String, String> autMap = new Map<String, String>(); //
        //autMap.put('Authorization: Basic', 'TEST'); //
		//ws.inputHttpHeaders_x = autMap; //
		
        // sets the endpoint of the web service
        ws.endpoint_x  = u.getEndpoint('CustomerRegistration');
        
        //Test
        //ws.endpoint_x  = 'http://requestb.in/1521raf1'; //  
        
        debug('endpoint: ' + ws.endpoint_x);
        rs.message_v1 = ws.endpoint_x;
		ws.timeout_x = u.getTimeOut();
        String errormessage = '';
        

        try
        {            
            piCceagDeSfdc_CustomerRegistration.CC_CustomerRegistration reg = new piCceagDeSfdc_CustomerRegistration.CC_CustomerRegistration();           
                        
            setRequesteData(reg, u, testMode);
            
            String jsonsfdcItems = JSON.serialize(reg);
            String fromJSONMapRequest = getDataFromJSON(jsonsfdcItems);
            debug('from json CustomerRegistration data: ' + fromJSONMapRequest);
                                                            
            rs.message = '\n messageData: ' + fromJSONMapRequest;
            
            // check if there are missing mandatory fields
            if(rs.message_v3 == null || rs.message_v3 == '')
            {
                // All mandatory fields are filled
                // callout
                if(!testMode)
                {
                    // It is not the test from the test class
                    errormessage = 'Call ws.CustomerRegistration: \n\n';
                    rs.setCounter(1);
                    // parameter: String UserID,String CustomerPortalID,piCceagDeSfdc_CustomerRegistration.AccountNumberSoldTo_element AccountNumberSoldTo,piCceagDeSfdc_CustomerRegistration.AccountNumberShipTo_element[] AccountNumberShipTo,String Comment
                    // CCWSCustomerRegistrationRequest_Out(String UserID,String CustomerPortalID,String AccountNumberSoldTo,String[] AccountNumberShipTo,String AccountNumberRegistrationUser,String Comment) {                    
                    ws.CCWSCustomerRegistrationRequest_Out(reg.UserID, reg.CustomerPortalID, reg.AccountNumberSoldTo, reg.AccountNumberShipTo, reg.AccountNumberRegistrationUser, reg.Comment);
                    rs.Message_v2 = 'void';
                }
            }
        }
        catch(Exception e)
        {
            String prevMsg = e.getMessage();
            if(prevMsg.contains('Parser was expecting element') && prevMsg.contains('CustomerRegistration:SFDCResponse')){
            	system.debug('ignore Error in async Webservice: Parser was expecting element ... CustomerRegistration:SFDCResponse ...');
            }else{
	            String newMsg = 'fillAndSendERPData: ' + errormessage + ' \n\n ' + prevMsg;
	            debug(newMsg);
	            e.setMessage(newMsg);
	            throw e;
            }
        }
                
        return retValue;

    }
     
   
     /**
     * sets the call out Request structure with data form
     *
     * @param reg callout data
     * @param u instance of the util class CCWSUtil
     * @param testMode
     * @return ###
     */
     
 
    public String setRequesteData(piCceagDeSfdc_CustomerRegistration.CC_CustomerRegistration reg, CCWSUtil u, Boolean testMode)
    {
        CCSettings__c ccset = CCSettings__c.getInstance();
    
        String retValue = '';
     	      
	       	reg.UserID = regUserID;
	        reg.CustomerPortalID = regCustomerPortalID;
	        reg.AccountNumberSoldTo = regAccountNumberSoldTo;
	        reg.AccountNumberShipTo = regAccountNumberShipTo;
	        reg.AccountNumberRegistrationUser = regAccNumberRegUser;
	        reg.Comment = regComment;
        	        
        return retValue;    
    }


   /**
     * Sets the ERPStatusEquipmentUpdate__c in the root object to 'pending'
     * 
     * @param dataMap of objects to dispatch
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure with error messages
     * @param error true if some error has occurred
     */
    public override void setERPStatus(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs, Boolean error)
    {
    }
    

    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }

}
