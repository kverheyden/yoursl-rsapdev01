/**
 * @(#)SCInterfaceClearing
 * Copyright 2012 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

 /**
 * @author Eugen Tiessen <etiessen@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCInterfaceClearingController 
{
    
    private static final String TYPE_MOBILEORDER = 'mobileorder';
    
    private Map<String, SCInterfaceClearing__c> clearingMap {private get;private set;}
    private Integer currentPage = 1;
    public SCboInterfaceClearing boClearing {get; private set;}
    
    public ApexPages.StandardSetController clearingController {get; private set;} 
    
    
    public List<Note> noteList {get; private set;}
    
    public SCInterfaceClearing__c selectedClearingItem {get; set;}
    public String selectedClearingItemId {get; set;}
    public String attToDeleteId {get; set;}
    public String selectedStatus
    {
        get
        {
            if(selectedStatus == null)
            {
                selectedStatus = SCboInterfaceClearing.STATUS_CHECK;
            }
            return selectedStatus;
        }
        set;    
    }
    
    public List<String> selectedTypes
    {
    	get
    	{
    		if(selectedTypes == null)
    		{
    			selectedTypes = new List<String>(); 
    			selectedTypes.addAll(SCboInterfaceClearing.SUPPORTED_OBJECTS);
    		}
    		return selectedTypes;
    	}
    	set;	
    }
    
    public Boolean isValid {get; private set;}
    
    public String supportedObjects
    {
        get
        {
            List<String> supList = new List<String>();
            supList.addAll(SCboInterfaceClearing.SUPPORTED_OBJECTS);
            return String.join(supList,',');
        }
        private set;
    }
    public List<Attachment> attachmentList 
    {
        get
        {
            if(boClearing != null)
            {
                return boClearing.attachmentList;
            }
            return null;
        } 
        private set;
    }
    
    public Map<String,Boolean> attValidationMap 
    {
        get
        {
            if(boClearing != null)
            {
            	system.debug('#### attachmentValidationMap in Controller: ' + boClearing.attValidationMap);
                return boClearing.attValidationMap;
                
            }
            return null;
        } 
        private set;
        
    }
    
    public Boolean showTopErrorMessage 
    {
        get
        {
            //return result only once
            Boolean tmpMsg = showTopErrorMessage;
            showTopErrorMessage = false;
            system.debug('#### showTopErrorMessage: ' + tmpMsg);
            return tmpMsg;
        } 
        private set;
    }
    
    public SCTool__c dateTool
    {
    	get
    	{
    		if(dateTool == null)
    		{
    			dateTool = new SCTool__c();
    		}
    		return dateTool;
    	}
    	set;
    	
    }
    
    public SCOrder__c dummyObject4InputFields
    {
    	get
    	{
    		if(dummyObject4InputFields == null)
    		{
    			dummyObject4InputFields = new SCOrder__c();
    		}
    		return dummyObject4InputFields;
    	}
    	set;
    	
    }
    
    public SCMessage__c dummyObject4Resource
    {
    	get
    	{
    		if(dummyObject4Resource == null)
    		{
    			dummyObject4Resource = new SCMessage__c();
    		}
    		return dummyObject4Resource;
    	}
    	set;
    	
    }
    
    public SCInterfaceClearingController()
    {
        system.debug('#### SCInterfaceClearingController');
        
        this.init();
    }
    
    private void init()
    {
        system.debug('#### init');
        this.clearingMap = new Map<String, SCInterfaceClearing__c>();
        //this.attValidationMap = new Map<String,Boolean>();
        this.isValid = false;
        
        try
        {
            initLocator();
            
        }
        catch(Exception e)
        {
            
        }
        
        
    }
    
    private void initLocator()
    {
        /*
        clearingController = new ApexPages.StandardSetController(Database.getQueryLocator(
            [
                SELECT 
                    Id,
                    Name, 
                    Type__c,
                    Status__c, 
                    ResultInfo__c,
                    ResultCode__c,
                    Resource__c, 
                    Order__c, 
                    MaterialMovement__c,
                    CreatedDate,
                    LastModifiedDate,
                    LastModifiedById,
                    (
                    	SELECT 
                    		Id 
                    	FROM 
                    		Attachments
                    	WHERE
                    		//CCEAG
                    		//irgnore updated orderrepaircodes. This should not happened.
	                    	//orderrepaircode should not have activity U
							(
								Name = 'orderrepaircode'
								AND
								Description != 'U'
							)
							//all others
							OR
							(
								Name != 'orderrepaircode'
								
							)
						
                    ) 
                    
                FROM
                    SCInterfaceClearing__c 
                WHERE
                  //Status__c !=: SCboInterfaceClearing.STATUS_COMPLETED
                  Status__c =: this.selectedStatus
                ORDER BY
                    Name DESC
                LIMIT 1000
            ]
        ));
        
        */
        String whereClause = ' WHERE Status__c = \'' + this.selectedStatus + '\' ';
							
        //select only selected types (mobileorder,assignment,materialmovement...)
        if(this.selectedTypes != null)
        {
        	whereClause +='	AND Type__c IN :selectedTypes ';
        }	
        
        //select by date from
        if(this.dummyObject4InputFields.Price_Valid_From__c != null)
        {
			Date dateFrom = this.dummyObject4InputFields.Price_Valid_From__c; 
        	whereClause +='	AND createdDate >= :dateFrom ';
        }
        //select by date to
        if(this.dummyObject4InputFields.Price_Valid_To__c != null)
        {
        	Date dateTo = this.dummyObject4InputFields.Price_Valid_To__c.addDays(1); 
        	whereClause +='	AND createdDate < :dateTo ';
        }
         	
        //select by engineer
        if(this.dummyObject4Resource.Resource__c != null)
        {
        	whereClause += ' AND Resource__c = \'' + dummyObject4Resource.Resource__c + '\'';
        }	
        				
        //select by order
        if(this.dummyObject4InputFields.OrderOrigin__c != null)
        {
        	whereClause += ' AND Order__c = \'' + dummyObject4InputFields.OrderOrigin__c + '\'';
        }
        	
		String stmt =	'SELECT  ' +
						'	Id, ' +
						'	Name, ' + 
						'	Type__c, ' +
						'	Status__c, ' + 
						'	ResultInfo__c, ' +
						'	ResultCode__c, ' +
						'	Resource__c, ' + 
						'	Order__c,  ' +
						'	MaterialMovement__c, ' +
						'	CreatedDate, ' +
						'	LastModifiedDate, ' +
						'	LastModifiedById, ' +
						'	( ' +
						'		SELECT ' +
						'			Id ' + 
						'		FROM ' +
						'			Attachments ' +
						'		WHERE ' +
									//CCEAG
									//irgnore updated orderrepaircodes. This should not happened.
									//orderrepaircode should not have activity U
						'		( ' +
						'			Name = \'orderrepaircode\' ' +
						'			AND ' +
						'			Description != \'U\' ' +
						'		) ' +
								//all others
						'		OR ' +
						'		( ' +
						'			Name != \'orderrepaircode\' ' +
						'		) ' +	
						'	)  ' +
						'FROM ' +
						'	SCInterfaceClearing__c  ' +
						whereClause +
						'ORDER BY ' +
						'	Name DESC ' +
						'LIMIT 1000';
								
        system.debug('#### generated statement: ' + stmt);
        clearingController = new ApexPages.StandardSetController(Database.getQueryLocator(stmt));
        //GMSET TODO: set the pagesize by application setting?
        clearingController.setPageSize(10);
        clearingController.setpageNumber(this.currentPage);
        
        //fill map with ther first page
        List<SCInterfaceClearing__c> cList= (List<SCInterfaceClearing__c>)this.clearingController.getRecords();
        this.clearingMap.clear();
        this.clearingMap.putAll(cList);
               
    
    }
    
    public void onSelectClearingItem()
    {
        system.debug('#### onSelectClearingItem');
        if(selectedClearingItemId == null )
        {
            system.debug('#### no clearing item selected');
            return ;
        }
        /**
         * activate if neccessary
        if(this.selectedClearingItem.Id == selectedClearingItemId)
        {
            system.debug('#### item already selected. nothing to do...');
            return ;
        }
        **/
        
        //TODO validate if clearing item available
        this.selectedClearingItem = this.clearingMap.get(selectedClearingItemId);
        this.boClearing = new SCboInterfaceClearing(this.selectedClearingItem);    
        
       
        
        //Get Notes for SCInterfaceClearing__c
        try
        {   
            this.noteList =
            [
                SELECT 
                    Title,
                    Body,
                    CreatedDate,
                    CreatedById
                FROM 
                    Note
                WHERE
                    ParentId IN 
                    (
                        SELECT  
                            Id 
                        FROM 
                            SCInterfaceClearing__c 
                        WHERE 
                            Id = :selectedClearingItemId
                    ) 
                ORDER BY
                    CreatedDate DESC
                
            ];
            
            
            system.debug('#### Notes found: ' + noteList);
        }
        catch(Exception e)
        {
            system.debug('#### Exception: ' + e);
            this.noteList = null;
        }
                
    }
    
    public void onDeleteAll()
    {
        Integer countValues = this.clearingMap.size();
        
        delete this.clearingMap.values();
        initLocator();
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, countValues  +' Objects deleted'));
        //TODO Exceptionhandling
        this.showTopErrorMessage = true;
        this.deselectClearingItem();
    }
    
    public void onDelete()
    {
        this.selectedClearingItem = this.clearingMap.get(selectedClearingItemId);
        String itemName = selectedClearingItem.Name;
        String itemtype = selectedClearingItem.Type__c;
        //TODO Exceptionhandling
        Database.delete(this.selectedClearingItemId);
        initLocator();
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Object deleted', 'Name: ' + itemName + ', Type: ' + itemType));
        this.showTopErrorMessage = true;
        
        this.deselectClearingItem();
    }
    
    public void onSave()
    {
        system.debug('#### start onSave()');
        if(boClearing.save())
        {
            this.showTopErrorMessage = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Object saved. ', 'Name: ' + selectedClearingItem.Name + ', Type: ' + selectedClearingItem.Type__c));
            
            createNote(System.Label.SC_msg_Changed, this.selectedClearingItem.Name, this.selectedClearingItem.Id);
            deselectClearingItem();
            this.initLocator();
            return;
        }
        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Object could not be saved'));
        createNote(System.Label.SC_app_Error, boClearing.getErrorMessages(), this.selectedClearingItem.Id);
        this.onRefreshSelection();
        //this.onSelectClearingItem(); 
            
    }
    
    public Boolean validate()
    {
        return boClearing.validate(true);
    }
       
    public PageReference onValidate()
    {
        this.onSelectClearingItem();
        this.isValid = validate();
        return null;
    }
    
    public void onCancel()
    {
        deselectClearingItem();
    }
    
    
    
    private void deselectClearingItem()
    {
        this.selectedClearingItem = null;
        this.selectedClearingItemId = null;
        this.boClearing = null;
    }
    
    public List<SCInterfaceClearing__c> getClearingList()
    {
        List<SCInterfaceClearing__c> cList= (List<SCInterfaceClearing__c>)this.clearingController.getRecords();
        
        if(clearingController.getPageNumber() != this.currentPage)
        {
            this.clearingMap.clear();
            this.clearingMap.putAll(cList);
            this.currentPage = clearingController.getPageNumber();
        }
        return cList;
        
    }
    
    public void onRefreshSelection()
    {
        onValidate();
    }
    
    public void onChangeStatusSelection()
    {
        initLocator();
        deselectClearingItem();
    }
    
    public void onDeleteAttachment()
    {
        try
        {
            Attachment attToDelete = 
            [
                SELECT Id, Name FROM Attachment WHERE Id =:this.attToDeleteId LIMIT 1
            ]; 
            //String name = attToDelete.Name;
            String objectName = String.valueOf(this.boClearing.objMap.get(this.attToDeleteId).get('Name'));
            String name =   (objectName != null && objectName.length() > 0) ? 
                                attToDelete.Name + ' (' + objectName + ')'  :
                                attToDelete.Name; 
            delete attToDelete;
            
            createNote('Deleted', name, this.selectedClearingItem.Id);
       
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Object deleted. Name: ' + name ));
            this.onSelectClearingItem();
            this.validate();
        }
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Object could not be deleted: ' + e.getMessage()));
        }           
    }
    
    public List<SelectOption> getStatusOptionList() {
        List<SelectOption> options = new List<SelectOption>();
         
        /*for(String status : SCboInterfaceClearing.STATUS_FLAGS)
        {
            options.add(new SelectOption(status,SCboInterfaceClearing.statusMap.get(status)));
        }*/
        
        Schema.DescribeFieldResult dr = SCInterfaceClearing__c.Status__c.getDescribe();
        for(Schema.PicklistEntry entry: dr.getPicklistValues())
        {
        	system.debug('entry-> value:'+dr.getName()+entry.getValue()+' label:'+entry.getLabel());
        	options.add(new SelectOption(entry.getValue(),entry.getLabel()));
		} 
        return options;
    }
    
    public List<SelectOption> getTypeOptionList() 
    {
        List<SelectOption> options = new List<SelectOption>();
        
        /*for(String status : SCboInterfaceClearing.SUPPORTED_OBJECTS)
        {
        	options.add(new SelectOption(status,status));
        }*/
        
        Schema.DescribeFieldResult dr = SCInterfaceClearing__c.Type__c.getDescribe();
        for(Schema.PicklistEntry entry: dr.getPicklistValues())
        {
        	system.debug('entry-> value:'+dr.getName()+entry.getValue()+' label:'+entry.getLabel());
        	options.add(new SelectOption(entry.getValue(),entry.getLabel()));
		}             
            
        return options;
    }
    
    private static void createNote(String title, String msg, String clearingId)
    {
        Note note = new Note
        (   
            Title = title,
            Body = msg,
            parentId = clearingId         
        );
        insert note;
    }
    
    
    
    //SalesForce don't allow to delete this internal class. There are no dependencies
    //Message:
    //This apex class is referenced elsewhere in salesforce.com.  Remove the usage and try again.   
    //SCInterfaceClearingController.cls /dev All/src/classes    line 0  Force.com save problem
    //
    private class ObjectValidation
    {
        
    }
    
    public class InvalidDataException extends Exception {}
}
