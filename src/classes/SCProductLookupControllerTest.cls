/*
 * @(#)SCProductLookupControllerTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCProductLookupControllerTest {

    static testMethod void testConstructor() {
        SCProductLookupController controller = new SCProductLookupController();
    }
    
    
    static testMethod void testgetAllBrand() {
        SCProductLookupController controller = new SCProductLookupController();
        
        List<SelectOption> options = controller.getAllBrand();
        
        //TODO create brands and select them, do the assert
    }
    
    static testMethod void testGetFoundProducts() {
        SCProductLookupController controller = new SCProductLookupController();
        
        List<SCProductModel__c> foundProducts = controller.getFoundProducts();
        
        //TODO create brands and select them, do the assert
    }    

    static testMethod void testSetController()
    {
        SCProductLookupController controller = new SCProductLookupController();
        
        //TODO create products first!!
        controller.productName = 'test';
        ApexPages.StandardSetController setController = controller.setController;

        List<SCProductModel__c> foundProducts = controller.getFoundProducts();
    }        

    static testMethod void testComplete()
    {
        SCHelperTestClass.createProductModel(true);
        
        User runningUser = [Select AssignedCountries__c From User where Id = :UserInfo.getUserId()];
        runningUser.AssignedCountries__c = 'NL;DE';
        runningUser.Workcenter__c = 'Test';
        update runningUser;
        
        SCProductLookupController controller = new SCProductLookupController();
        
        //TODO create products first!!
        controller.selectedBrand = SCHelperTestClass.brand.Name;
        controller.searchProduct.UnitClass__c = SCHelperTestClass.prodModel.UnitClass__c;
        controller.searchProduct.UnitType__c = SCHelperTestClass.prodModel.UnitType__c;
        controller.searchProduct.Power__c = SCHelperTestClass.prodModel.Power__c;
        ApexPages.StandardSetController setController = controller.setController;

        List<SCProductModel__c> foundProducts = controller.getFoundProducts();
    }        

    static testMethod void testAdditional() {
        SCProductLookupController controller = new SCProductLookupController();
        
        controller.selectedBrand = '001';
        System.AssertEquals(controller.selectedBrand, '001');
    }
}
