/*
* @(#)SCDynamicTableControllerTest.cls
* 
* Copyright 2014 by GMS Development GmbH, Karl-Schurz-Strasse 29, 
* DE-33100 Paderborn. All rights reserved.
* 
* Test implementation of the main class.
* 
* @history 
* 2014-09-15 GMSSU created
* 
* @review 
* 
*/
@isTest
private class SCDynamicTableControllerTest {
    
    private static testmethod void mainTestsObjects()
    {
        // Creating an object
        Object mainObject;
        Account a = new Account();
        a.Name = 'My test acc 1';
        insert a;
        List<Account> accs = new List<Account>();
        accs.add(a);
        mainObject = accs;
        
        // Creating a button
        Object buttonsMapObject;
        List<Map<String,String>> buttonsList = new List<Map<String,String>>();
        Map<String,String> buttonValues = new Map<String,String>();
        buttonValues.put('value','Button 1');
        buttonValues.put('onclick','alert(\'It works;\'); return false;');
        buttonsList.add(buttonValues);
        buttonsMapObject = buttonsList;
        
        Test.startTest();
        
        SCDynamicTableController dt = new SCDynamicTableController();
        
        dt.dataRaw = mainObject;
        dt.customButtons = buttonsMapObject;
        
        Test.stopTest();
    }
}
