/*
 * @(#)CCWCYourSLResponseExample1.cls 
 * 
 * Copyright 2014 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 */
 
/**
 * ===============================================================================================================================
 * Note: Please do not change this class. It is the template for YourSL team, that ####-> can be copied and then the copy can be changed.
 * ===============================================================================================================================
 * This is an example class.
 *
 * The class for updating the ClockPort object using the response data from SCWSGenericResponse class.
 * This class is called from the GenericResponseHookExample and used by GenericResponseHookExampleTest class.
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */

global without sharing class CCWCYourSLResponseExample1 
{
	/**
	 * Example method for working out of the generic response input structure
	 * 
	 * - logs the response data in the interface log 
	 * - finds the Clock Port object that is to be updated using the response data
	 * - finds the request interface log
	 * - updates the Clock Port object
	 * - join the response interface log with the request interface log
	 *
	 * @param messageID
	 * @param requestMessageID
	 * @param headExternalID
	 * @param referenceItem
	 * @param MaximumLogItemSeverityCode
	 * @param logItemArr
	 * @param interfaceLogList
	 * @param GenericServiceResponseMessage
	 */
    public static void process(String messageID, String requestMessageID, String headExternalID, CCWSGenericResponse.ReferenceItem referenceItem, 
                                                  String MaximumLogItemSeverityCode, CCWSGenericResponse.LogItemClass[] logItemArr, 
                                                  List<SCInterfaceLog__c> interfaceLogList,
                                                  CCWSGenericResponse.GenericServiceResponseMessageClass GenericServiceResponseMessage,
                                                  SCInterfaceLog__c responseInterfaceLog)
    {
        String interfaceName = 'YourSL-InterfaceName';
        String interfaceHandler = 'YourSL-InterfaceClass';
        String type = 'INBOUND';
        String direction = 'inbound';
        ID referenceID = null;
        String refType = null;
        ID referenceID2 = null;
        String refType2 = '';
        ID responseID = null;
        String resultCode = 'E000';
        String resultInfo = 'Success'; 
        
        ///////////////////////////////////////////////////
        // log the response structure in the interface log
        ///////////////////////////////////////////////////
        String jsonInput = JSON.serialize(GenericServiceResponseMessage);
        SCInterfaceBase ib = new SCInterfaceBase();
        String fromJSONMap = ib.getDataFromJSON(jsonInput);
        debug('from json: ' + fromJSONMap);
        
        
        String data = 'headExternalID: ' + headExternalID + ',\n\nreferenceItem: ' + referenceItem + ',\n\nMaximumLogItemSeverityCode: ' + MaximumLogItemSeverityCode
                    + '\n allResponse: ' + fromJSONMap;
        debug('data: ' + data);            
		// Fill interface log response created by a pivot web service
        responseInterfaceLog.Interface__c = interfaceName;
        responseInterfaceLog.InterfaceHandler__c = interfaceHandler;
		responseInterfaceLog.Direction__c = direction;            
		responseInterfaceLog.MessageID__c = messageID;            
		responseInterfaceLog.ReferenceID__c = referenceID;            
		responseInterfaceLog.ResultCode__c = resultCode;            
		responseInterfaceLog.ResultInfo__c = resultInfo;
		responseInterfaceLog.Data__c = data;
		debug('interfaceLogResponse2: ' + responseInterfaceLog);
        responseInterfaceLog.Data__c = responseInterfaceLog.Data__c.left(32000);
        
        String step = '';
        Savepoint sp = Database.setSavepoint();
        try
        {
            // find the order
            step = 'find an yourSL object (in this example an order)'; 
			ID yourSLObjId; // to be read
            SCOrder__c order = readOrder(referenceItem.ExternalID, responseInterfaceLog);
            if(order != null)
            {
            	yourSLObjId = order.id;
            }	
            //debug('order: ' + order);
			//responseInterfaceLog.ReferenceID__c = order.ID;            

            // find a request interfacelog
            step = 'find a request interface';
            //GMSGB 03.09.13 It is possible that the SAP response is faster than the commit of the call
            // Thus it is possible that the interface does not exist.
            
            SCInterfaceLog__c requestInterfaceLog = CCWSGenericResponse.readOutoingInterfaceLog(requestMessageID, yourSLObjId, referenceItem.ExternalID, interfaceName);
            debug('requestInterfaceLog: ' + requestInterfaceLog);
            
            // update order
            step = 'update the yourSL object';

            order.ERPOrderNo__c = referenceItem.ReferenceID;
            order.ERPStatusOrderCreate__c = CCWSGenericResponse.getResultStatus(logItemArr);
            order.ERPResultDate__c = DateTime.now();
            order.IDocOrderCreate__c = responseInterfaceLog.Idoc__c;
            update order;
            debug('order after update: ' + order);
            step = 'write a response interface log';
            // write response interface log
			responseInterfaceLog.Count__c = 1;           
						
            debug('responseInterfaceLog: ' + responseInterfaceLog);
            // update request interface log
            step = 'join the request interface log with the response interface log';
            if(responseInterfaceLog != null)
            {
                //GMSGB 03.09.13 It is possible that the SAP response is faster than the commit of the call
                // Thus it is possible that the interface does not exist.            	
            	if(requestInterfaceLog != null)
            	{
	            	debug('join the request interface log with the response interface log');
	                requestInterfaceLog.Response__c = responseInterfaceLog.Id;
	                
	                debug('requestInterfaceLog: ' + requestInterfaceLog);
	                
	                interfaceLogList.add(requestInterfaceLog);
            	}
                
                debug('interfaceLogList.size: ' + interfaceLogList.size());
                debug('interfaceLogList: ' + interfaceLogList);
	            for(SCInterfaceLog__c il: interfaceLogList)
	            {
	            	debug('*ID: ' + il.id + ', MessageID: ' + il.MessageID__c + ', Order__c: ' + il.Order__c
	            	+ ', Response: ' + il.Response__c);
	            } 
            }
            else
            {
                throw new SCfwException('A response interface log object could not be created for messageID: ' + messageID + 
                                        ', yourSL object id: ' + yourSLObjId + ', order name: ' + headExternalID); 
            }    
        }
        catch(SCfwInterfaceRequestPendingException errorRequestNotPending)
        {
        	Database.rollback(sp);
        	throw errorRequestNotPending;
        }
        catch(SCfwException e) 
        {
            Database.rollback(sp);
            throw e;
        }  
        catch(Exception e) 
        {
            Database.rollback(sp);
            throw e;
        } 
        
        finally
        {
        }  
        
    }//process

    public static SCOrder__c readOrder(String headExternalID, SCInterfaceLog__c responseInterfaceLog)
    {
        SCOrder__c retValue = null;
        List<SCOrder__c> ol = [select ID, name, ERPStatusOrderCreate__c from SCOrder__c where name = : headExternalID];
        
        if(ol.size() > 0)
        {
        	if(ol[0].ERPStatusOrderCreate__c == 'error' || ol[0].ERPStatusOrderCreate__c == 'pending' )
        	{
	            retValue = ol[0];
	            debug('read order: ' + ol[0]);
        	}
        	else if (ol[0].ERPStatusOrderCreate__c == 'none' )
        	{
        		SCfwInterfaceRequestPendingException e = new SCfwInterfaceRequestPendingException('The order with name: ' + headExternalId + ' has ERPStatusOrderCreate__c  \'none\'.');
        		e.order = ol[0];
        		throw e;
        	}
        	else if (ol[0].ERPStatusOrderCreate__c == 'ok' )
        	{
        		//SCfwInterfaceRequestPendingException e = new SCfwInterfaceRequestPendingException('The order with name: ' + headExternalId + ' has ERPStatusOrderCreate__c  \'ok\'.');
        		//e.order = ol[0];       	
        		//throw e;
        		retValue = ol[0];
	            debug('The order with name: ' + headExternalId + ' has ERPStatusOrderCreate__c  \'ok\'.');
        	}
        	else
        	{
        		SCfwInterfaceRequestPendingException e = new SCfwInterfaceRequestPendingException('The order with name: ' + headExternalId + ' has ERPStatusOrderCreate__c  '
        		+ ol[0].ERPStatusOrderCreate__c +'.');
        		e.order = ol[0];       	
        		throw e;
        	}
        }
        else
        {
        	throw new SCfwException('The order with name: ' + headExternalId + ' has been not found in Salesforce.');
        }
        
        return retValue;
    }

    public static void debug(String msg)
    {
        System.debug('###...' +  msg);        
    }

}
