@isTest
private class CCWCPDFCreatorTest {
    
	static testMethod void testInitRequest() {
		CCWCPDFCreator.initHttpRequest(UserInfo.getSessionId());
		System.assert(CCWCPDFCreator.request != null);
		System.assertEquals(CCWCPDFCreator.request.getMethod(), 'GET');
		System.assertEquals(CCWCPDFCreator.request.getHeader('Authorization'), 'OAuth ' + UserInfo.getSessionId());
	} 
	
	static testMethod void testGetEndpointForContract() {
		Request__c request = new Request__c();
		insert request;
		
		RequestAttachment__c requAtt = new RequestAttachment__c();
		requAtt.Request__c = request.Id;
		insert requAtt;
		
		Attachment att = new Attachment();
		att.Name = 'Signature CoolerContract';
		att.Body = Blob.valueOf('Some Attachment Body');
		att.ParentId = requAtt.Id;
		insert att;
		
		String endpointUrl = CCWCPDFCreator.getEndpointForContract(att);
		System.assert(endpointUrl.endsWith('srLeihvertrag_Cooler_Muster_hdm_090714?'+CoolerContractController.ORDER_PARAMETER_LABEL+'=' + requAtt.Id + '&'+CoolerContractController.ATTACHMENT_PARAMETER_LABEL+'=' + att.Id),endpointUrl);
	}
    
    static testMethod void testCreatingSalesOrderSummeries_sendGeneratedPDFasFax(){
        User u = new User();
        u.FirstName ='First1';
        u.LastName = 'Tester';
        u.Alias = 'mtst';
        u.Email = 'mtester1@company.com';
        u.Username = u.Email;
        u.CommunityNickname = u.Alias;
        u.ProfileId = [select Id from Profile
                       where Name = 'System Administrator' OR Name = 'Systemadministrator'][0].Id;
        u.TimeZoneSidKey = 'America/Chicago';
        u.LocaleSidKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        u.LanguageLocaleKey = 'en_US';
        
        insert u;
        
        Account acc = new Account();
        acc.Name = 'Test Acc';
        acc.GFGHPreferredOrderTransactionMethod__c = 'fax';
        insert acc;
                        
        User runningUser = u;

        //run test class as System Administrator        
        System.runAs(runningUser){  
            Test.setMock(HttpCalloutMock.class, new CCWCPDFCreator_CalloutMockupTest());		
            CCWCPDFCreator.createSalesOrderSummariesAsyncCallout(new Set<ID>{acc.ID}, UserInfo.getSessionId());
        }
    }
	static testMethod void testCreatingSalesOrderSummeries_sendGeneratedPDFasEmail(){
        User u = new User();
        u.FirstName ='First1';
        u.LastName = 'Tester';
        u.Alias = 'mtst';
        u.Email = 'mtester1@company.com';
        u.Username = u.Email;
        u.CommunityNickname = u.Alias;
        u.ProfileId = [select Id from Profile
                       where Name = 'System Administrator' OR Name = 'Systemadministrator'][0].Id;
        u.TimeZoneSidKey = 'America/Chicago';
        u.LocaleSidKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        u.LanguageLocaleKey = 'en_US';
        
        insert u;
        
        Account acc = new Account();
        acc.Name = 'Test Acc';
        acc.Email__c = 'random@email.com';
        acc.GFGHPreferredOrderTransactionMethod__c = 'email';
        insert acc;
                        
        User runningUser = u;

        //run test class as System Administrator        
        System.runAs(runningUser){  
            Test.setMock(HttpCalloutMock.class, new CCWCPDFCreator_CalloutMockupTest());		
            CCWCPDFCreator.createSalesOrderSummariesAsyncCallout(new Set<ID>{acc.ID}, UserInfo.getSessionId());
        }
    }
    static testMethod void test_CCWCPDFCreatorHandler_createSalesOrderSummariesCallout(){
        User u = new User();
        u.FirstName ='First1';
        u.LastName = 'Tester';
        u.Alias = 'mtst';
        u.Email = 'mtester1@company.com';
        u.Username = u.Email;
        u.CommunityNickname = u.Alias;
        u.ProfileId = [select Id from Profile
                       where Name = 'System Administrator' OR Name = 'Systemadministrator'][0].Id;
        u.TimeZoneSidKey = 'America/Chicago';
        u.LocaleSidKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        u.LanguageLocaleKey = 'en_US';
        
        insert u;
        
        Account acc = new Account();
        acc.Name = 'Test Acc';
        acc.Email__c = 'random@email.com';
        acc.GFGHPreferredOrderTransactionMethod__c = 'email';
        insert acc;
                        
        User runningUser = u;

        //run test class as System Administrator        
        System.runAs(runningUser){  
            Test.setMock(HttpCalloutMock.class, new CCWCPDFCreator_CalloutMockupTest());		
            CCWCPDFCreatorHandler handler = new CCWCPDFCreatorHandler();
            handler.createSalesOrderSummariesCallout(new Set<ID>{acc.ID});
        }
    }
	static testMethod void testCreateCoolerContract() {
		
		//Test.startTest();
		Test.setMock(HttpCalloutMock.class, new CCWCPDFCreator_CalloutMockupTest());
        
		Request__c request = new Request__c();
		insert request;
		
		SCProductModel__c productModel = new SCProductModel__c();
		insert productModel;
		
		CdeOrder__c cdeOrder = new CdeOrder__c();
		cdeOrder.Request__c = request.Id;
		insert cdeOrder;
		
		CdeOrderItem__c cdeOrderItem = new CdeOrderItem__c();
		cdeOrderItem.CdeOrder__c = cdeOrder.Id;
		cdeOrderItem.ProductModel__c = productModel.Id;
		cdeOrderItem.Quantity__c = 5;
		insert cdeOrderItem;
		
		Test.startTest();
		Attachment att = new Attachment();
		att.Name = 'Signature CoolerContract';
		att.Body = Blob.valueOf('Some Attachment Body');
		att.ParentId = cdeOrder.Id;
		insert att;
		
		List<Attachment> attachments = new List<Attachment>();
		attachments.add(att);
		CCWCPDFCreatorHandler handler = new CCWCPDFCreatorHandler();
		handler.createContractsCallout(attachments);
		//Test.stopTest();
		attachments = [SELECT Id, Name, ParentId FROM Attachment WHERE ParentId =: cdeOrder.Id];
		System.assertEquals(1, attachments.size());
		
		Test.stopTest();
		
	}
	
}
