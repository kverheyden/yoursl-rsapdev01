@isTest
private class UserPermissionAssignmentsTest {

	static final String SALESAPP_PERMISSIONSET_NAME = 'SalesAppPermissions';
	
	static final String SALESAPP_GROUP_NAME = 'SalesAppUser';
	static final String SALESAPP_PERMISSIONSET_GENERIC = 'SalesAppUserGenericPermissions';
	static final String SALESAPP_USERTYPE = 'Sales Representative';
	
	static final String MANAGERAPP_GROUP_NAME = 'ManagerAppUser';
	static final String MANAGERAPP_USERTYPE = 'Manager';
	
	static Profile testProfile = [SELECT Id FROM profile WHERE Name IN ('System Administrator', 'Systemadministrator')  LIMIT 1];
	static User testUser;
	static Set<Id> testUserIds = new Set<Id>();
	static Group managerAppGroup;
	static Group salesAppGroup;
	
	@isTest
	static void testPermissionSets() {
		List<PermissionSet> psList = [SELECT Id FROM PermissionSet WHERE NAME IN 
			('ManagerAppSpecificPermissions', 'SalesAppSpecificPermissions', 'SalesAppUserGenericPermissions')];
		System.assertEquals(3, psList.size());
	}

	@isTest 
	static void testUserAfterInsertTrigger() {
		insertUser();
		System.runAs(testUser) {
			setupTest();
			insertTestUsers();
			List<GroupMember> managerAppGroupMembers = [SELECT Id, Group.DeveloperName, UserOrGroupId, GroupId FROM GroupMember 
				WHERE UserOrGroupId IN: testUserIds AND Group.DeveloperName =: MANAGERAPP_GROUP_NAME];

			List<GroupMember> salesAppGroupMembers = [SELECT Id, Group.DeveloperName, UserOrGroupId, GroupId FROM GroupMember 
				WHERE UserOrGroupId IN: testUserIds AND Group.DeveloperName =: SALESAPP_GROUP_NAME];
 
			System.assertEquals(3, managerAppGroupMembers.size());
			System.assertEquals(2, salesAppGroupMembers.size());

			List<PermissionSetAssignment> genericPermissionSetAssignments = [SELECT Id FROM PermissionSetAssignment 
				WHERE AssigneeId IN: testUserIds AND PermissionSet.Name IN 
				('SalesAppUserGenericPermissions')];
			System.assertEquals(5, genericPermissionSetAssignments.size());
			
			List<PermissionSetAssignment> salesAppSpecificPermissionSetAssignments = 
				[SELECT Id FROM PermissionSetAssignment 
					WHERE AssigneeId IN: testUserIds AND PermissionSet.Name IN 
					('SalesAppSpecificPermissions')];
			System.assertEquals(2, salesAppSpecificPermissionSetAssignments.size());

			List<PermissionSetAssignment> managerAppSpecificPermissionSetAssignments = 
				[SELECT Id FROM PermissionSetAssignment 
					WHERE AssigneeId IN: testUserIds AND PermissionSet.Name IN 
					('ManagerAppSpecificPermissions')];
			System.assertEquals(3, managerAppSpecificPermissionSetAssignments.size());
			
		}		
	}
	
	@isTest 
	static void testUserAfterUpdateTrigger() {
		insertUser();
		System.runAs(testUser) {
			setupTest();
			insertTestUsers();

			Map<Id, User> userMap = new Map<Id, User>([SELECT Id, SalesAppUserType__c FROM User WHERE Id IN: testUserIds]);
			System.assertEquals(6, userMap.size());

			for (User u : userMap.values())
				u.SalesAppUserType__c = SALESAPP_USERTYPE;

			update userMap.values();
			userMap	= new Map<Id, User>([SELECT Id, SalesAppUserType__c FROM User WHERE Id IN: testUserIds AND SalesAppUserType__c =: SALESAPP_USERTYPE]);
			System.assertEquals(6, userMap.size());	

			List<GroupMember> salesAppGroupMembers = [SELECT Id, Group.DeveloperName, UserOrGroupId, GroupId FROM GroupMember 
				WHERE Group.DeveloperName =: SALESAPP_GROUP_NAME AND UserOrGroupId IN: testUserIds];
			System.assertEquals(6, salesAppGroupMembers.size());

			for (User u : userMap.values())
				u.SalesAppUserType__c = '';

			update userMap.values();
			salesAppGroupMembers = [SELECT Id, Group.DeveloperName, UserOrGroupId, GroupId FROM GroupMember 
				WHERE Group.DeveloperName =: SALESAPP_GROUP_NAME AND UserOrGroupId IN: testUserIds];

			System.assertEquals(0, salesAppGroupMembers.size());

		}
	}
	
	static void insertTestUsers() {
		insertUser('manager@user.de', MANAGERAPP_USERTYPE);
		insertUser('manager1@user.de', MANAGERAPP_USERTYPE);
		insertUser('manager2@user.de', MANAGERAPP_USERTYPE);

		insertUser('sales@user.de', SALESAPP_USERTYPE);
		insertUser('sales1@user.de', SALESAPP_USERTYPE);
		insertUser('something@user.de', 'SALESAPP_USERTYPE');
	}

	static void insertUser(String userName, String userType) {
		User u = new User();
		u.SalesAppUserType__c = userType;
        u.LastName = userType;
        u.Alias = 'tu';
        u.Email = userName;
        u.Username = userName;
        u.TimeZoneSidKey = 'Europe/Berlin';
        u.LocaleSidKey = 'de_DE_EURO';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.ProfileId = testProfile.Id;
        u.LanguageLocaleKey = 'de';
        insert u;
        testUserIds.add(u.Id);
	}

	@isTest
	static void testAssignUserToPublicGroup() {
		insertUser();
		System.runAs(testUser) {
			setupTest();
			List<GroupMember> managerAppGroupMembers = [SELECT Id, UserOrGroupId, GroupId FROM GroupMember 
				WHERE GroupId =: managerAppGroup.Id];
			System.assertEquals(0, managerAppGroupMembers.size());

			Map<Id, User> userMap = new Map<Id, User>();
			userMap.put(testUser.Id, testUser);
			System.assert(userMap.size() == 1);
			UserPermissionAssignments.assignUserToPublicGroup(userMap, MANAGERAPP_GROUP_NAME);
			UserPermissionAssignments.assignUserToPublicGroup(userMap, MANAGERAPP_GROUP_NAME);
			UserPermissionAssignments.assignUserToPublicGroup(null, MANAGERAPP_GROUP_NAME);
			UserPermissionAssignments.assignUserToPublicGroup(userMap, 'NotExistingGroup');
			UserPermissionAssignments.assignUserToPublicGroup(userMap, '');

			managerAppGroupMembers = [SELECT Id, UserOrGroupId, GroupId FROM GroupMember 
				WHERE UserOrGroupId =: testUser.Id AND Group.DeveloperName =: MANAGERAPP_GROUP_NAME];
			
			System.assertEquals(1, managerAppGroupMembers.size());
		}
	}
	
	@isTest
	static void testRemoveUserFromPublicGroup() {
		insertUser();
		System.runAs(testUser) {
			setupTest();
			Map<Id, User> userMap = new Map<Id, User>();
			userMap.put(testUser.Id, testUser);
			
			UserPermissionAssignments.assignUserToPublicGroup(userMap, MANAGERAPP_GROUP_NAME);
			List<GroupMember> managerAppGroupMembers = [SELECT Id, UserOrGroupId, GroupId FROM GroupMember 
				WHERE UserOrGroupId =: testUser.Id AND Group.DeveloperName =: MANAGERAPP_GROUP_NAME];
			System.assertEquals(1, managerAppGroupMembers.size());


			UserPermissionAssignments.removeUserFromPublicGroup(userMap, MANAGERAPP_GROUP_NAME);
			UserPermissionAssignments.removeUserFromPublicGroup(userMap, MANAGERAPP_GROUP_NAME);
			UserPermissionAssignments.removeUserFromPublicGroup(null, MANAGERAPP_GROUP_NAME);
			UserPermissionAssignments.removeUserFromPublicGroup(userMap, 'SomeGroup');
			UserPermissionAssignments.removeUserFromPublicGroup(userMap, null);

			ManagerAppGroupMembers = [SELECT Id, UserOrGroupId, GroupId FROM GroupMember 
				WHERE UserOrGroupId =: testUser.Id AND Group.DeveloperName =: MANAGERAPP_GROUP_NAME];
			System.assertEquals(0, managerAppGroupMembers.size());
		}
	}
	
	@isTest
	static void testAssignPermissionSetByName() {
		insertUser();
		System.runAs(testUser) {
			setupTest();
			List<PermissionSetAssignment> psa = [SELECT Id FROM PermissionSetAssignment 
				WHERE AssigneeId =: testUser.Id AND PermissionSetId =: SalesAppSettings__c.getInstance('PermissionSetId').Value__c];
			System.assertEquals(0, psa.size());

			Map<Id, User> userMap = new Map<Id, User>();
			userMap.put(testUser.Id, testUser);

			UserPermissionAssignments.assignPermissionSet(userMap, SALESAPP_PERMISSIONSET_GENERIC);
			UserPermissionAssignments.assignPermissionSet(userMap, SALESAPP_PERMISSIONSET_GENERIC);

			psa = [SELECT Id FROM PermissionSetAssignment 
				WHERE AssigneeId =: testUser.Id AND PermissionSet.Name =: SALESAPP_PERMISSIONSET_GENERIC];
			System.assertEquals(1, psa.size());
		}
	}

	@isTest
	static void testDessignPermissionSetByName() {
		insertUser();
		System.runAs(testUser) {
			setupTest();
			Map<Id, User> userMap = new Map<Id, User>{testUser.id => testUser};
			UserPermissionAssignments.assignPermissionSet(userMap, SALESAPP_PERMISSIONSET_GENERIC);
			List<PermissionSetAssignment> psa = [SELECT Id FROM PermissionSetAssignment 
				WHERE AssigneeId =: testUser.Id AND PermissionSet.Name =: SALESAPP_PERMISSIONSET_GENERIC];
			System.assertEquals(1, psa.size());

			UserPermissionAssignments.deassignPermissionSet(userMap, SALESAPP_PERMISSIONSET_GENERIC);
			UserPermissionAssignments.deassignPermissionSet(userMap, SALESAPP_PERMISSIONSET_GENERIC);

			psa = [SELECT Id FROM PermissionSetAssignment 
				WHERE AssigneeId =: testUser.Id AND PermissionSet.Name =: SALESAPP_PERMISSIONSET_GENERIC];
			System.assertEquals(0, psa.size());			
		}
	}

	@isTest
	static void testAssignPermissionSetByCustomSetting() {
		insertUser();
		System.runAs(testUser) {
			setupTest();
			List<PermissionSetAssignment> psa = [SELECT Id FROM PermissionSetAssignment 
				WHERE AssigneeId =: testUser.Id AND PermissionSetId =: SalesAppSettings__c.getInstance('PermissionSetId').Value__c];
			System.assertEquals(0, psa.size());

			Map<Id, User> userMap = new Map<Id, User>();
			userMap.put(testUser.Id, testUser);

			UserPermissionAssignments.assignPermissionSetByCustomSetting(userMap);
			UserPermissionAssignments.assignPermissionSetByCustomSetting(userMap);

			psa = [SELECT Id FROM PermissionSetAssignment 
				WHERE AssigneeId =: testUser.Id AND PermissionSetId =: SalesAppSettings__c.getInstance('PermissionSetId').Value__c];
			System.assertEquals(1, psa.size());
		}
	}

	@isTest 
	static void testDeassignPermissionSet() {
		insertUser();
			System.runAs(testUser) {
			setupTest();
			List<PermissionSetAssignment> psa = [SELECT Id FROM PermissionSetAssignment 
				WHERE AssigneeId =: testUser.Id AND PermissionSetId =: SalesAppSettings__c.getInstance('PermissionSetId').Value__c];
			System.assertEquals(0, psa.size());

			Map<Id, User> userMap = new Map<Id, User>();
			userMap.put(testUser.Id, testUser);

			UserPermissionAssignments.assignPermissionSetByCustomSetting(userMap);
			UserPermissionAssignments.deassignPermissionSetByCustomSetting(userMap);
			UserPermissionAssignments.deassignPermissionSetByCustomSetting(userMap);

			psa = [SELECT Id FROM PermissionSetAssignment 
				WHERE AssigneeId =: testUser.Id AND PermissionSetId =: SalesAppSettings__c.getInstance('PermissionSetId').Value__c];
			System.assertEquals(0, psa.size());
		}
	}
	
	static void setupTest() {
		insertUserGroups();
		insertCustomSettings();
	}

	static void insertUserGroups() {
		List<Group> userGroups = new List<Group>{
			new Group(Name = SALESAPP_GROUP_NAME),
			new Group(Name = MANAGERAPP_GROUP_NAME)
		};

		insert userGroups;

		salesAppGroup = [SELECT Id FROM Group WHERE Name =: SALESAPP_GROUP_NAME LIMIT 1];
		System.assert(salesAppGroup != null);

		managerAppGroup = [SELECT Id FROM Group WHERE Name =: MANAGERAPP_GROUP_NAME LIMIT 1];
		System.assert(managerAppGroup != null);
	}

	static void insertUser() {
		testUser = new User();
        testUser.LastName = 'Testuser';
        testUser.Alias = 'tu';
        testUser.Email = 'test@user.de';
        testUser.Username =  String.valueOf(Math.random()) + '@test.user.guest.cceag.de';
        testUser.TimeZoneSidKey = 'Europe/Berlin';
        testUser.LocaleSidKey = 'de_DE_EURO';
        testUser.EmailEncodingKey = 'ISO-8859-1';
        testUser.ProfileId = testProfile.Id;
        testUser.LanguageLocaleKey = 'de';
        insert testUser;
	}

	
	static void insertCustomSettings() {
		PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name =: SALESAPP_PERMISSIONSET_NAME];
		System.assert(ps != null, 'PermissionSet \'SalesAppPermissions\' does not exists');

		insert new SalesAppSettings__c(Name = 'PermissionSetId', Value__c = ps.Id);

		SalesAppSettings__c setting = SalesAppSettings__c.getInstance('PermissionSetId');
		System.assertEquals(ps.Id, setting.Value__c);
	}
}
