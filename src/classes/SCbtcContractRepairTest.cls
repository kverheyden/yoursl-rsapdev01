/*
 * @(#)SCbtcContractRepairTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCbtcContractRepairTest
{
    static testMethod void testRepairContract()
    {
        Test.StartTest();
        SCContractTestHelper cth = new SCContractTestHelper();
        String contractID = cth.prepareForBtcContractVisitCreateTest();
        System.debug('###Test contract: ' + cth.contract);
        System.debug('###ContractId: ' + contractID);
        Id id = (Id) contractID;
        SCbtcContractRepair.repairContract(id, 'XX', 1, 'trace');
        String contractName = null;
        List<SCContract__c> cList = [Select Name from SCContract__c where Id = :id];
        if(cList.size() > 0)
        {
            contractName = cList[0].Name;
            SCbtcContractRepair.repairContract(contractName, 'XX', 1, 'trace');
        }
        SCbtcContractRepair.repairAll('NL', 1, 'trace');       
        Test.StopTest();
    }
    static testMethod void testMissingFields()
    {
        SCbtcContractRepair cr = new SCbtcContractRepair('NL', 0, 'trace');
        List<SCInterfaceLog__c> interfaceLogList = new List<SCInterfaceLog__c>();
        SCContract__c c = new SCContract__c();
        cr.repairContract(c, interfaceLogList);
    }
}
