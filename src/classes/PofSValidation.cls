/**
* @author    
* Bernd Werner           
* YourSL GmbH
* b.werner@yoursl.de
*            
*
* @description  
* This class isbased on a trigger "before insert" and "before update". 
* It ensures the validity of several data for the Picture of success. 
* 1) Enddate not < than today()
* 2) End date not less than Start date
* 3) Qurey all other PofS to ensure, that for the same subtrade channel not PofS has valid time range overlap with the existing PofS.
*
*
* @date 24.09.2013
*
* Timeline:
* Name				DateTime				Version			Description
* Bernd Werner		24.09.2013				1.0				Creation of this class
*
*
*	
*/

public with sharing class PofSValidation {
	
	// Main Method 
	public void validateData(List<PictureOfSuccess__c> newList, String mode){
		// initialize Set to hold all subtradechannels
		Set<String> subtrade = new Set<String>();
		system.debug('####TriggerMap'+newList);
		// run thru all pofs and do validation
		for(PictureOfSuccess__c pofs : newList){
			// Check, if End date less than start date
			if(pofs.ValidUntil__c < pofs.ValidFrom__c){
				pofs.ValidUntil__c.addError(Label.fsfa_pofs_start_end_colision); 
			}
			List<String> tmpList = new List<string>();
			if (pofs.Subtradechannel__c != null)
				tmpList = pofs.Subtradechannel__c.split(';',0);
			// Loop thru all elements and add to the subtrade list if not already contained
			for (String sub : tmpList){
				if (!subtrade.contains(sub)){
					subtrade.add(sub);
				}
			}
		}
		// Here we have a set with all possible subtrade channels and creating a string from it:
		String slist = '';
		for (String s: subtrade) {
			slist += '\'' + s + '\',';
		}
		if (slist != '')
			slist = slist.substring (0,slist.length() -1);
		
		// Do a query to find all relevant PofS to check, if there is a time frame conflict
		String squery = 'SELECT Id, Name, ValidFrom__c, ValidUntil__c, Subtradechannel__c FROM PictureOfSuccess__c WHERE ( Subtradechannel__c includes ('+slist+') ) AND ( Active__c = true )';
		PictureOfSuccess__c [] relevantPofS;
		try {
			relevantPofS = database.query(squery);
		} catch (QueryException e) {
			relevantPofS = new List<PictureOfSuccess__c>();
			System.debug('QueryException: ' + e.getMessage());
		}
		System.debug('####Relevant PofS:'+relevantPofS);
		// Go thru the result and check, if we find a conflict of the tiime range of any of the given PofSs
		for (PictureOfSuccess__c xpofs : relevantPofS){
			List<String> xsub = new List<String>();
			// Generate a list with all subtradechannels of the given PofS
			xsub = xpofs.Subtradechannel__c.split(';',0);
			// Put the values to a new set to make the use of "contains" available
			Set<String> xsubset = new Set<String>();
			for (String tmp_x_sub : xsub){
				xsubset.add(tmp_x_sub);
			}
			// Step thru all new PofS and check agains the actual PofS if a conflic occours
			for (PictureOfSuccess__c triggerpofs : newList){
				// If we are in update mode, skip the comparrison of the same record
				if(mode=='update' && triggerpofs.Name==xpofs.Name){
					continue;
				}
				Boolean error = false;
				// first check the timeframe
				if (xpofs.ValidFrom__c<= triggerpofs.ValidUntil__c && xpofs.ValidUntil__c>=triggerpofs.ValidFrom__c){
					// The time frame matches. Second, check if one of the subtradechannels is also matching
					for(String tmp_checkSub : triggerpofs.Subtradechannel__c.split(';',0)){
						if(xsubset.contains(tmp_checkSub)){
							triggerpofs.Subtradechannel__c.addError(Label.fsfa_pofs_date_colision_pre+' '+xpofs.Name+' '+Label.fsfa_pofs_date_colision_post);
							error = true;
							break;
						}
					}
				}
			}
			
		}
		
		
	}
	
}
