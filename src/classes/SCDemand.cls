/*
 * @(#)SCDemand.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Holds the order and order item data used to schedule an engineer
 * @author H. Schroeder <hschroeder@gms-online.de>
 * @version $Revision$, $Date$
 */
global class SCDemand
{
    // skill_obj
    
    // demand
    public String orderID { get; set; }
    public Integer ordtyp { get; set; }
    public String ordno_int { get; set; }
    public String custprio { get; set; }
    public DateTime cwstart { get; set; }
    public DateTime cwend { get; set; }
    public Integer cwtyp { get; set; }
    public String name { get; set; }
    public Double x { get; set; }
    public Double y { get; set; }
    public String zipcode { get; set; }
    public String street { get; set; }
    public String city { get; set; }
    public String subcity { get; set; }
    public String houseno { get; set; }
    public String addr_extension { get; set; }
    public String employee_ik_cw { get; set; }
    public String employee_ik_ncw { get; set; }
    public String employee_ik_req { get; set; }
    public Integer appt_index { get; set; }
    public Integer failuretyp { get; set; }
    public String deptCurrent { get; set; }
    public String deptResp { get; set; }
    public Boolean standby {get; set; }
    public Integer addEmployee {get; set; }
    public Boolean fixflg {get; set;}    
    public Boolean emplfixflg {get; set;}    
    public String preferedemployees {get; set;}
    public String phones {get; set;}
    public String resourceStandbys {get; set;}
    public String brand {get; set;}
    
    // service item
    public String itemID { get; set; }
    public Decimal esttime { get; set; }
    public Integer esttimeintervall { get; set; }   
    public Integer unitclass { get; set; }
    public Integer unittyp { get; set; }
    public Integer productgroup { get; set; }
    public String productpower { get; set; }
    public Integer productenergy { get; set; }
    public String errortext { get; set; }
    public String infotext { get; set; }  
 }
