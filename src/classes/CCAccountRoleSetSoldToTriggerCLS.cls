/**
 * @created 2014-10-08T15:45:00+02:00
 * @author "thomas richter" <thomas@meformobile.com>
 */
public class CCAccountRoleSetSoldToTriggerCLS 
{
    public void updateIsSoldToFlag(CCAccountRole__c[] roles)
    {
        List<Id> accountIdSetIsSoldTo = new List<Id>();
        List<Id> accountIdRemoveIsSoldTo = new List<Id>();
        for (CCAccountRole__c role : roles) {
            
           	// yes. the following conditions can be written in one line with || - but we want test to test every single condition!!!            
            if (role.AccountRole__c!='AG') {
                continue;
           	}
            
            if (role.MasterAccount__c!=role.SlaveAccount__c) {
                // we ignore such cases - should not happen
                continue;
            }
            
            if (role.Status__c!='Active') {
                accountIdRemoveIsSoldTo.add(role.MasterAccount__c);
                continue;
            }
            accountIdSetIsSoldTo.add(role.MasterAccount__c);
        }
        List<Account> accountToSet = [SELECT Id, IsSoldTo__c FROM Account WHERE Id IN :accountIdSetIsSoldTo];
        List<Account> accountToUnset = [SELECT Id, IsSoldTo__c FROM Account WHERE Id IN :accountIdRemoveIsSoldTo];

        List<Account> accounts = new List<Account>();
        for (Account account: accountToSet) {
            account.IsSoldTo__c = true;
            accounts.add(account);
        }
        for (Account account: accountToUnset) {
            account.IsSoldTo__c = false;
            accounts.add(account);
        }

		update accounts;
    }
    
    public void removeIsSoldToFlag(CCAccountRole__c[] roles)
    {
        List<Id> accountIds = new List<Id>();
        for (CCAccountRole__c role : roles) {
           	// yes. the following conditions can be written in one line with || - but we want test to test every single condition!!!
            if (role.AccountRole__c!='AG') {
                continue;
            }
            if (role.MasterAccount__c!=role.SlaveAccount__c) {
                continue;
            } 
            if (role.Status__c!='Active') {
                continue;
            }
            
            accountIds.add(role.MasterAccount__c);
        }
        List<Account> accounts = [SELECT Id, IsSoldTo__c FROM Account WHERE Id IN :accountIds];
        for (Account account : accounts) {
            account.IsSoldTo__c = false;
        }
        update accounts;
    }
    
    public void updateSoldToReferences(CCAccountRole__c[] roles)
    {       
        List<Id> accountIds = new List<Id>();
        for (CCAccountRole__c role : roles) {
            
           	// yes. the following conditions can be written in one line with || - but we want test to test every single condition!!!            
            if (role.AccountRole__c!='WE') {
                continue;
           	}
            
            if (role.MasterAccount__c==role.SlaveAccount__c) {
                // we ignore such cases - this happens many times, but 
                // self referencing Account.SoldToAccount__c=Account is prohibited!
                continue;
            }
            
            if (role.Status__c!='Active') {
                accountIds.add(role.SlaveAccount__c);
                continue;
            }
            accountIds.add(role.SlaveAccount__c);
        }
        Map<Id,Account> accounts = new Map<Id,Account>([SELECT Id, SoldToAccount__c FROM Account WHERE Id IN :accountIds]);

        for (CCAccountRole__c role : roles) {            
            if (role.AccountRole__c!='WE' || role.MasterAccount__c==role.SlaveAccount__c) continue;

            Account account = accounts.get(role.SlaveAccount__c);
            if (role.Status__c!='Active') {
                // remove reference
                account.SoldToAccount__c = null;
            }else{
                // set reference
				account.SoldToAccount__c = role.MasterAccount__c;
            }
        }
		update accounts.values();
    }
    
    public void removeSoldToReferences(CCAccountRole__c[] roles)
    {
        List<ID> accountIds = new List<ID>();
        for (CCAccountRole__c role : roles) {
            if (role.AccountRole__c=='WE' && role.Status__c=='Active' && role.MasterAccount__c!=role.SlaveAccount__c) {
                accountIds.add(role.SlaveAccount__c);
            } 
        }
        List<Account> accounts = [SELECT Id, SoldToAccount__c FROM Account WHERE Id IN :accountIds];
        for (Account account : accounts) {
            account.SoldToAccount__c = null;
        }
        update accounts;
    }
}
