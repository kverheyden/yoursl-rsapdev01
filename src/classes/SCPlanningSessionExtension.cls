/*
 * @(#)SCPlanningExtension.cls SCCloud    hs 27.09.2010
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author HS <hschroeder@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCPlanningSessionExtension 
{
    private Boolean testMode = false;

    public List<SCAppointment__c> pendingAppointments { get; set; }
    public SCOrderRole__c roleRE {get; set;}
    
    //PMS 33906/W2: Erweiterungen - ZC16 - Austausch
    public Boolean isValidZC16
    {
    	get
    	{
    		if(isValidZC16 == null)
    		{
    			isValidZC16 = false;
    		}
    		return isValidZC16;
    	}
    	private set;
    }	
    
    
    private static SCApplicationSettings__c applicationSettings = SCApplicationSettings__c.getInstance(); 
    
    /**
     * Context meta data
     */
    private String[] metaContext = new String[]
    {
        'id',
        'userid',
        'rights'
    };
    
    /**
     * Constructor as controller
     */
    public SCPlanningSessionExtension() 
    {
    }
    public SCPlanningSessionExtension(Boolean testMode) 
    {
        this.testMode = testMode;
    }

    
    public SCOrderItem__c orditem {get; set;}
    
    /**
     * Constructor as extension
     */
    public SCPlanningSessionExtension(ApexPages.StandardController controller) 
    {
        init(controller);
    }
    public SCPlanningSessionExtension(ApexPages.StandardController controller, Boolean testMode) 
    {
        this.testMode = testMode;
        init(controller);
    }
    
    private void init(ApexPages.StandardController controller)
    {
        String contrId = controller.getId();
        System.debug('###na' + contrId);
        List<SCOrderItem__c> items = [select id, order__c, RecordType.DeveloperName, order__r.type__c, order__r.status__c, order__r.InvoicingReleased__c, 
                                      order__r.departmentcurrent__c, order__r.cce_ExternalOperationCount__c, cce_canDispatch__c
                                        from SCOrderItem__c where id = :contrId or order__c = :contrId order by name desc];
        System.debug('###na' + items);

        if(items != null && items.size() > 0)
        {
            // use the first order item 
            orditem = items[0];
            if(orditem.id == contrId)
            {
                // order item context (we use the standard controller)
                SCDemandController demandController = new SCDemandController(controller, sessionID, testMode);
            }
            else
            {
                // order context - we create a new order item controller (first order item)
                ApexPages.StandardController sc = new ApexPages.StandardController(orditem);
                SCDemandController demandController = new SCDemandController(sc, sessionID, testMode);
            }
            
            //PMS 33906/W2: Erweiterungen - ZC16 - Austausch
            if(orditem.order__r.Type__c == SCfwConstants.DOMVAL_ORDERTYPE_REPLACEMENT)
            {
            	Boolean eq = false;
	            Boolean eqNew = false;
	            for(SCOrderItem__c item : items)
	            {
	            	if(item.RecordType.DeveloperName == 'Equipment')
	            	{
	            		eq = true;
	            	}
	            	else if(item.RecordType.DeveloperName == 'EquipmentNew')
	            	{
	            		eqNew = true;
	            	}
	            }
	            isValidZC16 = eq && eqNew;
            }	
        }
        
    }
    
    public Boolean canDispatch
    {
        get
        {
            if(orditem != null && orditem.order__r != null && orditem.order__r.type__c != null && orditem.order__r.status__c != null)
            {
                return (!(SCfwConstants.DOMVAL_ORDERSTATUS_NOT_SCHEDULEABLE.contains(orditem.order__r.status__c) ||
                          SCfwConstants.DOMVAL_ORDERTYPE_NOT_SCHEDULEABLE.contains(orditem.order__r.type__c)) && 
                        !orditem.order__r.InvoicingReleased__c && orditem.cce_canDispatch__c == '1' &&
                         orditem.order__r.cce_ExternalOperationCount__c == 0 
                         //&&
                         //PMS 33906/W2: Erweiterungen - ZC16 - Austausch
                         //(orditem.order__r.type__c != SCfwConstants.DOMVAL_ORDERTYPE_REPLACEMENT || isValidZC16)
                         );
            }
            return false;                     
        }
        
        private set;
    }
    
    /**
     * Checks if the order can have more than one appointment. (gmssu 20.10.2011)
     */
    public Boolean getCanHaveMoreAppointments()
    {   
        if(orditem != null && orditem.order__c != null)
        {
            pendingAppointments = SCboAppointment.readByOrder(orditem.order__c, false);
            String multipleApps = applicationSettings.SCHEDULING_MULTIPLE_APPOINTMENTS__c;
            
            if(multipleApps != '0' && multipleApps != '1')
            {
                multipleApps = '1';
            }
            
            if((pendingAppointments.size() >  0 && multipleApps == '1') || 
               (pendingAppointments.size() == 0 && multipleApps == '0') ||
               (pendingAppointments.size() == 0 && multipleApps == '1'))
                return true;
        }
        
        return false;
    }
    
   /*
    * Checks the lock type and risc class of the account.
    */
    public Boolean getIsCustomerLocked()
    {
        /*
        public final static String DOMVAL_LOCKTYP_CASH = '50002';
        public final static String DOMVAL_LOCKTYP_LOCKED = '50003';
        DOMVAL_RISKCLASS_NONE = '51001';
        DOMVAL_RISKCLASS_REMINDERS = '51002';
        */
        if (orditem != null && orditem.order__c != null)
        {
            SCboOrder boOrder = new SCboOrder(orditem.order__r);
            roleRE = boOrder.getRole(SCfwConstants.DOMVAL_ORDERROLE_RE);
        }

        return ((null != roleRE) && 
                (null != roleRE.Account__r) && 
                (((null != roleRE.Account__r.LockType__c) && 
                  (SCfwConstants.DOMVAL_LOCKTYP_NONE != roleRE.Account__r.LockType__c)) || 
                 ((null != roleRE.Account__r.RiskClass__c) && 
                  (SCfwConstants.DOMVAL_RISKCLASS_NONE != roleRE.Account__r.RiskClass__c))));
    } // getIsCustomerLocked

    /**
     * Getter / setter for ASE session ID
     */
    public String sessionID 
    {
        get
        {
            if (sessionID == null)
            {
                PageReference page = ApexPages.currentPage();
                Cookie aseCookie = null;
                
                if (page != null)
                {
                    aseCookie = page.getCookies().get('ase_sid');
                    System.debug('Reading ASE cookie: ' + aseCookie);
                }
                
                if(aseCookie == null) 
                {
                    System.debug('Getting UserID: ' + UserInfo.getUserId());
                    System.debug('Getting SessionID: ' + UserInfo.getSessionID());
                    String sID = UserInfo.getSessionID();
                    if(sID != null)
                    {
                        sessionID = sID.toLowerCase();
                    }
                    
                    // store ase session (valid for 8 hours)
                    aseCookie = new Cookie('ase_sid', sID.toLowerCase(), null, 8 * 3600, false);
                    // Set the new cookie for the page
                    ApexPages.currentPage().setCookies(new Cookie[]{aseCookie});        
                    System.debug('New SCPlanningSession: ' + sessionID);
                    
                    // create ASE webservice session
                    callout();                    
                    
                    
                }
                else
                {        
                    sessionID = aseCookie.getValue();
                }
           }
           System.debug('ASE sid: ' +  sessionID);
           return sessionID;
        }
            
        set
        {
            // store ase session
            Cookie aseCookie = new Cookie('ase_sid', sessionID, null, -1, false);
            // Set the new cookie for the page
            ApexPages.currentPage().setCookies(new Cookie[]{aseCookie});        
            System.debug('New SCPlanningSession: ' + sessionID);
            
            // create ASE webservice session
            callout();
        }
    }


    /**
     * Initialize the demand with the current user
     *
     * @author HS <hschroeder@gms-online.de>
     */
    public String getUserID()
    {
        String userID = UserInfo.getUserId();
        User user = [ select id, alias from user where user.id = :userID limit 1 ];
        return user.alias;
    }
   
    /**
     * Returns the user UI language
     */
    public String getUserLang() 
    {
        return UserInfo.getLanguage();
    }

    /**
     * Returns the service endpoint
     */
    public String getService() 
    {
        String service = 'aseweb';
        String endpoint = AseCalloutConstants.ENDPOINT;
        Integer pos = endpoint.indexOf(service);
        
        if (pos != -1)
        {
            Integer pos2 = endpoint.indexOf('/', pos);
            
            if (pos2 != -1)
            {
                service = endpoint.subString(pos, pos2);
            }
        }
        return service;
    }

    /**
     * Returns the service endpoint
     */
    public String getCodebase() 
    {
        String codeBase = '/';
        String endpoint = AseCalloutConstants.ENDPOINT;
        Integer pos = endpoint.indexOf('/services/ase');
        
        if (pos != -1)
        {
            codeBase = endpoint.subString(0, pos);
        }
        return codeBase;
    }

   /**
    * Creates an applet session context on the ASE
    */
   public void callout()
    {
        String tenant = UserInfo.getOrganizationID().toLowerCase();

        AseService.aseDataEntry dataEntry = new AseService.aseDataEntry();
        AseService.aseKeyValueType keyValue = new AseService.aseKeyValueType();
        
        // prepare context
        AseService.aseDataType contextParam = new AseService.aseDataType();
        dataEntry = new AseService.aseDataEntry();
        dataEntry.keyValues = new AseService.aseKeyValueType[0]; 

        for (String key : metaContext)
        {
            keyValue = new AseService.aseKeyValueType();
            keyValue.key = key;
            keyValue.value = getValue(key) != null ? getValue(key) : '';
            System.debug('Context: ' + key + ' = ' + keyValue.value);
            dataEntry.keyValues.add(keyValue);
        }

        contextParam.dataEntries = new AseService.aseDataEntry[0];
        contextParam.dataEntries.add(dataEntry);
        contextParam.type_x = AseCalloutConstants.ASE_TYPE_CONTEXT_STR;
        
        AseService.aseDataType[] params = new AseService.aseDataType[]
        { 
            contextParam
        };
        
        AseService.aseSOAP aseSoap = AseCalloutUtils.getAseSoap();
        aseSoap.endpoint_x = AseCalloutConstants.ENDPOINT;
        if (!testMode)
        {
            aseSoap.set(tenant, params);
        }
    }
    
    /**
     * Get a named value
     *
     * @param key the attribute name
     * @return the translated value
     * @author HS <hschroeder@gms-online.de>
     */
    public String getValue(String key)
    {
        // meta key
        if (key == 'id') return sessionID;
        if (key == 'userid') return getUserID();
        if (key == 'rights') return AseRights.getAccess();
        
        return null;
    }
}
