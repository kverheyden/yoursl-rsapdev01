/*
 * @(#)SCAddressServiceImplNL2Test.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/** 
 * Additional tests for the webserivce.nl integration. 
 */
@isTest
private class SCAddressServiceImplNL2Test
{
    static testMethod void CodeCoverageA()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        
        SCAddressServiceImplNL2 obj = new SCAddressServiceImplNL2();
        
        AvsAddress addr = new AvsAddress();
        addr.matchInfo = 'test';
        obj.check(addr);
        
        addr.matchInfo = null;
        addr.postalcode = '1234567 ';
        obj.check(addr);
        
        addr.postalcode = '123456 ';
        addr.housenumber = '12 ';
        obj.check(addr);
        
        addr.postalcode = '123456789 ';
        obj.geocode(addr);
        
        addr.matchInfo = 'test';
        obj.geocode(addr);
    }
}
