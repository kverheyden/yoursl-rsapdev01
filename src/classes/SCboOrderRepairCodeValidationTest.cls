/*
 * @(#)SCboOrderRepairCodeValidationTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This class contains the test code for SCboOrderRepairCodeValidation.
 *
 * @author Alexander Wagner <awagner@gms-online.de>
*/ 
@isTest
private class SCboOrderRepairCodeValidationTest
{
    /*
     * Test: Validation of order repair code data
     */
    static testMethod void validateOrderRepairCodeDataTest() 
    {
        SCHelperTestClass.createOrder(true);        

        SCboOrderRepairCode boRepairCode = new SCboOrderRepairCode();
        boRepairCode.repairCode.Order__c       = SCHelperTestClass.order.Id;

        Test.startTest();

        // activate the validation of the picklist values
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        String oldValue = appSettings.VALIDATE_PICKLIST_VALUES__c;
        appSettings.VALIDATE_PICKLIST_VALUES__c = '1';
        update appSettings;

        // Test 1: wrong error activity code 1
        boRepairCode.repairCode.ErrorActivityCode1__c  = 'XY';
        try
        {
            SCboOrderRepairCodeValidation.validate(boRepairCode);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRepairCodeValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 2: wrong error activity code 2
        boRepairCode.repairCode.ErrorActivityCode1__c  = '';
        boRepairCode.repairCode.ErrorActivityCode2__c  = 'XY';
        try
        {
            SCboOrderRepairCodeValidation.validate(boRepairCode);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRepairCodeValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 3: wrong error symptom
        boRepairCode.repairCode.ErrorActivityCode2__c  = '';
        boRepairCode.repairCode.ErrorSymptom1__c       = 'XY';
        try
        {
            SCboOrderRepairCodeValidation.validate(boRepairCode);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRepairCodeValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 4: wrong error type 1
        boRepairCode.repairCode.ErrorSymptom1__c       = '';
        boRepairCode.repairCode.ErrorType1__c          = 'XY';
        try
        {
            SCboOrderRepairCodeValidation.validate(boRepairCode);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRepairCodeValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 5: wrong error type 2
        boRepairCode.repairCode.ErrorType1__c          = '';
        boRepairCode.repairCode.ErrorType2__c          = 'XY';
        try
        {
            SCboOrderRepairCodeValidation.validate(boRepairCode);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRepairCodeValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 6: wrong error location 1
        boRepairCode.repairCode.ErrorType2__c          = '';
        boRepairCode.repairCode.ErrorLocation1__c      = 'XY';
        try
        {
            SCboOrderRepairCodeValidation.validate(boRepairCode);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRepairCodeValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 7: wrong error location 2
        boRepairCode.repairCode.ErrorLocation1__c      = '';
        boRepairCode.repairCode.ErrorLocation2__c      = 'XY';
        try
        {
            SCboOrderRepairCodeValidation.validate(boRepairCode);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRepairCodeValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 8: wrong return cause
        boRepairCode.repairCode.ErrorLocation2__c      = '';
        boRepairCode.repairCode.ReturnCause__c         = 'XY';
        try
        {
            SCboOrderRepairCodeValidation.validate(boRepairCode);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRepairCodeValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 9: wrong return usage
        boRepairCode.repairCode.ReturnCause__c         = '';
        boRepairCode.repairCode.ReturnUsage__c         = 'XY';
        try
        {
            SCboOrderRepairCodeValidation.validate(boRepairCode);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRepairCodeValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 10: no errors
        boRepairCode.repairCode.ReturnUsage__c         = '';
        try
        {
            Boolean ret = SCboOrderRepairCodeValidation.validate(boRepairCode);
            System.assertEquals(true, ret);
        }
        catch(Exception e)
        {
            System.assert(false);
        }

        // set the old value for the validation of the picklist values
        appSettings.VALIDATE_PICKLIST_VALUES__c = oldValue;
        update appSettings;
        Test.stopTest();
    } // validateOrderRepairCodeDataTest
} // SCboOrderRepairCodeValidationTest
