/*
 * @(#)SCboOrderValidation.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * www.gms-online.de 
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of
 * GMS Development GmbH. ("Confidential Information").  You shall not disclose 
 * any confidential information and shall use it only in accordance with the 
 * terms of the license agreement you aggreed with GMS.
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCboOrderValidation
{
    public static final Integer ORDER_DATA                 =     1; // 0x0001;
    public static final Integer ROLE_DATA                  =     2; // 0x0002;
    public static final Integer ROLE_ADDRESS_DATA          =     4; // 0x0004;
    public static final Integer ROLE_GEO_DATA              =     8; // 0x0008;
    public static final Integer ORDERITEM_DATA             =    16; // 0x0010;
    public static final Integer INSTBASE_DATA              =    32; // 0x0020;
    public static final Integer INSTBASELOC_DATA           =    64; // 0x0040;
    public static final Integer INSTBASELOC_ADDRESS_DATA   =   128; // 0x0080;
    public static final Integer INSTBASELOC_GEO_DATA       =   256; // 0x0100;
    public static final Integer ORDERLINE_DATA             =   512; // 0x0200;
    public static final Integer REPAIRCODE_DATA            =  1024; // 0x0400;
    
    public static final Integer ALL                        = 65535; // 0xFFFF;
    
    private static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();

    /**
     * Validates a business object from the type order.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static Boolean validate(SCboOrder boOrder)
    {
        return validate(boOrder, ALL);
    }
    public static Boolean validate(SCboOrder boOrder, Integer mode)
    {
        Boolean ret = true;
        
        if ((mode & ORDER_DATA) == ORDER_DATA)
        {
            Date today = Date.today();
            /* CCEAG
            if (!SCBase.isSet(boOrder.order.CustomerPriority__c))
            {
                throw new SCfwException(2002);
            }
            
            if (!SCBase.isSet(boOrder.order.FailureType__c))
            {
                throw new SCfwException(2003);
            }
            
            if (null == boOrder.order.CustomerPrefStart__c)
            {
                throw new SCfwException(2004);
            }
            if (null == boOrder.order.CustomerPrefEnd__c)
            {
                throw new SCfwException(2005);
            }
            
            if (!SCbase.isSet(boOrder.order.Id) && (boOrder.order.CustomerPrefStart__c < today))
            {
                throw new SCfwException(2006);
            }
            if (!SCbase.isSet(boOrder.order.Id) && (boOrder.order.CustomerPrefEnd__c < today))
            {
                throw new SCfwException(2007);
            }
            if (boOrder.order.CustomerPrefStart__c > boOrder.order.CustomerPrefEnd__c)
            {
                throw new SCfwException(2008);
            }
            
            if (!SCBase.isSet(boOrder.order.CustomerTimewindow__c))
            {
                throw new SCfwException(2009);
            }
            */
            if (!SCBase.isSet(boOrder.order.DepartmentCurrent__c))
            {
                throw new SCfwException(2011);
            }
            
            if (SCBase.isSet(appSettings.VALIDATE_PICKLIST_VALUES__c) && 
                appSettings.VALIDATE_PICKLIST_VALUES__c.equals('1'))
            {
                if (SCBase.isSet(boOrder.order.Type__c) && 
                    !SCBase.isPicklistValue(SCOrder__c.Type__c, boOrder.order.Type__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrder.order.Type__c, 'SCOrder__c.Type__c'));
                }
                
                if (SCBase.isSet(boOrder.order.InvoicingType__c) && 
                    !SCBase.isPicklistValue(SCOrder__c.InvoicingType__c, boOrder.order.InvoicingType__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrder.order.InvoicingType__c, 'SCOrder__c.InvoicingType__c'));
                }
                
                if (SCBase.isSet(boOrder.order.InvoicingSubType__c) && 
                    !SCBase.isPicklistValue(SCOrder__c.InvoicingSubType__c, boOrder.order.InvoicingSubType__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrder.order.InvoicingSubType__c, 'SCOrder__c.InvoicingSubType__c'));
                }
                
                if (SCBase.isSet(boOrder.order.PaymentType__c) && 
                    !SCBase.isPicklistValue(SCOrder__c.PaymentType__c, boOrder.order.PaymentType__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrder.order.PaymentType__c, 'SCOrder__c.PaymentType__c'));
                }
                
                if (SCBase.isSet(boOrder.order.CustomerPriority__c) && 
                    !SCBase.isPicklistValue(SCOrder__c.CustomerPriority__c, boOrder.order.CustomerPriority__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrder.order.CustomerPriority__c, 'SCOrder__c.CustomerPriority__c'));
                }
                
                if (SCBase.isSet(boOrder.order.Status__c) && 
                    !SCBase.isPicklistValue(SCOrder__c.Status__c, boOrder.order.Status__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrder.order.Status__c, 'SCOrder__c.Status__c'));
                }
                
                if (SCBase.isSet(boOrder.order.FailureType__c) && 
                    !SCBase.isPicklistValue(SCOrder__c.FailureType__c, boOrder.order.FailureType__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrder.order.FailureType__c, 'SCOrder__c.FailureType__c'));
                }
                
                if (SCBase.isSet(boOrder.order.CustomerTimewindow__c) && 
                    !SCBase.isPicklistValue(SCOrder__c.CustomerTimewindow__c, boOrder.order.CustomerTimewindow__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrder.order.CustomerTimewindow__c, 'SCOrder__c.CustomerTimewindow__c'));
                }
                
                if (SCBase.isSet(boOrder.order.DistanceZone__c) && 
                    !SCBase.isPicklistValue(SCOrder__c.DistanceZone__c, boOrder.order.DistanceZone__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrder.order.DistanceZone__c, 'SCOrder__c.DistanceZone__c'));
                }
                
                if (SCBase.isSet(boOrder.order.Country__c) && 
                    !SCBase.isPicklistValue(SCOrder__c.Country__c, boOrder.order.Country__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrder.order.Country__c, 'SCOrder__c.Country__c'));
                }
                
                if (SCBase.isSet(boOrder.order.Followup1__c) && 
                    !SCBase.isPicklistValue(SCOrder__c.Followup1__c, boOrder.order.Followup1__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrder.order.Followup1__c, 'SCOrder__c.Followup1__c'));
                }
                
                if (SCBase.isSet(boOrder.order.Followup2__c) && 
                    !SCBase.isPicklistValue(SCOrder__c.Followup2__c, boOrder.order.Followup2__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrder.order.Followup2__c, 'SCOrder__c.Followup2__c'));
                }
                
                if (SCBase.isSet(boOrder.order.InvoicingStatus__c) && 
                    !SCBase.isPicklistValue(SCOrder__c.InvoicingStatus__c, boOrder.order.InvoicingStatus__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrder.order.InvoicingStatus__c, 'SCOrder__c.InvoicingStatus__c'));
                }
            } // if (SCBase.isSet(appSettings.VALIDATE_PICKLIST_VALUES__c) && ...
        } // if ((mode & ORDER_DATA) == ORDER_DATA)
        
        if (((mode & ROLE_DATA) == ROLE_DATA) || 
            ((mode & ROLE_ADDRESS_DATA) == ROLE_ADDRESS_DATA) || 
            ((mode & ROLE_GEO_DATA) == ROLE_GEO_DATA))
        {
            if (null == boOrder.getRole(SCfwConstants.DOMVAL_ORDERROLE_LE))
            {
                throw new SCfwException(2000);
            }

            for (SCOrderRole__c role :boOrder.allRole)
            {
                ret |= SCboOrderRoleValidation.validate(role, mode);
            } // for (SCOrderRole__c role :boOrder.allRole)
        } // if (((mode & ROLE_DATA) == ROLE_DATA) || ...
        
        if (((mode & ORDERITEM_DATA) == ORDERITEM_DATA) ||
            ((mode & INSTBASE_DATA) == INSTBASE_DATA) || 
            ((mode & INSTBASELOC_DATA) == INSTBASELOC_DATA) || 
            ((mode & INSTBASELOC_ADDRESS_DATA) == INSTBASELOC_ADDRESS_DATA) || 
            ((mode & INSTBASELOC_GEO_DATA) == INSTBASELOC_GEO_DATA) ||  
            (((mode & ORDERLINE_DATA) == ORDERLINE_DATA) && 
             ((mode & ORDERITEM_DATA) == ORDERITEM_DATA))|| 
            ((mode & REPAIRCODE_DATA) == REPAIRCODE_DATA))
        {
            if (boOrder.boOrderItems.size() == 0)
            {
                throw new SCfwException(2001);
            }

            for (SCboOrderItem item :boOrder.boOrderItems)
            {
                ret |= SCboOrderItemValidation.validate(item, mode);
            } // for (SCboOrderItem item :boOrder.boOrderItems)
        } // if (((mode & ORDERITEM_DATA) == ORDERITEM_DATA) ||...
        
        if ((mode & ORDERLINE_DATA) == ORDERLINE_DATA)
        {
            for (SCboOrderLine line :boOrder.boOrderLines)
            {
                ret |= SCboOrderLineValidation.validate(line);
            } // for (SCboOrderLine line :boOrder.boOrderLines)
        } // if (mode & ORDERLINE_DATA) == ORDERLINE_DATA)
        return ret;
    } // validate
} // SCboOrderValidation
