/*
* @(#)SCVPOrderAttachmentController.cls
* 
* Copyright 2014 by GMS Development GmbH, Karl-Schurz-Strasse 29, 
* DE-33100 Paderborn. All rights reserved.
* 
* This software is the confidential and proprietary information
* of GMS Development GmbH. ("Confidential Information").  You
* shall not disclose such Confidential Information and shall use
* it only in accordance with the terms of the license agreement
* you entered into with GMS.
*
* # Creates a new Attachment for a specified Order record.
*
* @author Marc Sälzler <msaelzler@gms-online.de>
*
* @history  
* 2014-11-05 GMSmae created
* 
*/

public class SCVPOrderAttachmentController
{
    public Id x_orderId; // Stores the id of the current Order record.
    
    /**
    * Default Constructor
    * 
    */
    public SCVPOrderAttachmentController()
    {
        if(ApexPages.currentPage().getParameters().containsKey('id'))
        {
            x_orderId = ApexPages.currentPage().getParameters().get('id');
        }
        else
        {
            a_errorText = Label.SC_msg_VP_NoOrderSelected;
        }
    }
    
    /*
    * Attribute for the vf page that contains any upcoming errors in general.
    *
    */
    public String a_errorText
    {
        get
        {
            if(a_errorText == null)
            {
                a_errorText = '';
            }
            
            return a_errorText;
        }
        
        set;
    }
    
    /*
    * Attribute for the VF page that contains upcoming errors regarding the Attachment.
    *
    */
    public String a_errorTextAtt
    {
        get
        {
            if(a_errorTextAtt == null)
            {
                a_errorTextAtt = '';
            }
            
            return a_errorTextAtt;
        }
        
        set;
    }
    
    /*
    * Attribute of an Attachment record for the VF page to create a new Attachment.
    *
    */
    public Attachment a_attachment
    {
        get
        {
            if (a_attachment == null)
            {
                a_attachment = new Attachment();
            }
            
            return a_attachment;
        }
        
        set;
    }
    
    /**
    * Attribute for the VF page to identify that the creation was successful.
    *
    */
    public Boolean a_success
    {
        get
        {
            if(a_success == null)
            {
                a_success = false;
            }
            
            return a_success;
        }
        
        set;
    }
    
    /**
    * Creates a new Attachment for the specified Order record.
    *
    */ 
    public PageReference CreateAttachment()
    {
        if (x_orderId != null)
        {
            a_attachment.isPrivate  = false;
            a_attachment.ParentId   = x_orderId;
            
            if (a_attachment.Name == null || a_attachment.Body == null)
            {
                 a_errorTextAtt = Label.SC_msg_VP_NoAttachmntSelected;
            }
            else
            {
                try
                {
                    upsert a_attachment;
                    
                    a_success = true;
                }
                catch (Exception e)
                {
                    a_errorTextAtt = String.valueOf(e);
                }
            }
        }
        
        return null;
    }
}
