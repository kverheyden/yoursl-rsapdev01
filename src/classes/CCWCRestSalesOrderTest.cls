@isTest(SeeAllData=true)
private class CCWCRestSalesOrderTest {
	
	static Pricebook2 pb;
	static PricebookEntry standardPrice;
	static PricebookEntry pbe;
	static Product2 prod;
	static SalesOrder__c sorder;
	static Request__c requestC;
	static Account account;
	static User user;
	static CCAccountRole__c accountrole;
	static PricebookEntryDetail__c ped;
	
	static CCWCRestSalesOrder.SalesOrderWrapper newOrder = new CCWCRestSalesOrder.SalesOrderWrapper();
	static CCWCRestSalesOrder.SalesOrderItemWrapper newOrderItem = new CCWCRestSalesOrder.SalesOrderItemWrapper();
	
	public static testMethod void testPost()
	{
		setupTestData();
		newOrder.Status = 'Active';
        newOrder.EarliestDeliveryDate = '2014-01-01';
        newOrder.CustomerNumberAtSupplier = 'KD30472';
		newOrder.RecordType = 'Indirect Order';
		newOrder.AppIdentifierKey = '1234';
		newOrder.AccountId = account.Id;
		newOrder.OrderDate = '2014-07-28 13:22:04';
		newOrder.OwnerId = user.Id;
		newOrder.Pricebook2Id = pb.Id;
		newOrder.RequestedDeliveryDate = '2014-01-01';
		newOrder.RequestId = requestC.Id;
		newOrder.SupplierId = account.Id;
		newOrder.Type = 'Paid Order';
		newOrder.Description = 'test';
		
		newOrder.SalesOrderItems = new List<CCWCRestSalesOrder.SalesOrderItemWrapper>();
		newOrderItem.Description = 'SomeDescription';
		newOrderItem.PricebookEntryDetailId = ped.Id;
		newOrderItem.Product2Id = prod.Id;
		newOrderItem.Quantity = '3';
		newOrderItem.SortOrder = '1';
		
		newOrder.SalesOrderItems.add(newOrderItem);
        
		String jsonMsg = JSON.serialize(newOrder);
       	System.debug('JSON ' + jsonMsg);
		
		Test.startTest();

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        //req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/SalesOrder/';
        req.requestURI = '/SalesOrder/';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonMsg);
        RestContext.request = req;
        RestContext.response = res;

        CCWCRestSalesOrder.ResponseWrapper resp = new CCWCRestSalesOrder.ResponseWrapper();
        CCWCRestSalesOrder.doPost(newOrder);
        System.debug('URI: ' + RestContext.Request.RequestURI);
        System.debug('BODY: ' + RestContext.Request.RequestBody.toString());
        
        Test.stopTest();       
	}
	
	static void setupTestData() {
		insertPricebook();
		insertProduct2();
		insertStandardPricebookEntry();		
		insertPricebookEntry();
		insertRequest();
		insertAccount();
		insertUser();
		insertAccountRole();
		insertPricebookEntryDetail();
	}
	
	static void insertRequest(){
		requestC = new Request__c();
		requestC.OutletCompanyName__c = 'Muster GmbH';
		requestC.ShipToCity__c = 'Musterhausen';
		requestC.PayerEmail__c = 'abc@def.de';
		insert requestC;
	}
	
	static void insertAccount(){
		account = new Account();
        account.LastName__c = 'testaccount 00';
        account.FirstName__c = 'Person';
        account.BillingPostalCode = '33106 PB'; 
        account.BillingCountry__c = 'DE';
        account.BillingStreet = 'Karl-Schurz Str.';
        account.BillingHouseNo__c = '29';
        account.BillingPostalCode = '33100 PB';
        account.BillingCity = 'Padebrorn';
        account.BillingCountry__c = 'PB';
        account.Request__c = requestC.Id;
        insert account;   
	} 
	
	static void insertAccountRole(){
		accountrole = new CCAccountRole__c();
		accountrole.CustomerNumberAtSlaveAccount__c = 'test';
		accountrole.MasterAccount__c = account.Id;
		accountrole.SlaveAccount__c  = account.Id;
		insert accountrole;
		
	}
	static void insertUser(){
		user = new User();
		user.LastName = 'Mutzek';
		user.FirstName = 'Horst';
		user.Email = 'mutzek@test.de';
		user.Alias = 'lumpi';
		user.Username = 'aasasd@asd.de';
		user.CommunityNickname = 'lumpi';
		user.TimeZoneSidKey = 'GMT';
		user.LocaleSidKey = 'DE';
		user.EmailEncodingKey = 'ISO-8859-1';
		user.LanguageLocaleKey = 'DE';
		user.ProfileId = [Select Id From Profile Where Name = 'System Administrator'].Id;
		insert user;
	}
	
	static void insertPricebook() {
		pb = new Pricebook2();
		pb.Name = 'Standard Price Book';
		pb.Description = 'Price Book Products';
		pb.IsActive = true;
		insert pb;
	}
	
	static void insertProduct2() {
		prod = new Product2();
		prod.Name = 'Test Product';
		prod.Family = 'Test Products';
		prod.IsActive = true;
		insert prod;
	}
	
	static void insertStandardPricebookEntry(){
		standardPrice = new PricebookEntry();
		standardPrice.Pricebook2Id = Test.getStandardPricebookId();
		standardPrice.Product2Id = prod.Id;
		standardPrice.UnitPrice = 10000;
		standardPrice.IsActive = true;
		standardPrice.UseStandardPrice = false;
		insert standardPrice;
	}
	
	static void insertPricebookEntry() {
		pbe = new PricebookEntry();
		pbe.Pricebook2Id = pb.Id;
		pbe.Product2Id = prod.Id; 
		pbe.UnitPrice = 10000;
		pbe.IsActive = true;
		pbe.UseStandardPrice = false;
		insert pbe;
	}
	
	static void insertPricebookEntryDetail() {
		ped = new PricebookEntryDetail__c();
		ped.PricebookEntryID__c = pbe.Id;
		ped.Product2__c = prod.Id;
		insert ped;
	}
	
}
