/*
 * @(#)CCWSMaterialStockChangeTest.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Tests for interface CCWSMaterialStockChangeTest 
 */
@isTest
public with sharing class CCWSMaterialStockChangeTest {
    
   /*
    * Positive test
    */
    @isTest
    static void test1()
    {
        prepareTestData();
        
        
        CCWSMaterialStockChange.CCWSMaterialStockChangeClass stockChange = new CCWSMaterialStockChange.CCWSMaterialStockChangeClass();
        CCWSMaterialStockChange.MessageHeaderClass messageHeader = new CCWSMaterialStockChange.MessageHeaderClass();
        messageHeader.MessageID = '50D01340996D0E10E10080000A62025A';
        messageHeader.MessageUUID = '50D01340-996D-0E10-E100-80000A62025A';
        
        CCWSMaterialStockChange.ItemClass item1 = new CCWSMaterialStockChange.ItemClass();        
        item1.CompanyCode = '1095';
        item1.MaterialDocumentNumber = '4926418802';
        item1.MaterialDocumentYear = '2012';
        item1.PostingDate = date.newinstance(2013, 2, 11);
        item1.MovementType = '5203'; 
        item1.Plant = 'T0361'; 
        item1.StorageLocation = 'T6500';
        item1.Batch = null;
        item1.ReceivingPlant = null;
        item1.ReceivingStorageLocation = null;
        item1.ReceivingBatch = null;
        item1.MaterialID = '151000049';
        item1.UnitOfMeasure = 'EA';
        item1.Quantity = '-3.0';
        item1.ValuationType = null;
        
        CCWSMaterialStockChange.ItemClass item2 = new CCWSMaterialStockChange.ItemClass();        
        item2.CompanyCode = '1095';
        item2.MaterialDocumentNumber = '4926418666';
        item2.MaterialDocumentYear = '2012';
        item2.PostingDate = date.newinstance(2013, 1, 11);
        item2.MovementType = '5212'; 
        item2.Plant = 'T0359'; 
        item2.StorageLocation = 'T6500';
        item2.Batch = null;
        item2.ReceivingPlant = 'T0359';
        item2.ReceivingStorageLocation = 'TSC01';
        item2.ReceivingBatch = null;
        item2.MaterialID = '151000001';
        item2.UnitOfMeasure = 'EA';
        item2.Quantity = '1.0';
        item2.ValuationType = null;     
        
        
        CCWSMaterialStockChange.ItemClass item3 = new CCWSMaterialStockChange.ItemClass();        
        item3.CompanyCode = '1095';
        item3.MaterialDocumentNumber = '4926418669';
        item3.MaterialDocumentYear = '2012';
        item3.PostingDate = date.newinstance(2013, 1, 10);
        item3.MovementType = '5228'; 
        item3.Plant = 'T0359'; 
        item3.StorageLocation = 'T6500';
        item3.Batch = null;
        item3.ReceivingPlant = null;
        item3.ReceivingStorageLocation = null;
        item3.ReceivingBatch = null;
        item3.MaterialID = '151000002';
        item3.UnitOfMeasure = 'EA';
        item3.Quantity = '4.0';
        item3.ValuationType = 'ZNEU';   
           
        Test.startTest();

        stockChange.MessageHeader = messageHeader;
        stockChange.Item = new List<CCWSMaterialStockChange.ItemClass>();
        CCWSMaterialStockChange.transmit(stockChange);
        
        
        stockChange.Item.add(item1);
        stockChange.Item.add(item2);
        stockChange.Item.add(item3);
        CCWSMaterialStockChange.transmit(stockChange);
        CCWSMaterialStockChange.debugMessage(stockChange);
        test.stopTest();
        
        // Item1: 'T0361-T6500-151000049-'
        List<SCStockItem__c> stockItems = [SELECT ID, Name, Stock__r.Name, Plant__c, Article__r.Name, Qty__c, util_StockItemSelector__c, ValuationType__c FROM SCStockItem__c WHERE util_StockItemSelector__c =: 'T0361-T6500-151000049-' ];
        System.assertEquals(stockItems[0].Article__r.Name, '151000049');                                  
        
        // Item2: 'T0359-T6500-151000001-'
        stockItems = [SELECT ID, Name, Stock__r.Name, Plant__c, Article__r.Name, Qty__c, util_StockItemSelector__c, ValuationType__c FROM SCStockItem__c WHERE util_StockItemSelector__c =: 'T0359-T6500-151000001-' ];
        System.assertEquals(stockItems[0].Article__r.Name, '151000001'); 

        
        // Item3: 'T0359-T6500-151000002-ZNEU'
        stockItems = [SELECT ID, Name, Stock__r.Name, Plant__c, Article__r.Name, Qty__c, util_StockItemSelector__c, ValuationType__c FROM SCStockItem__c WHERE util_StockItemSelector__c =: 'T0359-T6500-151000002-ZNEU' ];
        System.assertEquals(stockItems[0].Article__r.Name, '151000002'); 
                        
    }
    
    
   /*
    * Missing data test cases:
    * Case 5203 without storage Location
    * Case 5212 without storage Location and receiving SL
    * Case 5228 without storage Location
    * Case 5203 without article
    * Case 5212 with character quantity
    * Case 5228 without plant
    */
    
    @isTest
    static void test2()
    {
        prepareTestData();
        
        
        CCWSMaterialStockChange.CCWSMaterialStockChangeClass stockChange = new CCWSMaterialStockChange.CCWSMaterialStockChangeClass();
        CCWSMaterialStockChange.MessageHeaderClass messageHeader = new CCWSMaterialStockChange.MessageHeaderClass();
        messageHeader.MessageID = '50D01340996D0E10E10080000A62025A';
        messageHeader.MessageUUID = '50D01340-996D-0E10-E100-80000A62025A';
        
        CCWSMaterialStockChange.ItemClass item1 = new CCWSMaterialStockChange.ItemClass();        
        item1.CompanyCode = '1095';
        item1.MaterialDocumentNumber = '4926418802';
        item1.MaterialDocumentYear = '2012';
        item1.PostingDate = date.newinstance(2013, 2, 11);
        item1.MovementType = '5203'; 
        item1.Plant = 'T0361'; 
        item1.StorageLocation = null;
        item1.Batch = null;
        item1.ReceivingPlant = null;
        item1.ReceivingStorageLocation = null;
        item1.ReceivingBatch = null;
        item1.MaterialID = '151000049';
        item1.UnitOfMeasure = 'EA';
        item1.Quantity = '-3.0';
        item1.ValuationType = null;
        
        CCWSMaterialStockChange.ItemClass item2 = new CCWSMaterialStockChange.ItemClass();        
        item2.CompanyCode = '1095';
        item2.MaterialDocumentNumber = '4926418666';
        item2.MaterialDocumentYear = '2012';
        item2.PostingDate = date.newinstance(2013, 1, 11);
        item2.MovementType = '5212'; 
        item2.Plant = 'T0359'; 
        item2.StorageLocation = null;
        item2.Batch = null;
        item2.ReceivingPlant = 'T0359';
        item2.ReceivingStorageLocation = null;
        item2.ReceivingBatch = null;
        item2.MaterialID = '151000001';
        item2.UnitOfMeasure = 'EA';
        item2.Quantity = '1.0';
        item2.ValuationType = null;     
        
        
        CCWSMaterialStockChange.ItemClass item3 = new CCWSMaterialStockChange.ItemClass();        
        item3.CompanyCode = '1095';
        item3.MaterialDocumentNumber = '4926418669';
        item3.MaterialDocumentYear = '2012';
        item3.PostingDate = date.newinstance(2013, 1, 10);
        item3.MovementType = '5228'; 
        item3.Plant = 'T0359'; 
        item3.StorageLocation = null;
        item3.Batch = null;
        item3.ReceivingPlant = null;
        item3.ReceivingStorageLocation = null;
        item3.ReceivingBatch = null;
        item3.MaterialID = '151000002';
        item3.UnitOfMeasure = 'EA';
        item3.Quantity = '4.0';
        item3.ValuationType = 'ZNEU';   
           
           
        CCWSMaterialStockChange.ItemClass item4 = new CCWSMaterialStockChange.ItemClass();        
        item4.CompanyCode = '1095';
        item4.MaterialDocumentNumber = '4926418802';
        item4.MaterialDocumentYear = '2012';
        item4.PostingDate = date.newinstance(2013, 2, 11);
        item4.MovementType = '5203'; 
        item4.Plant = 'T0361'; 
        item4.StorageLocation = 'T6500';
        item4.Batch = null;
        item4.ReceivingPlant = null;
        item4.ReceivingStorageLocation = null;
        item4.ReceivingBatch = null;
        item4.MaterialID = null;
        item4.UnitOfMeasure = 'EA';
        item4.Quantity = '-3.0';
        item4.ValuationType = null;
        
        CCWSMaterialStockChange.ItemClass item5 = new CCWSMaterialStockChange.ItemClass();        
        item5.CompanyCode = '1095';
        item5.MaterialDocumentNumber = '4926418666';
        item5.MaterialDocumentYear = '2012';
        item5.PostingDate = date.newinstance(2013, 1, 11);
        item5.MovementType = '5212'; 
        item5.Plant = 'T0359'; 
        item5.StorageLocation = 'T6500';
        item5.Batch = null;
        item5.ReceivingPlant = 'T0359';
        item5.ReceivingStorageLocation = 'TSC01';
        item5.ReceivingBatch = null;
        item5.MaterialID = '151000000';
        item5.UnitOfMeasure = 'EA';
        item5.Quantity = 'eins';
        item5.ValuationType = null;     
        
        
        CCWSMaterialStockChange.ItemClass item6 = new CCWSMaterialStockChange.ItemClass();        
        item6.CompanyCode = '1095';
        item6.MaterialDocumentNumber = '4926418669';
        item6.MaterialDocumentYear = '2012';
        item6.PostingDate = date.newinstance(2013, 1, 10);
        item6.MovementType = '5228'; 
        item6.Plant = null; 
        item6.StorageLocation = 'T6500';
        item6.Batch = null;
        item6.ReceivingPlant = null;
        item6.ReceivingStorageLocation = null;
        item6.ReceivingBatch = null;
        item6.MaterialID = '151000003';
        item6.UnitOfMeasure = 'EA';
        item6.Quantity = '4.0';
        item6.ValuationType = 'ZNEU';   
              
        Test.startTest();

        stockChange.MessageHeader = messageHeader;
        stockChange.Item = new List<CCWSMaterialStockChange.ItemClass>();
        CCWSMaterialStockChange.transmit(stockChange);
        
        
        stockChange.Item.add(item1);
        stockChange.Item.add(item2);
        stockChange.Item.add(item3);
        stockChange.Item.add(item4);
        stockChange.Item.add(item5);
        stockChange.Item.add(item6);
        CCWSMaterialStockChange.transmit(stockChange);
        CCWSMaterialStockChange.debugMessage(stockChange);
        test.stopTest();
        
        // Item1: 'T0361-null-151000049-'
        List<SCStockItem__c> stockItems = [SELECT ID, Name, Stock__r.Name, Plant__c, Article__r.Name, Qty__c, util_StockItemSelector__c, ValuationType__c FROM SCStockItem__c WHERE util_StockItemSelector__c =: 'T0361--151000049-' ];
        System.assertEquals(stockItems.size(), 0);                                  
        
        // Item2: 'T0359-null-151000001-'
        stockItems = [SELECT ID, Name, Stock__r.Name, Plant__c, Article__r.Name, Qty__c, util_StockItemSelector__c, ValuationType__c FROM SCStockItem__c WHERE util_StockItemSelector__c =: 'T0359--151000001-' ];
        System.assertEquals(stockItems.size(), 0);

        
        // Item3: 'T0359-null-151000002-ZNEU'
        stockItems = [SELECT ID, Name, Stock__r.Name, Plant__c, Article__r.Name, Qty__c, util_StockItemSelector__c, ValuationType__c FROM SCStockItem__c WHERE util_StockItemSelector__c =: 'T0359--151000002-ZNEU' ];
        System.assertEquals(stockItems.size(), 0); 
                        
    }
    
     /*
    * Fire set of real data 
    * 
    * 
    */
    
    @isTest
    static void test3()
    {
        prepareTestData();
        
        String jsonInput = '{"MessageHeader":{"MessageUUID":"52802CD2D5D50BF0E10080000A63020B","MessageID":"52802CD0D5D50BF0E10080000A63020B"},"Item":[{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"5.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151000050","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"2.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151000857","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"0.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151001433","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":"ZNEU"},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"0.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151001953","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"0.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151001954","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"0.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151001984","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":"ZGEBROW"},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"5.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151004024","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"2.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151004302","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"0.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151004611","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":"ZGEBRMW"},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"2.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151005246","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"1.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151005247","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"2.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151006176","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":"ZGEBROW"},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"0.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151006176","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":"ZNEU"},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"0.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151010293","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"2.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151011262","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"13.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151011263","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"M","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"20.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151011535","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"M","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"10.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151011540","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"10.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151012812","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"0.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151015128","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":"ZNEU"},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"14.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151019663","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"150.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151022607","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"0.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151022622","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":"ZGEBRMW"},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"0.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151022623","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":"ZNEU"},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"17.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151022871","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"0.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151022893","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"0.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151022894","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"3.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151022935","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":"ZNEU"},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"100.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151023017","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"90.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151023018","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"60.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151023052","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"0.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151023127","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"25.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151023137","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"30.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151023234","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"50.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151023277","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"0.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151023590","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":"ZGEBRMW"},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"0.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151023590","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":"ZGEBROW"},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"60.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151023808","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"0.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151023852","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null},{"ValuationType":null,"UnitOfMeasure":"EA","StorageLocation":"SC23","ReceivingStorageLocation":null,"ReceivingPlant":null,"ReceivingBatch":null,"Quantity":"0.0","PostingDate":"2013-11-11","Plant":"0356","MovementType":"5228","MaterialID":"151023853","MaterialDocumentYear":"2013","MaterialDocumentNumber":null,"CompanyCode":null,"Batch":null}]}';
        CCWSMaterialStockChange.CCWSMaterialStockChangeClass testMessage = 
            (CCWSMaterialStockChange.CCWSMaterialStockChangeClass)JSON.deserialize(jsonInput, CCWSMaterialStockChange.CCWSMaterialStockChangeClass.class);
        
                      
        Test.startTest();

        CCWSMaterialStockChange.doProcess(testMessage,'transmit');
        CCWSMaterialStockChange.debugMessage(testMessage);
        test.stopTest();
        
        // Item1: '0356-SC63-151019663-'
        List<SCStockItem__c> stockItems = [SELECT ID, Name, Stock__r.Name, Plant__c, Article__r.Name, Qty__c, util_StockItemSelector__c, ValuationType__c FROM SCStockItem__c WHERE util_StockItemSelector__c =: 'SC23--151019663-' ];
        System.assertEquals(stockItems.size(), 0);                                  
        
        /*// Item2: 'T0359-null-151000001-'
        stockItems = [SELECT ID, Name, Stock__r.Name, Plant__c, Article__r.Name, Qty__c, util_StockItemSelector__c, ValuationType__c FROM SCStockItem__c WHERE util_StockItemSelector__c =: 'SC23--151000001-' ];
        System.assertEquals(stockItems.size(), 0);

        
        // Item3: 'T0359-null-151000002-ZNEU'
        stockItems = [SELECT ID, Name, Stock__r.Name, Plant__c, Article__r.Name, Qty__c, util_StockItemSelector__c, ValuationType__c FROM SCStockItem__c WHERE util_StockItemSelector__c =: 'SC23--151000002-ZNEU' ];
        System.assertEquals(stockItems.size(), 0); */
                        
    }
    
    
    public static void prepareTestData()
    {
        CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();

        appSettings = SCApplicationSettings__c.getInstance();

    }
    

}
