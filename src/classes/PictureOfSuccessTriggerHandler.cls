public with sharing class PictureOfSuccessTriggerHandler {

	public static final String DEVELOPERNAME_FIELDNAME = 'PictureDocDevName__c';
	public static final String IDTEXT_FIELDNAME = 'Picture__c';


	public class PictureOfSuccessBIHandler extends TriggerHandlerBase {
		public override void mainEntry(TriggerParameters tp) {
			setIdTextByDeveloperName(tp.newList);
		}
	}

	public class PictureOfSuccessBUHandler extends TriggerHandlerBase {
		public override void mainEntry(TriggerParameters tp) {
			setIdTextByDeveloperName(tp.newList);
		}
	}

	public static void setIdTextByDeveloperName(List<PictureOfSuccess__c> pofs) {
		Set<String> documentDeveloperNames = new Set<String>();
		for (PictureOfSuccess__c p : pofs) {
			String devName = (String)p.get(DEVELOPERNAME_FIELDNAME);
			if (!String.isBlank(devName))
				documentDeveloperNames.add(devName);
		}
		if (documentDeveloperNames.isEmpty())
			return;

		List<Document> documents = [SELECT Id, DeveloperName FROM Document WHERE DeveloperName IN: documentDeveloperNames];
		if (documents.isEmpty())
			return;
		
		Map<String, Id> developerNamesWithId = new Map<String, Id>();
		for (Document d : documents)
			developerNamesWithId.put(d.DeveloperName, d.Id);


		for (PictureOfSuccess__c p : pofs) {
			String devName = (String)p.get(DEVELOPERNAME_FIELDNAME);
			Id documentId = developerNamesWithId.get(devName);
			p.put(IDTEXT_FIELDNAME, documentId);
		}
	}
}
