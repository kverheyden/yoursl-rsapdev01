/*
 * @(#)SCTimereportViewController.cls
 *
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Implements the controller for simple hovers that can be used to
 * display context sensitive information. The controller evalates
 * the mode and oid field and reads the corresponding data.
 *
 * @author Dietrich Herdt  <dherdt@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCTimereportViewController extends SCfwComponentControllerBase
{
    // public String rid { get; set; }
    // public String day { get; set; }
    public String status { get; set; }
    public String dayDisplay { get; set; }
    public String dayDeleted { get; set; }
    public String resource { get; set; }
    public String employee { get; set; }
    public String statusIcon { get; set; }
    public String selectedTimeRepItem { get; set; }
    public String dayStartListString { get; set; }
    public String timeReportOpenStartId { get; set; }   
    
    public boolean enableEdit { get; set; }
    public boolean enableDelete { get; set; }
    public boolean closeButtonDisabled { get; set; }
    public boolean dayEndExists { get; set; }
    public boolean addButtonDisabled { get; set; }
    public boolean errorIntoTimeReport  { get; set; }
    public boolean closeTimeReportError  { get; set; }
    public boolean dayStartIsDeleted { get; set; }
    public boolean pageControllerIsNotNull { get; set; }
    public boolean processInThePast { get; set; }
    public boolean refreshParent  { get; set; }    
    public boolean getHasItems ()
    {
      return ((itemsEvaluation != null) && (itemsEvaluation.size() > 0));
    }
    
    public List<SCTimeReport__c> items;
    public List<timeReportEvaluation> itemsEvaluation;
    public List<timeReportMulti> multiTimeReport;
    public List <String> timeReportStartIDs { get; set; }   
    public timeReportEvaluation curTimeReport { get; private set; }
    
    public Integer timeReportIndex { get; set; }


    public SCTimereportViewController ()
    {
         try
         {
         
            //If set, the order can be processed in the past.
            SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();

             // validation for past order processing      
             String aid  = ApexPages.currentPage().getParameters().get('aid');
             if ((aid != null) && (appset.TIMEREPORT_PAST_PROCESS__c))
             {
                 processInThePast = true;
             }
          }
          catch (Exception e)
          {
             // TODO
          }   
    }

    /**
     * Deletes the selected time report item.
     *
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference deleteItem()
    {
        String tid  = ApexPages.currentPage().getParameters().get('tid');
        System.debug('Delete item ID is ####:' + tid);
        SCboTimereport boTimereport = new SCboTimereport();
        // defines time report for selected entry
        SCTimereport__c deleteEntryTR = new SCTimereport__c ();
        boolean entryHadDistance = false;
        dayStartIsDeleted = false;
        pageControllerIsNotNull = false;

        String currentStartId = null;
        
        // try to find the selected entry into DB
        try
        {
            // clear the time report list
            items.clear();

            deleteEntryTR = [select Id,
                                    Type__c,
                                    Start__c,
                                    Distance__c,
                                    ReportLink__c
                               from SCTimereport__c
                              where Id = :tid];

            // search all items to the selected start Id
            if (deleteEntryTR.ReportLink__c == null)
            {
                items = boTimereport.giveTimeReportsByStart(deleteEntryTR.Id);
            }
            else
            {
                items = boTimereport.giveTimeReportsByStart(deleteEntryTR.ReportLink__c);
            }
                
            System.debug('#### deleteItem ITEMS: '+ items);
        }
        catch (Exception e)
        {
            // do nothing
            return null;
        }
        
        System.debug('#### deleteItem ITEMS size: '+ items.size());
        // if items have only one item, time report contains only day start
        if (items.size() <= 1)
        {
            System.debug('#### deleteItem dayDeleted: '+ dayDeleted);
            if (deleteEntryTR != null)
            {
                dayDeleted = deleteEntryTR.Start__c.format('dd.MM.yyyy');
            }    
            dayStartIsDeleted = true;
        }
        else if (items.size() > 1)
        {
            // save start Id
            currentStartId = deleteEntryTR.ReportLink__c;

            if (deleteEntryTR.Distance__c != null)
            {
                if (deleteEntryTR.Distance__c.intValue() > 0)
                {
                    entryHadDistance = true;
                }
            }
            Integer i = 0;
            Integer deleteListIndex = 0;
            // removes the item from the list
            for (SCTimereport__c timeRepItem: items)
            {
                if (timeRepItem.Id == deleteEntryTR.Id)
                {
                   //items.remove(i);
                   deleteListIndex = i;
                }
                else
                {
                    // set all status to open, if delete entry is day end
                    if (deleteEntryTR.Type__c == SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYEND)
                    {
                       timeRepItem.Status__c = SCfwConstants.DOMVAL_TIMEREPORTSTATUS_OPENED;
                    }
                }
                i++;
            }
            
            items.remove(deleteListIndex);
        } // else (items.size() == 1)
        
        // delete item from DB
        try
        {
            delete deleteEntryTR;
        }
        catch (DmlException e)
        {
            // Process exception here
        }

        // optimize mileage if entry had mileage
        if (entryHadDistance)
        {
            try
            {
                System.debug('MILEAGE upsert for ReportLink');
                boTimeReport.optimizeMileage(currentStartId);
            }
            catch (Exception e)
            {
                // Mileage upsert fail
                System.debug('MILEAGE upsert fail');
            }
        }
        
        /** 
         *  Call day start controller, if day start is deleted.
         */
        if (dayStartIsDeleted == true)
        { 
            callDayStartController();
        }
        else
        {
            // update time report entries in DB
            upsert items;
            // update the data structure
            // getitemsEvaluation ();
            if (deleteEntryTR.ReportLink__c != null)
            {            
                getItemsEvaluationStartId(deleteEntryTR.ReportLink__c);
            }
        }
        
        return null;
    } // deleteItem()

    /**
     * Select time report entry.
     *
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference selectItem()
    {
       //selectedTimeRepItem = ApexPages.currentPage().getParameters().get('selectedTimeRepItem');
       System.debug('#### selectItem(): selectedTimeRepItem -> ' + selectedTimeRepItem);

       for (timeReportEvaluation item :itemsEvaluation)
       {
           System.debug('timeReportEvaluation item ### is: '+item);
            if ((selectedTimeRepItem != null) && (item != null) &&
                 (selectedTimeRepItem.length() > 0) && (item.timeReportItem != null))
            {
                if (item.timeReportItem.Id == selectedTimeRepItem)
                {
                    System.debug('#### selectItem(): current Item found');
                    curTimeReport = item;
                }
            }
       }
       
       // for debug display
       if (itemsEvaluation.size() > 0)
       {
           System.debug('#### TYPE IS: '+
                  itemsEvaluation.get(itemsEvaluation.size()-1).timeReportItem.Type__c);
       }           
       
       return null;
    } //selectItem ()

    /**
     * Optimize of current time report.
     * All start times of time report items will be set to
     * the end of item before.
     *
     * @param startId start Id from time report
     *
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference optimize(String startId)
    {
        /** new method, current start = last item end AND
         *  current item end is current start plus "the same" duration
         */
        Integer currentMileage = 0;

        // if first item is not day begin search them
        if (items.get(0).Type__c != SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART)
        {
            for (Integer i = 0; i <= items.size()-1; i++)
            {
                if ((i > 0) && (items.get(i).Type__c == SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART))
                {
                    // day begin found, set it at start
                    items.get(i).Start__c = items.get(0).Start__c;
                    items.get(i).End__c = items.get(0).Start__c;
                    // set start mileage to first item mileage
                    if (items.get(0).Mileage__c != null)
                    {
                        items.get(i).Mileage__c = items.get(0).Mileage__c.intValue();
                    }
                    else
                    {
                        items.get(i).Mileage__c = 0;
                    }
                }
            }
        }

        if ((items != null) && (items.size() > 0))
        {
            if (items.get(0).Mileage__c != null)
            {
                currentMileage = items.get(0).Mileage__c.intValue();
            }
            else
            {
                currentMileage = 0;
            }
        }

        for (Integer i = 0; i <= items.size()-1; i++)
        {
            if (i > 0)
            {
                if (items.get(i).Start__c != items.get(i - 1).End__c)
                {
                    Integer currentDuration = items.get(i).Duration__c.intValue();
                    items.get(i).Start__c = items.get(i - 1).End__c;

                    items.get(i).End__c =
                    items.get(i).Start__c.addMinutes(currentDuration);
                }

                /**
                 * Update the mileage of current time report entry,
                 * if it is not the first item.
                 */
                try
                {
                  //if ((items.Id != items.get(0).Id)&&(!items.Distance__c.isEmpty()))
                        currentMileage = currentMileage +
                                         items.get(i).Distance__c.intValue();
                        items.get(i).Mileage__c = currentMileage;
                }
                catch (Exception e)
                {
                    // Distance__c is empty ...
                    items.get(i).Mileage__c = currentMileage;
                    items.get(i).Distance__c = 0;
                }
            }
        }
        try
        {
            upsert items;
        }
        catch (Exception e)
        {

        }
        // getitemsEvaluation must be updatet from here
        //getitemsEvaluation();
        getItemsEvaluationStartId(startId);
        //return checkDayOpen();
        return null;
    }

    /**
     * Create an end day entry in the working time report object.
     *
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference closeTimeReport()
    {
        System.debug('#### closeDay() with index: '+timeReportIndex);
        closeTimeReportError = false;
        
        //clear current time report list
        items.clear();
        //fill the list for selected time report
        List <TimeReportEvaluation> currentTimeReportEvaluation =
            multiTimeReport.get(timeReportIndex).timeReportEvaluation;

        System.debug('#### closeDay() currentTimeReportEvaluation: '+
                                      currentTimeReportEvaluation);

        for (TimeReportEvaluation timeReportEval :currentTimeReportEvaluation)
        {
             items.add(timeReportEval.timeReportItem);
        }
        System.debug('#### closeDay() items: '+
                                      items);

        // Time for time report end equals the last entry end
        Datetime dayEnd = items[(items.size() - 1)].End__c;
        // Day start ID for report link
        String dayStartId = items[0].Id;
        String insertRid = items[0].Resource__c;
        // Day start time for comparing with day end
        Datetime dayBegin = items[0].Start__c;

        //dayEnd = SCboTimereport.roundMinutes(dayEnd, 15);
        dayEnd = SCutilDatetime.roundMinutes(dayEnd);

        // before the day can be closed, optimize all entrys
        // optimize();

        // the mileage equals the last mileage entry
        Decimal lastMileage = items.get(items.size()-1).Mileage__c;
        
        // the last compensation type
        String lastCompensationType = items.get(items.size()-1).CompensationType__c;

        SCTimereport__c dayEndEntry = new SCTimereport__c(Start__c = dayEnd,
                                                   End__c = dayEnd,
                                                   ReportLink__c = dayStartId,
                                                   Resource__c = insertRid,
                                                   Type__c = SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYEND,
                                                   Status__c = SCfwConstants.DOMVAL_TIMEREPORTSTATUS_CLOSED,
                                                   CompensationType__c = lastCompensationType,
                                                   Mileage__c = lastMileage);

        // if end time equals start time, it is a error
        if (dayEndEntry.Start__c == dayBegin)
        {
            closeTimeReportError = true;
            // the new entry is before time report starts, error
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                                                       System.Label.SC_msg_EndEqualsStart));
            return null;
        }

        for (SCTimereport__c timeReportItem :items)
        {
             timeReportItem.Status__c = SCfwConstants.DOMVAL_TIMEREPORTSTATUS_CLOSED;
        }

        // for all time reports excluding day end set edit and delete to disabled
        for (timeReportEvaluation item :itemsEvaluation)
        {
            if (item.timeReportItem.Type__c != SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYEND)
            {
                item.enableEdit = false;
                item.enableDelete = false;
            }
        }

        try
        {
            upsert items; 
            insert dayEndEntry;
        }
        catch (Exception e)
        {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error during closing day!'));
                //return null;
        }    
        
        // no more close days for current day
        closeButtonDisabled = true;

        // call evaluation for selected start id
        getItemsEvaluationStartId(dayStartId);
        // call day start controller
        callDayStartController();
        return null;
    }
    
    
    /**
     * Calls the controller for SCDayStart.
     *
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public void callDayStartController ()
    {
        System.debug('#### pageController is: ' + pageController);
        // check for further open days
        if (pageController != null)
        {
            System.debug('HERE I AM, pageController' + pageController);

            refreshParent = true;
            // set flag for page controller (SCDayStart)
            pageControllerIsNotNull = true;
            
            System.debug('HERE I AM, refreshParent' + refreshParent);
            ((SCDayStartController)pageController).checkDayOpen();

            //return null;
        }
        else
        {
            System.debug('#### pageController is null: ' + pageController);
            System.debug('HERE I AM, refreshParent' + refreshParent);

            refreshParent = false;
            // set flag for page controller (SCDayStart)
            pageControllerIsNotNull = false;            
            //return null;
        }
        System.debug('#### pageControllerIsNotNull is: '+pageControllerIsNotNull);
    }
    
    
    /*
     * Read all time report for selected day and show it.
     */
    public List<timeReportEvaluation> getItemsEvaluationStartId(String startId)
    {
        // help variables for add button
        selectedTimeRepItem = ApexPages.currentPage().getParameters().get('selectedTimeRepItem');

        Boolean selectItemIntoList = false;
        closeButtonDisabled = false;
        addButtonDisabled = false;
        boolean dayEndExists = false;
        errorIntoTimeReport = false;
        String isNewEntry = null;

        SCboTimereport boTimereport = new SCboTimereport();
        itemsEvaluation = new List<timeReportEvaluation>();

        System.debug('getitemsEvaluation() startId is ####: '+startId);

        // time report entry was selected
        try
        {
            items = boTimereport.giveTimeReportsByStart(startId);
            System.debug('#### normal ITEMS: '+ items);
        }
        catch (Exception e)
        {
            // no open time reports found
            return null;
        }

        // Iterator for index into items
        integer i = 0;

        if ((items.size() > 0) &&
            (items.get(items.size()-1).Type__c == SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYEND))
        {
            dayEndExists = true;
        }
        
        // Strings for title
        if ((items != null) && (items.size() > 0))
        {
            dayDisplay =  items.get(0).Start__c.format('dd.MM.yyyy');
            if (items.get(0).Start__c.day() != items.get(items.size()-1).End__c.day())
            {
                dayDisplay = dayDisplay + ' - ' +
                             items.get(items.size()-1).End__c.format('dd.MM.yyyy');

            }
            employee = items.get(0).Employee__c;
            resource = items.get(0).Resource__r.Name;
        }
        else
        {
            dayDisplay = null;
            employee = null;
            resource = null;
        }
        
        // time report evaluation
        for (SCTimereport__c timeReportItem :items)
        {
            /**
             *  Test if selected item contains into
             *  current time report list.
             */
            if ((selectedTimeRepItem != null) && (selectedTimeRepItem.length() > 0))
            {
                String currentTRId = timeReportItem.Id;

                 System.debug('currentTRId #### :' + currentTRId);
                 System.debug('selectedTimeRepItem ####:' + selectedTimeRepItem);

                if (currentTRId == selectedTimeRepItem)
                {
                    selectItemIntoList = true;
                    System.debug('for (SCTimereport__c timeReportItem...####:'+selectItemIntoList);
                }
            }

            //Edit evaluation depend from type and order dependecy
            if (((timeReportItem.Type__c == SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART) &&
                !dayEndExists) ||
                ((timeReportItem.Order__c == null) && !dayEndExists))
            {
                enableEdit = true;
            }
            else
            {
                enableEdit = false;
            }

            //Delete evaluation depend from type and order dependecy
            // DH: todo: add evaluation dependent on custom settings for DAYEND
            if (((timeReportItem.Type__c != SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART) &&
                (timeReportItem.Order__c == null) && !dayEndExists))
                //|| (timeReportItem.Type__c == SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYEND))
            {
                enableDelete = true;
            }
            else
            {
                enableDelete = false;
            }
            
            /**
             * For time report list with only one entry
             * delete and edit button are enabled.
             */
            if ((timeReportItem.Type__c == SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART) && (items.size() == 1))
            {
                // the first entry should be the day start
                enableEdit = true;
                enableDelete = true;
                statusIcon = 'blank';
            }
            //Icon evaluation depend from type, start and end time from each item
            else if (timeReportItem.Type__c == SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART)
            {
                statusIcon = 'blank';
            }
            else
            {
                //next element index is (i + 1)
                i++;
                if (timeReportItem.Start__c == items.get(i - 1).End__c)
                {
                    statusIcon = 'ok';
                }
                else
                {
                    statusIcon = 'warning';
                    errorIntoTimeReport = true;

                    if (timeReportItem.Start__c > items.get(i - 1).End__c)
                    {
                        String startTimeStringNewItem = items.get(i - 1).End__c.hour()+':'+
                        items.get(i - 1).End__c.format('mm');

                        String endTimeStringNewItem = timeReportItem.Start__c.hour()+':'+
                                 timeReportItem.Start__c.format('mm');

                        // new time report with Id from last time report
                        // with that you can edit the new entry
                        SCTimereport__c newEntryTimeReport =
                                        new SCTimereport__c(Id = items.get(i - 1).Id);

                        // add new empty TimeReportItem for display
                        itemsEvaluation.add(new timeReportEvaluation(newEntryTimeReport, statusIcon,
                                                                     true, false, startTimeStringNewItem, 
                                                                     endTimeStringNewItem, 'timeReportNew',
                                                                     dayDisplay));

                        // Status again ok?
                        statusIcon = 'ok';
                        // set null for isNewEntry again
                        //isNewEntry = null;
                    }
                    else
                    {
                        try
                        {
                            itemsEvaluation.get(i-1).statusIcon = 'warning';
                        }
                        catch (Exception e)
                        {

                        }
                    }
                }
            } // else (timeReportItem.Type__c == SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART)

            String startTimeString = timeReportItem.Start__c.hour()+':'+
                                     timeReportItem.Start__c.format('mm');
            String endTimeString = timeReportItem.End__c.hour()+':'+
                                     timeReportItem.End__c.format('mm');
                                     
            itemsEvaluation.add(new timeReportEvaluation(timeReportItem, statusIcon, enableEdit,
                                                         enableDelete, startTimeString,
                                                         endTimeString, null, dayDisplay));

        } // for (SCTimereport__c timeReportItem :items)

        // close button disabled, if last item in the list is the end
        if (itemsEvaluation.size() > 0)
        {
            if (itemsEvaluation.get(itemsEvaluation.size()-1).timeReportItem.Type__c ==
                SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYEND)
            {
                 closeButtonDisabled = true;
            }
            else
            {
                 closeButtonDisabled = false;
            }
        }
        // addButton should be off by not selected item, if day exists and
        // if item is not in the time report list!
        if ((selectedTimeRepItem == null) || (!selectItemIntoList) || dayEndExists)
        {
            System.debug('### itemsEvaluation: item is null or not in the list '+selectItemIntoList);
            System.debug('### selectedTimeRepItem is '+selectedTimeRepItem);
            addButtonDisabled = true;
        }
        else
        {
            System.debug('### itemsEvaluation: item found selectItemIntoList '+selectItemIntoList);
            System.debug('### selectedTimeRepItem is '+selectedTimeRepItem);
            addButtonDisabled = false;
        }

        System.debug('closeButtonDisabled is ####:'+closeButtonDisabled);
        System.debug('addButtonDisabled is ####:'+addButtonDisabled );

        System.debug('getitemsEvaluation() itemsEvaluation is ####:'+itemsEvaluation);
        return itemsEvaluation;
    } // public List<timeReportEvaluation> getitemsEvaluation()

    /*
     * Read all time reports for selected day and show it.
     * Supports display of several time report begins.
     */
    public List<timeReportMulti> getMultiTimeReport()
    {
        timeReportStartIDs = new List <String> ();
        multiTimeReport = new List <timeReportMulti> ();

        if ((dayStartListString != null) && (dayStartListString.length() > 0))
        {
            timeReportStartIDs = dayStartListString.split(',');
        }
        else
        {
            String currentDay = dayDeleted;
            multiTimeReport.add(new timeReportMulti (null,
                                true,
                                true,
                                false,
                                false,
                                0, currentDay));
            
            return multiTimeReport;
        }

        System.debug('### getMultiTimeReport timeReportStartIDs :'+
                                             timeReportStartIDs);

        if (timeReportStartIDs != null)
        {
            Integer index = 0;

            for (String startId :timeReportStartIDs)
            {
                List <timeReportEvaluation> itemsEvaluationForId =
                    getItemsEvaluationStartId(startId);
                
                String currentDay = null;
                
                // add the day for display
                if (itemsEvaluationForId.size() != 0)
                {
                    currentDay = itemsEvaluationForId.get(0).dayDisplay;
                }
                else
                {
                    currentDay = dayDeleted;
                }
                
                System.debug('### getMultiTimeReport itemsEvaluationForId :' +
                                                     itemsEvaluationForId);

                // add current time report evaluation to multi time report map
                multiTimeReport.add(new timeReportMulti (itemsEvaluationForId,
                                    addButtonDisabled,
                                    closeButtonDisabled,
                                    dayEndExists,
                                    errorIntoTimeReport,
                                    index, currentDay));
                index++;
            }
        }
        System.debug('### getMultiTimeReport() multiTimeReport: '+multiTimeReport);
        return multiTimeReport;
    }

    // Data Container for time report evaluation
    public class TimeReportEvaluation
    {
        public boolean enableEdit { get; set; }
        public boolean enableDelete { get; set; }
        public String statusIcon { get; set; }
        public SCTimeReport__c timeReportItem { get; set; }
        public String startTime { get; set; }
        public String endTime { get; set; }
        public String isNewEntry { get; set; }
        public String dayDisplay { get; set; }

        timeReportEvaluation(SCTimeReport__c timeReportItem,
                             String statusIcon, boolean enableEdit,
                             boolean enableDelete, String startTime,
                             String endTime, String isNewEntry,
                             String dayDisplay)
        {
            this.statusIcon = statusIcon;
            this.enableEdit = enableEdit;
            this.enableDelete  = enableDelete;
            this.timeReportItem = timeReportItem;
            this.startTime  = startTime;
            this.endTime  = endTime;
            this.isNewEntry = isNewEntry;
            this.dayDisplay = dayDisplay;
        }
    }

    // Data Container for multi time reports
    public class TimeReportMulti
    {
        //public String timeReportStartId { get; set; }
        public List <timeReportEvaluation> timeReportEvaluation { get; set; }
        public boolean addButtonDisabled { get; set; }
        public boolean closeButtonDisabled { get; set; }
        public boolean dayEndExists { get; set; }
        public boolean errorIntoTimeReport { get; set; }
        public Integer index { get; set; }
        public String currentDay { get; set; }

        timeReportMulti(List <timeReportEvaluation> timeReportEvaluation,
                        boolean addButtonDisabled, boolean closeButtonDisabled,
                        boolean dayEndExists, boolean errorIntoTimeReport, 
                        Integer index, String currentDay)
        {
            this.timeReportEvaluation = timeReportEvaluation;
            this.addButtonDisabled = addButtonDisabled;
            this.closeButtonDisabled  = closeButtonDisabled;
            this.dayEndExists = dayEndExists;
            this.errorIntoTimeReport  = errorIntoTimeReport;
            this.index  = index;
            this.currentDay = currentDay;
        }
    }
    
    public void setAllOkTrue()
    {
        ((SCDayStartController)pageController).setAllOkTrue();
        callDayStartController();
    }
}
