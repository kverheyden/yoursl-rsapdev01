/**********************************************************************
Name:  CCWSAddChatterFeeditem 
======================================================
Purpose:                                                            
Represents a webservice. Receives Postinformation, create a new FeedItem and ContenVersion with the given Info and inserts it to the database.                                                      
 the sUserId has to be changed to sUserExtId.
======================================================
History                                                            
-------                                                            
Date  	AUTHOR	DETAIL  
11/26/2013 Chris Sandra Schautt	<c.schautt@yoursl.de>           
***********************************************************************/


global with sharing class CCWSAddChatterFeeditem {
	
	WebService static OperationResult addFeed( String sParentType, String sParentExtId, String sUserId, String sBody, String sDocName, Blob sDocBody){
		
		//required local variables
		List<Account> listAccounts = new List<Account>(); // accounts with the given id
		List<Promotion__c> listPromotions = new List<Promotion__c>(); // accounts with the given id
		List<User> listUsers = new List<User>(); //users with the given id
		OperationResult objOpResult = new OperationResult();
		// String to hold the original User-ID of Salesforce.com
		String sUserSFDCId = '';
		ContentVersion  objContentVersion;
		
		//Query User and validate result
		if(sUserId!=null && sUserId!=''){
		try{
			listUsers = [SELECT Name, IsActive, Id, ID2__c FROM User WHERE ID2__c = : sUserId]; 
		}catch (QueryException e){
			objOpResult.addError(e.getMessage());
			return objOpResult;
		}
		// check, if list is empty
		if(listUsers.size()==0){
			objOpResult.addError('No User found for ID2 '+sUserId+'.');
			return objOpResult;
		}
		// Check, if we found to much Users
		else if(listUsers.size()>1){
			objOpResult.addError('Found '+listUsers.size()+' Users for Id2 '+sUserId+'.');
			return objOpResult;
		}else{
			sUserSFDCId = listUsers[0].Id;
			objOpResult.addUser(sUserSFDCId);
		}
	}// end if(sUserId!=null && sUserId!='')
		
		
	// String to hold the original Account-ID of Salesforce.com
	String sParentSFId = '';
		
	//+++++++++++++++++ STEP 2.1: Validate Account ++++++++++++++++++++++++++++++++++++
	if(sParentType=='Account'){
		//Query Account and validate result
		try{
			listAccounts = [SELECT Name, Id, ID2__c FROM Account WHERE ID2__c = : sParentExtId];
		}catch (QueryException e){
			objOpResult.addError(e.getMessage());
			return objOpResult;
		}
		// check, if list is empty
		if(listAccounts.size()==0){
			objOpResult.addError('No Account found for ID2 '+ sParentExtId +'.');
			return objOpResult;
		}
		// Check, if there are to many Accounts
		else if(listAccounts.size()>1){
			objOpResult.addError('Found '+listAccounts.size()+' Accounts for ID2 '+ sParentExtId +'.');
			return objOpResult;
		}
		else{
			sParentSFId = listAccounts[0].Id;
			objOpResult.addSObject(sParentSFId);
		}	
	}// End if(sParentType=='Account')
	
	//+++++++++++++++++ STEP 2.2: Validate Promotion ++++++++++++++++++++++++++++++++++++
	else if(sParentType =='Promotion'){
		//Query Promotion and validate result
		try{
			listPromotions = [SELECT Name, Id, PromotionID__c FROM Promotion__c WHERE PromotionID__c = : sParentExtId];
		}catch (QueryException e){
			objOpResult.addError(e.getMessage());
			return objOpResult;
		}
		// check, if list is empty
		if(listPromotions.size()==0){
			objOpResult.addError('No Promotion found for PromotionID '+sParentExtId+'.');
			return objOpResult;
		}
		// Check, if there are to many Promotions
		else if(listPromotions.size()>1){
			objOpResult.addError('Found '+listPromotions.size()+' Promotion for PromotionID '+sParentExtId+'.');
			return objOpResult;
		}
		else{
			sParentSFId = listPromotions[0].Id;
			objOpResult.addSObject(sParentSFId);
		}
		//+++++++++++++++++ STEP 3: Validate sDocBody and sDocName ++++++++++++++++++++++++++++++++++++		
		if(sDocName!=null&& sDocName!='' && sDocBody!= null){
				SalesAppSettings__c [] objSalesAppSetting = new List<SalesAppSettings__c> ();
				try{
					objSalesAppSetting = [SELECT Id, Name, Value__c FROM SalesAppSettings__c WHERE (Name =:'PromotionLibraryId') OR(Name=:'SalesFolderRecordTypeId') ];
				}
				catch (QueryException e){
						objOpResult.addError(e.getMessage());
						return objOpResult;
				}	
				// check, if list is empty
				if(objSalesAppSetting.size()==0){
					objOpResult.addError('No SalesAppSetting found for the name PromotionLibraryId or SalesFolderRecordTypeId.');
					return objOpResult;
				}
				// Check, if there are to many entries
				else if(listPromotions.size()>2){
					objOpResult.addError('Found '+ objSalesAppSetting.size() +' SalesAppSettings with the name PromotionLibraryId or SalesFolderRecordTypeId.');
					return objOpResult;
				}
				objSalesAppSetting.sort();
				//+++++++++++++++++ STEP 4: Create the ContenVersion ++++++++++++++++++++++++++++++++++++
				objContentVersion= new ContentVersion(Title=sDocName , OwnerId= sUserSFDCId);
				objContentVersion.FirstPublishLocationId = objSalesAppSetting[0].Value__c;
				// objContentVersion.FirstPublishLocationId = sParentSFId;
				// convert the String into an base64
				//Blob bContenVersionData = EncodingUtil.base64Decode(sDocBody);
				
				objContentVersion.VersionData 		= sDocBody;
				objContentVersion.PathOnClient 		= sDocName;
				objContentVersion.RecordTypeId 		= objSalesAppSetting[1].Value__c;
				objContentVersion.PromotionsId2__c 	= sParentExtId;
				system.debug('###Content Version: '+objContentVersion);
				
				
				// the ContenURL or the PathOnClient must be set 
				//objContent.ContentUrl = 'https://c.na14.content.force.com/servlet/servlet.ImageServer?id=015d00000014L3G&oid=00Dd0000000d6Ea&lastMod=1385466328000';
				try{
						insert objContentVersion;
				}catch (DmlException e){
					objOpResult.addError(e.getMessage());
					return objOpResult;
				}
			}// end if(sDocName!=null&& sDocName!='' && sDocBody== null)
		
	}// end if(sParentType =='Promotion')
	else{
		objOpResult.addError('the parenttype is not a Promotion or Account! ');
		return objOpResult;
			//listPromotions[0].p
	}// end else
	//+++++++++++++++++ STEP 5: Create the Chatter Post ++++++++++++++++++++++++++++++++++++
				
		//set required fields
		FeedItem objFeedItem = new FeedItem();
		objFeedItem.ParentId = sParentSFId ;
		if(sUserSFDCId != ''){
			objFeedItem.CreatedById = sUserSFDCId;
		}else{
			objFeedItem.CreatedById = System.Userinfo.getUserId();
		}
		String sfullFileURL= '';
		// only if a ContenVersion is set.
		if(objContentVersion!=null && objContentVersion.Id != null){
			objFeedItem.RelatedRecordId = objContentVersion.Id;
			sfullFileURL = 'Link to the Owner: '+ URL.getSalesforceBaseUrl().toExternalForm() +
	   		'/' + objContentVersion.OwnerId +' Link to the ContenDokument: '+ URL.getSalesforceBaseUrl().toExternalForm() +
	   		'/' + objContentVersion.Id +' .';
	   		if(sBody!=null){
	   			objFeedItem.Body = sBody+' '+ sfullFileURL;
	   		}else{
	   			objFeedItem.Body = sfullFileURL;
	   		}	
		}
		else if(sBody!= null&&sBody !=''){
			objFeedItem.Body = sBody;
		}else{
			objOpResult.addError('The body of the Chatterpost is empty. ');
			return objOpResult;
		}
		// insert the feed
		try{
			insert objFeedItem;
		}
		catch (DmlException e){
			objOpResult.addError(e.getMessage());
			return objOpResult;
		}
		// return value if success
		objOpResult.bSuccess = true;
		return objOpResult;	
	}
	
	/************************************************************************
	**
	**	Class to prepare and return a reult to the caller
	**
	************************************************************************/
	global class OperationResult{
		
		// Class variables
		webservice String MessageUUID{get;set;} 
		webservice String sSObjectID{get;set;}
		webservice String sUserID{get;set;}
		webservice String sError{get;set;}
		webservice Boolean bSuccess{get;set;}
		
		// Constructor
		private OperationResult(){
			// MessageUUID='5257AA3FC97C0FE0E10080000A621094'; 
		}
		
		public boolean addUser(String sUserID){
			this.sUserID = sUserID;
			return true;
		}
		
		public boolean addSObject(String sSObjektID){
			this.sSObjectID = sSObjektID;
			return true;
		}
		
		public boolean addError(String sError){
			this.sError = sError;
			this.bSuccess = false;
			return true;
		}
	}// end global class OperationResult

}
