public with sharing class RequestAfterInsertTriggerHandler extends TriggerHandlerBase {
	
	public override void mainEntry(TriggerParameters tp) {
		RequestAfterInsertCLS RAIC = new RequestAfterInsertCLS();
		RAIC.prepareRequestAccountUpdate((List<Request__c>)tp.newList);
	}
}
