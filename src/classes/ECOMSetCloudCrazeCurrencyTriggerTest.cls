@isTest(SeeAllData=true)
public class ECOMSetCloudCrazeCurrencyTriggerTest {
	static testMethod void testTrigger()
    {
        Profile profile = [SELECT Id FROM Profile WHERE Name='CloudCraze Customer Community User'];
        User currentUser = [SELECT Id From User WHERE Id=:UserInfo.getUserId()];
        
        Account a = new Account(Name='Account');
        insert a;
        
        Contact c = new Contact(AccountId=a.Id,LastName='Lastname');
        insert c;
        
        User u = new User(Username='user@example.org', 
                          LastName='Lastname', 
                          Email='user@example.org',
                          Alias='alias', 
                          CommunityNickname='nickname', 
                          TimeZoneSidKey = 'Europe/Berlin', 
                          LocaleSidKey='de', 
                          EmailEncodingKey='UTF-8', 
                          ProfileId=profile.Id, 
                          LanguageLocaleKey='de',
                          ContactId=c.Id);
        
        Test.startTest();
        System.runAs(currentUser) {
            insert u;
        }
        Test.stopTest();
        
        u = [SELECT Id,ccrz__CC_CurrencyCode__c From User WHERE Id=:u.Id];
        System.assertEquals('EUR', u.ccrz__CC_CurrencyCode__c);
    }
}
