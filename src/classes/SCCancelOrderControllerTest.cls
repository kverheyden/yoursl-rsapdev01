/*
 * @(#)SCCancelOrderControllerTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Test class checks the controler functions for the order cancel
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCCancelOrderControllerTest
{
    private static SCCancelOrderController coc;

    static testMethod void cancelOrderControllerPositiv1() 
    {
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet(true);

        SCHelperTestClass.order.type__C = '5705';
        SCHelperTestClass.order.status__c = SCfwConstants.DOMVAL_ORDERSTATUS_OPEN;
        update SCHelperTestClass.order;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(SCHelperTestClass.order);
        
        coc = new SCCancelOrderController(sc);
        coc.testcase = true; //suppress web service execution
  
        coc.closeCancellation();
        coc.order.CancelReason__c = '286001';
        //coc.order.Info__c = 'Cancelled text.';
        coc.cancellable = true;
        coc.cancelOrder(); 

    }
    
    static testMethod void cancelOrderControllerNegativ1() 
    {
        //SCHelperTestClass3.createCustomSettings('de',true);
        
        // Creating order with status 'cancelled'
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();   
        SCOrder__c order = new SCOrder__c
                      (
                        CustomerPrefStart__c = Date.today(),
                        CustomerPrefEnd__c = Date.today()+1,
                        CustomerTimewindow__c = SCfwConstants.DOMVAL_CUSTOMERTIMEWINDOW_DEFAULT,
                        CustomerPriority__c = SCfwConstants.DOMVAL_ORDERPRIO_DEFAULT
                      );

        // set default values that are global
        order.Status__c = SCfwConstants.DOMVAL_ORDERSTATUS_CANCELLED;
        
        order.Type__c = appSettings.DEFAULT_ORDERTYPE__c;
        
        insert order;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(order);
  
        coc = new SCCancelOrderController(sc);
        coc.testcase = true; //suppress web service execution
  
        coc.closeCancellation();
        coc.order.CancelReason__c = '286001';
        //coc.order.Info__c = 'Cancelled text.';
        coc.cancelOrder();
        
    }
    
}
