/*
 * @(#)SCSendMessageController.cls 
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Test class checks the controler functions for SMS sending.
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCSendMessageControllerTest
{

    private static SCSendMessageController smc;

    static testMethod void sendMessageControllerPositiv1() 
    {
        Test.startTest();  
        
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.createOrderTestSet(true);    
        //SCHelperTestClass.createTestUsers(true);
        
        ApexPages.currentPage().getParameters().put('oid', SCHelperTestClass.order.Id); 
        
        
        // Creating Profile
        Profile profile = [select Id from Profile where UserType = 'Standard' Limit 1];
        
        // Creating User
        User user = new User();
        user.FirstName = 'John';
        user.LastName = 'Doe';
        user.MobilePhone = '12345';
        user.Email = 'test@test.com';
        user.Username = 'test@megatest.com';
        user.Alias = 'gmstest';
        user.CommunityNickname = 'me';
        user.TimeZoneSidKey = 'Europe/Berlin';
        user.LocaleSidKey = 'de_DE_EURO';
        user.EmailEncodingKey = 'UTF-8';
        user.ProfileId = profile.id;
        user.LanguageLocaleKey = 'en_US';
        insert user;
        
        
        // Creating Resource
        SCResource__c resource = new SCResource__c();
        resource.Alias_txt__c = SCHelperTestClass.createResAlias(user);
        resource.Employee__c = user.id;
        insert resource;

        // Creating Resource Assignment
        SCAssignment__c assignment = new SCAssignment__c();
        assignment.Resource__c = resource.id;
        assignment.Order__c = SCHelperTestClass.order.Id;
        assignment.Status__c = '5509';
        insert assignment;    
        
        smc = new SCSendMessageController();
        
        smc.messageText = 'SMS Text';
        smc.order = SCHelperTestClass.order;
        smc.resource = resource;
        
        // smc.msg.onMessageSend(SCHelperTestClass.order.Id, smc.messageText, resource.id);

        smc.onSendMessage();
        
        
        Test.stopTest();
    }
    
    /*
    static testMethod void sendMessageControllerPositiv2() 
    {
    
        Test.startTest();  
        
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.createOrderTestSet(true);     
        //SCHelperTestClass.createTestUsers(true);
        
        ApexPages.currentPage().getParameters().put('oid', SCHelperTestClass.order.Id); 
        
        
        // Creating Profile
        Profile profile = [select Id from Profile where UserType = 'Standard' Limit 1];
        
        // Creating User
        User user = new User();
        user.FirstName = 'John';
        user.LastName = 'Doe';
        user.MobilePhone = '12345';
        user.Email = 'test@test.com';
        user.Username = 'test@megatest.com';
        user.Alias = 'gmstest';
        user.CommunityNickname = 'me';
        user.TimeZoneSidKey = 'Europe/Berlin';
        user.LocaleSidKey = 'de_DE_EURO';
        user.EmailEncodingKey = 'UTF-8';
        user.ProfileId = profile.id;
        user.LanguageLocaleKey = 'en_US';
        insert user;
        
        
        // Creating Resource
        SCResource__c resource = new SCResource__c();
        resource.Employee__c = user.id;
        insert resource;
        
        ApexPages.currentPage().getParameters().put('rid',resource.id);

        // Creating Resource Assignment
        SCAssignment__c assignment = new SCAssignment__c();
        assignment.Resource__c = resource.id;
        assignment.Order__c = SCHelperTestClass.order.Id;
        assignment.Status__c = '5509';
        insert assignment;    
        
        smc = new SCSendMessageController();
        
        smc.messageText = 'SMS Text';
        smc.order = SCHelperTestClass.order;
        //smc.resource = resource;
        
        // smc.msg.onMessageSend(SCHelperTestClass.order.Id, smc.messageText, resource.id);

        smc.onSendMessage();
        
        
        Test.stopTest();
    }
    */

}
