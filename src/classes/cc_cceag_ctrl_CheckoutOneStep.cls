global without sharing class cc_cceag_ctrl_CheckoutOneStep {
    public cc_cceag_ctrl_CheckoutOneStep() {
        
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult fetchAccountParams(ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
        result.inputContext = ctx;
        result.success = false; 
        try {
            String userId = (String.isBlank(ctx.portalUserId)) ? UserInfo.getUserId() : ctx.portalUserId;
            ccrz__E_Cart__c cart = [SELECT ccrz__ShipTo__r.ccrz__Partner_Id__c FROM ccrz__E_Cart__c WHERE ccrz__EncryptedId__c = : ctx.currentCartID LIMIT 1];
            Account shipTo = [SELECT CustomerOrderNumberRequired__c, CustomerOrderNumberRequiredLength__c FROM Account WHERE AccountNumber =: cart.ccrz__ShipTo__r.ccrz__Partner_Id__c LIMIT 1];
            SCAccountInfo__c info = cc_cceag_dao_Account.fetchSCAccountInfo(shipTo.Id, false);
            Map<String, Object> retData = new Map<String, Object>();
            retData.put('orderIdRequired', shipTo.CustomerOrderNumberRequired__c);
            retData.put('orderIdLength', shipTo.CustomerOrderNumberRequiredLength__c);
            if (info != null)
                retData.put('paymentType', info.CustomerPaymentType__c);
            result.data = retData;
            result.success = true;
        } catch(Exception e) {
            System.debug(LoggingLevel.INFO, e.getStackTraceString());
            ccrz.cc_bean_Message msg = new ccrz.cc_bean_Message();
            msg.type = ccrz.cc_bean_Message.MessageType.CUSTOM;
            msg.classToAppend = 'messagingSection-Error';
            msg.message = e.getStackTraceString();
            msg.severity = ccrz.cc_bean_Message.MessageSeverity.ERROR;
            result.messages.add(msg);
        }
        return result;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult save(ccrz.cc_RemoteActionContext ctx, String encryOrderId, String orderId, String note, String deliveryTime, Boolean newsletter, String paymentMethod) {
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
        result.inputContext = ctx;
        result.success = false; 
        try {
            ccrz__E_Order__c orderObj = cc_cceag_dao_Cart.getOrderByEncId(encryOrderId);
            orderObj.ccrz__PONumber__c = orderId;
            orderObj.CCEAGDeliveryTime__c = deliveryTime;
            orderObj.CCEAGNewsletter__c = newsletter;
            orderObj.ccrz__Note__c = note;
            orderObj.ccrz__PaymentMethod__c = paymentMethod;
            //orderObj.ccrz__OrderStatus__c = 'In Bearbeitung';
            update orderObj;
            Id contactId = ccrz.cc_CallContext.currContact.Id;
            Contact c = [SELECT productnewsletter__c, promotionnewsletter__c FROM Contact WHERE Id =: contactId LIMIT 1];
            c.productnewsletter__c = newsletter;
            c.promotionnewsletter__c = newsletter;
            update c;
            result.data = true;
            result.success = true;
        } catch(Exception e) {
            ccrz.cc_bean_Message msg = new ccrz.cc_bean_Message();
            msg.type = ccrz.cc_bean_Message.MessageType.CUSTOM;
            msg.classToAppend = 'messagingSection-Error';
            msg.message = e.getStackTraceString();
            msg.severity = ccrz.cc_bean_Message.MessageSeverity.ERROR;
            result.messages.add(msg);
        }
        return result;
    }


}
