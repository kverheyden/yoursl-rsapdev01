@isTest
private class cc_cceag_ctrl_test_ContactInfo {
	@isTest static void testContactInfo() {
		insert new ecomSettings__c(Name='editContactDataEmailRecipient', Value__c='test@cceagtestemail2312123.com');
		insert new ecomSettings__c(Name='systemEmailAddress', Value__c='testsys@cceagtestemail2312123.com');

		Account testAccount = cc_cceag_test_TestUtils.createAccount(true);
		User testUser = cc_cceag_test_TestUtils.getPortalUser(testAccount);
		insert testUser;

    	ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
    	ctx.portalUserId = testUser.Id;

    	Test.startTest();
    	cc_cceag_ctrl_ContactInfo controller = new cc_cceag_ctrl_ContactInfo();

		ccrz.cc_RemoteActionResult result = cc_cceag_ctrl_ContactInfo.loadUserData(ctx);
		System.assertNotEquals(null, result);
		System.assert(result.success);

		Map<String, String> accountData = controller.accountData;
		System.assertNotEquals(null, accountData);

		result = cc_cceag_ctrl_ContactInfo.saveData(ctx, accountData);
		System.assertNotEquals(null, result);
		System.assert(result.success);

		result = cc_cceag_ctrl_ContactInfo.fetchAccountNumber(ctx, testAccount.Id);
		System.assertNotEquals(null, result);
		System.assert(result.success);

		Test.stopTest();
	}

	/**
	 * test exception scenarios
	 */
	@isTest static void testContactInfoExceptions() {
		insert new ecomSettings__c(Name='editContactDataEmailRecipient', Value__c='test@cceagtestemail2312123.com');

    	ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();

		ccrz.cc_RemoteActionResult result = cc_cceag_ctrl_ContactInfo.saveData(ctx, null);
		System.assertNotEquals(null, result);
		System.assertEquals(false, result.success);

		result = cc_cceag_ctrl_ContactInfo.fetchAccountNumber(ctx, 'invalidaccountid');
		System.assertNotEquals(null, result);
		System.assertEquals(false, result.success);
	}
}
