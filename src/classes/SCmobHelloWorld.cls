/*
 * @(#)SCmobHelloWorld.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Implements an example for a 'mobile transaction' to be used in conjunction
 * with WSMobileTransaction. See WSMobileTransaction for further details.
 *
 * @author mautenrieth@gms-online.de
*/ 

global class SCmobHelloWorld
{
        public class Input // Container for Input Data
        {
            public String   name {get; set;}
        }

        public class Output // Container for Output Data
        {
            public String message {get; set;}
        }
        
       /**
        * Contains the BusinessLogic for the HelloWorld transaction example.
        *
        * Generates an 'Output' object for each 'Input' Object given, containing
        * a personalized greeting message, using 'Input.name'.
        *
        * If no 'Input' is given it returns one 'Output' containing
        * 'Hello World!' as greeting message.
        */
        public static SCmobResult exec(List<Input> input)
        {
            SCmobResult res = new SCmobResult('E000');
        
            if (input.size() > 0)
            {
            
                for(Input data : input)
                {
                    Output o = new Output();
                    o.message = 'Hello ' + data.name + '!';
                    res.data.add(o);
                }
            }
            else
            {
                    Output o = new Output();
                    o.message = 'Hello World!';
                    res.data.add(o);
            }
                               
            return res;
        }       
}
