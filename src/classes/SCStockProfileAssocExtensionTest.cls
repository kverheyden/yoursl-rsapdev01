/*
 * @(#)SCStockProfileAssocExtensionTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 * <p>
 * This class contains unit tests for managing stock profil assocs.
 * A detailed test for managing assocs will be done in 
 * SCTestMatMoveBaseExtention.cls
 * <p>
 * A complete testing environment is created first. Remember that all data
 * created in a Unit test is not commited to the database!!
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */

@isTest
private class SCStockProfileAssocExtensionTest
{
    /**
     * testStockProfileAssocExtension
     * ==============================
     *
     * Test the extension class for managing stock profil assocs.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    static testMethod void testStockProfileAssocExtension()
    {
        System.debug('########################################################');
        System.debug('#### Starting Test:  testStockProfileAssocExtension()');
        System.debug('########################################################');

        // create all neccessary datas
        Id plantId = SCHelperTestClass4.createTestPlant();
        List<SCStock__c> stocks = SCHelperTestClass4.createTestStocks(plantId);
        List<SCArticle__c> articleList = SCHelperTestClass4.createTestArticles();
        List<SCStockProfile__c> profiles = SCHelperTestClass4.createTestStockProfiles();
        SCHelperTestClass4.createTestStockProfileAssocs(stocks.get(0).Id, stocks.get(1).Id,
                                                      profiles.get(0).Id, profiles.get(1).Id);
        
        Test.startTest();
        
        SCStockProfileAssocExtension stockProfAssocExt = 
                     new SCStockProfileAssocExtension(new ApexPages.StandardController(stocks.get(0)));
        
        // a detailed test for managing assocs will be done in SCTestMatMoveBaseExtention.cls
        stockProfAssocExt.saveAssocs();
        System.assert(stockProfAssocExt.savingOk);

        Test.stopTest();
    } // testStockProfileAssocExtension
} // SCStockProfileAssocExtensionTest
