/*
 * @(#)SCInterfaceBase.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * 
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
public virtual class SCInterfaceBase 
{ 
     public static SCInterfaceLog__c fillInterfaceLog(String interfaceName, 
                            String interfaceHandler,
                            String type,      // OUTBOUND
                            String direction, // outbound
                            String messageID,
                            ID referenceID, 
                            String refType,
                            ID referenceID2,
                            String refType2, 
                            ID responseID,
                            String resultCode, 
                            String resultInfo, 
                            String data, 
                            Datetime start, 
                            Integer count)
    {
        SCInterfaceLog__c ic = new SCInterfaceLog__c();
        ic.Interface__c = interfaceName;
        ic.InterfaceHandler__c = interfaceHandler;
        ic.Type__c = type;
        ic.MessageID__c = messageID;
        ic.Count__c = count;
        ic.ReferenceID__c = referenceID;
        ic.ReferenceID2__c = referenceID2;
        ic.Response__c = responseID;
        ic.ResultCode__c = resultCode;
        ic.ResultInfo__c = resultInfo;
        
        if(data.length() < 32000)
        {
        	ic.Data__c = data;
        }
        else
        {
        	ic.Data__c = data.left(32000);
        }
        ic.Direction__c = direction;
        ic.Start__c = start;
        ic.Stop__c = Datetime.now();
        Long duration = ic.Stop__c.getTime() - ic.Start__c.getTime();
        ic.Duration__c = duration; 
        if(refType != null && refType == 'SCContract__c')
        {
            ic.Contract__c = referenceID;
        }    
        if(refType2 != null && refType2 == 'SCContract__c')
        {
            ic.Contract__c = referenceID2;
        }    
        
        if(refType != null && refType == 'SCOrder__c')
        {
            ic.Order__c = referenceID;
        }    
        if(refType2 != null && refType2 == 'SCOrder__c')
        {
            ic.Order__c = referenceID2;
        }    
        if(refType != null && refType == 'SCInventory__c')
        {
            ic.Inventory__c = referenceID;
        }    
        if(refType2 != null && refType2 == 'SCInventory__c')
        {
            ic.Inventory__c = referenceID2;
        }    
        if(refType != null && refType == 'SCMaterialReplenishment__c')
        {
            ic.Replenishment__c = referenceID;
        }    
        if(refType2 != null && refType2 == 'SCMaterialReplenishment__c')
        {
            ic.Replenishment__c = referenceID2;
        }    
        if(refType != null && refType == 'SCStock__c')
        {
            ic.Stock__c = referenceID;
        }    
        if(refType2 != null && refType2 == 'SCStock__c')
        {
            ic.Stock__c = referenceID2;
        }    
        // 23.01.2013 required for SparepartPackageReceived
        if(refType != null && refType == 'SCMaterialMovement__c')
        {
            ic.MaterialMovement__c = referenceID;
        }    
        if(refType2 != null && refType2 == 'SCMaterialMovement__c')
        {
            ic.MaterialMovement__c = referenceID2;
        }    
        
        
        


        return ic;
    }    

     public static List<SCInterfaceLog__c> fillInterfaceLogs(String interfaceName, 
                            String interfaceHandler,
                            String type,      // OUTBOUND
                            String direction, // outbound
                            String messageID,
                            ID responseID,
                            String resultCode, 
                            String resultInfo, 
                            Datetime start, 
                            List<InterfaceLogPart> ilpList)
    {
        List<SCInterfaceLog__c> retValue = new List<SCInterfaceLog__c>();
        for(InterfaceLogPart ilp: ilpList)
        {
            SCInterfaceLog__c ic = new SCInterfaceLog__c();
            ic.Interface__c = interfaceName;
            ic.InterfaceHandler__c = interfaceHandler;
            ic.Type__c = type;
            ic.MessageID__c = messageID;
            ic.ReferenceID__c = ilp.referenceID;
            ic.ReferenceID2__c = ilp.referenceID2;
            ic.Response__c = responseID;
            ic.ResultCode__c = resultCode;
            ic.ResultInfo__c = resultInfo;
            ic.Direction__c = direction;
            ic.Start__c = start;
            ic.Stop__c = Datetime.now();
            Long duration = ic.Stop__c.getTime() - ic.Start__c.getTime();
            ic.Duration__c = duration; 
            ic.order__c = ilp.order;
            ic.replenishment__c =  ilp.replenishment;
            
            
            if(ilp.refType != null && ilp.refType == 'SCContract__c')
            {
                ic.Contract__c = ilp.referenceID;
            }    
            if(ilp.refType2 != null && ilp.refType2 == 'SCContract__c')
            {
                ic.Contract__c = ilp.referenceID2;
            }               
            if(ilp.refType != null && ilp.refType == 'SCOrder__c')
            {
                ic.Order__c = ilp.referenceID;
            }    
            if(ilp.refType2 != null && ilp.refType2 == 'SCOrder__c')
            {
                ic.Order__c = ilp.referenceID2;
            }    
            if(ilp.refType != null && ilp.refType == 'SCInventory__c')
            {
                ic.Inventory__c = ilp.referenceID;
            }    
            if(ilp.refType2 != null && ilp.refType2 == 'SCInventory__c')
            {
                ic.Inventory__c = ilp.referenceID2;
            }    
            if(ilp.refType != null && ilp.refType == 'SCMaterialReplenishment__c')
            {
                ic.Replenishment__c = ilp.referenceID;
            }    
            if(ilp.refType2 != null && ilp.refType2 == 'SCMaterialReplenishment__c')
            {
                ic.Replenishment__c = ilp.referenceID2;
            }    
            if(ilp.refType != null && ilp.refType == 'SCStock__c')
            {
                ic.Stock__c = ilp.referenceID;
            }    
            if(ilp.refType2 != null && ilp.refType2 == 'SCStock__c')
            {
                ic.Stock__c = ilp.referenceID2;
            }    
            // 23.01.2013 required for SparepartPackageReceived
            if(ilp.refType != null && ilp.refType == 'SCMaterialMovement__c')
            {
                ic.MaterialMovement__c = ilp.referenceID;
            }    
            if(ilp.refType2 != null && ilp.refType2 == 'SCMaterialMovement__c')
            {
                ic.MaterialMovement__c = ilp.referenceID2;
            }    

            ic.Count__c = ilp.count;
            //ic.Data__c = ilp.data;
            if(ilp.data.length() < 32000)
	        {
	        	ic.Data__c = ilp.data;
	        }
	        else
	        {
	        	ic.Data__c = ilp.data.left(32000);
	        }
        
            retValue.add(ic);
        }    
        return retValue;
    }    


     public static SCInterfaceLog__c writeInterfaceLog(String interfaceName, 
                            String interfaceHandler,
                            String type,      // OUTBOUND
                            String direction, // outbound
                            String messageID,
                            ID referenceID, 
                            String refType,
                            ID referenceID2,
                            String refType2, 
                            ID responseID,
                            String resultCode, 
                            String resultInfo, 
                            String data, 
                            Datetime start, 
                            Integer count)
    {
        SCInterfaceLog__c ic = fillInterfaceLog(interfaceName, interfaceHandler, type, 
                            direction, messageID, referenceID, refType, referenceID2,
                            refType2, responseID, resultCode, resultInfo, 
                            data, start, count);
        System.debug('###....interfaceLog ic before insert: ' + ic);
        insert ic;
        System.debug('###....interfaceLog ic after insert: ' + ic);
        return ic;
    }    

    public class InterfaceLogPart 
    {
        public Integer count = 0;
        public ID referenceID; 
        public String refType;
        public ID referenceID2;
        public String refType2; 
        public String data;   
        public String order;
        public String replenishment;  
    }
     public static List<SCInterfaceLog__c> writeInterfaceLogs(String interfaceName,
                            String interfaceHandler,
                            String type,      // OUTBOUND
                            String direction, // outbound
                            String messageID,
                            ID responseID,
                            String resultCode, 
                            String resultInfo, 
                            Datetime start, 
                            List<InterfaceLogPart> ilpList)
    {
        List<SCInterfaceLog__c> ilList = fillInterfaceLogs(interfaceName, interfaceHandler, type, 
                            direction, messageID, responseID, resultCode, resultInfo, 
                            start, ilpList);
        insert ilList;
        return ilList;
    }    


     public static SCInterfaceLog__c addInterfaceLog(List<SCInterfaceLog__c> interfaceLogList,
                            String interfaceName, 
                            String interfaceHandler,
                            String type,      // OUTBOUND
                            String direction, // outbound
                            String messageID,
                            ID referenceID, 
                            String refType,
                            ID referenceID2,
                            String refType2, 
                            ID responseID,
                            String resultCode, 
                            String resultInfo, 
                            String data, 
                            Datetime start, 
                            Integer count)
    {
        SCInterfaceLog__c ic = fillInterfaceLog(interfaceName, interfaceHandler, type, 
                            direction, messageID, referenceID, refType, referenceID2,
                            refType2, responseID, resultCode, resultInfo, 
                            data, start, count);
        interfaceLogList.add(ic);
        return ic;
    }    

    public String createSOAPUICall(String function, String headStructureName, String prefix, String nameSpace, String jsonInput)
    {
        String retValue = '<?xml version="1.0" encoding="UTF-8"?>'
                        + '\n<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"'
                        + ' xmlns:' + prefix + '="' + nameSpace + '">'
                        + '\n\t<soapenv:Header>'
                        + '\n\t\t<soapenv:SessionHeader>'
                        + '\n\t\t\t<soapenv:sessionId></soapenv:sessionId>'
                        + '\n\t\t</soapenv:SessionHeader>'
                        + '\n\t</soapenv:Header>'
                        + '\n\t<soapenv:Body>'
                        + '\n\t\t<' + prefix + ':' + function + '>'
                        + '\n\t\t\t<' + prefix + ':' + headStructureName + '>';
        
        retValue += getXMLDataFromJSON(jsonInput, prefix, 3);               
        retValue += '\n\t\t\t</' + prefix + ':' + headStructureName + '>'
                 + '\n\t\t</' + prefix + ':' + function + '>'
                 + '\n\t</soapenv:Body>'
                 + '\n</soapenv:Envelope>';                 

        return retValue;
    }

    public String getDataFromJSON(String jsonInput)
    {
        String retValue = '';
        Object obj = JSON.deserializeUntyped(jsonInput);
        if(obj instanceof List<Object>)
        {
            List<Object> genList = (List<Object>)obj;
            retValue = parseArray(genList, 0);
        }
        else
        {
            Map<String, Object> genMap = (Map<String, Object>)obj;
            retValue = parseData(genMap, 0);
        }   
        return retValue;
    }
    
    // recursive function walking over the JSON object and creating the Salesforce customerObjects
    // Map<String, Object> retValue = (Map<String, Object>)JSON.deserializeUntyped(jsonInput);
    public String parseData(Map<String, Object> jsonMap, Integer level)
    {
        String retValue = '';
        if(jsonMap != null)
        {
            level++;
            Set<String> keys = jsonMap.keySet();
            // primitive data
            for(String key: keys)
            {
                debug('parseData key: ' + key);
                Integer idxTypeInfo = key.indexOf('_type_info');
                if(idxTypeInfo < 0)
                {
                    // it is the normal field
                    Object value = jsonMap.get(key);
                    if(!(value instanceOf List<Object>))
                    {
                        if(isPrimitive(value))
                        {
                            retValue += '\n' + getIndent(level);
                            if(key == 'DocumentContent')
                            {
                                retValue += key + ': a place holder for the document content';
                            }
                            else
                            {
                                retValue += key + ': ' + value;
                            }    
                        }
                    }           
                }
            }// for first
            // complex data
            for(String key: keys)
            {
                debug('parseData key: ' + key);
                Integer idxTypeInfo = key.indexOf('_type_info');
                if(idxTypeInfo < 0)
                {
                    // it is the normal field
                    Object value = jsonMap.get(key);
                    if(isPrimitive(value))
                    {
                        // do nothing  it is artificially made because the primitve types are also the List<Object> type
                    }
                    else if((value instanceOf List<Object>))
                    {
//                        retValue += '\n';
//                        retValue += '\n' + getIndent(level) +  key + ' Array: ';
                        retValue += '\n' + getIndent(level) +  key + ':';
                        retValue += parseArray((List<Object>)value, level);
                    }
                    else if((value instanceof Map<String, Object>))
                    {
//                        retValue += '\n';
//                        retValue += '\n' + getIndent(level) + 'Element ' + key + ': ';
                        retValue += '\n' + getIndent(level) + key + ':';
                        retValue += parseData((Map<String, Object>)value, level);
                    }
                }
            }// for complex
        }    
        return retValue;
    }//parseImportData  

    public String parseArray(List<Object> value, Integer level)
    {
        String retValue = '';
        if(value != null)
        {
            level++;
            Integer i = 0;
            for(Object o: value)
            {
                if(isPrimitive(o))
                {
                    retValue += '\n' + getIndent(level);
                    retValue += o;
                }
                else if (o instanceof Map<String, Object>)
                {
//                    retValue += '\n';
//                    retValue += '\n' + getIndent(level) + 'Element[' + i + ']:';
                    retValue += '\n' + getIndent(level) + '[' + i + ']:';
                    retValue += parseData((Map<String, Object>)o, level);
                }
                else if((o instanceOf List<Object>))
                {
//                    retValue += '\n';
                    retValue += '\n' + getIndent(level) + 'Array: ';
                    retValue += parseArray((List<Object>)o, level);
                }
                i++;
            }
        }   
        return retValue;
    }

    public String getXMLDataFromJSON(String jsonInput, String prefix, Integer level)
    {
        String retValue = '';
        String jsonInputLoc = jsonInput.replace('\n', '');
        Object obj = JSON.deserializeUntyped(jsonInputLoc);
        if(obj instanceof List<Object>)
        {
            List<Object> genList = (List<Object>)obj;
            String key = null;
            retValue = parseXMLArray(genList, level, prefix, key);
        }
        else
        {
            Map<String, Object> genMap = (Map<String, Object>)obj;
            retValue = parseXMLData(genMap, level, prefix);
        }   
        return retValue;
    }
    
    // recursive function walking over the JSON object and creating the Salesforce customerObjects
    // Map<String, Object> retValue = (Map<String, Object>)JSON.deserializeUntyped(jsonInput);
    public String parseXMLData(Map<String, Object> jsonMap, Integer level, String prefix)
    {
        debug('Level: ' + level);
        String retValue = '';
        String prefixLoc = prefix + ':';
        if(jsonMap != null)
        {
            level++;
            Set<String> keys = jsonMap.keySet();
            // primitive data
            for(String key: keys)
            {
                debug('parseData key: ' + key);
                Integer idxTypeInfo = key.indexOf('_type_info');
                if(idxTypeInfo < 0)
                {
                    // it is the normal field
                    Object value = jsonMap.get(key);
                    if(!(value instanceOf List<Object>))
                    {
                        if(isPrimitive(value))
                        {
                            retValue += '\n' + getIndent(level);
                            if(key == 'DocumentContent')
                            {
                                retValue += '<' + prefixLoc + key + '> a place holder for the document content </' + prefixLoc + key + '>' ;
                            }
                            else
                            {
                                retValue += '<' + prefixLoc + key + '>' + value + '</' + prefixLoc + key + '>' ;
                            }    
                        }
                    }           
                }
            }// for first
            // complex data
            for(String key: keys)
            {
                debug('parseData key: ' + key);
                Integer idxTypeInfo = key.indexOf('_type_info');
                if(idxTypeInfo < 0)
                {
                    // it is the normal field
                    Object value = jsonMap.get(key);
                    if(isPrimitive(value))
                    {
                        // do nothing  it is artificially made because the primitve types are also the List<Object> type
                    }
                    else if((value instanceOf List<Object>))
                    {
                        debug('Array: ' + value);
                        //retValue += '\n' + getIndent(level) +   '<' + prefixLoc + key + '>';
                        retValue += parseXMLArray((List<Object>)value, level, prefix, key);
                        //retValue += '\n' + getIndent(level) +   '</' + prefixLoc + key + '>';
                    }
                    else if((value instanceof Map<String, Object>))
                    {
                        retValue += '\n' + getIndent(level) + '<' + prefixLoc + key + '>';
                        retValue += parseXMLData((Map<String, Object>)value, level, prefix);
                        retValue += '\n' + getIndent(level) + '</' + prefixLoc + key + '>';
                    }
                }
            }// for complex
        }    
        return retValue;
    }//parseImportData  

    public String parseXMLArray(List<Object> value, Integer level, String prefix, String key)
    {
        String retValue = '';
        String prefixLoc = prefix + ':';
        if(value != null)
        {
//            level++;
            Integer i = 0;
            for(Object o: value)
            {
                if(isPrimitive(o))
                {
                    retValue += '\n' + getIndent(level);
                    retValue += o;
                }
                else if (o instanceof Map<String, Object>)
                {
                    retValue += '\n' + getIndent(level);
                    if(key != null)
                    {
                        retValue +=  '<' + prefixLoc + key + '>';
                    }   
                    retValue += parseXMLData((Map<String, Object>)o, level, prefix);
                    retValue += '\n' + getIndent(level);
                    if(key != null)
                    {
                        retValue += '</' + prefixLoc + key + '>';
                    }
                }
                else if((o instanceOf List<Object>))
                {
                    retValue += '\n' + getIndent(level);
                    if(key != null)
                    {
                        retValue +=  '<' + prefixLoc + key + '>';
                    }   
                    retValue += parseXMLArray((List<Object>)o, level, prefix, null);
                    retValue += '\n' + getIndent(level);
                    if(key != null)
                    {
                        retValue += '</' + prefixLoc + key + '>';
                    }
                }
                i++;
            }
        }   
        return retValue;
    }


    public String getIndent(Integer level)
    {
        String retValue = '';
        for(Integer i = 0; i < level; i++)
        {
            retValue += '\t';
        }
        return retValue;
    }
   public Boolean isPrimitive(Object value)
    {
        Boolean retValue = false;
        if((value instanceOf String)
            || (value instanceOf Integer) 
            || (value instanceOf Double)
            || (value instanceOf Datetime)
            || (value instanceOf Date)
            || (value instanceOf Long)
            || (value instanceOf Boolean)
            || (value instanceOf Decimal))
        {
            retValue = true;
        }
        return retValue;
    }
    public void debug(String msg)
    {
        System.debug('###...' +  msg);        
    }

}
