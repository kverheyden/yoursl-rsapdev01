global with sharing class SalesOrderSummaryController{
	
	public Account SupplierAccount { get; private set; }
	public Account SalesOrderAccount { get; private set; } 
	public List<SalesOrder__c> SalesOrders { get; private set; }
	
	public SalesOrderSummaryController() {
		Id SupplierId = ApexPages.currentPage().getParameters().get('supplierId');
		if (SupplierId == null) 
			return;
		
		String salesOrderQuery = 'SELECT ' + 
			'Name, ' +
			'RequestedDeliveryDate__c, ' +
			'OrderDate__c, ' +
			'Description__c, ' +
			'CustomerNumberAtSupplier__c, ' +
			'Type__c, ' +
		
			'Account__r.Name, ' +
			'Account__r.Name2__c, ' +
			'Account__r.BillingStreet, ' +
			'Account__r.BillingHouseNo__c, ' +
			'Account__r.BillingPostalCode, ' +
			'Account__r.BillingCity, ' +
			'Account__r.Phone, ' +
			'Account__r.ContactDefault__c, ' +
			'Account__r.ShippingStreet, ' +
			'Account__r.ShippingPostalCode, ' +
			'Account__r.ShippingCity, ' +
		
			'Supplier__r.AccountNumber, ' +
			'Supplier__r.Name, ' +
			'Supplier__r.ContactDefault__c, ' +
			'Supplier__r.Phone, ' +
			'(SELECT Name, ExternalArticleNumber__c, PricebookEntryDetail__r.Product2__r.Name, Product2__r.Name, Product2__r.ID2__c, Quantity__c FROM SalesOrderItems__r)' +
		'FROM SalesOrder__c ' +
		'WHERE Id IN (SELECT SalesOrder__c FROM SalesOrderItem__c) ' +
		'AND Supplier__r.Id =: SupplierId AND ' + 
		'RecordType.Name = \'Indirect Order\' AND ' + 
		'Status__c = \'Active\' AND ' + 
		'SendToSupplierDate__c != null';
	
		SalesOrders = Database.query(salesOrderQuery);
		if (SalesOrders.size() > 0)
			SupplierAccount = SalesOrders[0].Supplier__r;
	}
	
}
