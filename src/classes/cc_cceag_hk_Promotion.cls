global without sharing class cc_cceag_hk_Promotion extends ccrz.cc_hk_Promotion {
	
	global override Map<String,Object> filter(Map<String,Object> inpData){ 
    	Map<String, List<ccrz.cc_bean_promoRD>> inputList = (Map<String, List<ccrz.cc_bean_promoRD>>)inpData.get(PARAM_PROMOS);
    	
    	//First get the list of all promotion ids.  We need these to grab the rules
    	List<Id> promoIds = new List<Id>();
    	for(List<ccrz.cc_bean_promoRD> promoList:inputList.values()){
    		for(ccrz.cc_bean_promoRD p:promoList){
    			promoIds.add(p.sfid);
    		}
    	}
    	
        //Now grab the current Account... really we want the Account Group Id
		// String effAccount = ccrz.cc_CallContext.remoteContext.effAccountId;
		System.debug(LoggingLevel.INFO, 'ccrz.cc_CallContext.remoteContext: ' + ccrz.cc_CallContext.remoteContext);
		ccrz__E_Cart__c cart = [
		    select
		         Id
		        ,ccrz__ShipTo__r.ccrz__Partner_Id__c
		        ,ccrz__RequestDate__c
		    from
		        ccrz__E_Cart__c
		    where
		        ccrz__EncryptedID__c = :ccrz.cc_CallContext.remoteContext.currentCartID
		];
		Account acc = [SELECT Id FROM Account WHERE AccountNumber =: cart.ccrz__ShipTo__r.ccrz__Partner_Id__c];
		String effAccount = acc.Id;
		System.debug(LoggingLevel.INFO, 'effAccount: ' + effAccount);
		List<ccrz__E_PromotionAccountGroupFilter__c> promoFilters = [
        		SELECT
        			ccrz__CC_Promotion__r.Id
        		FROM
        			ccrz__E_PromotionAccountGroupFilter__c
				WHERE
					ccrz__CC_Promotion__r.Id IN :promoIds AND
					ccrz__StartDate__c <= TODAY AND
					ccrz__EndDate__c > TODAY AND
					CCAccount__c =: effAccount
        		];
        System.debug(LoggingLevel.INFO, promoFilters);
        List<String> filteredPromoIds = new List<String>();
        for(ccrz__E_PromotionAccountGroupFilter__c filter: promoFilters)
        	filteredPromoIds.add(filter.ccrz__CC_Promotion__r.Id);

        Map<ID, ccrz__E_Promo__c> promoData = new Map<ID, ccrz__E_Promo__c>([
    			SELECT
    				ID,
    				(SELECT Product__r.Id, Product__r.Name, Product__r.ccrz__SKU__c FROM CC_Promotion_Products__r)
    			FROM
    				ccrz__E_Promo__c
    			WHERE
    				Id =: filteredPromoIds
    		]);
       	System.debug(LoggingLevel.INFO, promoData);

        Map<String, List<cc_cceag_bean_promo>> filtered = new Map<String, List<cc_cceag_bean_promo>>();
    	for (String key:inputList.keySet()) {
    		List<ccrz.cc_bean_promoRD> candidatePromos = inputList.get(key);
    		List<cc_cceag_bean_promo> filteredList = new List<cc_cceag_bean_promo>();
    		filtered.put(key,filteredList);
    		for (ccrz.cc_bean_promoRD promo: candidatePromos){
    			ccrz__E_Promo__c promoEntry = promoData.get(promo.sfid);
    			if (promoEntry != null) {
    				cc_cceag_bean_promo newEntry = new cc_cceag_bean_promo(promo);
    				for (E_PromotionProduct__c promoProd: promoEntry.CC_Promotion_Products__r) {
    					newEntry.items.add(new cc_ctrl_MostOrdered.MostOrderedProductBean(promoProd.Product__r, 0));
    				}
    				filteredList.add(newEntry);
    			}
    		}
    	}

    	return new Map<String,Object>{
            PARAM_PROMOS => filtered
        };

    }

    global class cc_cceag_bean_promo {
    	global Id sfid {get;set;}
		global String sku { get; set; }
		global String name { get; set; }
		global String shortDecription { get; set; }
		global String pageLocation { get; set;}
		global String locationType { get; set; }
		global Integer sequence { get; set; }
		global String imageUri { get; set; }
		global String externalLink { get; set; }
		global String imageSource { get; set; }
		global String imageStaticResource { get; set; }
		global boolean isProductRelated { get; set; }
		global String fullImageURL { get; set; }
		global Boolean noLink { get; set; }
		global Boolean isDownload {get;set;}
		global String downloadUri {get;set;}
		global String downloadContentType{get;set;}
		global boolean isNewWindow{get;set;}
		global Id categoryId{get;set;}
		global List<cc_ctrl_MostOrdered.MostOrderedProductBean> items { get; set; }
	
		global cc_cceag_bean_promo(ccrz.cc_bean_promoRD bean){
			this.sfid = bean.sfid;
			this.sku = bean.sku;
			this.name = bean.name;
			this.shortDecription = bean.shortDecription;
			this.pageLocation = bean.pageLocation;
			this.locationType = bean.locationType;
			this.sequence = bean.sequence;
			this.imageUri = bean.imageUri;
			this.externalLink = bean.externalLink;
			this.imageSource = bean.imageSource;
			this.imageStaticResource = bean.imageStaticResource;
			this.isProductRelated = bean.isProductRelated;
			this.fullImageURL = bean.fullImageURL;
			this.noLink = bean.noLink;
			this.isDownload = bean.isDownload;
			this.downloadUri = bean.downloadUri;
			this.downloadContentType = bean.downloadContentType;
			this.isNewWindow = bean.isNewWindow;
			this.categoryId = bean.categoryId;
			this.items = new List<cc_ctrl_MostOrdered.MostOrderedProductBean>();
		}
    }
    

}
