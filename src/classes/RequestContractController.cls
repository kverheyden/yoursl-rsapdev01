global with sharing class RequestContractController{
	
	public static final String ATTACHMENT_ID_PARAMETER = 'attachmentId';
	public static final String ATTACHMENT_PARENT_ID_PARAMETER = 'parentId';
	
	public Id AttachmentId { get; private set; }
	public Id AttachmentParentId { get; private set; }
	
	public RequestAttachment__c RequestAttachment { get; private set; }
	public Request__c Request { get; private set; }
	public Account Account { get; private set; }
	public CdeOrder__c CdeOrder { get; private set; }
	
	public Boolean RequestIsEmpty { get { return Request == null; }}
	public String CurrentDate { get; private set; }
	
	public RequestContractController() {		
		CurrentDate = System.now().format('dd.MM.yyyy');
		AttachmentId = ApexPages.currentPage().getParameters().get(ATTACHMENT_ID_PARAMETER);
		AttachmentParentId = ApexPages.currentPage().getParameters().get(ATTACHMENT_PARENT_ID_PARAMETER);
		
		if (AttachmentId == null || AttachmentParentId == null)
			return;
		
		initRequestAttachment();
		initRequest();			
		initCdeOrderWithParent();		
	}
	
	private void initRequestAttachment() {
		if (AttachmentParentId.getSObjectType() != RequestAttachment__c.SObjectType)
			return;
		RequestAttachment = [SELECT 
			Request__r.Name, 
			Request__r.PayerIsIndividual__c,
		
			Request__r.PayerName__c, 
			Request__r.PayerName2__c, 
			Request__r.PayerStreet__c, 
			Request__r.PayerHouseNo__c, 
			Request__r.PayerPostalCode__c,
			Request__r.PayerCity__c,
			Request__r.PayerCustomerNumber__c,

			Request__r.OutletCity__c, 
			Request__r.OutletHouseNo__c, 
			Request__r.OutletIsPayer__c, 
			Request__r.OutletIsSeasonal__c, 
			Request__r.OutletCompanyName__c, 
			Request__r.OutletStreet__c, 
			Request__r.OutletPostalCode__c,

			Type__c
		FROM RequestAttachment__c 
		WHERE Id =: AttachmentParentId LIMIT 1];
	}
	
	private void initRequest() {
		if (RequestAttachment != null)
			Request = RequestAttachment.Request__r;
	}
	
	private void initCdeOrderWithParent() {
		if (AttachmentParentId.getSobjectType() != CdeOrder__c.SObjectType)
			return;
		
		CdeOrder = [SELECT 
			Account__r.Name,
			Account__r.FirstName__c,
			Account__r.LastName__c,
			Account__r.ContactDefault__c,
			Account__r.Phone,
			Account__r.ShippingStreet,
			Account__r.ShippingCity,
		
			Request__r.OutletCompanyName__c,
			Request__r.OutletStreet__c,
			Request__r.ShipToCompanyName__c,
			Request__r.ShipToStreet__c,
			Request__r.ShipToHouseNo__c,
			Request__r.ShipToCity__c,
			Request__r.OutletCity__c,
			Request__r.OutletPhone__c,
			Request__r.ContactFirstName__c,
			Request__r.ContactLastName__c,
			(
				SELECT 
					Quantity__c,
					Building__c,
					Floor__c,
					Room__c,
					ProductModel__r.Name,
					ProductModel__r.Brand__c,
					ProductModel__r.ComBoxType__c,
					ProductModel__r.Group__c
				FROM CDE_Order_Items__r
			) 
			FROM CdeOrder__c 
			WHERE Id =: AttachmentParentId LIMIT 1];
		
		if (CdeOrder != null) {
			Account = CdeOrder.Account__r;
			Request = CdeOrder.Request__r;
		}
	}
}
