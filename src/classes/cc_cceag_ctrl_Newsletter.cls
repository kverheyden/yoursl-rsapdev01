global with sharing class cc_cceag_ctrl_Newsletter {
    
    public cc_cceag_ctrl_Newsletter() { 
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult fetchData(ccrz.cc_RemoteActionContext ctx) {
        
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
        
        result.inputContext = ctx;
        result.success = false;
        
        try {
            
            String userId = cc_cceag_dao_User.getFlowUserWithContext(ctx);
            User user = [
                SELECT
                    Contact.promotionnewsletter__c,
                    Contact.productnewsletter__c
                FROM
                    User
                WHERE
                    Id = :userId
            ];
            
            result.data = user.Contact;
            result.success = true;
            
        } catch (Exception e) { handleException(result, e); }
        
        return result;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult saveData(ccrz.cc_RemoteActionContext ctx, String productNewsletter, String promotionNewsletter) {
        
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
        result.inputContext = ctx;
        result.success = false; 
        
        try {
            
            String userId = cc_cceag_dao_User.getFlowUserWithContext(ctx);
                                                                                                
            System.debug('saving newsletter data for user:' + userId);
            System.debug('product newsletter: ' + productNewsletter);
            System.debug('promotion newsletter: ' + promotionNewsletter);
            User user = [
                SELECT
                	Contact.Id,
                	Contact.promotionnewsletter__c,
                	Contact.productnewsletter__c
                FROM
                	User
                WHERE
                	Id = :userId
            	];
            
            user.Contact.promotionnewsletter__c = (promotionNewsletter == '1');
            user.Contact.productnewsletter__c = (productNewsletter == '1');
            
            update user.Contact;
            
            result.data = user;
            result.success = true;
            
        } catch (Exception e) { handleException(result, e); }
        
        return result;
    }
    

    /**
     * In any case of an error updating the users newsletter settings, notify using a message bear
     */
    public static void handleException (ccrz.cc_RemoteActionResult result, Exception e) {

        System.debug(System.LoggingLevel.ERROR, e.getMessage());
        System.debug(System.LoggingLevel.ERROR, e.getStackTraceString());
        
        ccrz.cc_bean_Message msg = new ccrz.cc_bean_Message();
        msg.type = ccrz.cc_bean_Message.MessageType.CUSTOM;
        msg.classToAppend = 'messagingSection-Error';
        msg.message = e.getStackTraceString();
        msg.severity = ccrz.cc_bean_Message.MessageSeverity.ERROR;
        
        result.messages.add(msg);
    }
}
