/*
 * @(#)SCAddressFormattedControllerTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
@isTest
private class SCAddressFormattedControllerTest
{
    private static SCAddressFormattedController afc;
        
    static testMethod void getCheckPositive1() 
    {
        SCHelperTestClass3.createCustomSettings('DE', true);
        SCHelperTestClass.createInstalledBaseLocation(true);  
        
        String street = '';
        if(SCHelperTestClass.location.Street__c != null)
            street = SCHelperTestClass.location.Street__c;
        
        String house = '';
        if(SCHelperTestClass.location.HouseNo__c != null)
            house = SCHelperTestClass.location.HouseNo__c;
        
        String district = '';
        if(SCHelperTestClass.location.District__c != null) 
            district = SCHelperTestClass.location.District__c;
            
        String postal = '';
        if(SCHelperTestClass.location.PostalCode__c != null) 
            postal = SCHelperTestClass.location.PostalCode__c;
            
        String city = '';
        if(SCHelperTestClass.location.City__c != null) 
            city = SCHelperTestClass.location.City__c;
        
        afc = new SCAddressFormattedController();
        
        afc.objectId   = SCHelperTestClass.location.Id;
        afc.objectType = 'location';
        
        afc.getAddress();
        
        System.assertEquals(afc.line1, street + ' ' + house);
        System.assertEquals(afc.line2, postal + ' ' + city + ' ' + district);
    }
    
    static testMethod void getCheckPositive2() 
    {
        SCHelperTestClass3.createCustomSettings('GB', true);
        SCHelperTestClass.createAccountObject('Customer', true);
        
        String street = '';
        if(SCHelperTestClass.account.BillingStreet != null)
            street = SCHelperTestClass.account.BillingStreet;
        
        String house = '';
        if(SCHelperTestClass.account.BillingHouseNo__c != null)
            house = SCHelperTestClass.account.BillingHouseNo__c;
        
        String district = '';
        if(SCHelperTestClass.account.BillingDistrict__c != null) 
            district = SCHelperTestClass.account.BillingDistrict__c;
            
        String postal = '';
        if(SCHelperTestClass.account.BillingPostalCode != null) 
            postal = SCHelperTestClass.account.BillingPostalCode;
            
        String city = '';
        if(SCHelperTestClass.account.BillingCity != null) 
            city = SCHelperTestClass.account.BillingCity;
        
        afc = new SCAddressFormattedController();
        
        afc.objectId   = SCHelperTestClass.account.Id;
        afc.objectType = 'account';
        
        afc.getAddress();
        
        System.assertEquals(afc.line1, house + ' ' + street);
        System.assertEquals(afc.line2, city + ' ' + postal);
    }
    
    static testMethod void getCheckPositive3() 
    {
        SCHelperTestClass3.createCustomSettings('NL', true);
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet(true);
        
        String street = '';
        if(SCHelperTestClass.orderItem.InstalledBase__r.InstalledBaseLocation__r.Street__c != null)
            street = SCHelperTestClass.orderItem.InstalledBase__r.InstalledBaseLocation__r.Street__c;
        
        String house = '';
        if(SCHelperTestClass.orderItem.InstalledBase__r.InstalledBaseLocation__r.HouseNo__c != null)
            house = SCHelperTestClass.orderItem.InstalledBase__r.InstalledBaseLocation__r.HouseNo__c;
        
        String district = '';
        if(SCHelperTestClass.orderItem.InstalledBase__r.InstalledBaseLocation__r.District__c != null) 
            district = SCHelperTestClass.orderItem.InstalledBase__r.InstalledBaseLocation__r.District__c;
            
        String postal = '';
        if(SCHelperTestClass.orderItem.InstalledBase__r.InstalledBaseLocation__r.PostalCode__c != null) 
            postal = SCHelperTestClass.orderItem.InstalledBase__r.InstalledBaseLocation__r.PostalCode__c;
            
        String city = '';
        if(SCHelperTestClass.orderItem.InstalledBase__r.InstalledBaseLocation__r.City__c != null) 
            city = SCHelperTestClass.orderItem.InstalledBase__r.InstalledBaseLocation__r.City__c;
            
        String extension = '';
        if(SCHelperTestClass.orderItem.InstalledBase__r.InstalledBaseLocation__r.Extension__c != null) 
            city = SCHelperTestClass.orderItem.InstalledBase__r.InstalledBaseLocation__r.Extension__c;
        
        afc = new SCAddressFormattedController();
        
        afc.objectId   = SCHelperTestClass.orderItem.Id;
        afc.objectType = 'orderitem';
        
        afc.getAddress();
        
        System.assertEquals(afc.line1, street + ' ' + house + ' ' + extension);
        System.assertEquals(afc.line2, postal + ' ' + city + ' ' + district);
    }
    
    static testMethod void getCheckPositive4() 
    {
        SCHelperTestClass3.createCustomSettings('NL', true);
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet(true);
        
        String street = '';
        if(SCHelperTestClass.orderRoles[0].Street__c != null)
            street = SCHelperTestClass.orderRoles[0].Street__c;
        
        String house = '';
        if(SCHelperTestClass.orderRoles[0].HouseNo__c != null)
            house = SCHelperTestClass.orderRoles[0].HouseNo__c;
        
        String district = '';
        if(SCHelperTestClass.orderRoles[0].District__c != null) 
            district = SCHelperTestClass.orderRoles[0].District__c;
            
        String postal = '';
        if(SCHelperTestClass.orderRoles[0].PostalCode__c != null) 
            postal = SCHelperTestClass.orderRoles[0].PostalCode__c;
            
        String city = '';
        if(SCHelperTestClass.orderRoles[0].City__c != null) 
            city = SCHelperTestClass.orderRoles[0].City__c;
            
        String extension = '';
        if(SCHelperTestClass.orderRoles[0].Extension__c != null) 
            city = SCHelperTestClass.orderRoles[0].Extension__c;
        
        afc = new SCAddressFormattedController();
        
        afc.objectId   = SCHelperTestClass.orderRoles[0].Id;
        afc.objectType = 'orderrole';
        
        afc.getAddress();
        
        System.assertEquals(afc.line1, street + ' ' + house + ' ' + extension);
        System.assertEquals(afc.line2, postal + ' ' + city + ' ' + district);
    }
}
