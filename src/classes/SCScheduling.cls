/*
 * @(#)SCScheduling.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Used to create appointment proposals (data object)
 * @author H. Schroeder <hschroeder@gms-online.de>
 * @version $Revision$, $Date$
 */
global class SCScheduling
{
    public Date day {get; set;}
    public DateTime cdstart { get; set; }
    public DateTime cdend { get; set; }
    public DateTime datstart { get; set; }
    public String timeOfDay {get; set;}
    public String employee { get; set; }
    public String employeeId { get; set; }
    public String employeeFunc { get; set; }
    public String employeeName { get; set; }
    public String employeeFirstname { get; set; }
    public String address { get; set; }
    public String tour { get; set; }
    public String score { get; set; }
    public Integer listID { get; set; }
    public boolean selected {get; set;}
    public boolean selectedFixed {get; set;}
    public Id resourceID { get; set; }
    public SCResource__c resource { get; set; }
    public boolean manuallyInput {get; set; }
    public String department { get; set; }
    public boolean isProposal {get; set; }
    public Set<Id> orderIds;
    public List<SCOrder__c> orders {get; set; }
    public boolean showSelected {get; set;}
    
    public SCScheduling()
    {
        manuallyInput = false;
        isProposal = true;
    }
}

/* An example appointment proposal string from the scheduling engine 
            <dataEntries>
               <keyValues>
                  <key>EMPLOYEE</key>
                  <value>(GMSDH) Herdt, Dietrich [Engineer (field)]</value>
               </keyValues>
               <keyValues>
                  <key>EMPLOYEE_IK</key>
                  <value>a0xP00000008t7hIAA</value>
               </keyValues>
               <keyValues>
                  <key>ADDRESS</key>
                  <value>8152 BS Lemelerveld, Klipperstraat 7</value>
               </keyValues>
               <keyValues>
                  <key>SCORE</key>
                  <value>-1000</value>
               </keyValues>
               <keyValues>
                  <key>INDEX</key>
                  <value>4</value>
               </keyValues>
               <keyValues>
                  <key>CDSTART</key>
                  <value>20110708120000</value>
               </keyValues>
               <keyValues>
                  <key>CDEND</key>
                  <value>20110708175100</value>
               </keyValues>
               <keyValues>
                  <key>DATSTART</key>
                  <value>20110708120000</value>
               </keyValues>
               <keyValues>
                  <key>TOUR</key>
                  <value>0 km</value>
               </keyValues>
               <keyValues>
                  <key>EMPLID</key>
                  <value>GMSDH</value>
               </keyValues>
               <keyValues>
                  <key>EMPLFUNC</key>
                  <value>Engineer (field)</value>
               </keyValues>
               <keyValues>
                  <key>EMPLNAME</key>
                  <value>Herdt</value>
               </keyValues>
               <keyValues>
                  <key>EMPLFIRSTNAME</key>
                  <value>Dietrich</value>
               </keyValues>
            </dataEntries>
*/
