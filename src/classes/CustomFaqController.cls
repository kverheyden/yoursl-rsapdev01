public with sharing class CustomFaqController {
	private Apexpages.Standardcontroller controller;
    
	private PageReference page;
    public string id;
    public Faq__c myFaq  {get;set;}
    public string action {get;set;}
    public transient Attachment myfile;
    public Attachment getmyfile()
    {
        myfile = new Attachment();
        return myfile;
    }
    public list<Attachment> ListFaqAttachments {get;set;}
    public Attachment currentAttachment {get;set;} 
        
	public CustomFaqController(ApexPages.StandardController stdController) {
        this.controller = stdController;
        this.myFaq = (Faq__c)stdController.getrecord();
        this.page = ApexPages.currentPage();
        id = page.getParameters().get('id');
        
        if(ApexPages.currentPage().getParameters().get('action')!=null)
                action = this.page.getParameters().get('action');

	   if(this.id!=null)
        {
            try
            {
                myFaq = [SELECT Id,OwnerId,ContentType__c,LastModifiedDate,Name,Title__c,Description__c,Status__c,Application__c,Category__c FROM Faq__c WHERE Id=:id LIMIT 1];
                getAttachment();
            }
            catch(Exception e){System.debug(e.getTypeName() + ' caught: ' + e.getMessage());}
            
            if(ApexPages.currentPage().getParameters().get('retURL')!=null || action=='edit')
                action='edit';
            else
                action='show';
        }
        
        if(action==null)
                action = 'list';
    }
    
    
    public list<Attachment> getAttachment()
    {
        ListFaqAttachments = new list<Attachment>();
        ListFaqAttachments = [SELECT Id, Name, ParentId FROM Attachment WHERE ParentId =:id ORDER BY CreatedDate DESC];
        if(ListFaqAttachments.size() > 0)
            currentAttachment = ListFaqAttachments[0];
        else
            currentAttachment = null;
        return ListFaqAttachments;
    }
    
    /******************************************
        delete attachments before insert new
    ******************************************/     
    public void delAtt(){
        system.debug('### Delete ListFaqAttachments: ' + ListFaqAttachments);
        delete ListFaqAttachments;
    }
    
    public PageReference showList() {
		return customFaqPageReference('list');
    }

    public PageReference editFaq() {
		return customFaqPageReference('edit');
    }   
    
	public PageReference newFaq() {
		return customFaqPageReference('new');
    }   

	public PageReference deleteAttachment(){
        delAtt();
        getAttachment();
        
		if(myFaq.Id != null)
		{
		  myFaq.ContentType__c = '';
		  update myFaq;
		}
		
		return null;
    }

    public PageReference cancel() {
        if (action == 'new')
			return customFaqPageReference('list');
		if (action == 'edit')
			return customFaqPageReference('read');
		return customFaqPageReference('list');
    }
        
    public PageReference saveFaq() {
        try
        {
            if(myFaq.Id == null)
                insert myFaq;
            else
                update myFaq;
        }
        catch(Exception e){System.debug(e.getTypeName() + ' caught: ' + e.getMessage());}
        
        system.debug('### Created new FAQ: ' + myFaq);
        /***************************
                Attachment
        ***************************/
        
        if(myfile.body != NULL)
        {
            system.debug('### new File found: '+myfile.name);
            if(ListFaqAttachments != null && ListFaqAttachments.size()>0)
            {
                system.debug('### try to delete existing Files: '+ ListFaqAttachments.size());
                delAtt();
            }           
        }

		Attachment faqAttachment = new Attachment(parentId = myFaq.Id, name = myfile.name, body = myfile.body);
		try {
			system.debug('### saveFAQ Attachment: FAQId='+ myFaq.Id + ' FileName='+ myfile.name);
			if (myFaq.Id != null &&myfile.body != NULL) {
				system.debug('### try to insert File: ');
				insert faqAttachment;
			}
			myFaq.ContentType__c = getContentTypeByFileEnding(myfile.name);
			faqAttachment.ContentType = myFaq.ContentType__c;
			update myFaq;
			update faqAttachment;
        }
        catch(Exception e) 
        {
            System.debug('### ' + e.getTypeName() + ' caught: ' + e.getMessage());    
        }   
        
		return customFaqPageReference('show');
    }
	
	private PageReference customFaqPageReference(String actionName) {
		PageReference newPage = New PageReference('/apex/CustomFaqAction');		
		if (actionName == 'edit' || actionName == 'show' || actionName == 'read')
        	newPage.getParameters().put('id', myFaq.Id);
		newPage.getParameters().put('action', actionName);
        newPage.setRedirect(true);
        return newPage;
	}
	
	private static String getContentTypeByFileEnding(String fileName) {
        string fileNameLower = fileName.toLowerCase();
        
        if(fileNameLower.endsWith('.png'))
            return 'image/png';
        
        if(fileNameLower.endsWith('.jpg') || fileName.endsWith('.jpeg'))
            return 'image/jpeg';
        
        if(fileNameLower.endsWith('.pdf'))
            return 'application/pdf';
        
        if(fileNameLower.endsWith('.txt'))
            return 'text/plain';
        
        return '';
    }
}
