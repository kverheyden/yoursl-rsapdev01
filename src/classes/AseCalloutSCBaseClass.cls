/**
 * AseCalloutSCBaseClass.cls    mt  28.09.2009
 * 
 * Copyright (c) 2009 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 /**
 * AseCalloutSCBaseClass is an Abstract class which is used in 
 * the AseServiceManager because all classes whch are used there
 * has to be derived from this one
 * 
 * @author mt
 * @version $Revision$, $Date$
 */
 public abstract class AseCalloutSCBaseClass implements AseCalloutSCBaseInterface  {

    /**
     * if this flas is true, then the class is called in test-mode. 
     * So there are e.g. different limits as in normale mode. 
     */
    public boolean isTest = false;


    /**
     * @return the type of the class as Integer
     */
    abstract Integer getASEType();
        
    /**
     * @return the type of this class as String
     */
    public String getAseTypeStr()
    {
        return AseCalloutUtils.getDataTypeAsString(getASEType());
    } 
        
    /*
     * @see AseCalloutSCBaseInterface#addRemoveAllTypes()
     */
    public virtual void addRemoveAllTypes(String[] strDataTypes)
    {
        strDataTypes.add(getAseTypeStr());
    }


    /*
     * @see AseCalloutSCBaseInterface#addPrintDebugGetAllTypes()
     */
    public virtual void addPrintDebugGetAllTypes(integer[] intDataTypes)
    {
        intDataTypes.add(getASEType());
    }


    /*
     * @see AseCalloutSCBaseInterface#addSetAseDataTypes()
     */
    public virtual void addSetAseDataTypes(AseService.aseDataType[] dTypes, String[] paras)
    {
        AseService.aseDataEntry[] dEntries = new AseService.aseDataEntry[0];
        AseService.aseDataEntry dEntry = new AseService.aseDataEntry();

        if (paras.size()>0) 
        {
            for(Integer i=0; i<paras.size(); i++)
            {
                AseCalloutUtils.addStringsAsKeyVal2Entry('key'+i, paras[i], dEntry);
            }
        }
        else 
        {
            AseCalloutUtils.addEmptyKeyVal2Entry(dEntry);
        }
        
        dEntries.add(dEntry);

        AseService.aseDataType tmpDataType = new AseService.aseDataType();
        tmpDataType.dataEntries = dEntries;
        tmpDataType.type_x = getAseTypeStr();
        dTypes.add(tmpDataType);
    }

    
    /*
     * @see AseCalloutSCBaseInterface#setIsTest()
     */
    public void setIsTest(boolean isTest)
    {
        this.isTest = isTest;
    }
    
    /*
     * @see AseCalloutSCBaseInterface#getIsTest()
     */
    public boolean getIsTest()
    {
        return this.isTest;
    }
    
}
