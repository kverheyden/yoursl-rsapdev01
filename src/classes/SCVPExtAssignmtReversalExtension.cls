/*
* @(#)SCVPExtAssignmtReversalExtension.cls
* 
* Copyright 2014 by GMS Development GmbH, Karl-Schurz-Strasse 29, 
* DE-33100 Paderborn. All rights reserved.
*
* 
* 
* # Extension class of page 'SCVPExtAssignmtReversal'.
*
* @author Marc Sälzler <msaelzler@gms-online.de>
*
* @history  
* 2014-11-25 GMSmae created
* 
* @review
*
*/

public class SCVPExtAssignmtReversalExtension
{
    public ApexPages.StandardController x_stdCtrl;
    
    /**
    * Constructor
    *
    * @param ApexPages.StandardController p_stdCtrl
    */
    public SCVPExtAssignmtReversalExtension(ApexPages.StandardController p_stdCtrl)
    {
        x_stdCtrl = p_stdCtrl;
        
        if(!ApexPages.currentPage().getParameters().containsKey('id'))
        {
            a_errorText = Label.SC_msg_VP_NoExtAssignmtSelected;
        }
    }
    
    /**
    * Attribute for the VF page to identify that the update was successful.
    *
    */
    public Boolean a_success
    {
        get
        {
            if(a_success == null)
            {
                a_success = false;
            }
            
            return a_success;
        }
        
        set;
    }
    
    /*
    * Attribute for the VF page that contains any upcoming errors in general.
    *
    */
    public String a_errorText
    {
        get
        {
            if(a_errorText == null)
            {
                a_errorText = '';
            }
            
            return a_errorText;
        }
        
        set;
    }
    
    /*
    * Attribute for the VF page that contains upcoming errors regarding the Attachment.
    *
    */
    public String a_errorTextExtAssignmt
    {
        get
        {
            if(a_errorTextExtAssignmt == null)
            {
                a_errorTextExtAssignmt = '';
            }
            
            return a_errorTextExtAssignmt;
        }
        
        set;
    }
    
    /**
    * 
    *
    */
    public PageReference SaveProcess()
    {
        try
        {
            SCOrderExternalAssignment__c tmp_extAssignment = (SCOrderExternalAssignment__c) x_stdCtrl.getRecord();
            
            tmp_extAssignment.Status__c = 'rejected';
            
            update tmp_extAssignment;
            
            a_success = true;
        }
        catch(Exception e)
        {
            a_errorTextExtAssignmt = String.valueOf(e);
        }
        
        return null;
    }
}
