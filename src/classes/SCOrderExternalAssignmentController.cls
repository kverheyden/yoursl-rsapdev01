/*
 * @(#)SCOrderExternalAssignmentController.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This class adds a new order extern assignment and their items to the order.
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
public virtual with sharing class SCOrderExternalAssignmentController 
{
    public List<SCVendorContract__c> vContracts { get; set; }
    public List<SCVendorContractItem__c> vContractItems { get; set; }

    public SCOrderExternalAssignment__c extAssignment { get; set; }
    public String selectedVendorContract { get; set; }
    public String selectedVendorContractItem { get; set; }
    public SCVendorContractItem__c vendorContractItem;
    public List<SCVendorContractService__c> services { get; set; }
    public List<SCOrderExternalAssignmentItem__c> extraServices { get; set; }
    public Double sum { get; set; }
    public Double sum2 { get; set; }
    public Integer pageNum { get; set; }
    public Integer selectedExtraServiceToDelete { get; set; }
    public Integer selectedNumberExtraServicesToAdd { get; set; }
    
    public String oid { get; set; }
    public String currentPage { get; set; }
    public Boolean canDoOrder { get; set; }
    
    public Boolean showErrorMessage { get; set; }
    public String  errorMessage { get; set; }
    public Boolean isOrderlessMode { get; set; } // true if order assignment is not needed (if called from SCOrderWorkshopWizard), default false
    
    public String  errorMsgForNoItemSelected {get;set;}
    public Boolean showErrorMsgForNoItem {get;set;}
    public Boolean getServicesDefined()
    {
        return services != null && services.size() > 0;
    }
        
    /**
     * Constructor for the standard controller
     *
     * @param    controller    the standard controller
     */
    public SCOrderExternalAssignmentController(ApexPages.StandardController controller)
    {


        System.debug('#### SCOrderExternalAssignmentController constructor 2' + controller);
        isOrderlessMode = false;
        init();
        extraServices = new List<SCOrderExternalAssignmentItem__c>();
        
        if ( ApexPages.currentPage().getParameters().containsKey('oid') && ApexPages.currentPage().getParameters().get('oid') != '' )
        {
            oid = ApexPages.currentPage().getParameters().get('oid');
            
            extAssignment.Order__c = oid;
            
            canDoOrder = getCanProcesOrder();
        }
        else
        {
            oid = null;
            
            canDoOrder = false;
        }
    }
    
    /**
     * Constructor
     */
    public SCOrderExternalAssignmentController()
    {
        System.debug('#### SCOrderExternalAssignmentController constructor 1 std (new init, called from SCOrderWorkshopWizardExtension)');
        isOrderlessMode = true;
        init();
    }

   /*
    * Initializes the variables, reads data, performs an initial validation 
    */
    public void init()
    {
        System.debug('####init executed;');
        vContracts = new List<SCVendorContract__c>();
        vContractItems = new List<SCVendorContractItem__c>();
    
        currentPage = 'first';
        extAssignment = new SCOrderExternalAssignment__c();
        services = new List<SCVendorContractService__c>();
        sum = 0.00;
        sum2 = 0.00;
        pageNum = 1;
        showErrorMessage = false;
        errorMessage = '';
        extraServices = new List<SCOrderExternalAssignmentItem__c>();       
    }    
    
    
    /**
     * Pre list with selected services
     *
     * @return    list with selected objects (services)
     */
    public List<SCVendorContractService__c> getServicesSelected()
    {
        System.debug('####getservices selected executed;');
        List<SCVendorContractService__c> items = new List<SCVendorContractService__c>();
        for(SCVendorContractService__c s : services)
        {
            if(s.IsSelected__c)
            {
                items.add(s);
            }
        }
        return items;
    }
    
    /**
     * Reads all contracts for the selected order
     *
     * @return    list with contracts
     */
    public PageReference getListVendorContracts()
    {

        System.debug('#### getListVendorContracts: isOrderlessMode' + isOrderlessMode);        
        if (extAssignment != null)
        {
            System.debug('#### extAssignment: ' + extAssignment.Id);
        }
        if(vContracts == null || (extAssignment != null && extAssignment.Vendor__c != null))
        {
        	List<String> excludeStatus = new List<String>();
            excludeStatus.add('completed');
            vContracts = new List<SCVendorContract__c>();
            
            vContracts = [ Select Id, Name, toLabel(Status__c), Vendor__c, Vendor__r.Name 
                           From SCVendorContract__c
                           Where Vendor__c = :extAssignment.Vendor__c
                           and Status__c not in :excludeStatus
                           Order By Name ];

            System.debug('#### vContracts: ' + vContracts.size());
        }
        
        System.debug('#### 1');
        
        // Pre-selecting the first item
        if(!vContracts.isEmpty())
        {
            System.debug('#### 2');
            System.debug('#### selectedVendorContract 1: ' + selectedVendorContract);
            
            
            if(String.isBlank(selectedVendorContract))
            {
                selectedVendorContract = vContracts[0].id;
                
                System.debug('#### selectedVendorContract 2: ' + selectedVendorContract);
            }
        }

        return null;
    }

    /**
     * Reads all contracts items for the selected vendor contract
     *
     * @return    list with items
     */
    public PageReference getListVendorContractItems()
    {
        vContractItems = new List<SCVendorContractItem__c>();
        List<String> excludeStatus = new List<String>();
        excludeStatus.add('Deleted');
        if(String.isNotBlank(selectedVendorContract))
        {
            //extAssignment.VendorContract__c = selectedVendorContract;
        
            vContractItems = [ Select Id, Name, CompanyCode__c, Id2__c, MaterialGroup__c, PacketNo__c, Plant__c, toLabel(Status__c), VendorContract__c 
                               From SCVendorContractItem__c
                               Where VendorContract__c = :selectedVendorContract
                               and status__c not in :excludeStatus
                               Order By Name ];
			
            System.debug('#### vContractItems: ' + vContractItems.size() + '##' + vContractItems);
        }
        
        if(!vContractItems.isEmpty())
        {
            // Pre-selecting the first item
            if(String.isBlank(selectedVendorContractItem))
            {
                selectedVendorContractItem = vContractItems[0].id;
            }
        }

        return null;
    }
    
    /**
     * Reads all services for the selected vendor contract item
     *
     * @return    null
     */
    public PageReference getReadServices()
    {
        if(String.isNotBlank(selectedVendorContractItem))
        {
            extAssignment.VendorContractItem__c = selectedVendorContractItem;
        
            services = [ Select Id, Name, ID2__c, GrossPrice__c, VendorContractItem__c, Qty__c, Unit__c, SystemModstamp, Status__c, 
                                ServiceNumber__c, PacketNo__c, LastModifiedDate, IsSelected__c
                         From SCVendorContractService__c 
                         Where VendorContractItem__c = :selectedVendorContractItem ];
                         
            if(!services.isEmpty())
            {
                for(SCVendorContractService__c s : services)
                {
                    if(s.Qty__c == null)
                    {
                        s.Qty__c = 0;
                    }
                    
                    /*
                    if(s.Qty__c != null && s.Qty__c > 0)
                    {
                        s.IsSelected__c = true;
                    }
                    */
                }
            }
        }
        
        System.debug('#### selectedVendorContractItem: ' + selectedVendorContractItem);
        System.debug('#### services: ' + services.size());

        return null;
    }
    
    /**
     * Saves the data and makes a call to the SAP system
     */
    public PageReference save()
    {        
        if(extAssignment != null && extAssignment.Vendor__c != null && extAssignment.Order__c != null)
        {
            PageReference p = new PageReference('/' + extAssignment.Order__c);
        
            try
            {
                // First saving the assignment object
                upsert extAssignment;
                
                System.debug('#### upsert extAssignment ok');
                
                List<SCOrderExternalAssignmentItem__c> items = new List<SCOrderExternalAssignmentItem__c>();
                
                // Now saving the services if the list is not empty
                if(!services.isEmpty())
                {   
                    for(SCVendorContractService__c s : services)
                    {
                        if(s.isSelected__c)
                        {
                            SCOrderExternalAssignmentItem__c i = new SCOrderExternalAssignmentItem__c();
                            
                            i.VendorContractService__c    = s.Id;
                            i.Qty__c                      = s.Qty__c;
                            i.Unit__c                     = s.Unit__c;
                            i.Price__c                    = s.GrossPrice__c;
                            i.VendorContractItem__c       = extAssignment.VendorContractItem__c;
                            i.OrderExternalAssignment__c  = extAssignment.id;
                            
                            items.add(i);
                        }
                    }
                }
                
                if(!extraServices.isEmpty())
                {
                    for(SCOrderExternalAssignmentItem__c es : extraServices)
                    {
                        if(es.Price__c != null)
                        {
                            if(extAssignment.VendorContractItem__c != null)
                            {
                                es.VendorContractItem__c = extAssignment.VendorContractItem__c;
                            }
                            
                            es.OrderExternalAssignment__c = extAssignment.id;
                            
                            items.add(es);
                        }
                    }
                }
                
                if(!items.isEmpty())
                {
                    
                    System.debug('#### items: ' + items);
                    
                    upsert items;
                }
                
                //###### CCE ###### -->
                // Call the SAP web service 
                CCWCOrderExternalOperationAdd.callout(extAssignment.id, true, false);
                //###### CCE ###### <--

                return p;
            }
            catch(Exception e)
            {
                System.debug('#### Can not save exttern assignment: ' + e.getMessage());
                
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
                ApexPages.addMessage(myMsg);
                
                return null;
            }
        }
        
        return null;
    }
    
    /**
     * Cancels the process and returns the user to the order or personal profile
     */
    public PageReference cancel()
    {
        if(oid == null || oid == '')
        {
            oid = UserInfo.getUserId();
        }
        
        PageReference p = new PageReference('/' + oid);
        
        return p;
    }
    
    /**
     * Checks wether the order can be used and the vendor field must be required field.
     * For example if no order was selected - the user must select the order first,
     * and only after that the vendor. TRhe order status can not be Completed, Cancelled.
     */    
    public Boolean getCanProcesOrder()
    {
        if(extAssignment.Order__c != null)
        {            
            String orderStatus = [ Select Status__c From SCOrder__c Where Id = :extAssignment.Order__c ].Status__c;
            String orderStatusLabel = [ Select toLabel(Status__c) From SCOrder__c Where Id = :extAssignment.Order__c ].Status__c;
            
            // DOMVAL_ORDERSTATUS_NOT_COMPLETED = '5501,5502,5503,5509,5510';
            if(SCfwConstants.DOMVAL_ORDERSTATUS_NOT_COMPLETED.contains(orderStatus))
            {
                showErrorMessage = false;
                errorMessage = '';
                return true;
            }
            else
            {
                System.debug('#### Can not process order because of status: ' + orderStatus);
                
                errorMessage = Label.SC_msg_InvalidOrderStatus + ': ' + orderStatusLabel;
                showErrorMessage = true;
                
                return false;
            }
        }
        
        return false;
    }
    
    /**
     * Checks wether the order can be used
     */  
    public PageReference checkOrder()
    {       
        canDoOrder = getCanProcesOrder();
    
        return null;
    }
    
    /**
     * List with options 
     */ 
    public List<SelectOption> getNumberOfExtraServicesToAdd() 
    {
        List<SelectOption> options = new List<SelectOption>();
        for(Integer i = 1; i < 5; i++)
        {
            options.add(new SelectOption(String.valueOf(i),String.valueOf(i)));
        }
        return options;
    }
    
    public PageReference addExtraService()
    {
        for(Integer i = 1; i <= selectedNumberExtraServicesToAdd; i++)
        {
            SCOrderExternalAssignmentItem__c service = new SCOrderExternalAssignmentItem__c(Qty__c = 1);
            extraServices.add(service);
        }
        
        return null;
    }
    
    public PageReference removeExtraService()
    {
        extraServices.remove(selectedExtraServiceToDelete);
        
        return null;
    }
    
    public PageReference goNext()
    {
        //PMS38340 , at least on Vendor Contract Services should be selected  21.07.2014 GMSSW
        //PMS38467 , cancel the change made in PMS38340
        //boolean serviceSelected = false;
        /* for(SCVendorContractService__c ser:services)
        {
            if(ser.IsSelected__c == true)
            {
                serviceSelected = true;
            }
        }
           
        if(!serviceSelected)
        {
            currentPage = 'first';
            pageNum = 1;
            errorMsgForNoItemSelected = Label.SC_msg_NoVCSSelected;
            showErrorMsgForNoItem = true;
            return null;
            
        }
		*/
        errorMsgForNoItemSelected = null;
        showErrorMsgForNoItem  = false;
        if(String.isNotBlank(selectedVendorContract))
        {
            extAssignment.VendorContract__c = selectedVendorContract;
        }
        
        if(String.isNotBlank(selectedVendorContractItem))
        {
            extAssignment.VendorContractItem__c = selectedVendorContractItem;
        }
        
            
        return null;
    }
    

}
