/*
 * @(#)SCboOrderLine.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Christoph Stein <cstein@salesforce.com>
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCboOrderLine
{
    private static SCApplicationSettings__c applicationSettings = SCApplicationSettings__c.getInstance();
    private static SCfwDomain domMatStat = new SCfwDomain('DOM_MATERIALMOVEMENT_STATUS');
    
    public SCOrderLine__c orderLine { get; set; }
    public String matStatDescr { get; set; }
    public Boolean isSelected { get; set; }
    public Boolean canChangeValType { get; set; }
    /*
     * If the valuation type is set by a selected list (incl "null")
     */
    private static Map<String,String> mapValuationType = new Map<String,String>();
    public String getValTypeViaSelectedList()
    {
        try
        {
            if ((orderLine != null) || (orderLine.ValuationType__c != null))
            {
                return orderLine.ValuationType__c.toLowerCase();
            }
        }
        catch (Exception e) { }
        return '-none-';
    }
    
    public void setValTypeViaSelectedList(String valType)
    {
        if ((valType != null) && (!valType.equalsIgnoreCase('-none-')))
        {
            if ((mapValuationType == null) || (mapValuationType.size() == 0))
            {
                for (Schema.PicklistEntry schemaPickEntry : SCOrderLine__c.ValuationType__c.getDescribe().getPicklistValues())
                {
                    mapValuationType.put(schemaPickEntry.getValue().toLowerCase(), schemaPickEntry.getValue());
                }
            }
            orderLine.ValuationType__c = mapValuationType.get(valType.toLowerCase());
        }
        else
        {
            orderLine.ValuationType__c = null;
        }
    }
     

    /**
     * The order item this order line belongs to.
     *
     * Order lines can be attached to either the order item or 
     * the order itself
     */
    public String itemIdentifier  
    { 
        get
        {
            if (this.itemIdentifier == null)
            {
                if (orderLine.OrderItem__r != null &&
                    orderLine.OrderItem__r.InstalledBase__r.ProductModel__r.Name != null)
                {
                    this.itemIdentifier = orderLine.OrderItem__r.InstalledBase__r.ProductModel__r.Name;
                }
                else
                {
                    this.itemIdentifier = orderLine.Order__r.Name;
                }
            }
            
            return this.itemIdentifier;
        }
        set; 
    }

    /**
     * The name of the symbol for displaying the material status.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public String symMatStat
    { 
        get
        {
            // first set an empty symbol
            symMatStat = 'matstat_012.png';
            matStatDescr = '';
            
            String matStat = orderline.MaterialStatus__c;
            if (null != matStat)
            {
                SCfwDomainValue domVal = domMatStat.getDomainValue(matStat);
                String matStatV4 = domVal.getControlParameter('V4');
                matStatDescr = domVal.getText(UserInfo.getLanguage());

                if ((null != matStatV4) && (matStatV4.length() > 0))
                {
                    symMatStat = 'matstat_0';
                    if (matStatV4.length() == 1)
                    {
                        symMatStat += '0';
                    } // if (matStatV4.length() == 1)
                    symMatStat += matStatV4 + '.png';
                } // if ((null != matStatV4) && (matStatV4.length() > 0))
            } // if (null != matStat)
            
            return symMatStat;
        }
        set; 
    } // symMatStat

    /**
     * Indicator for editing the order line during oder processing.
     * If it say the line is not editable, there are still fields which can be changed.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public Boolean isNotEditable
    {
        get
        {
            System.debug('#### isNotEditable(): getter -> ' + orderline.Id);
            isNotEditable = false;
            
            String matStat = orderline.MaterialStatus__c;
            System.debug('#### isNotEditable(): matStat -> ' + matStat);
            if (null != matStat)
            {
                SCfwDomainValue domVal = domMatStat.getDomainValue(matStat);
                Integer matStatV5 = domVal.getControlParameterInt('V5');
                System.debug('#### isNotEditable(): matStatV5 -> ' + matStatV5);
                isNotEditable = ((matStatV5 & 2) == 2);
            } // if (null != matStat)
            
            return (isNotEditable || orderLine.Invoiced__c);
        }
        set; 
    } // isNotEditable

    /**
     * Indicator for editing the order line during oder processing.
     * If it say the line is not editable, all filds are read only.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public Boolean isReadOnly
    {
        get
        {
            System.debug('#### isReadOnly(): getter -> ' + orderline.Id);
            
            String artNo = applicationSettings.ORDERLINE_ARTNO_PAYMENT__c;
            String autoPos = orderLine.AutoPos__c;
            System.debug('#### isReadOnly(): autoPos -> ' + autoPos);
            return ((((null != autoPos) && autoPos.equals(SCfwConstants.DOMVAL_AUTOPOSTYPE_COMBINED)) && 
                     (!SCBase.isSet(artNo) || !artNo.equals(orderLine.Article__r.Name))) || 
                    orderLine.Invoiced__c || orderLine.ContractRelated__c);
        }
        set; 
    } // isReadOnly

    /**
     * Indicator for deleting the order line during oder processing.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public Boolean isDeletable
    {
        get
        {
            System.debug('#### isDeletable(): getter -> ' + orderline.Id);
            
            String artNo = applicationSettings.ORDERLINE_ARTNO_PAYMENT__c;
            String autoPos = orderLine.AutoPos__c;
            String matStat = orderLine.MaterialStatus__c;
            System.debug('#### isDeletable(): autoPos -> ' + autoPos);
            return (((null == autoPos) || !autoPos.equals(SCfwConstants.DOMVAL_AUTOPOSTYPE_COMBINED) || 
                     !SCBase.isSet(artNo) || artNo.equals(orderLine.Article__r.Name)) && 
                    !orderLine.Invoiced__c && !orderLine.ContractRelated__c && 
                    (!SCBase.isSet(matStat) || (SCfwConstants.DOMVAL_MATSTAT_NOMATERIAL == matStat) || 
                     (SCfwConstants.DOMVAL_MATSTAT_ORDERABLE == matStat)));
        }
        set; 
    } // isDeletable

    /**
     * Default constructor using default values from application settings
     *
     * @author Christoph Stein <cstein@salesforce.com>
     * @author Thorsten Klein <tklein@gms-online.de>
     * @return Order line business object
     */
    public SCboOrderLine()
    {
        isSelected = false;
        canChangeValType = true;
        this.orderLine = new SCOrderLine__c();
        setDefaultValues();
    }
    
    /**
     * Create a new order line BO using a given parameter set using 
     * default values for invoicing type, invoicing sub type and
     * order line type. 
     * If you want to get these values from the order use a different 
     * constructor.
     *
     * @param orderId      SFDC order ID this order line belongs to.
     * @param orderItemId  SFDC order item ID this order belongs to, could be <code>null</code>
     * @param articleId    SFDC article ID for this order line which is used for the pricing
     * @param assignmentId SFDC assignment ID that works on this order
     * @return Order Line business object
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public SCboOrderLine(Id orderId, Id orderItemId, Id articleId, Id assignmentId)
    {
        isSelected = false;
        canChangeValType = true;
        this.orderLine = new SCOrderLine__c(Article__c = articleId,
                                            Assignment__c = assignmentId,
                                            Order__c = orderId,
                                            OrderItem__c = orderItemId,
                                            InvoicingType__c = applicationSettings.DEFAULT_INVOICINGTYPE__c,
                                            InvoicingSubType__c = applicationSettings.DEFAULT_INVOICINGSUBTYPE__c,
                                            Type__c = SCfwConstants.DOMVAL_ORDERLINETYPE_INV,
                                            Qty__c = 1,
                                            PriceType__c = SCfwConstants.DOMVAL_CONDPRICETYPE_PRICE, 
                                            MaterialStatus__c = SCfwConstants.DOMVAL_MATSTAT_NOMATERIAL
                                           );
    }

    /**
     * Create a new order line BO using a given parameter set where 
     * the values for invoicing type, invoicing sub type are used
     * from the corresponding order
     * If you want to get these values from the order use a different 
     * constructor.
     *
     * @param order        Order object this order line belongs to (SCOrder__c).
     * @param orderItemId  SFDC order item ID this order belongs to, could be <code>null</code>
     * @param articleId    SFDC article ID for this order line which is used for the pricing
     * @param assignmentId SFDC assignment ID that works on this order
     * @return Order Line business object
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public SCboOrderLine(SCOrder__c order, Id orderItemId, Id articleId, Id assignmentId)
    {
        isSelected = false;
        canChangeValType = true;
        this.orderLine = new SCOrderLine__c(Article__c = articleId,
                                            Assignment__c = assignmentId,
                                            Order__c = order.Id,
                                            Order__r = order,
                                            OrderItem__c = orderItemId,
                                            InvoicingType__c = order.InvoicingType__c,
                                            InvoicingSubType__c = order.InvoicingSubType__c,
                                            Type__c = SCfwConstants.DOMVAL_ORDERLINETYPE_INV,
                                            Qty__c = 1,
                                            PriceType__c = SCfwConstants.DOMVAL_CONDPRICETYPE_PRICE, 
                                            MaterialStatus__c = SCfwConstants.DOMVAL_MATSTAT_NOMATERIAL
                                           );
    }
    
    /**
     * Create a new order line business object from a given order line object
     *
     * @param orderLine Existing order line object (SCOrderLine__c)
     * @return Order Line business object
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public SCboOrderLine(SCOrderLine__c orderLine)
    {
        isSelected = false;
        canChangeValType = true;
        this.orderLine = orderLine;
    }

    /**
     * Set default values for new order lines
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    private void setDefaultValues()
    {
        orderLine.InvoicingType__c = applicationSettings.DEFAULT_INVOICINGTYPE__c;
        orderLine.InvoicingSubType__c = applicationSettings.DEFAULT_INVOICINGSUBTYPE__c;
        orderLine.Type__c = SCfwConstants.DOMVAL_ORDERLINETYPE_INV;
        orderLine.Qty__c = 1;
        orderLine.PriceType__c = SCfwConstants.DOMVAL_CONDPRICETYPE_PRICE;
        orderline.MaterialStatus__c = SCfwConstants.DOMVAL_MATSTAT_NOMATERIAL;
    }
    /**
     * Save all 
     *
     */
    public void save()
    {
        try
        {
            //#### Update order item ###
            upsert orderLine;
        }
        catch (DmlException e)
        {
            throw new SCfwOrderException('Unable to save order line ' + e.getMessage());
        }
    }    

    /**
     * Reads all order lines for an order
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public List<SCOrderLine__c> readByOrder(Id orderId)
    {
        return [select Id, Name, ArticleName__c, Qty__c, ListPrice__c, OrderItem__c, 
                       PriceType__c, Order__c, InvoicingType__c, InvoicingSubtype__c, 
                       Article__c, UnitPrice__c, Tax__c, TaxRate__c, Type__c, Price__c,
                       PricingDate__c, PositionPrice__c, Assignment__c, UnitNetPrice__c, 
                       RelativeDiscount__c, Discount__c, ManualListPrice__c, 
                       ManualPricing__c, ManualUnitNetPrice__c, ManualPrimeCost__c, 
                       ManualTax__c, ContractRelated__c, PriceGross__c, ValuationType__c,
                       MaterialStatus__c, AutoPos__c, Article__r.Id, Article__r.Name, 
                       Article__r.ArticleNameCalc__c, Article__r.Text_en__c, 
                       Article__r.SalesUnit__c, Article__r.SalesUnitQty__c, 
                       toLabel(Article__r.Class__c), toLabel(Article__r.Type__c), 
                       Article__r.ReplacedBy__c, Article__r.ReplacedBy__r.Name, 
                       toLabel(Article__r.LockType__c), Article__r.ReturnType__c, 
                       Article__r.TaxType__c, Article__r.PriceGroup__c, Article__r.ValuationType__c, 
                       Article__r.Orderable__c, Article__r.Stockable__c
                  from SCOrderLine__c 
                where Order__c = :orderId];         
    } // readByOrder
}
