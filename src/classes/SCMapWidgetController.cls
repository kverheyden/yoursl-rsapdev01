/**
 * @(#)SCMapWidgetController
 * Copyright 2012 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

 /**
 * Controller for SCMapWidget.component
 * Allows to sign a GoogleStreetView Url
 *
 * @author Eugen Tiessen <etiessen@gms-online.de>
 * @version $Revision$, $Date$
 */

public class SCMapWidgetController 
{
	
	public String lat {get; set;}
	public String lng {get; set;}
	public String sizeX {get; set;}
	public String sizeY {get; set;}
	
	private static final String KEY_STRING = SCApplicationSettings__c.getInstance().GOOGLE_GEOCODE_PRIVATE_KEY__c;
	private static final String CLIENT = SCApplicationSettings__c.getInstance().GOOGLE_GEOCODE_CLIENTID__c;
	private static final String STREET_VIEW_URL_PREFIX = 'https://maps.googleapis.com/maps/api/streetview?fov=90&pitch=10&sensor=false&client=' + CLIENT;
	//&size=1500x1500&location=51.5078808%2C-0.29828229999998257&';
	
	public SCMapWidgetController()
	{
		
	}
	
	/**
	 * Sign a streetview url
	 *
	 * @return a signed streetview image url
	 */
	public String getSignedStreetViewImageUrl()
	{
		String sizeEncoded = (sizeX != null && sizeY != null) ? ('&size=' + EncodingUtil.urlEncode(sizeX + 'x' + sizeY, 'UTF-8')) :'';  
		String locationEncoded = (lat != null && lng != null) ? ('&location=' + EncodingUtil.urlEncode(lat + ',' + lng, 'UTF-8')) :'';
		
		String urlString = STREET_VIEW_URL_PREFIX + sizeEncoded + locationEncoded;
		
		URL url = new URL(urlString);

	    UrlSigner signer = new UrlSigner(KEY_STRING);
	    String request = signer.signRequest(url.getPath(),url.getQuery());
	
	    system.debug('Signed URL :' + url.getProtocol() + '://' + url.getHost() + request);
	    return url.getProtocol() + '://' + url.getHost() + request;
	}
	
	/**
	 * Helper Class to sign google urls
	 * 
	 */
	public class UrlSigner {

  	
		private Blob key;

 		/**
 		 * Constructor
 		 * @param keyString the google private key
 		 *
 		 */
 		
		public UrlSigner(String keyString)
		{
		    // Convert the key from 'web safe' base 64 to binary
		    keyString = keyString.replace('-', '+');
		    keyString = keyString.replace('_', '/');
		    system.debug('Key: ' + keyString);
		    this.key = EncodingUtil.base64Decode(keyString);
		}

		/**
		 * Sign a uri
		 * @param path the path of the url (e.g. /maps/api/...)
		 * @param query the parameters (e.g. location=10,25&sensor=false)
		 *
		 * @return a uri with a google signature
		 */
		public String signRequest(String path, String query) {
		
		    // Retrieve the proper URL components to sign
		    String resource = path + '?' + query;
		
		    
		    Blob sigBytes = Crypto.generateMac('HMacSHA1',Blob.valueOf(resource),this.key);
		    
		    // base 64 encode the binary signature
		    String signature = EncodingUtil.base64Encode(sigBytes);
		
		    // convert the signature to 'web safe' base 64
		    signature = signature.replace('+', '-');
		    signature = signature.replace('/', '_');
		
		    return resource + '&signature=' + signature;
		}
	}

}
