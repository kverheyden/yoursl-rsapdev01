/**
* @author           Oliver Preuschl
*
* @description      Send the data of a SCOrder/SCOrderItem to SAP
*
* @date             27.08.2014
*
* Timeline:
* Name              DateTime                Version        Description
* Oliver Preuschl   27.08.2014              *1.0*          Created
*/

public with sharing class fsFASCOrderCreate {
	
	//The maximum number of SCOrders that will be send to SAP in one execution
	private static final Integer MAX_SCORDER_NUMBER = 5;

	//Get the relevant SCOrders and send the data to SAP
	public static void sendSCOrdersToSAP( Map< Id, SCORderItem__c > PM_OldSCOrderItems, Map< Id, SCORderItem__c > PM_NewSCOrderItems ){
		System.debug( '### - Start sendSCOrdersToSAP, PM_OldSCOrderItems: ' + PM_OldSCOrderItems + ', PM_NewSCOrderItems: ' + PM_NewSCOrderItems );

		//Get the SCOrders
		List< Id > LL_SCOrderItemIds = getSCOrderItemsWithUpdatedSerialnumber( PM_OldSCOrderItems, PM_NewSCOrderItems );
		List< Id > LL_SCOrderIds = getCDERelatedSCOrders( LL_SCOrderItemIds );
		//Process only 5 orders at a time
		if( LL_SCOrderIds.size() > MAX_SCORDER_NUMBER ){
			LL_SCOrderIds = getTopListRecords( LL_SCOrderIds, MAX_SCORDER_NUMBER );
		}
		//Send the SCOrders to SAP
		if( LL_SCOrderIds.size() > 0 ){
			sendSCOrdersToSAP( LL_SCOrderIds );
		}

		System.debug( '### - Stop sendSCOrdersToSAP' );
	}

	//Get the SCOrderItems that had their serialnumber updated
	private static List< Id > getSCOrderItemsWithUpdatedSerialnumber( Map< Id, SCORderItem__c > PM_OldSCOrderItems, Map< Id, SCORderItem__c > PM_NewSCOrderItems ){
		System.debug( '### - Start getSCOrderItemsWithUpdatedSerialnumber, PM_OldSCOrderItems: ' + PM_OldSCOrderItems + ', PM_NewSCOrderItems: ' + PM_NewSCOrderItems );

		List< Id > LL_SCOrderItemIds = new List< Id >();
		for( Id LV_SCOrderItemId: PM_NewSCOrderItems.KeySet() ){
			SCOrderItem__c LO_NewSCOrderItem = PM_NewSCOrderItems.get( LV_SCOrderItemId );
			SCOrderItem__c LO_OldSCOrderItem = PM_OldSCOrderItems.get( LV_SCOrderItemId );
			if( ( !String.isBlank(LO_NewSCOrderItem.cce_SerialNo__c) ) && ( String.isBlank(LO_OldSCOrderItem.cce_SerialNo__c) ) ){
				LL_SCOrderItemIds.add( LV_SCOrderItemId );
			}
		}

		System.debug( '### - Stop getSCOrderItemsWithUpdatedSerialnumber' );

		return LL_SCOrderItemIds;
	}

	//Get the SCOrders for the specified SCOrderItems that were generated from a CDEOrder and have not yet been send to SAP
	private static List< Id > getCDERelatedSCOrders( List< Id > PL_SCOrderItemIds ){
		System.debug( '### - Start getCDERelatedSCOrders, PL_SCOrderItemIds: ' + PL_SCOrderItemIds );

		List< Id > LL_SCOrderIds = new List< Id >();
		List< SCOrderItem__c > LL_SCOrderItems = [ SELECT id, Order__c 
														FROM SCOrderItem__c 
														WHERE ( Id IN: PL_SCOrderItemIds )
															AND ( Order__r.ERPStatusOrderCreate__c = 'none' ) 
															AND ( ( Order__r.ERPOrderNo__c = null ) OR ( Order__r.ERPOrderNo__c = '' ) )
															AND ( ( InstalledBase__r.SerialNo__c != null ) AND ( InstalledBase__r.SerialNo__c != '' ) )
															AND ( Order__r.isCDEOrder__c = true ) 
												  ];
		for( SCOrderItem__c LO_SCOrderItem: LL_SCOrderItems ){
			LL_SCOrderIds.add( LO_SCOrderItem.Order__c );
		}

		System.debug( '### - Stop getCDERelatedSCOrders' );

		return LL_SCOrderIds;
	}

	//Send the SCOrders to SAP
	private static void sendSCOrdersToSAP( List< Id > PL_SCOrderIds ){
		System.debug( '### - Start sendSCOrdersToSAP, PL_SCOrderIds: ' + PL_SCOrderIds );

		for( Id LV_SCOrderId: PL_SCOrderIds ){
			CCWCOrderCreate.callout( LV_SCOrderId, true, false );
		}

		System.debug( '### - Stop sendSCOrdersToSAP' );
	}

	//Get the top n Records of a specified list
	private static List< Id > getTopListRecords( List< Id > LL_List, Integer LV_NumberOfRecords ){
		System.debug( '### - Start getTopListRecords, LL_List: ' + LL_List + ', LV_NumberOfRecords: ' + LV_NumberOfRecords );

		List< Id > LL_NewList = new List< Id >();
		if ( LL_List.size() > LV_NumberOfRecords ){
			for( Integer i = 0; i < LV_NumberOfRecords; i++ ){
				LL_NewList.add( LL_List.get( i ) );
			}
		}else{
			LL_NewList = LL_List;
		}

		System.debug( '### - Stop getTopListRecords' );

		return LL_NewList;
	}


}
