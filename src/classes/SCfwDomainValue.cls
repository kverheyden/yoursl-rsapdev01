public class SCfwDomainValue
{
    private SCDomainValue__c domainValue;
    
    /**
     * map locale to description object
     * <Locale, SCDomainDescription__c>
     */    
    private Map<String,SCDomainDescription__c> description = new Map<String,SCDomainDescription__c>();

    /**
     * Default constructor
     */
    public SCfwDomainValue()
    {
        
    }

    /**
     * Create a new domain value 
     *
     * @param domVal domain value object to store
     * @param translation description object with translated content
     * @param locale locale used for the translation
     */
    public SCfwDomainValue(SCDomainValue__c domVal, SCDomainDescription__c translation, String locale)
    {
        this.domainValue = domVal;
        this.description.put(locale,translation);
    }

    /**
     * Create a new domain value 
     *
     * @param domVal domain value object to store
     */
    public SCfwDomainValue(SCDomainValue__c domval)
    {
        this.domainValue = domVal;
    }

    /**
     * Save (upsert) domain value object
     */ 
    public void save()
    {
        try
        {
            upsert this.getObject();
            
            List<SCDomainDescription__c> d = description.values();
            
            upsert d;
        }
        catch (DmlException e)
        {
            throw new SCfwDomainException('Unable to save object: ' + 
                e.getMessage());
        }
    }


    /**
     * Get all control parameter for this domain 
     *
     * @param domainValue Id of this domain (as String although it's a number)
     * @return List of all controll parameters (V1=xxx, V2=yyy)
     */ 
    public List<String> getControlParameter()
    {
        List<String> controlValue = new List<String>();
        String controlParameters = this.getObject().ControlParameter__c;
        System.debug('##### getControlParameter(): paramString -> ' + controlParameters);
        
        if ((null != controlParameters) && (controlParameters.length() > 0))
        {
            controlValue = controlParameters.split('\\|');
        }
        
        return controlValue;
    }

    /**
     * Get all control parameter for this domain 
     *
     * @return All control parameters concatenated
     */ 
    public String getControlParameterAsString()
    {
        if (this.getObject() <> null)
        {
            return this.getObject().ControlParameter__c;
        }
        
        return null;
    }
    
    
    /**
     * Get a specific control parameter (e.g. V1 or V2) for the given domain value
     * 
     * Since we use the domain value as Picklist values we stored them as
     * String although they're numerical values actually.
     *
     * @param domainValue Id of this domain
     * @param controlParam control parameter to fetch (e.g. V1, V2, etc)
     * @return control parameter which could be a comma separated list or a 
     *         single value
     */ 
    public String getControlParameter(String controlParam)
    {
        String controlValue = '';

        if (this.getObject() == null || 
            controlParam == null || controlParam.length() == 0)
        {
            return null;
        }

        List<String> tmp = getControlParameter();
         
        System.debug(LoggingLevel.FINER, 
            '#####getControlParameter(): domainValue -> ' + getID());
        System.debug(LoggingLevel.FINER, 
            '#####getControlParameter(): controlParam -> ' + controlParam);
        System.debug(LoggingLevel.FINER, 
            '#####getControlParameter(): tmp -> ' + tmp);
        
        for (String paramString : tmp)
        {
            System.debug(LoggingLevel.FINER, 
                         '#####getControlParameter(): paramString -> ' + paramString);
                        
            if ( !paramString.startsWith(controlParam) )
            {
                continue;
            }
            
            Integer pos = paramString.indexOf('=');

            //System.debug(paramString.length() + '+++' + pos);

            // REMIND: .length() starts with 1 and pos with 0                       
            if (paramString.length() > pos + 1)
            {
                controlValue = paramString.substring(pos + 1);
            }
            
            // I've got what I wanted, get me out of here
            return controlValue;
        }

        return controlValue;   
    }
    
    /**
     * Get a specific control parameter (e.g. V1 or V2) for the given domain value
     * as an integer value.
     * If the value is not set, we get -1.
     *
     * @param controlParam control parameter to fetch (e.g. V1, V2, etc)
     * @return control parameter as an integer value
     */ 
    public Integer getControlParameterInt(String controlParam)
    {
        String value = getControlParameter(controlParam);
        if ((null == value) || (value.length() == 0))
        {
            return -1;
        } // if ((null == value) || (value.length() == 0))
        return Integer.valueOf(value);
    } // getControlParameterInt
    
    /**
     * Get the n-th value (1 based) from the list of a control parameter for the 
     * given domain value
     * 
     * Example:
     * domain: 51601
     * controlParameter: V1
     * position: 2
     * 51601 -> V1=1234,2345,3456|V2=6543,5432,4321
     * returns 2345
     * 
     * @param domainValue Id of this domain
     * @param controlParam control parameter to fetch (e.g. V1, V2, etc)
     * @return n-th value (1 based) from control parameter
     */ 
    public String getControlParameterValue(String controlParam,
                                           Integer position)
    {
        String paramValue = '';
        String controlValue = getControlParameter(controlParam);
        
        if (controlValue.length() == 0)
        {
            return paramValue;
        }
        
        List<String> tmp = controlValue.split(',');
        if (tmp.size() < position)
        {
            return paramValue;
        }       

        paramValue = tmp.get(position - 1);
        
        return paramValue;
    }
    
    /**
     * Set the complete control parameter string
     *
     * @param controlParameter complete controlparameter string
     */
    public void setControlParameter(String controlParameter)
    {
        System.debug(LoggingLevel.FINER, 
                     '##### setControlParameter(): paramString -> ' + controlParameter);
        this.getObject().ControlParameter__c = controlParameter;
    }


    /**
     * Set a given control parameter
     *
     * @param controlParameter control parameter
     * @param value value of this parameter
     */
    public void setControlParameter(String controlParameter, String value)
    {
        String controlString = '';

        if (this.getObject() == null || 
            controlParameter == null || controlParameter.length() == 0)
        {
            return;
        }

        List<String> tmp = getControlParameter();
        
        for (Integer i = 0; i < tmp.size(); i++)
        {
            String paramString = tmp.get(i);
            System.debug(LoggingLevel.FINER, 
                         '#####getControlParameter(): paramString -> ' + paramString);
                        
            if ( !paramString.startsWith(controlParameter) )
            {
                continue;
            }
         
            paramString = controlParameter + '=' + value;
            
            tmp.set(i, paramString);   
        }
        
        for (Integer i = 0; i < tmp.size(); i++)
        {
            controlString += tmp.get(i);
            
            if (i < tmp.size() - 1)
            {
                controlString += '|';
            }
        }
        
        setControlParameter(controlString);

        save();
    }


    /**
     * Get all locales fetches so far for this domain value
     *
     * @return all available locales or an empty set otherwise
     */
    public Set<String> getLocales()
    {
        try
        {
            return description.keySet();
        }
        catch (Exception e)
        {
            return new Set<String>();
        }
        
    }
 
    /**
     * Get ID2 from current domain value
     *
     * @return ID2 string
     */
    public String getId2 ()
    {
        String id2 = '';
        try
        {
            id2 = this.domainValue.ID2__c;
        }
        catch (Exception e)
        {
        
        }

        return id2; 
    }


    /**
     * Get Constant from current domain value
     *
     * @return Constant string
     */
    public String getConstant ()
    {
        String constant = '';
        try
        {
            constant = this.domainValue.Constant__c;
        }
        catch (Exception e)
        {
        
        }

        return constant; 
    }


    /**
     * Get domain ID from current domain value
     *
     * @return ID string
     */
    public String getID ()
    {
        String id = '';
        try
        {
            id = domainValue.Name;
        }
        catch (Exception e)
        {

        }
        
        return id;
    }

    /**
     * Get the SCDomainValue object
     *
     * @return domain value object
     */
    public SCDomainValue__c getObject()
    {
        return this.domainValue;    
    }
    
    
    /**
     * Get the available translation or fetch it from the database
     *
     * @param locale the locale for the translation to fetch
     * @return the translation object
     */
    public SCDomainDescription__c getTranslation(String locale)
    {
        String retLocale = locale;
        
        if (!this.description.containsKey(locale))
        {
            retLocale = locale.substring(0, 2);
            if (!this.description.containsKey(retLocale))
            {
                SCDomainDescription__c translation = fetchTranslation(locale);
                this.description.put(translation.LocaleSidKey__c, translation);
            }
        }
        
        return this.description.get(retLocale);
    }
    
    /**
     * Get the available translation without fetching it from the database
     *
     * @param locale the locale for the translation to fetch
     * @return the translation object
     */
    public SCDomainDescription__c getTranslationEx(String locale)
    {
        String retLocale = locale;
        
        if (!this.description.containsKey(locale))
        {
            retLocale = locale.substring(0, 2);
            if (!this.description.containsKey(retLocale))
            {
                return null;
            }
        }
        
        return this.description.get(retLocale);
    }

    /**
     * Get the translated short text version
     *
     * @param locale that the text shall belong to
     * @return the actual text in the corresponding locale
     */
    public String getText(String locale)
    {
        try
        {
            SCDomainDescription__c descr = getTranslation(locale);
            return ((null != descr) && (null != descr.Text__c)) ? descr.Text__c : '';
        }
        catch (SCfwDomainException ex)
        {
            if(domainValue.Name != null)
            {
                return '' + domainValue.Name + ' (' + locale + ') ';
            }
            return locale;
        }
    }

    /**
     * Get the translated short text version
     *
     * @param locale that the text shall belong to
     * @return the actual text in the corresponding locale
     */
    public String getTextEx(String locale)
    {
        try
        {
            SCDomainDescription__c descr = getTranslationEx(locale);
            if (null != descr)
            {
                return (null != descr.Text__c) ? descr.Text__c : '';
            }
            else if (domainValue.Name != null)
            {
                return '' + domainValue.Name + ' (' + locale + ') ';
            }
            return '' + domainValue.Id + ' (' + locale + ') ';
        }
        catch (SCfwDomainException ex)
        {
            if(domainValue.Name != null)
            {
                return '' + domainValue.Name + ' (' + locale + ') ';
            }
            return locale;
        }
    }

    /**
     * Add a new translation of a domain value for the locale in the object
     *
     * @param domainId id of the domain value
     * @param locale locale to use
     * @param textShort short text version
     * @param textLong long text version
     */
    public void addTranslation (String locale, 
                                String textShort, String textLong)
    {
        String id2 = 'DOM-' + domainValue.Name + '-' + locale;

        try 
        {
            SCfwTranslation translation = new SCfwTranslation();
            
            translation.setTranslation(domainValue.Id, locale, id2, textShort, textLong);

            save();
        }
        catch (SCfwTranslationException e)
        {
            throw new SCfwDomainException(true, 'Unable to insert translation for ' + domainValue.Name );
        }

    }
    
    /**
     * Set translation of a domain value for the locale
     *
     * @param translation translation object
     * @param locale locale to use
     */
    public void setTranslation(SCDomainDescription__c translation, String locale)
    {
        this.description.put(locale, translation);
    }
    
    /**
     * Get the translation for the given domain ID and locale
     *
     * @param domainId ID of the domain value
     * @param locale wanted locale
     */
    private SCDomainDescription__c fetchTranslation (String locale)
    {
        SCDomainDescription__c descr;

        try
        {
            SCfwTranslation translation = new SCfwTranslation();
            descr = new SCDomainDescription__c(Text__c = 'unknown', 
                                               LocaleSidKey__c = locale);
            if (null != this.domainValue)
            {
                if (SCBase.isSet(this.domainValue.Id))
                {
                    descr = translation.getTranslation(this.domainValue.Id, locale);
                }
                else
                {
                    descr = new SCDomainDescription__c(Text__c = this.domainValue.Name, 
                                                       LocaleSidKey__c = locale);
                }
            }
        }
        catch (SCfwTranslationException e)
        {
            throw new SCfwDomainException(true, 'Unable to fetch translation for ' + domainValue.Name );
        }
        return descr;
    }
}
