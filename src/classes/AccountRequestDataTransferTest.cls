/**
* @author		Austen Buennemann
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				a.buennemann@yoursl.de
*
* @description	UnitTest for AccountRequestDataTransfer
* 				Transfer the following objects from the Request to the Account:
*				MarketingAttribute of request__c
*				CdeOrder
*				SalesOrder__c
*				Visitation
*				RedSurveyResult
*
* @date			03.09.2014
*
* Timeline:
* Name               DateTime                  Description
* Austen Buennemann  03.09.2014 15:14          Class created
* Andre Mergen		 11.09.2014				   pushed up to 96% Coverage
*/

@isTest
private class AccountRequestDataTransferTest {
	
    static testMethod void myUnitTest() {
    	
    	String recTypte_create; 
		String recTypte_update;
		String recTypte_SalesOrder;
		recTypte_create = [Select Id From RecordType Where SobjectType = 'Request__c' AND DeveloperName = 'NewCustomerRegistration' LIMIT 1].Id; 
		recTypte_update = [Select Id From RecordType Where SobjectType = 'Request__c' AND DeveloperName = 'CustomerMasterDataUpdate' LIMIT 1].Id;
		recTypte_SalesOrder = [Select Id From RecordType Where SobjectType = 'SalesOrder__c' AND DeveloperName = 'IndirectOrder' LIMIT 1].Id;
		String recTypeDefault = [Select Id From RecordType Where SobjectType = 'Request__c' LIMIT 1].Id;
    	List<Account>  ListAccount = new List<Account>();
    	
        SalesAppSettings__c settingSalesApp = new SalesAppSettings__c();
        settingSalesApp.Name = 'RequestMasterDataUpdateRecordTypeId';
        settingSalesApp.Value__c = recTypte_update;
        insert settingSalesApp;
        

/************************************************************************************************	
										MarketingAttribute
************************************************************************************************/		
		system.debug('MarketingAttribute');
		// create MarketingAttribute
		MarketingAttribute__c tmpMA1 = new MarketingAttribute__c();
		tmpMA1.UniqueKey__c = 'tmpMA1';
		insert tmpMA1;		
 
/************************************************************************************************	
										Request
************************************************************************************************/
        system.debug('Request');
        // create Request
        Request__c tmpReq = new Request__c();
        tmpReq.MarketingAttributeIDsCSV__c = tmpMA1.Id ;
		tmpReq.OutletCompanyName__c 					= 'Muster GmbH';
		tmpReq.ShipToCity__c 							= 'Musterhausen';
		tmpReq.PayerEmail__c 							= 'abc@def.de';
		tmpReq.MainBrandBeer__c 						= 'MainBrandBeer';
		tmpReq.ContractObligationBeer__c 				= 'ContractObligationBeer';
		tmpReq.EndDateBeerContractBinding__c 			= Date.today();
		tmpReq.MainBrandWheatBeer__c 					= 'MainBrandWheatBeer';
		tmpReq.MainBrandCola__c 						= 'MainBrandCola__c';
		tmpReq.ContractObligationCola__c 				= 'ContractObligationCola__c';
		tmpReq.EndDateColaContractBinding__c 			= Date.today();
		tmpReq.MainBrandWater__c 						= 'MainBrandWater';
		tmpReq.SecondMainBrandWater__c 				= 'SecondMainBrandWater';
		tmpReq.ContractObligationWater__c 				= 'ContractObligationWater';
		tmpReq.EndDateWaterContractBinding__c 	 		= Date.today();
		tmpReq.PricingStartDate__c 					= Date.today();
		//tmpReq.RequestProcessed__c 					= true;		        
        
        insert tmpReq;

/************************************************************************************************	
										CdeOrder
************************************************************************************************/      
        system.debug('CdeOrder__c');
        // create CdeOrder__c
        CdeOrder__c tmpCDEO = new CdeOrder__c();
        tmpCDEO.Request__c = tmpReq.Id;
        insert tmpCDEO;
        
/************************************************************************************************	
										SalesOrder
************************************************************************************************/        
        system.debug('SalesOrder__c');
        // create SalesOrder__c
        SalesOrder__c tmpSO = new SalesOrder__c();
        tmpSO.Request__c  = tmpReq.Id;
        insert tmpSO;

/************************************************************************************************	
										SalesOrderItem
************************************************************************************************/    
        system.debug('SalesOrderItem__c');
        SalesOrderItem__c tmpSOI 	= new SalesOrderItem__c();
        tmpSOI.SalesOrder__c 		= tmpSO.Id;
        tmpSOI.Quantity__c			= 3;
        insert tmpSOI;
        
/************************************************************************************************	
										Visitation
************************************************************************************************/    
        system.debug('Visitation__c');
        // create Visitation__c
        Visitation__c tmpVisitation = new Visitation__c();
        tmpVisitation.Request__c = tmpReq.Id;
        
        insert tmpVisitation;
        
/************************************************************************************************	
										RedSurveyResult
************************************************************************************************/      
        system.debug('RedSurveyResult__c');
        // create RedSurveyResult__c
        RedSurveyResult__c tmpRSR = new RedSurveyResult__c();
        tmpRSR.Request__c = tmpReq.Id;
        insert tmpRSR;
        

/************************************************************************************************	
										Account + Updates
************************************************************************************************/  
        system.debug('Account');
        // create Account
        Account tmpAcc = new Account(); 
        string tmpReqIdStr = tmpReq.Id;
        tmpAcc.Name	= 'Test Account';
        tmpAcc.RequestID__c = tmpReqIdStr.left(15);
        tmpAcc.ID2__c = '12345';
        tmpAcc.BillingPostalCode = '33106 PB'; 
        tmpAcc.BillingCountry__c = 'DE';
        //tmpAcc.CurrencyIsoCode = 'EUR';
        tmpAcc.BillingStreet = 'Karl-Schurz Str.';
        tmpAcc.BillingHouseNo__c = '29';
        tmpAcc.BillingPostalCode = '33100 PB';
        tmpAcc.BillingCity = 'Padebrorn';
        tmpAcc.BillingCountry__c = 'PB';
        
        insert tmpAcc;
        
        // create second Request
        Request__c tmpReq2 = new Request__c();
        tmpReq2.RelatedAccount__c=tmpAcc.ID;
        insert tmpReq2;
        Request__c tmpReq3 = new Request__c();
        tmpReq3.RelatedAccount__c=tmpAcc.ID;
        insert tmpReq3;
  
        tmpAcc.RequestID__c='112345';
        update tmpAcc;
        tmpAcc.RequestID__c=tmpReq2.Id;
        update tmpAcc;
        
		     	
    }
	
	
	

}
