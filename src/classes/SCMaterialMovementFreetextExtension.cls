/*
 * @(#)SCMaterialMovementFreetextExtension.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Georg Birkenheuer <gbirkenheuer@gms-online.de>
 * @version $Revision$, $Date$
 */
 
public with sharing class SCMaterialMovementFreetextExtension {

  public SCMaterialMovement__c movement {get; private set;}
  public String message {get; private set;} 
  private boolean status = false;
  public String severity {get; private set;}  
  private List<String> mml;
  
  public SCMaterialMovementFreetextExtension(ApexPages.StandardController controller)
  {
    
    String mode = ApexPages.currentPage().getParameters().get('mode');
    
    SCMaterialMovement__c movementID = (SCMaterialMovement__c)controller.getRecord();
    this.movement = [ Select id, name, article__c, status__c, OrderLine__c  
      from SCMaterialMovement__c where id = :movementID.id ]; 
    system.debug('#### SCMaterialMovement__c: ' + movement);
    message = '';
    severity = 'warning';
    mml = new List<String>();
    
    if(movement.Status__c == '5402' )
    {
      List<SCArticle__c> articles = [select Id, ERPRelevant__c from SCArticle__c 
        where id = : movement.article__c];
      if(articles.size() > 0)
      {  
        if(articles[0].ERPRelevant__c == true)
        {
          
          mml.add(movement.id);
          message += 'OK – Die Reservierung kann durchgeführt werden.';
          severity = 'info';
          status = true; 
        }
        else
        {
          message +='Die Reservierung kann nicht erstellt werden. Der Artikel ist nicht ERPRelevant.';
        }
      }
      else
      {
        message +='Die Reservierung kann nicht durchgeführt werden. Der Artikel existiert nicht.';
      }
    }
    else
    {
      message += 'Die Reservierung kann nicht erstellt werden. Die Materialbewegung '+movement.name+' ist nicht im Zustand \"Anfordern\".';
    }
    
  }
  
  public Pagereference onCreate()
  {
    
    if(status)
    {
      CCWCMaterialReservationCreate.callout(mml,true,false);
      // If the callout is created, update the corresponding order line.
      // The article of the material movement has to be set also to the  
      // corresponding order line.
      if(movement.OrderLine__c != null)
      {
      	List<SCOrderLine__c> oll = [select id, article__c from SCOrderLine__c where id = : movement.OrderLine__c];
      	if(oll.size() > 0)
      	{
      		SCOrderLine__c ol = oll[0];
      		ol.Article__c = movement.Article__c;
      		update ol; 
      	}
      }
    }
    return new Pagereference('/' + movement.id);
  }
  
  public Pagereference onCancel()
  {
    //nothing to do
  	return new Pagereference('/' + movement.id);
  }
  
}
