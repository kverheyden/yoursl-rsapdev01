/*
 * @(#)AvsAddress.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
/**
 * Generated by wsdl2apex
 */
public class AvsService {
    public class updateResponseType {
        public String country;
        public Long docCount;
        private String[] country_type_info = new String[]{'country','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] docCount_type_info = new String[]{'docCount','http://www.w3.org/2001/XMLSchema','long','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.gms.com/avs/','false','false'};
        private String[] field_order_type_info = new String[]{'country','docCount'};
    }
    public class SearchAddressResponse_element {
        public AvsService.Address[] address;
        private String[] address_type_info = new String[]{'address','http://www.gms.com/avs/','Address','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.gms.com/avs/','false','false'};
        private String[] field_order_type_info = new String[]{'address'};
    }
    public class searchFieldsType {
        public String[] searchField;
        private String[] searchField_type_info = new String[]{'searchField','http://www.w3.org/2001/XMLSchema','string','1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.gms.com/avs/','false','false'};
        private String[] field_order_type_info = new String[]{'searchField'};
    }
    public class AddAddressRequest_element {
        public String tenant;
        public String country;
        public AvsService.Address address;
        private String[] tenant_type_info = new String[]{'tenant','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] country_type_info = new String[]{'country','http://www.gms.com/avs/','countrySimpleType','1','1','false'};
        private String[] address_type_info = new String[]{'address','http://www.gms.com/avs/','Address','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.gms.com/avs/','false','false'};
        private String[] field_order_type_info = new String[]{'tenant','country','address'};
    }
    public class Address {
        public String country;
        public String PC;
        public String city;
        public String district;
        public String street;
        public String noFrom;
        public String noTo;
        public Double geoX;
        public Double geoY;
        public Long nodeID;
        public String houseNumber;
        public Integer matchType;
        private String[] country_type_info = new String[]{'country','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] PC_type_info = new String[]{'PC','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] city_type_info = new String[]{'city','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] district_type_info = new String[]{'district','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] street_type_info = new String[]{'street','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] noFrom_type_info = new String[]{'noF','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] noTo_type_info = new String[]{'noT','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] geoX_type_info = new String[]{'geoX','http://www.w3.org/2001/XMLSchema','float','1','1','false'};
        private String[] geoY_type_info = new String[]{'geoY','http://www.w3.org/2001/XMLSchema','float','1','1','false'};
        private String[] nodeID_type_info = new String[]{'nodeID','http://www.w3.org/2001/XMLSchema','long','1','1','false'};
        private String[] houseNumber_type_info = new String[]{'houseNumber','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] matchType_type_info = new String[]{'matchType','http://www.w3.org/2001/XMLSchema','int','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.gms.com/avs/','false','false'};
        private String[] field_order_type_info = new String[]{'country','PC','city','district','street','noF','noT','geoX','geoY','nodeID','houseNumber','matchType'};
    }
    public class SearchAddressRequest_element {
        public String tenant;
        public String country;
        public String matchMode;
        public String query;
        public AvsService.searchFieldsType searchFields;
        private String[] tenant_type_info = new String[]{'tenant','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] country_type_info = new String[]{'country','http://www.gms.com/avs/','countrySimpleType','1','1','false'};
        private String[] matchMode_type_info = new String[]{'matchMode','http://www.gms.com/avs/','matchMode_element','1','1','false'};
        private String[] query_type_info = new String[]{'query','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] searchFields_type_info = new String[]{'searchFields','http://www.gms.com/avs/','searchFieldsType','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.gms.com/avs/','false','false'};
        private String[] field_order_type_info = new String[]{'tenant','country','matchMode','query','searchFields'};
    }
    public class UpdateIndexRequest_element {
        public String[] country;
        private String[] country_type_info = new String[]{'country','http://www.gms.com/avs/','countrySimpleType','1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.gms.com/avs/','false','false'};
        private String[] field_order_type_info = new String[]{'country'};
    }
    public class UpdateIndexResponse_element {
        public AvsService.updateResponseType[] updateType;
        private String[] update_type_info = new String[]{'update','http://www.gms.com/avs/','updateResponseType','1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.gms.com/avs/','false','false'};
        private String[] field_order_type_info = new String[]{'updateType'};
    }
    public class AddAddressResponse_element {
        public Boolean success;
        private String[] success_type_info = new String[]{'success','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.gms.com/avs/','false','false'};
        private String[] field_order_type_info = new String[]{'success'};
    }
    public class AddressValidationSOAP {
        public String endpoint_x = 'http://owa.gms-online.de/avsweb/services/AddressValidation/';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://www.gms.com/avs/', 'AvsService'};
        
        public Boolean AddAddress(String tenant,String country,AvsService.Address address) {
            AvsService.AddAddressRequest_element request_x = new AvsService.AddAddressRequest_element();
            AvsService.AddAddressResponse_element response_x;
            request_x.tenant = tenant;
            request_x.country = country;
            request_x.address = address;
            Map<String, AvsService.AddAddressResponse_element> response_map_x = new Map<String, AvsService.AddAddressResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://www.gms.com/avs/AddAddress',
              'http://www.gms.com/avs/',
              'AddAddressRequest',
              'http://www.gms.com/avs/',
              'AddAddressResponse',
              'AvsService.AddAddressResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.success;
        }


        public AvsService.Address[] SearchAddress(String tenant,String country,String matchMode,String query) {
            AvsService.SearchAddressRequest_element request_x = new AvsService.SearchAddressRequest_element();
            AvsService.SearchAddressResponse_element response_x;
            request_x.tenant = tenant;
            request_x.country = country.toLowerCase();
            request_x.matchMode = matchMode;
            if(matchMode != 'test')
            {
                request_x.matchMode = '0';
            }
            request_x.query = query;
            Map<String, AvsService.SearchAddressResponse_element> response_map_x = new Map<String, AvsService.SearchAddressResponse_element>();
            response_map_x.put('response_x', response_x);
            if(matchMode == 'test')
            {
                response_x = createTestResult();
            }
            else
            {
                WebServiceCallout.invoke(
                  this,
                  request_x,
                  response_map_x,
                  new String[]{endpoint_x,
                  'http://www.gms.com/avs/SearchAddress',
                  'http://www.gms.com/avs/',
                  'SearchAddressRequest',
                  'http://www.gms.com/avs/',
                  'SearchAddressResponse',
                  'AvsService.SearchAddressResponse_element'}
                );
                response_x = response_map_x.get('response_x');
            }
            return response_x.address;
        }

        public AvsService.Address[] SearchAddress(String tenant,String country,String matchMode,String query,AvsService.searchFieldsType searchFields) {
            AvsService.SearchAddressRequest_element request_x = new AvsService.SearchAddressRequest_element();
            AvsService.SearchAddressResponse_element response_x;
            request_x.tenant = tenant;
            request_x.country = country;
            request_x.matchMode = matchMode;
            request_x.query = query;
            request_x.searchFields = searchFields;
            Map<String, AvsService.SearchAddressResponse_element> response_map_x = new Map<String, AvsService.SearchAddressResponse_element>();
            response_map_x.put('response_x', response_x);
            if(matchMode == 'test')
            {
                response_x = createTestResult();
            }
            else
            {
                WebServiceCallout.invoke(
                  this,
                  request_x,
                  response_map_x,
                  new String[]{endpoint_x,
                  'http://www.gms.com/avs/SearchAddress',
                  'http://www.gms.com/avs/',
                  'SearchAddressRequest',
                  'http://www.gms.com/avs/',
                  'SearchAddressResponse',
                  'AvsService.SearchAddressResponse_element'}
                );
                response_x = response_map_x.get('response_x');
            }
            return response_x.address;
        }
        public AvsService.updateResponseType[] UpdateIndex(String[] country) {
            AvsService.UpdateIndexRequest_element request_x = new AvsService.UpdateIndexRequest_element();
            AvsService.UpdateIndexResponse_element response_x;
            request_x.country = country;
            Map<String, AvsService.UpdateIndexResponse_element> response_map_x = new Map<String, AvsService.UpdateIndexResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://www.gms.com/avs/UpdateIndex',
              'http://www.gms.com/avs/',
              'UpdateIndexRequest',
              'http://www.gms.com/avs/',
              'UpdateIndexResponse',
              'AvsService.UpdateIndexResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.updateType;
        }
        
        // just emulate the result
        private AvsService.SearchAddressResponse_element createTestResult()
        {
            AvsService.SearchAddressResponse_element response_x = new AvsService.SearchAddressResponse_element();
            
            List<AvsService.Address> res = new List<AvsService.Address>();
            AvsService.Address a = new AvsService.Address();
            a.country = 'de';
            a.PC = '12345';
            a.city = 'Musterstadt';
            a.street = 'Hauptstr';              
            res.add(a);
            
            response_x.address = new AvsService.Address[1];
            response_x.address = res;
            
            return response_x;
        }
        
    }
}
