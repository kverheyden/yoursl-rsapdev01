/*
 * @(#)CCWCOrderExternalOperationRem.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * The class is used to remove an order external assignment in SAP of an existing order
 * 
 * To start the callout:
 *   CCWCOrderExternalOperationRem.callout(String extassid, boolean async, boolean testMode)
 *
 * Methods to override: 
 *      fillAndSendERPData(...)
 *      fillAndSendERPData(...)
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
public without sharing class CCWCOrderExternalOperationRem extends SCInterfaceExportBase
{

    public CCWCOrderExternalOperationRem ()
    {
    }
    public CCWCOrderExternalOperationRem (String interfaceName, String interfaceHandler, String refType, String subclassName)
    {
        super(interfaceName, interfaceHandler, refType, subclassName);
    }
    public CCWCOrderExternalOperationRem (String interfaceName, String interfaceHandler, String refType)
    {
        //super(interfaceName, interfaceHandler, refType, subclassName);
    }

    /**
     * Prepare to make the webservice callout synchrounously or asynchronously (future)
     *
     * ### Set the interface name, its interface handler and a refType for head id in an interface object!
     *
     * @param oid   order id
     * @param async     true: use a future method to call 
     *                  false: do a synchrounous callout
     * @param testMode true: emulate call
     */
    public static String callout(String externalAssignmentId, boolean async, boolean testMode) 
    {
        String interfaceName = 'SAP_ORDER_EXTOP_REM';
        String interfaceHandler = 'CCWCOrderExternalOperationRem';
        String refType = 'SCOrder__c';
        CCWCOrderExternalOperationRem oc = new CCWCOrderExternalOperationRem(interfaceName, interfaceHandler, refType);
        return oc.calloutCore(externalAssignmentId, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCOrderExternalOperationRem');
    } // callout   

    /**
     * Reads an order to send
     *
     * @param externalAssignmentId
     * @param testMode if true no call is made, because in the test mode the salesforce framework prevent it.
     *
     */
    public override void readSalesforceData(String externalAssignmentId, Map<String, Object> retValue, Boolean testMode)
    {
        List<SCOrderExternalAssignment__c> oeal =  [Select ID, Name, ERPOperationID__c, Order__r.ID, 
        						Order__r.ERPOrderNo__c, Status__c,
        						Order__r.ERPStatusOrderClose__c, Order__r.ERPStatusOrderCreate__c 
                                from SCOrderExternalAssignment__c where ID = : externalAssignmentId];
                               
        if(oeal.size() > 0)
        {
            retValue.put(ROOT, oeal[0]);
            debug('order: ' + retValue);
            setReferenceID(oeal[0].Order__r.ID);
            setReferenceType('SCOrder__c');
        }
        else
        {
            String msg = 'The OrderExternalAssignment with id: ' + externalAssignmentId + ' could not be found!';
            debug(msg);
            throw new SCfwException(msg);
        }

    }

    /**
     * Fills the send structure and sends the call to SAP
     * 
     * @param dataMap with the order under the key ROOT
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure
     *           message = data sent to ERP
     *           message_v1 = endpoint 
     *           message_v2 = a return value of web service 
     *           message_v3 = a mandatory fields error
     *
     */
    public override Boolean fillAndSendERPData(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs)
    {
        Boolean retValue = true;
     
        SCOrderExternalAssignment__c externalAssignment = (SCOrderExternalAssignment__c)dataMap.get(ROOT);
        
             
        // Check if we can remove the external assignment for the order
        if(externalAssignment.Order__r.ERPStatusOrderClose__c == 'ok')
        {
            rs.message = 'The OrderExternalOperationRem was skipped as ERPStatusOrderClose__c is already [ok]';
            rs.message_v4 = 'E001';
            return retValue;
        }
        else if (externalAssignment.Order__r.ERPStatusOrderCreate__c != 'ok')
        {
        	rs.message = 'The OrderExternalOperationRem was skipped as ERPStatusOrderCreate__c was not [ok]';
            rs.message_v4 = 'E001';
            return retValue;
        }
        /*else if(
        	(externalAssignment.Status__c == 'assigned' 
        	|| externalAssignment.Status__c == 'deleted'
        	|| externalAssignment.Status__c == 'pendingWaitForDelete')
        	) //PMS 36793/GMS: CCWCOrderExternalOperationRem - no OperationNumber
        {
        	rs.message = 'The OrderExternalOperationRem was skipped as Status__c hast to be [assigned|pendingWaitForDelete|deleted]. Status is: ' + externalAssignment.Status__c ;
            rs.message_v4 = 'E001';
            return retValue;
        }*/
        
        
        List<SCOrderExternalAssignmentItem__c> itemList = (List<SCOrderExternalAssignmentItem__c>) dataMap.get(KEY_LIST);
        // instantiate the client of the web service
        piCceagDeSfdcCSOrderExternalOperationRem.HTTPS_Port ws = new piCceagDeSfdcCSOrderExternalOperationRem.HTTPS_Port();

        // sets the basic authorization for the web service
        ws.inputHttpHeaders_x = u.getBasicAuth();

        // sets the endpoint of the web service
        ws.endpoint_x  = u.getEndpoint('OrderExternalOperationRemove');
        debug('endpoint: ' + ws.endpoint_x);
        rs.message_v1 = ws.endpoint_x;
		ws.timeout_x = u.getTimeOut();
        
        try
        {
            // instantiate a message header
            piCceagDeSfdcCSOrderExternalOperationRem.BusinessDocumentMessageHeader messageHeader = new piCceagDeSfdcCSOrderExternalOperationRem.BusinessDocumentMessageHeader();
            setMessageHeader(messageHeader, externalAssignment, messageID, u, testMode);
            
            // instantiate the body of the call
            piCceagDeSfdcCSOrderExternalOperationRem.ExternalOperation_element externalOperation = new piCceagDeSfdcCSOrderExternalOperationRem.ExternalOperation_element();
            
            // set the data to the body
            // rs.message_v3 = 
            setExternalAssignment(externalOperation, externalAssignment, u, testMode);

            String jsonInputMessageHeader = JSON.serialize(messageHeader);
            String fromJSONMapMessageHeader = getDataFromJSON(jsonInputMessageHeader);
            debug('from json Message Header: ' + fromJSONMapMessageHeader);

            String jsonInputOrder = JSON.serialize(externalOperation);
            String fromJSONMapOrder = getDataFromJSON(jsonInputOrder);
            debug('from json Order: ' + fromJSONMapOrder);
            
            rs.message = '\n\nMessageHeader: ' + fromJSONMapMessageHeader + ',\n\nmessageData: ' + fromJSONMapOrder ;
            debug('rs.message: ' + rs.message);
            // check if there are missing mandatory fields
            if(rs.message_v3 == null || rs.message_v3 == '')
            {
                debug('message_v3 ok');
                // All mandatory fields are filled
                // callout
                if(!testMode)
                {
                    debug('real mode');
                    // It is not the test from the test class
                    // go
                    rs.setCounter(1);
                    ws.CustomerServiceOrderRemoveExternalOperation_Out(messageHeader, externalOperation);
                    rs.Message_v2 = 'void';
                }
            }
            debug('after sending');
        }
        catch(Exception e) 
        {
            throw e;
        }
        return retValue;
    }
    
    /**
     * Sets the ERPStatusOrderCreate in the root object to 'pending'
     * 
     * @param dataMap of obects to dispatch
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure with error messages
     * @param error true if some error has been occured
     */
    public override void setERPStatus(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs, Boolean error)
    {
        if(dataMap != null)
        {
            SCOrderExternalAssignment__c oea = (SCOrderExternalAssignment__c)dataMap.get(ROOT);
            if(oea != null)
            {
		        // abort the ERPStatus manipulation, when the order staus is already 'ok'
		        //GMSGB PMS 36271: Auslösen von Interface Schnittstellen verhindern, wenn OderCreate noch nicht zurück ist
		        if(!testMode && (oea.Order__r.ERPStatusOrderClose__c == 'ok' || oea.Order__r.ERPStatusOrderCreate__c != 'ok' ) )
		        {
		            return;
		        }
                if(!testMode && error)
                {
                    oea.ERPStatusRem__c = 'error';
                }
                else
                {   
                    oea.ERPStatusRem__c = 'pending';
                    oea.Status__c 		= 'pendingWaitForDelete';
                }   
                update oea;
                debug('order external assignment after update: ' + oea);
                updateOrder(oea.Order__r.Id, oea.ERPStatusRem__c);
            }
        }    
    }

    public void updateOrder(ID orderID, String status)
    {
        if(orderID != null)
        {
            List<SCOrder__c> orderList = [Select ID from SCOrder__c where id = :orderID for update];
            if(orderList != null && orderList.size() > 0)
            {
                for(SCOrder__c o: orderList)
                {
                    if(status != null)
                    {
                        o.ERPStatusExternalAssignmentRem__c = status;
                    }       
                }
            }
            update orderList;
        }       
    }
    
    /**
     * sets data into the message header
     * messageID, referenceID = order.Name, CreationDateTime, SenderBusinessSystemID, RecipientBusinessSystemID
     *
     * @param mh message header 
     * @param order 
     * @param messageID MS: milliseconds R: random, e.g.    MS:1351254139745-R:0.7486318721270884
     * @param u CCWSUtil instance
     * @param testMode used to prevent call out by calls from CCWCOrderCreateTest class
     *
     */
    public void  setMessageHeader(piCceagDeSfdcCSOrderExternalOperationRem.BusinessDocumentMessageHeader mh, 
                                         SCOrderExternalAssignment__c externalAssignment, String messageID, CCWSUtil u, Boolean testMode)
    {
        mh.MessageID = messageID;
//      mh.MessageUUID;
        mh.ReferenceID = externalAssignment.Name;
//      mh.ReferenceUUID;
        mh.CreationDateTime = u.getFormatedCreationDateTime();
        if(testMode)
        {
            mh.TestDataIndicator = 'Test';
        }    
        mh.SenderBusinessSystemID = u.getSenderBusinessSystemID();
        mh.RecipientBusinessSystemID = u.getRecipientBusinessSystemID ();
        debug('Message Header: ' + mh);
    }

    /**
     * sets the call out order structure with data form an order
     *
     * @param cso callout order sturcture
     * @param order order with input data
     * @param u instance of the util class CCWSUtil
     * @param testMode used to prevent call out by calls from CCWCOrderCreateTest class
     *
     */
    public String setExternalAssignment(piCceagDeSfdcCSOrderExternalOperationRem.ExternalOperation_element eoe, 
                                        SCOrderExternalAssignment__c a, 
                                        CCWSUtil u, Boolean testMode)
    {
        String retValue = '';
        String step = '';
        try
        {
            step = 'OrderNumber';
            eoe.OrderNumber     = a.Order__r.ERPOrderNo__c;
            if(a.ERPOperationID__c != null && a.ERPOperationID__c != '' && a.ERPOperationID__c != '0000')
            {
            	eoe.OperationNumber = a.ERPOperationID__c; 
            }
            else 
            {
            	throw new SCfwException('The callout was aborted by the Clockport interface. \n SAP requires an OperationNumber, therfore the ERPOperationID has to be set in the ExternalAssignment.');
            }
            eoe.ReferenceNumber = a.Name;           
        }
        catch(Exception e)
        {
            String prevMsg = e.getMessage();
            String newMsg = 'setExternalAssignment: ' + step + ' ' + prevMsg;
            String stack = SCfwException.getExceptionInfo(e);
            newMsg += stack;
            debug(newMsg);
            e.setMessage(newMsg);
            throw e;
        }

        return retValue;    
    }

    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }
}
