/**
 * @(#)SCInterfaceClearing
 * Copyright 2012 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

 /**
 * @author Eugen Tiessen <etiessen@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCInterfaceClearingAttachmentController 
{
    //Visualforce pages may not display more than 10 dependent picklists together with their controlling fields. 
    //This includes any picklists in components or inline pages. 
    //If SF decide to increase the limit, just modify the constant 'MAX_DEP_PICKLIST'
    public static final Integer MAX_DEP_PICKLIST = 10;
    
    public static final String ACTIVITY_UPDATE = 'U';
    public static final String ACTIVITY_INSERT= 'I';
    public static final String ACTIVITY_DELETE= 'D';
    public static final String ACTIVITY_READONLY= 'R';
    
    public static final String ATTACHMENT = 'attachment';
    
    public static final String BASE_STMT = 'SELECT {0} FROM {1} WHERE Id = {2} LIMIT 1';
    
    public String id { get; private set; }
    public Attachment att { get; private set; }
    public sObject attObject { get; set; }
    public sObject origObject { get; private set; }
    public String activity {get; private set;}
    
    //fieldName -> label
    public Map<String,String> labelMap {get; private set;}
    
    //Mark fields not equal
    //fieldName -> color
    public Map<String,String> colorMap {get; private set;}
    
    public String fieldToRestore {get; set; }
    
    public Boolean showRawData {get; private set;}
    public Boolean forceRawVisibility {get; private set;}
    public Boolean isAttachment 
    {
    	get
    	{
    		if(this.att.Name == ATTACHMENT)
        	{
            	isAttachment = true;
        	}
        	else
        	{
        		isAttachment = false;
        	}
        	return isAttachment;
    	} 
    	private set;
    }
    
    public String objectType 
    { 
        get;
        private set;
    }
    
    public String objectName 
    { 
        get;
        private set;
    }
    
    public String att2Json
    {
        get
        {
            if(att2Json == null)
            {
            	
            	if(this.att.Name != ATTACHMENT)
	        	{
                	att2Json = formatJson(att.Body.toString()); 
            	}
	        	else
	        	{
	        		att2Json = att.Body.toString();
	        	} 
            }
            return att2Json;
        }
        set;
    }
    
    public List<String> attObjectInputFields { get; private set;}
    public List<String> attObjectReadOnlyFields { get; private set;}
    public List<String> attObjectAdditionalFields { get; private set;}
    
    public SCInterfaceClearingAttachmentController()
    {
        this.id = ApexPages.currentPage().getParameters().get('id');
        String rawParam = ApexPages.currentPage().getParameters().get('forceRawVisibility');
        this.forceRawVisibility = (rawParam != null && rawParam == 'true' ) ? true : false;
        
        if(this.id == null)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Id not found'));
            return;
        }
        try
        {
            this.att =
            [
                SELECT 
                    Id,
                    ContentType,
                    Body,
                    BodyLength,
                    Description,
                    Name,
                    parentId
                FROM 
                    Attachment
                WHERE
                    Id = :this.id
                LIMIT 1 
                
            ];  
        }
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Attachment not found'));
            return;
        }
        
        if(!this.forceRawVisibility)
        {
            initAsSObject();
        }
         
    }
    
    private void initAsSObject()
    {
        
        try
        {
            this.showRawData = false;
            this.attObject = SCboInterfaceClearing.getObjectFromAttachment(this.att);
            this.activity = this.att.Description;
            
            //Do not validate attachment objects
            if(this.att.Name != ATTACHMENT)
	        {
		        initFields();
		        
		        //TODO: Testing
	            //ET 28.03.2013: Merge original object with attachment object
	            SCInterfaceClearingObjectValidation icov;
	            if(this.activity == 'U')
	            {
	                sObject toValidate = SCBase.mergeSObject(this.attObject, this.origObject);
	                icov = new SCInterfaceClearingObjectValidation(toValidate);
	            }
	            else
	            {
	                icov = new SCInterfaceClearingObjectValidation(this.attObject);
	            }
	            icov.validate();
            }
            
        }
        catch(Exception e)
        {
            this.showRawData = true;
            clearAll();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
            system.debug('#### conversion exception: ' + e);
            return;
        }   
        
        
    }
    
    private void clearAll()
    {
        this.attObjectInputFields = new List<String>();
        this.attObjectReadOnlyFields = new List<String>();
        this.attObjectAdditionalFields = new List<String>();
        this.labelMap = new Map<string,string>();
        this.colorMap = new Map<string,string>();
    }
    
    /**
     * Init Lists with with all fields from an dynamic sObject 
     */
    private void initFields()
    {
        
        this.attObjectInputFields = new List<String>();
        this.attObjectReadOnlyFields = new List<String>();
        this.attObjectAdditionalFields = new List<String>();
        this.labelMap = new Map<string,string>();
        this.colorMap = new Map<string,string>();
        Schema.DescribeSObjectResult d = attObject.getSObjectType().getDescribe();
        this.objectType = d.getLabel();
        this.objectName = d.getName();
        Integer countDepPicklist = 0;
        
        //read all keys from json
        Set<String> keySet = SCBase.getSObjectAsMap(attObject).keySet();
        system.debug('#### keyset:' + keySet);
        for(Schema.SObjectField field : d.fields.getMap().values())
        {
            
            Schema.DescribeFieldResult fieldResult = field.getDescribe();
            if(fieldResult.getName().equals('OwnerId'))
            {
                system.debug('#### Found OwnerId. Ignored...');
                continue;
            }
            system.debug('#### field:' + fieldResult.getName());
            labelMap.put(fieldResult.getName(),fieldResult.getLabel());
            Boolean isValidInputField = fieldResult.isAccessible() && fieldResult.isUpdateable();
            
            if(attObject.get(field) == null && !keySet.contains(fieldResult.getName()))
            {
                //Do not show dependent picklists if not available in source object. 
                //VisualForce Page has a Limit of 10 for dependent picklists
                //if(isValidInputField && !fieldResult.isDependentPicklist())
                if(isValidInputField)
                {
                    if(fieldResult.isDependentPicklist())
                    {
                        if(countDepPicklist < MAX_DEP_PICKLIST)
                        {
                            countDepPicklist++;
                        }
                        else
                        {
                            this.attObjectReadOnlyFields.add(fieldResult.getName());
                            continue;
                        }
                    }
                    
                    this.attObjectAdditionalFields.add(fieldResult.getName());
                }
                else
                {
                    this.attObjectReadOnlyFields.add(fieldResult.getName());
                }
                
            }
            else if(isValidInputField)  
            {  
                if(fieldResult.isDependentPicklist())
                {
                    if(countDepPicklist < MAX_DEP_PICKLIST)
                    {
                        countDepPicklist++;
                    }
                    else
                    {
                        this.attObjectReadOnlyFields.add(fieldResult.getName());
                        continue;
                    }
                }
                this.attObjectInputFields.add(fieldResult.getName());  
            }
            else
            {
                this.attObjectReadOnlyFields.add(fieldResult.getName()); 
            }  
        }
        
        List<String> cols = new List<String>();
        cols.addAll(this.attObjectReadOnlyFields);
        cols.addAll(this.attObjectInputFields);
        cols.addAll(this.attObjectAdditionalFields);
        
        String objectId = String.valueOf(attObject.get('Id'));
        system.debug('#### objectid: ' + objectid);
        String stmt = String.format(BASE_STMT,new String[]{String.join(cols,','),this.objectName, '\'' + objectId +'\''});
        system.debug('#### Generated statement: ' + stmt);
        
        //TODO Check if dataset not available and create a new one.
        try
        {
            if(ACTIVITY_UPDATE.equals(activity))
            {
                this.origObject = Database.query(stmt);
            }
            else
            {
                //if an id is set, remove it
                //attObject.put('Id', null);
                if(this.attObject.get('Id') != null)
                {
                    this.attObject = this.attObject.clone(false);
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, '"Id" should not be set on insert. Click [Save] to remove "Id".'));
                }
                return;
            }
        }
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
            return;
        }
        
        
        for(String fieldName : cols)
        {
            
            if(attObject.get(fieldName) != origObject.get(fieldName))
            {
                
                //set value to original date if not available
                if(attObject.get(fieldName) == null && !keySet.contains(fieldName))
                {
                    //attObject.put(fieldName,origObject.get(fieldName));
                    //TODO set color on vf page not here
                    colorMap.put(fieldName,'#999999');
                }
                else
                {
                    colorMap.put(fieldName,'#FFFF33');
                }
                system.debug('#### changed field: ' + fieldName + ' old value=' + origObject.get(fieldName) + ' new value=' + attObject.get(fieldName));
            }
            else
            {
                colorMap.put(fieldName,'#FFFFFF');
            }
        }
            
    }
    
    public void onRestoreField()
    {
        if(fieldToRestore != null)
        {
            attObject.put(fieldToRestore,origObject.get(fieldToRestore));
            
        }
        fieldToRestore = null;
    }
    
    public void onSave()
    {
        try
        {
            //TODO: Testing
            //ET 28.03.2013: Merge original object with attachment object
            //sObject toValidate = SCBase.mergeSObject(this.attObject, this.origObject);
            //SCInterfaceClearingObjectValidation icov = new SCInterfaceClearingObjectValidation(toValidate);
            //
            SCInterfaceClearingObjectValidation icov;
            if(this.activity == 'U')
            {
                sObject toValidate = SCBase.mergeSObject(this.attObject, this.origObject);
                icov = new SCInterfaceClearingObjectValidation(toValidate);
            }
            else
            {
                icov = new SCInterfaceClearingObjectValidation(this.attObject);
            }
            
            
            if(!icov.validate())
            {
                //return;
            }
            SCboInterfaceClearing.saveObjectToAttachment(this.att, this.attObject);
            Note note = new Note
            (   
                Title = System.Label.SC_msg_Changed,
                Body = this.att.Name,
                parentId = att.parentId             
            );
            insert note;
            this.initFields();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, this.objectType + ' saved'));
        }
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
        }
        
        
    }
    
    public void onSaveRaw()
    {
        try
        {
            att.Body = Blob.valueOf(att2Json);
            upsert att;
            Note note = new Note
            (   
                Title = System.Label.SC_msg_Changed,
                Body = this.att.Name,
                parentId = att.parentId             
            );
            insert note;
            initAsSObject();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, this.objectType + ' saved'));
        }
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
        }
        
        
    }
    
    public void onClose()
    {
        //do nothing at the momement
    }
    
    //TODO: move function to a global class
    public static String formatJson(String jsonStr)
    {
        Integer i = 0;
        Integer il = 0;
        String tab = '\t';
        String newJson = '';
        Integer indentLevel = 0;
        Boolean inString = false;
        String currentChar = null;

        for (i = 0, il = jsonStr.length(); i < il; i += 1) 
        {
            currentChar = jsonStr.substring(i, i + 1);

            if(currentChar == '{' || currentChar == '[') 
            {
           
                if (!inString)
                {
                    newJson += currentChar + '\n' + tab.repeat(indentLevel + 1);
                    indentLevel += 1;
                } 
                else 
                {
                    newJson += currentChar;
                }
            }
            else if(currentChar == '}' || currentChar == ']')
            {
                if (!inString) 
                {
                    indentLevel -= 1;
                    newJson += '\n' + tab.repeat(indentLevel) + currentChar;
                } 
                else 
                {
                    newJson += currentChar;
                }
            }
            else if (currentChar == ',')
            {
                if (!inString) 
                {
                    newJson += ',\n' + tab.repeat(indentLevel);
                } 
                else 
                {
                    newJson += currentChar;
                }
            }
            else if(currentChar == ':')
            {
                if (!inString) 
                {
                    newJson += ': ';
                } 
                else 
                {
                    newJson += currentChar;
                }
            }
            else if(currentChar == ' ' || currentChar == '\n' || currentChar == '\t')
            {
                if (inString) 
                {
                    newJson += currentChar;
                }
            }
            else if (currentChar == '"')
            {
                if (i > 0 && jsonStr.substring(i-1, i) != '\\') 
                {
                    inString = !inString;
                }
                newJson += currentChar;
            }
            else
            {
                newJson += currentChar;

            }
        }
        return newJson;
    }
        
}
