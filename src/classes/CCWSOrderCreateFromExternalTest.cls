/*
 * @(#)CCWSOrderCreateFromExternalTest.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 /**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Note !!!
// ========
//@isTest(SeeAllData=true) not allowed by tests creating an order. Because a trigger SCOrder_AI_CallSapWebService calls the Web Service
//====================================
// CCWCOrderCreate if IFEnableTriggerOrderCreate__c of CCSetting is greater than 0. By production and qa data it is the case.
// Calling of Web services while testing causes an exception that aborts the test. So we will not get the test running over the 75 procent of 
// the code
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@isTest
private class CCWSOrderCreateFromExternalTest 
{
  private static Boolean seeAllData = CCWCTestBase.isSeeingAllData();

    //product model, address ok
    static testMethod void positiveTest1()
  {
    Test.startTest();
    String sapOrderNo = 'SAP-ORDER-001';
      String partnerAGNumber = '1';
    String partnerWENumber = '2';
    String orderType = 'ZC02'; // 5701
    String productNumber = 'prodNumber';
    String serialNumber = '123456';
    String priceListName = 'DE301';
    String plantName = 'Plant';// WorkCenterPlanned    
    String mainWorkCenter = plantName;
    String workCenterPlanned = plantName;

    // create base data needed to create an order
    createBaseData(partnerAGNumber, partnerWENumber, plantName, productNumber, serialNumber, priceListName);

    // prepare the input structure for the web service
    CCWSOrderCreateFromExternal.CreateOrderFromExternalMessageClass input = createInputStructure(partnerWENumber, partnerAGNumber, 
                                  mainWorkCenter, workCenterPlanned, sapOrderNo, orderType, productNumber, serialNumber,
                                  priceListName, plantName);
    // call the handler of the web service
    CCWSOrderCreateFromExternal.GenericServiceResponseMessageClass output = CCWSOrderCreateFromExternal.transmit(input);
    List<SCOrder__c> orderList = [select id from SCOrder__c where ERPOrderNo__c = :sapOrderNo];
    System.assertEquals(true, orderList.size() > 0);
    Test.stopTest();
  }

  // find the installed base on the base of the serialnumber. So do not create the installed base role in the base data
    static testMethod void positiveTest2()
  {
    Test.startTest();
    String sapOrderNo = 'SAP-ORDER-001';
      String partnerAGNumber = '1';
    String partnerWENumber = '2';
    String orderType = 'ZC02'; // 5701
    String productNumber = 'prodNumber';
    String serialNumber = '123456';
    String priceListName = 'DE301';
    String plantName = 'Plant';// WorkCenterPlanned    
    String mainWorkCenter = plantName;
    String workCenterPlanned = plantName;

    // create base data needed to create an order
    createBaseDataWithoutInstBaseRole(partnerAGNumber, partnerWENumber, plantName, productNumber, serialNumber, priceListName);

    // prepare the input structure for the web service
    CCWSOrderCreateFromExternal.CreateOrderFromExternalMessageClass input = createInputStructure(partnerWENumber, partnerAGNumber, 
                                  mainWorkCenter, workCenterPlanned, sapOrderNo, orderType, productNumber, serialNumber,
                                  priceListName, plantName);
    // call the handler of the web service
    CCWSOrderCreateFromExternal.GenericServiceResponseMessageClass output = CCWSOrderCreateFromExternal.transmit(input);
    List<SCOrder__c> orderList = [select id from SCOrder__c where ERPOrderNo__c = :sapOrderNo];
    System.assertEquals(true, orderList.size() > 0);
    Test.stopTest();
  }


    static testMethod void negativeTestWithoutTags()
  {
    Test.startTest();
    String sapOrderNo = 'SAP-ORDER-001';
      String partnerAGNumber = '1';
    String partnerWENumber = '2';
    String orderType = 'ZC02'; // 5701
    String productNumber = 'prodNumber';
    String serialNumber = '123456';
    String priceListName = 'DE301';
    String plantName = 'Plant';// WorkCenterPlanned    
    String mainWorkCenter = plantName;
    String workCenterPlanned = plantName;

    // create base data needed to create an order
    createBaseDataWithoutInstBaseRole(partnerAGNumber, partnerWENumber, plantName, productNumber, serialNumber, priceListName);

    // prepare the input structure for the web service
    Boolean createAllSectionTags = false;
    Boolean createAllOptionalFields = true;
    CCWSOrderCreateFromExternal.CreateOrderFromExternalMessageClass input = createInputStructureCore(partnerWENumber, partnerAGNumber, 
                                  mainWorkCenter, workCenterPlanned, sapOrderNo, orderType, productNumber, serialNumber,
                                  priceListName, createAllSectionTags, createAllOptionalFields, plantName);
    // call the handler of the web service
    CCWSOrderCreateFromExternal.GenericServiceResponseMessageClass output = CCWSOrderCreateFromExternal.transmit(input);
    List<SCOrder__c> orderList = [select id from SCOrder__c where ERPOrderNo__c = :sapOrderNo];
    System.assertEquals(true, orderList.size() == 0);
    Test.stopTest();
  }
    static testMethod void positiveTestWithoutOptionalFields()
  {
    Test.startTest();
    String sapOrderNo = 'SAP-ORDER-001';
      String partnerAGNumber = '1';
    String partnerWENumber = '2';
    String orderType = 'ZC02'; // 5701
    String productNumber = 'prodNumber';
    String serialNumber = '123456';
    String priceListName = 'DE301';
    String plantName = 'Plant';// WorkCenterPlanned    
    String mainWorkCenter = plantName;
    String workCenterPlanned = plantName;

    // create base data needed to create an order
    createBaseDataWithoutInstBaseRole(partnerAGNumber, partnerWENumber, plantName, productNumber, serialNumber, priceListName);

    // prepare the input structure for the web service
    Boolean createAllSectionTags = true;
    Boolean createAllOptionalFields = false;
    CCWSOrderCreateFromExternal.CreateOrderFromExternalMessageClass input = createInputStructureCore(partnerWENumber, partnerAGNumber, 
                                  mainWorkCenter, workCenterPlanned, sapOrderNo, orderType, productNumber, serialNumber,
                                  priceListName, createAllSectionTags, createAllOptionalFields, plantName);
    // call the handler of the web service
    CCWSOrderCreateFromExternal.GenericServiceResponseMessageClass output = CCWSOrderCreateFromExternal.transmit(input);
    List<SCOrder__c> orderList = [select id from SCOrder__c where ERPOrderNo__c = :sapOrderNo];
    System.assertEquals(true, orderList.size() == 1);
    Test.stopTest();
  }
    static testMethod void negativeTestProductModelIsNotThere()
  {
    Test.startTest();
    String sapOrderNo = 'SAP-ORDER-001';
      String partnerAGNumber = '1';
    String partnerWENumber = '2';
    String orderType = 'ZC02'; // 5701
    String productNumber = 'prodNumber';
    String serialNumber = '123456';
    String priceListName = 'DE301';
    String plantName = 'Plant';// WorkCenterPlanned    
    String mainWorkCenter = plantName;
    String workCenterPlanned = plantName;

    // create base data needed to create an order
    createBaseData(partnerAGNumber, partnerWENumber, plantName, productNumber, serialNumber, priceListName);

    // prepare the input structure for the web service
    CCWSOrderCreateFromExternal.CreateOrderFromExternalMessageClass input = createInputStructure(partnerWENumber, partnerAGNumber, 
                                  mainWorkCenter, workCenterPlanned, sapOrderNo, orderType, productNumber + 'diff', serialNumber,
                                  priceListName, plantName);
    // call the handler of the web service
    CCWSOrderCreateFromExternal.GenericServiceResponseMessageClass output = CCWSOrderCreateFromExternal.transmit(input);
    List<SCOrder__c> orderList = [select id from SCOrder__c where ERPOrderNo__c = :sapOrderNo];
    System.assertEquals(true, orderList.size() == 0);
    Test.stopTest();
  }
    static testMethod void negativeTestPartnerWEIsNotThere()
  {
    Test.startTest();
    String sapOrderNo = 'SAP-ORDER-001';
      String partnerAGNumber = '1';
    String partnerWENumber = '2';
    String orderType = 'ZC02'; // 5701
    String productNumber = 'prodNumber';
    String serialNumber = '123456';
    String priceListName = 'DE301';
    String plantName = 'Plant';// WorkCenterPlanned    
    String mainWorkCenter = plantName;
    String workCenterPlanned = plantName;

    // create base data needed to create an order
    createBaseData(partnerAGNumber, partnerWENumber, plantName, productNumber, serialNumber, priceListName);

    // prepare the input structure for the web service
    CCWSOrderCreateFromExternal.CreateOrderFromExternalMessageClass input = createInputStructure(partnerWENumber + 'dif', partnerAGNumber, 
                                  mainWorkCenter, workCenterPlanned, sapOrderNo, orderType, productNumber, serialNumber,
                                  priceListName, plantName);
    // call the handler of the web service
    CCWSOrderCreateFromExternal.GenericServiceResponseMessageClass output = CCWSOrderCreateFromExternal.transmit(input);
    List<SCOrder__c> orderList = [select id from SCOrder__c where ERPOrderNo__c = :sapOrderNo];
    System.assertEquals(true, orderList.size() == 0);
    Test.stopTest();
  }
    static testMethod void negativeTestPartnerAGIsNotThere()
  {
    Test.startTest();
    String sapOrderNo = 'SAP-ORDER-001';
      String partnerAGNumber = '1';
    String partnerWENumber = '2';
    String orderType = 'ZC02'; // 5701
    String productNumber = 'prodNumber';
    String serialNumber = '123456';
    String priceListName = 'DE301';
    String plantName = 'Plant';// WorkCenterPlanned    
    String mainWorkCenter = plantName;
    String workCenterPlanned = plantName;

    // create base data needed to create an order
    createBaseData(partnerAGNumber, partnerWENumber, plantName, productNumber, serialNumber, priceListName);

    // prepare the input structure for the web service
    CCWSOrderCreateFromExternal.CreateOrderFromExternalMessageClass input = createInputStructure(partnerWENumber, partnerAGNumber + 'dif', 
                                  mainWorkCenter, workCenterPlanned, sapOrderNo, orderType, productNumber, serialNumber,
                                  priceListName, plantName);
    // call the handler of the web service
    CCWSOrderCreateFromExternal.GenericServiceResponseMessageClass output = CCWSOrderCreateFromExternal.transmit(input);
    List<SCOrder__c> orderList = [select id from SCOrder__c where ERPOrderNo__c = :sapOrderNo];
    System.assertEquals(true, orderList.size() == 0);
    Test.stopTest();
  }

    static testMethod void positiveTestMainWorkCenterIsNotThere()
  {
    Test.startTest();
    String sapOrderNo = 'SAP-ORDER-001';
      String partnerAGNumber = '1';
    String partnerWENumber = '2';
    String orderType = 'ZC02'; // 5701
    String productNumber = 'prodNumber';
    String serialNumber = '123456';
    String priceListName = 'DE301';
    String plantName = 'Plant';// WorkCenterPlanned    
    String mainWorkCenter = plantName;
    String workCenterPlanned = plantName;

    // create base data needed to create an order
    createBaseData(partnerAGNumber, partnerWENumber, plantName, productNumber, serialNumber, priceListName);

    // prepare the input structure for the web service
    CCWSOrderCreateFromExternal.CreateOrderFromExternalMessageClass input = createInputStructure(partnerWENumber, partnerAGNumber, 
                                  mainWorkCenter + 'dif', workCenterPlanned, sapOrderNo, orderType, productNumber, serialNumber,
                                  priceListName, plantName);
    // call the handler of the web service
    CCWSOrderCreateFromExternal.GenericServiceResponseMessageClass output = CCWSOrderCreateFromExternal.transmit(input);
    List<SCOrder__c> orderList = [select id from SCOrder__c where ERPOrderNo__c = :sapOrderNo];
    System.assertEquals(true, orderList.size() == 1);
    Test.stopTest();
  }
    static testMethod void positiveTestWorkCenterPlannedIsNotThere()
  {
    Test.startTest();
    String sapOrderNo = 'SAP-ORDER-001';
      String partnerAGNumber = '1';
    String partnerWENumber = '2';
    String orderType = 'ZC02'; // 5701
    String productNumber = 'prodNumber';
    String serialNumber = '123456';
    String priceListName = 'DE301';
    String plantName = 'Plant';// WorkCenterPlanned    
    String mainWorkCenter = plantName;
    String workCenterPlanned = plantName;

    // create base data needed to create an order
    createBaseData(partnerAGNumber, partnerWENumber, plantName, productNumber, serialNumber, priceListName);

    // prepare the input structure for the web service
    CCWSOrderCreateFromExternal.CreateOrderFromExternalMessageClass input = createInputStructure(partnerWENumber, partnerAGNumber, 
                                  mainWorkCenter, workCenterPlanned + 'dif', sapOrderNo, orderType, productNumber, serialNumber,
                                  priceListName, plantName);
    // call the handler of the web service
    CCWSOrderCreateFromExternal.GenericServiceResponseMessageClass output = CCWSOrderCreateFromExternal.transmit(input);
    List<SCOrder__c> orderList = [select id from SCOrder__c where ERPOrderNo__c = :sapOrderNo];
    System.assertEquals(true, orderList.size() == 1);
    Test.stopTest();
  }

    static testMethod void positiveTestSerialNumberIsNotThere()
  {
    Test.startTest();
    String sapOrderNo = 'SAP-ORDER-001';
      String partnerAGNumber = '1';
    String partnerWENumber = '2';
    String orderType = 'ZC02'; // 5701
    String productNumber = 'prodNumber';
    String serialNumber = '123456';
    String priceListName = 'DE301';
    String plantName = 'Plant';// WorkCenterPlanned    
    String mainWorkCenter = plantName;
    String workCenterPlanned = plantName;

    // create base data needed to create an order
    createBaseData(partnerAGNumber, partnerWENumber, plantName, productNumber, serialNumber, priceListName);

    // prepare the input structure for the web service
    CCWSOrderCreateFromExternal.CreateOrderFromExternalMessageClass input = createInputStructure(partnerWENumber, partnerAGNumber, 
                                  mainWorkCenter, workCenterPlanned, sapOrderNo, orderType, productNumber, serialNumber + 'dif',
                                  priceListName, plantName);
    // call the handler of the web service
    CCWSOrderCreateFromExternal.GenericServiceResponseMessageClass output = CCWSOrderCreateFromExternal.transmit(input);
    List<SCOrder__c> orderList = [select id from SCOrder__c where ERPOrderNo__c = :sapOrderNo];
    System.assertEquals(true, orderList.size() > 0);
    
    List<SCOrderItem__c> orderItemList = [select id, IBSerialNo__c from SCOrderItem__c where Order__c = :orderList[0].id];
    System.assertEquals(serialNumber + 'dif', orderItemList[0].IBSerialNo__c);
    
    
    Test.stopTest();
  }

  private static CCWSOrderCreateFromExternal.CreateOrderFromExternalMessageClass createInputStructure(String partnerWENumber, String partnerAGNumber, 
                                  String mainWorkCenter, String workCenterPlanned, String sapOrderNo, String orderType, String productNumber, String serialNumber,
                                  String priceList, String plant)
  {
    Boolean createAllSectionTags = true; 
    Boolean createAllOptionalFields = true;
    return createInputStructureCore(partnerWENumber, partnerAGNumber, 
                    mainWorkCenter, workCenterPlanned, sapOrderNo, orderType, productNumber, serialNumber,
                    priceList, createAllSectionTags, createAllOptionalFields, plant);
                    
  }

  private static CCWSOrderCreateFromExternal.CreateOrderFromExternalMessageClass createInputStructureCore(String partnerWENumber, String partnerAGNumber, 
                                  String mainWorkCenter, String workCenterPlanned, String sapOrderNo, String orderType, String productNumber, String serialNumber,
                                  String priceList, Boolean createAllSectionTags, Boolean createAllOptionalFields, String plant)
  {
    CCWSOrderCreateFromExternal.CreateOrderFromExternalMessageClass input = createRawInputStructureCore(createAllSectionTags);
    
    if(createAllSectionTags)
    {
      input.PlainFields.OrderID = sapOrderNo;
      if(createAllOptionalFields)
      {
        input.PlainFields.BasicStartDate = Date.today(); //  CustomerPrefStart__c
        input.SalesData.SalesArea = 'SalesArea'; // SalesArea__c
        input.SalesData.DistributionChannel = 'Distr_Channel'; //  DistributionChannel__c  
        input.SalesData.Division = 'Division'; //  Division__c
        input.SalesData.SalesOffice = 'SalesOffice'; //  SalesOffice__c
      }
  
      input.PlainFields.WorkflowID = 'OrderI.ID3__c'; // order.ID3__c
      input.PlainFields.MainWorkcenter = mainWorkCenter; //  DepartmentResponsible__c , PlantName
      input.PlainFields.WorkCenterPlanned = workCenterPlanned; //  DepartmentCurrent__c
      input.PlainFields.ShortText = 'Description'; //  Description__c
      input.PlainFields.OrderType = orderType; // 'ZC02'; // Type__c = 5701
      input.PlainFields.Assembly = productNumber; // ProductNumber 
          input.PlainFields.SerialNumber = serialNumber; // SerialNumber
      input.PlainFields.PriceList = priceList; // 'DE301'; // order.priceList
      input.PlainFields.Plant = plant;
      
      // add partner
      if(partnerWENumber != null && partnerWENumber != '')
      {
        CCWSOrderCreateFromExternal.PartnerClass partnerWE = new CCWSOrderCreateFromExternal.PartnerClass();
        partnerWE.PartnerFunction = 'WE';
        partnerWE.PartnerNumber = partnerWENumber;
        input.Partners.Partner.add(partnerWE);
      }   
  
      if(partnerAGNumber != null && partnerAGNumber != '')
      {
        CCWSOrderCreateFromExternal.PartnerClass partnerAG = new CCWSOrderCreateFromExternal.PartnerClass();
        partnerAG.PartnerFunction = 'AG';
        partnerAG.PartnerNumber = partnerAGNumber;
        input.Partners.Partner.add(partnerAG);
      }
    }     
    return input;    
  }



  private static CCWSOrderCreateFromExternal.CreateOrderFromExternalMessageClass createRawInputStructure()
  {
    Boolean createAllSectionTags = true;
    return createRawInputStructureCore(createAllSectionTags);
  }
  private static CCWSOrderCreateFromExternal.CreateOrderFromExternalMessageClass createRawInputStructureCore(Boolean createAllSectionTags)
  {
    CCWSOrderCreateFromExternal.CreateOrderFromExternalMessageClass retValue = new CCWSOrderCreateFromExternal.CreateOrderFromExternalMessageClass();
    if(createAllSectionTags)
    {
          retValue.MessageHeader = new CCWSOrderCreateFromExternal.MessageHeaderClass();
          retValue.PlainFields = new CCWSOrderCreateFromExternal.CreateOrderExternalFieldsClass();
          retValue.SalesData = new CCWSOrderCreateFromExternal.SalesDataClass();
          retValue.Partners = new CCWSOrderCreateFromExternal.PartnerContainer();
          retValue.Partners.Partner = new List<CCWSOrderCreateFromExternal.PartnerClass>();
          retValue.ContactInfo =  new CCWSOrderCreateFromExternal.ContactInfoClass();
    }    
    return retValue;
  }


  private static void createBaseDataWithoutInstBaseRole( String partnerAGNumber,
                                String partnerWENumber,
                                String plantName, 
                              String productNumber, 
                              String serialNumber,
                              String priceListName)
  {                  
    Boolean createInstBaseRole = false;
    createBaseDataCore(partnerAGNumber,
              partnerWENumber,
              plantName, 
              productNumber, 
              serialNumber,
              priceListName,
              createInstBaseRole);
  }

  private static void createBaseData( String partnerAGNumber,
                      String partnerWENumber,
                      String plantName, 
                    String productNumber, 
                    String serialNumber,
                    String priceListName)
  {
    Boolean createInstBaseRole = true;    
    createBaseDataCore(partnerAGNumber,
              partnerWENumber,
              plantName, 
              productNumber, 
              serialNumber,
              priceListName,
              createInstBaseRole);
  }                    
  private static void createBaseDataCore( String partnerAGNumber,
                      String partnerWENumber,
                      String plantName, 
                    String productNumber, 
                    String serialNumber,
                    String priceListName,
                    Boolean createInstBaseRole)
  {                  
    Boolean doUpsert = true;
          
    CCWSUtil u = new CCWSUtil();

    SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
          appSettings = SCApplicationSettings__c.getInstance();
        }
    CCWCTestBase.setAppSettingsForOrder(appSettings, seeAllData);
    
    CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
          ccSettings = CCSettings__c.getInstance();
        }
    CCWCTestBase.setCCSettings(ccSettings, seeAllData);

        String country = 'DE';
        // initiate and create a brand
        Brand__c brand = new Brand__c(Name = 'GMS Test 1', Competitor__c = false);
        brand = CCWCTestBase.createBrandObject(brand, doUpsert);
        
        // initiate and create a price list
        SCPriceList__c priceList = new SCPriceList__c(Name = priceListName, ID2__c = priceListName, Country__c = country);
        priceList = CCWCTestBase.createPricelist(appSettings, priceList, brand, doUpsert);
    
    // initiate and create an account
    String currencyIsoCode = 'EUR';

        Account accountAG = new Account(AccountNumber = partnerAGNumber,
                      LastName__c = 'Pietrzyk', FirstName__c = 'Jerzy', name = 'Jerzy Pietrzyk',
                      CurrencyIsoCode = currencyIsoCode, Type = SCfwConstants.DOMVAL_ACCOUNT_TYPE_CUSTOMER,
                      TaxType__c = SCfwConstants.DOMVAL_TAXACCOUNT_TAX,
                      TargetGroup__c = null, SubTargetGroup__c = null,
                      BillingPostalCode = '33104',
                      BillingCountry__c = country,
                      BillingStreet = 'Karl-Schurz-Str.',
                      BillingHouseNo__c = '29',
                      BillingCity = 'Paderborn',
                                 GeoX__c = -0.10345,
                                 GeoY__c = 51.49250,
                      GeoApprox__c = false,
                      ShippingPostalCode = '33104',
                      ShippingCountry__c = country,
                      ShippingStreet__c = 'Karl-Schurz-Str.',
                      ShippingHouseNo__c = '29',
                      ShippingPostalCode__c = '33104',
                      ShippingCity__c = 'Paderborn');
      
    String recordTypeName = 'Customer';
    accountAG = CCWCTestBase.createAccountObject(appSettings, recordTypeName, accountAG, priceList, doUpsert);

    // initiate and create an account role
       List<SCAccountRole__c> accountRoleListAG = new List<SCAccountRole__c>();

        accountRoleListAG.add(
            new SCAccountRole__c(AccountRole__c = SCfwConstants.DOMVAL_ACCOUNT_ROLE_INVOICE_RECIPIENT));

        accountRoleListAG.add(
            new SCAccountRole__c(AccountRole__c = SCfwConstants.DOMVAL_ACCOUNT_ROLE_PAYER));
    Account master = accountAG;
    Account slave = accountAG;    
        accountRoleListAG = CCWCTestBase.createAccountRoles(master, slave, accountRoleListAG, doUpsert);
    debug('accountAG: ' + accountAG);

        Account accountWE = new Account(AccountNumber = partnerWENumber,
                      LastName__c = 'Birkenheuer', FirstName__c = 'Georg', name = 'Georg Birkenheuer',
                      CurrencyIsoCode = currencyIsoCode, Type = SCfwConstants.DOMVAL_ACCOUNT_TYPE_CUSTOMER,
                      TaxType__c = SCfwConstants.DOMVAL_TAXACCOUNT_TAX,
                      TargetGroup__c = null, SubTargetGroup__c = null,
                      BillingPostalCode = '33104',
                      BillingCountry__c = country,
                      BillingStreet = 'Karl-Schurz-Str.',
                      BillingHouseNo__c = '29',
                      BillingCity = 'Paderborn',
                                 GeoX__c = -0.10345,
                                 GeoY__c = 51.49250,
                      GeoApprox__c = false,
                      ShippingPostalCode = '33104',
                      ShippingCountry__c = country,
                      ShippingStreet__c = 'Karl-Schurz-Str.',
                      ShippingHouseNo__c = '29',
                      ShippingPostalCode__c = '33104',
                      ShippingCity__c = 'Paderborn');
      
    accountWE = CCWCTestBase.createAccountObject(appSettings, recordTypeName, accountWE, priceList, doUpsert);

    // initiate and create an account role
       List<SCAccountRole__c> accountRoleListWE = new List<SCAccountRole__c>();

        accountRoleListWE.add(
            new SCAccountRole__c(AccountRole__c = SCfwConstants.DOMVAL_ACCOUNT_ROLE_INVOICE_RECIPIENT));

        accountRoleListWE.add(
            new SCAccountRole__c(AccountRole__c = SCfwConstants.DOMVAL_ACCOUNT_ROLE_PAYER));
    Account master2 = accountWE;
    Account slave2 = accountWE;    
        accountRoleListWE = CCWCTestBase.createAccountRoles(master2, slave2, accountRoleListWE, doUpsert);
    debug('accountWE: ' + accountWE);

    // initiate and create a condition tax list
    List<SCConditionTax__c> conditionTaxList = new List<SCConditionTax__c>();
        conditionTaxList.add(new SCConditionTax__c(
                        Country__c = country,
                        TaxRate__c = 7.0,
                        TaxAccount__c = SCfwConstants.DOMVAL_TAXACCOUNT_TAX,
                        TaxArticle__c = SCfwConstants.DOMVAL_TAXARTICLE_FULL,
                        ValidFrom__c = Date.today()
                    ));

    conditionTaxList.add(new SCConditionTax__c(
                        Country__c = country,
                        TaxRate__c = 3.0,
                        TaxAccount__c = SCfwConstants.DOMVAL_TAXACCOUNT_TAX,
                        TaxArticle__c = SCfwConstants.DOMVAL_TAXARTICLE_HALF,
                        ValidFrom__c = Date.today()
                    ));
  
        conditionTaxList = CCWCTestBase.createTaxRecord(conditionTaxList, doUpsert);
        
        // initiate and create a plant
        SCPlant__c plant = new SCPlant__c(Name = plantName, ID2__c = plantName, Info__c = 'Test plant');
    plant = CCWCTestBase.createTestPlant(doUpsert);
    
    // initiate and create stocks
    List<SCStock__c> stockList = CCWCTestBase.createTestStocks(plant.Id, doUpsert);
    
    // initiate and create a calendar
        SCCalendar__c calendar = new SCCalendar__c(Name = 'GMS Test Cal', Country__c = country, CurrencyIsoCode = currencyIsoCode);
    calendar = CCWCTestBase.createTestCalendar(calendar, doUpsert);
  
    // initiate and create a business unit
        SCBusinessUnit__c businessUnit = new SCBusinessUnit__c(Name = 'GMS Test Dev',
                        ID2__c = plantName,
                                    Info__c = 'GMS test Dev',
                                    Stock__c = stockList[0].id,
                                    Calendar__c = calendar.id,
                                    Type__c = '5901'
                                   );

        businessUnit = CCWCTestBase.createTestBusinessUnit(businessUnit, doUpsert);

    // create installed base
    List<SCArticle__c> articleList = CCWCTestBase.createTestArticles(appSettings, doUpsert);
    String serialNo = '123456';
    List<String> installedBaseRoleList = new List<String>();
    if(createInstBaseRole)
    {
      installedBaseRoleList.add('User');
    }
    // initiate and create a product model
        SCProductModel__c prodModel = new SCProductModel__c(Brand__c = brand.Id, 
                                          Country__c = country, 
                                          UnitClass__c = SCfwConstants.DOMVAL_PRODUCTUNITCLASS_STANDARD,
                                          UnitType__c = SCfwConstants.DOMVAL_PRODUCTUNITTYPE_DEFAULT, 
                                          Group__c = '40001304', 
                                          Name = productNumber,
                                          ID2__c = productNumber,  
                                          Power__c = 'Standard');
        prodModel = CCWCTestBase.createProductModel(prodModel, doUpsert);
    
    // initiate and create and installed base
        SCInstalledBase__c installedBase = new SCInstalledBase__c(
                                            SerialNo__c = serialNumber,
                                            Status__c = 'active',
                                            Type__c = 'Appliance',
                                            ProductSkill__c = 'PS1');

    installedBase = CCWCTestBase.createInstalledBase(installedBase, accountWE, installedBaseRoleList, 
                              articleList[0], prodModel, doUpsert);
    

  }
    /**
    * Used for Debug messages.
    */
    public static void debug(String msg)
    {
        System.debug('###...' +  msg);        
    }

}
