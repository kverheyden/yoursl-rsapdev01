/*
 * @(#)SCOrderMaterialControllerTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @version $Revision$, $Date$
 */
@isTest
private class SCOrderMaterialControllerTest
{
    public static SCfwDomain domMatMove = new SCfwDomain('DOM_MATERIALMOVEMENT_TYPE');
    public static SCboMaterialMovement boMatMove = new SCboMaterialMovement(domMatMove);
    
    static testMethod void testMatRequestAndMatStatus() 
    {
        

        // first create all necessary test data
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);
        SCHelperTestClass.createTestCalendar(true);
        SCHelperTestClass.createTestBusinessUnit(SCHelperTestClass.stocks.get(2).Id, 
                                               SCHelperTestClass.Calendar.Id, true);
        List<SCStock__c> assignStocks = new List<SCStock__c>();
        assignStocks.add(SCHelperTestClass.stocks.get(0));
        assignStocks.add(SCHelperTestClass.stocks.get(1));
        SCHelperTestClass.createOrderTestSet3(true);
        SCHelperTestClass.createTestAssignments(SCHelperTestClass.resources, assignStocks, 
                                              SCHelperTestClass.businessUnit.Id, true);
        SCboOrder boOrder = new SCboOrder(SCHelperTestClass.Order);
        SCboOrderItem boOrderItem = new SCboOrderItem(SCHelperTestClass.orderItem);
        
        // Id orderId, Id orderItemId, Id articleId, Id assignmentId
        SCboOrderLine boOrderLine1 = new SCboOrderLine(SCHelperTestClass.order.Id,
                                                       SCHelperTestClass.orderItem.Id,
                                                       SCHelperTestClass.articles[0].Id,
                                                       SCHelperTestClass.assignmentMap.get('assignmentDispo1').Id);
        SCboArticle boArticle1 = new SCboArticle(SCHelperTestClass.articles[0].Id);
        boOrderLine1.orderLine.Article__r = boArticle1.article;
        
        // set the required fields manually
        boOrderLine1.orderLine.Type__c = SCfwConstants.DOMVAL_ORDERLINETYPE_INV;
        boOrderLine1.orderLine.MaterialStatus__c = SCfwConstants.DOMVAL_MATSTAT_ORDERABLE;
        boOrderLine1.orderLine.Qty__c = 1.0;
        boOrderLine1.orderLine.ListPrice__c = 10.00;
        boOrderLine1.orderLine.Order__c = SCHelperTestClass.order.Id;
        boOrderItem.boOrderLines.add(boOrderLine1);

        SCboOrderLine boOrderLine2 = new SCboOrderLine(SCHelperTestClass.order.Id,
                                                       SCHelperTestClass.orderItem.Id,
                                                       SCHelperTestClass.articles[1].Id,
                                                       SCHelperTestClass.assignmentMap.get('assignmentDispo1').Id);
        SCboArticle boArticle2 = new SCboArticle(SCHelperTestClass.articles[1].Id);
        boOrderLine2.orderLine.Article__r = boArticle2.article;
        
        // set the required field manually
        boOrderLine2.orderLine.Type__c = SCfwConstants.DOMVAL_ORDERLINETYPE_INV;
        boOrderLine2.orderLine.MaterialStatus__c = SCfwConstants.DOMVAL_MATSTAT_ORDERABLE;
        boOrderLine2.orderLine.Qty__c = 1.0;
        boOrderLine2.orderLine.ListPrice__c = 20.00;
        boOrderLine2.orderLine.Order__c = SCHelperTestClass.order.Id;
        boOrderItem.boOrderLines.add(boOrderLine2);
        boOrderItem.save(); 
        
        Test.startTest();
        // create a material movement for an article
        boMatMove.createMatMove(SCHelperTestClass.stocks.get(0).Id, boArticle2.article.Id, 
                                1.0, SCfwConstants.DOMVAL_MATSTAT_ORDER, 
                                SCfwConstants.DOMVAL_MATMOVETYP_MATREQUEST_EMPL, 
                                null, null, null, null);
        Test.stopTest();
        
        ApexPages.currentPage().getParameters().put('oid', boOrder.order.Id);
        ApexPages.currentPage().getParameters().put('sid', SCHelperTestClass.stocks.get(0).Id);
        SCOrderMaterialController controller = new SCOrderMaterialController();
        
        // check initializes variables
        System.assert(controller.sourceList.size() > 0);
        System.assert(controller.receiverList.size() > 0);
        System.assert(controller.artInfoList.size() > 0);
        System.assert(controller.assignmentOk);

        // check the type list, should be filled
        List<SelectOption> typeList = controller.getTypeList();
        System.assert(controller.getHasArticles());
        
        // deselect all articles
        controller.selectAll();
        // select all articles
        controller.selectAll();
        
        // now test the material request
        controller.sourceStock = controller.sourceList.get(0).getValue();
        controller.receiverStock = controller.receiverList.get(0).getValue();
        controller.matMoveType = typeList.get(0).getValue();
        
        controller.checkAndRequestMat();
        System.assert(!controller.requestOk);
        System.assert(controller.errMsg.length() > 0);
        
        controller.requestMat();
        System.assert(controller.requestOk);
        System.assert(controller.errMsg.length() == 0);
        
        // finally test the material status with a new controller
        ApexPages.currentPage().getParameters().put('m', '2');
        ApexPages.currentPage().getParameters().put('oid', boOrder.order.Id);
        ApexPages.currentPage().getParameters().put('sid', SCHelperTestClass.stocks.get(0).Id);
        controller = new SCOrderMaterialController();
        System.assert(controller.matMoves.size() > 0);
    } // testMatRequestAndMatStatus

    static testMethod void testMatBooking() 
    {
        
        System.debug('###set necessary customizing settings');
        // set necessary customizing settings
        SCfwDomain domSysParam = new SCfwDomain('DOM_SYSPARAM');
        SCfwDomainValue domVal = domSysParam.getDomainValue('1400159', true);
        domVal.setControlParameter('V2', '1');
        domVal = domSysParam.getDomainValue('1400097', true);
        domVal.setControlParameter('V4', '1');
                                    
        System.debug('###first create all necessary test data');
        // first create all necessary test data
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);
        SCHelperTestClass.createTestCalendar(true);
        //10 SOQL 14 DML
        SCHelperTestClass.createTestBusinessUnit(SCHelperTestClass.stocks.get(2).Id, 
                                               SCHelperTestClass.Calendar.Id, true);
        List<SCStock__c> assignStocks = new List<SCStock__c>();
        assignStocks.add(SCHelperTestClass.stocks.get(0));
        assignStocks.add(SCHelperTestClass.stocks.get(1));
    
        SCHelperTestClass.createOrderTestSet3(true);
        SCHelperTestClass.createTestAssignments(SCHelperTestClass.resources, assignStocks, 
                                              SCHelperTestClass.businessUnit.Id, true);

        
        SCHelperTestClass.createTestStockItem(SCHelperTestClass.stocks.get(0).Id, 
                                            SCHelperTestClass.articles[0].Id, false);
        
        System.debug('###SCboOrderLine1');
        SCHelperTestClass.stockItem.Qty__c = 1.0;
        insert SCHelperTestClass.stockItem;
        SCboOrder boOrder = new SCboOrder(SCHelperTestClass.Order);
        SCboOrderItem boOrderItem = new SCboOrderItem(SCHelperTestClass.orderItem);
        
        // Id orderId, Id orderItemId, Id articleId, Id assignmentId
        SCboOrderLine boOrderLine1 = new SCboOrderLine(SCHelperTestClass.order.Id,
                                                       SCHelperTestClass.orderItem.Id,
                                                       SCHelperTestClass.articles[0].Id,
                                                       SCHelperTestClass.assignmentMap.get('assignmentDispo1').Id);
        SCboArticle boArticle1 = new SCboArticle(SCHelperTestClass.articles[0].Id);
        boOrderLine1.orderLine.Article__r = boArticle1.article;
        

        // set the required fields manually
        System.debug('###SCboOrderLine2');
        boOrderLine1.orderLine.Type__c = SCfwConstants.DOMVAL_ORDERLINETYPE_INV;
        boOrderLine1.orderLine.MaterialStatus__c = SCfwConstants.DOMVAL_MATSTAT_ORDERABLE;
        boOrderLine1.orderLine.Qty__c = 2.0;
        boOrderLine1.orderLine.ListPrice__c = 10.00;
        boOrderLine1.orderLine.Order__c = SCHelperTestClass.order.Id;
        boOrderItem.boOrderLines.add(boOrderLine1);

        SCboOrderLine boOrderLine2 = new SCboOrderLine(SCHelperTestClass.order.Id,
                                                       SCHelperTestClass.orderItem.Id,
                                                       SCHelperTestClass.articles[1].Id,
                                                       SCHelperTestClass.assignmentMap.get('assignmentDispo1').Id);
        SCboArticle boArticle2 = new SCboArticle(SCHelperTestClass.articles[1].Id);
        boOrderLine2.orderLine.Article__r = boArticle2.article;
        
        // set the required field manually
        boOrderLine2.orderLine.Type__c = SCfwConstants.DOMVAL_ORDERLINETYPE_INV;
        boOrderLine2.orderLine.MaterialStatus__c = SCfwConstants.DOMVAL_MATSTAT_ORDERABLE;
        boOrderLine2.orderLine.Qty__c = 1.0;
        boOrderLine2.orderLine.ListPrice__c = 20.00;
        boOrderLine2.orderLine.Order__c = SCHelperTestClass.order.Id;
        boOrderItem.boOrderLines.add(boOrderLine2);
        boOrderItem.save(); 

        Test.startTest();

        System.debug('###SCOrderMaterialController');
        SCOrderMaterialController controller = new SCOrderMaterialController('3', boOrder.order.Id, 
                                                                             SCHelperTestClass.stocks.get(0).Id, 
                                                                             null);
        
        // check initializes variables
        System.assert(controller.sourceList.size() > 0);
        System.assert(controller.receiverList.size() > 0);
        System.assert(controller.artInfoList.size() > 0);
        System.assert(controller.assignmentOk);

        // check the type list, should be filled
        List<SelectOption> typeList = controller.getTypeList();
        System.assert(controller.getHasArticles());
        
        // deselect all articles
        controller.selectAll();
        // select all articles
        controller.selectAll();   
        
        // now test the material booking
        controller.sourceStock = controller.sourceList.get(0).getValue();
        controller.receiverStock = controller.receiverList.get(0).getValue();
        controller.matMoveType = typeList.get(0).getValue();
        controller.matMove.ReplenishmentType__c = SCfwConstants.DOMVAL_MATERIALREPLENISHMENT_TYPE_NORMAL;
        
        controller.checkAndBookMat();
        //TODO System.assert(!controller.bookingOk);
        //TODO System.assert(controller.errMsg.length() > 0);
        
        controller.bookMat();
        System.assert(controller.bookingOk);
        System.assert(controller.errMsg.length() == 0);

        Test.stopTest();
    } // testMatBooking
    
    
    static testMethod void testCodeCoverage() 
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); } catch (Exception e) {}
        
        SCOrderMaterialController controller = new SCOrderMaterialController();
        
        try { controller.bookMat(); } catch (Exception e) {}
        controller.getShowPricing();
        controller.getValuationTypeActivated();
        controller.getHasValTypeToBeSelected();
        
        SCOrderMaterialController controller2 = new SCOrderMaterialController();
        controller2.getShowReplensihmentType();
        
        System.assertNotEquals(null,controller.getValuationTypeOptions());
    }
} // SCOrderMaterialControllerTest
