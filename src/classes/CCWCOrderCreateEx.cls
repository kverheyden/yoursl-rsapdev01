/* 
 * @(#)CCWCOrderCloseEx.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * www.gms-online.de 
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of
 * GMS Development GmbH. ("Confidential Information").  You shall not disclose 
 * any confidential information and shall use it only in accordance with the 
 * terms of the license agreement you aggreed with GMS.
 */
 
/**
 * CCWCOrderCloseEx
 * 
 * @author Georg Birkeheuer <gbirkenheuer@gms-online.de>
 *
 *
 * The entry methods are: 
 *      process(String oid)
 *      processNext(String oid)
 */
public without sharing class CCWCOrderCreateEx {
    private static SCOrder__c order;
    
    /**
     * readOrder - get the order from the database.
     * Set the global variable "order"
     * @param oid -> OrderNumber of the order
     * @return    -> TRUE - There was an order-entry in the database
     **/
    private static Boolean readOrder(String oid)
    {
        List<SCOrder__c> listOrder = [SELECT Id, ERPOrderNo__c, ERPStatusOrderCreate__c, ERPStatusEquipmentUpdate__c,
                                             ERPStatusMaterialReservation__c, ERPStatusMaterialMovement__c,
                                             ERPStatusExternalAssignmentAdd__c, ERPStatusExternalAssignmentRem__c,
                                             ERPStatusOrderClose__c, ERPStatusArchiveDocumentInsert__c,
                                             ERPAutoOrderCreate__c, Name, Status__c FROM SCOrder__c
                                       WHERE Id = :oid];
        
        if ((listOrder != null) && (listOrder.size() > 0))
        {
            order = listOrder.get(0);
            return true;
        }
        return false;
        
    }//readOrder
    
    /**
     * controlOrder - Controls the Properties of the order
     * @param oid -> OrderNumber of the order
     * @return    +-> TRUE - The next callout to SAP can be started
     *            +-> FALSE - The next callout to SAP can NOT be started
     **/
    private static Boolean controlOrder()
    {
        if (order == null) { return false; }
        
        try
        {

            if (order.Status__c == SCfwConstants.DOMVAL_ORDERSTATUS_OPEN
            	|| order.Status__c == SCfwConstants.DOMVAL_ORDERSTATUS_PENDING_ORDERS
            	|| order.Status__c == SCfwConstants.DOMVAL_ORDERSTATUS_RELEASED_FOR_PROCESSING)
            {
                System.debug('+++###+++ CCWCOrderCreateEx_controlOrder is true; ERPAutoOrderCreate__c: ' + order.ERPAutoOrderCreate__c + '; TimeStamp__c: ' + createTimeStamp());
                
                return true;
            }
            
        }
        catch (Exception e)
        {
            System.debug('+++###+++ CCWCOrderCreateEx_controlOrder has an Error; ERPAutoOrderCreate__c: ' + order.ERPAutoOrderCreate__c + '; TimeStamp__c: ' + createTimeStamp() + '; Exception: ' + e);
            
            return false;
        }
        
        System.debug('+++###+++ CCWCOrderCloseEx_controlOrder is false; ERPAutoOrderCreate__c: ' + order.ERPAutoOrderCreate__c + '; TimeStamp__c: ' + createTimeStamp());
            
        return false;
    }//controlOrder
    
    
    
    
    
    
    /**
     * process - Starts the automatic procedure for creating the order in SAP
     * The status-field of the order has to be equal to '5501' (open; SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL)
     * @param oid -> OrderNumber of the order
     * @return    -> TRUE - The necessary properties were set successfully
     **/
    public static Boolean process(String oid)
    {
        if (readOrder(oid))
        {
            if ((order.ERPAutoOrderCreate__c != null) && (order.ERPAutoOrderCreate__c.equalsIgnoreCase('Created')))
            {
                return false;
            }
            
            if ((order.ERPAutoOrderCreate__c == null) || (order.ERPAutoOrderCreate__c.equalsIgnoreCase('Inactive')))
            {
                order.ERPAutoOrderCreate__c = 'C0-Active';
                Database.update(order);
            }
            
            processNext(oid);
            
            return true;
        }
        return false;
    }
    
    /**
     * processNext - Continues the automatic procedure for closing the order in SAP
     * The status-field of the order has to be equal to '5508' (closed final; SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL)
     * @param oid -> OrderNumber of the order
     **/
    public static void processNext(String oid)
    {
        if (order == null)
        {
            readOrder(oid);
        }//order == null
        
        System.debug('+++###+++ CCWCOrderCloseEx_processNext before controlorder; ERPAutoOrderCreate__c: ' + order.ERPAutoOrderCreate__c + '; TimeStamp__c: ' + createTimeStamp());
        
        //order != null and no SAP-Job is running or in Error
        if (controlOrder())
        {
            System.debug('+++###+++ CCWCOrderCloseEx_processNext after controlorder; ERPAutoOrderCreate__c: ' + order.ERPAutoOrderCreate__c + '; TimeStamp__c: ' + createTimeStamp());
            //Step 1 - Order Create was successful
            if (   (order.ERPAutoOrderCreate__c != null)
                && (order.ERPAutoOrderCreate__c.equalsIgnoreCase('C0-Active')) )
            {
                processStep1AddExOp();
            }
            
            //Step 2 - processStep1AddExOp was successful
            else if (   (order.ERPAutoOrderCreate__c != null)
                     && (order.ERPAutoOrderCreate__c.equalsIgnoreCase('C1-AddExOp')) )
            {
                processStep2Created();
            }
        }//order != null and no SAP-Job is running or in Error
    }//processNext
    
 
    
    
    
    
    /**
     * processStep1AddExOp - Starts the SAP-Job "Add external Opeation"
     **/
    private static void processStep1AddExOp()
    {
        System.debug('+++###+++ processStep1AddExOp control; ERPAutoOrderCreate__c: ' + order.ERPAutoOrderCreate__c + '; TimeStamp__c: ' + createTimeStamp());
        if (   (order.ERPOrderNo__c != null)           && (order.ERPOrderNo__c != '')
            && (order.ERPStatusOrderCreate__c != null) && (order.ERPStatusOrderCreate__c.equalsIgnoreCase('ok')) )
        {
            order.ERPAutoOrderCreate__c = 'C1-AddExOp';
            Database.update(order);
            
            System.debug('+++###+++ processStep1AddExOp start; ERPAutoOrderCreate__c: ' + order.ERPAutoOrderCreate__c + '; TimeStamp__c: ' + createTimeStamp());
            //CCWCOrderEquipmentUpdate.callout(order.id, true, Test.isRunningTest());
            
            SCOrderExternalAssignment__c assignment = readExternalAssignment(order.ID);
            if (assignment != null)
            {
        	CCWCOrderExternalOperationAdd.callout(assignment.id, true, false);
            }        
        }
    }//processStep1AddExOp
    

    /**
     * processStep2Created - Ends the AutoCreateOrder
     **/
    private static void processStep2Created()
    {
        System.debug('+++###+++ processStep2Created control; ERPAutoOrderCreate__c: ' + order.ERPAutoOrderCreate__c + '; TimeStamp__c: ' + createTimeStamp());
        if ((order.ERPStatusExternalAssignmentAdd__c != null) && (order.ERPStatusExternalAssignmentAdd__c.equalsIgnoreCase('ok')))
        {
            order.ERPAutoOrderCreate__c = 'Created';
            Database.update(order);
            
            System.debug('+++###+++ processStep2Created end; ERPAutoOrderCreate__c: ' + order.ERPAutoOrderCreate__c + '; TimeStamp__c: ' + createTimeStamp());
        }
    }//processStep2Created
    
     /**
     * Reads an external assignment that belongs to the order id 
     *
     * @param orderID
     *
     */
    public static SCOrderExternalAssignment__c readExternalAssignment(ID orderID)
    {
        debug('orderID: ' + orderID);

        List<SCOrderExternalAssignment__c> oeal =  [Select ID, Order__c, Order__r.ID 
                                                  	from SCOrderExternalAssignment__c
                                					where Order__r.ID = : orderID];
                               
        if(oeal.size() > 0)
        {
			return oeal[0];
        }
        else
        {
			return null; 
        }
    }
    
    
    
    
    private static String createTimeStamp()
    {
        DateTime timeNow = Datetime.now();
        String timeStamp = timeNow.yearGMT() + '/' + timeNow.monthGMT() + '/' + timeNow.dayGMT() + ' ';
        timeStamp += (timeNow.hourGMT() < 10 ? '0' : '') + timeNow.hourGMT();
        timeStamp += ':' + (timeNow.minuteGMT() < 10 ? '0' : '') + timeNow.minuteGMT();
        timeStamp += ':' + (timeNow.secondGMT() < 10 ? '0' : '') + timeNow.secondGMT();
        timeStamp += ',' + (timeNow.millisecondGMT() < 10 ? '0' : '') + (timeNow.millisecondGMT() < 100 ? '0' : '') + timeNow.millisecondGMT();
        return timeStamp;
    }
    
    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }
}
