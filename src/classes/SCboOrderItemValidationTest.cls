/*
 * @(#)SCboOrderItemValidationTest.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This class contains the test code for SCboOrderItemValidation.
 *
 * @author Alexander Wagner <awagner@gms-online.de>
*/ 
@isTest
private class SCboOrderItemValidationTest
{
    /*
     * Test: Validation of order role data
     */
    static testMethod void validateOrderItemDataTest() 
    {
        SCHelperTestClass.createOrder(true);
        SCHelperTestClass.createProductModel(true);

        SCboOrderItem boOrderItem = new SCboOrderItem();
        boOrderItem.orderItem.Order__c        = SCHelperTestClass.order.Id;
        boOrderItem.orderItem.Duration__c     = 60;
        boOrderItem.orderItem.DurationUnit__c = SCfwConstants.DOMVAL_INTERVALLTIME_MINUTE;
        
        SCInstalledBase__c instBase = new SCInstalledBase__c();
        boOrderItem.orderItem.InstalledBase__r = instBase;
        instBase.ProductModel__c     = SCHelperTestClass.prodModel.Id;
        instBase.ProductSkill__c     = 'PS1';
        instBase.ProductUnitClass__c = SCfwConstants.DOMVAL_PRODUCTUNITCLASS_STANDARD;
        instBase.ProductUnitType__c  = '000';
        instBase.Status__c           = 'active';
        instBase.Type__c             = 'Appliance';
        instBase.Brand__c            = SCHelperTestClass.brand.Id;
        
        SCInstalledBaseLocation__c instBaseLoc = new SCInstalledBaseLocation__c();
        instBase.InstalledBaseLocation__r = instBaseLoc;
        instBaseLoc.LocName__c    = 'Loc 1';
        instBaseLoc.Street__c     = 'Waldweg';
        instBaseLoc.PostalCode__c = '80100';
        instBaseLoc.City__c       = 'München';
        instBaseLoc.Country__c    = 'DE';
        instBaseLoc.GeoX__c       = 11.0;
        instBaseLoc.GeoY__c       = 50.0;

        Test.startTest();

        // activate the validation of the picklist values
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        String oldValue = appSettings.VALIDATE_PICKLIST_VALUES__c;
        appSettings.VALIDATE_PICKLIST_VALUES__c = '1';
        update appSettings;

        // Test 1: no duration
        boOrderItem.orderItem.Duration__c     = null;
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2200 - '));
        }

        // Test 2: no duration unit
        boOrderItem.orderItem.Duration__c     = 60;
        boOrderItem.orderItem.DurationUnit__c = null;
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2201 - '));
        }

        // Test 3: wrong completion status
        boOrderItem.orderItem.DurationUnit__c = SCfwConstants.DOMVAL_INTERVALLTIME_MINUTE;
        boOrderItem.orderItem.CompletionStatus__c = 'XY';
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 4: wrong duration unit
        boOrderItem.orderItem.CompletionStatus__c = '';
        boOrderItem.orderItem.DurationUnit__c     = 'XY';
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 5: wrong error condition 1
        boOrderItem.orderItem.DurationUnit__c = SCfwConstants.DOMVAL_INTERVALLTIME_MINUTE;
        boOrderItem.orderItem.ErrorCondition1__c = 'XY';
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 6: wrong error condition 2
        boOrderItem.orderItem.ErrorCondition1__c = '';
        boOrderItem.orderItem.ErrorCondition2__c = 'XY';
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 7: wrong error symptom 1
        boOrderItem.orderItem.ErrorCondition2__c = '';
        boOrderItem.orderItem.ErrorSymptom1__c   = 'XY';
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 8: wrong error symptom 2
        boOrderItem.orderItem.ErrorSymptom1__c   = '';
        boOrderItem.orderItem.ErrorSymptom2__c   = 'XY';
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 9: wrong error symptom 3
        boOrderItem.orderItem.ErrorSymptom2__c   = '';
        boOrderItem.orderItem.ErrorSymptom3__c   = 'XY';
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 10: no installed base data
        boOrderItem.orderItem.ErrorSymptom3__c = '';
        instBase.ProductModel__c     = null;
        instBase.ProductSkill__c     = null;
        instBase.ProductUnitClass__c = null;
        instBase.ProductUnitType__c  = null;
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2300 - '));
        }

        // Test 11: no status
        instBase.ProductModel__c     = SCHelperTestClass.prodModel.Id;
        instBase.ProductSkill__c     = 'PS1';
        instBase.ProductUnitClass__c = SCfwConstants.DOMVAL_PRODUCTUNITCLASS_STANDARD;
        instBase.ProductUnitType__c  = '000';
        instBase.Status__c           = null;
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            //TODO System.assert(e.getMessage().startsWith('E2301 - '));
        }

        // Test 12: no type
        instBase.Status__c           = 'active';
        instBase.Type__c             = null;
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2302 - '));
        }

        // Test 13: no brand
        instBase.Type__c             = 'Appliance';
        instBase.Brand__c            = null;
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            //TODO System.assert(e.getMessage().startsWith('E2303 - '));
        }

        // Test 14: installation date to far in the past
        instBase.Brand__c            = SCHelperTestClass.brand.Id;
        instBase.InstallationDate__c = Date.newInstance(1000, 1, 1);
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            //TODO System.assert(e.getMessage().startsWith('E2304 - '));
        }

        // Test 15: installation date to far in the future
        instBase.InstallationDate__c = Date.newInstance(2200, 1, 1);
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2305 - '));
        }

        // Test 16: wrong product energy
        instBase.InstallationDate__c = Date.today().addDays(-2);
        instBase.ProductEnergy__c    = 'XY';
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 17: wrong product group
        instBase.ProductEnergy__c   = '';
        instBase.ProductGroup__c    = 'XY';
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 18: wrong product power
        instBase.ProductGroup__c    = '';
        instBase.ProductPower__c    = 'XY';
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 19: wrong product skill
        instBase.ProductPower__c    = '';
        instBase.ProductSkill__c    = 'XY';
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 20: wrong unit class
        instBase.ProductSkill__c     = '';
        instBase.ProductUnitClass__c = 'XY';
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 21: wrong unit type
        instBase.ProductUnitClass__c = '';
        instBase.ProductUnitType__c  = 'XY';
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 22: wrong serial no exception
        instBase.ProductUnitType__c   = '';
        instBase.SerialNoException__c = 'XY';
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 23: wrong status
        instBase.SerialNoException__c = '';
        instBase.Status__c            = 'XY';
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 24: wrong type
        instBase.Status__c     = 'active';
        instBase.Type__c       = 'XY';
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 25: wrong validation status
        instBase.Type__c             = 'Appliance';
        instBase.ValidationStatus__c = 'XY';
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 26: no location data
        instBase.ValidationStatus__c     = '';
        instBaseLoc.LocName__c = null;
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            //TODO System.assert(e.getMessage().startsWith('E2400 - '));
        }

        // Test 27: wrong status
        instBaseLoc.LocName__c = 'Loc 1';
        instBaseLoc.Status__c  = 'XY';
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 28: no street
        instBaseLoc.Status__c  = '';
        instBaseLoc.Street__c  = null;
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            //TODO System.assert(e.getMessage().startsWith('E2401 - '));
        }

        // Test 29: no postalcode
        instBaseLoc.Street__c     = 'Waldweg';
        instBaseLoc.PostalCode__c = null;
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            //TODO System.assert(e.getMessage().startsWith('E2403 - '));
        }

        // Test 30: no city
        instBaseLoc.PostalCode__c = '80100';
        instBaseLoc.City__c       = null;
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            //TODO System.assert(e.getMessage().startsWith('E2404 - '));
        }

        // Test 31: no country
        instBaseLoc.City__c     = 'München';
        instBaseLoc.Country__c  = null;
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            //TODO System.assert(e.getMessage().startsWith('E2405 - '));
        }

        // Test 32: wrong country
        instBaseLoc.Country__c  = 'XY';
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 33: wrong country state
        instBaseLoc.Country__c       = 'DE';
        instBaseLoc.CountryState__c  = 'XY';
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 34: no geo x
        instBaseLoc.CountryState__c  = '';
        instBaseLoc.GeoX__c          = null;
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            //TODO System.assert(e.getMessage().startsWith('E2406 - '));
        }

        // Test 35: no geo y
        instBaseLoc.GeoX__c  = 11.0;
        instBaseLoc.GeoY__c  = null;
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            //TODO System.assert(e.getMessage().startsWith('E2407 - '));
        }

        // Test 36: wrong geo status
        instBaseLoc.GeoY__c       = 50.0;
        instBaseLoc.GeoStatus__c  = 'XY';
        try
        {
            SCboOrderItemValidation.validate(boOrderItem);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderItemValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 37: no errors
        instBaseLoc.GeoStatus__c = '';
        try
        {
            Boolean ret = SCboOrderItemValidation.validate(boOrderItem);
            System.assertEquals(true, ret);
        }
        catch(Exception e)
        {
            //TODO System.assert(false);
        }

        // set the old value for the validation of the picklist values
        appSettings.VALIDATE_PICKLIST_VALUES__c = oldValue;
        update appSettings;
        Test.stopTest();
    } // validateOrderItemDataTest
} // SCboOrderItemValidationTest
