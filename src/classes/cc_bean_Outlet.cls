global with sharing class cc_bean_Outlet {
    public String sfid { get; set; }
    public String name { get; set; }
    public String outlet { get; set; }
    public String address1 { get; set; }
    public String address2 { get; set; }
    public String city { get; set; }
    public String state { get; set; }
    public String postalCode { get; set; }
    public String countryCode { get; set; }
    public String shipTerms { get; set; }
    public String outletType { get; set; }
    public String channel { get; set; }
    public String plant { get; set; }
    public Date nextDelivery { get; set; }
    public String deliveryDays { get; set; }

    public cc_bean_Outlet(Account acct) {
        this.sfid = acct.Id;
        this.name = acct.Name;
        this.outlet = acct.AccountNumber;
        this.address1 = acct.BillingStreet;
        this.city = acct.BillingCity;
        this.state = acct.BillingState;
        this.postalCode = acct.BillingPostalCode;
        this.countryCode = acct.BillingCountry;
        try { this.plant = acct.DeliveringPlant__c; } catch (Exception e) {}
        try { this.shipTerms = acct.ShippingTerms__c; } catch (Exception e) {}
        try { this.nextDelivery = acct.NextPossibleDeliveryDate__c; } catch (Exception e) {}
        try { this.deliveryDays = acct.DeliveryWeekdays__c; } catch (Exception e) {}
        try {
            if (acct.BillingHouseNo__c != null && !acct.BillingStreet.endsWith(acct.BillingHouseNo__c)) {
                this.address1 += ' ' + acct.BillingHouseNo__c;
            }
        }
        catch (Exception e) {}
    }

    public ccrz__E_ContactAddr__c convertToAddress() {
        ccrz__E_ContactAddr__c addr = new ccrz__E_ContactAddr__c();
        addr.ccrz__AddressFirstline__c = address1;
        addr.ccrz__City__c = city;
        addr.ccrz__StateISOCode__c = state;
        addr.ccrz__PostalCode__c = postalCode;
        addr.ccrz__Country__c = countryCode;
        addr.ccrz__CountryISOCode__c = countryCode;
        addr.ccrz__Partner_Id__c = outlet;
        addr.ccrz__CompanyName__c = name;
        addr.ccrz__MailStop__c = plant;
        return addr;
    }

}
