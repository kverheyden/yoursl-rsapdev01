@isTest//(SeeAllData = true)
private class CCWSAddChatterFeedItemTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Profile p = [select id from profile where name='System Administrator']; 
        User testUser = new User(alias = 'sta23', email='admin@testorg.com', 
            emailencodingkey='UTF-8', lastname='Testing123', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, 
            timezonesidkey='America/Los_Angeles', username='admin@cceag.de', isActive=true, ID2__c = '12345678');
            
    	System.runAs(testUser) {
        //++++++++++++++ basic case +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        
        
 
	    Account testAccount = new Account();
	 
	    RecordType accountRecType= [SELECT Id, Name, SobjectType FROM RecordType WHERE SobjectType = 'Account' AND Name = 'Customer' LIMIT 1];
	    testAccount.RecordTypeId = accountRecType.Id;
	    
	    testAccount.Name = 'Max Mustermann';
	    testAccount.BillingCountry__c = 'DE';
	    testAccount.ID2__c = '12345678';
	    
	    insert testAccount;
	    testAccount = [SELECT Name, Id, ID2__c FROM Account WHERE Name = 'Max Mustermann' AND BillingCountry__c = 'DE' AND ID2__c = '12345678' LIMIT 1];
	    String teststring ='12345';
	    Blob testblob = Blob.valueOf(testString);
	  	CCWSAddChatterFeeditem.addFeed('Account', '12345678', testUser.ID2__c, 'Test Body', 'testName', testblob);
	    
        // Case where no user is found
        List<Account> listAccounts = [SELECT Name, Id, ID2__c FROM Account WHERE ID2__c = '1236'];
        system.assertEquals(0, listAccounts.size());
	   	CCWSAddChatterFeeditem.addFeed('Account', '12345678', '123' , 'Test Body', 'testName', testblob);
		// Case where no account is found
        CCWSAddChatterFeeditem.addFeed('Account', '1236', '12345678' , 'Test Body', 'testName', testblob);
		
        // Case where the sParentType does not match the assumed
        CCWSAddChatterFeeditem.addFeed('NotMatchedParentType', '1236', '12345678' , 'Test Body', 'testName', testblob);
	    //+++++++++++++ Promotion part ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	    
	    Promotion__c oTestPromotion		= new Promotion__c();
	    oTestPromotion.Name				= 'Testpromotion';
	    oTestPromotion.PromotionID__c	= '54321';
	    insert oTestPromotion;
        
        // Duc Nguyen Tien quickFix: adding custom settings for Promotion
        List<ContentWorkspace> workspaces = [Select ID from ContentWorkspace where Name = 'Sales Folder'];
        //system.assertEquals(1, workspaces.size()); <- there's problem with accessing workspace if you are not a member. Still need to resolve.
        SalesAppSettings__c setting = new SalesAppSettings__c(Name = 'PromotionLibraryId', value__c = '058L00000004eEkIAI');
        insert setting;
        List<RecordType> rt = [Select ID from RecordType where DeveloperName = 'Sales_Folder' AND SobjectType = 'ContentVersion'];
        system.assertEquals(1, rt.size());
        SalesAppSettings__c setting2 = new SalesAppSettings__c(Name = 'SalesFolderRecordTypeId', value__c = rt.get(0).ID);
        insert setting2;
            
		CCWSAddChatterFeeditem.addFeed('Promotion', '54321', testUser.ID2__c, 'Test Body', 'testName', testblob);
            
        // Case where no promotionID is incorrect or not existing
        CCWSAddChatterFeeditem.addFeed('Promotion', 'CantFindThisID', testUser.ID2__c, 'Test Body', 'testName', testblob);
            
        // Case where there two promotion records with the same Id
        Promotion__c oTestPromotion2		= new Promotion__c();
	    oTestPromotion2.Name				= 'Testpromotion2';
	    oTestPromotion2.PromotionID__c	= '54321';
	    insert oTestPromotion2;
        
        CCWSAddChatterFeeditem.addFeed('Promotion', '54321', testUser.ID2__c, 'Test Body', 'testName', testblob);    
        }
    }
}
