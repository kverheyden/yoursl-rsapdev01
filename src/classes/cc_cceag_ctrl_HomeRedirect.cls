public with sharing class cc_cceag_ctrl_HomeRedirect {
    public cc_cceag_ctrl_HomeRedirect() {
        
    }

    public PageReference forwardToHomePage() {
        
        if (UserInfo.getUserType() == 'Guest') {
            PageReference ref = Page.EntryLogin;
            return ref;
        }
        else {
            PageReference ref = Page.ccrz__CCPage;
            String url = ref.getUrl();
            url = url.replace('ccpage', 'CCPage');
            ref = new PageReference(url);
            ref.getParameters().put('pageKey', 'Landing');
            return ref;
        }
    }
}
