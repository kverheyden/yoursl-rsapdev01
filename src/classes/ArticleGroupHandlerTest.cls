@isTest
private class ArticleGroupHandlerTest {

	@isTest 
	static void testArticleGroupHandler() {
		Folder f = [SELECT Id FROM Folder LIMIT 1];
		Document d = new Document(FolderId = f.Id, Name = 'TestDoc.pdf', DeveloperName = 'TestDoc', Body = Blob.valueOf('1234'));
		insert d;

		ArticleGroup__c ag = new ArticleGroup__c (
			Group__c = 'Fanta Erdbeere',
			KavRelevant__c = false,
			KindOfPackage__c = 'PET',
			MeasuringUnit__c = 'L',
			Name = 'Fanta Erdbeere 1L PET',
			NoAnnualOrderDiscount__c = false,
			NoCateringBottleDiscount__c = false,
			NoCentralBillingDiscount__c = false,
			NoSponsoringDiscount__c = false,
			Packaging_size__c = '1',
			Status__c = 'Active',
			SubGroup__c = 'Standard',
			PictureDocDevName__c = 'TestDoc'
		);

		insert ag;
	}
}
