/*
 * @(#)SCboEngineerStandbyTest.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Test for Helper for evaluating engineer standbys
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest 
private class SCboEngineerStandbyTest
{
   /*
    * Test reading and converting engineer standbys.
    */
    private static testmethod void testBasic()
    {
        SCHelperTestClass2.createEngineerStandbys(true);
        
        String ret;
        Date today = Date.Today();
        List<String> groups = new List<String>();
        List<SCEngineerStandby__c> engineers;
        
        Test.startTest();        

        engineers = SCboEngineerStandby.readStandbys(SCHelperTestClass.resources[0].Id, groups, null, null);
        System.AssertEquals(1, engineers.size());
        
        ret = SCboEngineerStandby.convert(engineers, -1);
        System.AssertNotEquals('', ret);
        
        groups.add('Group 1');
        engineers = SCboEngineerStandby.readStandbys(SCHelperTestClass.resources[0].Id, groups, today, today);
        System.AssertEquals(2, engineers.size());
        
        ret = SCboEngineerStandby.convert(engineers, 2);
        System.AssertNotEquals('', ret);
        System.debug('#### testBasic: ret -> ' + ret);

        Test.stopTest();
    }    
}
