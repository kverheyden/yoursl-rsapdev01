/*
 * @(#)CCWSUtilTest.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Tests for interface utilities 
 */
@isTest
private class CCWSUtilTest
{
    static testMethod void testGetMessageID ()
    {
        CCWSUtil u = new CCWSUtil();
        String id = u.getMessageID();
        System.assertNotEquals(null, id);
        
    } // testGetMessageID 

    static testMethod void testGetCreationDateTime()
    {
    /*
        CCWSUtil u = new CCWSUtil();
        String utc = u.getFormatedCreationDateTime();
        System.assertNotEquals(null, utc);
        
        String utcGmt = u.getGMTCreationDateTime ();
        System.assertNotEquals(null, utcGmt);
      */  
    } // testGetCreationDateTime


   /*
    * Test 
    */
    static testMethod void testMuster()
    {
        Test.StartTest();
        Test.StopTest();
    } 
    
    
   /*
    * Test the automatic creation of plants
    */
    static testMethod void testselectOrCreatePlants()
    {
        Test.StartTest();
        
        List<String> plantnames = new List<String>();
        plantnames.add('GMStestPlant1');
        
        // we expect that exactly on plant is in the result has table
        Map<String, SCPlant__c> mapPlants = CCWSUtil.selectOrCreatePlants(plantnames, 'CCWSUtilTest');        
        System.assertEquals(mapPlants.size(), 1, 'test 1.a failed - plant not created or multiple records found');

        SCPlant__c plant = mapPlants.get('GMStestPlant1');
        System.assertNotEquals(plant, null, 'test 1.b failed - plant not found');
        if(plant != null)
        {
            System.assertEquals(plant.name,   'GMStestPlant1', 'test 1.c failed - name invalid');
            System.assertEquals(plant.ID2__c, 'GMStestPlant1', 'test 1.d failed - ID2__c invalid');
        }    
        // if we call the method again no change is expected
        mapPlants = CCWSUtil.selectOrCreatePlants(plantnames, 'CCWSUtilTest');        
        System.assertEquals(mapPlants.size(), 1, 'test 2 failed - multiple plants created or plant not found');

        // we expect that an additional plant is created
        plantnames.add('GMStestPlant2');
        mapPlants = CCWSUtil.selectOrCreatePlants(plantnames, 'CCWSUtilTest');        
        System.assertEquals(mapPlants.size(), 2, 'test 3 failed - additional plant not created');

        // we expect that white space is ignored
        plantnames.add('  ');
        plantnames.add('');
        plantnames.add(null);
        mapPlants = CCWSUtil.selectOrCreatePlants(plantnames, 'CCWSUtilTest');        
        System.assertEquals(mapPlants.size(), 2, 'test 4 failed - white space handling fault');

        Test.StopTest();
        
    } // testselectOrCreatePlants
    
    
   /*
    * Test the automatic creation of stocks
    */
    static testMethod void testselectOrCreateStocks()
    {
        List<String> plantnames = new List<String>();
        plantnames.add('GMStestPlant1');
        Map<String, SCPlant__c> mapPlants = CCWSUtil.selectOrCreatePlants(plantnames, 'CCWSUtilTest');        
        SCPlant__c plant = mapPlants.get('GMStestPlant1');

        Test.StartTest();
        String stock1Name = CCWSUtil.getStockSelector('GMStestPlant1', 'GMStestStock1');
        List<String> stocknames = new List<String>();
        stocknames.add(stock1Name );
        
        // we assume that one stock will be created for this plant
        Map<String, SCStock__c> mapStocks = CCWSUtil.selectOrCreateStocks(stocknames, mapPlants, 'CCWSUtilTest');        
        System.assertEquals(mapStocks.size(), 1, 'test 1.a failed - stocks not created or multiple records found');

        SCStock__c stock = mapStocks.get(stock1Name);
        System.assertNotEquals(stock, null, 'test 1.b failed - stock not found');
        if(stock != null)
        {
            System.assertEquals(stock.name,   'GMStestStock1', 'test 1.c failed - name invalid');
            System.assertEquals(stock.ID2__c, stock1Name,      'test 1.d failed - ID2__c invalid');
            System.assertEquals(stock.plant__c, plant.id,      'test 1.e failed - plant invalid');
        }    

        // if we call the method again no change is expected
        mapStocks = CCWSUtil.selectOrCreateStocks(stocknames, mapPlants, 'CCWSUtilTest');        
        System.assertEquals(mapStocks.size(), 1, 'test 2 failed - multiple stocks created or plant not found');

        // we expect that an additional stock is created
        stocknames.add(CCWSUtil.getStockSelector('GMStestPlant1', 'GMStestStock2'));
        mapStocks = CCWSUtil.selectOrCreateStocks(stocknames, mapPlants, 'CCWSUtilTest');        
        System.assertEquals(mapStocks.size(), 2, 'test 3 failed - additional stock not created');


        // we expect that white space is ignored
        stocknames.add('  ');
        stocknames.add('');
        stocknames.add(null);
        mapStocks = CCWSUtil.selectOrCreateStocks(stocknames, mapPlants, 'CCWSUtilTest');        
        System.assertEquals(mapStocks.size(), 2, 'test 4 failed - white space handling fault');

        Test.StopTest();
        
    } // testselectOrCreateStocks
    
    
   /*
    * Test the automatic creation of stocks helper functions
    */
    static testMethod void testselectOrCreateStockHelper()
    {
        Test.StartTest();

        Map<String, SCPlant__c> mapPlants = CCWSUtil.selectOrCreatePlant('GMStestPlant1', 'CCWSUtilTest');
        System.assertEquals(mapPlants.size(), 1, 'test 1.a failed - plant helper failed');
        
        
        SCStock__c stock = CCWSUtil.selectOrCreateStock('GMStestPlant1', 'GMStestStock1', 'CCWSUtilTest');
        System.assertNotEquals(stock, null, 'test 1.b failed - stock not found');
        if(stock != null)
        {
            System.assertEquals(stock.name,   'GMStestStock1', 'test 1.c failed - name invalid');
        }         

        String stockselector = CCWSUtil.getStockSelector('a', 'b');
        System.assertEquals(stockselector, 'a-b', 'test 1.c failed - stock selector failed');
        
        Test.StopTest();
    }

   /*
    * Test the automatic creation of pricelists
    */
    static testMethod void testselectOrCreatePriceList()
    {
        Test.StartTest();

        String name1 = 'GMSPricelist1';
        String name2 = 'GMSPricelist2';
        
        List<String> itemNames = new List<String>();
        itemNames.add(name1);

        Map<String, SCPriceList__c> mapItems = CCWSUtil.selectOrCreatePriceLists(itemNames, 'TEST', 'CCWSUtilTest');
        System.assertEquals(mapItems.size(), 1, 'test 1.a failed - pricelist not created or multiple records found');

        SCPriceList__c item = mapItems.get(name1);
        if(item != null)
        {
            System.assertEquals(item.name,   name1, 'test 1.b failed - name invalid');
            System.assertEquals(item.ID2__c, name1, 'test 1.c failed - ID2__c invalid');
        }    

        // create another pricelist
        itemNames.add(name2);
        mapItems = CCWSUtil.selectOrCreatePriceLists(itemNames, 'TEST', 'CCWSUtilTest');
        System.assertEquals(mapItems.size(), 2, 'test 1.d failed - new record not created');

        Test.StopTest();
    }


   /*
    * Test the automatic creation of product models
    */
    static testMethod void testselectOrCreateProduct()
    {
        Test.StartTest();

        String name1 = 'GMSProduct1';
        String name2 = 'GMSProduct2';
        
        List<String> itemNames = new List<String>();
        itemNames.add(name1);

        Map<String, SCProductModel__c> mapItems = CCWSUtil.selectOrCreateProductModel(itemNames, 'CCWSUtilTest');
        System.assertEquals(mapItems.size(), 1, 'test 1.a failed - record not created or multiple records found');

        SCProductModel__c item = mapItems.get(name1);
        if(item != null)
        {
            System.assertEquals(item.name,   name1, 'test 1.b failed - name invalid');
            System.assertEquals(item.ID2__c, name1, 'test 1.c failed - ID2__c invalid');
            System.assertEquals(item.UnitClass__c, SCfwConstants.DOMVAL_PRODUCTUNITCLASS_STANDARD, 'test 1.d failed - UnitClass__c invalid');
            System.assertEquals(item.UnitType__c, SCfwConstants.DOMVAL_PRODUCTUNITTYPE_DEFAULT, 'test 1.e failed - UnitType__c invalid');
        }    
        
        itemNames.add(name2);
        mapItems = CCWSUtil.selectOrCreateProductModel(itemNames, 'CCWSUtilTest');
        System.assertEquals(mapItems.size(), 2, 'test 1.d failed - new record not created');
        
        
        Test.StopTest();
    }

   /*
    * Test the automatic creation of articles (spareparts or products)
    */
    static testMethod void testselectOrCreateArticle()
    {
        String name1 = 'GMSArticle1';
        String name2 = 'GMSArticleProduct2';
       
        // create a test product
        List<String> productNames = new List<String>();
        productNames.add(name2);
        Map<String, SCProductModel__c> mapProducts = CCWSUtil.selectOrCreateProductModel(productNames, 'CCWSUtilTest');
        SCProductModel__c product = mapProducts.get(name2);
 
    
        Test.StartTest();
 
        // 1. create a sparepart (product model map is null)
        List<String> itemNames = new List<String>();
        itemNames.add(name1);
        Map<String, SCArticle__c> mapItems = CCWSUtil.selectOrCreateArticles(itemNames, null, 'CCWSUtilTest');
        System.assertEquals(mapItems.size(), 1, 'test 1.a failed - record not created or multiple records found');

        SCArticle__c item = mapItems.get(name1);
        if(item != null)
        {
            System.assertEquals(item.name,   name1, 'test 1.b failed - name invalid');
            System.assertEquals(item.ID2__c, name1, 'test 1.c failed - ID2__c invalid');
            
            System.assertEquals(item.Class__c, SCfwConstants.DOMVAL_ARTICLECLASS_SPAREPART, 'test 1.d failed - Class__c invalid');
            System.assertEquals(item.Type__c, SCfwConstants.DOMVAL_ARTICLETYP_NO_TYP, 'test 1.e failed - Type__c invalid');
        }    


        // 2. create a product based article 
        itemNames.add(name2);
        mapItems = CCWSUtil.selectOrCreateArticles(itemNames, mapProducts, 'CCWSUtilTest');
        System.assertEquals(mapItems.size(), 2, 'test 2.a failed - record not created or multiple records found');
        item = mapItems.get(name2);
        if(item != null)
        {
            System.assertEquals(item.name,   name2, 'test 2.b failed - name invalid');
            System.assertEquals(item.ID2__c, name2, 'test 2.c failed - ID2__c invalid');
            
            System.assertEquals(item.Class__c, SCfwConstants.DOMVAL_ARTICLECLASS_PRODUCT, 'test 2.d failed - Class__c invalid');
            System.assertEquals(item.Type__c, SCfwConstants.DOMVAL_ARTICLETYP_PRODUCT, 'test 2.e failed - Type__c invalid');
            
            System.assertEquals(item.productModel__c, product.id, 'test 2.f failed - product__c invalid');
        }

        Test.StopTest();
    }

   /*
    * Test the endpoint determination 
    */
    static testMethod void testGetEndpoint()
    {
        String srv = 'mysrv/';
    
        CCWSUtil cc = new CCWSUtil();
        cc.ccSettings.SAPDispSenderSystemID__c = 'SRC';
        cc.ccSettings.SAPDispRecipientSystemID__c = 'DEST';
        cc.ccSettings.SAPDispBasicAuthUsername__c = 'USER';
        cc.ccSettings.SAPDispBasicAuthPassword__c = 'PWD';
        cc.ccSettings.SAPDispServer__c = srv;
        cc.ccSettings.SAPDispEndpointOrderCreate__c = 'A';
        cc.ccSettings.SAPDispEndpointOrderClose__c = 'B';
        cc.ccSettings.SAPDispEndpointOrderEquipmentUpdate__c = 'C';
        cc.ccSettings.SAPDispEndpointMaterialMovementCreate__c = 'D';
        cc.ccSettings.SAPDispEndpointMaterialReservationCreate__c = 'E';
        cc.ccSettings.SAPDispEndpointOrderExternalOperationAdd__c = 'F';
        cc.ccSettings.SAPDispEndpointOrderExternalOperationRem__c = 'G';
        cc.ccSettings.SAPDispEndpointMaterialInventoryCreate__c = 'H';
        cc.ccSettings.SAPDispEndpointArchiveDocumentInsert__c = 'I';
        cc.ccSettings.SAPDispEndpointMaterialPackageReceived__c = 'J';
        cc.ccSettings.SAPDispEndpointCustomerCreate__c = 'K';
        cc.ccSettings.SAPDispEndpointAssetInventory__c = 'L';
        cc.ccSettings.SAPDispEndpointNotizenOut__c = 'M';
        cc.ccSettings.SAPDispEndpointSalesOrder__c = 'N';
        cc.ccSettings.SAPDispEndpointFaxService__c = 'O';
        


        Test.StartTest();
    
        // 1. test some helper functions
        System.assertEquals(cc.getSenderBusinessSystemID(), cc.ccSettings.SAPDispSenderSystemID__c);
        System.assertEquals(cc.getRecipientBusinessSystemID(), cc.ccSettings.SAPDispRecipientSystemID__c);

        // 2. test the endpoint creation functions
        System.assertEquals(cc.getEndpoint('OrderCreate'), srv + cc.ccSettings.SAPDispEndpointOrderCreate__c);
        System.assertEquals(cc.getEndpoint('OrderClose'), srv + cc.ccSettings.SAPDispEndpointOrderClose__c);
        System.assertEquals(cc.getEndpoint('OrderEquipmentUpdate'), srv + cc.ccSettings.SAPDispEndpointOrderEquipmentUpdate__c);
        System.assertEquals(cc.getEndpoint('MaterialMovementCreate'), srv + cc.ccSettings.SAPDispEndpointMaterialMovementCreate__c);
        System.assertEquals(cc.getEndpoint('MaterialReservationCreate'), srv + cc.ccSettings.SAPDispEndpointMaterialReservationCreate__c);
        System.assertEquals(cc.getEndpoint('OrderExternalOperationAdd'), srv + cc.ccSettings.SAPDispEndpointOrderExternalOperationAdd__c);
        System.assertEquals(cc.getEndpoint('OrderExternalOperationRemove'), srv + cc.ccSettings.SAPDispEndpointOrderExternalOperationRem__c);
        System.assertEquals(cc.getEndpoint('MaterialInventoryCreate'), srv + cc.ccSettings.SAPDispEndpointMaterialInventoryCreate__c);
        System.assertEquals(cc.getEndpoint('ArchiveDocumentInsert'), srv + cc.ccSettings.SAPDispEndpointArchiveDocumentInsert__c);
        System.assertEquals(cc.getEndpoint('MaterialPackageReceived'), srv + cc.ccSettings.SAPDispEndpointMaterialPackageReceived__c);
        System.assertEquals(cc.getEndpoint('CustomerCreate'), srv + cc.ccSettings.SAPDispEndpointCustomerCreate__c);
        System.assertEquals(cc.getEndpoint('AssetInventoryDataRequest'), srv + cc.ccSettings.SAPDispEndpointAssetInventory__c);
        System.assertEquals(cc.getEndpoint('NotizenOut'), srv + cc.ccSettings.SAPDispEndpointNotizenOut__c);
        System.assertEquals(cc.getEndpoint('SalesOrder'), srv + cc.ccSettings.SAPDispEndpointSalesOrder__c);    
        System.assertEquals(cc.getEndpoint('FaxService'), srv + cc.ccSettings.SAPDispEndpointFaxService__c);       
           
        //System.assertEquals(cc.getEndpoint(''), srv + cc.ccSettings.);
        
        // 3. test the basic authentication function
        // prepare the expected string        
        Blob headerValue = Blob.valueOf(cc.ccSettings.SAPDispBasicAuthUsername__c + ':' + cc.ccSettings.SAPDispBasicAuthPassword__c);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        // now get the value from the method to be tested
        Map<String, String> mapAuth = cc.getBasicAuth();
        String value = mapAuth.get('Authorization');
        
        System.assertEquals(value, authorizationHeader, 'basic authentication string creation failed');
        
        Test.StopTest();
    } 
    
   /*
    * Test some helper functions
    */
    static testMethod void testHelperFunctions()
    {
        CCWSUtil cc = new CCWSUtil();
    
        String gmtstr = '2012-01-11 19:05:45';
        
        DateTime dt = DateTime.newInstanceGmt(2012, 1, 11, 19, 5, 45);
        Date d = dt.date();
        
        String value; 

        Test.StartTest();
        
        //### TODO: check if the time zone implementation is correct!
        value = cc.getFormatedCreationDateTime();

        value = cc.formatDateTime(null);
        value = cc.formatDateTime(dt);  // dt.format('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
        System.AssertEquals(value.startswith('2012-01-11T19:05:45'), true);

        // we expect the GMT date value
        value = cc.formatToDate(null);
        value = cc.formatToDate(dt);
        System.AssertEquals(value, '2012-01-11');

        // we expect the GMT time value
        value = cc.formatToTime(null);
        value = cc.formatToTime(dt);
        System.AssertEquals(value, '19:05:45');
        
        
        //### remove
        value = cc.formatDate(d);       // '' + year + '-' + rect('' + month) + '-' + rect('' + day); 
        //### remove
        value = cc.getFormatedGMTCreationDateTime(); // formatGmt('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
         
        Test.StopTest();
    } 
    
 
   /*
    * Test some futher helper functions
    */
    static testMethod void testUserTests()
    {
        Test.StartTest();

        CCWSUtil cc = new CCWSUtil();
        
        // just for test coverage
        cc.getUser();
        
        // now test some field
        cc.user = new User();
        cc.user.ERPPlannerGroup__c  = 'A';
        cc.user.ERPWorkCenter__c    = 'B';
        cc.user.ERPPurchaseGroup__c = 'C';
        cc.user.ERPPurchaseOrg__c   = 'D';

        String value;
        value = cc.getPlannerGroup('AA');
        System.AssertEquals(value, 'AA');
        
        value = cc.getPlannerGroup(null);
        System.AssertEquals(value, 'A');
        
        value = cc.getWorkCenter();
        System.AssertEquals(value, cc.user.ERPWorkCenter__c);
         
        value = cc.getPurchaseGroup();
        System.AssertEquals(value, cc.user.ERPPurchaseGroup__c);
        
        value = cc.getPurchaseOrg();
        System.AssertEquals(value, cc.user.ERPPurchaseOrg__c); 
        
        Test.StopTest();
    } 

    
}
