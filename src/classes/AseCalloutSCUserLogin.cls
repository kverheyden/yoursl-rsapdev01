/**
 * AseCalloutSCUserLogin.cls   mt  28.09.2009
 * 
 * Copyright (c) 2009 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 /**
 * AseCalloutSCUserLogin represents SCUserLogin
 * The SCUserLogin isn't queried by the ASEWebservice, but 
 * must be send with the set-call  
 * 
 * @author mt
 * @version $Revision$, $Date$
 */
public class AseCalloutSCUserLogin extends AseCalloutSCBaseClass 
{

    /*
     * @see AseCalloutSCBaseClass#getASEType()
     */
    public Integer getASEType()
    {
        return AseCalloutConstants.ASE_TYPE_USERLOGIN;
    }

    /**
     * this method adds the username and userpas as keyValues to the
     * aseDataTypes. 
     * FIXME: the login is hardcoded!
     * 
     * @param dTypes the array with the previous datatypes
     * @param parameter this parameter is used in this case, but must 
     *        be there to fulfill the interface 
     */
    public override void addSetAseDataTypes(AseService.aseDataType[] dTypes, String[] parameter)
    {        
        //prepare dataEntry
        AseService.aseDataEntry[] dEntries = new AseService.aseDataEntry[0];

        AseService.aseDataEntry dEntry = new AseService.aseDataEntry();

        //TODO - FIXME: hardcoded and plaintext userlogin!
        AseCalloutUtils.addStringsAsKeyVal2Entry('username', 'comuser2@test-gms.de', dEntry);
        AseCalloutUtils.addStringsAsKeyVal2Entry('userpass', 'QVs0DXPtc1sFhb8', dEntry);
        dEntries.add(dEntry);

        AseService.aseDataType tmpDataType = new AseService.aseDataType();
        tmpDataType.dataEntries = dEntries;
        tmpDataType.type_x = getAseTypeStr();
        dTypes.add(tmpDataType);
    }    
 
}
