/*
 * @(#)SCMaterialRequestExtensionTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 * <p>
 * This class contains unit tests for material request.
 * <p>
 * A complete testing environment is created first. Remember that all data
 * created in a Unit test is not commited to the database!!
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */

@isTest
private class SCMaterialRequestExtensionTest
{
    /**
     * testMaterialRequestExtension
     * ============================
     *
     * Test the manual stock correction with the extension class.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    static testMethod void testMaterialRequestExtension()
    {
        System.debug('########################################################');
        System.debug('#### Starting Test:  testMaterialRequestExtension()');
        System.debug('########################################################');

        // create all neccessary datas
        Id plantId = SCHelperTestClass4.createTestPlant();
        List<SCStock__c> stocks = SCHelperTestClass4.createTestStocks(plantId);
        List<SCStock__c> originStocks = new List<SCStock__c>();
        originStocks.add(stocks.get(0));
        originStocks.add(stocks.get(1));
        List<SCStockOrigin__c> origins = SCHelperTestClass4.createTestStockOrigins(originStocks);
        List<SCArticle__c> articleList = SCHelperTestClass4.createTestArticles();
        List<User> users = SCHelperTestClass4.createTestUsers();
        List<SCResource__c> resources = SCHelperTestClass4.createTestResources(users);
        Id calId = SCHelperTestClass4.createTestCalendar();
        Id unitId = SCHelperTestClass4.createTestBusinessUnit(stocks.get(2).Id, calId);
        List<SCStock__c> assignStocks = new List<SCStock__c>();
        assignStocks.add(stocks.get(3));
        assignStocks.add(stocks.get(4));
        List<SCResourceAssignment__c> assignments = 
                        SCHelperTestClass4.createTestAssignments(resources, 
                                                               assignStocks, 
                                                               unitId);
        
        Test.startTest();
        
        SCMaterialRequestExtension matRequestExt = 
                     new SCMaterialRequestExtension(new ApexPages.StandardController(stocks.get(3)));
        
        List<SelectOption> typeList = matRequestExt.getTypeList();
        
        // set an article manual
        matRequestExt.stockItem.Article__c = articleList.get(0).Id;
        // get the current quantity
        matRequestExt.initQuantity();
        // validate the quantity
        System.assert(matRequestExt.curQty == 0);
        
        // set the request date to yesterday
        matRequestExt.assignment.ValidFrom__c = Date.today().addDays(-1);
        // refill the list again
        matRequestExt.refillLists();
        // validate the request date
        System.assert(matRequestExt.assignment.ValidFrom__c == Date.today());
        
        // set a new quantity and type for correction
        matRequestExt.qty = 2;
        matRequestExt.matMoveType = SCfwConstants.DOMVAL_MATMOVETYP_MATREQUEST_EMPL;
        // correct the stock
        matRequestExt.requestMat();
        // validate the result
        System.assert(matRequestExt.requestOk);

        Test.stopTest();
    } // testMaterialRequestExtension
} // SCMaterialRequestExtensionTest
