/*
 * @(#)SCStockProfileExtensionTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 * <p>
 * This class contains unit tests for importing articles to stock profiles.
 * A detailed test for managing assocs will be done in 
 * SCTestMatMoveBaseExtention.cls
 * <p>
 * A complete testing environment is created first. Remember that all data
 * created in a Unit test is not commited to the database!!
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */

@isTest
private class SCStockProfileExtensionTest
{
    /**
     * testStockProfileExtension
     * =========================
     *
     * Test the extension class for importing articles to stock profiles.
     *
     * - Test 1: importing a valid article, but no quantity
     * - Test 2: importing valid articles
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    static testMethod void testStockProfileExtension()
    {
        System.debug('########################################################');
        System.debug('#### Starting Test:  testStockProfileExtension()');
        System.debug('########################################################');

        SCfwDomain domSysParam = new SCfwDomain('DOM_SYSPARAM');
        SCfwDomainValue domVal = domSysParam.getDomainValue('1400159', true);
        Integer sysparamStockV8 = -1;
        String paramV8 = domVal.getControlParameter('V8');
        if ((null != paramV8) && (paramV8.length() > 0))
        {
            sysparamStockV8 = Integer.valueOf(paramV8);
        } // if ((null != paramV8) && (paramV8.length() > 0))

        // create all neccessary datas
        Id plantId = SCHelperTestClass4.createTestPlant();
        List<SCStock__c> stocks = SCHelperTestClass4.createTestStocks(plantId);
        List<SCArticle__c> articleList = SCHelperTestClass4.createTestArticles();
        SCHelperTestClass4.createTestStockItems(articleList, stocks.get(0).ID, stocks.get(1).Id);
        List<SCStockProfile__c> profiles = SCHelperTestClass4.createTestStockProfiles();
        SCHelperTestClass4.createTestStockProfileItems(articleList, profiles.get(0).Id);
        
        Test.startTest();
        
        SCStockProfileExtension stockProfileExt = 
                     new SCStockProfileExtension(new ApexPages.StandardController(profiles.get(0)));
        
        // Test 1: importing a valid article, but no quantity
        stockProfileExt.input = SCStockItem__c.Article__c.getDescribe().getLabel();
        stockProfileExt.input += '\t' + SCStockItem__c.MinQty__c.getDescribe().getLabel();
        stockProfileExt.input += '\t' + SCStockItem__c.MaxQty__c.getDescribe().getLabel();
        stockProfileExt.input += '\t' + SCStockItem__c.ReplenishmentQty__c.getDescribe().getLabel();
        stockProfileExt.input += '\n';
        stockProfileExt.input += articleList.get(0).name;
        
        stockProfileExt.checkItems();
        // we expect an error
        System.assert(!stockProfileExt.checked);
        
        
        // Test 2: importing valid articles
        stockProfileExt.input = SCStockItem__c.Article__c.getDescribe().getLabel();
        stockProfileExt.input += '\t' + SCStockItem__c.MinQty__c.getDescribe().getLabel();
        stockProfileExt.input += '\t' + SCStockItem__c.MaxQty__c.getDescribe().getLabel();
        stockProfileExt.input += '\t' + SCStockItem__c.ReplenishmentQty__c.getDescribe().getLabel();
        stockProfileExt.input += '\n';
        stockProfileExt.input += articleList.get(0).name + '\t1\t2\t1\n';
        stockProfileExt.input += articleList.get(2).name + '\t1\t2\t1\n';
        stockProfileExt.input += articleList.get(3).name + '\t1\t2\n';
        
        stockProfileExt.checkItems();
        // we expect no error
        System.assert(stockProfileExt.checked);
        
        stockProfileExt.importItems();
        // we expect no error
        System.assert(stockProfileExt.importOk);

        Test.stopTest();
    } // testStockProfileExtension
} // SCStockProfileExtensionTest
