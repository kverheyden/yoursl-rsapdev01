// PoC Trigger-Framework
public virtual class TriggerDispatcherBase implements ITriggerDispatcher {
	
	private static ITriggerHandler beforeInserthandler;
	private static ITriggerHandler beforeUpdatehandler;
	private static ITriggerHandler beforeDeleteHandler;
	private static ITriggerHandler afterInserthandler;
	private static ITriggerHandler afterUpdatehandler;
	private static ITriggerHandler afterDeleteHandler;
	private static ITriggerHandler afterUndeleteHandler;

	public virtual void beforeInsert(TriggerParameters tp) {
		
	}
	
	public virtual void beforeUpdate(TriggerParameters tp) {
		
	}
	
	public virtual void beforeDelete(TriggerParameters tp) {
		
	}
	
	public virtual void afterInsert(TriggerParameters tp) {
		
	}
	
	public virtual void afterUpdate(TriggerParameters tp) {
		
	}
	
	public virtual void afterDelete(TriggerParameters tp) {
		
	}
	
	public virtual void afterUnDelete(TriggerParameters tp) {
		
	}

	public virtual void executed() {
		
	}
	
	protected void execute(ITriggerHandler handler, TriggerParameters triggerParams, TriggerParameters.TriggerEvent event) {
    	if(handler != null) {
			
    		if(event == TriggerParameters.TriggerEvent.beforeInsert)
    			beforeInsertHandler = handler;
    		
			if(event == TriggerParameters.TriggerEvent.beforeUpdate)
    			beforeUpdateHandler = handler;
    		
			if(event == TriggerParameters.TriggerEvent.beforeDelete)
    			beforeDeleteHandler = handler;
    		
			if(event == TriggerParameters.TriggerEvent.afterInsert)
    			afterInsertHandler = handler;
    		
			if(event == TriggerParameters.TriggerEvent.afterUpdate)
    			afterUpdateHandler = handler;
    		
			if(event == TriggerParameters.TriggerEvent.afterDelete)
    			afterDeleteHandler = handler;
    		
			if(event == TriggerParameters.TriggerEvent.afterUnDelete)
    			afterUndeleteHandler = handler;
    		
			handler.mainEntry(triggerParams);
    	}
    	else {
    		
			if(event == TriggerParameters.TriggerEvent.beforeInsert)
    			beforeInsertHandler.inProgressEntry(triggerParams);
    		
			if(event == TriggerParameters.TriggerEvent.beforeUpdate)
    			beforeUpdateHandler.inProgressEntry(triggerParams);
    		
			if(event == TriggerParameters.TriggerEvent.beforeDelete)
    			beforeDeleteHandler.inProgressEntry(triggerParams);
    		
			if(event == TriggerParameters.TriggerEvent.afterInsert)
    			afterInsertHandler.inProgressEntry(triggerParams);
    		
			if(event == TriggerParameters.TriggerEvent.afterUpdate)
    			afterUpdateHandler.inProgressEntry(triggerParams);
    		
			if(event == TriggerParameters.TriggerEvent.afterDelete)
    			afterDeleteHandler.inProgressEntry(triggerParams);
    		
			if(event == TriggerParameters.TriggerEvent.afterUnDelete)
    			afterUndeleteHandler.inProgressEntry(triggerParams);
    	}
    }
	
	protected void execute(TriggerParameters triggerParams, TriggerParameters.TriggerEvent event) {
		
	}
}
