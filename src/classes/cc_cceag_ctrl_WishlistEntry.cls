global with sharing class cc_cceag_ctrl_WishlistEntry {
    public cc_cceag_ctrl_WishlistEntry() {
        
    }

    @RemoteAction
    global static Map<String, cc_cceag_bean_Product> getPromotionProducts(String storeName, String portalUserId, List<Id> promoIdList){
        Map<String, cc_cceag_bean_Product> promoProductMap = new Map<String, cc_cceag_bean_Product>();
        List<ccrz__E_Promo__c> promoList = [SELECT Id,
                                                    ccrz__Product__c,
                                                    ccrz__Product__r.Name,
                                                    ccrz__Product__r.ccrz__Sku__c,
                                                    ccrz__Product__r.ccrz__ShortDesc__c
                                            FROM ccrz__E_Promo__c
                                            WHERE Id IN :promoIdList];

        Set<Id> productIdSet = new Set<Id>();
        List<String> skuList = new List<String>();

        for(ccrz__E_Promo__c promo : promoList){
            productIdSet.add(promo.ccrz__Product__c);
            skuList.add(promo.ccrz__Product__r.ccrz__Sku__c);
        }

        List<ccrz__E_ProductMedia__c> pmList = [Select ccrz__FilePath__c, ccrz__ProductMediaSource__c, ccrz__StaticResourceName__c, ccrz__URI__c, ccrz__Product__r.Id from ccrz__E_ProductMedia__c e where ccrz__Product__r.Id IN :productIdSet and ccrz__MediaType__c = 'Product Image'];
        Map<String, String> mediaMap = new Map<String, String>();
        cc_cceag_api_ProductQuantityRule qRule = new cc_cceag_api_ProductQuantityRule();
        Map<String, Map<String,Object>> quantityRuleMap = qRule.getRules(skuList);

        for (ccrz__E_ProductMedia__c media: pmList)
            mediaMap.put(media.ccrz__Product__r.Id, media.ccrz__URI__c);

        for(ccrz__E_Promo__c promo : promoList){
            cc_cceag_bean_Product prodBean = new cc_cceag_bean_Product();
            prodBean.id = promo.ccrz__Product__c;
            prodBean.sku = promo.ccrz__Product__r.ccrz__Sku__c;
            prodBean.name = promo.ccrz__Product__r.Name;
            prodBean.shortDesc = promo.ccrz__Product__r.ccrz__ShortDesc__c;
            prodBean.imageURL = mediaMap.get(promo.ccrz__Product__c);
            prodBean.quantityRules = quantityRuleMap.get(promo.ccrz__Product__r.ccrz__Sku__c);
            promoProductMap.put(promo.Id, prodBean);
        }

        return promoProductMap;
    }
/*
    @RemoteAction
    global static ccrz.cc_RemoteActionResult createDefaultTemplate(ccrz.cc_RemoteActionContext context){
        ccrz.cc_CallContext.initRemoteContext(context);
        ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
        result.inputContext = context;
        result.success = false; 
        ccrz__E_Cart__c cart = cc_cceag_ctrl_WishlistEntry.createDefaulTemplate(context.portalUserId, context.storefront);
        ccrz.cc_bean_CartSummary data = new ccrz.cc_bean_CartSummary(cart);
        result.data = data;
        return result;
    }
    
    public static ccrz__E_Cart__c createDefaulTemplate(string ownerId, string storeName) {
        List<ccrz__E_Order__c> orderList =
            [select 
                Id,
                (select Name, ccrz__Quantity__c, ccrz__Product__r.Id,ccrz__Product__r.ccrz__SKU__c
                 from ccrz__E_OrderItems__r)
            from ccrz__E_Order__c
            where ownerId = :ownerId limit 6];
        map<string, decimal> idQtymap = new map<string, decimal>();
        map<string, ccrz__E_OrderItem__c> idOrdItmMap = new map<string, ccrz__E_OrderItem__c>();
        for(ccrz__E_Order__c ord : orderList) {
            for(ccrz__E_OrderItem__c oi : ord.ccrz__E_OrderItems__r) {
                if(!idQtymap.containsKey(oi.ccrz__Product__r.Id)) {
                    idQtymap.put(oi.ccrz__Product__r.Id, 0);
                }
                idQtymap.put(oi.ccrz__Product__r.Id, idQtymap.get(oi.ccrz__Product__r.Id) + oi.ccrz__Quantity__c);
                idOrdItmMap.put(oi.ccrz__Product__r.Id, oi);
            }
        }
        User userData = cc_cceag_dao_Account.fetchUser(ownerId);
        ccrz__E_Cart__c cartHeader             = new ccrz__E_Cart__c();
        cartHeader.ccrz__CartType__c           = 'WishList';
        cartHeader.ccrz__CartStatus__c         = 'Open';
        cartHeader.ccrz__User__c               = ownerId;
        cartHeader.ccrz__Contact__c            = userData.ContactId;
        cartHeader.ccrz__Name__c               = 'Default WishList';
        cartHeader.OwnerId                      = ownerId;
        cartHeader.ccrz__AnonymousID__c        = false;
        cartHeader.ccrz__ActiveCart__c         = false;
        cartHeader.ccrz__Storefront__c         = storeName;
        cartHeader.ccrz__SessionID__c = UserInfo.getSessionId();
        cartHeader.ccrz__BuyerFirstName__c = userData.Contact.FirstName;
        cartHeader.ccrz__BuyerLastName__c = userData.Contact.LastName;
        update cartHeader;
        
        List<ccrz__E_CartItem__c> cartItems = new List<ccrz__E_CartItem__c> ();
        for(string key : idQtymap.keyset()) {
            cartItems.add(new ccrz__E_CartItem__c(ccrz__Quantity__c=idQtymap.get(key), ccrz__product__c=key, ccrz__cart__c=cartHeader.id, ccrz__price__c=idOrdItmMap.get(key).ccrz__price__c, ccrz__SubAmount__c=idQtymap.get(key)*idOrdItmMap.get(key).ccrz__price__c));
        }
        insert cartItems;
        
        return cc_cceag_dao_Cart.retrieveCart(cartHeader.id);
    }
*/


}
