global without sharing class cc_cceag_ctrl_StatusBar {
    

    /**
     *  StatusBarModel - Remote Action that gets all the model data for the status bar
     *  @param String portalUserId - user id of the current user in context
     *  @param String cartId - Encrypted value of current cart id
     *  @return cc_cceag_ctrl_StatusBar.StatusBarModel - Model bean
     */
    @RemoteAction
    global static StatusBarModel showStatusBar(String portalUserId, String cartId){
        String userId = (portalUserId == null || portalUserId.trim().length() == 0) ? UserInfo.getUserId() : portalUserId;
        StatusBarModel model = new StatusBarModel();
        try{
            //Get user/contact information
            ccrz__E_Cart__c cart = [
                select
                     Id
                    ,ccrz__ShipTo__r.ccrz__Partner_Id__c
                from
                    ccrz__E_Cart__c
                where
                    ccrz__EncryptedId__c = :cartId
            ];
            String shiptoId = cart.ccrz__ShipTo__r.ccrz__Partner_Id__c;
            Account shipTo = [SELECT StaggeredRebate__c, MinimumOrderQuantityInZFK__c FROM Account WHERE AccountNumber =: shiptoId LIMIT 1];
            
            //Set the cart and showBar boolean values
            model.showBar = shipTo.StaggeredRebate__c;
            model.zfkMiamfoac = shipTo.MinimumOrderQuantityInZFK__c;
            if (model.zfkMiamfoac == null)
                model.zfkMiamfoac = 10;
            model.activeCart =  cc_cceag_dao_Cart.retrieveCartByEncryptedId(cartId, true);
            if (model.activeCart != null && model.activeCart.ccrz__RequestDate__c != null) {
                DateTime dt = DateTime.newInstance(model.activeCart.ccrz__RequestDate__c.year(), model.activeCart.ccrz__RequestDate__c.month(), model.activeCart.ccrz__RequestDate__c.day());
                model.dispReqDate = dt.format('dd M yyyy');
            }

            //Calculate rebate
            Map<Id, ccrz__E_CartItem__c> cartItemMap = new Map<Id, ccrz__E_CartItem__c>();
            Set<Id> productIdSet = new Set<Id>();
            for(ccrz__E_CartItem__c item : model.activeCart.ccrz__E_CartItems__r){
                cartItemMap.put(item.Id, item);
                productIdSet.add(item.ccrz__Product__c);
            }

            Map<Id, ccrz__E_Product__c> productMap = new Map<Id, ccrz__E_Product__c>(
                [
                    SELECT Id, 
                        (SELECT ccrz__SpecValue__c FROM ccrz__Product_Specs__r WHERE ccrz__Spec__r.Name = 'iZFKPerSalesUnit' LIMIT 1)
                    FROM ccrz__E_Product__c
                    WHERE Id IN :productIdSet
                ]
            );

            //Determine zfkValue aggregate of all products in cart
            Map<String, Decimal> zfkMap = new Map<String, Decimal>();
            Decimal zfkTotal = 0;
            for(ccrz__E_CartItem__c item : model.activeCart.ccrz__E_CartItems__r){
                Decimal zfkValue = 0.0;
                ccrz__E_Product__c currentProduct = productMap.get(item.ccrz__Product__c);
                if(currentProduct != null && currentProduct.ccrz__Product_Specs__r != null && currentProduct.ccrz__Product_Specs__r.size() > 0) {
                    zfkValue = Decimal.valueOf(currentProduct.ccrz__Product_Specs__r.get(0).ccrz__SpecValue__c);
                }
                zfkTotal += item.ccrz__Quantity__c * zfkValue;
                zfkMap.put(item.ccrz__Product__c, zfkValue);
            }
            model.zfkMap = zfkMap;
            model.zfkTotal = zfkTotal;
            List<Integer> levelList = new List<Integer>{0, 10, 12, 16, 24};
            //level 1 is between MinimumOrderQuantityInZFK__c and 12
            //level 2 is between 12 and 15
            //level 3 is between 16 and 23
            //level 4 is between >24 and 1 pallet
            
            integer zfkMiamfoacLevel = 1;
            levelList[zfkMiamfoacLevel] = integer.valueOf(model.zfkMiamfoac);
            
            model.discountLevel = 0;
            model.quantityRemaining = null;
            for (Integer i = 0; i <levelList.size(); i++) {
                Integer level = levelList.get(i);
                if (zfkTotal >= level) {
                    model.discountLevel = i;
                }
                else {
                    model.quantityRemaining = (level - zfkTotal).round(System.RoundingMode.HALF_UP);
                    break;
                }
            }

        }catch(QueryException e){
            System.debug(LoggingLevel.ERROR, e.getMessage());
        }
        return model;
    }

    /**
     *  getUniqueCartCount - retrieves the number of unique sku's in the specified cart
     *  @param String cartId - encrypted cart id to retrieve
     *  @return Integer - total number of unique skus in cart
     */
    @RemoteAction
    global static Integer getUniqueCartCount(String cartId){
        Integer size = 0;
        if(cartId != null && cartId.trim().length() != 0){
            ccrz__E_Cart__c activeCart = cc_cceag_dao_Cart.retrieveCartByEncryptedId(cartId, true);
            size = activeCart.ccrz__E_CartItems__r.size();
        }

        return size;
    }

    global class StatusBarModel{
        public Boolean showBar{get;set;}
        public ccrz__E_Cart__c activeCart{get;set;}
        public Integer discountLevel{get;set;}
        public Long quantityRemaining{get;set;}
        public Map<String, Decimal> zfkMap { get; set; }
        public Decimal zfkMiamfoac { get; set; }
        public Decimal zfkTotal { get; set; }
        public String dispReqDate { get; set; }
    }

}
