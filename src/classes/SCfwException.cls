/*
 * @(#)SCfwException.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
public virtual class SCfwException extends Exception
{
    private Boolean logicError = false;
    public SCfwException(Boolean log, String message)
    {
        System.debug(Logginglevel.ERROR, message);
        this.setMessage(message);
    }

    public SCfwException(Integer key)
    {
        this.setMessage(fwExceptionTexts.getExceptionText(key));
    }

    public SCfwException(Integer key, String param)
    {
        this.setMessage(fwExceptionTexts.getExceptionText(key, param));
    }

    public SCfwException(Integer key, String param1, String param2)
    {
        this.setMessage(fwExceptionTexts.getExceptionText(key, param1, param2));
    }

    public SCfwException(Integer key, String param1, String param2, String param3)
    {
        this.setMessage(fwExceptionTexts.getExceptionText(key, param1, param2, param3));
    }

    public SCfwException(String message, Boolean logic)
    {
        this.setMessage(message);
        this.logicError = logic;
    }
    
    public Boolean isLogic()
    {
        return logicError;
    }
    /**
     * This class contains all possible exceptions that can occure.
     * 
     * @author Thorsten Klein <tklein@gms-online.de>
     * @version $Revision$, $Date$
     */
    public class SCfwExceptionTexts
    {
        private Map<Integer, String> exceptionMap;
        
        /**
         * The constructor create the intern map with all exceptions.
         * 
         * @author Thorsten Klein <tklein@gms-online.de>
         * @version $Revision$, $Date$
         */
        public SCfwExceptionTexts()
        {
            exceptionMap = new Map<Integer, String>();
            
            // #########  general exceptions
            exceptionMap.put(1000, 'General error!');
            exceptionMap.put(1001, 'An input object is missing!');
            exceptionMap.put(1002, 'Price list could not be determined!');
            
            // #########  web service exceptions
            // exception because of missing or wrong context parameters
            exceptionMap.put(1100, 'Account Id in context object is missing!');
            exceptionMap.put(1101, 'Language in context object is missing!');
            exceptionMap.put(1102, 'Country in context object is missing!');
            exceptionMap.put(1103, 'Brand Id in context object is missing!');
            exceptionMap.put(1104, 'Endpoint of Web Service %1 is missing in Application Settings %2!');
            exceptionMap.put(1105, 'User of Web Service %1 is missing in Application Settings %2!');           
            exceptionMap.put(1106, 'Password of Web Service %1 is missing in Application Settings %2!'); 
            exceptionMap.put(1107, 'Creating a sequence %1 for Web Service % failed!'); 
            exceptionMap.put(1108, 'Id for account record type in context object is missing!');
            
            // exception because of wrong parameter mode
            exceptionMap.put(1200, 'Invalid or unknown mode [%1], expected values: %2');
            
            // other exceptions
            exceptionMap.put(1300, 'Address validation: country is required, expected DE, NL, GB, ....!');
            exceptionMap.put(1301, 'Input value [installed base id] is not set!');
            exceptionMap.put(1302, 'Installed base with id [%1] could not be read: ');
            exceptionMap.put(1303, 'Error reading owner of installed base with id [%1]: ');
            exceptionMap.put(1304, 'Input value [serial number] is not set!');
            exceptionMap.put(1305, 'Installed base with serial number [%1] could not be read: ');
            exceptionMap.put(1306, 'Error reading owner of installed base with serial number [%1]: ');
            exceptionMap.put(1307, 'Input value [order id] is not set!');
            exceptionMap.put(1308, 'Order with id [%1] could not be read: ');
            exceptionMap.put(1309, 'The installed base role of type [owner] could not be found for the location of the installed base with the id [%1]');
            exceptionMap.put(1310, 'Please provide a valid assignment id');
            exceptionMap.put(1311, 'No order could be found for the following assignment id: [%1]');
            exceptionMap.put(1312, 'The requested status for the following order id [%1] could not be set and should be [%2]');
            exceptionMap.put(1313, 'The provided order line with the id [%1] could not be updated as it does not exist for the order item with the id [%2]');
            exceptionMap.put(1314, 'Input value [order item id] is not set!');
            exceptionMap.put(1315, 'Order item with id [%1] could not be read: ');
            exceptionMap.put(1316, 'The provided order item with the id [%1] could not be updated as it does not exist for the order with the id [%2]');
            exceptionMap.put(1317, 'The provided assignment with the id [%1] could not be updated as it does not exist for the order with the id [%2]');
            exceptionMap.put(1318, 'Input value [article number] is not set!');
            exceptionMap.put(1319, 'Input value [resource name] is not set!');
            exceptionMap.put(1320, 'Input value [report start] is not set!');
            exceptionMap.put(1321, 'Input value [report end] is not set!');
            exceptionMap.put(1322, 'Input value [report start] must be before [report end]!');
            exceptionMap.put(1323, 'No safety check data are set!');
            exceptionMap.put(1324, 'Input value [import date] is not set!');
            exceptionMap.put(1325, 'The provided assignment with the id [%1] could not be updated because of its status [%2]');
            exceptionMap.put(1326, 'The product model with the id [%1] could not be read: ');
            exceptionMap.put(1327, 'The brand model with the id [%1] could not be read: ');
            exceptionMap.put(1328, 'The default brand model with the id2 [%1] could not be read: ');
            exceptionMap.put(1329, 'The resource assignment for the engineer [%1] and the date from [%2] to [%3] could not be read: ');
            exceptionMap.put(1330, 'The value [%1] is not valid for the picklist [%2]!');
            
            // #########  validation exceptions
            // order validation
            exceptionMap.put(2000, 'No role of type [service recipient] is defined!');
            exceptionMap.put(2001, 'No order item is defined!');
            exceptionMap.put(2002, 'Priority is not set!');
            exceptionMap.put(2003, 'Failure type is not set!');
            exceptionMap.put(2004, 'Customer preference start date is not set!');
            exceptionMap.put(2005, 'Customer preference end date is not set!');
            exceptionMap.put(2006, 'Customer preference start date must be today or in the future!');
            exceptionMap.put(2007, 'Customer preference end date must be today or in the future!');
            exceptionMap.put(2008, 'Customer preference end date must be equal or after start date!');
            exceptionMap.put(2009, 'Customer timewindow is not set!');
            exceptionMap.put(2010, 'No role of type [invoice recipient] is defined!');
            exceptionMap.put(2011, 'Department (current) is not set!');

            // order role validation
            exceptionMap.put(2100, 'Field [order role] is not set!');
            exceptionMap.put(2101, 'Not enough order role data are set! Set an account id or name1, street, postalcode, city and country!');
            exceptionMap.put(2102, 'Street is not set!');
            exceptionMap.put(2103, 'Houseno is not set!');
            exceptionMap.put(2104, 'Postalcode is not set!');
            exceptionMap.put(2105, 'City is not set!');
            exceptionMap.put(2106, 'Country is not set!');
            exceptionMap.put(2107, 'X coordinate for address is not set!');
            exceptionMap.put(2108, 'Y coordinate for address is not set!');
            exceptionMap.put(2109, 'Not enough order role data are set! Set an order role id or an account id and a role type!');
            exceptionMap.put(2110, 'Order [%1] has no or more than one role of the type [service recipient]! ');

            // order item validation
            exceptionMap.put(2200, 'Duration is not set!');
            exceptionMap.put(2201, 'Duration unit is not set!');
            exceptionMap.put(2202, 'Completion status is not set!');
            exceptionMap.put(2203, 'Error symptom1 is not set!');
            exceptionMap.put(2204, 'Error symptom2 is not set!');
            exceptionMap.put(2205, 'Error condition1 is not set!');

            // installed base validation
            exceptionMap.put(2300, 'Not enough installed base data are set! Set an installed base id or a product model or unitclass and unittype or product skill!');
            exceptionMap.put(2301, 'Status is not set!');
            exceptionMap.put(2302, 'Type is not set!');
            exceptionMap.put(2303, 'Brand is not set!');
            exceptionMap.put(2304, 'The installation date lies too far in the past. Minimum possible date is: ');
            exceptionMap.put(2305, 'The installation date lies too far in the future. Maximum possible date is: ');

            // installed base location validation
            exceptionMap.put(2400, 'Not enough installed base location data are set! Set an installed base location id or a location name!');
            exceptionMap.put(2401, 'Street is not set!');
            exceptionMap.put(2402, 'Houseno is not set!');
            exceptionMap.put(2403, 'Postalcode is not set!');
            exceptionMap.put(2404, 'City is not set!');
            exceptionMap.put(2405, 'Country is not set!');
            exceptionMap.put(2406, 'X coordinate for location address is not set!');
            exceptionMap.put(2407, 'Y coordinate for location address is not set!');

            // order line validation
            exceptionMap.put(2500, 'Reference to order is not set!');
            exceptionMap.put(2501, 'Article is not set!');
            exceptionMap.put(2502, 'Quantity is not set!');
            exceptionMap.put(2503, 'Type is not set!');
            exceptionMap.put(2504, 'Material status is not set!');
            exceptionMap.put(2505, 'Price type is not set!');
            exceptionMap.put(2506, 'Position price is not set!');
            exceptionMap.put(2507, 'Price is not set!');
            exceptionMap.put(2508, 'Unit price is not set!');
            exceptionMap.put(2509, 'Unit net price is not set!');
            exceptionMap.put(2510, 'Tax is not set!');
            exceptionMap.put(2511, 'Tax rate is not set!');
            exceptionMap.put(2512, 'Invoicing type is not set!');
            exceptionMap.put(2513, 'Invoicing subtype is not set!');
            exceptionMap.put(2514, 'Contract related is not set!');
            exceptionMap.put(2515, 'Article with number [%1] could not be read: ');
            exceptionMap.put(2516, 'List price is not set!');

            // assignment/appointment validation
            exceptionMap.put(2600, 'Status is not set!');
            exceptionMap.put(2601, 'Class is not set!');
            exceptionMap.put(2602, 'Type is not set!');
            exceptionMap.put(2603, 'Planning type is not set!');
            exceptionMap.put(2604, 'Start is not set!');
            exceptionMap.put(2605, 'End is not set!');
            exceptionMap.put(2606, 'Input value [start] must be before [end]!');
            exceptionMap.put(2607, 'Input value [cust pref start] must be before [cust pref end]!');
            exceptionMap.put(2608, 'Cust time window is not set!');
            exceptionMap.put(2609, 'Resource with name [%1] could not be read: ');
            exceptionMap.put(2610, 'Material booking for resource [%1] not possible! There are missing data. Please check following items: - Is a stock assigned to the engineer? - Is a stock assigned to all departments?');
        } // SCfwExceptionTexts
        
        /**
         * Method for getting an exception text by the number.
         * 
         * @author Thorsten Klein <tklein@gms-online.de>
         * @version $Revision$, $Date$
         */
        public String getExceptionText(Integer key)
        {
            String exceptionText = 'E' + String.valueOf(key) + ' - ';
            if (exceptionMap.containsKey(key))
            {
                exceptionText += exceptionMap.get(key);
            } // if (exceptionMap.containsKey(key))
            else
            {
                exceptionText += 'Unknown error!';
            } // else [if (exceptionMap.containsKey(key))]
            return exceptionText;
        } // getExceptionText
        
        /**
         * Method for getting an exception text by the number.
         * It also replaces one parameter.
         * 
         * @author Thorsten Klein <tklein@gms-online.de>
         * @version $Revision$, $Date$
         */
        public String getExceptionText(Integer key, String param)
        {
            String exceptionText = getExceptionText(key);
            if (exceptionText.length() > 0)
            {
                exceptionText = exceptionText.replace('%1', param);
            } // if (exceptionText.length() > 0)
            return exceptionText;
        } // getExceptionText
        
        /**
         * Method for getting an exception text by the number.
         * It also replaces two parameters.
         * 
         * @author Thorsten Klein <tklein@gms-online.de>
         * @version $Revision$, $Date$
         */
        public String getExceptionText(Integer key, String param1, String param2)
        {
            String exceptionText = getExceptionText(key);
            if (exceptionText.length() > 0)
            {
                exceptionText = exceptionText.replace('%1', param1);
                exceptionText = exceptionText.replace('%2', param2);
            } // if (exceptionText.length() > 0)
            return exceptionText;
        } // getExceptionText
        
        /**
         * Method for getting an exception text by the number.
         * It also replaces three parameters.
         * 
         * @author Thorsten Klein <tklein@gms-online.de>
         * @version $Revision$, $Date$
         */
        public String getExceptionText(Integer key, String param1, String param2, String param3)
        {
            String exceptionText = getExceptionText(key);
            if (exceptionText.length() > 0)
            {
                exceptionText = exceptionText.replace('%1', param1);
                exceptionText = exceptionText.replace('%2', param2);
                exceptionText = exceptionText.replace('%3', param3);
            } // if (exceptionText.length() > 0)
            return exceptionText;
        } // getExceptionText
    } // class SCfwExceptionTexts
    
    // static class object, which can be use in the web service to get an exception text
    public static SCfwExceptionTexts fwExceptionTexts = new SCfwExceptionTexts();
 
    /**
     * Gets all information about an exception.
     *
     * @return a string in the form: Line:x, Type: y, Message: z, stack: s 
     */
    public static String getExceptionInfo(Exception e)
    {
        String ret = '';
        if(e != null)
        {
            Integer line = e.getLineNumber();
            String msg = e.getMessage();
            String stack = e.getStackTraceString();
            String typeName = e.getTypeName();
            Exception cause = e.getCause();
            if(cause != null)
            {
                ret = '\nException info. ' + '\nType: ' + typeName + ',\nMessage: ' + msg +  ',\nLine: ' + line +  ',\nStack:\n' + stack + '\n';
                ret += '\nCause: ' + getExceptionInfo(cause);
            }
            else
            {
                ret = '\nException info. ' + '\nType: ' + typeName + ',\nMessage: ' + msg +  ',\nLine: ' + line +  ',\nStack:\n' + stack + '\n';
            }    
        }
        return ret;
    } 

    /**
     * Gets all information about an exception.
     *
     * @return a string in the form: Line:x, Type: y, Message: z, stack: s 
     */
    public static String getExceptionInfo(UnexpectedException e)
    {
        String ret = '';
        if(e != null)
        {
            Integer line = e.getLineNumber();
            String msg = e.getMessage();
            String stack = e.getStackTraceString();
            String typeName = e.getTypeName();
            Exception cause = e.getCause();
            if(cause != null)
            {
                ret = '\nException info. ' + '\nType: ' + typeName + ',\nMessage: ' + msg +  ',\nLine: ' + line +  ',\nStack:\n' + stack + '\n';
                ret += '\nCause: ' + getExceptionInfo(cause);
            }
            else
            {
                ret = '\nException info. ' + '\nType: ' + typeName + ',\nMessage: ' + msg +  ',\nLine: ' + line +  ',\nStack:\n' + stack + '\n';
            }    
        }
        return ret;
    }   
}
