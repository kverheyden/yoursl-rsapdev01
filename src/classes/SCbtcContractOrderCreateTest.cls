/*
 * @(#)SCbtcContractVisitCreateTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCbtcContractOrderCreateTest
{
    static testMethod void testOrderCreatePositive1()
    {
        Test.StartTest();
        SCContractTestHelper cth = new SCContractTestHelper();
        String contractID = cth.prepareForBtcContractVisitCreateTest();
        System.debug('###Test contract: ' + cth.contract);
        System.debug('###ContractId: ' + contractID);
        SCbtcContractVisitCreate.syncCreateContractVisits(contractID);
        
        // Order Creation
        SCContract__c c = [Select Name, Id from SCContract__c where Id = :contractID];
        List<SCContractVisit__c> visitList = [Select Id from SCContractVisit__c where Contract__c = :contractID];
        List<String> visitIdList = new List<String>();
        for(SCContractVisit__c visit: visitList)
        {
            visitIdList.add(visit.Id);
            // create only one order
            break;
        } 
        
        String artNo = SCbtcContractOrderCreate.appSettings.DEFAULT_MAINTENANCE_ARTICLE_NO__c;
        SCbtcContractOrderCreate.appSettings.DEFAULT_MAINTENANCE_ARTICLE_NO__c = '899999';
        update SCbtcContractOrderCreate.appSettings;
        
        SCbtcContractOrderCreate.canCreateOrders(visitIdList);
        SCbtcContractOrderCreate.syncCreateOrders(visitIdList);

        SCbtcContractOrderCreate.createContractOrderAll('NL', 1, 'test');
        SCbtcContractOrderCreate.createContractOrder(c.Id, 'test');
        
        SCbtcContractOrderCreate.appSettings.DEFAULT_MAINTENANCE_ARTICLE_NO__c = artNo;
        update SCbtcContractOrderCreate.appSettings;
        Test.StopTest();
    } // testOrderCreatePositive1

    static testMethod void testOrderCreatePositive2()
    {
        Test.StartTest();
        SCContractTestHelper cth = new SCContractTestHelper();
        String contractID = cth.prepareForBtcContractVisitCreateTest();
        System.debug('###Test contract: ' + cth.contract);
        System.debug('###ContractId: ' + contractID);
        SCbtcContractVisitCreate.syncCreateContractVisits(contractID);
        
        // Order Creation
        SCContract__c c = [Select Name, Id from SCContract__c where Id = :contractID];
        List<SCContractVisit__c> visitList = [Select Id from SCContractVisit__c where Contract__c = :contractID];
        List<String> visitIdList = new List<String>();
        for(SCContractVisit__c visit: visitList)
        {
            visitIdList.add(visit.Id);
            // create only one order
            break;
        } 

        SCbtcContractOrderCreate.createContractOrder(c.Name, 'test');
        SCbtcContractOrderCreate.asyncCreateContractOrder(visitIdList);
        
        Test.StopTest();
    } // testOrderCreatePositive2

    static testMethod void testBasics()
    {
        SCArticle__c testArt = new SCArticle__c(Text_zh__c = 'Text_zh', 
                                                Text_hr__c = 'Text_hr', 
                                                Text_cs__c = 'Text_cs', 
                                                Text_da__c = 'Text_da', 
                                                Text_nl__c = 'Text_nl',
                                                Text_en__c = 'Text_en', 
                                                Text_fr__c = 'Text_fr', 
                                                Text_de__c = 'Text_de', 
                                                Text_hu__c = 'Text_hu', 
                                                Text_it__c = 'Text_it',
                                                Text_pl__c = 'Text_pl', 
                                                Text_sl__c = 'Text_sl', 
                                                Text_ro__c = 'Text_ro', 
                                                Text_ru__c = 'Text_ru', 
                                                Text_sk__c = 'Text_sk',
                                                Text_es__c = 'Text_es', 
                                                Text_tr__c = 'Text_tr', 
                                                Text_uk__c = 'Text_uk');
        insert testArt;

        Test.StartTest();
        SCContractTestHelper cth = new SCContractTestHelper();
        String contractID = cth.prepareForBtcContractVisitCreateTest();
        System.debug('###Test contract: ' + cth.contract);
        System.debug('###ContractId: ' + contractID);
        SCbtcContractVisitCreate.syncCreateContractVisits(contractID);
        
        // Order Creation
        SCContract__c c = [Select Name, Id from SCContract__c where Id = :contractID];
        List<SCContractVisit__c> visitList = [Select Id from SCContractVisit__c where Contract__c = :contractID];
        List<String> visitIdList = new List<String>();
        for(SCContractVisit__c visit: visitList)
        {
            visitIdList.add(visit.Id);
            // create only one order
            break;
        } 

        SCbtcContractOrderCreate contrOrd = new SCbtcContractOrderCreate(visitIdList, 'test');
        
        SCbtcContractOrderCreate.getArticleDescription('CN', testArt);
        SCbtcContractOrderCreate.getArticleDescription('HR', testArt);
        SCbtcContractOrderCreate.getArticleDescription('CZ', testArt);
        SCbtcContractOrderCreate.getArticleDescription('DK', testArt);
        SCbtcContractOrderCreate.getArticleDescription('NL', testArt);
        SCbtcContractOrderCreate.getArticleDescription('GB', testArt);
        SCbtcContractOrderCreate.getArticleDescription('FR', testArt);
        SCbtcContractOrderCreate.getArticleDescription('DE', testArt);
        SCbtcContractOrderCreate.getArticleDescription('HU', testArt);
        SCbtcContractOrderCreate.getArticleDescription('IT', testArt);
        SCbtcContractOrderCreate.getArticleDescription('PL', testArt);
        SCbtcContractOrderCreate.getArticleDescription('RO', testArt);
        SCbtcContractOrderCreate.getArticleDescription('RU', testArt);
        SCbtcContractOrderCreate.getArticleDescription('SK', testArt);
        SCbtcContractOrderCreate.getArticleDescription('SL', testArt);
        SCbtcContractOrderCreate.getArticleDescription('ES', testArt);
        SCbtcContractOrderCreate.getArticleDescription('TR', testArt);
        SCbtcContractOrderCreate.getArticleDescription('UK', testArt);
        
        Test.StopTest();
    } // testBasics

    static testMethod void testOrderDepartmentAll()
    {
        Test.StartTest();
        SCContractTestHelper cth = new SCContractTestHelper();
        String contractID = cth.prepareForBtcContractVisitCreateTest();
        System.debug('###Test contract: ' + cth.contract);
        System.debug('###ContractId: ' + contractID);
        SCbtcContractVisitCreate.syncCreateContractVisits(contractID);

        SCbtcContractOrderCreate.createContractOrderDepartmentAll('NL', 'Depart', 1, 'test');
        
        Test.StopTest();
    } // testOrderDepartmentAll
    
    
    //@author GMSS
    static testMethod void CodeCoverageA()
    {
        try { SCHelperTestClass3.createCustomSettings('DE',true); } catch(Exception e) { }
        
        SCArticle__c article = new SCArticle__c(Name = 'test1234');
        Database.insert(article);
        
        List<SCApplicationSettings__c> listAppSetting = new List<SCApplicationSettings__c>();
        for (SCApplicationSettings__c appSetting : [SELECT Id, DEFAULT_MAINTENANCE_ARTICLE_NO__c FROM SCApplicationSettings__c])
        {
            appSetting.DEFAULT_MAINTENANCE_ARTICLE_NO__c = article.Name;
            listAppSetting.add(appSetting);
        }
        Database.update(listAppSetting);
        
        
        Test.startTest();
        
        
        SCbtcContractOrderCreate obj = new SCbtcContractOrderCreate(null, 'de', 'department', 10, 'test');
        
        SCbtcContractOrderCreate.canCreateOrders(new List<String>());
        SCbtcContractOrderCreate.createContractOrder('contractName', 'test');
        SCbtcContractOrderCreate.createContractOrder((Id) UserInfo.getUserId(), 'test');
        SCbtcContractOrderCreate.syncCreateOrders(new List<String>());
        SCbtcContractOrderCreate.asyncCreateContractOrder(new List<String>());
        
        
        Test.stopTest();
    }
    
    
    //@author GMSS
    static testMethod void CodeCoverageB()
    {
        Test.startTest();
        
        SCbtcContractOrderCreate.methodForCodeCoverage();
        
        Test.stopTest();
    }
}
