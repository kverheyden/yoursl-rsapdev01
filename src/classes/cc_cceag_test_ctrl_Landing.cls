@isTest
private class cc_cceag_test_ctrl_Landing {
	
	@isTest static void test_fetchOutlets() {
		Account soldTo = createSoldTo();
		createShipTos(soldTo, 'WE', 5, false);
		createAlternateShipTos(soldTo, 'WE', 2, false);
		createShipTos(soldTo, 'RG', 1, false);
    	User u = cc_cceag_test_TestUtils.getPortalUser(soldTo);
    	insert u;
    	ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
    	ctx.storefront = 'DefaultStore';
    	ctx.portalUserId = u.Id;
    	Test.startTest();
    	ccrz.cc_RemoteActionResult res = cc_cceag_ctrl_Landing.fetchOutlets(ctx);
    	cc_cceag_ctrl_Landing.cc_cceag_bean_HeaderData headerData = (cc_cceag_ctrl_Landing.cc_cceag_bean_HeaderData) res.data;
    	if (headerData != null) {
    		ctx.currentCartID = headerData.cartData.encryptedId;
    		ccrz.cc_RemoteActionResult res2 = cc_cceag_ctrl_Landing.fetchOutlets(ctx);
    	}

    	Test.stopTest();
	}

	@isTest static void test_fetchOutletsBadData() {
		Account soldTo = createSoldTo();
		createShipTos(soldTo, 'WE', 5, false);
		createShipTos(soldTo, 'RG', 1, false);
    	User u = cc_cceag_test_TestUtils.getPortalUser(soldTo);
    	insert u;
    	ccrz__E_Cart__c cart = cc_cceag_test_TestUtils.createCart(soldTo, false);
    	cart.ccrz__User__c = u.Id;
    	cart.OwnerId = u.Id;
    	cart.ccrz__ShipTo__c  = null;
    	insert cart;
    	ccrz__E_Cart__c badCart = [SELECT ccrz__EncryptedId__c from ccrz__E_Cart__c WHERE Id=:cart.Id];
    	ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
    	ctx.storefront = 'DefaultStore';
    	ctx.portalUserId = u.Id;
    	ctx.currentCartID = badCart.ccrz__EncryptedId__c;
    	Test.startTest();
    	ccrz.cc_RemoteActionResult res = cc_cceag_ctrl_Landing.fetchOutlets(ctx);
    	cc_cceag_ctrl_Landing.cc_cceag_bean_HeaderData headerData = (cc_cceag_ctrl_Landing.cc_cceag_bean_HeaderData) res.data;

    	Test.stopTest();
	}

	@isTest static void test_selectOutletPickup() {
		Account soldTo = createSoldTo();
		createShipTos(soldTo, 'WE', 5, true);
		createShipTos(soldTo, 'RG', 1, true);
    	User u = cc_cceag_test_TestUtils.getPortalUser(soldTo);
    	insert u;
    	ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
    	ctx.storefront = 'DefaultStore';
    	ctx.portalUserId = u.Id;
    	ccrz.cc_RemoteActionResult res = cc_cceag_ctrl_Landing.fetchOutlets(ctx);
    	cc_cceag_ctrl_Landing.cc_cceag_bean_HeaderData headerData = (cc_cceag_ctrl_Landing.cc_cceag_bean_HeaderData) res.data;
    	Test.startTest();
    	if (headerData != null) {
    		ctx.currentCartID = headerData.cartData.encryptedId;
    		ccrz.cc_RemoteActionResult res2 = cc_cceag_ctrl_Landing.selectOutlet(ctx, headerData.outlets.get(0), headerData.cartData);
    	}
    	Test.stopTest();
	}
	
	@isTest static void test_selectOutlet() {
		Account soldTo = createSoldTo();
		createShipTos(soldTo, 'WE', 5, false);
		createShipTos(soldTo, 'RG', 1, false);
    	User u = cc_cceag_test_TestUtils.getPortalUser(soldTo);
    	insert u;
    	ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
    	ctx.storefront = 'DefaultStore';
    	ctx.portalUserId = u.Id;
    	ccrz.cc_RemoteActionResult res = cc_cceag_ctrl_Landing.fetchOutlets(ctx);
    	cc_cceag_ctrl_Landing.cc_cceag_bean_HeaderData headerData = (cc_cceag_ctrl_Landing.cc_cceag_bean_HeaderData) res.data;
    	Test.startTest();
    	if (headerData != null) {
    		ctx.currentCartID = headerData.cartData.encryptedId;
    		ccrz.cc_RemoteActionResult res2 = cc_cceag_ctrl_Landing.selectOutlet(ctx, headerData.outlets.get(0), headerData.cartData);
    	}
    	Test.stopTest();
	}

	@isTest static void test_miscellaneous() {
		Test.setMock(HttpCalloutMock.class, new cc_cceag_WeatherForecastMock());
		Account soldTo = createSoldTo();
		createShipTos(soldTo, 'WE', 5, false);
		createShipTos(soldTo, 'RG', 1, false);
    	User u = cc_cceag_test_TestUtils.getPortalUser(soldTo);
    	insert u;
    	ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
    	ctx.storefront = 'DefaultStore';
    	ctx.portalUserId = u.Id;
    	Test.startTest();
    	ccrz.cc_RemoteActionResult res = cc_cceag_ctrl_Landing.getProductTeaserProducts(ctx);
    	ccrz.cc_RemoteActionResult res2 = cc_cceag_ctrl_Landing.fetchWeatherForecast(ctx, soldTo.Id);
    	Test.stopTest();
	}

	static Account createSoldTo() {
		SCPlant__c plant = new SCPlant__c(Name='0123', Name__c='PlantName', Street__c='PlantAddr', ZipCode__c='00000', City__c='PlantCity');
		insert plant;
        Account soldTo = new Account(Name='SoldTo');
        insert soldTo;
        CCAccountRole__c role = new CCAccountRole__c(AccountRole__c='AG',MasterAccount__c=soldTo.Id, SlaveAccount__c=soldTo.Id);
        insert role;
        return soldTo;
    }
    
    static void createShipTos(Account soldTo, String rel, Integer max, boolean pickup) {
    	List<Account> shiptos = new List<Account>();
    	String terms = '24 Stunden';
    	if (pickup)
    		terms = 'Abholung';
    	for (Integer i = 0; i < max; i++)
        	shiptos.add(new Account(Name='ShipTo'+i, AccountNumber='1000'+i, BillingStreet='BillingStreet'+i,BillingCity='BillingCity'+i, BillingPostalCode='0000'+i, BillingCountry='DE', DeliveringPlant__c='0123',ShippingTerms__c=terms,NextPossibleDeliveryDate__c=System.today(), DeliveryWeekdays__c='So,Mo,Di,Mi,Fr,Sa'));
        insert shiptos;
        List<CCAccountRole__c> accRoles = new List<CCAccountRole__c>();
        for (Account shipto: shiptos)
        	accRoles.add(new CCAccountRole__c(AccountRole__c=rel, SlaveAccount__c=shipto.Id, MasterAccount__c=soldTO.Id));
        insert accRoles;
    } 

    static void createAlternateShipTos(Account soldTo, String rel, Integer max, boolean pickup) {
    	List<Account> shiptos = new List<Account>();
    	String terms = '72 Stunden';
    	if (pickup)
    		terms = 'Abholung';
    	for (Integer i = 100; i < (100 + max); i++)
        	shiptos.add(new Account(Name='ShipTo'+i, AccountNumber='1000'+i, BillingStreet='BillingStreet'+i,BillingCity='BillingCity'+i, BillingPostalCode='0000'+i, BillingCountry='DE', DeliveringPlant__c='0123',ShippingTerms__c=terms, DeliveryWeekdays__c='So,Sa'));
        insert shiptos;
        List<CCAccountRole__c> accRoles = new List<CCAccountRole__c>();
        for (Account shipto: shiptos)
        	accRoles.add(new CCAccountRole__c(AccountRole__c=rel, SlaveAccount__c=shipto.Id, MasterAccount__c=soldTO.Id));
        insert accRoles;
    } 

	
}
