/*
 * @(#)SCfwAccount.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCfwAccount
{

    private static SCfwDomain domAccountRole = new SCfwDomain('DOM_ACCOUNT_ROLE');
    
    public Account account { get; set; }
    
    // This value can't be set, but is determined by the record type of the account.
    // Unfortunately you need to re-fetch the record to get it updated after insert.
    public Boolean isPersonAccount { get; set; }
    
    public SCAccountRole__c accountRole { get; set; }
    public SCInstalledBaseRole__c installedBaseRole { get; set; }

    /**
     * Map the account role (domain Id) to the slave account SFDC ID
     */
    //         <domain value, role object>
    private Map<String, SCAccountRole__c> hasRoles = new  Map<String, SCAccountRole__c>();

    /**
     * Constructor taking an Account Id. The account will be fetched then.
     *
     * @param id SFDC Id for an account.
     */
    public SCfwAccount(Id id)
    {
        try
        {
            this.account = [select Id, Name, BillingStreet, BillingCity, BillingCountry__c,
                                isPersonAccount__c
                              from Account
                             where Id = :id];
            this.isPersonAccount = account.isPersonAccount__c;
            fetchRoles();
        }
        catch(Exception e)
        {
            System.debug(LoggingLevel.WARN, 'Unable to find Account for ID ' + id);
        }
    }

    /**
     * Constructor taking an account object.
     */
    public SCfwAccount(Account account)
    {
        this.account = account;
        
        try
        {
            this.isPersonAccount = account.isPersonAccount__c;
            fetchRoles();
        }
        catch (Exception e)
        {
        }
    }

    /**
     * Add the given role and all default account roles betweend a master 
     * and a slave account.
     *
     * @param masterAccount account the order is created for
     * @param slaveAccount account that plays the given role for the master account
     * @param roleId domain values defining an account role
     */
    public void addAllDefaultRoles(Id masterAccount, Id slaveAccount, String roleId)
    {
        addAccountRole(masterAccount, slaveAccount, roleId);
        
        SCfwDomainValue invoiceRecipientDom = domAccountRole.getDomainValueById2('IRecipient');
        SCfwDomainValue serviceRecipientDom = domAccountRole.getDomainValueById2('SRecipient');

        // no invoice recipient so far, add it
        if (!hasRole(invoiceRecipientDom.getId()))
        {
            addAccountRole(masterAccount, slaveAccount, invoiceRecipientDom.getId());
        }
        
        
        // no service recipient so far, add it
        if (!hasRole(serviceRecipientDom.getId()))
        {
            addAccountRole(masterAccount, slaveAccount, serviceRecipientDom.getId());
        }
    }
    
    /**
     * Add all default account roles betweend a master 
     * and a slave account.
     *
     * @param masterAccount account the order is created for
     * @param slaveAccount account that plays the given role for the master account
     */
    public void addAllDefaultRoles(Id masterAccount, Id slaveAccount)
    {
        addAccountRole(masterAccount, slaveAccount, SCfwConstants.DOMVAL_ORDERROLE_RE);
        addAccountRole(masterAccount, slaveAccount, SCfwConstants.DOMVAL_ORDERROLE_LE);
    }
    
    
    /**
     * Add another account role to an existing account. Any existing role will
     * be overwritten.
     *
     * @param masterAccount account the order is created for
     * @param slaveAccount account that plays the given role for the master account
     * @param roleId domain values defining an account role
     */
    public void addAccountRole (Id masterAccount, Id slaveAccount, String roleId)
    {
        try
        {
            SCAccountRole__c role = getRole(roleId);
            
            role.MasterAccount__c = masterAccount;
            role.SlaveAccount__c = slaveAccount;
            role.AccountRole__c = roleId;
            
            upsert role;
            
            hasRoles.put(roleId, role);
            
        }
        catch (Exception e)
        {
            System.debug(LoggingLevel.WARN, 'Unable to upsert Account role ' 
                            + e.getMessage());
        }
        
    
    
    }

    /**
     * Fetch all available roles for an account.
     * <p>
     * The account roles are cached internally and the cache is filled.
     * Existing role are overwritten!
     * TODO: Check if not overwriting existing roles might be better.
     */
    private void fetchRoles()
    {

        try
        {    
            for (SCAccountRole__c role : [select Id, Name, MasterAccount__c, 
                                                SlaveAccount__c, AccountRole__c
                                            from SCAccountRole__c 
                                           where MasterAccount__c = :account.Id])
            {
                hasRoles.put(role.AccountRole__c, role);
            }
        }
        catch (Exception e)
        {
            System.debug(LoggingLevel.WARN, 'Unable to fetch Account roles ' 
                            + e.getMessage());
        }
        
    }
    
    /**
     * Check if an Account has the given role already
     *
     * @param role domain value ID of the Account role
     * @return The corresponding account role if existing or an empty
     *         SCAccountRole object
     */
    public SCAccountRole__c getRole(String roleId)
    {
        // we currently have no master account, 
        // so what should we search for
        if (account == null)
        {
            return new SCAccountRole__c();
        }
        
        if (hasRoles.isEmpty())
        {
            fetchRoles();
        }
        
        if (hasRoles.containsKey(roleId))
        {
            return hasRoles.get(roleId);
        }
        
        return new SCAccountRole__c();
    }

    /**
     * Return all defined account roles that will be used as 
     * defaults for order roles
     *
     * @return all defined order roles
     */
    public List<SCAccountRole__c> getAllRole()
    {
        return hasRoles.values();
    }

    /**
     * Check if an Account has the given role already
     *
     * @param role domain value ID of the Account role
     * @return <code>true</code> if the role is already assigned
     */
    public Boolean hasRole(String roleId)
    {
    
        if (hasRoles.isEmpty())
        {
            fetchRoles();
        }
        
        if (hasRoles.containsKey(roleId))
        {
            return true;
        }
        
        return false;
    }
    
    
    /**
     * Get the service recipient for the current account.
     * <p>
     * The account for the role service recipient is read directly from the DB
     * TODO: It might be better to check the internal cache first!
     *
     * @return Account in the role of a service recipient
     */
    public Account getServiceRecipient()
    {
        String roleId = domAccountRole.getDomainValueById2('SRecipient').getId();
        
        try
        {
            Account recipient = [select Id, GeoX__c, GeoY__c, GeoApprox__c, FirstName__c, LastName__c,
                                        BillingStreet, BillingCity, 
                                        BillingPostalCode, BillingCountry__c
                                   from Account
                                  where Id in (Select SlaveAccount__c from SCAccountRole__c 
                                                        where MasterAccount__c = :account.Id
                                                          and AccountRole__c = :roleId) limit 1];
            return recipient;
        
        }
        catch(Exception e)
        {
            return null;
        }
    
    }
    
    /**
     * Check if the given Account is already geocoded or not.
     *
     * @param account Account to check the geocoding for
     * @return <code>true</code> if the account is gecoded and false otherwise
     */
    public static Boolean isGeoCoded(Account account)
    {
        if (account <> null && 
            account.GeoX__c != null &&
            account.GeoY__c != null &&
            (account.GeoX__c != 0 || account.GeoY__c != 0))
        {
            return true;
        }
        
        return false;
    }

    /**
     * Check if the given Order Role is already geocoded or not.
     *
     * @param orderRole Order Role to check the geocoding for
     * @return <code>true</code> if the account is gecoded and false otherwise
     */
    public static Boolean isGeoCoded(SCOrderRole__c orderRole)
    {
        if (orderRole <> null && 
            orderRole.GeoX__c != null &&
            orderRole.GeoY__c != null &&
            (orderRole.GeoX__c != 0 || orderRole.GeoY__c != 0))
        {
            return true;
        }

        return false;
    }

    /**
     * Looks for a given account and returns null if not fount or the account object.
     *
     * @param  srcAccount    account object to search for
     * @return the found account object or null if not found
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static Account findAccount(Account srcAccount)
    {
        return findAccount(srcAccount, true);
    }
    public static Account findAccount(Account srcAccount, Boolean exact)
    {
        if (SCBase.isSet(srcAccount.BillingStreet))
        {
            Integer pos = srcAccount.BillingStreet.lastIndexOf('Str');
            if (pos != -1)
            {
                srcAccount.BillingStreet = srcAccount.BillingStreet.substring(0, pos + 3) + '%';
            } // if (pos != -1)
            else
            {
                pos = srcAccount.BillingStreet.lastIndexOf('str');
                if (pos != -1)
                {
                    srcAccount.BillingStreet = srcAccount.BillingStreet.substring(0, pos + 3) + '%';
                }
                else
                {
                    srcAccount.BillingStreet += '%';
                }
            } // else [if (pos != -1)]
        } // if (SCBase.isSet(srcAccount.BillingStreet))
        
        String firstName = SCBase.isSet(srcAccount.FirstName__c) ? '\'' + srcAccount.FirstName__c + '%\'' : 'null';
        String lastName = SCBase.isSet(srcAccount.LastName__c) ? '\'' + srcAccount.LastName__c + '%\'' : 'null';
        String name1 = SCBase.isSet(srcAccount.Name) ? '\'%' + srcAccount.Name + '\'' : '\'null\'';
        String name2 = SCBase.isSet(srcAccount.LastName__c) ? '\'' + srcAccount.LastName__c + '%\'' : '\'null\'';
        System.debug('#### findAccount(): search firstName -> ' + firstName);
        System.debug('#### findAccount(): search lastName  -> ' + lastName);
        System.debug('#### findAccount(): search name1     -> ' + name1);
        System.debug('#### findAccount(): search name2     -> ' + name2);
        
        String query = 'Select Id, Name, AccountNumber, Type, Salutation__c, ' + 
                              'FirstName__c, LastName__c, Name2__c, Name3__c, ' + 
                              'BillingStreet, BillingHouseNo__c, ' + 
                              'BillingExtension__c, BillingPostalCode, ' + 
                              'BillingCity, BillingCounty__c, ' + 
                              'BillingCountryState__c, BillingCountry__c, ' + 
                              'BillingDistrict__c, BillingFlatNo__c, ' + 
                              'BillingFloor__c, Phone, Phone2__c, Fax, Email__c, ' + 
                              'GeoApprox__c, PersonTitle__c, ' + 
                              'Mobile__c, PersonHomePhone__c, GeoX__c, GeoY__c, ' + 
                              'isPersonAccount__c, RecordTypeId, ' + 
                              'util_updateAccount__c ' + 
                         'from account where (';
        if (firstName.equals('null') && !lastName.equals('null'))
        {
            query += 'LastName__c like ' + lastName + ' or ';
        }
        else if (!firstName.equals('null') && !lastName.equals('null'))
        {
            query += '(FirstName__c like ' + firstName + ' ' + 
                       'and LastName__c like ' + lastName + ') or ';
        }
        if (!name1.equals('\'null\''))
        {
            query += 'Name like ' + name1 + ' or ';
        }
        query += 'Name like ' + name2 + ') ' + 
                 'and BillingStreet like \'' + srcAccount.BillingStreet + '\' ' + 
                 'and BillingHouseNo__c = \'' + srcAccount.BillingHouseNo__c + '\' ' + 
                 'and BillingPostalCode = \'' + srcAccount.BillingPostalCode + '\' ' + 
                 'and BillingCity = \'' + srcAccount.BillingCity + '\' ' + 
                 'and BillingCountry__c = \'' + srcAccount.BillingCountry__c + '\'';
        System.debug('#### findAccount(): query -> ' + query);
        List<Account> foundAccounts = null;
        try
        {
            foundAccounts = Database.query(query);
            System.debug('#### findAccount(): foundAccounts -> ' + foundAccounts);
            if (exact && (foundAccounts.size() != 1))
            {
                return null;
            }
            return (foundAccounts.size() > 0) ? foundAccounts[0] : null;
        }
        catch(Exception e)
        {
            return null;
        }
    } // findAccount
}
