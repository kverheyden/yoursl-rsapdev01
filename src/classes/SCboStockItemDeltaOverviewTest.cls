/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SCboStockItemDeltaOverviewTest {

    static testMethod void upsertTest() 
    {
      	SCHelperTestClass.createTestUsers(true);
        SCHelperTestClass.createTestResources(SCHelperTestClass.users, true);
        SCHelperTestClass3.createCustomSettings('DE',true);
        
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);
        SCHelperTestClass.createTestArticles(true);
        SCHelperTestClass.createTestStockItems(SCHelperTestClass.articles, SCHelperTestClass.stocks.get(0).id, SCHelperTestClass.stocks.get(1).id, true);
        SCHelperTestClass.createTestAssignments(SCHelperTestClass.resources, SCHelperTestClass.stocks,null, true);
        
        system.debug('#### stocks before update: ' + SCHelperTestClass.stocks);
        system.debug('#### stockitems before update: ' + SCHelperTestClass.stockItems);
        
        Test.startTest();
        
        SCStockItem__c[] stockItems = SCHelperTestClass.stockItems;
        SCStock__c[] stocks = SCHelperTestClass.stocks;
        
        List<SCStockItem__c> updatedStockItems =
        [
        	SELECT 
        		Id,
        		Qty__c,
        		util_StockItemSelector__c
        		
        	FROM
        		SCStockItem__c
        	
        	
        ];
        
        List<SCStockItemDeltaOverview__c> sidoListCMS = new List<SCStockItemDeltaOverview__c>();
        List<SCStockItemDeltaOverview__c> sidoListSAP = new List<SCStockItemDeltaOverview__c>();
        for(SCStockItem__c item: updatedStockItems)
        {
        	SCStockItemDeltaOverview__c sido = new SCStockItemDeltaOverview__c();
        	sido.StockItem__c = item.Id;
        	sido.StockItemId__c = item.Id;
        	//set random qty
        	sido.QtyCMS__c = (Integer)(Math.random() * 10);
        	sido.Source__c = 'CMS';
        	sidoListCMS.add(sido);
        	
        }
        
        upsert sidoListCMS StockItemId__c;
        
        for(SCStockItem__c item: updatedStockItems)
        {
        	system.debug('#### item for sap: ' + item);
        	SCStockItemDeltaOverview__c sido = new SCStockItemDeltaOverview__c();
        	sido.ID2__c = item.util_StockItemSelector__c;
        	
        	//set random qty
        	sido.QtySAPunrestricted__c = (Integer)(Math.random() * 10);
        	sido.Source__c = 'SAP';
        	sidoListSAP.add(sido);
        	
        }
        upsert sidoListSAP ID2__c;
        
           
        Test.stopTest();
      	
      
    }
}
