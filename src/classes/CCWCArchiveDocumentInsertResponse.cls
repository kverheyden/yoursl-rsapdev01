/*
 * @(#)CCWCArchiveDocumentInsertResponse.cls 
 * 
 * Copyright 2012 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
global without sharing class CCWCArchiveDocumentInsertResponse  
{
    
    public static void processArchiveDocumentInsertResponse(String messageID, String requestMessageID, String headExternalID, 
                                                  CCWSGenericResponse.ReferenceItem referenceItem, 
                                                  String MaximumLogItemSeverityCode, 
                                                  CCWSGenericResponse.LogItemClass[] logItemArr, 
                                                  List<SCInterfaceLog__c> interfaceLogList,
                                                  CCWSGenericResponse.GenericServiceResponseMessageClass GenericServiceResponseMessage)
    {
        SCInterfaceLog__c responseInterfaceLog = null;
        processArchiveDocumentInsertResponse(messageID, requestMessageID, headExternalID, 
                                                  referenceItem, 
                                                  MaximumLogItemSeverityCode, 
                                                  logItemArr, 
                                                  interfaceLogList,
                                                  GenericServiceResponseMessage,
                                                  responseInterfaceLog);
    }
    
    public static void processArchiveDocumentInsertResponse(String messageID, String requestMessageID, String headExternalID, 
                                                  CCWSGenericResponse.ReferenceItem referenceItem, 
                                                  String MaximumLogItemSeverityCode, 
                                                  CCWSGenericResponse.LogItemClass[] logItemArr, 
                                                  List<SCInterfaceLog__c> interfaceLogList,
                                                  CCWSGenericResponse.GenericServiceResponseMessageClass GenericServiceResponseMessage,
                                                  SCInterfaceLog__c responseInterfaceLog)
    {
        debug('reference: ' + referenceItem);
        String interfaceName = 'SAP_ARCHIVE_DOCUMENT_INSERT';
        String interfaceHandler = 'CCWCArchiveDocumentInsertResponse';
        String type = 'INBOUND';
        String direction = 'inbound';
        ID referenceID = null;
        String refType = null;
        ID referenceID2 = null;
        String refType2 = '';
        ID responseID = null;
        String resultCode = 'E000';
        String resultInfo = 'Success'; 
        
        String jsonInput = JSON.serialize(GenericServiceResponseMessage);
        SCInterfaceBase ib = new SCInterfaceBase();
        String fromJSONMap = ib.getDataFromJSON(jsonInput);
        debug('from json: ' + fromJSONMap);
        
        
        String data = 'headExternalID: ' + headExternalID + ',\n\nreferenceItem: ' + referenceItem + ',\n\nMaximumLogItemSeverityCode: ' + MaximumLogItemSeverityCode
                    + '\n allResponse: ' + fromJSONMap;
        debug('data: ' + data);            
        // Fill interface log response created by a pivot web service
        responseInterfaceLog.Interface__c = interfaceName;
        responseInterfaceLog.InterfaceHandler__c = interfaceHandler;
        responseInterfaceLog.Direction__c = direction;            
        responseInterfaceLog.MessageID__c = messageID;            
        responseInterfaceLog.ReferenceID__c = referenceID;            
        responseInterfaceLog.ResultCode__c = resultCode;            
        responseInterfaceLog.ResultInfo__c = resultInfo;
        responseInterfaceLog.Data__c = data;
        responseInterfaceLog.Data__c = responseInterfaceLog.Data__c.left(32000);

        String step = '';
        Savepoint sp = Database.setSavepoint();

        ID orderIdHead = null;
        try
        {
            step = 'find the movements'; 
            
            Attachment attachment = readAttachment(referenceItem); 
            debug('attachment: ' + attachment);

            // find the interfacelog
            step = 'find a request interface';

            SCInterfaceLog__c requestInterfaceLog = CCWSGenericResponse.readOutoingInterfaceLog(requestMessageID, null, null, 'SAP_ARCHIVE_DOCUMENT_INSERT');       

            
            debug('requestInterfaceLog: ' + requestInterfaceLog);
            
            SObject parent = readParentOfAttachment(attachment.ParentId, attachment.Id);
            
            //GMSGB 03.09.13 It is possible that the SAP response is faster than the commit of the call
            // Thus it is possible that the interface does not exist.
            if(requestInterfaceLog == null)
            {
            	SCfwInterfaceRequestPendingException e = new SCfwInterfaceRequestPendingException('As the request log does not exist the commit of the request is not processed.'); 
            	if(parent != null && parent instanceof SCOrder__c)
                {
                    e.order = [select id from SCOrder__c where id =: parent.id];
                }
                throw e;
            }

            
            // Test
            //SCfwInterfaceRequestPendingException e = new SCfwInterfaceRequestPendingException('As the request log does not exist the commit of the request is not processed.'); 
            //if(parent != null && parent instanceof SCOrder__c)
            //{
            //	e.order = [select id from SCOrder__c where id =: parent.id];
            //}
            //throw e;
            
            //SCfwException er = new SCfwException('Test SCfwException');
        	//throw er;
        
            if(parent != null)
            {
                CCWSGenericResponse.LogItemClass[] loopLogItemArr = CCWSGenericResponse.getLogItems(referenceItem.externalID, logItemArr);
                debug('loop log Item Arr: ' + loopLogItemArr);

                parent.put('ERPStatusArchiveDocumentInsert__c', CCWSGenericResponse.getResultStatus(loopLogItemArr));
                parent.put('ERPResultDate__c', DateTime.now());
                
                // write response interface log
                responseInterfaceLog.Count__c = 1;
           



                update parent;
                debug('parent after update: ' + parent);          

                if(parent instanceof SCOrder__c)
                {
                    orderIdHead = parent.id;
                }
                if(parent instanceof SCOrderItem__c)
                {
                    step = 'update orders';
                    updateOrders(((SCOrderItem__c)parent).Order__c, ((SCOrderItem__c)parent).ERPStatusArchiveDocumentInsert__c);
                }
            }




            
            step = 'write a response interface log';
            debug('responseInterfaceLog: ' + responseInterfaceLog);

            // update request interface log
            step = 'update the request interface log';
            
            if(responseInterfaceLog != null)
            {
                // GMSGB 03.09.13 It is possible that the SAP response is faster than the commit of the call
                // Thus it is possible that the interface does not exist.
                if(requestInterfaceLog != null)
                {
                    responseInterfaceLog.order__c = requestInterfaceLog.order__c;
                    requestInterfaceLog.Response__c = responseInterfaceLog.Id;
                    requestInterfaceLog.Idoc__c = responseInterfaceLog.Idoc__c;
                    interfaceLogList.add(requestInterfaceLog);
                }
            }
            else
            {
                throw new SCfwException('A response interface log object could not be crated for messageID: ' + messageID); 
            }
        }
        catch(SCfwInterfaceRequestPendingException errorRequestNotPending)
        {
        	Database.rollback(sp);
        	throw errorRequestNotPending;
        }
        catch(SCfwException e) 
        {
            Database.rollback(sp);
            throw e;
        }  
        catch(Exception e) 
        {
            Database.rollback(sp);
            throw e;
        } 
        finally
        {
            if(orderIdHead != null)
            {
                CCWCOrderCloseEx.processNext(orderIdHead);
            }               
        }  
    }//processCreateOrderResponse

    public static Attachment readAttachment(CCWSGenericResponse.ReferenceItem referenceItem)
    {
        Attachment retValue = null;
        // TODO: ###What ERPStatus
        List<Attachment> attachmentList = [select ID, ParentId from Attachment where id = : referenceItem.externalID];
        if(attachmentList.size() == 0)
        {
            throw new SCfwException('There no attachment for the id: ' + referenceItem.externalID);
        }
        else
        {
            retValue = attachmentList[0];
        }   
        return retValue;
    }

    public static sObject readParentOfAttachment(ID parentId, ID attachmentId)
    {
        sObject retValue = null;
        Boolean parentOfAttachmentFound = false;
        List<SCOrderItem__c> orderItemList = [Select id, Order__c from SCOrderItem__c where id = : parentId];
        if(orderItemList.size() > 0)
        {
            retValue = orderItemList[0];

            parentOfAttachmentFound = true;
        }
        

        if(!parentOfAttachmentFound)
        {
            orderItemList = [Select id, Order__c from SCOrderItem__c where Order__r.id = : parentId];
            if(orderItemList.size() > 0)
            {
                retValue = orderItemList[0];

                parentOfAttachmentFound = true;
            }
        }

        if(!parentOfAttachmentFound)
        {
            List<SCInventory__c> inventoryList = [Select id from SCInventory__c where id = : parentId];
            if(inventoryList.size() > 0)
            {
                retValue = inventoryList[0];
                parentOfAttachmentFound = true;
            }
        }
        if(!parentOfAttachmentFound)        
        {
            String msg = 'There is no order item for the id: ' + parentId + ' and attachment id: ' + attachmentId;
            debug(msg);
            throw new SCfwException(msg);
        }
        return retValue;
    }
    

    public static void updateOrders(ID orderID, String status)
    {
        if(orderID != null) // prevent to read all orders in case of null
        {
            List<SCOrder__c> orderList = [Select ID from SCOrder__c where id = :orderID];
            if(orderList != null && orderList.size() > 0)
            {
                for(SCOrder__c o: orderList)
                {
                      if(status != null)
                    {
                        o.ERPStatusArchiveDocumentInsert__c = status;
                        o.ERPResultDate__c = DateTime.now();
                    }       
                }
            }
            update orderList;
        }
    }
    

    public static void debug(String msg)
    {
        System.debug('###...' +  msg);        
    }

   
}
