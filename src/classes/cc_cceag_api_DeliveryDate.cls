global without sharing class cc_cceag_api_DeliveryDate extends ccrz.cc_api_DeliveryDate {
    public cc_cceag_api_DeliveryDate() {
        
    }

    global override Map<String,Object> getDeliveryDates(Map<String,Object> inputData){
        List<cc_bean_Outlet> outlets = (List<cc_bean_Outlet>) inputData.get('shipTos');
        if (outlets.size() > 0) {
            Map<String, Map<String,Object>> retData = new Map<String, Map<String,Object>>();
            for (cc_bean_Outlet outlet: outlets) {
                Map<String,Object> outletDateData = new Map<String,Object>();
                Integer buffer = determineBuffer(outlet.shipTerms);
                Set<Integer> daysAllowed = allowedDays(outlet.deliveryDays);
                outletDateData.put('buffer', buffer);
                Date firstDate = determineFirstDate(outlet.nextDelivery, buffer, daysAllowed);
                outletDateData.put('direction', formatDate(firstDate, ' '));
                if (outlet.nextDelivery == null || outlet.nextDelivery < firstDate)
                    outlet.nextDelivery = firstDate;
                outletDateData.put('rawDate', outlet.nextDelivery);
                outletDateData.put('defaultDate', formatDate(outlet.nextDelivery, ' '));
                List<String> excludedDates = buildHolidays();
                if (daysAllowed.isEmpty())
                    excludedDates.add('* * * 0,6');
                else
                    excludedDates.add(buildDaysFilter(daysAllowed));
                outletDateData.put(EXCLUDED_DATES, excludedDates);
                retData.put(outlet.outlet, outletDateData);
            }
            return retData;
        }
        return null;
    }

    private List<String> buildHolidays() {
        List<String> holidays = new List<String>();
        List<Holiday__c> holidayData = [ SELECT Date__c FROM Holiday__c WHERE Date__c > : System.today()];
        for (Holiday__c holiday: holidayData)
            holidays.add(formatDate(holiday.Date__c, ' '));
        return holidays;
    }

    private String formatDate(Date filterDate, String sep) {
        DateTime dt = DateTime.newInstance(filterDate.year(), filterDate.month(), filterDate.day());
        return dt.format('dd' + sep + 'M' + sep + 'yyyy');
    }

    private Date determineFirstDate(Date nextAvailable, Integer termLength, Set<Integer> daysAllowed) {
        DateTime dt = System.now();
        Time currTime = dt.time();
        Date currDate = dt.date();
        Time cutoffTime = Time.newInstance(8, 30, 0, 0);
        DateTime currDT = DateTime.newInstance(currDate, currTime);
        DateTime cutoffDT = DateTime.newInstance(currDate, cutoffTime);
        Integer adjustedLength = termLength;
        if (currDT.getTime() > cutoffDT.getTime())
            adjustedLength += 1;
        Date firstAllowed = currDate.addDays(adjustedLength);
        Date baseline = Date.newInstance(0001, 1, 1);
        Integer dayOfWeek = Math.mod(baseline.daysBetween(firstAllowed), 7)-1;
        if (daysAllowed.contains(dayOfWeek))
            return firstAllowed;
        else {
            Date updatedDate = firstAllowed;
            for (Integer i = 0; i <= 7; i++) {
                Integer dayVal = Math.mod(dayOfWeek + i, 7);
                if (daysAllowed.contains(dayVal)) {
                    updatedDate = updatedDate.addDays(i);
                    break;
                }
            }
            return updatedDate;
        }
    }

    private Integer determineBuffer(String shipTerms) {
        if (shipTerms == 'Abholung')
            return 1;
        else if (shipTerms == '24 Stunden')
            return 0;
        else if (shipTerms == '48 Stunden')
            return 1;
        else if (shipTerms == '72 Stunden')
            return 2;
        else
            return 1;
    }

    private String buildDaysFilter(Set<Integer> daysAllowed) {
        String filterDays = '* * *';
        String sep = ' ';
        for (Integer i = 0; i <= 6; i++) {
            if (!daysAllowed.contains(i)) {
                filterDays += sep + i;
                sep = ',';
            }
        }
        return filterDays;
    }

    private Set<Integer> allowedDays(String daysAvailable) {
        if (daysAvailable == null)
            daysAvailable = '';
        Set<Integer> daysAllowed = new Set<Integer>();
        if (daysAvailable.indexOf('So') != -1)
            daysAllowed.add(0);
        if (daysAvailable.indexOf('Mo') != -1)
            daysAllowed.add(1);
        if (daysAvailable.indexOf('Di') != -1)
            daysAllowed.add(2);
        if (daysAvailable.indexOf('Mi') != -1)
            daysAllowed.add(3);
        if (daysAvailable.indexOf('Do') != -1)
            daysAllowed.add(4);
        if (daysAvailable.indexOf('Fr') != -1)
            daysAllowed.add(5);
        if (daysAvailable.indexOf('Sa') != -1)
            daysAllowed.add(6);
        return daysAllowed;
    }

}
