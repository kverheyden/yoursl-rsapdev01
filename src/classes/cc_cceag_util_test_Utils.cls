/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class cc_cceag_util_test_Utils {

    static testMethod void myUnitTest() {
        cc_cceag_util_Utils utils = new cc_cceag_util_Utils();
        ccrz.cc_RemoteActionContext context = new ccrz.cc_RemoteActionContext();
        system.assertEquals(false, cc_cceag_util_Utils.initAction(context).success);
        try {
        	integer y = 12/0;
        } catch (Exception e) {
        	ccrz.cc_bean_Message  msg = cc_cceag_util_Utils.buildExceptionMessage( e);
        	system.assert(msg.message.indexOf(e.getMessage()) != -1);
        	msg = cc_cceag_util_Utils.buildErrorMessage(e.getMessage(), 'Message Label', null);
        	system.assert(msg.message.indexOf(e.getMessage()) != -1);
        	msg = cc_cceag_util_Utils.buildErrorMessage(e.getMessage(), 'Message Label', 'messagingSection-Error');
        	system.assert(msg.message.indexOf(e.getMessage()) != -1);
        }
        
    }
}
