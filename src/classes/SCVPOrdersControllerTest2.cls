/*
* @(#)SCVPOrdersControllerTest2.cls
* 
* Copyright 2014 by GMS Development GmbH, Karl-Schurz-Strasse 29, 
* DE-33100 Paderborn. All rights reserved.
* 
* Test implementation of the SCPortal class
* 
* @history 
* 2014-11-04 GMSSU created
* 
* @review 
*/
@isTest (oninstall=true seealldata=false)
private class SCVPOrdersControllerTest2 
{

   /* Data needed for the test:
    * 
    * SCOrder__c
    * User
    * SCVendor__c
    * SCVendorUser__c
    * SCSearchFilter__c
    * SCOrderExternalAssignment__c
    */
    static testMethod void testPositivForVendor1() 
    {
        // Initializing controller
        SCVPOrdersController2 con = new SCVPOrdersController2();
        // Setting some variable values
        con.filterObjectType = 'SCOrder__c';
        con.filterGroupId = '001';
        // Creating an order object
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet(true);
        // Creating test user
        User testUser;
        for(User u : SCHelperTestClass4.createTestUsers())
        {
            if(u.Alias == 'GMSTest1')
            {
                testUser = u;
            }
        }
        // Vendor
        SCVendor__c vendor = new SCVEndor__c(Name           = '0123456789',
                                             Name1__c       = 'My test vendor',
                                             Country__c     = 'DE',
                                             PostalCode__c  = '33100',
                                             City__c        = 'Paderborn',
                                             Street__c      = 'Bahnhofstrt',
                                             LockType__c    = '5001',
                                             Status__c      = 'Active');
        insert vendor;
        // Vendor - User
        SCVendorUser__c vendorUser = new SCVendorUser__c(User__c   = testUser.Id,
                                                         Vendor__c = vendor.id);
        insert vendorUser;
        // Order External Asignment
        SCOrderExternalAssignment__c extAssignment = new SCOrderExternalAssignment__c(Order__c          = SCHelperTestClass.order.id,
                                                                                      Vendor__c         = vendor.id,
                                                                                      Status__c         = 'assigned',
                                                                                      WishedDate__c     = Date.today());
        insert extAssignment;
        
        
        Attachment t_attachment = new Attachment(
            Name = 'test',
            Body = Blob.valueOf('.'),
            ParentId = SCHelperTestClass.order.id
        );
        
        insert t_attachment;
        

        Test.startTest();
        
        System.runAs(testUser) 
        {
            // null prevention calls
            Attachment[] t_extAssignmtAttachmtList = con.a_extAssignmtAttachmtList;
            SCOrderExternalAssignmentItem__c[] t_orderExtAssignmItmList = con.a_orderExtAssignmItmList;
            String t_closeStatus = con.a_closeStatus;
            
            // Orders list
            List<SCOrder__c> orders = con.getOrders();
            System.assertEquals(orders.size(), 1);
            
            // Export to Excel
            con.selectedValues = String.valueOf(SCHelperTestClass.order.id);
            con.exportOrdersToExcel();
            System.assertEquals(con.ordersForExcelExport.size(), 1);
            
            // Export to PDF
            con.openOrdersToPDF();
            orders = new List<SCOrder__c>();
            orders = con.getExportOrdersToPDF();
            System.assertEquals(orders.size(), 1);
            
            // Order import fields
            System.assertEquals(con.getImportedFieldsTxt().isEmpty(),false);
            
            // Order type list
            System.assertEquals(con.getOrderType().isEmpty(), false);
            
            // Order status list
            System.assertEquals(con.getOrderStatus().isEmpty(), false);
            
            // Edit order
            con.selectedOrderID = SCHelperTestClass.order.id;
            con.doOrderEdit();
            
            System.assert(!con.a_extAssignmtAttachmtList.isEmpty());
            
            // Reload Attachment List
            con.ReloadAttachmentList();
            
            // Set current item.
            System.assert(!con.selectedOrder.OrderItem__r.isEmpty());
            con.a_currentOrderExtAssignmItmId =  con.selectedOrder.OrderItem__r.get(0).Id;
            
            System.assert(con.a_currentOrderExtAssignmItmId != null);
            
            // Set an item to completed.
            con.SetExtAssignmItmCompleted();
            
            // Close Order (positive)
            con.CloseOrder();
            
            System.assert([SELECT ExternalReady__c FROM SCOrder__c WHERE Id =: con.selectedOrder.Id LIMIT 1].ExternalReady__c);
            
            SCOrder__c t_currentOrder = con.selectedOrder;
            
            t_currentOrder.ExternalReady__c = false;
            
            update t_currentOrder;
            
            // Set an item to incomplete.
            con.SetExtAssignmItmToIncomplete();
            
            // Close Order (negative 1: undone external assignment items)
            con.CloseOrder();
            
            // Delete Equipment(s)
            System.assert(con.selectedOrder.OrderItem__r.size() == 1, con.selectedOrder.OrderItem__r.size());
            
            delete con.selectedOrder.OrderItem__r;
            
            con.setCon = null;
            con.doOrderEdit();
            
            // Close Order (negative 2: no equipment)
            con.CloseOrder();
            
            
            // Delete Attachment
            Integer t_currentAttachmentSize = con.a_extAssignmtAttachmtList.size();
            
            con.a_selctedAttachmentId = con.a_extAssignmtAttachmtList.get(0).Id;
            
            con.DeleteAttachment();
            
            System.assert(con.a_extAssignmtAttachmtList.isEmpty());
            
            // Close Order (negative 3: no attachment)
            con.CloseOrder();
            
            
            // Save order
            con.orderSave();
            
            // Reload items after equipment change
            con.reloadOrderItems();
            
            // Filter init
            // Check filter on start
            // Filters: check filter on start
            SCSearchFilter__c defaultFilter = new SCSearchFilter__c(Default__c  = true,
                                                                    User__c     = testUser.id,
                                                                    Title__c    = 'Test filter 1',
                                                                    Object__c   = 'SCOrder__c',
                                                                    Group__c    = '001',
                                                                    Filter__c   = '');
            insert defaultFilter;
            
            List<SCSearchFilter__c> filters = [Select Id From SCSearchFilter__c Where User__c = :testUser.Id];
            system.assertEquals(1, filters.size());
            
            // Creating a new filter
            con.helperOrder.ERPOrderNo__c = '123';
            con.selectedTypeValues.add('5001');
            con.selectedStatusValues.add('5002');
            con.dateHelper1.Start__c = Date.today();
            con.dateHelper1.End__c = Date.today() + 5;
            con.dateHelper2.Start__c = Date.today();
            con.dateHelper2.End__c = Date.today() + 5;
            con.createNewFilter();
            // Assign filter
            con.assignFilter(con.filters.values()[0]);

        }
        
        Test.stopTest();
    }
}
