/*
 * @(#)SCMapViewData.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
* @author Eugen Tiessen <etiessen@gms-online.de>
* @version $Revision$, $Date$
*/
public class SCMapViewData {

    
    public List<SCMapViewMarker> markers {get; set;}
    public List<SCMapViewTour> tours {get; set;}
    public SCMapViewMarker.Point center {get; set;}
    public Double radius {get; set;}
    public String color {get; set;}
    
    public Map<String,Boolean> optionsControl {get; private set;}

    public SCMapViewData(){
        this.init();
        
        this.color = SCMapViewMarker.DEFAULT_COLOR;
        this.setPanControl(true);
        this.setZoomControl(true);
        
        
    }
    
    public SCMapViewData(String color){
        this.init();
        
        this.color=color;
        this.setPanControl(true);
        this.setZoomControl(true);
        
        
    }
    
    public SCMapViewData(Boolean showControls){
        this.init();
         
        this.color = SCMapViewMarker.DEFAULT_COLOR;
        this.setPanControl(showControls);
        this.setZoomControl(showControls);
       
    }
    
    
    private void init()
    {
    	this.markers = new List<SCMapViewMarker>();
    	this.tours = new List<SCMapViewTour>();
        this.optionsControl = new Map<String,Boolean>();
        
        
    }
    /**
     * not in use
     
    public static SCMapViewData getInstanceFromJson(String jsonCode){
        SCMapViewData tmp=(SCMapViewData)JSON.deserialize(jsonCode, SCMapViewData.class);
        //system.debug('json deseri:'+tmp);
        return tmp;
    }*/
    public String getJson(){
        String tmp=JSON.serialize(this);
        //system.debug('json serialization='+tmp);
        return tmp;
    }
    public void setCenter(Double lat, Double lng){
        this.center=new SCMapViewMarker.Point(lat,lng);
    }
    //convert to meter
    public void setRadius(Double radius){
        this.radius=radius;
    }
   
    
    public void addMarker(SCMapViewMarker marker){
        if(markers!=null)
            this.markers.add(marker);
    }
    
    public void addMarkers(List<SCMapViewMarker> markers){
        if(markers!=null)
            this.markers.addAll(markers);
    }
    
    public void addTour(SCMapViewTour tour){
        this.tours.add(tour);
    }
    
    public void setTours(List<SCMapViewTour> tours)
    {
        this.tours = tours;
    }
    
    public List<SCMapViewTour> getTours()
    {
    	return this.tours;
    }
    
    public void setPanControl(Boolean b)
    {
    	this.optionsControl.put('panControl',b);
    }
    
    public void setZoomControl(Boolean b)
    {
    	this.optionsControl.put('zoomControl',b);
    }
        

    
}
