/**********************************************************************
Name:  CCWCRestAccountsWithinRadius()
======================================================
Purpose:                                                            
Represents a web service. Gets accounts withing a certain radius. Requires longitude and latitude.                                                      
======================================================
History                                                            
-------                                                            
Date  	AUTHOR	DETAIL 
01/09/2013	Jan Mensfeld
***********************************************************************/
@RestResource(urlMapping='/AccountsWithinRadius/*')
global with sharing class CCWCRestAccountsWithinRadius {
	public static final string sSalesAppSetting_ProximitySearchRadiusM_Name = 'ProximitySearchRadiusM';
	public static final string sSalesAppSetting_ProximitySearchMaxResults_Name = 'ProximitySearchMaxResults';
	public static final Double dOneKM = 0.009;
	
	global class Coordinate {
		public Double dLongitude { get; set; }
		public Double dLatitude { get; set; }
		public Coordinate(){}		
		public Coordinate (Double dLongitude, Double dLatitude) {		
			this.dLongitude = dLongitude;
			this.dLatitude = dLatitude;
		}
		public override String toString(){
			return '(' + dLongitude + ' ; ' + dLatitude + ')';
		}		
	}
		
	//Used for REST response
	global class AccountWithinRadius implements Comparable {
		public String AccountNumber { get; set; } 
		public String BillingCity { get; set; }
		public String BillingPostalCode { get; set; }
		public String BillingStreet { get; set; }
		public String BillingHouseNumber { get; set; }
		public String AccountName { get; set; }
		public String AccountName2 { get; set; }
		public String Phone { get; set; }
		public String SubTradeChannelText { get; set; }
		public Id AccountId { get; set; }
		public Double Longitude { get; set; }
		public Double Latitude { get; set; }
		public Integer Distance { get; set; }
		
		public String Error { get; set; }
		public Boolean Success { get; set; }

		public AccountWithinRadius() {
		}	
		public AccountWithinRadius(Account objAccount) {
			this.AccountId =           objAccount.Id;
			this.Longitude =           objAccount.GeoX__c;
			this.Latitude =            objAccount.GeoY__c;
			this.AccountNumber =       objAccount.AccountNumber;
			this.BillingCity =         objAccount.BillingCity;
			this.BillingPostalCode =   objAccount.BillingPostalCode;
			this.BillingStreet =       objAccount.BillingStreet;
			this.BillingHouseNumber =  objAccount.BillingHouseNo__c;
			this.AccountName =         objAccount.Name;
			this.SubTradeChannelText = objAccount.Subtradechannel__c;
			this.AccountName2        = objAccount.Name2__c;
			this.Phone               = objAccount.Phone;
			this.Success = true;
			
		}
		
		public AccountWithinRadius(Account objAccount, Integer iDistance) {
			this(objAccount);
			this.Distance = iDistance;
		}
		
		public void addError(String sError){
			this.Error = sError;
			this.Success = false;
		}		
		// listAccountsWithinRadius.sort() is sorting list by Distance
		public Integer compareTo(Object objCompareTo) {
			AccountWithinRadius objAcc = (AccountWithinRadius) objCompareTo;
			if (objAcc.Distance > this.Distance)
				return -1;
			if (objAcc.Distance < this.Distance)
				return 1;
			return 0;
		}
	}
	
	private static Coordinate getSourceCoordinateFromRequest() {
		try {
			RestRequest objRequest = RestContext.Request;
			String sLongitudeValue = objRequest.params.get('longitude');
			String sLatitudeValue = objRequest.params.get('latitude');
			if (sLongitudeValue != null && sLatitudeValue != null)
			{
				Double dLongitude = Double.valueOf(sLongitudeValue); 
				Double dLatitude = Double.valueOf(sLatitudeValue);
				return new Coordinate(dLongitude, dLatitude);
			}
			return null;
		}
		catch(TypeException e) {
			System.debug(e.getTypeName() + ': ' + e.getMessage());
			return null;
		}
	}
	
	@TestVisible 
	private static Integer calcDistanceBetweenLocationAndAccount(Coordinate objSourceCoordinate, Coordinate objDestinationCoordinate) {
	 
		Double dRadiusOfEarth = 6371; // km
		Double dPi = 3.141592653589793;
		
		// convert to radian
		Double dDifferenceLongitude  = (dPi/180) * (objDestinationCoordinate.dLongitude - objSourceCoordinate.dLongitude);
		Double dDifferenceLatitude = (dPi/180) * (objDestinationCoordinate.dLatitude - objSourceCoordinate.dLatitude); 
		Double dSourceLatitude = (dPi/180) * objSourceCoordinate.dLatitude;
		Double dDestinationLatitude = (dPi/180) * objDestinationCoordinate.dLatitude;
		
		// haversine formula: http://www.movable-type.co.uk/scripts/latlong.html
		Double dA = Math.sin(dDifferenceLatitude/2) * Math.sin(dDifferenceLatitude/2) +
		        Math.sin(dDifferenceLongitude/2) * Math.sin(dDifferenceLongitude/2) * Math.cos(dSourceLatitude) * Math.cos(dDestinationLatitude); 
		Double dC = 2 * Math.atan2(Math.sqrt(dA), Math.sqrt(1-dA)); 
		Double dDistance = dRadiusOfEarth * dC;
		
		return Integer.valueOf(dDistance * 1000); // distance in m instead of km		
	}
		
	private static Double tryGetDoubleValueOfSalesAppSettingByName(String sSalesAppSettingName) {
		try {
			SalesAppSettings__c objSalesAppSetting = SalesAppSettings__c.getValues(sSalesAppSettingName);
			return Double.ValueOf(objSalesAppSetting.Value__c);
		} catch(TypeException e) {
			throw e;
		}
	}
	
	private static void cutList(List<AccountWithinRadius> listAccs, Integer iNumberOfElems) {
		for(Integer i = listAccs.size(); i > iNumberOfElems; i--)
			listAccs.remove(i);
	}
	
	private static List<AccountWithinRadius> getSubList(List<AccountWithinRadius> listAccs, Integer iNumberOfElems) {
		List<AccountWithinRadius> subList = new List<AccountWithinRadius>();
		for (Integer i = 0; (i < iNumberOfElems) && (i < listAccs.size()); i++ )
			subList.add(listAccs.get(i));
		return subList;
	}

		
	@HttpGet
    global static void doGet() {
		Datetime requestStart = System.now();
		Boolean requestSuccess = true;
		
		Double dRadius;
		Double dMaxNumberOfAccounts;
		Coordinate objSourceCoordinate;
		List<AccountWithinRadius> listAccountsWithinRadius = new List<AccountWithinRadius>();
		AccountWithinRadius objAccountWithinRadius = new AccountWithinRadius();
		
		objSourceCoordinate = getSourceCoordinateFromRequest();		
		if (objSourceCoordinate == null) {
			objAccountWithinRadius.addError('Source coordinate is not valid.');
			listAccountsWithinRadius.add(objAccountWithinRadius);
			RestContext.response.addHeader('Content-Type', 'application/json');
	        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(listAccountsWithinRadius));
			RestServiceUtilities.insertNewLog(requestStart, true);
			return;
		}
		
		Coordinate objDestinationCoordinate = new Coordinate();
				
		try {
			dRadius = tryGetDoubleValueOfSalesAppSettingByName( sSalesAppSetting_ProximitySearchRadiusM_Name );
			dMaxNumberOfAccounts = tryGetDoubleValueOfSalesAppSettingByName( sSalesAppSetting_ProximitySearchMaxResults_Name );
		}
		catch (TypeException e) {
			System.debug(e.getMessage());
			objAccountWithinRadius.addError(e.getMessage());
			listAccountsWithinRadius.add(objAccountWithinRadius);
			RestContext.response.addHeader('Content-Type', 'application/json');
	        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(listAccountsWithinRadius));
			requestSuccess = false;
			RestServiceUtilities.insertNewLog(requestStart, requestSuccess);
			return;
		}        
        
		Double dMaxLat = objSourceCoordinate.dLatitude + dOneKM / 1000.0 * dRadius + dOneKM;
		Double dMaxLong = objSourceCoordinate.dLongitude + dOneKM / 1000.0 * dRadius + dOneKM;		
		
		Double dMinLat = objSourceCoordinate.dLatitude - dOneKM / 1000.0 * dRadius - dOneKM;
		Double dMinLong = objSourceCoordinate.dLongitude - dOneKM / 1000.0 * dRadius - dOneKM;
		
        for (Account a: [SELECT Name, AccountNumber, BillingStreet, BillingHouseNo__c, BillingCity, BillingPostalCode, Phone, Name2__c, Subtradechannel__c,  GeoX__c, GeoY__c FROM Account
									WHERE GeoX__c < : dMaxLong AND GeoX__c > : dMinLong AND
										  GeoY__c < : dMaxLat AND GeoY__c > : dMinLat AND
										  IsDeleted =: false LIMIT 50000]) {
														
			objDestinationCoordinate.dLongitude = a.GeoX__c;
			objDestinationCoordinate.dLatitude = a.GeoY__c;
			Integer iDistance = calcDistanceBetweenLocationAndAccount(objSourceCoordinate, objDestinationCoordinate);
			if (iDistance <= dRadius) {
				AccountWithinRadius objAcc = new AccountWithinRadius(a, iDistance);
				listAccountsWithinRadius.add(objAcc);
			}		
		}
		
        if (listAccountsWithinRadius.size() < 1) {
			objAccountWithinRadius.addError('Found ' + listAccountsWithinRadius.size() + ' Accounts for this position ' + objSourceCoordinate.toString() + '.');
			listAccountsWithinRadius.add(objAccountWithinRadius);
			RestContext.response.addHeader('Content-Type', 'application/json');
	        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(listAccountsWithinRadius));
			RestServiceUtilities.insertNewLog(requestStart, requestSuccess);
			return;
		}
		
		// sorts by Distance
		listAccountsWithinRadius.sort();
		RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(getSubList(listAccountsWithinRadius, (Integer)dMaxNumberOfAccounts)));
		RestServiceUtilities.insertNewLog(requestStart, requestSuccess);
    }    
}
