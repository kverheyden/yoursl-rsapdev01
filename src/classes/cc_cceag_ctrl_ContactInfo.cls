global with sharing class cc_cceag_ctrl_ContactInfo {

    private static final String[] recipients = new String[] {
        ecomSettings__c.getInstance('editContactDataEmailRecipient').value__c            
    };

    private static final String SUBJECT = 'Anfrage Kontaktdatenänderung Ecom für Kunde ';

    private static final String NOT_ENTERED = 'n/a';
    
    private static final String CHANGED_MARKER = '** ';
    
    private static final String NOT_CHANGED_MARKER = '';

    private static final String BODY_TEMPLATE =
'Anfrage zur Änderung von Kontaktinformationen\n' +
'==============================================\n' +
'\n' +
'User-ID: {userEmail}\n' +
'Kundennummer: {accountNumber}\n'+
'\n' +
'Neue Daten\n' +
'===========\n' +
'\n' +
'Kontaktadresse\n' +
'---------------\n' +
'{contactNameChanged}\tName: {contactName}\n' +
'{contactStreetChanged}\tStraße: {contactStreet}\n' +
'{contactStreetNumberChanged}\tNummer: {contactStreetNumber}\n' +
'{contactPostalCodeChanged}\tPLZ: {contactPostalCode}\n' +
'{contactCityChanged}\tOrt: {contactCity}\n' +
'{contactCountryChanged}\tLand: {contactCountry}\n' +
'{contactPhoneChanged}\tTelefon: {contactPhone}\n' +
'{contactMobileChanged}\tMobilfunknr.: {contactMobile}\n' +
'\n' +
'Rechnungsadresse ({billingAccountNumber})\n' +
'-----------------\n' +
'{billingNameChanged}\tName: {billingName}\n' +
'{billingStreetChanged}\tStraße: {billingStreet}\n' +
'{billingStreetNumberChanged}\tNummer: {billingStreetNumber}\n' +
'{billingPostalCodeChanged}\tPLZ: {billingPostalCode}\n' +
'{billingCityChanged}\tOrt: {billingCity}\n' +
'{billingCountryChanged}\tLand: {billingCountry}\n' +
'\n';


    @RemoteAction
    global static ccrz.cc_RemoteActionResult loadUserData(ccrz.cc_RemoteActionContext ctx) {

        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
        result.inputContext = ctx;
        result.success = false;

        try {
            
            String userId = processUserId(ctx.portalUserId);

            result.data = loadAccountData(userId);
            result.success = true;

        } catch (Exception e) {

            System.debug(System.LoggingLevel.ERROR, e.getMessage());
            System.debug(System.LoggingLevel.ERROR, e.getStackTraceString());
            ccrz.cc_bean_Message msg = new ccrz.cc_bean_Message();
            msg.type = ccrz.cc_bean_Message.MessageType.CUSTOM;
            msg.classToAppend = 'messagingSection-Error';
            msg.message = e.getMessage() + ' => ' + e.getStackTraceString();
            msg.severity = ccrz.cc_bean_Message.MessageSeverity.ERROR;
            result.messages.add(msg);
        }

        return result;
    }

    public Map<String, String> AccountData {
    	//following code breaks CSR flow
        //get { return loadAccountData(UserInfo.getUserId()); }
        //use following code to fix CSR flow issue
        get { return loadAccountData(cc_cceag_ctrl_ContactInfo.getFlowUser()); }
    }
    
    public static final string CSR_PORTAL_USER_ID='portalUser';
    private static string getFlowUser() {
    	string usr = UserInfo.getUserId();
		if(Apexpages.currentPage() != null) {
			string portalUserId=Apexpages.currentPage().getParameters().get(CSR_PORTAL_USER_ID);
			if(string.isNotBlank(portalUserId)) {
				usr = portalUserId;
				//XSS prevention
				usr = usr.escapeHtml4();
			}
		}
		return usr;
	}

    private static Map<String, String> loadAccountData (String userId)
    {
        String accountId = cc_cceag_dao_Account.fetchAccountId(userId);
        system.debug('accountId=' + accountId);
        List<Account> accounts = [
            SELECT 
            	AccountNumber,
                Name,
                Phone2__c,
                Mobile__c,
                BillingStreet,
                BillingCity,
                BillingPostalCode,
                BillingCountry
            FROM
                Account
            WHERE
                id =: accountId
        ];
        
        Account account = null;
        Account invoiceAccount = null;
        if (accounts.size() > 0) {
			account = accounts.get(0);
	        cc_cceag_dao_Account.ensureFieldsForAccount(account, 'AccountNumber,Name,BillingStreet,BillingCity,BillingPostalCode,BillingCountry,Phone2__c,Mobile__c');        
            invoiceAccount = cc_cceag_dao_Account.fetchInvoiceRecipientAccount(account);
        }        
        
       	System.debug('#TR account: ');
        System.debug(account);
        System.debug('#TR invoiceAccount: ');
        System.debug(invoiceAccount);
        
        boolean noInvoiceFound = false;
        if (invoiceAccount==null) {
            noInvoiceFound = true;
            invoiceAccount = account;
        }
        
        if (invoiceAccount!=null) {
	        cc_cceag_dao_Account.ensureFieldsForAccount(invoiceAccount, 'AccountNumber,Name,BillingStreet,BillingPostalCode,BillingCity,BillingCountry');            
        }
        
        Map<String, String> contactData = new Map<String, String>();
        contactData.put('accountNumber',account!=null && account.AccountNumber!=null ? account.AccountNumber : '');
        contactData.put('contactName', account!=null && account.Name!=null ? account.Name : '');
        contactData.put('contactStreet', account!=null && account.BillingStreet!=null ? account.BillingStreet : '');
        contactData.put('contactStreetNumber', '');
        contactData.put('contactCity', account!=null && account.BillingCity!=null ? account.BillingCity : '');
        contactData.put('contactPostalCode', account!=null && account.BillingPostalCode!=null ? account.BillingPostalCode : '');
        contactData.put('contactCountry', account!=null && account.BillingCountry!=null ? account.BillingCountry : '');
        contactData.put('contactPhone', account!=null && account.Phone2__c!=null ? account.Phone2__c : '');
        contactData.put('contactMobile', account!=null && account.Mobile__c!=null ? account.Mobile__c : '');
        
        contactData.put('billingAccountNumber', invoiceAccount!=null && invoiceAccount.AccountNumber!=null ? invoiceAccount.AccountNumber : '');        
        contactData.put('billingName',  invoiceAccount!=null && invoiceAccount.Name!=null ? (invoiceAccount.Name)  : '');
        contactData.put('billingStreet',  invoiceAccount!=null && invoiceAccount.BillingStreet!=null ? invoiceAccount.BillingStreet : '');
        contactData.put('billingStreetNumber', '');
        contactData.put('billingPostalCode',  invoiceAccount!=null && invoiceAccount.BillingPostalCode!=null ? invoiceAccount.BillingPostalCode : '');
        contactData.put('billingCity',  invoiceAccount!=null && invoiceAccount.BillingCity!=null ? invoiceAccount.BillingCity : '');
        contactData.put('billingCountry',  invoiceAccount!=null && invoiceAccount.BillingCountry!=null ? invoiceAccount.BillingCountry : '');
        
        return contactData;
    }


    @RemoteAction
    global static ccrz.cc_RemoteActionResult saveData(ccrz.cc_RemoteActionContext ctx, Map<String, String> newData) {

        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
        result.inputContext = ctx;
        result.success = false;

        try {

            String userId = processUserId(ctx.portalUserId);

            sendEmail(userId, newData);

            result.data = newData;
            result.success = true;

        } catch (Exception e) {

            System.debug(System.LoggingLevel.ERROR, e.getMessage());
            System.debug(System.LoggingLevel.ERROR, e.getStackTraceString());
            ccrz.cc_bean_Message msg = new ccrz.cc_bean_Message();
            msg.type = ccrz.cc_bean_Message.MessageType.CUSTOM;
            msg.classToAppend = 'messagingSection-Error';
            msg.message = e.getMessage() + ' => ' + e.getStackTraceString();
            msg.severity = ccrz.cc_bean_Message.MessageSeverity.ERROR;
            result.messages.add(msg);
        }

        return result;
    }


    private static void sendEmail (String userId, Map<String, String> newData)
    {
        Map<String, String> existingData = loadAccountData(userId);

        cc_cceag_Mailer.sendEmail(recipients, SUBJECT + existingData.get('accountNumber'), buildEmailBody(userId, existingData, newData));
    }


    private static String buildEmailBody (String userId, Map<String, String> existingData, Map<String, String> newData) {

        User u = [SELECT Email FROM User WHERE id=:userId LIMIT 1];
        
        Map<String, String> bodyData = new Map<String, String>();
        bodyData.put('userId', userId);
        bodyData.put('userEmail', u.Email);        
        
        set<String> changedFields = getChangedFields(existingData, newData);
       
        bodyData.put('accountNumber', existingData.get('accountNumber'));
        
        bodyData.put('contactName', newData.get('contactName'));
        bodyData.put('contactStreet', newData.get('contactStreet'));
        bodyData.put('contactStreetNumber', newData.get('contactStreetNumber'));
        bodyData.put('contactPostalCode', newData.get('contactPostalCode'));
        bodyData.put('contactCity', newData.get('contactCity'));
        bodyData.put('contactCountry', newData.get('contactCountry'));
        bodyData.put('contactPhone', newData.get('contactPhone'));
        bodyData.put('contactMobile', newData.get('contactMobile'));

        bodyData.put('billingAccountNumber', existingData.get('billingAccountNumber'));
        bodyData.put('billingName', newData.get('billingName'));
        bodyData.put('billingStreet', newData.get('billingStreet'));
        bodyData.put('billingStreetNumber', newData.get('billingStreetNumber'));
        bodyData.put('billingPostalCode', newData.get('billingPostalCode'));
        bodyData.put('billingCity', newData.get('billingCity'));
        bodyData.put('billingCountry', newData.get('billingCountry'));

        String body = BODY_TEMPLATE;

        for (String key : bodyData.keySet()) {
            
            String replacement = bodyData.get(key);                        
            String oldValue = existingData.get(key);
            
            if (changedFields.contains(key)) {
                replacement = oldValue + ' --> ' + replacement;
            }
            
            body = body.replace('{' + key + '}', replacement != null ? replacement : NOT_ENTERED);
            
            String keyChanged = key + 'Changed';
            body = body.replace('{' + keyChanged + '}', changedFields.contains(key) ? CHANGED_MARKER : NOT_CHANGED_MARKER);
        }

        return body;
    }
    
    
    private static set<String> getChangedFields (Map<String, String> existingData, Map<String, String> newData)
    {
        set<String> changedFields = new set<String>();
        
        for (String key : existingData.keySet()) {
            if (key=='accountNumber') continue;
            if (key=='billingAccountNumber') continue;
            if (existingData.get(key) != newData.get(key)) {
                changedFields.add(key);
            }
        }
        
        return changedFields;
    }


    private static String processUserId(String portalUserId) {

        if (String.isBlank(portalUserId)) {
            return UserInfo.getUserId();
        }

        return portalUserId;
    }


    @RemoteAction
    global static ccrz.cc_RemoteActionResult fetchAccountNumber(ccrz.cc_RemoteActionContext ctx, String acctSfid) {
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
        result.inputContext = ctx;
        result.success = false;
        try {
            Account acct = [SELECT AccountNumber FROM Account WHERE Id =: acctSfid];
            result.data = acct.AccountNumber;
            result.success = true;
        } catch (Exception e) {
            System.debug(System.LoggingLevel.ERROR, e.getMessage());
            System.debug(System.LoggingLevel.ERROR, e.getStackTraceString());
            ccrz.cc_bean_Message msg = new ccrz.cc_bean_Message();
            msg.type = ccrz.cc_bean_Message.MessageType.CUSTOM;
            msg.classToAppend = 'messagingSection-Error';
            msg.message = e.getMessage() + ' => ' + e.getStackTraceString();
            msg.severity = ccrz.cc_bean_Message.MessageSeverity.ERROR;
            result.messages.add(msg);
        }
        return result;
    }

}
