/*
 * @(#)SCOrderPageControllerTest.cls SCCloud
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author Alexei Geiger <ageiger@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCOrderPageControllerTest
{
    static testMethod void testOnGetRoleAccountIds()
    {   
    
        SCHelperTestClass.createInstalledBase(true);
        SCHelperTestClass.createOrderItem(true);
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.orderRolePerson(SCHelperTestClass.order, SCHelperTestClass.account, '50301', true);  
        
        SCOrderPageController pageController = new SCOrderPageController(SCHelperTestClass.account.Id);
        String accountIds = pageController.getRoleAccountIds();
        String helperIds = SCHelperTestClass.account.Id + ',' + SCHelperTestClass.account.Id + ',' + SCHelperTestClass.account.Id + ',' + SCHelperTestClass.account.Id;
        System.assertEquals( accountIds, helperIds );
    }

}
