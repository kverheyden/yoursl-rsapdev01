/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class cc_cceag_ctrl_test_OrderWidget {
	static void setupTestData() {
        ccrz__E_ContactAddr__c billTo = new ccrz__E_ContactAddr__c(
			ccrz__AddressFirstline__c='100 Pine Street', ccrz__City__c='SmallTown', ccrz__DaytimePhone__c='(847) 555-1212',
			ccrz__FirstName__c='Test', ccrz__HomePhone__c='(847) 555-1212', ccrz__LastName__c='User',
			ccrz__PostalCode__c='60601', ccrz__State__c='Idaho', ccrz__StateISOCode__c='ID',
			ccrz__CountryISOCode__c='USA');
		ccrz__E_ContactAddr__c shipTo = new ccrz__E_ContactAddr__c(
			ccrz__AddressFirstline__c='100 Pine Street', ccrz__City__c='SmallTown', ccrz__DaytimePhone__c='(847) 555-1212',
			ccrz__FirstName__c='Craig', ccrz__HomePhone__c='(847) 555-1212', ccrz__LastName__c='Traxler',
			ccrz__PostalCode__c='60601', ccrz__State__c='Idaho', ccrz__StateISOCode__c='ID',
			ccrz__CountryISOCode__c='USA');
		insert new List<ccrz__E_ContactAddr__c> {billTo, shipTo};
		String testName = 'testcart';
		String testEmail = 'myemail@test.com';
		String testFirstName = 'Test';
		String testLastName = 'User';
		String testCompanyName = 'MyCo';
		String testPhone = '(800) 555-1111';
		String testMobilePhone = '(800) 555-2222';
		String testPaymentMethod = 'PO';
		String testPONumber = '12345';
		
		ccrz__E_Cart__c cart = new ccrz__E_Cart__c(
			ccrz__Name__c = testName,
			//ccrz__Contact__c = testUser.ContactId,
			ccrz__BillTo__c = billTo.Id,
			ccrz__ShipTo__c = shipTo.Id,
			ccrz__BuyerEmail__c = testEmail,
			ccrz__BuyerFirstName__c = testFirstName,
			ccrz__BuyerLastName__c = testLastName,
			ccrz__BuyerCompanyName__c = testCompanyName,
			ccrz__BuyerPhone__c = testPhone,
			ccrz__BuyerMobilePhone__c = testMobilePhone,
			ccrz__PaymentMethod__c = testPaymentMethod,
			ccrz__PONumber__c = testPONumber,
			ccrz__SessionID__c = 'test session',
			ccrz__storefront__c='DefaultStore',
			ccrz__RequestDate__c = Date.today(),
			ccrz__ActiveCart__c = true,
			ccrz__CartStatus__c = 'Open',
			ccrz__CartType__c = 'Cart',
			ccrz__CartId__c = 'MyTestCart0001'
		);
		
		insert cart;
		
		
		List<ccrz__E_Product__c> ps = new List<ccrz__E_Product__c>{
			new ccrz__E_Product__c(Name='Product1', ccrz__Sku__c='sku1', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12),
			new ccrz__E_Product__c(Name='Product2', ccrz__Sku__c='sku2', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12),
			new ccrz__E_Product__c(Name='Product3', ccrz__Sku__c='sku3', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12),
			new ccrz__E_Product__c(Name='Product4', ccrz__Sku__c='sku4', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12),
			
			new ccrz__E_Product__c(Name='Product1_1', ccrz__Sku__c='sku1_1', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12),
			new ccrz__E_Product__c(Name='Product1_2', ccrz__Sku__c='sku1_2', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12),
			new ccrz__E_Product__c(Name='Product1_3', ccrz__Sku__c='sku1_3', ccrz__ProductStatus__c='Released', ccrz__shippingWeight__c=20.0, NumberOfSalesUnitsPerPallet__c=12)
			
		};
		insert ps;
		
		List<ccrz__E_RelatedProduct__c> related = new List<ccrz__E_RelatedProduct__c> {
			new ccrz__E_RelatedProduct__c(ccrz__Product__r=new ccrz__e_product__c(ccrz__sku__c='sku1'), ccrz__RelatedProduct__r = new ccrz__e_product__c(ccrz__sku__c='sku1_1'), ccrz__RelatedProductType__c='Related' ),
			new ccrz__E_RelatedProduct__c(ccrz__Product__r=new ccrz__e_product__c(ccrz__sku__c='sku1'), ccrz__RelatedProduct__r = new ccrz__e_product__c(ccrz__sku__c='sku1_2'), ccrz__RelatedProductType__c='Related' ),
			new ccrz__E_RelatedProduct__c(ccrz__Product__r=new ccrz__e_product__c(ccrz__sku__c='sku1'), ccrz__RelatedProduct__r = new ccrz__e_product__c(ccrz__sku__c='sku1_3'), ccrz__RelatedProductType__c='Related' )};
		insert related;
		
		List<ccrz__E_CartItem__c> cartItems = new List<ccrz__E_CartItem__c> {
			new ccrz__E_CartItem__c(ccrz__Quantity__c=1, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku1'), ccrz__cart__c=cart.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0, ccrz__cartItemType__c='Major'),
			new ccrz__E_CartItem__c(ccrz__Quantity__c=1, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku2'), ccrz__cart__c=cart.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0, ccrz__cartItemType__c='Minor'),
			new ccrz__E_CartItem__c(ccrz__Quantity__c=1, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku3'), ccrz__cart__c=cart.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0, ccrz__cartItemType__c='Major'),
			new ccrz__E_CartItem__c(ccrz__Quantity__c=1, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku4'), ccrz__cart__c=cart.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0, ccrz__cartItemType__c='Minor')
		};
		insert cartItems;
		
		ccrz__E_order__c o = new ccrz__E_Order__c(
			ccrz__OrderId__c='MyTestOrder',
			ccrz__TaxAmount__c=1.99, 
			ccrz__ShipAmount__c=2.99, 
			ccrz__CurrencyIsoCode__c='USD',
			ccrz__OriginatedCart__c = cart.id,
			ccrz__OrderStatus__c = 'Order Submitted',
			ccrz__BillTo__c = billTo.Id,
			ccrz__ShipTo__c = shipTo.Id,
			ccrz__BuyerEmail__c = testEmail,
			ccrz__BuyerFirstName__c = testFirstName,
			ccrz__BuyerLastName__c = testLastName,
			ccrz__BuyerCompanyName__c = testCompanyName,
			ccrz__BuyerPhone__c = testPhone,
			ccrz__BuyerMobilePhone__c = testMobilePhone,
			ccrz__PaymentMethod__c = testPaymentMethod,
			ccrz__PONumber__c = testPONumber,
			ccrz__storefront__c='DefaultStore',
			ccrz__RequestDate__c = Date.today(),
			CCEAGDeliveryTime__c = Date.today().addDays(30).format(),
			CCEAGNewsletter__c = false
		);
		insert o;
		List<ccrz__E_OrderItem__c> orderItems = new List<ccrz__E_OrderItem__c> {
			new ccrz__E_OrderItem__c(ccrz__Quantity__c=1, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku1'), ccrz__order__c=o.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0),
			new ccrz__E_OrderItem__c(ccrz__Quantity__c=1, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku2'), ccrz__order__c=o.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0),
			new ccrz__E_OrderItem__c(ccrz__Quantity__c=1, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku3'), ccrz__order__c=o.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0),
			new ccrz__E_OrderItem__c(ccrz__Quantity__c=1, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku4'), ccrz__order__c=o.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0),
			new ccrz__E_OrderItem__c(ccrz__Quantity__c=1, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku1_1'), ccrz__order__c=o.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0),
			new ccrz__E_OrderItem__c(ccrz__Quantity__c=1, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku1_2'), ccrz__order__c=o.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0),
			new ccrz__E_OrderItem__c(ccrz__Quantity__c=1, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku1_3'), ccrz__order__c=o.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0)
		};
		insert orderItems;
    }
    
    static testMethod void testReorder() {
    	cc_cceag_ctrl_test_OrderWidget.setupTestData();
    	Test.startTest();
        cc_cceag_ctrl_OrderWidget o = new cc_cceag_ctrl_OrderWidget();
        ccrz__E_Cart__c cart = [select id, ccrz__EncryptedId__c from ccrz__E_Cart__c cart where ccrz__CartId__c = 'MyTestCart0001' limit 1];
        //system.assert(cc_cceag_ctrl_OrderWidget.cloneCart(cart.id) != null);
        
        ccrz__E_order__c order = [select id from ccrz__E_order__c cart where ccrz__OrderId__c='MyTestOrder' limit 1];
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        //test exception flow
        system.assertEquals(true, cc_cceag_ctrl_OrderWidget.reorder(ctx, order.id).success);
        	
        ctx.currentCartID = cart.ccrz__EncryptedId__c;
        ccrz.cc_RemoteActionResult result = cc_cceag_ctrl_OrderWidget.reorder(ctx, order.id);
        system.assert(o.getJsonOrderStatus() != null);
       
        Test.stopTest();
    }
    static testMethod void testFetchOrder() {
    	cc_cceag_ctrl_test_OrderWidget.setupTestData();
    	Test.startTest();
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        ccrz.cc_RemoteActionResult result = cc_cceag_ctrl_OrderWidget.fetchOrders(ctx, null);
        system.assertEquals(false, result.success);
        map<string, object> formData = new map<string, object> {'searchOrderStatus'=>'Order Submitted'};
        result = cc_cceag_ctrl_OrderWidget.fetchOrders(ctx, JSON.serialize(formData));
        system.assertEquals(true, result.success);
        Test.stopTest();
    }
}
