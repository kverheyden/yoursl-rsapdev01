/**
* @author		Austen Buennemann
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				a.buennemann@yoursl.de
*
* @description	Handler for SalesOrder__c callouts of CCWCSalesOrder
*
* @date			26.05.2014
*
*/

public with sharing class CCWCSalesOrderHandler {
	
	/**
	* Prepare single callout for CustomerCreate to SAP
	*
	* @param ibid 	SFDC Id
	* @param test	Testmode
	* 
	*/
	public void callout(String ibid, boolean test){
		List <SalesOrder__c> request =  [Select Id, Name, Account__r.ID2__c, Send_to_SAP__c, RequestedDeliveryDate__c, Status__c, Type__c From SalesOrder__c  Where Id =: ibid];
		CCWCSalesOrderHandler send = new CCWCSalesOrderHandler();
		String msg = send.sendCallout(request, null, test);
		if(msg != null){
			throw new CCWCSalesOrderException(msg);
		}
	}
	
	/**
	* Prepare Trigger callout forwarding for CustomerCreate to SAP
	*
	* @param newData 	Trigger.new Records
	* @param oldData	Trigger.old Records
	* @param test		Testmode
	* 
	*/
	public void forwardCallout(SalesOrder__c [] newData, SalesOrder__c [] oldData, boolean test){
		 	Map<Id, String> olddataMap = null;
		 	if(oldData != null){
		 		olddataMap = new Map<Id, String>();
			 	for(SalesOrder__c oldorder : oldData){
			 		olddataMap.put(oldorder.Id, oldorder.Status__c);
			 	}
		 	}
		 	CCWCSalesOrderHandler send = new CCWCSalesOrderHandler();
	    	send.sendCallout(newData, olddataMap, false);
	}
	
	/**
	* Prepare callouts for CustomerCreate to SAP with status value
	*
	* @param newRequests 	Sales Order Records
	* @param olddata		Map of Send To SAP status
	* @param test			Testmode
	* 
	*/
    public String sendCallout(SalesOrder__c [] newRequests, Map<Id, String> olddata, boolean test){   	
    	String  error = null;
    	Set<Id> idSet = new Set<Id>();
    	for(SalesOrder__c entry: newRequests){
    		idSet.add(entry.Id);
    	}
		Map<Id, SalesOrder__c> request = new Map<Id, SalesOrder__c>( [Select Id, Name, Account__r.ID2__c, Send_to_SAP__c, RequestedDeliveryDate__c, Status__c, Type__c From SalesOrder__c  Where Id =: idSet]);  
			
		
		List<SalesOrderItem__c>	lineitems = new List<SalesOrderItem__c>([SELECT Id, ExternalArticleNumber__c, Quantity__c,  ShipToCustomerNumber__c, SalesOrder__c 
																			FROM SalesOrderItem__c Where SalesOrder__c =: idSet]);
									
    	if(newRequests != null && !newRequests.isEmpty()){
	    	for(SalesOrder__c newRequest : newRequests){
	    		if(newRequest.Status__c == Label.CCWCSalesOrder_Status && (olddata == null || olddata.get(newRequest.Id) != Label.CCWCSalesOrder_Status) ){  
			    	String item = JSON.serialize(request.get(newRequest.Id));
			    	if(lineitems != null && !lineitems.isEmpty()){
			    		List<String> line = new List<String>();
			    		for(SalesOrderItem__c lineitem : lineitems){
			    			if(lineitem.SalesOrder__c == request.get(newRequest.Id).Id){
			    				if(lineitem.ShipToCustomerNumber__c != null){
			    					line.add(JSON.serialize(lineitem));
			    				}else{
			    					error = 'SAP Webservice: The Partner Number ShipToCustomerNumber__c of the record ' + lineitem.Id + ' is a required field!';
			    					throw new CCWCSalesOrderException(error);
			    				}
			    			}
			    		}
			    		CCWCSalesOrder.futurecallout(newRequest.Id, test, item, line);			    					    		
			    	}else{
		    			error = 'SAP Webservice: The Sales Order ' + newRequest.Id + ' has no lines!';
		    			throw new CCWCSalesOrderException(error);	
			    	}
	    		}	    		
    		}
    	}
    	return error;
    }
    
	/**
	* Prepare callouts for CustomerCreate to SAP with checkbox value
	*
	* @param newRequests 	Sales Order Records
	* @param olddata		Map of Send To SAP status
	* @param test			Testmode
	* 
	*/
    public String sendCalloutbyCheckbox(SalesOrder__c [] newRequests, boolean test){   	
    	String  error = null;
    	Set<Id> idSet = new Set<Id>();
    	for(SalesOrder__c entry: newRequests){
    		idSet.add(entry.Id);
    	}
		Map<Id, SalesOrder__c> request = new Map<Id, SalesOrder__c>( [Select Id, Name, Account__r.ID2__c, Send_to_SAP__c, RequestedDeliveryDate__c, Status__c, Type__c From SalesOrder__c  Where Id =: idSet]);  
			
		
		List<SalesOrderItem__c>	lineitems = new List<SalesOrderItem__c>([SELECT Id, ExternalArticleNumber__c, Quantity__c,  ShipToCustomerNumber__c, SalesOrder__c 
																			FROM SalesOrderItem__c Where SalesOrder__c =: idSet]);
									
    	if(newRequests != null && !newRequests.isEmpty()){
	    	for(SalesOrder__c newRequest : newRequests){
	    		if(newRequest.Send_to_SAP__c){  
			    	String item = JSON.serialize(request.get(newRequest.Id));
			    	if(lineitems != null && !lineitems.isEmpty()){
			    		List<String> line = new List<String>();
			    		for(SalesOrderItem__c lineitem : lineitems){
			    			if(lineitem.SalesOrder__c == request.get(newRequest.Id).Id){
			    				if(lineitem.ShipToCustomerNumber__c != null){
			    					line.add(JSON.serialize(lineitem));
			    				}else{
			    					error = 'SAP Webservice: The Partner Number ShipToCustomerNumber__c of the record ' + lineitem.Id + ' is a required field!';
			    					throw new CCWCSalesOrderException(error);
			    				}
			    			}
			    		}
			    		CCWCSalesOrder.futurecallout(newRequest.Id, test, item, line);			    					    		
			    	}else{
		    			error = 'SAP Webservice: The Sales Order ' + newRequest.Id + ' has no lines!';
		    			throw new CCWCSalesOrderException(error);	
			    	}
	    		}	    		
    		}
    	}
    	return error;
    }
    
    public class CCWCSalesOrderException extends Exception {}
}
