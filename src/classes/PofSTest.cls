/**********************************************************************

Name: PofSTest

======================================================

Purpose: 

This is the test class for several PofS* classes

======================================================

History 
 
------- 

Date 			AUTHOR 				DETAIL

11/08/2013 		Oliver Preuschl 	INITIAL DEVELOPMENT

***********************************************************************/

@isTest
private class PofSTest {
    
    static List< sObject > GL_FolderSettings;
    static List< sObject > GL_ClickAreaTypes;
    static List< sObject > GL_Assortments;
    
    static testMethod void testInsertManyPofSWithMatchingTimeFrameAndSubtradechannel() {
    	List<PictureOfSuccess__c> listPofS = new List<PictureOfSuccess__c>();
    	List<PictureOfSuccess__c> listPofS2 = new List<PictureOfSuccess__c>();
    	for(Integer i = 0; i < 20; i++) {
	    	PictureOfSuccess__c pofs = new PictureOfSuccess__c(
				Bezeichnung__c					=	'Name' + i,
				ValidFrom__c					=	Date.today(),
				ValidUntil__c					=	Date.today(),
				RedWeightingActivation__c		=	25,
				RedWeightingArea__c				=	25,
				RedWeightingAssortment__c		=	25,
				RedWeightingEquipment__c		=	25,
                Active__c						=	true,
				Subtradechannel__c				=	Math.mod(i, 2) == 0 ? '108;121' : '121'
			);		
			if (Math.mod(i, 2) == 0)
				listPofS.add(pofs);
			else
				listPofS2.add(pofs);
    	}
		
		Test.startTest();
		try {
			insert listPofS;
			insert listPofS2;
		} catch (Exception e) {			
			Boolean expectedExceptionThrown =  e.getMessage().contains(Label.fsfa_pofs_date_colision_pre) ? true : false;	
			System.AssertEquals(expectedExceptionThrown, true);
		}
		
		Test.stopTest();
    }
    
    static testMethod void testInsertInvalidPofS() {
    	List<PictureOfSuccess__c> listInvalidPofS = new List<PictureOfSuccess__c>();
    	
    	PictureOfSuccess__c invalidPofS = new PictureOfSuccess__c(
			Bezeichnung__c					=	'Name',
			ValidFrom__c					=	Date.today(),
			ValidUntil__c					=	Date.today() - 1,
			RedWeightingActivation__c		=	25,
			RedWeightingArea__c				=	25,
			RedWeightingAssortment__c		=	25,
			RedWeightingEquipment__c		=	25,
			Subtradechannel__c				=	'108',
            Active__c						=	true
		);		
		listInvalidPofS.add(invalidPofS);
		
		Test.startTest();
		try {
			insert listInvalidPofS;
		} catch(Exception e) {			
			Boolean expectedExceptionThrown =  e.getMessage().contains(Label.fsfa_pofs_start_end_colision) ? true : false;	
			System.AssertEquals(expectedExceptionThrown, true);
		}
		Test.stopTest();
    }
    
	static testMethod void myUnitTest() {
		//Create Custom Settings
        createCustomSettings();
        
        Test.startTest();
        
        //Create PofS
		
		PictureOfSuccess__c LO_PofS = new PictureOfSuccess__c(
			Bezeichnung__c					=	'Name',
			ValidFrom__c					=	Date.today(),
			ValidUntil__c					=	Date.today(),
			RedWeightingActivation__c		=	25,
			RedWeightingArea__c				=	25,
			RedWeightingAssortment__c		=	25,
			RedWeightingEquipment__c		=	25,
			Subtradechannel__c				=	'SubtradeChannel'
		);
		
		insert( LO_PofS );
                
        //Check PofS
        List< PictureOfSuccess__c > LL_TestPofSs = [ SELECT Id FROM PictureOfSuccess__c WHERE Id =: LO_PofS.Id ];
        System.assertEquals( 1, LL_TestPofSs.size() );
		
        //Create PofS Controller
        ApexPages.StandardController LO_StandardController = new ApexPages.StandardController( LO_PofS );
        PofSController LO_PofSController = new PofSController( LO_StandardController );
        LO_PofSController.Dummy();
        LO_PofSController.refreshBackgrounds();
        
        //Getter/Setter
        LO_PofSController.setGO_DocumentController( LO_PofSController.getGO_DocumentController() );
        LO_PofSController.setGV_ImageId( LO_PofSController.getGV_ImageId() );
        LO_PofSController.setGV_NewImageName( LO_PofSController.getGV_NewImageName() );
        LO_PofSController.setGO_Image( LO_PofSController.getGO_Image() );
        LO_PofSController.setGO_PofSSettings( LO_PofSController.getGO_PofSSettings() );
		LO_PofSController.getGO_PofSDeviceModels();
        
        system.assertNotEquals(null, LO_PofSController.getGO_PofSSettings());
        LO_PofSController.toggleActivation();
        LO_PofSController.getImageWidth();
        LO_PofSController.getImageHeight();
        
            
        //Init PofS
		PofSImageComponentController LO_ImageComponentController = new PofSImageComponentController();
		LO_ImageComponentController.GV_ImageId = LO_PofS.Id;
		LO_ImageComponentController.init();
		
        //Create new ClickArea
		LO_ImageComponentController.drawNewArea();
		LO_ImageComponentController.GO_ImageArea.Type__c = 'Area';
		LO_ImageComponentController.GO_ImageArea.Name = 'Name';
		
        createNewQuestions(LO_ImageComponentController);

        //Change Question Order
        LO_ImageComponentController.GV_OldClickAreaQuestionRowNumber = 1;
        LO_ImageComponentController.GV_NewClickAreaQuestionRowNumber = 2;
        LO_ImageComponentController.changeQuestionOrder();
        
        //Save Area
		LO_ImageComponentController.saveArea();

        //Check ClickArea
        LL_TestPofSs = [ SELECT Id, ( SELECT Id FROM ClickAreas__r ) FROM PictureOfSuccess__c WHERE Id =: LO_PofS.Id ];
        System.assertEquals( 1, LL_TestPofSs.get( 0 ).ClickAreas__r.size() );

        //Check Questions
        List< PofSClickarea__c > LL_TestClickAreas = [ SELECT Id, ( SELECT Id FROM RED_Survey_Questions__r ) FROM PofSClickarea__c WHERE Id =: LL_TestPofSs.get( 0 ).ClickAreas__r.get( 0 ).Id ];
        System.assertEquals( 3, LL_TestClickAreas.get( 0 ).RED_Survey_Questions__r.size() );

        //Choose Area
        LO_ImageComponentController.chooseArea();
        
        //Move Area
        LO_ImageComponentController.moveArea();
        
        //Cancel Area
        LO_ImageComponentController.cancelArea();
        
        //Choose Area
        LO_ImageComponentController.chooseArea();
        
        //Delete Question
        LO_ImageComponentController.GV_ClickAreaQuestionRowNumber = 2;
        LO_ImageComponentController.deleteQuestion();

        //Check Questions
        LL_TestClickAreas = [ SELECT Id, ( SELECT Id FROM RED_Survey_Questions__r ) FROM PofSClickarea__c WHERE Id =: LL_TestPofSs.get( 0 ).ClickAreas__r.get( 0 ).Id ];
        System.assertEquals( 2, LL_TestClickAreas.get( 0 ).RED_Survey_Questions__r.size() );
                
        //Change ClickArea Type
        LO_ImageComponentController.GO_ImageArea.Type__c = 'Assortment';
        LO_ImageComponentController.changeClickAreaType();
        
        //Create Assortments
        LO_ImageComponentController.GV_AssortmentProductId = GL_Assortments.get( 0 ).Id;
        LO_ImageComponentController.GV_AssortmentRowNumber = 1;
        LO_ImageComponentController.newAssortment();
        LO_ImageComponentController.newAssortment();
        LO_ImageComponentController.newAssortment();
		
        //Change Assortment Order
        LO_ImageComponentController.GV_OldAssortmentRowNumber = 1;
        LO_ImageComponentController.GV_NewAssortmentRowNumber = 2;
        LO_ImageComponentController.changeAssortmentOrder();

        //Save Area
		LO_ImageComponentController.saveArea();

        //Check Assortments
        LL_TestPofSs = [ SELECT Id, ( SELECT Id FROM Assortment__r ) FROM PictureOfSuccess__c WHERE Id =: LO_PofS.Id ];
        System.assertEquals( 3, LL_TestPofSs.get( 0 ).Assortment__r.size() );
        
        //Delete Assortment
        LO_ImageComponentController.GV_AssortmentRowNumber = 2;
        LO_ImageComponentController.deleteAssortment();
        
        //Getter/Setter
        LO_ImageComponentController.setGV_ImageId(LO_ImageComponentController.getGV_ImageId());
        LO_ImageComponentController.setGO_Image(LO_ImageComponentController.getGO_Image());
        LO_ImageComponentController.setGO_PofSSettings(LO_ImageComponentController.getGO_PofSSettings());
        LO_ImageComponentController.setGL_ClickAreas(LO_ImageComponentController.getGL_ClickAreas());
        LO_ImageComponentController.setGV_DrawNewArea(LO_ImageComponentController.getGV_DrawNewArea());
        LO_ImageComponentController.setGV_EditArea(LO_ImageComponentController.getGV_EditArea());
        LO_ImageComponentController.setGV_MoveArea(LO_ImageComponentController.getGV_MoveArea());
        LO_ImageComponentController.setGM_ClickAreaTypes(LO_ImageComponentController.getGM_ClickAreaTypes());
        LO_ImageComponentController.setGO_ImageArea(LO_ImageComponentController.getGO_ImageArea());
        LO_ImageComponentController.setGV_ImageAreaId(LO_ImageComponentController.getGV_ImageAreaId());
        LO_ImageComponentController.setGV_LastClickAreaType(LO_ImageComponentController.getGV_LastClickAreaType());
        LO_ImageComponentController.setGV_X(LO_ImageComponentController.getGV_X());
        LO_ImageComponentController.setGV_Y(LO_ImageComponentController.getGV_Y());
        LO_ImageComponentController.setGV_NewImageName(LO_ImageComponentController.getGV_NewImageName());
        LO_ImageComponentController.setGM_ClickAreaQuestions(LO_ImageComponentController.getGM_ClickAreaQuestions());
        LO_ImageComponentController.setGV_RemainingQuestions(LO_ImageComponentController.getGV_RemainingQuestions());
        LO_ImageComponentController.setGV_ClickAreaQuestionRowNumber(LO_ImageComponentController.getGV_ClickAreaQuestionRowNumber());
        LO_ImageComponentController.setGV_OldClickAreaQuestionRowNumber(LO_ImageComponentController.getGV_OldClickAreaQuestionRowNumber());
        LO_ImageComponentController.setGV_NewClickAreaQuestionRowNumber(LO_ImageComponentController.getGV_NewClickAreaQuestionRowNumber());
        LO_ImageComponentController.setGV_AssortmentsCount(LO_ImageComponentController.getGV_AssortmentsCount());
        LO_ImageComponentController.setGV_AssortmentRowNumber(LO_ImageComponentController.getGV_AssortmentRowNumber());
        LO_ImageComponentController.setGV_OldAssortmentRowNumber(LO_ImageComponentController.getGV_OldAssortmentRowNumber());
        LO_ImageComponentController.setGV_NewAssortmentRowNumber(LO_ImageComponentController.getGV_NewAssortmentRowNumber());
        LO_ImageComponentController.setGM_AssortmentProducts(LO_ImageComponentController.getGM_AssortmentProducts());
        LO_ImageComponentController.setGV_AssortmentProductId(LO_ImageComponentController.getGV_AssortmentProductId());
        LO_ImageComponentController.setGL_FilterFields(LO_ImageComponentController.getGL_FilterFields());
        LO_ImageComponentController.setGM_AssortmentProductFilters(LO_ImageComponentController.getGM_AssortmentProductFilters());
        LO_ImageComponentController.setGM_AssortmentProductFilterValues(LO_ImageComponentController.getGM_AssortmentProductFilterValues());
        LO_ImageComponentController.setGM_AssortmentProductFilterLabels(LO_ImageComponentController.getGM_AssortmentProductFilterLabels());
        LO_ImageComponentController.setGO_DocumentController(LO_ImageComponentController.getGO_DocumentController());
        
        LO_ImageComponentController.getGO_DocumentController().setGV_DocumentURL(LO_ImageComponentController.getGO_DocumentController().getGV_DocumentURL());
        LO_ImageComponentController.getGO_DocumentController().setGO_UploadFile(LO_ImageComponentController.getGO_DocumentController().getGO_UploadFile());
        LO_ImageComponentController.getGO_DocumentController().setGL_Backgrounds(LO_ImageComponentController.getGO_DocumentController().getGL_Backgrounds());
        LO_ImageComponentController.getGO_DocumentController().setGL_ClickAreaIcons(LO_ImageComponentController.getGO_DocumentController().getGL_ClickAreaIcons());
        LO_ImageComponentController.getGO_DocumentController().setGL_ClickAreaQuestions(LO_ImageComponentController.getGO_DocumentController().getGL_ClickAreaQuestions());
            
        Test.stopTest();
		
	}
    
    public static void createCustomSettings(){
        // Duc Nguyen Tien | Creating a custom settings for PofSSettings__c. Values taken from staging org directly
        PofSSettings__c setting = new PofSSettings__c();
        setting.Name = 'Default';
        setting.ImageHeightValidation__c = 2.034;
        setting.ImageWidthValidation__c = 2.048;
        setting.Max_Image_Height__c = 1.000;
        setting.MaxImageHeightClickArea__c = 300;
        setting.Max_Image_Width__c = 1.200;
        setting.MaxImageWidthClickArea__c = 300;
        insert setting;
        
        GL_FolderSettings = Test.loadData( PofSDocumentsFolder__c.sObjectType, 'PofS_TestDataDocumentFolders' );
        GL_ClickAreaTypes = Test.loadData( PofSClickAreaTypes__c.sObjectType, 'PofS_TestDataClickAreaTypes' );
        
        //Duc Nguyen Tien | Test class fix activity. The data loaded from static resources had hardcoded values for recordtype id resulting in crash of SFDC. The same is with the objects above, but the error is not critical atm.
        GL_Assortments = new List<Sobject>();
        RecordType rt = [Select Id from RecordType where DeveloperName = 'AssortmentArticleGroup' and SobjectType = 'ArticleGroup__c'];
        //assuming that recordtype exsits 
        List<String> records = new List<String>();
        records.add('Coke 0,5L PET#EUR#0,5 Glas; 0,5 PEW#PET#Coke#L#0,5#Standard');
        records.add('Sprite 0,5L PET#EUR#0,5 Glas; 0,5 PEW#PET#Sprite#L#0,5#Standard');
        records.add('Sprite Zero 0,5L PET#EUR#0,5 PEW#PET#Sprite#L#0,5#Zero');
        records.add('Fanta 0,5L PET#EUR#0,5 Glas; 0,5 PEW#PET#Fanta#L#0,5#Standard');
        records.add('Nestea 0,5L PET#EUR#0,5 Glas; 0,5 PEW#PET#Nestea#L#0,5#Standard');
        records.add('Bonaqa 0,5L PET#EUR#0,5 Glas; 0,5 PEW#PET#Bonaqa#L#0,5#Standard');
        records.add('Coke Zero 0,5L PET#EUR#0,5 PEW#PET#Coke#L#0,5#Zero');
        records.add('Fanta Zero 0,5L PET#EUR#0,5 PEW#PET#Fanta#L#0,5#Zero');
        records.add('Lift 0,5L PET#EUR#0,5 PEW#PET#Lift#L#0,5#Standard');
        records.add('Coke Light 0,5L PET#EUR#0,5 Glas; 0,5 PEW#PET#Coke#L#0,5#Light');
        for(String rec : records){
        	List<String> flds = rec.split('#');
        	GL_Assortments.add(new ArticleGroup__c(NAME=flds[0],CURRENCYISOCODE=flds[1],REDSURVEYREPLACEMENTS__C=flds[2],KINDOFPACKAGE__C=flds[3],GROUP__C=flds[4],MEASURINGUNIT__C=flds[5],PACKAGING_SIZE__C=flds[6],SUBGROUP__C=flds[7], RecordTypeID = rt.ID));
        	
        }
    	insert GL_Assortments;
    }
    
    private static void createNewQuestions(PofSImageComponentController LO_ImageComponentController) {
    	LO_ImageComponentController.newQuestion();
		LO_ImageComponentController.GM_ClickAreaQuestions.get( 1 ).put( 'Name', 'Name' );
		LO_ImageComponentController.GM_ClickAreaQuestions.get( 1 ).put( 'Type__c', 'Type' );
		LO_ImageComponentController.GM_ClickAreaQuestions.get( 1 ).put( 'Potential_Hint__c', 'Hint' );
        LO_ImageComponentController.newQuestion();
		LO_ImageComponentController.GM_ClickAreaQuestions.get( 2 ).put( 'Name', 'Name' );
		LO_ImageComponentController.GM_ClickAreaQuestions.get( 2 ).put( 'Type__c', 'Type' );
		LO_ImageComponentController.GM_ClickAreaQuestions.get( 2 ).put( 'Potential_Hint__c', 'Hint' );
        LO_ImageComponentController.newQuestion();
		LO_ImageComponentController.GM_ClickAreaQuestions.get( 3 ).put( 'Name', 'Name' );
		LO_ImageComponentController.GM_ClickAreaQuestions.get( 3 ).put( 'Type__c', 'Type' );
		LO_ImageComponentController.GM_ClickAreaQuestions.get( 3 ).put( 'Potential_Hint__c', 'Hint' );        
    }
}
