/*
 * @(#)SCfwComponentControllerBase.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 *
 * Class Hierarchy
 * ===============
 *
 *                                      this class
 *                                          |
 * SCItemsTabComponentController            |
 * SCOrderTabComponentController            |
 * SCPartsServicesTabComponentController    |
 *          |                               |
 * SCOrderComponentController               |
 *          |                               |
 * SCfwOrderControllerBase                  |
 *          |                               |
 * SCfwComponentControllerBase    <---------+
 *
 */
 
/**
 * see http://wiki.developerforce.com/index.php/Controller_Component_Communication
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing virtual class SCfwComponentControllerBase 
{
    //the new property
    public String key 
    { 
        get;
        set
        {
            if (value != null)
            {
                key  = value;
                if (pageController != null)
                {
                    pageController.setComponentControllerMap(key, this);
                }
            }
        }
    }

    public SCfwPageControllerBase pageController 
    {
        get; 
        set 
        {
            if (value != null) 
            {
                pageController = value;
                pageController.setComponentController(this);
            }
        }
    }
    
    public String pageControllerRerender {get; set;}
    
    
   /*
    * Called by the page to pass date to the controller
    * Override this method in derived classed to implement component <-> page communication
    * @param ctx  context information optional context information
    * @param data  a data object 
    */    
    public virtual String OnUpdateData(String ctx, Object data)
    {
        // called by the component 
        return null;
    }
    
} // class SCfwComponentControllerBase
