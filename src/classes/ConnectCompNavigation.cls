public with sharing class ConnectCompNavigation {

    //public variables
    public string profileImageUrl { get; set; }
    public string userProfileName { get; set; }
    public String userName {get; set;}
    public List<NavigatorItem> list_navigatorItems {get; set;}

    //constructor
    public ConnectCompNavigation(){
        userName = Userinfo.getName();
        
        Id profileId=Userinfo.getProfileId();
        userProfileName=[Select Id,Name from Profile where Id=:profileId].Name;
                    
        List<User> user_list = new List<User>([select FullPhotoUrl from User where Id =:Userinfo.getUserId()]);
        if(user_list.size()>0){
            profileImageUrl = user_list.get(0).FullPhotoUrl;
            
        }else{
            profileImageUrl='';
            userProfileName='';
        }
        initNavigatorItems();
    }

    private void initNavigatorItems(){
        list_navigatorItems = new List<NavigatorItem> ();
        
        list_navigatorItems.add(new NavigatorItem('BoderLeft1','einstellungen_small','Startseite','ConnectStartPage'));
        list_navigatorItems.add(new NavigatorItem('BoderLeft2','kalkulation','Kalkulatoren','CalculatorOverview'));
        list_navigatorItems.add(new NavigatorItem('BoderLeft3','success','Picture of success','PofSList', '_blank'));
        list_navigatorItems.add(new NavigatorItem('BoderLeft4','bibliothek','Bibliothek','CustomDocumentMain'));
        list_navigatorItems.add(new NavigatorItem('BoderLeft5','faq','FAQ','CustomFaqMain'));
        list_navigatorItems.add(new NavigatorItem('BoderLeft6','video','Vimeo','https://vimeo.com/log_in', '_blank'));
        
        
        //list_navigatorItems.add(new NavigatorItem('BoderLeft2','bilddatenbank','Bilddatenbank','CPCtrlBilddatenbank'));
        //list_navigatorItems.add(new NavigatorItem('BoderLeft7','video','Video','CPCtrlVideos'));
        //list_navigatorItems.add(new NavigatorItem('BoderLeft8','einstellungen','Einstellungen','CPCtrlSettings'));
        //list_navigatorItems.add(new NavigatorItem('BoderLeft9','help','Hilfe','CPCtrlHilfe'));
    }

    //inner wrapper object class
    public class NavigatorItem{
        public String tdClass {get; set;}
        public String imageSrc {get; set;}
        public String label {get; set;}
        public String navigateUrl {get; set;}
        public String target {get; set;}
        public navigatorItem(String tdClass, String imageSrc, String label, String navigateUrl){
            this.tdClass = tdClass;
            this.imageSrc = imageSrc;
            this.label = label;
            this.navigateUrl = navigateUrl;
            this.target = '_self';
        }
        
        public navigatorItem(String tdClass, String imageSrc, String label, String navigateUrl, String target){
            this.tdClass = tdClass;
            this.imageSrc = imageSrc;
            this.label = label;
            this.navigateUrl = navigateUrl;
            this.target = target;
        }
    }

}
