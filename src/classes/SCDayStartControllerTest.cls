/*
 * @(#)SCDayStartControllerTest.cls
 *
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author Dietrich Herdt <dherdt@gms-online.de>
 * @author Sebastian Schrage  <sschrage@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCDayStartControllerTest
{
    private static SCDayStartController dayStartController;

    /**
     * SCDayStartController under positiv test 1
     */
    static testMethod void dayStartControllerInitPositiv1()
    {
        // SCHelperTestClass2 for time report creation
        SCHelperTestClass2.createTimeReportSet(true);

        String userId = SCHelperTestClass.users.get(0).Id;

        dayStartController = new SCDayStartController ('testID', userId);
        dayStartController.checkDayOpen();
        dayStartController.openDay();
        System.assertEquals(true, dayStartController.allOk);
    }

    /**
     * SCDayStartController under positiv test 2
     */
    static testMethod void dayStartControllerInitPositiv2()
    {
        // SCHelperTestClass2 for time report creation
        SCHelperTestClass2.createTimeReportSet(true);

        String userId = SCHelperTestClass.users.get(1).Id;

        dayStartController = new SCDayStartController ('testID', userId);
        dayStartController.checkDayOpen();
        dayStartController.openDay();
        System.assertEquals(true, dayStartController.allOk);
    }
    
     /**
     * SCDayStartController under positiv test 3
     */
    static testMethod void dayStartControllerInitPositiv3()
    {
        // SCHelperTestClass2 for time report creation
        SCHelperTestClass2.createTimeReportSet(true);
        
        String userId = SCHelperTestClass.users.get(1).Id;
        ApexPages.currentPage().getParameters().put('uid', userId);
        
        test.startTest();
        dayStartController = new SCDayStartController ();        
        System.assertNotEquals(null, dayStartController.getCurrentDay());
        test.stopTest();
    }
    
    /**
     * init method under positiv test 
     */
    static testMethod void initPositiv()
    {
        ApexPages.currentPage().getParameters().put('uid', null);
        
        test.startTest();
        dayStartController = new SCDayStartController ();
        System.assertEquals(false, dayStartController.allOk);
        System.assertEquals(true, dayStartController.showOpenDay);
        test.stopTest();
    }
    
    /**
     * init method under negativ test with new user
     */
    static testMethod void initNegativ()
    {
    
        ApexPages.currentPage().getParameters().put('uid', null);
        // create test user without the resource
        SCHelperTestClass.createTestUsers(true);
        User testUser = SCHelperTestClass.users.get(0);
        
        test.startTest();
        // The user had no resource in the system.
        System.runAs(testUser)
        {
            dayStartController = new SCDayStartController ();
            System.assertEquals('', dayStartController.getSelectedResource());
        }       
        test.stopTest();
    }
    
    /**
     * checkDayOpen() method under positiv test with new user
     */
    static testMethod void checkDayOpenPositiv()
    {
        // create test user 
        SCHelperTestClass.createTestUsers(true);
        // use the NL user for the test, can be modified here
        // NL user have the number 3 in the list (starting at 0)
        User testUser = SCHelperTestClass.users.get(3);
        
        test.startTest();
        // The user had resource and no open days in the system.
        System.runAs(testUser)
        {
            // create resource for the test user
            SCHelperTestClass.createTestResources(SCHelperTestClass.users, true);
        
            dayStartController = new SCDayStartController ();
            dayStartController.checkDayOpen();
            System.assertEquals(false, dayStartController.allOk);
        }       
        test.stopTest();
    }   
    
   /**
     * @author: Sebastian Schrage <sschrage@gms-online.de>
     */
    static testMethod void SCDayStartControllerPageCalls()
    {
        try
        {
            SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
            appset.RESOURCE_CHANGE_ALLOWED__c = 0.0;
            Database.upsert(appset);
        }
        catch (Exception e)
        {
            Database.insert(new SCApplicationSettings__c(RESOURCE_CHANGE_ALLOWED__c = 0.0));
        }
             
        Test.startTest();
        
        dayStartController = new SCDayStartController ();
        
        System.assertEquals(dayStartController.getAllowedToChooseTheResource(),false);
        System.assertNotEquals(dayStartController.getSelectedResource(),null);
        System.assertEquals(dayStartController.ChangeResource(),null);
        System.assertEquals(dayStartController.setAllOkTrue(),null);
        
        dayStartController.getAllResourceList();
        dayStartController.setSelectedResource(null);
        
        Test.stopTest();
    }   
}
