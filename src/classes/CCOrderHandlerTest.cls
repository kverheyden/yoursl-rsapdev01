/*
* @description	Test class for CCOrderHandler
*
* @date			20.11.2014
*
* Timeline:
* Name               Date                      Description
* Oliver Preuschl    20.11.2014				   Created
*
*/

@isTest
private class CCOrderHandlerTest {
	
	@isTest static void testLastOrder() {
		//Create Accounts
		List< Account > listAccounts 			= createAccounts( 100 );
		insert( listAccounts );
		//Create CCOrders
		List< ccrz__E_Order__c > listCCOrders 	= createCCOrders( listAccounts, 10 );
		insert( listCCOrders );

		//All Accounts should have been updated
		listAccounts = [ SELECT Id, LastOrder__c FROM Account WHERE ( Id IN :listAccounts ) ];
		Integer i = 1;
		for( Account oAccount: listAccounts ){
			System.assertEquals( oAccount.LastOrder__c, listCCOrders.get( i * 10 - 1 ).Id );
			i++;
		}

	}

	@isTest static void testHasOrderForNextDeliveryDate() {
		//Create Accouts
		List< Account > listAccounts 			= createAccounts( 100 );
		Integer i = 0;
		for( Account oAccount: listAccounts ){
			oAccount.NextPossibleDeliveryDate__c = Date.today() + i;
			i++;
		}
		insert( listAccounts );
		//Create CCOders
		List< ccrz__E_Order__c > listCCOrders 	= createCCOrders( listAccounts, 10 );
		i = 0;
		for( ccrz__E_Order__c oCCOrder: listCCOrders ){
			oCCOrder.ccrz__RequestDate__c = Date.today() + i;
			i++;
			if( i == 10 ){
				i = 0;
			}
		}
		insert( listCCOrders );

		//The first 10 Accounts should have been updated
		listAccounts = [ SELECT Id, LastOrder__c, NextPossibleDeliveryDate__c, HasOrderForNextDeliveryDate__c FROM Account WHERE ( Id IN :listAccounts ) ];
		i = 0;
		for( Account oAccount: listAccounts ){
			if( i < 10 ){
				System.assertEquals( true, oAccount.HasOrderForNextDeliveryDate__c );
			}else{
				System.assertEquals( false, oAccount.HasOrderForNextDeliveryDate__c );
			}
			i++;
		}

		//Update Accounts
		for( i = 10; i < 20; i++ ){
			Account oAccount = listAccounts.get( i );
			oAccount.NextPossibleDeliveryDate__c = Date.Today() + ( i - 10 );
		}
		update( listAccounts );

		//This time 10 more Accounts should have been updated
		listAccounts = [ SELECT Id, LastOrder__c, NextPossibleDeliveryDate__c, HasOrderForNextDeliveryDate__c FROM Account WHERE ( Id IN :listAccounts ) ];
		i = 0;
		for( Account oAccount: listAccounts ){
			if( i < 20 ){
				System.assertEquals( true, oAccount.HasOrderForNextDeliveryDate__c );
			}else{
				System.assertEquals( false, oAccount.HasOrderForNextDeliveryDate__c );
			}
			i++;
		}

	}
	
	private static List< Account > createAccounts( Integer iNumberOfAccounts ){
		List< Account > listAccounts = new List< Account >();
		for( Integer i = 0; i < iNumberOfAccounts; i++ ){
			Account oAccount = new Account(
				Name		= 'Account' + i
			);
			listAccounts.add( oAccount );
		}
		return listAccounts;
	}

	private static List< ccrz__E_Order__c > createCCOrders( List< Account > listAccounts, Integer iNumberOfCCOrders ){
		List< ccrz__E_Order__c > listCCOrders = new List< ccrz__E_Order__c >();
		for( Account oAccount: listAccounts ){
			for( Integer i = 0; i < iNumberOfCCOrders; i++ ){
				ccrz__E_Order__c oCCOrder = new ccrz__E_Order__c(
					ccrz__Account__c		= oAccount.Id
				);
				listCCOrders.add( oCCOrder );
			}
		}
		return listCCOrders;
	}

}
