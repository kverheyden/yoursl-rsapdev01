/**
 * This class contains unit tests for validating the behavior of Apex classes 
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ContactMethodsTest {
   
    /*
    One Account with some Contacts. One contact is flagged as PrimarySales.
    */
    static testMethod void testInsertionWithOnePrimarySalesContact() {
    	
    	Account oTestAccount = createAccount();
    	insert oTestAccount;
    	
    	List<Contact> listTestContacts = new List< Contact >(); 	
    	listTestContacts.add( createContact( oTestAccount, 'TestPSC001', 'TestPSC001' ) );
    	listTestContacts.add( createContact( oTestAccount, 'Verkaufsberater', 'TestPSC002' ) );
    	listTestContacts.add( createContact( oTestAccount, 'TestPSC003', 'TestPSC003' ) );
    	insert listTestcontacts;
    	    	
    	oTestAccount = [ SELECT PrimarySalesContact__c FROM Account WHERE Id = :oTestAccount.Id ];	
    	System.assertEquals( listTestContacts.get( 1 ).Id, oTestAccount.PrimarySalesContact__c );
    }
    
    /*
    One Account with some Contacts. A new contact is flagged as PrimarySales. One old contact is flagged too. 
    */
    static testMethod void testInsertionOfNewPrimarySalesContact() {
    	
    	Account oTestAccount = createAccount();
    	insert oTestAccount;
    	
    	List<Contact> listTestContacts = new List< Contact >(); 	
    	listTestContacts.add( createContact( oTestAccount, 'Verkaufsberater', 'TestPSC004' ) );
    	insert listTestcontacts;

        oTestAccount = [ SELECT PrimarySalesContact__c FROM Account WHERE Id = :oTestAccount.Id ];  
        System.assertEquals( listTestContacts.get( 0 ).Id, oTestAccount.PrimarySalesContact__c );
   	
    	List<Contact> listTestContacts2 = new List<Contact>(); 
    	listTestContacts2.add( createContact( oTestAccount, 'TestPSC005', 'TestPSC005' ) );
    	listTestContacts2.add( createContact( oTestAccount, 'Verkaufsberater', 'TestPSC006' ) );
    	insert listTestContacts2;
    	
        oTestAccount = [ SELECT PrimarySalesContact__c FROM Account WHERE Id = :oTestAccount.Id ];  
        System.assertEquals( listTestContacts2.get( 1 ).Id, oTestAccount.PrimarySalesContact__c );
    }
   	
   	/*
   	One account with two flagged contacts
   	*/ 
    static testMethod void testWithTwoPrimarySalesContacts() {
    	Account oTestAccount = createAccount();
    	insert oTestAccount;
    	
    	List< Contact > listTestContacts = new List< Contact >();
    	listTestContacts.add( createContact( oTestAccount, 'Verkaufsberater', 'TestPSC007' ) );
    	listTestContacts.add( createContact( oTestAccount, 'Verkaufsberater', 'TestPSC008' ) );
    	insert listTestcontacts;
    	
        oTestAccount = [ SELECT PrimarySalesContact__c FROM Account WHERE Id = :oTestAccount.Id ];  
        System.assert( ( oTestAccount.PrimarySalesContact__c == listTestContacts.get( 0 ).Id ) || ( oTestAccount.PrimarySalesContact__c == listTestContacts.get( 1 ).Id ), 'The PrimarySalesComtact was not correctly assigned to the Acount' );
    	
    }    
    
    //Create a TestAccount
    private static Account createAccount() {
    	Account oAccount = new Account();
    	oAccount.Name = 'TextName';
    	oAccount.BillingCountry__c = 'DK';
    	oAccount.RecordTypeId = [ Select Id from RecordType where Name = 'Customer'  and SobjectType = 'Account' LIMIT 1 ].Id;	
    	return oAccount;
    }
    
    //Create a TestContact
    private static Contact createContact(Account oAccount, String sFirstName, String sLastName) {	
    	Contact oContact = new Contact();
        oContact.FirstName = sFirstName;
    	oContact.LastName = sLastName;
    	oContact.AccountId = oAccount.Id;    	
    	return oContact;
    }    
}
