global class CCPluginSendOrder implements ccrz.cc_if_OutboundOrder {

    global void setStorefrontSettings(Map<String,Object> storeSettings) {}
    global void setDaoObject(ccrz.cc_if_dao obj) {}
    global void setServiceObject(ccrz.cc_if_service obj) {}

    global Map<String,String> sendOrder(String orderId,String storeName,boolean skipEligibility) {

        ccrz__E_Order__c theOrder = [
            select
                 Name
                ,Id
                ,ccrz__Account__c
                ,ccrz__OrderId__c
                ,ccrz__PONumber__c
                ,ccrz__RequestDate__c
                ,ccrz__Note__c 
                ,ccrz__EffectiveAccountID__c
                ,ccrz__OrderStatus__c
                ,ccrz__User__c
                ,ccrz__Contact__c
                ,ccrz__BuyerFirstName__c
                ,ccrz__BuyerLastName__c
                ,ccrz__BuyerEmail__c
                ,ccrz__ShipMethod__c
                ,ccrz__ShipTo__c
                ,ccrz__ShipTo__r.ccrz__AddressFirstline__c
                ,ccrz__ShipTo__r.ccrz__City__c
                ,ccrz__ShipTo__r.ccrz__State__c
                ,ccrz__ShipTo__r.ccrz__StateISOCode__c
                ,ccrz__ShipTo__r.ccrz__PostalCode__c
                ,ccrz__ShipTo__r.ccrz__Country__c
                ,ccrz__ShipTo__r.ccrz__CountryISOCode__c
                ,ccrz__ShipTo__r.ccrz__Partner_Id__c
                ,ccrz__ShipTo__r.ccrz__ShippingComments__c
                ,ccrz__ShipTo__r.ccrz__CompanyName__c
                ,ccrz__ShipTo__r.ccrz__MailStop__c
                ,ccrz__BillTo__c
                ,ccrz__BillTo__r.ccrz__FirstName__c
                ,ccrz__BillTo__r.ccrz__LastName__c
                ,ccrz__BillTo__r.ccrz__AddressFirstline__c
                ,ccrz__BillTo__r.ccrz__City__c
                ,ccrz__BillTo__r.ccrz__State__c
                ,ccrz__BillTo__r.ccrz__StateISOCode__c
                ,ccrz__BillTo__r.ccrz__PostalCode__c
                ,ccrz__BillTo__r.ccrz__Country__c
                ,ccrz__BillTo__r.ccrz__CountryISOCode__c
                ,ccrz__BillTo__r.ccrz__Partner_Id__c
                ,ccrz__BillTo__r.ccrz__CompanyName__c
                ,ccrz__BillTo__r.ccrz__MailStop__c
                ,(
                    select
                         AlternativeFor__c
                        ,Name
                        ,Promotion__c
                        ,ReducedQuantityUC__c
                        ,ccrz__AvailabilityMessage__c
                        ,ccrz__Comments__c
                        ,ccrz__Product__r.ccrz__ProductId__c
                        ,ccrz__Product__r.ccrz__Sku__c
                        ,ccrz__Quantity__c
                        ,ccrz__RequestDate__c
                        ,ccrz__UnitOfMeasure__c
                        ,ccrz__OrderItemId__c
                    from
                        ccrz__E_OrderItems__r
                )
            from
                ccrz__E_Order__c
            where
                Id = :orderId
        ];

        CCWSUtil utils = new CCWSUtil();
        CCWSSalesOrderRequestResponse_Out.CCWSSalesOrderRequestResponse_OutPort stub = new CCWSSalesOrderRequestResponse_Out.CCWSSalesOrderRequestResponse_OutPort();
        stub.endpoint_x = utils.getEndpoint('SalesOrderRequestResponse');
        stub.inputHttpHeaders_x = utils.getBasicAuth();
        stub.timeout_x = utils.getTimeOut();

        String uniqueOrderNo = theOrder.Name;
        if (theOrder.ccrz__PONumber__c != null)
            uniqueOrderNo = theOrder.ccrz__PONumber__c;

        CCWSSalesOrderRequestResponse_Out.BAPISDHD1 orderHeader = new CCWSSalesOrderRequestResponse_Out.BAPISDHD1();
        orderHeader.REQ_DATE_H = formatDate(theOrder.ccrz__RequestDate__c); // Requeste date from the order
        orderHeader.PURCH_NO_C = uniqueOrderNo;                 // Purchase number
        orderHeader.PURCH_NO_S = theOrder.Name;                 // Purchase number
        orderHeader.CREATED_BY = S_CLOUDCRAZE;

        CCWSSalesOrderRequestResponse_Out.ORDER_ITEMS_IN_element orderItems = new CCWSSalesOrderRequestResponse_Out.ORDER_ITEMS_IN_element();
        orderItems.item = new CCWSSalesOrderRequestResponse_Out.BAPIITEMIN[]{};

        Integer index = INCREMENT;
        for(ccrz__E_OrderItem__c orderItem : theOrder.ccrz__E_OrderItems__r) {
            CCWSSalesOrderRequestResponse_Out.BAPIITEMIN reqitem = new CCWSSalesOrderRequestResponse_Out.BAPIITEMIN();
            reqitem.PO_ITM_NO_S = orderItem.Name;
            reqitem.PURCH_NO_S = theOrder.Name;
            reqitem.ITM_NUMBER = leftPad(index);                                    // Index for order processor to keep track
            reqitem.MATERIAL = formatSku(orderItem.ccrz__Product__r.ccrz__SKU__c);
            orderItems.item.add(reqitem);
            index += INCREMENT;
        }

        CCWSSalesOrderRequestResponse_Out.ORDER_PARTNERS_element orderPartners = new CCWSSalesOrderRequestResponse_Out.ORDER_PARTNERS_element();
        orderPartners.item = new CCWSSalesOrderRequestResponse_Out.BAPIPARTNR[]{};

        CCWSSalesOrderRequestResponse_Out.BAPIPARTNR partnerItem = new CCWSSalesOrderRequestResponse_Out.BAPIPARTNR();
        partnerItem.PARTN_ROLE = 'WE';                                          // Fixed
        partnerItem.PARTN_NUMB = padData(theOrder.ccrz__ShipTo__r.ccrz__Partner_Id__c, 10, '0');// Ship to number, selected by customer
        orderPartners.item.add(partnerItem);

        CCWSSalesOrderRequestResponse_Out.ORDER_SCHEDULES_IN_element orderSchedules = new CCWSSalesOrderRequestResponse_Out.ORDER_SCHEDULES_IN_element();
        orderSchedules.item = new CCWSSalesOrderRequestResponse_Out.BAPISCHDL[]{};

        index = INCREMENT;
        for(ccrz__E_OrderItem__c orderItem : theOrder.ccrz__E_OrderItems__r) {
            CCWSSalesOrderRequestResponse_Out.BAPISCHDL orderSchedule = new CCWSSalesOrderRequestResponse_Out.BAPISCHDL();
            orderSchedule.ITM_NUMBER = leftPad(index);                                   // Index for order processor to keep track
            orderSchedule.SCHED_LINE = String.valueOf(index);                            // Repeat?
            orderSchedule.REQ_DATE = formatDate(theOrder.ccrz__RequestDate__c);          // Requeste date from the lineitem
            orderSchedule.REQ_QTY = String.valueOf(orderItem.ccrz__Quantity__c) + '000'; // Quantity for this lineitem
            orderSchedules.item.add(orderSchedule);
            index += INCREMENT;
        }

        CCWSSalesOrderRequestResponse_Out.ORDER_TEXT_element orderText = null;
        if (theOrder.ccrz__Note__c != null) {
            orderText = new CCWSSalesOrderRequestResponse_Out.ORDER_TEXT_element();
            orderText.item = new CCWSSalesOrderRequestResponse_Out.BAPISDTEXT[]{};
            CCWSSalesOrderRequestResponse_Out.BAPISDTEXT text = new CCWSSalesOrderRequestResponse_Out.BAPISDTEXT();
            text.TEXT_LINE = theOrder.ccrz__Note__c;
            orderText.item.add(text);
        }

        CCWSSalesOrderRequestResponse_Out.CC_SalesOrderCreateOrderResponse_element response = stub.CreateOrder(
             orderHeader    //CCWSSalesOrderRequestResponse_Out.BAPISDHD1 ORDER_HEADER_IN
            ,null           //String SALESDOCUMENTIN
            ,null           //String TESTRUN
            ,orderItems     //CCWSSalesOrderRequestResponse_Out.ORDER_ITEMS_IN_element ORDER_ITEMS_IN
            ,orderPartners  //CCWSSalesOrderRequestResponse_Out.ORDER_PARTNERS_element ORDER_PARTNERS
            ,orderSchedules //CCWSSalesOrderRequestResponse_Out.ORDER_SCHEDULES_IN_element ORDER_SCHEDULES_IN
            ,orderText      //CCWSSalesOrderRequestResponse_Out.ORDER_TEXT_element ORDER_TEXT
        );

        if(null == response) 
            throw new CCWSSalesOrderException('TRANSLATE: No Reponse Received From Create Order');
        else {
            /*
            CCWSSalesOrderRequestResponse_Out.BAPIRET2[] items = response.RETURN_x.item;
            string salesOrderId = null;
            if(items != null) {
                for(CCWSSalesOrderRequestResponse_Out.BAPIRET2 item : items) {
                    salesOrderId = item.ID;
                }
                theOrder.ccrz__OrderId__c = salesOrderId;
                update theOrder;
            }
            */
            theOrder.ccrz__OrderStatus__c = 'In Bearbeitung';
            update theOrder;
            system.debug(logginglevel.info, 'response=' + JSON.serialize(response));
        }
        for(ccrz__E_OrderItem__c orderItem : theOrder.ccrz__E_OrderItems__r) {
            orderItem.ccrz__OrderItemId__c = orderItem.Name;
        }
        update theOrder.ccrz__E_OrderItems__r;
        flagActivePromo(theOrder);
        return null;
    }

    /*
    * Stamp the Promotion Name on the Order Item , which has an active promotion running. 
    */
    private void flagActivePromo(ccrz__E_Order__c theOrder){
        String shiptoId = theOrder.ccrz__ShipTo__r.ccrz__Partner_Id__c;
        Account shipTo = [SELECT DeliveringPlant__c, AccountNumber FROM Account WHERE AccountNumber =: shiptoId LIMIT 1];
        String accountId = shipTo.Id;
        system.debug(LoggingLevel.DEBUG,'@@accountId='+ accountId);
        if(accountId != null){
            List<ccrz__E_PromotionAccountGroupFilter__c> accountPromos = [SELECT ccrz__CC_Promotion__r.Name, CCAccount__r.Id FROM ccrz__E_PromotionAccountGroupFilter__c where ccrz__CC_Promotion__r.ccrz__Enabled__c = true AND ccrz__StartDate__c <= TODAY AND ccrz__EndDate__c   >  TODAY AND ccrz__FilterType__c = 'Inclusive' AND CCAccount__r.Id =:accountId];    
            system.debug(LoggingLevel.DEBUG,'@@accountPromos='+ accountPromos);
            if(accountPromos != null && accountPromos.size() >0){
                List<String> promos = new List<String>();
                for(ccrz__E_PromotionAccountGroupFilter__c accGrpFltr: accountPromos){
                    promos.add(accGrpFltr.ccrz__CC_Promotion__r.Name);
                }
                
                List<E_PromotionProduct__c> promoProds = [SELECT Promotion__r.Name, Product__r.ccrz__SKU__c FROM E_PromotionProduct__c where Promotion__r.Name in :promos];
                system.debug(LoggingLevel.DEBUG,'@@promoProds='+ promoProds);
                if(promoProds != null && promoProds.size()>0){
                    Set<String> promoSKUs = new Set<String>();
                    Map<String,String> promoNameMap = new Map<String,String>();

                    for(E_PromotionProduct__c promoProd:promoProds){
                        promoSKUs.add(promoProd.Product__r.ccrz__SKU__c);
                        promoNameMap.put(promoProd.Product__r.ccrz__SKU__c, promoProd.Promotion__r.Name);
                    }
                    List<ccrz__E_OrderItem__c> orderItems = new List<ccrz__E_OrderItem__c>();
                    for(ccrz__E_OrderItem__c orderItem : theOrder.ccrz__E_OrderItems__r) {
                        system.debug(LoggingLevel.DEBUG,'@@orderItem.ccrz__Product__r.ccrz__Sku__c='+ orderItem.ccrz__Product__r.ccrz__Sku__c);
                        system.debug(LoggingLevel.debug,'@@promoSKUs='+ promoSKUs);
                        boolean needsUpdating = false;
                        if(promoSKUs.contains(orderItem.ccrz__Product__r.ccrz__Sku__c)){
                            orderItem.Promotion__c = promoNameMap.get(orderItem.ccrz__Product__r.ccrz__Sku__c);
                            needsUpdating = true;
                        }
                        if (orderItem.ccrz__AvailabilityMessage__c != null) {
                            orderItem.AlternativeFor__c = orderItem.ccrz__AvailabilityMessage__c;
                            orderItem.ccrz__AvailabilityMessage__c = null;
                            needsUpdating = true;
                        }
                        if (orderItem.ccrz__Comments__c != null) {
                            try {
                                Decimal origQty = Decimal.valueOf(orderItem.ccrz__Comments__c);
                                Decimal multiplier = 1;
                                try {
                                    multiplier = Decimal.valueOf(orderItem.ccrz__UnitOfMeasure__c);
                                }
                                catch (Exception me) {
                                    multiplier = 1;
                                }
                                Decimal diffQty = origQty - orderItem.ccrz__Quantity__c;
                                orderItem.ReducedQuantityUC__c = diffQty * multiplier;
                                needsUpdating = true;
                            }
                            catch (Exception e) {}
                        }
                        if (needsUpdating)
                            orderItems.add(orderItem);

                    }
                    system.debug(LoggingLevel.debug,'@@orderItems='+ orderItems);
                    update orderItems; 
                }
            }  
                     
        }  
        
    }


    global Map<String, String> sendOrder(Map<String,Object> dataMap,String storeName,boolean skipEligibility) {
        return null;
    }

    // Returns formatted string with up to 6 leading zeros.
    private String leftPad(Integer input) {
        String inStr = String.valueOf(input);
        return ('000000' + inStr).substring(inStr.length());
    }

    private String formatDate(Date theDate) {
        String y = String.valueOf(theDate.year());
        String m = String.valueOf(theDate.month());
        String d = String.valueOf(theDate.day());
        return ('0000' + y).substring(y.length()) + ('00' + m).substring(m.length()) + ('00' + d).substring(d.length());
    }

    private String padData(Object inputData, Integer length, String padChar) {
        if (inputData == null)
            return null;
        String val = String.valueOf(inputData);
        while (val.length() < length)
            val = padChar + val;
        return val;
    }

    private String formatSku(String sku) {
        return '000000000000' + sku;
        //return sku;
    }

    private final Integer INCREMENT = 10;

    private final String S_CLOUDCRAZE = 'CLOUDCRAZE';
}
