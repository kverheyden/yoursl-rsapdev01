/**
* @author Jan Mensfeld
* @date 30.10.2014
* @description Class that ITriggerHandler to provide abstract and virtual methods for the interface methods.
* Trigger handlers need to implement only the method they have to excepted the 'mainEntry' method.
* Trigger handlers have to implement the method 'mainEntry'.
*/
public abstract class TriggerHandlerBase implements ITriggerHandler {
	public Map<Id, SObject> sObjectsToUpdate = new Map<Id, SObject>();
	
	/** 
	* @author Jan Mensfeld
	* @date 30.10.2014
	* @description Method that runs if the trigger is  the first time in the execution context. 
	* Subclasses have to implement this method.
	* @param TriggerParameters The trigger parameters such as the list of records before and after the update.
	*/
	public abstract void mainEntry(TriggerParameters tp);
	
	
	/** 
	* @author Jan Mensfeld
	* @date 30.10.2014
	* @description Called for the subsequent times in the same execution context. The trigger handlers can chose
	* to ignore if they are not running recursively.
	* @param TriggerParameters The trigger parameters such as the list of records before and after the update.
	*/
	public virtual void inProgressEntry(TriggerParameters tp) {
		
	}
	
	/** 
	* @author Jan Mensfeld
	* @date 30.10.2014
	* @description Updated the objects if there are any object to update.
	*/
	public virtual void updateObjects() {
		if(sObjectsToUpdate.size() > 0)
			update sObjectsToUpdate.values();
	}
}
