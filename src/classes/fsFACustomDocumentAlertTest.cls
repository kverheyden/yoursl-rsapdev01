/**********************************************************************

Name: fsFA_PofS_ImageComponentController

======================================================

Purpose: 
 
This is the test class for fsFACustomDocumentAlert

======================================================

History 

------- 

Date 			AUTHOR 				DETAIL

09/12/2014 		Oliver Preuschl 	INITIAL DEVELOPMENT

***********************************************************************/

@isTest
private class fsFACustomDocumentAlertTest {

    static testMethod void snedContentAlertWithoutExpirationSettings() {
    	List< CustomDocument__c > listCustomDocuments	= new List< CustomDocument__c >();
    	CustomDocument__c oCustomDocument1 = createCustomDocument( 'CD1', Date.today() + 14 );
    	listCustomDocuments.add( oCustomDocument1 );
    	CustomDocument__c oCustomDocument2 = createCustomDocument( 'CD2', Date.today() + 14 );
    	listCustomDocuments.add( oCustomDocument2 );
    	CustomDocument__c oCustomDocument3 = createCustomDocument( 'CD3', Date.today() + 14 );
    	listCustomDocuments.add( oCustomDocument3 );
    	CustomDocument__c oCustomDocument4 = createCustomDocument( 'CD4', Date.today() + 14 );
    	listCustomDocuments.add( oCustomDocument4 );
    	CustomDocument__c oCustomDocument5 = createCustomDocument( 'CD5', Date.today() + 14 );
        listCustomDocuments.add( oCustomDocument5 );
        
        insert listCustomDocuments;
        
        
        Test.StartTest();

		fsFACustomDocumentAlertScheduler LO_CustomDocumentAlertScheduler = new fsFACustomDocumentAlertScheduler();      
		String LO_Schedule = '0 0 23 * * ?';
		System.schedule( 'CustomDocumentAlertScheduler Test', LO_Schedule, LO_CustomDocumentAlertScheduler);

		Test.StopTest();
    }

    static testMethod void snedContentAlertWithExpirationSettings() {
    	SalesAppSettings__c oSalesAppSettings = createSettings( '21' );
    	insert( oSalesAppSettings );

    	List< CustomDocument__c > listCustomDocuments	= new List< CustomDocument__c >();
    	CustomDocument__c oCustomDocument1 = createCustomDocument( 'CD1', Date.today() + 21 );
    	listCustomDocuments.add( oCustomDocument1 );
    	CustomDocument__c oCustomDocument2 = createCustomDocument( 'CD2', Date.today() + 21 );
    	listCustomDocuments.add( oCustomDocument2 );
    	CustomDocument__c oCustomDocument3 = createCustomDocument( 'CD3', Date.today() + 21 );
    	listCustomDocuments.add( oCustomDocument3 );
    	CustomDocument__c oCustomDocument4 = createCustomDocument( 'CD4', Date.today() + 21 );
    	listCustomDocuments.add( oCustomDocument4 );
    	CustomDocument__c oCustomDocument5 = createCustomDocument( 'CD5', Date.today() + 21 );
        listCustomDocuments.add( oCustomDocument5 );
        
        insert listCustomDocuments;
        
        
        Test.StartTest();

		fsFACustomDocumentAlertScheduler LO_CustomDocumentAlertScheduler = new fsFACustomDocumentAlertScheduler();      
		String LO_Schedule = '0 0 23 * * ?';
		System.schedule( 'CustomDocumentAlertScheduler Test', LO_Schedule, LO_CustomDocumentAlertScheduler);

		Test.StopTest();
    }

    private static SalesAppSettings__c createSettings( String sExpirationTreshold ){
    	SalesAppSettings__c oExpirationThresholdDays = new SalesAppSettings__c(
    		Name 		= 'CustomDocumentExpirationThresholdDays',
    		Value__c	= sExpirationTreshold
    	);
    	return oExpirationThresholdDays;
    }

	private static CustomDocument__c createCustomDocument( String sTitle, Date dValidTo ){
		CustomDocument__c oCustomDocument = new CustomDocument__c(
			Title__c = sTitle,
			ValidFrom__c = Date.today(),
			ValidTo__c = dValidTo
		);
		return oCustomDocument;
	}

}
