@RestResource(urlMapping='/ProductWithUnitPrice/*')
global with sharing class CCWCRestProductWithUnitPrice{
		
	private static JSONGenerator jsonGEN;
	
	private static final String FIELDSET_NAME = 'SalesAppQueryProductWithPricebookFields';
	private static final String PARAMETER_OFFSET_ID = 'offsetId';
	private static final String PARAMETER_TIMESTAMP = 'timestamp';
	private static final Integer PRODUCT_LIMIT = 500;
	
	private static List<String> productQueryFieldList = Utils.getFieldSetMemberStringList(Product2.SObjectType, FIELDSET_NAME);
	
	private static String getParameterValueFromUrlRequest(String sUrlParameter) {
		try {
			RestRequest objRequest = RestContext.Request;
			return objRequest.params.get(sUrlParameter);			
		}
		catch(TypeException e) {
			System.debug(e.getTypeName() + ': ' + e.getMessage());
			return null;
		}
	}
	
	private static Set<String> defaultProductTypeList = new Set<String>{
		'ZEMP',
		'ZFER',
		'ZHAW', 
		'ZMM'
	};
	/* Duc Nguyen Tien | Seem like never used here
	private static Map<Id, Product2> queryProducts() {
		String productQuery = String.format('SELECT {0} FROM Product2 WHERE Type__c IN: defaultProductTypeList ORDER BY Id', new List<String>{
				String.join(productQueryFieldList, ', ')
			}
		);
		return new Map<Id, Product2>((List<Product2>)Database.query(productQuery));
	}*/
	
	public static List<Product2> queryProducts(String offsetId, String timestamp) {
        System.debug('dav>> offsetID: '+offsetID+' timestamp: '+timestamp);
		String productQuery = 'SELECT ' + String.join(productQueryFieldList, ', ') + ' FROM Product2 ';
		productQuery += 'WHERE ';
		
		if (offsetId != null && offsetId != '')
			productQuery += '(Id > :offsetId) AND ';
		
		if (timestamp != null && timestamp != '')
			productQuery += '(LastModifiedDate > ' + timestamp + ') AND ';
		
		productQuery += '(IsSalesAppRelevant__c = true) AND ';
		productQuery += 'Type__c  IN (\'' + String.join(new List<String>(defaultProductTypeList), '\', \'')  + '\') ';
		productQuery += 'ORDER BY Id ';
		productQuery += 'LIMIT ' + PRODUCT_LIMIT;			
		System.debug('dav>> '+productQuery);
		
		List<Product2> productList = Database.query(productQuery);
				
		return productList;
	}
	/* Duc Nguyen Tien | Seems like never used here
	private static String getCommaSeparatedkIdListForQueries(Set<Id> ids) {
		return '\'' + String.join(new List<Id>(ids), '\',\'') + '\'';
	}*/
		
	@HttpGet
	global static void doGet() {
		DateTime requestStart = System.now();
		RestServiceUtilities.WSLog log = new RestServiceUtilities.WSLog();
		log.setStart(requestStart);
		jsonGEN = JSON.createGenerator(true);
		String offsetId = getParameterValueFromUrlRequest(PARAMETER_OFFSET_ID);
		String timestamp = getParameterValueFromUrlRequest(PARAMETER_TIMESTAMP);
		try {
			List<Product2> products = queryProducts(offsetId, timestamp);			
			String pricebookQuery = 'SELECT Id, (SELECT UnitPrice, Product2Id FROM PricebookEntries) FROM Pricebook2 WHERE IsStandard = true LIMIT 1 ALL ROWS';
			Pricebook2 pricebook = Database.query(pricebookQuery);
			
			Map<Id, Double> productUnitPriceMap = new Map<Id, Double>();
			for (PricebookEntry pe : pricebook.PricebookEntries)
				productUnitPriceMap.put(pe.Product2Id, pe.UnitPrice);
			Double unitPrice;
			
			jsonGEN.writeStartArray();			
			for (Product2 p : products) {
				unitPrice = productUnitPriceMap.get(p.Id);
				jsonGEN.writeStartObject();						
				jsonGEN.writeObjectField('UnitPrice', unitPrice == null ? 0.00 : unitPrice);
				for(String f : productQueryFieldList) {
					if (p.get(f) == null) 
						jsonGEN.writeNullField(f);
					else
						jsonGEN.writeObjectField(f, p.get(f));
				}
				jsonGEN.writeEndObject();
			}
			jsonGEN.writeEndArray();
			RestContext.response.addHeader('Content-Type', 'application/json');
        	RestContext.response.responseBody = Blob.valueOf(jsonGEN.getAsString());			
			
		} catch(Exception e) {
			log.setStatus(RestServiceUtilities.WSStatus.ERROR);
		} finally {
			//log.insertLog();
		}		
	}
}
