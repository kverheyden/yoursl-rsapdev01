/**
* @author		Austen Buennemann
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
*				a.buennemann@yoursl.de
*
* @description	Checks the Account relation to the appended Request__c objects and adds them if they are missing.
*
* @date			14.08.2014
*
* Timeline:
* Name               DateTime                  Version         Description
* Austen Buennemann  14.08.2014 11:21          *1.0*           created the class
*/

public with sharing class AccountRelationOfRequestChields {
	
	/**
	* @description	Read relevant Request__c records
	* @param		idSet Set of Ids
	*/	
	public Map<Id, Request__c> readRequest(Set<Id>  idSet) {
		Map<Id, Request__c> requests = new Map<Id, Request__c>([SELECT Account__c,Id FROM Request__c Where Id =: idSet]);
		return requests;
	}
	
	/**
	* @description	Relate SalesOrder__c
	* @param		entries with new Trigger data
	*/
	public void relateSalesOrder(SalesOrder__c [] entries ){
		boolean action = false;		
		Set<Id> ids = new Set<Id>();
		for (SalesOrder__c record : entries){
			if(record.Account__c  == null){
				action = true;		
				ids.add(record.Request__c);
			}
		}
		
		if(action){		
			List<SalesOrder__c> records = [SELECT Id, Request__c, Account__c FROM SalesOrder__c Where Request__c =: ids];				
			AccountRelationOfRequestChields relation = new AccountRelationOfRequestChields();
			Map<Id, Request__c> requestMap = relation.readRequest(ids);			
			List<SalesOrder__c> recordUpdates = new List<SalesOrder__c>();			
			if(requestMap != null){
				for (SalesOrder__c record : records){
					if(record.Account__c == null && requestMap.containsKey(record.Request__c)){
						if(requestMap.get(record.Request__c).Account__c != null){
							record.Account__c = requestMap.get(record.Request__c).Account__c;
							recordUpdates.add(record);
						}
					}
				} 
			}			
			if(!recordUpdates.isEmpty()){ 
				update recordUpdates;
			}
		}
	}
	
	/**
	* @description	Relate CdeOrder__c
	* @param		entries with new Trigger data
	*/
	public void relateCdeOrder(CdeOrder__c [] entries ){
		boolean action = false;		
		Set<Id> ids = new Set<Id>();
		for (CdeOrder__c record : entries){
			if(record.Account__c  == null){
				action = true;		
				ids.add(record.Request__c);
			}
		}
		
		if(action){		
			List<CdeOrder__c> records = [SELECT Id, Request__c, Account__c FROM CdeOrder__c Where Request__c =: ids];	
			AccountRelationOfRequestChields relation = new AccountRelationOfRequestChields();
			Map<Id, Request__c> requestMap = relation.readRequest(ids);
			List<CdeOrder__c> recordUpdates = new List<CdeOrder__c>();
			if(requestMap != null){
				for (CdeOrder__c record : records){
					if(record.Account__c == null && requestMap.containsKey(record.Request__c)){
						if(requestMap.get(record.Request__c).Account__c != null){
							record.Account__c = requestMap.get(record.Request__c).Account__c;
							recordUpdates.add(record);
						}
					}
				} 
			}	
			if(!recordUpdates.isEmpty()){ 
				update recordUpdates;
			}
		}
	}
	
	/**
	* @description	Relate Visitation__c
	* @param		entries with new Trigger data
	*/
	public void relateVisitation(Visitation__c [] entries ){
		boolean action = false;		
		Set<Id> ids = new Set<Id>();
		for (Visitation__c record : entries){
			if(record.Account__c  == null){
				action = true;		
				ids.add(record.Request__c);
			}
		}
		
		if(action){		
			List<Visitation__c> records = [SELECT Id, Request__c, Account__c FROM Visitation__c Where Request__c =: ids];
			AccountRelationOfRequestChields relation = new AccountRelationOfRequestChields();
			Map<Id, Request__c> requestMap = relation.readRequest(ids);
			List<Visitation__c> recordUpdates = new List<Visitation__c>();
			if(requestMap != null){
				for (Visitation__c record : records){
					if(record.Account__c == null && requestMap.containsKey(record.Request__c)){
						if(requestMap.get(record.Request__c).Account__c != null){
							record.Account__c = requestMap.get(record.Request__c).Account__c;
							recordUpdates.add(record);
						}
					}
				} 
			}
			if(!recordUpdates.isEmpty()){ 
				update recordUpdates;
			}
		}
	}
	
	/**
	* @description	Relate RedSurveyResult__c
	* @param		entries with new Trigger data
	*/
	public void relateRedSurveyResult(RedSurveyResult__c [] entries ){
		boolean action = false;
		Set<Id> ids = new Set<Id>();
		for (RedSurveyResult__c record : entries){
			if(record.Account__c  == null){
				action = true;		
				ids.add(record.Request__c);
			}
		}
		
		if(action){
			List<RedSurveyResult__c> records = [SELECT Id, Request__c, Account__c FROM RedSurveyResult__c Where Request__c =: ids];
			AccountRelationOfRequestChields relation = new AccountRelationOfRequestChields();
			Map<Id, Request__c> requestMap = relation.readRequest(ids);
			List<RedSurveyResult__c> recordUpdates = new List<RedSurveyResult__c>();
			if(requestMap != null){
				for (RedSurveyResult__c record : records){
					if(record.Account__c == null && requestMap.containsKey(record.Request__c)){
						if(requestMap.get(record.Request__c).Account__c != null){
							record.Account__c = requestMap.get(record.Request__c).Account__c;
							recordUpdates.add(record);
						}
					}
				} 
			}
			
			if(!recordUpdates.isEmpty()){ 
				update recordUpdates;
			}
		}
	}

}
