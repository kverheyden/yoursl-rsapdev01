/**
* @author		Andre Mergen
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
*
* @description	Transfer the following objects from the Request to the Account:
*				MarketingAttribute
*				CdeOrder
*				SalesOrder__c
*				Visitation
*				RedSurveyResult
*
* @date			01.07.2014
*
* Timeline:
* Name               	Datetime					Description
* Andre Mergen			01.07.2014 09:00			Class created
* Austen Buennemann		03.09.2014 09:59			ckeck and comments
* Bernd Werner			21.11.2014 11:00			Added two entry functions to enricht data from STC and Sales Office for ecom
*/

public with sharing class AccountRequestDataTransfer {
	
	Map<Id,Account>		masterAccMap 		= new Map<Id,Account>(); 
	Map<Id,Request__c>	masterRequestMap	= new Map<Id,Request__c>();
	Map<Id,Account>		mapAccountMap		= new Map<Id,Account>();
	Map<Id,Account>		mapAccountUpdate	= new Map<Id,Account>();
	Set<String>			setRelevantSTCs		= new Set<String>();
	Map<String,Id>		mapSubtradechannel	= new Map<String,Id>();

	boolean addToList;
	Id properReqId;
	Account enrTmpAcc = new Account();
	
	// Marketing Attribute  -- public LIST<###> list = new public LIST<###>();
	public LIST<CdeOrder__c> 		listCdeOrders 					= new LIST<CdeOrder__c>();
	public LIST<SalesOrder__c> 		listSalesOrder 					= new LIST<SalesOrder__c>();
	public LIST<Visitation__c> 		listVisitations 				= new  LIST<Visitation__c>();
	public LIST<RedSurveyResult__c> listRedSurveyResults 			= new LIST<RedSurveyResult__c>();
	public LIST<Request__c> 		listRequestsMarketingAttribute	= new LIST<Request__c>();

	// update List?s
	public LIST<CdeOrder__c> 		listUPDCdeOrders 		= new LIST<CdeOrder__c>();
	public LIST<SalesOrder__c> 		listUPDSalesOrder 		= new LIST<SalesOrder__c>();
	public LIST<Visitation__c> 		listUPDVisitations 		= new LIST<Visitation__c>();
	public LIST<RedSurveyResult__c> listUPDRedSurveyResults = new LIST<RedSurveyResult__c>();
	public List<Request__c>			listUPDRequest			= new List<Request__c>();
	public List<Account>			listStcUpdate			= new List<Account>();

	//AccountMarketingAttribute__c
	public LIST<AccountMarketingAttribute__c> listINSERTAccountMarketingAttribute = new LIST<AccountMarketingAttribute__c>();


	/*******************************************************************************
	* @description	enrich account data wit information of the subtrade channel and Sales office
	* @param		accTriggerList List of Accounts
	********************************************************************************/
	public void insertEntry (Map<Id,Account> mapAccNew){
		listStcUpdate	= new List<Account>();
		for(Account tmpAcc : mapAccNew.values()){
			If(tmpAcc.Subtradechannel__c!=null && !setRelevantSTCs.contains(tmpAcc.Subtradechannel__c)){
				setRelevantSTCs.add(tmpAcc.Subtradechannel__c);
			}
			If(tmpAcc.Subtradechannel__c!=null){
				listStcUpdate.add(tmpAcc);
			}
		}
		enrichSTC ();
		enrichData(mapAccNew.values());
	}


	/*******************************************************************************
	* @description	enrich account data wit information of the subtrade channel and Sales office
	* @param		accTriggerList List of Accounts
	********************************************************************************/
	public void updateEntry (Map<Id,Account> mapAccNew, Map<Id,Account> mapAccOld){
		List<Account>listTransfer	= new List<Account>();
		listStcUpdate	= new List<Account>();
		// Collect all relevant Accounts and STCs for STC Assignment
		for(Id tmpId : mapAccNew.keySet()){
			if(mapAccNew.get(tmpId).Subtradechannel__c!=mapAccOld.get(tmpId).Subtradechannel__c && mapAccNew.get(tmpId).Subtradechannel__c !=null){
				if(!setRelevantSTCs.contains(mapAccNew.get(tmpId).Subtradechannel__c)){
					setRelevantSTCs.add(mapAccNew.get(tmpId).Subtradechannel__c);
				}
				listStcUpdate.add(mapAccNew.get(tmpId));
			}

			if(mapAccNew.get(tmpId).RequestID__c!=mapAccOld.get(tmpId).RequestID__c){
				listTransfer.add(mapAccNew.get(tmpId));
			}
		}
		enrichSTC();
		if(listTransfer.size()>0){
			enrichData(listTransfer);
		}
	}


	/*******************************************************************************
	* @description	Enrich a list of Accounts with subtradechannel relation
	* @param		List of Accounts, Set of relevant STCs
	********************************************************************************/
	public void enrichSTC (){
		List<Account> 	listFinalUpdate = new List<Account>();
		Account 		accToUpdate 	= new Account();
		collectSTCs ();
		for(Account tmpAccEnr : listStcUpdate){
			If(mapSubtradechannel.containsKey(tmpAccEnr.Subtradechannel__c)){
				accToUpdate 					= new Account();
				accToUpdate.Id					= tmpAccEnr.Id;
				accToUpdate.Marketsegment__c	= mapSubtradechannel.get(tmpAccEnr.Subtradechannel__c);
				listFinalUpdate.add(accToUpdate);
			}
		}

		// if list is not empty, fire Update
		If(listFinalUpdate.size()>0){
			update listFinalUpdate;
		}
	}


	/*******************************************************************************
	* @description	Collect all relevant STCs with ID an Name
	* @param		Set of Strings of subtradechannel names
	********************************************************************************/
	public void collectSTCs (){
		for(Subtradechannel__c tmpSTC : [Select Id,Name,Subtradechannel__c from Subtradechannel__c where Subtradechannel__c in : setRelevantSTCs]){
			If(!mapSubtradechannel.containsKey(tmpSTC.Subtradechannel__c)){
				mapSubtradechannel.put(tmpSTC.Subtradechannel__c,tmpSTC.Id);
			}
		}
	}


	/*******************************************************************************
	* @description	enrich data to account
	* @param		accTriggerList List of Accounts
	********************************************************************************/
	public void enrichData(List<Account> accTriggerList){
		
		
		// First convert IDs
		for(Account tmpAcc : accTriggerList){
			if(tmpAcc.RequestID__c!=null && tmpAcc.RequestID__c!=''){
				// Id properId = Id.valueOf(String s);
				addToList = true;
				try{
					properReqId = Id.valueOf(tmpAcc.RequestID__c);
					system.debug('###ID'+properReqId);
				}
				catch (Exception e){addToList = false; system.debug('###Error:'+e.getMessage());}
				if(addToList == true){
					masterAccMap.put(properReqId,tmpAcc);
				}
				mapAccountMap.put(tmpAcc.Id,tmpAcc);
			}
		}
		system.debug('###MasterMap: '+masterAccMap);
		if(masterAccMap.size()>0){
			for(Request__c tmpReq : [SELECT 	Id, MainBrandBeer__c, ContractObligationBeer__c, EndDateBeerContractBinding__c,Account__c,RelatedAccount__c,
												MainBrandWheatBeer__c, MainBrandCola__c, ContractObligationCola__c, EndDateColaContractBinding__c,
												MainBrandWater__c, SecondMainBrandWater__c, ContractObligationWater__c, EndDateWaterContractBinding__c,
												UnrestrictedDelivery__c, PricingAnnualOrderDiscount__c, PricingShipmentQuantityDiscount__c,
												PaymentDirectDebit__c, CentralEffort__c, IntroductionCateringBottle__c, SponsoringDiscountLevel__c,
												PricingCaseCompensation__c, PricingUnitThroughputDiscount__c, 
												PricingCustomerOwnedUnit__c,IsInLowEmissionZone__c,LowEmissionZoneType__c,
												PricingWaterConceptDiscount__c, PricingStartDate__c, MarketingAttributeIDsCSV__c,RequestProcessed__c
									 FROM Request__c WHERE Id in : masterAccMap.keySet() OR RelatedAccount__c in : mapAccountMap.keySet()
			]){
				masterRequestMap.put(tmpReq.Id,tmpReq);
				
				
				// If the Request ID does not appear in our master map, check if it is pending
				if(!masterAccMap.containsKey(tmpReq.Id)){
					// Decide new Account or already existing in updatemap
					if(!mapAccountUpdate.containsKey(tmpReq.RelatedAccount__c)){
						enrTmpAcc 								= new Account();
						enrTmpAcc.Id							= tmpReq.RelatedAccount__c;
						enrTmpAcc.HasPendingChangeRequest__c	= false;
					}
					else{
						enrTmpAcc = mapAccountUpdate.get(tmpReq.RelatedAccount__c);
					}
					if(tmpReq.Account__c==null){
						enrTmpAcc.HasPendingChangeRequest__c	= true;
					}
					mapAccountUpdate.put(enrTmpAcc.Id,enrTmpAcc);
				}
				
				else if(masterAccMap.containsKey(tmpReq.Id)){					
					// Decide new Account or already existing in updatemap
					if(!mapAccountUpdate.containsKey(masterAccMap.get(tmpReq.Id).Id)){
						enrTmpAcc 								= new Account();
						enrTmpAcc.Id							= masterAccMap.get(tmpReq.Id).Id;
						enrTmpAcc.HasPendingChangeRequest__c	= false;
					}
					else{
						enrTmpAcc = mapAccountUpdate.get(masterAccMap.get(tmpReq.Id).Id);
					}
					enrTmpAcc.Request__c							= tmpReq.Id;
					// Request already processed?
					if(!tmpReq.RequestProcessed__c ){						
						enrTmpAcc.MainBrandBeer__c 						= tmpReq.MainBrandBeer__c;
						enrTmpAcc.ContractObligationBeer__c 			= tmpReq.ContractObligationBeer__c;
						enrTmpAcc.EndDateBeerContractBinding__c 		= tmpReq.EndDateBeerContractBinding__c;
						enrTmpAcc.MainBrandWheatBeer__c 				= tmpReq.MainBrandWheatBeer__c;
						enrTmpAcc.MainBrandCola__c 						= tmpReq.MainBrandCola__c;
						enrTmpAcc.ContractObligationCola__c 			= tmpReq.ContractObligationCola__c;
						enrTmpAcc.EndDateColaContractBinding__c 		= tmpReq.EndDateColaContractBinding__c;
						enrTmpAcc.MainBrandWater__c 					= tmpReq.MainBrandWater__c;
						enrTmpAcc.SecondMainBrandWater__c 				= tmpReq.SecondMainBrandWater__c;
						enrTmpAcc.ContractObligationWater__c 			= tmpReq.ContractObligationWater__c;
						enrTmpAcc.EndDateWaterContractBinding__c 		= tmpReq.EndDateWaterContractBinding__c;
						enrTmpAcc.UnrestrictedDelivery__c 				= tmpReq.UnrestrictedDelivery__c;
						enrTmpAcc.PricingAnnualOrderDiscount__c 		= tmpReq.PricingAnnualOrderDiscount__c;
						enrTmpAcc.PricingShipmentQuantityDiscount__c 	= tmpReq.PricingShipmentQuantityDiscount__c;
						enrTmpAcc.PaymentDirectDebit__c 				= tmpReq.PaymentDirectDebit__c;
						enrTmpAcc.CentralEffort__c 						= tmpReq.CentralEffort__c;
						enrTmpAcc.IntroductionCateringBottle__c 		= tmpReq.IntroductionCateringBottle__c;
						enrTmpAcc.PricingSponsoringDiscountLevel__c		= tmpReq.SponsoringDiscountLevel__c;
						enrTmpAcc.PricingCaseCompensation__c 			= tmpReq.PricingCaseCompensation__c;
						enrTmpAcc.PricingUnitThroughputDiscount__c 		= tmpReq.PricingUnitThroughputDiscount__c;
						enrTmpAcc.PricingCustomerOwnedUnit__c 			= tmpReq.PricingCustomerOwnedUnit__c;
						enrTmpAcc.PricingWaterConceptDiscount__c 		= tmpReq.PricingWaterConceptDiscount__c;
						enrTmpAcc.PricingStartDate__c 					= tmpReq.PricingStartDate__c;
						enrTmpAcc.IsInLowEmissionZone__c				= tmpReq.IsInLowEmissionZone__c;
						enrTmpAcc.TypeOfLowEmissionZone__c				= tmpReq.LowEmissionZoneType__c;
						// updateAccountList.add(enrTmpAcc);
					}
					// Write Account ID to Request
					tmpReq.Account__c			= masterAccMap.get(tmpReq.Id).Id;
					tmpReq.RequestProcessed__c	= true;
					
					listUPDRequest.add(tmpReq);
					mapAccountUpdate.put(enrTmpAcc.Id,enrTmpAcc);
				}
			}
			
			if(mapAccountUpdate.size()>0){
				update mapAccountUpdate.values();
			}
			
			gatherer();
			ModifyAndUpdate();
		}
	} // End Function
	
	
	
	/*******************************************************************************
	* @description	gather data of relevant objects
	********************************************************************************/
	public void gatherer()
	{
		// Marketing Attribute
		// Sales orders / CDE Orders
		listCdeOrders = [SELECT Id, Name, Account__c, Request__c FROM CdeOrder__c WHERE Request__c IN:masterAccMap.keySet()];
		System.debug('### listCdeOrders :'+ listCdeOrders);
		//SalesOrder
		listSalesOrder = [SELECT Id,Name,Account__c, Status__c, Request__c FROM SalesOrder__c WHERE Request__c IN:masterAccMap.keySet()];
		System.debug('### listSalesOrder : ' + listSalesOrder);
		// Visitations
		listVisitations = [SELECT Id, Name, Account__c, Request__c FROM Visitation__c WHERE Request__c IN:masterAccMap.keySet()];
		System.debug('### listVisitations :'+ listVisitations);
		// Red Survey Results		
		listRedSurveyResults = [SELECT Id, Name, Account__c, Request__c FROM RedSurveyResult__c WHERE Request__c IN:masterAccMap.keySet()];
		System.debug('### listRedSurveyResults :'+ listRedSurveyResults);
		
		// Request MarketingAttribute & RequestFields
		listRequestsMarketingAttribute = [SELECT Id, Name, MarketingAttributeIDsCSV__c,MainBrandBeer__c, ContractObligationBeer__c, EndDateBeerContractBinding__c, MainBrandWheatBeer__c, MainBrandCola__c, ContractObligationCola__c, EndDateColaContractBinding__c, MainBrandWater__c, SecondMainBrandWater__c, ContractObligationWater__c, EndDateWaterContractBinding__c, UnrestrictedDelivery__c, PricingAnnualOrderDiscount__c, PricingShipmentQuantityDiscount__c, PaymentDirectDebit__c, CentralEffort__c, IntroductionCateringBottle__c, SponsoringDiscountLevel__c, PricingCaseCompensation__c, PricingUnitThroughputDiscount__c, PricingCustomerOwnedUnit__c, PricingWaterConceptDiscount__c, PricingStartDate__c FROM Request__c WHERE ID IN:masterAccMap.keySet()];
		System.debug('### listRequestsMarketingAttribute :'+ listRequestsMarketingAttribute);
	}

	/*******************************************************************************
	* @description	modify an update data of relevant objects
	********************************************************************************/
	public void ModifyAndUpdate()
	{
		// Sales orders / CDE Orders
		if(listCdeOrders.size()>0)
		{
			for(CdeOrder__c tmpCdeOrder:listCdeOrders)
			{	
				if(masterAccMap.containsKey(tmpCdeOrder.Request__c)){
					tmpCdeOrder.Account__c = masterAccMap.get(tmpCdeOrder.Request__c).Id;
					listUPDCdeOrders.add(tmpCdeOrder);
				}
			}
		}
		
		// SalesOrder
		if(listSalesOrder.size()>0)
		{
			for(SalesOrder__c tmpSalesOrder:listSalesOrder)
			{
				if(masterAccMap.containsKey(tmpSalesOrder.Request__c)){
					tmpSalesOrder.Account__c 		= masterAccMap.get(tmpSalesOrder.Request__c).Id;
					tmpSalesOrder.Send_to_SAP__c	= true;
					listUPDSalesOrder.add(tmpSalesOrder);
				}
			}
		}
		
		// Visitations
		if(listVisitations.size()>0)
		{
			for(Visitation__c tmpVisitation:listVisitations)
			{
				if(masterAccMap.containsKey(tmpVisitation.Request__c)){
					tmpVisitation.Account__c = masterAccMap.get(tmpVisitation.Request__c).Id;
					listUPDVisitations.add(tmpVisitation);
				}
			}
		}
		
		
		// Red Survey Results
		if(listRedSurveyResults.size()>0)
		{
			for(RedSurveyResult__c tmpRedSurveyResult:listRedSurveyResults)
			{
				if(masterAccMap.containsKey(tmpRedSurveyResult.Request__c)){
					tmpRedSurveyResult.Account__c = masterAccMap.get(tmpRedSurveyResult.Request__c).Id;
					listUPDRedSurveyResults.add(tmpRedSurveyResult);
				}
			}
		}
		
		//MarketingAttribute
		if(masterRequestMap.size()>0)
		{ 
			//map<string,list<string>> mapRequestsMarketingAttribute = new map<string,list<string>>();
			// loop all Requests
			Set<Id>sMaIdSetReq 	= new Set<Id>();
			Map<Id,MarketingAttribute__c>sMaIdSetVal	= new Map<Id,MarketingAttribute__c>();
			for(Request__c tmpReq:masterRequestMap.values())
			{
				if(tmpReq.MarketingAttributeIDsCSV__c!=NULL && tmpReq.MarketingAttributeIDsCSV__c !='')
				{
					for(String MarketingAttributeID:tmpReq.MarketingAttributeIDsCSV__c.split('\\,'))
					{
						if(!sMaIdSetReq.contains(MarketingAttributeID.TRIM()))
						{
							sMaIdSetReq.add(MarketingAttributeID.TRIM());
						}	
					}
				}
			}
			
			// TODO: Check, if Marketing Attributes exists on Account and if yes, do not add them a second time
			// Check which IDs are still available
			for(MarketingAttribute__c otmpMarAtt : [Select Id, SetId__c, Attribute__c, Value__c from MarketingAttribute__c where Id IN:sMaIdSetReq])
			{
				sMaIdSetVal.put(otmpMarAtt.Id,otmpMarAtt);
			}
			for(Request__c tmpReq:masterRequestMap.values())
			{
				//Request Id as String
				String tmpStringReqId = tmpReq.Id;
				
				// loop all MarketingAttributeID of one Request
				if(tmpReq.MarketingAttributeIDsCSV__c!=NULL && tmpReq.MarketingAttributeIDsCSV__c !='' && masterAccMap.containsKey(tmpReq.Id))
				{
					
					for(String MarketingAttributeID:tmpReq.MarketingAttributeIDsCSV__c.split('\\,'))
					{
						if(sMaIdSetVal.containsKey(MarketingAttributeID.TRIM()))
						{
							AccountMarketingAttribute__c tmpAMA 	= new AccountMarketingAttribute__c();
							tmpAMA.Account__c 						= masterAccMap.get(tmpReq.Id).Id;
							tmpAMA.MarketingAttribute__c 			= MarketingAttributeID.TRIM();
							// Account__r.ID2__c + "##" + TEXT(MarketingAttribute__r.SetId__c) + "##" + TEXT(MarketingAttribute__r.Attribute__c)
							tmpAMA.UniqueKey__c						= masterAccMap.get(tmpReq.Id).ID2__c + '##' + sMaIdSetVal.get(MarketingAttributeID.TRIM()).SetId__c + '##' + sMaIdSetVal.get(MarketingAttributeID.TRIM()).Attribute__c;
							system.debug('###UniqueKey: '+tmpAMA.UniqueKey__c);
							listINSERTAccountMarketingAttribute.add(tmpAMA);
						}
					}
				}
			}
			
			
		}
		
		
		// Update
		if(listUPDCdeOrders.size()>0)
		{
			update listUPDCdeOrders;
		}
		if(listUPDSalesOrder.size()>0)
		{
			update listUPDSalesOrder;
		}
		
		if(listUPDVisitations.size()>0)
		{
			update listUPDVisitations;
		}
		
		if(listUPDRedSurveyResults.size()>0)
		{
			update listUPDRedSurveyResults;
		}
		if(listUPDRequest.size()>0){
			update listUPDRequest;
		}
		
		// insert new AccountMarketingAttribute__c
		if(listINSERTAccountMarketingAttribute.size()>0)
		{
			//insert listINSERTAccountMarketingAttribute;
			 upsert listINSERTAccountMarketingAttribute UniqueKey__c;
		}
	
	}




































}
