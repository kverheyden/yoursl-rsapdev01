/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code 
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest 
private class SCVPEquipmentControllerTest 
{

//######################################  DATA STRUCTURE ##################################################




    public static Brand__c t_brand
    {
        get
        {
            if(t_brand == null)
            {
                t_brand  = new Brand__c(
                    Name = 'GMS Test 1',
                    ID2__c = 'GMS_Test' + String.valueOf(DateTime.Now())
                );
                
                insert t_brand;
                
                System.assert(t_brand.Id != null);
            }
            
            return t_brand;
        }
        
        set;
    }
    
    public static SCProductModel__c t_prodModel
    {
        get
        {
            if(t_prodModel == null)
            {
                t_prodModel = new SCProductModel__c(
                    Brand__c        = t_brand.Id,
                    Name            = 'VAM climaVAIR [026]',
                    Country__c      = 'NL',
                    UnitClass__c    = SCfwConstants.DOMVAL_PRODUCTUNITCLASS_STANDARD,
                    UnitType__c     = SCfwConstants.DOMVAL_PRODUCTUNITTYPE_DEFAULT,
                    Group__c        = '268',
                    Power__c        = '550'
                );
                
                insert t_prodModel;
                
                System.assert(t_prodModel.Id != null);
            }
            
            return t_prodModel;
        }
        
        set;
    }
    
    public static SCArticle__c t_article
    {
        get
        {
            if(t_article == null)
            {
                t_article = new SCArticle__c(
                    Name        = 'TEST-000251',
                    Text_en__c  = 'Unit Test Article',
                    TaxType__c  = SCfwConstants.DOMVAL_TAXARTICLE_FULL,
                    Class__c    = SCfwConstants.DOMVAL_ARTICLECLASS_PRODUCT,
                    
                    AvailabilityType__c = SCfwConstants.DOMVAL_MATERIALDISPO_A
                );
                
                insert t_article;
                
                System.assert(t_article.Id != null);
            }
            
            return t_article;
        }
        
        set;
    } 
   
    public static SCInstalledBaseLocation__c t_location
    {
        get
        {
            if(t_location == null)
            {
                t_location = new SCInstalledBaseLocation__c(
                    BuildingDate__c = (Date.today() - 1000),
                    Street__c = 'Moormanweg',
                    HouseNo__c = '6',
                    PostalCode__c = '9831 NK',
                    City__c = 'Aduard',
                    Country__c = 'NL',
                    GeoX__c = 6.454447,
                    GeoY__c = 53.253582,
                    LocName__c = 'Test Location',
                    Status__c = 'Active'
                );
                
                insert t_location;
                
                System.assert(t_location.Id != null);
            }
            
            return t_location;
        }
        
        set;
    }
    
  
    public static SCInstalledBase__c GetInstalledBase()
    {
        return (new SCInstalledBase__c(
            Article__c      = t_article.Id,
            Brand__c        = t_prodModel.Brand__c,
            ProductModel__c = t_prodModel.Id,
            ProductGroup__c = t_prodModel.Group__c,
            ProductPower__c = t_prodModel.Power__c,
            SerialNo__c     = '00305005',
            Status__c       = 'active',
            Type__c         = 'Appliance',
            ProductSkill__c = 'PS1',
            
            InstalledBaseLocation__c    = t_location.Id,
            InstalledBaseLocation__r    = t_location,
            ProductUnitClass__c         = t_prodModel.UnitClass__c,
            ProductUnitType__c          = t_prodModel.UnitType__c
        ));
    }




   



	




//######################################   TEST METHODS	 ##################################################
    
    /**
    * 
    * 
    */
    @isTest
    public static void SCVPEquipmentControllerTester()
    {
		SCVPEquipmentController controller = new SCVPEquipmentController();
		
		SCVEndor__c t_vendor = new SCVEndor__c(
        Name            = '0123456789',
        Name1__c        = 'My test t_vendor',
        Country__c      = 'DE',
        PostalCode__c   = '33100',
        City__c         = 'Paderborn',
        Street__c       = 'Bahnhofstrt',
        LockType__c     = '5001',
        Status__c       = 'Active'
		);
        
        insert t_vendor;
       
        System.assert(t_vendor.Id != null);
        System.debug('###TESTMETHOD - t_vendor: ' + t_vendor.id);        
        
        
        
        SCPlant__c t_plant = new SCPlant__c(
            Name = '9100',
            Info__c = 'Test plant'
        );
		
		insert t_plant; 

        SCStock__c t_stock = new SCStock__c(
            Name        = '9101',
            Info__c     = 'Test stock 1',
            Plant__c    = t_plant.Id,
            Vendor__c   = t_vendor.Id,
            Type__c     = 'EQReadyToMarket'
        );
        
         insert t_stock;
        
        SCStock__c t_stock2 = new SCStock__c(
            Name        = '9102',
            Info__c     = 'Test stock 2',
            Plant__c    = t_plant.Id,
            Vendor__c   = t_vendor.Id,
            Type__c     = 'EQReadyForScrapping'
        );
        
        insert t_stock2;

		System.assert(t_stock2.Id != null);
		System.assert(t_stock.Id != null);


        
        SCInstalledBase__c t_installBase = GetInstalledBase();
        t_installBase.stock__c = t_stock.id; 
        insert t_installBase;
        
        System.assert(t_installBase.Id != null);
        
        
        SCInstalledBase__c t_installBase2 = GetInstalledBase();
        t_installBase2.stock__c = t_stock2.id; 
        insert t_installBase2;
        
        System.assert(t_installBase2.Id != null);        
        

		Profile t_profile = [ SELECT Id FROM Profile WHERE Name IN ('Systemadministrator', 'System Administrator') LIMIT 1];

		        System.assert(t_profile != null);
		        System.assert(t_profile.Id != null);

        		User t_user = new User(
	            Alias = 'GMSTest1', 
	            FirstName = 'GMS',
	            LastName = 'Testuser 1', 
	            Username = 'gmstest1@gms-online.de', 
	            Email = 'gmstest1@gms-online.de', 
	            CommunityNickname = 'gmstest1', 
	            Street = 'Karl Schurz Str.',
	            PostalCode = '33100',
	            City = 'Paderborn',
	            Country = 'DE',
	            GEOX__c = -0.10345,
	            GEOY__c = 51.49250,
	            TimeZoneSidKey = 'Europe/Berlin', 
	            LocaleSidKey = 'de_DE_EURO', 
	            EmailEncodingKey = 'UTF-8',
	            LanguageLocaleKey = 'en_US',
	            
	            ProfileId = t_profile.Id
        		);
        
        		insert t_user;


        SCVendorUser__c vendorUser = new SCVendorUser__c(
            User__c   = t_user.Id,
            Vendor__c = t_vendor.id
        );
        
        insert vendorUser;
        
        System.assert(vendorUser.Id != null);
        
		test.starttest();
		
		controller.selectedVendor = new SCOrderExternalAssignment__c();
		controller.selectedVendor.Vendor__c = t_vendor.id; 
		controller.init();
		
		controller.getEquipmentType();
		controller.getEquipmentStatus();
		controller.getEquipment();		
		
		controller.g_pagesettings.scrapreportid__c = '001';
		
		
		//HelperForEmptyItems
		controller.checkForItems(); 
	
        List<SCVPEquipmentController.InnerStocks> InnerListTest = controller.g_reBookStocks;
        
        System.assert(InnerListTest != null); 
        System.assert(!InnerListTest.isEmpty()); 
        System.debug('###SCVPEquipmentControllerTest - InnerListTest: ' + InnerListTest);
        System.debug('###SCVPEquipmentControllerTest - InnerListTest[0]: ' + InnerListTest[0]);
        
        //Rebook with 2 Items
        controller.selectedItemIDs = t_installBase.id + ',' + t_installBase.id;        
        InnerListTest[0].rebook();
  		controller.ConfirmScrapping();
  		controller.g_pagesettings.ScrapReportID__c = '001';
  		controller.gotoreport();  
  		controller.chosenstockid = t_stock.id;
  		controller.gotoreport();  
  		
  		//Rebook with 1 Items
        controller.selectedItemIDs = t_installBase.id;
        InnerListTest[0].rebook();
        controller.ConfirmScrapping();
        
        //Rebook with 0 Items
        controller.selectedItemIDs = '';
        InnerListTest[0].rebook(); 
        controller.ConfirmScrapping();       
        
        
        
        controller.checkForItems(); 
        controller.sortField = 'name';
        controller.sortOrder = 'ASC';
 		controller.selectedStatusValues.add('Test');
		controller.selectedTypeValues.add('Test');
		controller.helperEqip.brand__c = t_brand.id;
		controller.helperMatNr = '1';
		controller.helperMatSD = '2'; 

		// ReDo Search
		controller.filterUpdateCustom();        
        controller.getEquipment();
        controller.sortField = 'name';
        controller.reloadForSort = true;
        controller.filterQuery = true;
        controller.getEquipment();

        controller.sortField = 'id';
        controller.reloadForSort = true;
        controller.filterQuery = true;
        controller.getEquipment();
  
		//CATCH ME IF U CAN
		controller.isVendor =null;
		controller.fetchStocks();

		controller.selectedTypeValues = null;
		controller.getEquipment();
		


		
		test.stoptest();
 
 
    }


  /**
    * 
    * 
    */
    @isTest(SeeAllData=false)
    public static void SCVPEquipmentControllerTester2()
    {
		SCVPEquipmentController controller = new SCVPEquipmentController();

		SCVEndor__c t_vendor = new SCVEndor__c(
        Name            = '0123456789',
        Name1__c        = 'My test t_vendor',
        Country__c      = 'DE',
        PostalCode__c   = '33100',
        City__c         = 'Paderborn',
        Street__c       = 'Bahnhofstrt',
        LockType__c     = '5001',
        Status__c       = 'Active'
		);
        
        insert t_vendor;
		
        SCPlant__c t_plant = new SCPlant__c(
            Name = '9100',
            Info__c = 'Test plant'
        );
		
		insert t_plant; 

        SCStock__c t_stock = new SCStock__c(
            Name        = '9101',
            Info__c     = 'Test stock 1',
            Plant__c    = t_plant.Id,
            Vendor__c   = t_vendor.Id,
            Type__c     = 'EQReadyToMarket'
        );
        
         insert t_stock;
        
        SCStock__c t_stock2 = new SCStock__c(
            Name        = '9102',
            Info__c     = 'Test stock 2',
            Plant__c    = t_plant.Id,
            Vendor__c   = t_vendor.Id,
            Type__c     = 'EQReadyForScrapping'
        );
        
        insert t_stock2;

		System.assert(t_stock2.Id != null);
		System.assert(t_stock.Id != null);


        
        
        SCInstalledBase__c t_installBase = GetInstalledBase();
        t_installBase.stock__c = t_stock.id; 
        insert t_installBase;
        
        System.assert(t_installBase.Id != null);
        
        
        SCInstalledBase__c t_installBase2 = GetInstalledBase();
        t_installBase2.stock__c = t_stock2.id; 
        insert t_installBase2;
        
        System.assert(t_installBase2.Id != null);        
        

		Profile t_profile = [ SELECT Id FROM Profile WHERE Name IN ('Systemadministrator', 'System Administrator') LIMIT 1];

		        System.assert(t_profile != null);
		        System.assert(t_profile.Id != null);

        		User t_user = new User(
	            Alias = 'GMSTest1', 
	            FirstName = 'GMS',
	            LastName = 'Testuser 1', 
	            Username = 'gmstest1@gms-online.de', 
	            Email = 'gmstest1@gms-online.de', 
	            CommunityNickname = 'gmstest1', 
	            Street = 'Karl Schurz Str.',
	            PostalCode = '33100',
	            City = 'Paderborn',
	            Country = 'DE',
	            GEOX__c = -0.10345,
	            GEOY__c = 51.49250,
	            TimeZoneSidKey = 'Europe/Berlin', 
	            LocaleSidKey = 'de_DE_EURO', 
	            EmailEncodingKey = 'UTF-8',
	            LanguageLocaleKey = 'en_US',
	            
	            ProfileId = t_profile.Id
        		);
        
        		insert t_user;


        SCVendorUser__c vendorUser = new SCVendorUser__c(
            User__c   = t_user.Id,
            Vendor__c = t_vendor.id
        );
        
        insert vendorUser;
        
        System.assert(vendorUser.Id != null);
        
		
		test.starttest();
		System.runAs(t_user)
		{

			controller.init();

			controller.getEquipmentType();
			controller.getEquipmentStatus();
			controller.getEquipment();		
	
			//HelperForEmptyItems
			controller.checkForItems(); 
		
	        List<SCVPEquipmentController.InnerStocks> InnerListTest = controller.g_reBookStocks;
	        
	        System.assert(InnerListTest != null); 
	        System.assert(!InnerListTest.isEmpty()); 
	        System.debug('###SCVPEquipmentControllerTest - InnerListTest: ' + InnerListTest);
	        System.debug('###SCVPEquipmentControllerTest - InnerListTest[0]: ' + InnerListTest[0]);
	        
	        //Rebook with 2 Items
	        controller.selectedItemIDs = t_installBase.id + ',' + t_installBase.id;        
	        InnerListTest[0].rebook();
	  		
	  		//Rebook with 1 Items
	        controller.selectedItemIDs = t_installBase.id;
	        InnerListTest[0].rebook();
	        
	        //Rebook with 0 Items
	        controller.selectedItemIDs = '';
	        InnerListTest[0].rebook();        
	        
	        controller.checkForItems(); 
	        
	        controller.sortOrder = 'ASC';
	 		controller.selectedStatusValues.add('Test');
			controller.selectedTypeValues.add('Test');
			controller.helperEqip.brand__c = t_brand.id;
			controller.helperMatNr = '1';
			controller.helperMatSD = '2'; 
	
			// ReDo Search
			controller.filterUpdateCustom();        
	        controller.getStocks();			
			
			controller.setcon = null;
			controller.getEquipment();
	
			controller.chosenstockid = null;
			controller.getEquipment();
			
					
		}


		test.stoptest();
 

    }    

}
