/*
 * @(#)SCutilSemaphore.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Implements a simple semaphore mechanism. ### BETA ###
 * 
 * @author narmbruster@gms-online.de
 */
public class SCutilSemaphore
{
   /*
    *
    */
    public static SCSemaphore__c lock(String semname)
    {    
    /*
        SCSemaphore__c sem;
        try
        {
            // try to get the country specific semuence 
            sem = [select Id, Name, value__c from SCSemaphore__c where Name = :semname for update];
        }
        catch (QueryException e)
        {
            System.debug('SCutilSemaphore lock [' + semname + '] ' + e);
        }
        
        // if the semaphore does not exist we create a singleton record
        if(sem == null)
        {
            SCSemaphore__c tmp = new SCSemaphore__c(name = semname);    
            insert tmp;
            // now lock the record
            sem = [select Id, Name, value__c from SCSemaphore__c where Name = :semname for update];
        }
        
        if(sem != null)
        {
            sem.value__c = 'locked';
            update sem;
        }
        return sem;
*/
        return null;        
    }
    
    public static void unlock(SCSemaphore__c sem)
    {    
        if(sem != null)
        {
            sem.value__c = null;
            update sem;
        }
    }

}
