/*
 * @(#)SCutilInterventionValidation.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
public virtual class SCutilInterventionValidation
{
    /**
     * Default constructor
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public SCutilInterventionValidation()
    {
    }

    /**
     * Do the actual validation in a defined order
     *
     * @throws SCutilInterventionValidationException
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public virtual void validate(SCboOrder boOrder, SCStock__c stock)
    {
        validateOrder(boOrder);

        for (SCboOrderItem item : boOrder.boOrderItems)
        {
            validateSerialNumber(item.orderItem.InstalledBase__r);
            validateProduct(item.orderItem.InstalledBase__r);
            validateInstalledBase(item.orderItem.InstalledBase__r);
            validateSafetyStatus(item.orderItem.InstalledBase__r);
//            validateTimes()
        }
        
        validateRepairCodes(boOrder);
        validateOrderLines(boOrder);
        validateMaterialManagement(boOrder, stock);
    }
  
    /**
     * Validate mandatory fireld direct in the object SCOrder.
     *
     * @param boOrder    the business object
     * @throws SCutilInterventionValidationException
     * @author Alexander Wagner <awagner@gms-online.de>
     */
    public void validateOrder(SCboOrder boOrder)
    {
        String lastError;
        
        if (!SCbase.isSet(boOrder.order.Type__c))
        {
            lastError = System.Label.SC_msg_MissingOrderType;
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
                                                          System.Label.SC_msg_MissingOrderType);
            ApexPages.addMessage(msg);
        } // if (!SCbase.isSet(boOrder.order.Type__c))
        
        
        
        Decimal minDuration = SCApplicationSettings__c.getInstance().ORDERCREATION_MINDURATION__c;
        if (boOrder.order.Duration__c < minDuration)
        {
            lastError = System.Label.SC_msg_OrderDurationToSmall;
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
                                                          System.Label.SC_msg_OrderDurationToSmall + ' ' + minDuration.intValue());
            ApexPages.addMessage(msg);
        } // if (boOrder.order.Duration__c < minDuration)
        
        String paymentMode = SCServiceProcessSettings__c.getInstance().PAYMENT_MODE__c;
        Boolean isPrePayment = ((null != paymentMode) ? paymentMode.equalsIgnoreCase('PREPAYMENT') : false);
        
        if (!SCbase.isSet(boOrder.order.InvoicingType__c))
        {
            lastError = System.Label.SC_msg_MissingInvoicingType;
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
                                                          System.Label.SC_msg_MissingInvoicingType);
            ApexPages.addMessage(msg);
        } // if (!SCbase.isSet(boOrder.order.InvoicingType__c))
        else if ((boOrder.order.InvoicingType__c == SCfwConstants.DOMVAL_INVOICINGTYPE_CONTRACT) && 
                 !SCBase.isSet(boOrder.order.Contract__c) && !isPrePayment)
        {
            lastError = System.Label.SC_msg_WrongOrdInvTypeContract;
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
                                                          System.Label.SC_msg_WrongOrdInvTypeContract);
            ApexPages.addMessage(msg);
        } // else if ((boOrder.order.InvoicingType__c == SCfwConstants.DOMVAL_INVOICINGTYPE_CONTRACT) && 
        
        if (!SCbase.isSet(boOrder.order.InvoicingSubtype__c))
        {
            lastError = System.Label.SC_msg_MissingInvoicingSubtype;
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
                                                          System.Label.SC_msg_MissingInvoicingSubtype);
            ApexPages.addMessage(msg);
        } // if (!SCbase.isSet(boOrder.order.InvoicingSubtype__c))
        
        /* CCEAG not required as we have the billing indicator matrix
        if (!SCbase.isSet(boOrder.order.PaymentType__c) && 
            (boOrder.order.InvoicingType__c != SCfwConstants.DOMVAL_INVOICINGTYPE_GUARANTEE))
        {
            lastError = System.Label.SC_msg_MissingPaymentType;
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
                                                          System.Label.SC_msg_MissingPaymentType);
            ApexPages.addMessage(msg);
        } 
        else 
        */
        if ((boOrder.order.PaymentType__c == SCfwConstants.DOMVAL_PAYMENTTYPE_CONTRACT) && 
                 !SCBase.isSet(boOrder.order.Contract__c) && !isPrePayment)
        {
            lastError = System.Label.SC_msg_WrongOrdPayTypeContract;
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
                                                          System.Label.SC_msg_WrongOrdPayTypeContract);
            ApexPages.addMessage(msg);
        } // else if ((boOrder.order.PaymentType__c == SCfwConstants.DOMVAL_PAYMENTTYPE_CONTRACT) && 
        
        if (('Service' == boOrder.order.UsedBy__c) && 
            ((null == boOrder.boOrderItems) || (boOrder.boOrderItems.size() == 0)))
        {
            lastError = System.Label.SC_msg_MissingOrderItem;
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
                                                          System.Label.SC_msg_MissingOrderItem);
            ApexPages.addMessage(msg);
        } // if (('Service' == boOrder.order.UsedBy__c) && ...

        if (ApexPages.hasMessages())
        {
            // duplicates are deleted, but an empty exception message is 
            // a different message
            throw new SCutilInterventionValidationException(lastError);
        } // if (ApexPages.hasMessages())
    } // validateOrder

    /**
     * Validate if a serial number is set and in case is valid.
     * If not set the serial number exception has to be filled.
     *
     * @param installedBase Installed Base to validate
     * @throws SCutilInterventionValidationException
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public void validateSerialNumber(SCInstalledBase__c installedBase)
    {
        // Either the serial number or the exception field has to be filled
        if ((installedBase.SerialNo__c == null || 
             installedBase.SerialNo__c.length() == 0) &&
            (installedBase.SerialNoException__c == null ||
             installedBase.SerialNoException__c.length() == 0))
        {
            throw new SCutilInterventionValidationException(System.Label.SC_msg_ValidateSerialNumber);
        }
        
        if (installedBase.SerialNo__c != null && 
            installedBase.SerialNo__c.length() > 0)
        {
            if (!SCutilSerialNumber.isValidSerialNumber(installedBase.SerialNo__c))
            {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
                                                              System.Label.SC_msg_InvalidSerialNumber);
                ApexPages.addMessage(msg);
                throw new SCutilInterventionValidationException(System.Label.SC_msg_InvalidSerialNumber);
            }

            // additional checks if the serial no and the article match could be added here
            //SCutilSerialNumber serialNo = new SCutilSerialNumber(installedBase.SerialNo__c);
        }
    }
    
    /**
     * Validate if the desired fields Brand, ProductUnitClass, ProductUnitType,
     * ProductGroup, ProductPower and ProductEnergy are set in case the 
     * Product Model is not set.
     *
     * @param installedBase Installed Base to validate
     * @throws SCutilInterventionValidationException
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public void validateProduct(SCInstalledBase__c installedBase)
    {
        String lastError;
        
        if (installedBase.ProductModel__c == null) 
        {
            if (installedBase.Brand__c == null)
            {
                lastError = System.Label.SC_msg_MissingBrand;
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
                                                              System.Label.SC_msg_MissingBrand);
                ApexPages.addMessage(msg);
            }
            
            if (installedBase.ProductUnitClass__c == null ||
                installedBase.ProductUnitClass__c.length() == 0)
            {
                lastError = System.Label.SC_msg_MissingProductUnitClass;
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
                                                              System.Label.SC_msg_MissingProductUnitClass);
                ApexPages.addMessage(msg);
            }
            
            if (installedBase.ProductUnitType__c == null ||
                installedBase.ProductUnitType__c.length() == 0)
            {
                lastError = System.Label.SC_msg_MissingProductUnitType;
                lastError = System.Label.SC_msg_MissingProductUnitType;
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
                                                              System.Label.SC_msg_MissingProductUnitType);
                ApexPages.addMessage(msg);
            }
            
            if (installedBase.ProductGroup__c == null ||
                installedBase.ProductGroup__c.length() == 0)
            {
                lastError = System.Label.SC_msg_MissingProductGroup;
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
                                                              System.Label.SC_msg_MissingProductGroup);
                ApexPages.addMessage(msg);
            }
            
            if (installedBase.ProductPower__c == null || 
                installedBase.ProductPower__c.length() == 0)
            {
                lastError = System.Label.SC_msg_MissingProductPower;
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
                                                              System.Label.SC_msg_MissingProductPower);
                ApexPages.addMessage(msg);
            }
            
            if (installedBase.ProductEnergy__c == null ||
                installedBase.ProductEnergy__c.length() == 0)
            {
                lastError = System.Label.SC_msg_MissingProductEnergy;
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
                                                              System.Label.SC_msg_MissingProductEnergy);
                ApexPages.addMessage(msg);
            }
            
            if (ApexPages.hasMessages())
            {
                // duplicates are deleted, but an empty exception message is 
                // a different message
                throw new SCutilInterventionValidationException(lastError);
            }
        }
    }
    
    /**
     * Validate if the Installation date is set.
     *
     * @param installedBase Installed Base to validate
     * @throws SCutilInterventionValidationException
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public void validateInstalledBase(SCInstalledBase__c installedBase)
    {
        System.debug('#### validateInstalledBase(): InstallationDate -> ' + installedBase.InstallationDate__c);
        System.debug('#### validateInstalledBase(): System.today -> ' + System.today());
        if ((installedBase.InstallationDate__c == null) ||
            (installedBase.InstallationDate__c > System.today()))
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
                                                          System.Label.SC_msg_MissingInstallationDate);
            ApexPages.addMessage(msg);
        }
        
        if (ApexPages.hasMessages())
        {
            throw new SCutilInterventionValidationException(System.Label.SC_msg_MissingInstallationDate);
        }
    }
    
    /**
     * Validate the safety status settings.
     * REMARK: For now this is a stub.
     *
     * @param installedBase Installed Base to validate
     * @throws SCutilInterventionValidationException
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public void validateSafetyStatus(SCInstalledBase__c installedBase)
    {
   
    }
    
    /**
     * Validate if labour times are added
     * REMARK: For now this is a stub.
     *
     * @param installedBase Installed Base to validate
     * @throws SCutilInterventionValidationException
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public void validateTimes()
    {

    }
    
    /**
     * Validate the order lines.
     *
     * @param installedBase Installed Base to validate
     * @throws SCutilInterventionValidationException
     * @author Alexander Wagner <awagner@gms-online.de>
     */
    public void validateOrderLines(SCboOrder boOrder)
    {
        List<SCboOrderLine> allOrderLines = new List<SCboOrderLine>();
        
        // add order lines attached to the order itself
        allOrderLines.addAll(boOrder.boOrderLines);
        // add all order lines for all order items
        for (SCboOrderItem item : boOrder.boOrderItems)
        {
            allOrderLines.addAll(item.boOrderLines);
        }
        
        String paymentMode = SCServiceProcessSettings__c.getInstance().PAYMENT_MODE__c;
        Boolean isPrePayment = ((null != paymentMode) ? paymentMode.equalsIgnoreCase('PREPAYMENT') : false);
        
        for (SCboOrderLine line :allOrderLines)
        {
            // check the invoicing type of the order lines to 'contract' only if
            // no prepayment is active
            if ((line.orderline.InvoicingType__c == SCfwConstants.DOMVAL_INVOICINGTYPE_CONTRACT) && 
                 ((!SCBase.isSet(boOrder.order.Contract__c) && SCBase.isSet(boOrder.order.Id)) || 
                  ((boOrder.order.PaymentType__c != SCfwConstants.DOMVAL_PAYMENTTYPE_CONTRACT) && 
                   !SCBase.isSet(boOrder.order.Contract__c) && 
                   !SCBase.isSet(boOrder.order.Id))))
//            if ((line.orderline.InvoicingType__c == SCfwConstants.DOMVAL_INVOICINGTYPE_CONTRACT) && 
//                !SCBase.isSet(boOrder.order.Contract__c) && !isPrePayment)
            {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
                                                              System.Label.SC_msg_WrongOrdLineInvTypeContract);
                ApexPages.addMessage(msg);
                break;
            } // if ((line.orderline.InvoicingType__c == SCfwConstants.DOMVAL_INVOICINGTYPE_CONTRACT) && 
        } // for (SCboOrderLine line :allOrderLines)
        
        if (ApexPages.hasMessages())
        {
            throw new SCutilInterventionValidationException(System.Label.SC_msg_WrongOrdLineInvTypeContract);
        } // if (ApexPages.hasMessages())
    } // validateOrderLines

    /**
     * Validate if at least on repair code is set.
     *
     * @param installedBase Installed Base to validate
     * @throws SCutilInterventionValidationException
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public void validateRepairCodes(SCboOrder boOrder)
    {
        Boolean hasRepairCode = false;
        for (SCboOrderItem item : boOrder.boOrderItems)
        {
            if (item.boRepairCodes.size() > 0)
            {
                hasRepairCode = true;
                break;
            }
        }
        
        if (!hasRepairCode)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
                                                          System.Label.SC_msg_MissingRepairCode);
            ApexPages.addMessage(msg);
            throw new SCutilInterventionValidationException(System.Label.SC_msg_MissingRepairCode);
        }
    }

    /**
     * Validate if material devilery is pending.
     * REMARK: For now this is a stub.
     *
     * @param installedBase Installed Base to validate
     * @throws SCutilInterventionValidationException
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public void validateMaterialManagement(SCboOrder boOrder, SCStock__c stock)
    {
   
    }
}
