/*
 * @(#)SCOrderPageControllerTest.cls SCCloud
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Test the SCOrderPostProcessExtension. 
 *
 * @author Marcus Autenrieth <mautenrieth@gms-online.de>
 */
@isTest
private class SCOrderPostProcessExtensionTest 
{
    
    public static Integer randrange(Integer lower, Integer upper) 
    {
        Integer rand = Crypto.getRandomInteger();
        rand = rand < 0 ? rand * -1 : rand;
        Integer range = upper - lower;
        return lower + Math.mod(rand, range);
    }
    
    public static final Integer VALID_LABOUR_DURA = randrange(5, 599);
    public static final Integer VALID_TRAVEL_DURA = randrange(6, 599);
    public static final Integer VALID_TRAVEL_DISTANCE = randrange(1, 999);
    public static final DateTime VALID_END_TIME = DateTime.now() - randrange(0, 179);
    
    private static SCOrder__c ord = null;
    private static SCOrderItem__c orderItem = null;
    private static List<SCResource__c> resources = null;
    
    private static ApexPages.Standardcontroller stdController = null;
    private static SCOrderPostProcessExtension postprocController = null;
    
    private static SCResource__c CHOSEN_ENGINEER = null;
    
    private static Integer appointment_count_after_setup = -1;
    private static Integer assignment_count_after_setup = -1;

    /**
     * Convenience Function to set up everything for testing
     */
    static void setup() 
    {
        
        // Create data for testing
        SCHelperTestClass.createOrder(true);
        SCHelperTestClass.createTestUsers(true);
        SCHelperTestClass.createTestResources(SCHelperTestClass.users, true);
        SCHelperTestClass.createOrderItem(true);
                        
        ord = SCHelperTestClass.order;
        
        // SCHelperTestClass might have generated some appointments and assignments,
        // memorize their number for cross-checking upon validation-failure or cancel
        List<SCAppointment__c> appointments = [SELECT Order__c
                                               FROM SCAppointment__c
                                               WHERE Order__c = :ord.id];
        appointment_count_after_setup = appointments.size();
        // SCHelperTestClass should not create Appointments for us!
        System.assertEquals(0, appointment_count_after_setup);  
        
        List<SCAssignment__c> assignments = [SELECT Order__c
                                             FROM SCAssignment__c
                                             WHERE Order__c = :ord.id];
        assignment_count_after_setup = assignments.size();
        // SCHelperTestClass should not create Assignments for us!
        System.assertEquals(0, assignment_count_after_setup);
        
        // Create the Controller to be tested
        stdController = new ApexPages.Standardcontroller(ord);
        postprocController = new SCOrderPostProcessExtension(stdController);
 
        // Input correct values into the controller
        ord.Status__c = SCfwConstants.DOMVAL_ORDERSTATUS_OPEN; 
              
        postprocController.getEngineers(); // Lookup engineers
        System.assert(!postprocController.engineersList.isEmpty());
        
        // Randomly choose an Engineer from the generated List
        Integer rand_idx = Math.mod(Crypto.getRandomInteger(), postprocController.engineersList.size());
        CHOSEN_ENGINEER = postprocController.engineersList.get(rand_idx > 0 ? rand_idx : rand_idx * -1);
        
        // Virtually fill out the form.
        postprocController.selectedEmployee = CHOSEN_ENGINEER.id;
        
        // Last Cross-Check regarding the Engineer.
        System.assertNotEquals(null, postprocController.selectedEmployee);
        
        postprocController.assignment.TimereportEnd__c = VALID_END_TIME;
        postprocController.assignment.TimereportLabourDuration__c = VALID_LABOUR_DURA;
        postprocController.assignment.TimereportTravelDuration__c = VALID_TRAVEL_DURA;
        postprocController.assignment.TimereportTravelDistance__c = VALID_TRAVEL_DISTANCE;
        
    }
    
    /**
     * Checks whether no data is written to the DB when the input-validation fails and
     * when the user cancels.
     */
    static void checkForDataNotSaved() 
    {
        List<SCAppointment__c> appointments = [SELECT Id, Order__c, OrderItem__c, Resource__c, Assignment__c,
                                                      AssignmentStatus__c, End__c, Start__c, PlanningType__c,
                                                      Class__c, Type__c, FixedResource__c, Fixed__c, Completed__c 
                                               FROM SCAppointment__c
                                               WHERE Order__c = :ord.id 
                                                 //AND PlanningType__c = :SCfwConstants.DOMVAL_APPOINTMENTPLANNINGTYPE_GENERATED
                                                 //AND End__c = :VALID_END_TIME 
                                                 //AND FixedResource__c = :true 
                                                 //AND Fixed__c =:true 
                                                 //AND Completed__c = :true
                                                 //AND Type__c = :SCfwConstants.DOMVAL_APPOINTMENTTYP_JOB
                                                 //AND Class__c = :SCfwConstants.DOMVAL_APPOINTMENTCLASS_ORDER
                                                 //AND AssignmentStatus__c = :SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED
                                                 //AND Resource__c = :CHOSEN_ENGINEER.Id
                                                 ];    
        // No additional Appointments should have been created!          
        System.assertEquals(appointment_count_after_setup, appointments.size());
        
        List<SCAssignment__c> assignments = [SELECT Id, Order__c, Resource__c, Status__c, Followup1__c, Followup2__c,
                                                    TimereportLabourDuration__c, TimereportTravelDuration__c, 
                                                    TimereportTravelDistance__c 
                                             FROM SCAssignment__c
                                             WHERE Order__c = :ord.id 
                                               AND Status__c = :SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED
                                               AND TimereportLabourDuration__c = :VALID_LABOUR_DURA
                                               AND TimereportTravelDuration__c = :VALID_TRAVEL_DURA
                                               AND TimereportTravelDistance__c = :VALID_TRAVEL_DISTANCE
                                               AND Resource__c = :CHOSEN_ENGINEER.Id];
              
        // No additional Assignments should have been created!
        System.assertEquals(assignment_count_after_setup, assignments.size());
        
        List<SCTimeReport__c> workReport = [SELECT Order__c, Assignment__c, Type__c, End__c, Start__c, Resource__c 
                                            FROM SCTimeReport__c 
                                            WHERE Order__c = :ord.id 
                                              AND Type__c = :SCfwConstants.DOMVAL_TIMEREPORTTYPE_WORKTIME
                                              AND End__c = :VALID_END_TIME
                                              AND Start__c = :VALID_END_TIME.addMinutes(-VALID_LABOUR_DURA)
                                              AND Status__c = :SCfwConstants.DOMVAL_TIMEREPORTSTATUS_CLOSED 
                                              AND Resource__c = :CHOSEN_ENGINEER.id];                                
        System.assertEquals(0, workReport.size());
        
        List<SCTimeReport__c> timeReports = [SELECT Order__c, Assignment__c, Type__c, End__c, Start__c, Resource__c 
                                               FROM SCTimeReport__c 
                                               WHERE Order__c = :ord.id 
                                                 AND Type__c = :SCfwConstants.DOMVAL_TIMEREPORTTYPE_DRIVETIME
                                                 AND End__c = :VALID_END_TIME.addMinutes(-VALID_LABOUR_DURA)
                                                 AND Start__c = :VALID_END_TIME.addMinutes(-VALID_LABOUR_DURA).addMinutes(-VALID_TRAVEL_DURA)
                                                 AND Distance__c = :VALID_TRAVEL_DISTANCE
                                                 AND Status__c = :SCfwConstants.DOMVAL_TIMEREPORTSTATUS_CLOSED
                                                 AND Resource__c = :CHOSEN_ENGINEER.id];
       // No TimeReports should have been created.                                    
       System.assertEquals(0, timeReports.size());                               
       
       List<SCOrder__c> orders = [SELECT Id, channel__c, Closed__c, ClosedByInfo__c
                                  FROM SCOrder__c 
                                  WHERE Id = :ord.Id
                                    // These values should not have been commited to the DB 
                                    AND channel__c = :SCfwConstants.DOMVAL_CHANNEL_POSTPROCESSED
                                    AND Closed__c = :VALID_END_TIME];
       // The modifications of the orders did not make it into the DB.                         
       System.assertEquals(0, orders.size());
       
       orders = [SELECT Id, channel__c, Closed__c, ClosedByInfo__c
                 FROM SCOrder__c 
                 WHERE Id = :ord.Id];        
       System.assertEquals(1, orders.size()); // We should still be able to find our test-Order                 
    }
    
    /**
     * Convenience Function to check if saving failed as it should
     */
    static void saveAndCheckForFailure(SCOrderPostProcessExtension.ErrorCode expected_errno) 
    {
        test.startTest();
           PageReference pageref = postprocController.onSave();     
        test.stopTest();
        
        // We are returned to the view we tried to save
        System.assertEquals(null, pageref);
        // A message is displayed
        System.assert(ApexPages.hasMessages());
       
        // Check if the system reacts correcly to the error
        System.assertEquals(expected_errno, postprocController.errno);
        
        checkForDataNotSaved();
    }   
    
    /**
     * Convenience Function to check if saving worked like it should
     */
     static void saveAndCheckForSuccess() 
     {
        test.startTest();
          PageReference pageref = postprocController.onSave();
        test.stopTest();
        
        System.assertEquals(SCOrderPostProcessExtension.ErrorCode.SUCCESS, postprocController.errno);
        
        // Are we returned to the OrderOverview?
        System.assertEquals(pageref.getUrl(), stdController.view().getUrl());
        
        // Now test whether the data has been corretly written to the db         
        //---<1. assignment>--------------------------------------------------- 
        // Fetch assignment from db
        List<SCAssignment__c> assignments = [SELECT Id, Order__c, Resource__c, Status__c, Followup1__c, Followup2__c,
                                                    TimereportLabourDuration__c, TimereportTravelDuration__c, 
                                                    TimereportTravelDistance__c 
                                             FROM SCAssignment__c
                                             WHERE Order__c = :ord.id 
                                               // Find the assignment we saved
                                               AND Id = :postprocController.assignment.Id];
                                               
        System.assertEquals(1, assignments.size());
        
        SCAssignment__c assignment = assignments.get(0);                   
        System.assert(assignment != null);
        System.assertEquals(CHOSEN_ENGINEER.id, assignment.Resource__c);
        
        // These might only be necessary for later regression tests.    
        System.assertEquals(SCfwConstants.DOMVAL_FOLLOWUP1_NONE, assignment.Followup1__c);
        System.assertEquals(SCfwConstants.DOMVAL_FOLLOWUP2_NONE, assignment.Followup2__c);
        
        System.assertEquals(VALID_LABOUR_DURA, assignment.TimereportLabourDuration__c);
        System.assertEquals(VALID_TRAVEL_DURA, assignment.TimereportTravelDuration__c);
        System.assertEquals(VALID_TRAVEL_DISTANCE, assignment.TimereportTravelDistance__c);
        
        System.assertEquals(CHOSEN_ENGINEER.id, assignment.Resource__c);
        
        System.assertEquals(SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED, assignment.Status__c);
               
        //---<2. appointment>--------------------------------------------------
        // There should be only one appointment in the db. 
        List<SCAppointment__c> appointments = [SELECT Id, Order__c, OrderItem__c, Resource__c, Assignment__c,
                                                      AssignmentStatus__c, End__c, Start__c, PlanningType__c,
                                                      Class__c, Type__c, FixedResource__c, Fixed__c, Completed__c
                                               FROM SCAppointment__c
                                               WHERE Order__c = :ord.id 
                                                 AND PlanningType__c = :SCfwConstants.DOMVAL_APPOINTMENTPLANNINGTYPE_GENERATED
                                                 AND End__c = :VALID_END_TIME 
                                                 AND FixedResource__c = :true 
                                                 AND Fixed__c =:true 
                                                 AND Completed__c = :true
                                                 AND Type__c = :SCfwConstants.DOMVAL_APPOINTMENTTYP_JOB
                                                 AND Class__c = :SCfwConstants.DOMVAL_APPOINTMENTCLASS_ORDER
                                                 AND AssignmentStatus__c = :SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED];
        
        System.assertEquals(1, appointments.size());
        
        SCAppointment__c appointment = appointments.get(0);
        System.assert(appointment != null);
        System.assertEquals(assignment.id, appointment.Assignment__c); // Is this the assignment we saved earlier?   
        System.assertEquals(CHOSEN_ENGINEER.id, appointment.Resource__c);
        System.assertEquals(VALID_END_TIME.addMinutes(-VALID_LABOUR_DURA), appointment.Start__c);
                          

        //---<3. timereports>--------------------------------------------------  
        List<SCTimeReport__c> workReports = [SELECT Order__c, Assignment__c, Type__c, End__c, Start__c, Resource__c 
                                            FROM SCTimeReport__c 
                                            WHERE Order__c = :ord.id 
                                              AND Assignment__c = :assignment.id
                                              AND Type__c = :SCfwConstants.DOMVAL_TIMEREPORTTYPE_WORKTIME
                                              AND End__c = :VALID_END_TIME
                                              AND Start__c = :VALID_END_TIME.addMinutes(-VALID_LABOUR_DURA)
                                              AND Status__c = :SCfwConstants.DOMVAL_TIMEREPORTSTATUS_CLOSED 
                                              AND Resource__c = :CHOSEN_ENGINEER.id];
        System.assertEquals(1, workReports.size());
        
        List<SCTimeReport__c> travelReports = [SELECT Order__c, Assignment__c, Type__c, End__c, Start__c, Resource__c 
                                               FROM SCTimeReport__c 
                                               WHERE Order__c = :ord.id 
                                                 AND Assignment__c = :assignment.id
                                                 AND Type__c = :SCfwConstants.DOMVAL_TIMEREPORTTYPE_DRIVETIME
                                                 AND End__c = :VALID_END_TIME.addMinutes(-VALID_LABOUR_DURA)
                                                 AND Start__c = :VALID_END_TIME.addMinutes(-VALID_LABOUR_DURA).addMinutes(-VALID_TRAVEL_DURA)
                                                 AND Distance__c = :VALID_TRAVEL_DISTANCE
                                                 AND Status__c = :SCfwConstants.DOMVAL_TIMEREPORTSTATUS_CLOSED
                                                 AND Resource__c = :CHOSEN_ENGINEER.id];
        System.assertEquals(1, travelReports.size());                            
                
        //---<5. update order>--------------------------------------------------
        // Re-fetch order form DB to see whether it has been updated correctly
        List<SCOrder__c> orders = [SELECT Id, channel__c, Closed__c, ClosedByInfo__c
                                      FROM SCOrder__c 
                                      WHERE Id = :ord.Id 
                                        AND channel__c = :SCfwConstants.DOMVAL_CHANNEL_POSTPROCESSED];
        System.assertEquals(1, orders.size());
        
        SCOrder__c new_order = orders.get(0);                        
        System.assert(new_order != null);                         
        System.assertEquals(VALID_END_TIME, new_order.Closed__c);
     }
     
     /**
      * Test whether the getEngineers() method correctly generates a list of 
      * available engineers.
      */
     static testMethod void testGetEngineers_notEmpty() 
     {
        setup();
        test.startTest();
          postprocController.getEngineers();
        test.stopTest();
        System.assert(!postprocController.engineersList.isEmpty());
     }
     
     /**
      * Test whether we can successfully search for a specific engineer
      */
      static testMethod void testGetSpecificEngineer_notEmpty() 
      {
        setup();
        postprocController.engineerSearchString = SCHelperTestClass.users.get(0).firstName;
        test.startTest();
          postprocController.getEngineers();
        test.stopTest();
        System.assert(!postprocController.engineersList.isEmpty());
      }
     
     /**
      * Test whether cancel brings us back to the OderOverview
      */
      static testMethod void testOnCancel() 
      {
        setup();
        test.startTest();
           PageReference pageref = postprocController.onCancel();
        test.stopTest();
        
        // Are we returned to the OrderOverview?
        System.assertEquals(pageref.getUrl(), stdController.view().getUrl());
           
        checkForDataNotSaved();
      }

    /*
     if(ord == null)
     {
        msg = 'Der Auftrag kann nicht nacherfasst werden.';
     }
    */
    static testMethod void testOnSave_OrderIsNull() 
    {
        // Create a Blank Setup.
        setup();
        postprocController.ord = null;
        
        saveAndCheckForFailure(SCOrderPostProcessExtension.ErrorCode.ORDER_NULL);       
    } 
    
    /*
     if(ord.Type__c == SCfwConstants.DOMVAL_ORDERTYPE_INHOUSE)
     {
         msg = 'Diese Auftragsart kann nicht nacherfasst werden.';
     }
     */
    static testMethod void testOnSave_OrderTypeInhouse() 
    {
        setup();
        postprocController.ord.Type__c = SCfwConstants.DOMVAL_ORDERTYPE_INHOUSE;
        upsert(postprocController.ord);
        
        saveAndCheckForFailure(SCOrderPostProcessExtension.ErrorCode.ORDER_TYPE_WRONG); 
    }
     
    /*
     if(ord.Status__c == SCfwConstants.DOMVAL_ORDERSTATUS_CANCELLED ||
        ord.Status__c == SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL)
     {
           msg = 'Der Auftrag kann in diesem Status nicht mehr nacherfasst werden.';
     }
     */
    static testMethod void testOnSave_OrderstatusCancelled() 
    {
        setup();
        postprocController.ord.Status__c = SCFwConstants.DOMVAL_ORDERSTATUS_CANCELLED;
        upsert(postprocController.ord);
        
        saveAndCheckForFailure(SCOrderPostProcessExtension.ErrorCode.ORDER_STATUS_WRONG);
    }
     
    static testMethod void testOnSave_OrderstatusClosedfinal() 
    {
        setup();
        postprocController.ord.Status__c = SCFwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL;
        upsert(postprocController.ord);
        
        saveAndCheckForFailure(SCOrderPostProcessExtension.ErrorCode.ORDER_STATUS_WRONG);
    }
     
    /*
     if(ord.Status__c != SCfwConstants.DOMVAL_ORDERSTATUS_OPEN 
        && ord.Status__c != SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED)
     {
           msg = 'Nur neu angelegte Aufträge können zur Nacherfassung genutzt werden.';
     }
     */
     static testMethod void testOnSave_OrderStatusNotOpenOrCompleted() 
     {
        setup();
        postprocController.ord.Status__c = SCfwConstants.DOMVAL_ORDERSTATUS_PROCESSING_MOBILE;
        upsert(postprocController.ord);
        
        saveAndCheckForFailure(SCOrderPostProcessExtension.ErrorCode.ORDER_STATUS_WRONG);
     }
       
    /*
     else if(selectedEmployee == null)
     {
           msg = 'Bitte wählen Sie den Techniker, für den nacherfasst werden soll.';
           canEnterData = true;
     }
     */
    static testMethod void testOnSave_EmployeeIsNull() 
    {
        setup();
        postprocController.selectedEmployee = null;
        
        saveAndCheckForFailure(SCOrderPostProcessExtension.ErrorCode.ENGINEER_NULL);
    }
     
    /*
     else if(assignment.TimereportEnd__c == null)
     {
          msg = 'Bitte geben Sie den Auftragsabschlusszeitpunkt an.';
          canEnterData = true;
     }
     */
    static testMethod void testOnSave_TimereportEndIsNull() 
    {
        setup();
        postprocController.assignment.TimereportEnd__c = null;
        
        saveAndCheckForFailure(SCOrderPostProcessExtension.ErrorCode.TIMEREPORT_END_NULL);
    }
     
    /*
     else if(assignment.TimereportEnd__c < DateTime.now().addDays(-180))
     {
         msg = 'Das eingegebene Datum liegt zu weit in der Vergangenzeit.';
        canEnterData = true;
     }
     */
    static testMethod void testOnSave_TimereportEndTooFarInPast() 
    {
        setup();
        postprocController.assignment.TimereportEnd__c = DateTime.now().addDays(-200);
        
        saveAndCheckForFailure(SCOrderPostProcessExtension.ErrorCode.TIMEREPORT_END_TOO_FAR_IN_PAST);
    }
      
    /*
     else if(assignment.TimereportEnd__c > DateTime.now())
     {
           msg = 'Das eingegebene Datum muss in der Vergangenheit liegen.';
           canEnterData = true;
     }
     */
    static testMethod void testOnSave_TimereportEndInFuture() 
    {
        setup();
        postprocController.assignment.TimereportEnd__c = DateTime.now().addDays(10);
        
        saveAndCheckForFailure(SCOrderPostProcessExtension.ErrorCode.TIMEREPORT_END_IN_FUTURE);
    }
    
    /*
     else if(assignment.TimereportLabourDuration__c == null)
     {
         msg = 'Bitte geben Sie die Arbeitszeit in Minuten an.';
         canEnterData = true;
     }
     */
    static testMethod void testOnSave_TimereportLabourDurationIsNull() 
    {
        setup();
        postprocController.assignment.TimereportLabourDuration__c = null;
        
        saveAndCheckForFailure(SCOrderPostProcessExtension.ErrorCode.TIMEREPORT_LABOUR_DURATION_NULL);
    }
    
    /* 
     else if(assignment.TimereportLabourDuration__c < 5 || assignment.TimereportLabourDuration__c > 60 * 10)
     {
         msg = 'Bitte geben Sie eine Arbeitszeit > 5 Minuten und < 600 Minuten (10 Stunden ) an.';
         canEnterData = true;
     }
     */
    static testMethod void testOnSave_TimereportLabourDurationTooSmall() 
    {
        setup();
        postprocController.assignment.TimereportLabourDuration__c = 2;

        saveAndCheckForFailure(SCOrderPostProcessExtension.ErrorCode.TIMEREPORT_LABOUR_DURATION_NOT_IN_RANGE);      
    }
    
    static testMethod void testOnSave_TimereportLabourDurationTooLarge() 
    {
        setup();
        postprocController.assignment.TimereportLabourDuration__c = 1000;
        
        saveAndCheckForFailure(SCOrderPostProcessExtension.ErrorCode.TIMEREPORT_LABOUR_DURATION_NOT_IN_RANGE);
    }
    
    /*
     else if(assignment.TimereportTravelDuration__c != null)
     {
         canEnterData = true;
         if(assignment.TimereportTravelDuration__c < 5 || assignment.TimereportTravelDuration__c > 60 * 10)
         {
             msg = 'Bitte geben Sie eine Fahrtzeit > 5 Minuten und < 600 Minuten (10 Stunden ) an.';
         }
         else if(assignment.TimereportTravelDistance__c == null)
         {
             msg = 'Bitte geben Sie eine Fahrtstrecke in km an.';
         }
         else if(assignment.TimereportTravelDistance__c < 1 || assignment.TimereportTravelDistance__c > 1000)
         {
             msg = 'Bitte geben Sie eine Fahrtstrecke > 1 km und < 1000 km an.';
         }
     }
     */
    static testMethod void testOnSave_TimereportTravelDurationIsNull() 
    {
        setup();
        postprocController.assignment.TimereportTravelDuration__c = null;
        
        saveAndCheckForFailure(SCOrderPostProcessExtension.ErrorCode.TIMEREPORT_TRAVEL_DURATION_NULL);  
    }
    
    static testMethod void testOnSave_TimereportTravelDurationTooLarge() 
    {
        setup();
        postprocController.assignment.TimereportTravelDuration__c = 1000;
        
        saveAndCheckForFailure(SCOrderPostProcessExtension.ErrorCode.TIMEREPORT_TRAVEL_DURATION_NOT_IN_RANGE);
    }
    
    static testMethod void testOnSave_TimereportTravelDurationTooSmall() 
    {
        setup();
        postprocController.assignment.TimereportTravelDuration__c = 1;
        
        saveAndCheckForFailure(SCOrderPostProcessExtension.ErrorCode.TIMEREPORT_TRAVEL_DURATION_NOT_IN_RANGE);
    }
    
    static testMethod void testOnSave_TimereportTravelDistanceisNull() 
    {
        setup();
        postprocController.assignment.TimereportTravelDistance__c = null;
        
        saveAndCheckForFailure(SCOrderPostProcessExtension.ErrorCode.TIMEREPROT_TRAVEL_DISTANCE_NULL);
    }
    
    static testMethod void testOnSave_TimereportTravelDistanceTooLarge() 
    {
        setup();
        postprocController.assignment.TimereportTravelDistance__c = 2000;
        
        saveAndCheckForFailure(SCOrderPostProcessExtension.ErrorCode.TIMEREPORT_TRAVEL_DISTANCE_NOT_IN_RANGE);
    }

    static testMethod void testOnSave_TimereportTravelDistanceTooSmall() 
    {
        setup();
        postprocController.assignment.TimereportTravelDistance__c = 0;
        
        saveAndCheckForFailure(SCOrderPostProcessExtension.ErrorCode.TIMEREPORT_TRAVEL_DISTANCE_NOT_IN_RANGE);
    }

    static testMethod void testOnSave_ValidData() 
    {
        setup();
        postprocController.selectedEmployee = CHOSEN_ENGINEER.id;
        System.assertEquals(CHOSEN_ENGINEER.id, postprocController.selectedEmployee);
        
        saveAndCheckForSuccess();
    }
    
    /* two positive tests for this:
     if(ord.Status__c != SCfwConstants.DOMVAL_ORDERSTATUS_OPEN 
        && ord.Status__c != SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED)
     {
    */
    static testMethod void testOnSave_OrderStatusOpen() 
    {
        setup();
        postprocController.ord.Status__c = SCfwConstants.DOMVAL_ORDERSTATUS_OPEN;
        postprocController.selectedEmployee = CHOSEN_ENGINEER.id;
        
        saveAndCheckForSuccess();   
    }
    
    static testMethod void testOnSave_OrderStatusCompleted() 
    {
        setup();
        postprocController.ord.Status__c = SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED;
        postprocController.selectedEmployee = CHOSEN_ENGINEER.id;
        
        saveAndCheckForSuccess();  
    }
    
    
    
}
