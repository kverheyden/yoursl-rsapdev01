/*
* @(#)SCCokeIDHandlerController.cls
* 
* Copyright 2014 by GMS Development GmbH, Karl-Schurz-Strasse 29, 
* DE-33100 Paderborn. All rights reserved.
* 
* Implements the possiblility to use CokeID OAuth with salesforce
*
* @history	
* 2014-10-13  GMSMT created
* 
* @review 
* 
*/

public with sharing class SCCokeIDHandlerController 
{

	public SCCokeIDHandlerController()
	{
		Returning = false;	
	}
	
	private String username		= '2786ad250e8436db6f21d8d905608b15';
    private String password		= '1412777493543546156f0083.45290579';
	private String redirectUri	= 'https://cceag--CCEAGQA.cs7.my.salesforce.com/apex/SCCokeIDHandler';
	private String grantType	= 'authorization_code';
	private String Endpoint		= 'https://dev-id.cceag.de/oauth/token';
	//private String Endpoint		= 'https://id.cceag.de/oauth/token';
	private String Method		= 'Post';
	private String OpenIDRedirect = 'https://test.salesforce.com/services/authcallback/00DM0000001XgVQMA0/CokeID';
	
	public  Boolean Returning {set;get;}
	public  String Response {set;get;}
	
	private Map<String, String> g_inputParameterMap;

	
	public Pagereference CokeLogin()
	{
		//String url = 'https://id.cceag.de/oauth/authorize?response_type=code&client_id=' + username + '&redirect_uri=' + redirectUri;
		String url = 'https://dev-id.cceag.de/oauth/authorize?response_type=code&client_id=' + username + '&redirect_uri=' + redirectUri; 
		return new Pagereference(url);
		//return new Pagereference('https://id.cceag.de/oauth/authorize?response_type=code&client_id' + username + '&redirect_uri=' + redirectUri);	
	}
	
	/**
	* Adds all of the elements in the specified set to this set if they're 
	* not already present. The addAll operation effectively modifies this	 * set so that its value is the union of the two sets.
	* 
	* @param value   set the set whose elements are to be added to this set
	* @return true   if this set changed as a result of the call
	*/
	public Pagereference CokeIdOAuth()
	{
		//curl -X POST https://clienId:clientSecret@id.cceag.de/oauth/token -d 'grant_type=authorization_code&code=<code>'
	
		g_inputParameterMap = Apexpages.currentPage().getParameters();
		
		System.debug('###HTTP inputparams: ' + g_inputParameterMap);
		
		if(g_inputParameterMap.containskey('code'))
		{
			Returning = true;
			HttpRequest req = new HttpRequest();
			
			
			Blob headerValue = Blob.valueOf(username + ':' + password);
	     	String authorizationHeader = 'BASIC ' +
	     	EncodingUtil.base64Encode(headerValue);
	     	req.setHeader('Authorization', authorizationHeader);
	
	     	String code = g_inputParameterMap.get('code');
	     	System.debug('###HTTP inputparams: ' + g_inputParameterMap);
	     	String params = 'grant_type=' + grantType + '&code=' + code + '&redirect_uri=' + redirectUri;
	     	//String params = '&code=b595322888951a63112cbbaf88600d21a10c443e&redirect_uri=' + redirectUri;
	     	
	
	     	req.setBody(params);
			req.setEndpoint(Endpoint);
	     	req.setMethod('POST');
	
		    Http http = new Http();
		    HTTPResponse res = http.send(req); 
		   
		   	//Response = String.valueof(res.getbody());
		   	
		   
		    System.debug('###HTTP req: ' + req);
		    System.debug('###HTTP req: ' + req.getbody());
		  
		    System.debug('###HTTP res: ' + res.getBody());	
		    
	
		    System.debug('###ResponseMap: ' + SplitResponseBody(String.valueof(res.getBody())));	
			
			 Map<String,String> paramsMap = SplitResponseBody(String.valueof(res.getBody()));

			System.debug('###ParaValue: ' + paramsMap.get('access_token'));

			String token = paramsMap.get('access_token');
			System.debug('###token: ' + token);

		   return new Pagereference('https://test.salesforce.com/services/authcallback/00DM0000001XgVQMA0/CokeID?&state=' + token); 		
		}		
		
		return null;
		//new Pagereference('https://test.salesforce.com/services/authcallback/00DM0000001XgVQMA0/CokeID?&state=');
		


	}
	
	private Map<String, String> SplitResponseBody(String response)
	{
			myDebug('response1', response);
			response = response.substring(1, response.length()-1);
			myDebug('response2', response);

		    
		    List<String> ResParams = response.split(',');
		    Map<String,String> paramsMap = new Map<String, String>();
		    List<String> onePair;
		    
		    for(String paramPair : ResParams)
		    {
		    	onePair = paramPair.split(':'); 
		    	paramsMap.put(onePair[0].substring(1, onePair[0].length()-1),onePair[1].substring(1, onePair[1].length()-1));			
		    }	
		    
		    return paramsMap; 	
	}
	
	private void myDebug(String myText, String myDebug)
	{
		System.debug('###' + myText + ': ' + myDebug);	
	}
	
	private void ForwardCode()
	{
		
	}
	
	private void ForwardToken()
	{
		
	}
}
