public with sharing class SCMapObjectViewController {
	
	private String id {get; set;}
	public String mode {get; private set;}
	
	public SCMapViewTourItem item {get; private set;}
	//public SCMapTourItem2 item {get; private set;}
	
	public SCMapObjectViewController(){
		
		//ID validation
		id=Apexpages.currentPage().getParameters().get('id');
		if(id.length()<15){
			throw new IdNotFoundException('id('+id+') not valid');			
		}
		this.initController();
		
	}
	
	public SCMapObjectViewController(String id){
		
		//ID validation 
		this.id = id;
		this.initController();
	}
	
	private void initController()
	{
		//get Object Type
		
		//SCResourceAssignment__c
		String objPrefix = this.id.subString(0,3);
		
		//Object Prefixes
		String prefixRA = SCResourceAssignment__c.SObjectType.getDescribe().getKeyPrefix();
		String prefixA = SCAppointment__c.SObjectType.getDescribe().getKeyPrefix();
		String prefixOI = SCOrderItem__c.SObjectType.getDescribe().getKeyPrefix();
		
		if(objPrefix.equals(prefixRA)){
			mode='Engineer';
			
			this.initEngineer();
            
		}
		else if(objPrefix.equals(prefixA)){
			mode='Order';
			
			this.initAppointment();
            
            //item.resourcePhone=item.resourcePhone.replace('#', '').replace('(','').replace(')','').replace(' ','');
		}
		else if(objPrefix.equals(prefixOI)){
			mode='OItem';
			
			this.initOrderItem();
			
		}
		
		else{
			throw new IdNotFoundException('id('+id+') not found');
			
		}
		
		
		
		//system.debug('Object='+r);
	}
	
	private void initEngineer()
	{
		item=new SCMapViewTourItem();
		
		try
		{
			SCResourceAssignment__c r=[
										SELECT 
											id, Resource__r.ID, 
											Resource__r.name, Resource__r.FirstName_txt__c, Resource__r.LastName_txt__c, 
											Resource__r.Mobile_txt__c, Resource__r.Phone_txt__c, Resource__r.EMail_txt__c,  
						                   	Country__c, District__c, PostalCode__c,  City__c, Street__c, HouseNo__c, 
						                   	GeoX__c, GeoY__c, Extension__c, Floor__c, FlatNo__c
					                    FROM 
					                    	SCResourceAssignment__c
					                    WHERE 
					                    	id=:this.id
					                    ];
	            
	        
	        //item=new SCMapTourItem2();
	                
	        //item.resourceUrl = r.Resource__r.util_url__c;
	        item.name = r.Resource__r.name;
	        item.resourceAddress = SCutilFormatAddress.formatAddress(r);
	        item.resourceMobile = r.Resource__r.Mobile_txt__c;
	        item.resourcePhone = r.Resource__r.Phone_txt__c;
	        item.resourceEmail = r.Resource__r.EMail_txt__c;
	        item.objectId = r.id;
	        item.resourceFullName = r.Resource__r.Firstname_txt__c + ' ' + r.Resource__r.Lastname_txt__c;
	        item.GeoX = r.GeoX__c;
			item.GeoY = r.GeoY__c;
		}
		catch(Exception e)
		{
			
			this.mode 		= 'Error';
			item.itemtype	= 'ERR';
			
			//item.name		= 'No Data found for ResourceAssignment';
			item.name		= Label.SC_msg_NoDataFound;
			
			item.description= e.getMessage();
        	
		}
	}
	
	private void initAppointment()
	{
		item=new SCMapViewTourItem();
		
		try
		{
			SCAppointment__c r=[SELECT 
		        					Description__c, Start__c, End__c, 
									Order__c, OrderItem__c, Assignment__c, AssignmentStatus__c,
				                    Order__r.name, TOLABEL(Order__r.type__c), TOLABEL(Order__r.status__c), 
				                    toLabel(Order__r.CustomerPriority__c),  
				                    OrderItem__r.InstalledBase__r.ProductNameCalc__c,
				                    Resource__c, Resource__r.LastName_txt__c, Resource__r.FirstName_txt__c, Resource__r.Name,
				                    Resource__r.Mobile_txt__c, Resource__r.Phone_txt__c
				                    
			                    FROM 
			                    	SCAppointment__c 
			                    WHERE 
			                    	id=:this.id
		                    
		                   ];
			//item=new SCMapTourItem2();
        
	        item.itemtype 		= 'ORD';
	        item.orderid 		= r.Order__c;
	        item.orderno 		= r.Order__r.name;
	        item.apptype 		= r.Order__r.type__c;
	        item.status 		= r.Order__r.status__c;
	        item.description 	= r.Description__c;
	        item.productName	= r.OrderItem__r.InstalledBase__r.ProductNameCalc__c;
	        
	        //item.dtStart     	= r.Start__c;            // planned start and end (internal appointment)
	        item.orderDate     	= formatInterval(r.Start__c,r.End__c);  
	                  
	        item.orderRoles		= getOrderRoles(r.Order__c);     
	        
	        item.dtEnd       	= r.End__c;
	        item.priority		= r.Order__r.CustomerPriority__c;
	        
	        item.resourceFullName = r.Resource__r.Firstname_txt__c + ' ' + r.Resource__r.Lastname_txt__c + ' (' + r.Resource__r.Name + ')';
	        item.resourceId 	= r.Resource__c;
	        item.resourceMobile = r.Resource__r.Mobile_txt__c;
	    	item.resourcePhone = r.Resource__r.Phone_txt__c;
	    	
	        
			this.setCustomerDetails(r.OrderItem__c);		 
		}
		catch(Exception e)
		{
			
			this.mode 		= 'Error';
			item.itemtype	= 'ERR';
			
			//item.name		= 'No Data found for Appointment';
			item.name		= Label.SC_msg_NoDataFound;
			item.description= e.getMessage();
        	
		}
		
	}
	
	private void initOrderItem()
	{
		item=new SCMapViewTourItem();
		try
		{
			SCOrderItem__c oI=[	SELECT 
									Order__c, Order__r.name, toLabel(Order__r.type__c), toLabel(Order__r.status__c),
									toLabel(Order__r.CustomerPriority__c), InstalledBase__r.ProductNameCalc__c,
									Order__r.CustomerPrefStart__c, Order__r.CustomerPrefEnd__c 
									
									
								FROM 
									SCOrderItem__c 
								WHERE id=:this.id
		                        Limit 1
		                       ];
			
					
			
			item.orderId = oI.Order__r.id;
	        item.orderno = oI.Order__r.name;
	        
	        
	        item.itemtype 		= 'ORD';
	        item.orderid 		= oI.Order__c;
	        item.orderno 		= oI.Order__r.name;
	        item.apptype 		= oI.Order__r.type__c;
	        item.status 		= oI.Order__r.status__c;
	        item.priority		= oI.Order__r.CustomerPriority__c;
	        item.productName	= oI.InstalledBase__r.ProductNameCalc__c;
	        item.prefStart		= (oI.Order__r.CustomerPrefStart__c).format();
	        item.prefEnd		= (oI.Order__r.CustomerPrefEnd__c).format();
	        
	        item.orderRoles		= getOrderRoles(oI.Order__c);          
	        this.setCustomerDetails(this.id);
		}
		catch(Exception e)
		{
			
			this.mode 			= 'Error';
			item.itemtype 		= 'ERR';
			
        	//item.name			= 'No Data found for OrderItem';
        	item.name		= Label.SC_msg_NoDataFound;
        	item.description	= e.getMessage();
        	return;
		}
      
	}
	
	private void initOrder()
	{
		
	}
	
	private List<SCOrderRole__c> getOrderRoles(String orderId)
	{
		//Order Roles
			List<SCOrderRole__c> orderRolesList = 
			[	
				SELECT 
					Account__c,toLabel(OrderRole__c),Name1__c,MobilePhone__c,Phone__c 
				FROM 
					SCOrderRole__c
				WHERE
					Order__c = :orderId
					AND
					OrderRole__c IN ('50301','50302')
				ORDER BY 
					OrderRole__c ASC
			];
			
			//create map with unique accounts in order role
			Map<Id,SCOrderRole__c> orderRolesMap = new Map<Id,SCOrderRole__c>();
			for(SCOrderRole__c role: orderRolesList)
			{
				orderRolesMap.put(role.Account__c,role);
			}
							
			system.debug('order roles: ' + orderRolesMap);
			return orderRolesMap.values();
	}
	
	/**
	 * Set the customer details to tour item
	 * 
	 * @param	orderItemId	the SCOrderItem id
	 */
	 
	private void setCustomerDetails(String orderItemId)
	{
		List<SCOrderItem__c> oIList=[	
							SELECT
								InstalledBase__r.InstalledBaseLocation__c, 
   								InstalledBase__r.InstalledBaseLocation__r.LocName__c, 
   								InstalledBase__r.InstalledBaseLocation__r.Street__c, 
							   	InstalledBase__r.InstalledBaseLocation__r.HouseNo__c, 
							   	InstalledBase__r.InstalledBaseLocation__r.Extension__c, 
							   	InstalledBase__r.InstalledBaseLocation__r.PostalCode__c,
							   	InstalledBase__r.InstalledBaseLocation__r.City__c, 
							   	InstalledBase__r.InstalledBaseLocation__r.District__c,   
							   	InstalledBase__r.InstalledBaseLocation__r.Country__c,
							   	InstalledBase__r.InstalledBaseLocation__r.GeoX__c, 
							   	InstalledBase__r.InstalledBaseLocation__r.GeoY__c  
							FROM 
								SCOrderItem__c 
							WHERE 
	                        	Id=:orderItemId                             
                            
	                      ];
		if(oIList.size() > 0)
		{
			SCOrderItem__c oItem = oIList.get(0);
			item.street	= oItem.InstalledBase__r.InstalledBaseLocation__r.Street__c;
			item.houseNo = oItem.InstalledBase__r.InstalledBaseLocation__r.HouseNo__c;
			item.city	= oItem.InstalledBase__r.InstalledBaseLocation__r.City__c;
			item.postalCode = oItem.InstalledBase__r.InstalledBaseLocation__r.PostalCode__c;
			item.GeoX = oItem.InstalledBase__r.InstalledBaseLocation__r.GeoX__c;
			item.GeoY = oItem.InstalledBase__r.InstalledBaseLocation__r.GeoY__c;
			SCInstalledBaseLocation__c loc = new SCInstalledBaseLocation__c
			(
				Street__c = item.street,
				HouseNo__c = item.houseNo,
				City__c = item.city,
				PostalCode__c = item.postalCode
			);
			
			item.formattedAddress = SCutilFormatAddress.formatAddress(loc);
			system.debug('formatted address: ' + item.formattedAddress);
			//item.name = oItem.InstalledBase__r.InstalledBaseLocation__r.LocName__c;
			item.description = oItem.InstalledBase__r.InstalledBaseLocation__r.LocName__c;
		}
	}
	
	 
	
	private static String formatInterval(DateTime dtStart, DateTime dtEnd)
    {
        if(dtStart != null && dtEnd != null)
        {
            Date dStart = dtStart.date();
            Date dEnd   = dtEnd.date();
            if(dStart == dEnd)
            {
                // if both are on the same day - the date is only formatted once
                return dtStart.Format('\'CW\'ww EEE dd MMMMMMMMMMMMMM yyyy') + ' ' + dtStart.format('HH:mm a') +  ' - ' + dtEnd.format('HH:mm a'); 
            }
            else
            {    // on multiple days both date values are to be formatted
                return dStart.Format() + ' ' + dtStart.format('HH:mm') +  '-' + dEnd.Format() + ' ' + dtEnd.format('HH:mm'); 
            }
        }
        if(dtStart != null)
        {
            Date dStart = dtStart.date();
            return dStart.Format() + ' ' + dtStart.format('HH:mm');
        }
        if(dtEnd != null)
        {
            Date dEnd = dtEnd.date();
            return dEnd.Format() + ' ' + dtEnd.format('HH:mm'); 
        }
        return '';
    }
	
	public class IdNotFoundException extends Exception{
		
	}
	
	

}
