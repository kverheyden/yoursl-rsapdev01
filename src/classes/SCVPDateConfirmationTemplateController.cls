/*
 * @(#)SCVPDateConfirmationTemplateController.cls    
 * 
 * Copyright 2014 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * Controller for 'PlannedDate Confirmation Function Template' - Component
 *
 * @author Mirco Stein 
 */


public with sharing class SCVPDateConfirmationTemplateController 
{
	//Parameter IN



	public String 					 g_exAssigmentID 
	{
		get;
		set
		{
			g_exAssigmentID = value;
			init();
		}
	}

	public	SCOrderExternalAssignment__c 		g_exAssigment		{set;get;}		

	public	List<SCInstalledBase__c> 			g_equipmentList		{set;get;}
	public	String 								g_plannedDate		{set;get;}	
	public	Account 							g_orderRoleSR		{set;get;} 

	
	// Globals
	
	public SCOrder__c							g_ThisOrder;

	public List<SCInstalledBase__c> getEquips()
	{
		return g_equipmentList;	
	}

	public void init()
	{
		try
		{
			g_exAssigment = [SELECT id, order__c, SCPlannedDate__c, SCPlannedDateEnd__c FROM SCOrderExternalAssignment__c WHERE id = :g_exAssigmentID];	
		}
		catch(exception e){}

		DateTime tmp_plannedDate = g_exAssigment.SCPlannedDate__c;
		DateTime tmp_plannedDateEnd = g_exAssigment.SCPlannedDateEnd__c;
 
		g_plannedDate = tmp_plannedDate.dateGMT().format() + ' zwischen ' +  tmp_plannedDate.hourGmt() + ':' + tmp_plannedDate.minuteGmt() + ' und ' +  tmp_plannedDateEnd.hourGmt() + ':' + tmp_plannedDateEnd.minuteGmt() ; 

		if(g_exAssigment!= null && g_exAssigment.Order__c != null)
		{
			g_ThisOrder = [SELECT id, roleSR__c, roleSR__r.Account__c FROM SCOrder__c WHERE id = :g_exAssigment.Order__c];	
		}
		
		g_orderRoleSR = [SELECT id, email__c, BillingStreet, BillingPostalCode, BillingCity FROM Account WHERE id = :g_ThisOrder.roleSR__r.Account__c];

		List<ID> tmp_equipmentIdList = new List<ID>();
		
		if(g_ThisOrder!=null)
		{
			for(SCOrderItem__c oi : [SELECT id, InstalledBase__c FROM SCOrderItem__c WHERE Order__c = :g_ThisOrder.id])
			{
				tmp_equipmentIdList.add(oi.InstalledBase__c);
			}

		}

		String AccountShipTo = '';
		
		if(!tmp_equipmentIdList.isEmpty())
		{			
			g_equipmentList = new List<SCInstalledBase__c>();
			
			for(SCInstalledBase__c eq : [SELECT id, ShipTo__c, name, ProductNameCalc__c FROM SCInstalledBase__c WHERE id IN :tmp_equipmentIdList])
			{
				g_equipmentList.add(eq);
				AccountShipTo = eq.ShipTo__c; 	
			}
		}	
	}		
}
