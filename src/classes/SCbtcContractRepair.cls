/*
 * @(#)SCContractRepair.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
global class SCbtcContractRepair extends SCbtcBase
       implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts
{
    private ID batchprocessid = null;
    private String mode = 'productive';
    String country = 'DE';

    Integer max_cycles = 0;    
    List<String> visitIdList = null;    // visitIdList = null, normal batch call
                                        // visitIdList != null, orders are repaird only for the list
                                        // of visits    

    // Object for application settings
    private static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();

    private Id contractId = null;

    /**
     * Entry point for starting the apex batch repairing all contracts 
     * @param mode could be 'trace', 'test' or 'productive'
     */
    public static ID repairAll(String country, Integer max_cycles, String mode)
    {
        String department =  null;
        SCbtcContractRepair btc = new SCbtcContractRepair(country, max_cycles, mode);
        btc.batchprocessid = btc.executeBatch();
        return btc.batchprocessid;
    } // repairAll


    /**
     * Entry point for starting the apex batch dispatching an order having the 
     * SCContractVisit__c.Status__c = 'repair' and SCContractVisit__c.Order__c = :orderId
     * @param contractId
     * @param mode could be 'test' or 'productive'
     */
    public static ID repairContract(Id contractId, String country, Integer max_cycles, String mode)
    {
        SCbtcContractRepair btc = new SCbtcContractRepair(contractId, country, max_cycles, mode);
        btc.batchprocessid = btc.executeBatch();
        return btc.batchprocessid;
    } // repairContract

    /**
     * Entry point for starting the apex batch dispatching an order having the 
     * SCContractVisit__c.Status__c = 'repair' and SCContractVisit__c.Order__c.Name = :orderName
     * @param orderName
     * @param mode could be 'test' or 'productive'
     */
    public static ID repairContract(String contractName, String country, Integer max_cycles, String mode)
    {
        Id contractId = null;
        System.debug('###mode: ' + mode);
        System.debug('###contractName: ' + contractName);
        if(contractName != null)
        {
            List<SCContract__c> contractList = [Select Id from SCContract__c where Name = :contractName];
            if(contractList.size() > 0)
            {
                contractId = contractList[0].Id;
                System.debug('###contractId: ' + contractId);
                SCbtcContractRepair btc = new SCbtcContractRepair(contractId, country, max_cycles, mode);
                btc.batchprocessid = btc.executeBatch();
                return btc.batchprocessid;
            }
        }
        return null;
    } // repairContract


    /**
     * Constructor
     * @param mode could be 'test' or 'productive'
     */
    public SCbtcContractRepair(String country, Integer max_cycles, String mode)
    {
        this(null, country, max_cycles, mode);
    }


    /**
     * Constructor
     * @param contractID
     * @param mode could be 'test' or 'productive'
     */
    public SCbtcContractRepair(Id contractId, String country, Integer max_cycles, String mode)
    {
        this.contractId = contractId;
        this.country = country;
        this.max_cycles = max_cycles;
        this.mode = mode;
    }

   /*
    * Called by the framework when the batch job is started. We construct the soql statement
    * for reading the resource assignments.
    * @param BC the batch context
    * @return the query locator with the selected material movements (up to 50 mio records possible)
    */
    global override Database.QueryLocator start(Database.BatchableContext BC)
    {
        debug('start');
        // one order will be created from one SContractVisit__c record 
        /* To check the syntax of SOQL statement */
        List<SCContract__c> visitList = [select ID, Country__c, DepartmentCurrent__c, (Select InstalledBase__r.InstalledBaseLocation__r.PostalCode__c from Service_Contract_Items__r Limit 1)
                                            from SCContract__c 
                                            where  DepartmentCurrent__c = null
                                            limit 1];
        
        debug('start: after checking statement');
        /**/
        String query = null;
        if(country == null || country.trim() == '')
        {
            country = 'XX';
        }
        debug('country: ' + country);
        if(contractId == null)
        {
            query = ' select ID, Country__c, DepartmentCurrent__c,  (Select InstalledBase__r.InstalledBaseLocation__r.PostalCode__c from Service_Contract_Items__r Limit 1)'
                  + ' from SCContract__c where DepartmentCurrent__c = null and Country__c = \'' + country + '\'';

           query += ' and (status__c = \'Active\' or status__c = \'Created\')'
                    + ' and Template__r.Status__c = \'Active\' ';
            if(max_cycles > 0)
            {
                query += ' limit ' + max_cycles;
            }
        }
        else
        {
            query = ' select ID, Country__c, DepartmentCurrent__c, (Select InstalledBase__r.InstalledBaseLocation__r.PostalCode__c from Service_Contract_Items__r Limit 1 )'
                  + ' from SCContract__c where DepartmentCurrent__c = null'
                      + ' and ID = \'' + contractId + '\'';
           query += ' and (status__c = \'Active\' or status__c = \'Created\')'
                    + ' and Template__r.Status__c = \'Active\' ';
            if(max_cycles > 0)
            {
                query += ' limit ' + max_cycles;
            }
        }
        debug('query: ' + query);
        return Database.getQueryLocator(query);
    } // start
    
   /*
    * Called for each batch of records to process.
    * @param BC the batch context
    * @param scope the list records to be processed
    */
    global override void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        debug('execute');
        List<SCInterfaceLog__c> interfaceLogList = new List<SCInterfaceLog__c>();
        for(sObject res: scope)
        {
            debug('sObject: ' + res);
            SCContract__c c = (SCContract__c)res;
            repairContract(c, interfaceLogList);
        }
        upsert scope;
        insert interfaceLogList;
    } // execute

   /*
    * Called by the framework when the batch job has been completed. 
    * We send an e-mail notification about the status
    * @param BC the batch context
    */
    global override void finish(Database.BatchableContext BC)
    {
    } // finish

    /**
     * Dispatches an order SCContractVisit__c.Order__c
     */
    public void repairContract(SCContract__c c, List<SCInterfaceLog__c> interfaceLogList)
    {
        debug('repairContract');
        debug('contract: ' + c);
        debug('contractID: ' + c.Id);
        
        DateTime start = Datetime.now();
        Boolean thrownException = false;
        String resultInfo = '';
        Integer count = 0;
        String step = '';
        Boolean error = false;
        try
        {
            step = 'determine a postal code';
            debug(step);
            String department = c.DepartmentCurrent__c;
            String postalCode = '';
            if(c.Service_Contract_Items__r.size() > 0)
            {
                postalCode = c.Service_Contract_Items__r[0].InstalledBase__r.InstalledBaseLocation__r.PostalCode__c;
            }
            else
            {
                error = true;
                resultInfo = 'There is no installed base.';
            }
            String country = c.Country__c;
            debug('country: ' + country);
            debug('postalCode: ' + postalCode);
            debug('department: ' + department);
          
            String newDepartment = null;
            if(country == null ||  country.trim() == '')
            {
                if(error)
                {
                    resultInfo += ' ';
                }
                resultInfo = 'Country is empty.';
                error = true;
            }
            if(postalCode == null ||  postalCode.trim() == '')
            {
                if(error)
                {
                    resultInfo += ' ';
                }
                resultInfo += 'Postal Code is empty.';
                error = true;
            }
                
            if(!error)
            {
                step = 'determine a business unit';
                SCBusinessUnit__c businessUnit = null;
                if (SCbase.isSet(country) && SCBase.isSet(postalCode))
                {
                    businessUnit = SCboArea.getBusinessUnitFirst(country, postalCode);
                }
                if (null != businessUnit)
                {
                    c.DepartmentCurrent__c = businessUnit.Id;
                    debug('succesful end of execute');       
                    count = 1;
                }
                else
                {
                    resultInfo = 'No business Unit found for country: ' + country + ' and  postal code: ' + postalCode+ '.';
                    error = true;
                }
            }
        }
        catch(Exception e)
        {
            thrownException = true;
            resultInfo = 'step: ' + step + ', exception: ' + e.getMessage();
            String prevMode = mode;
            mode = 'test';
            debug('resultInfo:' + resultInfo);
            mode = prevMode;
        }
        finally
        {
            /**
            E000 Success
            E001 Success with Info
            E100 Service not reachable
            E101 Service processing failed
            */
            String resultCode = 'E000';
            if(thrownException || error)
            {
                resultCode = 'E101';
            }
            logBatchInternal(interfaceLogList, 'CONTRACT_REPAIR', 'SCbtcContractRepair',
                                c.Id, null, resultCode, 
                                resultInfo, null, start, count); 
         }
    }// repairContract

 public static void logBatchInternal(List<SCInterfaceLog__c> interfaceLogList, String interfaceName, String interfaceHandler,
                                ID referenceID, ID referenceID2, String resultCode, 
                                String resultInfo, String data, Datetime start, Integer count)
    {
        SCInterfaceLog__c ic = new SCInterfaceLog__c();
        interfaceLogList.add(ic);
        ic.Interface__c = interfaceName;
        ic.InterfaceHandler__c = interfaceHandler;
        ic.Type__c = 'BATCH';
        ic.Count__c = count;
        ic.ReferenceID__c = referenceID;
        ic.ReferenceID2__c = referenceID2;
        ic.ResultCode__c = '';
        ic.ResultCode__c = resultCode;
        ic.ResultInfo__c = resultInfo;
        ic.Data__c = data;
        ic.Direction__c = 'internal';
        ic.Start__c = start;
        ic.Stop__c = Datetime.now();
        Long duration = ic.Stop__c.getTime() - ic.Start__c.getTime();
        ic.Duration__c = duration; 
        if(interfaceName == 'CONTRACT_VISIT'
            || interfaceName == 'CONTRACT_ORDER'
            || interfaceName == 'CONTRACT_VALIDATE'
            || interfaceName == 'CONTRACT_REPAIR')
        {
            ic.Contract__c = referenceID;
        }
    }       

    private void debug(String text)
    {
        if(mode.equalsIgnoreCase('test')
           || mode.equalsIgnoreCase('trace'))
        {
            System.debug('###...................' + text);
        }
    }

}
