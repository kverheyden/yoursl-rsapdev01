/*
 * @(#)SCboMatStatUpdate.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 Im 
 */
public class SCboMatStatUpdate
{
    private static SCfwDomain domMatStat = new SCfwDomain('DOM_MATERIALMOVEMENT_STATUS');
    private static List<SCfwDomainvalue> domValList = domMatStat.getDomainvalues();

    /**
     * Default constructor
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCboMatStatUpdate()
    {
        System.debug('##### SCboMatStatUpdate');
    } // SCboMatStatUpdate
    
    /**
     * Syncronize the material status in the order line depending 
     * on the status of a material movement entry
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public Boolean syncOrderLines(List<Id> orderLineIds)
    {
        System.debug('##### syncOrderLines: orderLineIds -> ' + orderLineIds);
        Boolean ret = false;
        
        Map<ID, Integer> matStatMap = new Map<Id, Integer>();
        Integer combMatStat = 0;
        // GMSMA: Excluded two tyes of MatMovements to prevent them from influencing the OrderLine's material-status
        for (SCMaterialMovement__c matMove :[Select Id, Status__c, Orderline__c, Type__c from SCMaterialMovement__c 
                                              where Orderline__c in :orderLineIds
                                                and Type__c != :SCfwConstants.DOMVAL_MATMOVETYP_RECEIPT_CONFIRMATION
                                                and Type__c != :SCfwConstants.DOMVAL_MATMOVETYP_SPAREPART_PACKAGE_RECEIVED])
        {
            if ((null != matMove.Id) && (null != matMove.Status__c))
            {
                SCfwDomainValue domVal = domMatStat.getDomainValue(matMove.Status__c);
                if (matStatMap.containsKey(matMove.Orderline__c))
                {
                    combMatStat = matStatMap.get(matMove.Orderline__c);
                    combMatStat |= domVal.getControlParameterInt('V1');
                    matStatMap.put(matMove.Orderline__c, combMatStat);
                } 
                else
                {
                    matStatMap.put(matMove.Orderline__c, domVal.getControlParameterInt('V1'));
                } // else [if (matStatMap.containsKey(matMove.Orderline__c))]
            } // if ((null != matMove.Id) && (null != matMove.Status__c))
        } // for (SCMaterialMovement__c matMove :[Select Id, MaterialStatus__c ...
        
        Set<Id> keySet = matStatMap.keySet();
        for (Id orderLineId :keySet)
        {
            combMatStat = matStatMap.get(orderLineId);
            Integer newMatStatV1 = getNewMatStatForOrderline(combMatStat);
            System.debug('##### syncOrderLines: [' + orderLineId + '] combMatStat  -> ' + combMatStat);
            System.debug('##### syncOrderLines: [' + orderLineId + '] newMatStatV1 -> ' + newMatStatV1);
            matStatMap.put(orderLineId, newMatStatV1);
        } // for (Id orderLineId :keySet)
        
        List<SCOrderLine__c> orderLines = [Select Id, MaterialStatus__c, Order__c, Type__c    
                                             from SCOrderLine__c 
                                            where Id in :orderLineIds];
        for (SCOrderLine__c orderLine :orderLines)
        {
            String newMatStat = getNewMatStat(matStatMap.get(orderLine.Id));
            System.debug('##### syncOrderLines: [' + orderLine.Id + '] newMatStat -> ' + newMatStat);
            System.debug('##### syncOrderLines: [' + orderLine.Id + '] Order__c   -> ' + orderLine.Order__c);
            
            if ((null != newMatStat) && !newMatStat.equals(orderLine.MaterialStatus__c))
            {
                orderLine.MaterialStatus__c = newMatStat;	
                ret = true;
                //GMSGB OrderLines that are in status ordered are set to a new type__c
                // old: type__c = '52702' // DOMVAL_ORDERLINETYPE_INV
                // new: Type__c = '52709' // DOMVAL_ORDERLINETYPE_SALESOFFER
                //this should prevent the orded oderline to be shown in the service report (Leistungsbeleg) PDF
                /*if(newMatStat == SCfwConstants.DOMVAL_MATSTAT_ORDERED)
                {
                	if(orderLine.Type__c == SCfwConstants.DOMVAL_ORDERLINETYPE_INV)
                	{
                 		orderLine.Type__c = SCfwConstants.DOMVAL_ORDERLINETYPE_MATERIAL_REQUIREMENT;
                	}
                }*/
            } // if ((null != newMatStat) && !newMatStat.equals(orderLine.MaterialStatus__c))
        } // for (SCOrderLine__c orderLine :orderLines)
        
        if (ret)
        {
            System.debug('##### syncOrderLines: orderLines -> ' + orderLines);
            update orderLines;
        } // if (ret)
        return ret;
    } // syncOrderLines
    
    /**
     * Syncronize the material status in an order depending 
     * on the status of a material movement entry
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public Boolean syncOrder(Id orderId)
    {
        List<Id> orderIds = new List<Id>();
        orderIds.add(orderId);
        
        return syncOrders(orderIds);
    } // syncOrder
    public Boolean syncOrders(List<Id> orderIds)
    {
        System.debug('##### syncOrders: orderIds -> ' + orderIds);
        Boolean ret = false;
        
        Integer combMatStat = 0;
        Map<Id, Integer> mapOrderMatStat = new Map<Id, Integer>();
        for (SCOrderLine__c orderLine :[Select Id, Order__c, MaterialStatus__c 
                                          from SCOrderLine__c 
                                         where Type__c = :SCfwConstants.DOMVAL_ORDERLINETYPE_INV 
                                           and Order__c = :orderIds])
        {
            if ((null != orderLine.Id) && (null != orderLine.MaterialStatus__c))
            {
                if (mapOrderMatStat.containsKey(orderLine.Order__c))
                {
                    combMatStat = mapOrderMatStat.get(orderLine.Order__c);
                }
                else
                {
                    combMatStat = 0;
                }
                
                SCfwDomainValue domVal = domMatStat.getDomainValue(orderLine.MaterialStatus__c);
                combMatStat |= domVal.getControlParameterInt('V1');
                mapOrderMatStat.put(orderLine.Order__c, combMatStat);
            } // if ((null != orderLine.Id) && (null != orderLine.MaterialStatus__c))
        } // for (SCOrderLine__c orderLine :[Select Id, MaterialStatus__c, Orderline__c ...
        
        for (Id orderId :mapOrderMatStat.keySet())
        {
            Integer newMatStatV1 = getNewMatStatForOrder(mapOrderMatStat.get(orderId));
            System.debug('##### syncOrder: orderId      -> ' + orderId);
            System.debug('##### syncOrder: combMatStat  -> ' + mapOrderMatStat.get(orderId));
            System.debug('##### syncOrder: newMatStatV1 -> ' + newMatStatV1);
            mapOrderMatStat.put(orderId, newMatStatV1);
        } // for (Id orderId :mapOrderMatStat.keySet())

        try
        {
            List<SCOrder__c> updOrders = new List<SCOrder__c>();
            
            for (SCOrder__c order :[Select Id, MaterialStatus__c from SCOrder__c 
                                     where Id = :orderIds])
            {
                String newMatStat = getNewMatStat(mapOrderMatStat.get(order.Id));
                System.debug('##### syncOrder: orderId    -> ' + order.Id);
                System.debug('##### syncOrder: newMatStat -> ' + newMatStat);
                
                if ((null != newMatStat) && !newMatStat.equals(order.MaterialStatus__c))
                {
                    SCOrder__c updOrd = new SCOrder__c(Id = order.Id);
                    updOrd.MaterialStatus__c = newMatStat;
                    updOrders.add(updOrd);
                    ret = true;
                } // if ((null != newMatStat) && !newMatStat.equals(orderLine.MaterialStatus__c))
            }
            
            if (ret)
            {
                System.debug('##### syncOrder: orders -> ' + updOrders);
                update updOrders;
            } // if (ret)
        } // try
        catch (Exception e)
        {
        	System.debug('##### SCboMatStatUpdate.syncOrder' +  e);
        } // catch (Exception e)
        return ret;
    } // syncOrders
    
    /**
     * Determines the new material status for an oder line 
     * depending on a given V1 value.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    private Integer getNewMatStatForOrderline(Integer matStatV1)
    {
        List<Integer> paramList = new List<Integer>();

        SCfwDomainValue domVal = domMatStat.getDomainValue(SCfwConstants.DOMVAL_MATSTAT_BOOK);
        paramList.add(domVal.getControlParameterInt('V1'));
        domVal = domMatStat.getDomainValue(SCfwConstants.DOMVAL_MATSTAT_ORDER);
        paramList.add(domVal.getControlParameterInt('V1'));
        domVal = domMatStat.getDomainValue(SCfwConstants.DOMVAL_MATSTAT_BOOKED);
        paramList.add(domVal.getControlParameterInt('V1'));
        domVal = domMatStat.getDomainValue(SCfwConstants.DOMVAL_MATSTAT_ORDERED);
        paramList.add(domVal.getControlParameterInt('V1'));
        domVal = domMatStat.getDomainValue(SCfwConstants.DOMVAL_MATSTAT_DELIVERED);
        paramList.add(domVal.getControlParameterInt('V1'));
        domVal = domMatStat.getDomainValue(SCfwConstants.DOMVAL_MATSTAT_CANCEL);
        paramList.add(domVal.getControlParameterInt('V1'));
        domVal = domMatStat.getDomainValue(SCfwConstants.DOMVAL_MATSTAT_CANCELLED);
        paramList.add(domVal.getControlParameterInt('V1'));
        
        for (Integer param :paramList)
        {
            if ((matStatV1 & param) == param)
            {
                return param;
            } // if ((matStatV1 & param) == param)
        } // for (Integer param :paramList)
        return 0;
    } // getNewMatStatForOrderline
    
    /**
     * Determines the new material status for an oder
     * depending on a given V1 value.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    private Integer getNewMatStatForOrder(Integer matStatV1)
    {
        List<Integer> paramList = new List<Integer>();

        SCfwDomainValue domVal = domMatStat.getDomainValue(SCfwConstants.DOMVAL_MATSTAT_ORDERABLE);
        paramList.add(domVal.getControlParameterInt('V1'));
        domVal = domMatStat.getDomainValue(SCfwConstants.DOMVAL_MATSTAT_ORDER);
        paramList.add(domVal.getControlParameterInt('V1'));
        domVal = domMatStat.getDomainValue(SCfwConstants.DOMVAL_MATSTAT_ORDERED);
        paramList.add(domVal.getControlParameterInt('V1'));
        domVal = domMatStat.getDomainValue(SCfwConstants.DOMVAL_MATSTAT_IN_TRANSIT);
        paramList.add(domVal.getControlParameterInt('V1'));
        domVal = domMatStat.getDomainValue(SCfwConstants.DOMVAL_MATSTAT_CANCEL);
        paramList.add(domVal.getControlParameterInt('V1'));
        domVal = domMatStat.getDomainValue(SCfwConstants.DOMVAL_MATSTAT_DELIVERED);
        paramList.add(domVal.getControlParameterInt('V1'));
        domVal = domMatStat.getDomainValue(SCfwConstants.DOMVAL_MATSTAT_BOOK);
        paramList.add(domVal.getControlParameterInt('V1'));
        domVal = domMatStat.getDomainValue(SCfwConstants.DOMVAL_MATSTAT_BOOKED);
        paramList.add(domVal.getControlParameterInt('V1'));
        domVal = domMatStat.getDomainValue(SCfwConstants.DOMVAL_MATSTAT_CANCELLED);
        paramList.add(domVal.getControlParameterInt('V1'));
        
        for (Integer param :paramList)
        {
            if ((matStatV1 & param) == param)
            {
                return param;
            } // if ((matStatV1 & param) == param)
        } // for (Integer param :paramList)
        return 0;
    } // getNewMatStatForOrder
    
    /**
     * Determines the new material status 
     * depending on a given V1 value.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    private String getNewMatStat(Integer matStatV1)
    {
        for (SCfwDomainvalue domVal :domValList)
        {
            if (matStatV1 == domVal.getControlParameterInt('V1'))
            {
                return domVal.getID();
            } // if (matStatV1 == domVal.getControlParameterInt('V1'))
        } // for (SCfwDomainvalue domVal :domValList)
        return null;
    } // getNewMatStat

    /**
     * Will be called by an update trigger on SCMaterialMovement__c.
     * Checks the chnage of the material status and updates order lines and 
     * orders if necessary.
     *
     * @param    itemsNew    list of new material movements
     * @param    itemsOld    list of old material movements
     */
    public void AfterUpdate(List<SCMaterialMovement__c> itemsNew, List<SCMaterialMovement__c> itemsOld)
    {
        System.debug('#### SCboMatStatUpdate.AfterUpdate()');
        
        SCMaterialMovement__c newEntry = null;    
        SCMaterialMovement__c oldEntry = null;    
    
        Set<Id> orderLineIds = new Set<Id>();
        Set<Id> orderIds = new Set<Id>();

        // check the change of the material status and create lists
        // for orders and order lines
        for (Integer i=0; i<itemsNew.size(); i++)
        {
            newEntry = itemsNew.get(i);    
            oldEntry = itemsOld.get(i);

            if (((SCfwConstants.DOMVAL_MATSTAT_ORDER == oldEntry.Status__c) && (SCfwConstants.DOMVAL_MATSTAT_ORDERED == newEntry.Status__c) ||
                 (SCfwConstants.DOMVAL_MATSTAT_ORDER == oldEntry.Status__c) && (SCfwConstants.DOMVAL_MATSTAT_DELIVERED == newEntry.Status__c) ||
                 (SCfwConstants.DOMVAL_MATSTAT_ORDERED == oldEntry.Status__c) && (SCfwConstants.DOMVAL_MATSTAT_DELIVERED == newEntry.Status__c) ||
                 (SCfwConstants.DOMVAL_MATSTAT_ORDERED == oldEntry.Status__c) && (SCfwConstants.DOMVAL_MATSTAT_CANCELLED == newEntry.Status__c) ||
                 (SCfwConstants.DOMVAL_MATSTAT_IN_TRANSIT == oldEntry.Status__c) && (SCfwConstants.DOMVAL_MATSTAT_DELIVERED == newEntry.Status__c)
                 || (SCfwConstants.DOMVAL_MATSTAT_BOOK == newEntry.Status__c) 
                 || (SCfwConstants.DOMVAL_MATSTAT_BOOKED == newEntry.Status__c) 
                ) && 
                SCBase.isSet(newEntry.OrderLine__c) && 
                SCbase.isSet(newEntry.Order__c) &&
                newEntry.type__c != SCfwConstants.DOMVAL_MATMOVETYP_RECEIPT_CONFIRMATION && 
                newEntry.type__c != SCfwConstants.DOMVAL_MATMOVETYP_SPAREPART_PACKAGE_RECEIVED)
            {
                if (!orderIds.contains(newEntry.Order__c))
                {
                    orderIds.add(newEntry.Order__c);
                }
                if (!orderLineIds.contains(newEntry.OrderLine__c))
                {
                    orderLineIds.add(newEntry.OrderLine__c);
                }
            } // if ((SCfwConstants.DOMVAL_MATSTAT_ORDER == oldEntry.Status__c) && ...
            else if(!SCBase.isSet(oldEntry.OrderLine__c) && SCBase.isSet(newEntry.OrderLine__c)  &&
                     SCBase.isSet(newEntry.Order__c)     && SCfwConstants.DOMVAL_MATSTAT_ORDER == newEntry.Status__c)
            { // GMSMA: Handle newly created MatMovements. We have to mark attached orders and OrderLines for an update  
                if (!orderIds.contains(newEntry.Order__c))
                {
                    orderIds.add(newEntry.Order__c);
                }
                if (!orderLineIds.contains(newEntry.OrderLine__c))
                {
                    orderLineIds.add(newEntry.OrderLine__c);
                }
            } // else if (....)
           
        } // for (Integer i=0; i<itemsNew.size(); i++)

        if ((orderIds.size() > 0) && (orderLineIds.size() > 0))
        {
            List<Id> orderLineIdList = new List<Id>();
            orderLineIdList.addAll(orderLineIds);            
            syncOrderLines(orderLineIdList);
            
            List<Id> orderIdList = new List<Id>();
            orderIdList.addAll(orderIds);
            syncOrders(orderIdList);
        }
    } // AfterUpdate

} // SCboMatStatUpdate
