public class cc_cceag_cokeid_settings 
{
	public String cokeIdLogoutUrl {get; set;}
    public String cokeIdChangePasswordUrl {get; set;}
    public String cokeIdAccountSettingsUrl {get; set;}
	
	public cc_cceag_cokeid_settings()
	{		
        ecomSettings__c setting = [SELECT value__c FROM ecomSettings__c WHERE Name='ECOMCokeIDLogoutURL'];
		this.cokeIdLogoutUrl = setting.value__c;
        
		setting = [SELECT value__c FROM ecomSettings__c WHERE Name='ECOMCokeIDChangePasswordURL'];
		this.cokeIdChangePasswordUrl = setting.value__c;        
        
        setting = [SELECT value__c FROM ecomSettings__c WHERE Name='ECOMCokeIDAccountSettingsURL'];
		this.cokeIdAccountSettingsUrl = setting.value__c;        
	}
}
