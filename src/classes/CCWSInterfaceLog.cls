/*
 * @(#)CCWSInterfaceLog.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/*
 * The class is used for management of the interface log object
 * and writing it to the data base. It uses the interface
 * CCWSGenericResponseInterface to get to the response data
 * of each specific incomming web service.
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
public without sharing class CCWSInterfaceLog 
{
    public SCInterfaceBase ib = new SCInterfaceBase();
    public SCInterfaceLog__c interfaceLog;
    public String exceptionInfo = null;

    public LogPart inputData;
    public LogPart outputData;
    public LogPartList outputDataList;
    public Integer severityCode;    
    public CCWSGenericResponseInterface gri;
    public Boolean rawlog;
    
    public class LogPart
    {
        String title;
        Object obj;
    }
 
    public class LogPartList
    {
        String title;
        List<Object> objList;
    }

    public CCWSInterfaceLog(String interfaceName, String interfaceHandler, 
                    String type, String direction, CCWSGenericResponseInterface gri)
    {
        this(interfaceName, interfaceHandler, type, direction, gri, false);
    }    
    public CCWSInterfaceLog(String interfaceName, String interfaceHandler, 
                    String type, String direction, CCWSGenericResponseInterface gri, Boolean rawlog)
    {
        interfaceLog = new SCInterfaceLog__c();
        interfaceLog.Interface__c = interfaceName;
        interfaceLog.InterfaceHandler__c = interfaceHandler;
        interfaceLog.Type__c = type;
        interfaceLog.Direction__c = direction;
        interfaceLog.Start__c = DateTime.now();
        severityCode = 0;
        interfaceLog.ResultCode__c = 'E000';
        interfaceLog.ResultInfo__c = 'Success';
        interfaceLog.Data__c = '';
        interfaceLog.Count__c = 0;
        this.gri = gri;
        this.rawlog = rawlog;
        initiateListOfOutputData('Output from ClockPort:');
    }


    public void setReference1(String refType, ID referenceID)
    {
        interfaceLog.ReferenceID__c = referenceID;
        if(refType != null )
        {
            if(refType == 'SCOrder__c')
            {
                interfaceLog.Order__c = referenceID;
            }
            else if(refType == 'SCStock__c')
            {
                interfaceLog.Stock__c = referenceID;
            }
            else if(refType == 'SCInventory__c')
            {
                interfaceLog.Inventory__c = referenceID;
            }

        }
    }
    public void setReference2(String refType, ID referenceID)
    {
        interfaceLog.ReferenceID2__c = referenceID;
        if(refType != null )
        {
            if(refType == 'SCOrder__c')
            {
                interfaceLog.Order__c = referenceID;
            }
            else if(refType == 'SCStock__c')
            {
                interfaceLog.Stock__c = referenceID;
            }
            else if(refType == 'SCInventory__c')
            {
                interfaceLog.Inventory__c = referenceID;
            }
        }
    }

    public Integer incrementCount(Integer value)
    {
        interfaceLog.Count__c += value;
        return (Integer)interfaceLog.Count__c;
    }

    public void setMessageID(String msgID)
    {
        interfaceLog.MessageID__c = msgID;
    }
    
    public void setResultCode(String rc)
    {
        interfaceLog.ResultCode__c = rc;
    }
    
    public void setResultInfo(String ri)
    {
        interfaceLog.ResultInfo__c = ri;
    }
    
    public void appendData(String data)
    {
        interfaceLog.Data__c += data;
    }
    public void addLeadingData(String data)
    {
        interfaceLog.Data__c = data + interfaceLog.Data__c;
    }
    
    public void appendStructuredData(String title, Object obj) 
    {
        interfaceLog.Data__c += '\n' + title + '\n';
        interfaceLog.Data__c += getStructuredData(obj);
    }

    public String getStructuredData(Object obj)  
    {
        String retValue = '';
        if(obj != null)
        {
            String jsonInput = JSON.serialize(obj);
            if(rawLog)
            {
                // System.LimitException: Too many script statements: 200001 
                retValue = '' + obj;    
            }
            else
            {
                retValue  = ib.getDataFromJSON(jsonInput);
            }   
            debug('from json object: ' + retValue);
        }
        return retValue;    
    }

    public String getXMLData(String function, String headStructureName, String prefix, String nameSpace, Object obj)  
    {
        String retValue = '';
        if(obj != null)
        {
            String jsonInput = JSON.serialize(obj);
            retValue = ib.createSOAPUICall(function, headStructureName, prefix, nameSpace, jsonInput);
            debug('from json object: ' + retValue);
        }
        return retValue;    
    }

    public void addExceptionInfo(String exceptionInfo)
    {
        if(this.exceptionInfo == null)
        {
            this.exceptionInfo = exceptionInfo;
        }
        else
        {
            this.exceptionInfo += '\nnextException:\n' + exceptionInfo;
        }   
    }
    

    public SCInterfaceLog__c getInterfaceLog()
    {
        return interfaceLog;
    }
    
    public void addInputData(String title, Object inputData)
    {
        this.inputData = new LogPart();
        this.inputData.title = title;
        this.inputData.obj = inputData;
        debug('addInputData: ' + this.inputData.obj);
    }
    
    public void addOutputData(String title, Object outputData)
    {
        this.outputData = new LogPart();
        this.outputData.title = title;
        this.outputData.obj = outputData;
    }

    public void initiateListOfOutputData(String title)
    {
        this.outputDataList = new LogPartList();
        this.outputDataList.title = title;
        this.outputDataList.objList = new List<Object>();
    }

    public void addOutputDataToList(Object outputData)
    {
        if(this.outputDataList.objList != null)
        {
            this.outputDataList.objList.add(outputData);
        }
        else
        {
            throw new SCfwException('CCWSInterfaceLog.outputDataList.objList is not allocated by initiateListOfOutputData() function');
        }   
    }


    public void writeInterfaceLog()
    {
        writeInterfaceLog(null, null, null, null, null, null);
    }

    public void addInterfaceLog()
    {
        addInterfaceLog(null, null, null, null, null, null);
    }

    public void addInterfaceLog(String prefix, String functionIn, String headStructureNameIn, 
                                  String functionOut, String headStructureNameOut, String nameSpace)
    {
        if(interfaceLog.Data__c == null || interfaceLog.Data__c != null && interfaceLog.Data__c.length() < 32000)
        {
            if(exceptionInfo != null)
            {
                interfaceLog.Data__c += exceptionInfo;          
            }
            
            if(outputData != null && interfaceLog.Type__c == 'INBOUND')
            {
                interfaceLog.ResultCode__c = gri.getResultCode(outputData.obj);
                interfaceLog.ResultInfo__c = gri.getResultInfoForAll(outputData.obj);
            }
            else
            {
                // TODO:
            }   
            if(inputData != null)
            {
                interfaceLog.Data__c += '\n' + inputData.title;
                debug('writeInterfaceLog inputData: ' + inputData.obj);
                interfaceLog.Data__c += '\n' + getStructuredData(inputData.obj) + '\n';
            }
            if(outputData != null && outputDataList != null && outputDataList.objList != null && outputDataList.objList.size() == 0)
            {
                // if there is no output data list
                interfaceLog.Data__c += '\n' + outputData.title;
                debug('outputdata: ' + outputData);
                interfaceLog.Data__c += '\n' + getStructuredData(outputData.obj) + '\n';
            }
            if(outputDataList != null && outputDataList.objList != null && outputDataList.objList.size() > 0)
            { 
                // if there is an output data list with at least one element
                interfaceLog.Data__c += '\n' + outputDataList.title;
                debug('outputdataList: ' + outputDataList);
                interfaceLog.Data__c += '\n' + getStructuredData(outputDataList.objList) + '\n';
            }
    
            if(inputData != null && functionIn != null)
            {
                interfaceLog.Data__c += '\n' + inputData.title;
                interfaceLog.Data__c += '\n' + getXMLData(functionIn, headStructureNameIn, prefix, nameSpace, inputData.obj) + '\n';
            }
            if(outputData != null && functionOut != null)
            {
                interfaceLog.Data__c += '\n' + outputData.title;
                debug('outputdata: ' + outputData);
                interfaceLog.Data__c += '\n' + getXMLData(functionOut, headStructureNameOut, prefix, nameSpace, outputData.obj) + '\n';
            }
            if(interfaceLog.Data__c != null)
            {
                interfaceLog.Data__c = interfaceLog.Data__c.left(32000);
            
            }
            // GMSGB prevent Exception when result info is too long.
            if(interfaceLog.ResultInfo__c != null)
            {
                interfaceLog.ResultInfo__c = interfaceLog.ResultInfo__c.left(32000);
            }    
            interfaceLog.Stop__c = Datetime.now();
            Long duration = interfaceLog.Stop__c.getTime() - interfaceLog.Start__c.getTime();
            interfaceLog.Duration__c = duration;
        }      
    }
    
    public void writeInterfaceLog(String prefix, String functionIn, String headStructureNameIn, 
                                  String functionOut, String headStructureNameOut, String nameSpace)
    {
       
        
        addInterfaceLog(prefix, functionIn, headStructureNameIn, 
                        functionOut, headStructureNameOut, nameSpace);
        upsert interfaceLog;        
    }
    

    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }


}
