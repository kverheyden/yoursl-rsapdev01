/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CCPluginSendOrderTest {

    private static void setupPromoData(String AccountId,String accGrpId){
        ccrz__E_Promo__c promo = new ccrz__E_Promo__c(Name = 'Test Promo',ccrz__Sequence__c = 500);
        insert promo;

        ccrz__E_PromotionAccountGroupFilter__c promoAccGrpFilter = new ccrz__E_PromotionAccountGroupFilter__c();
        promoAccGrpFilter.ccrz__CC_Promotion__c = promo.Id;
        promoAccGrpFilter.CCAccount__c = AccountId;
        promoAccGrpFilter.ccrz__FilterType__c = 'Inclusive';
        promoAccGrpFilter.ccrz__CC_AccountGroup__c = accGrpId;
        insert promoAccGrpFilter;

        E_PromotionProduct__c promoProduct = new E_PromotionProduct__c(Product__r=new ccrz__E_Product__c(ccrz__SKU__c='sku_1'),Promotion__c = promo.Id);
        insert promoProduct;


    }

    static testMethod void myUnitTestHappy() {
    	ccrz__E_AccountGroup__c agA = new ccrz__E_AccountGroup__c(Name='A');
		insert agA;
    	Account acc = new Account();
     	acc.Name = 'daoTestAccount';
     	acc.AccountNumber = '123456';
     	acc.Subtradechannel__c = '0';
     	acc.ccrz__E_AccountGroup__c = agA.Id;
     	insert acc;
    	ccrz__E_ContactAddr__c billTo = new ccrz__E_ContactAddr__c(
			ccrz__AddressFirstline__c='100 Pine Street', ccrz__City__c='SmallTown', ccrz__DaytimePhone__c='(847) 555-1212',
			ccrz__FirstName__c='Test', ccrz__HomePhone__c='(847) 555-1212', ccrz__LastName__c='User',
			ccrz__PostalCode__c='60601', ccrz__State__c='Idaho', ccrz__StateISOCode__c='ID',
			ccrz__CountryISOCode__c='USA');
		ccrz__E_ContactAddr__c shipTo = new ccrz__E_ContactAddr__c(
			ccrz__AddressFirstline__c='100 Pine Street', ccrz__City__c='SmallTown', ccrz__DaytimePhone__c='(847) 555-1212',
			ccrz__FirstName__c='Craig', ccrz__HomePhone__c='(847) 555-1212', ccrz__LastName__c='Traxler',
			ccrz__PostalCode__c='60601', ccrz__State__c='Idaho', ccrz__StateISOCode__c='ID',
			ccrz__CountryISOCode__c='USA', ccrz__Partner_Id__c=acc.AccountNumber);
		insert new List<ccrz__E_ContactAddr__c> {billTo, shipTo};
		
    	List<ccrz__E_Product__c> ps = new List<ccrz__E_Product__c>();
    	for(integer i=1; i <= 5; i++) {
    		string sku = 'sku_'+ string.valueOf(i);
    		ps.add(new ccrz__E_Product__c(Name='Product' + string.valueOf(i), ccrz__Sku__c=sku, ccrz__ProductStatus__c='Released', NumberOfSalesUnitsPerPallet__c=12));
    	}
		insert ps;
		
		Date requestDate = Date.today().addMonths(1); 
    	list<ccrz__E_Order__c> orders = new list<ccrz__E_Order__c>();
        for(integer i=1; i <= 1; i++) {
        	orders.add(new ccrz__E_Order__c(ccrz__storefront__c='DefaultStore', ccrz__OrderId__c='MyTestOrder'+string.valueOf(i),
        		ccrz__BillTo__c = billTo.Id, ccrz__ShipTo__c = shipTo.Id, ccrz__RequestDate__c = requestDate, ccrz__User__c=UserInfo.getUserId(),ccrz__Account__c = acc.Id));
        }
        insert orders;
        
        
        list<ccrz__E_OrderItem__c> orderItems = new list<ccrz__E_OrderItem__c>();
        for(ccrz__E_Order__c o : orders) {
        	for(integer j=1; j <= 5 ; j++) {
        		string skuName = 'sku_' + string.valueOf(j);
        		orderItems.add(new ccrz__E_OrderItem__c(ccrz__Quantity__c=1, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c=skuName), 
        			ccrz__order__c=o.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0, ccrz__RequestDate__c = requestDate));
        	}
        }
		insert orderItems;

        setupPromoData(acc.Id,agA.Id);
		 CCSettings__c ccSettings = new CCSettings__c();
        ccSettings.SAPDispBasicAuthUsername__c = 'user';
        ccSettings.SAPDispBasicAuthPassword__c = 'passsword';
        ccSettings.SAPDispEndpointAssetInventory__c = 'AssettEndpoint';
        ccSettings.SAPDispServer__c = 'Server';
        ccSettings.SAPDispEndpointSalesOrderRequestResponse__c='Server';
        insert ccSettings;
		Test.startTest();
		Test.setMock(WebServiceMock.class, new cc_cceag_SendOrderMock());
		CCPluginSendOrder plugin = new CCPluginSendOrder();
		plugin.setStorefrontSettings(new map<string, object>{});
		plugin.setDaoObject(null);
		plugin.setServiceObject(null);
		plugin.sendOrder(new Map<String,Object>(),null,null);
		system.assert(plugin.sendOrder(orders[0].id, 'DefaultStore', true) == null);
		Test.stopTest();
    }
    
    static testMethod void myUnitTestSad() {
    	
    	ccrz__E_ContactAddr__c billTo = new ccrz__E_ContactAddr__c(
			ccrz__AddressFirstline__c='100 Pine Street', ccrz__City__c='SmallTown', ccrz__DaytimePhone__c='(847) 555-1212',
			ccrz__FirstName__c='Test', ccrz__HomePhone__c='(847) 555-1212', ccrz__LastName__c='User',
			ccrz__PostalCode__c='60601', ccrz__State__c='Idaho', ccrz__StateISOCode__c='ID',
			ccrz__CountryISOCode__c='USA');
		ccrz__E_ContactAddr__c shipTo = new ccrz__E_ContactAddr__c(
			ccrz__AddressFirstline__c='100 Pine Street', ccrz__City__c='SmallTown', ccrz__DaytimePhone__c='(847) 555-1212',
			ccrz__FirstName__c='Craig', ccrz__HomePhone__c='(847) 555-1212', ccrz__LastName__c='Traxler',
			ccrz__PostalCode__c='60601', ccrz__State__c='Idaho', ccrz__StateISOCode__c='ID',
			ccrz__CountryISOCode__c='USA');
		insert new List<ccrz__E_ContactAddr__c> {billTo, shipTo};
		
    	List<ccrz__E_Product__c> ps = new List<ccrz__E_Product__c>();
    	for(integer i=1; i <= 5; i++) {
    		string sku = 'sku_'+ string.valueOf(i);
    		ps.add(new ccrz__E_Product__c(Name='Product' + string.valueOf(i), ccrz__Sku__c=sku, ccrz__ProductStatus__c='Released', NumberOfSalesUnitsPerPallet__c=12));
    	}
		insert ps;
		
		Date requestDate = Date.today().addMonths(1); 
    	list<ccrz__E_Order__c> orders = new list<ccrz__E_Order__c>();
        for(integer i=1; i <= 1; i++) {
        	orders.add(new ccrz__E_Order__c(ccrz__storefront__c='DefaultStore', ccrz__OrderId__c='MyTestOrder'+string.valueOf(i),
        		ccrz__BillTo__c = billTo.Id, ccrz__ShipTo__c = shipTo.Id, ccrz__RequestDate__c = requestDate));
        }
        insert orders;
        
        
        list<ccrz__E_OrderItem__c> orderItems = new list<ccrz__E_OrderItem__c>();
        for(ccrz__E_Order__c o : orders) {
        	for(integer j=1; j <= 5 ; j++) {
        		string skuName = 'sku_' + string.valueOf(j);
        		orderItems.add(new ccrz__E_OrderItem__c(ccrz__Quantity__c=1, ccrz__product__r=new ccrz__E_Product__c(ccrz__SKU__c=skuName), 
        			ccrz__order__c=o.id, ccrz__price__c=100, ccrz__SubAmount__c=100.0, ccrz__RequestDate__c = requestDate));
        	}
        }
		insert orderItems;
		 CCSettings__c ccSettings = new CCSettings__c();
        ccSettings.SAPDispBasicAuthUsername__c = 'user';
        ccSettings.SAPDispBasicAuthPassword__c = 'passsword';
        ccSettings.SAPDispEndpointAssetInventory__c = 'AssettEndpoint';
        ccSettings.SAPDispServer__c = 'Server';
        ccSettings.SAPDispEndpointSalesOrderRequestResponse__c='Server';
        insert ccSettings;
		Test.startTest();
		Test.setMock(WebServiceMock.class, new cc_cceag_SendOrderNullResponseMock());
		CCPluginSendOrder plugin = new CCPluginSendOrder();
		try {
			plugin.sendOrder(orders[0].id, 'DefaultStore', true);
			system.assert(false);
		} catch(CCWSSalesOrderException e) {
			system.assert(true);
		}
		Test.stopTest();
    }
}
