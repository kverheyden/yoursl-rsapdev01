/*
 * @(#)SCItemsTabComponentController.cls
 *
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 *
 * Class Hierarchy
 * ===============
 *
 *                                      this class
 *                                          |
 * SCItemsTabComponentController  <---------+
 * SCOrderTabComponentController
 * SCPartsServicesTabComponentController
 *          |
 * SCOrderComponentController
 *          |
 * SCfwOrderControllerBase
 *          |
 * SCfwComponentControllerBase
 *
 */

/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCItemsTabComponentController extends SCOrderComponentController
{
    public static Map<String, SCfwDomain> domMap = SCfwDomain.createDomains('DOM_PRODUCTUNITTYPE,DOM_PRODUCTGROUP,DOM_PRODUCTPOWER,DOM_PRODUCTENERGY');
    private static SCfwDomain domUnitType = domMap.get('DOM_PRODUCTUNITTYPE');
    private static SCfwDomain domProdGroup = domMap.get('DOM_PRODUCTGROUP');
    private static SCfwDomain domProdPower = domMap.get('DOM_PRODUCTPOWER');
    private static SCfwDomain domProdEnergy = domMap.get('DOM_PRODUCTENERGY');

    private SCfwConfOrderDuration orderDuration = null;

    // Show for the validation of the serial number
    public String isValidSerialNumber { get; private set; }
    public Boolean productIdFounded { get; private set; }
    public SCArticle__c articleFounded { get; set; }

    /*
     * Usage example:
     * ((SCProcessOrderController)pageController).varInController;
     * ((SCProcessOrderController)pageController).methodInController();
     */


    /**
     * Constructor
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCItemsTabComponentController()
    {
        // REMARK: Don't use the pageController in the constructor
        //         since it's not assigned yet for some weird reasons.
        orderDuration = new SCfwConfOrderDuration();
    } // SCItemsTabComponentController

    /**
     * Getter and setter for the service recipient of the current order.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCOrderRole__c serviceRecipient
    {
        get
        {
            this.serviceRecipient = boOrder.getRole(SCfwConstants.DOMVAL_ORDERROLE_LE);
            System.debug('### getServiceRecipient -> ' + this.serviceRecipient);
            return this.serviceRecipient;
        }

        set
        {
            this.serviceRecipient = value;
            System.debug('### setServiceRecipient -> ' + value);
        }
    } // serviceRecipient

    /**
     * This method is called for the validation of the
     * serial number. If the serial number is valid, the
     * boolean variable isValidSerialNumber will be set true.
     *
     * @return    PageReference of the follow-up page
     *
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference validateSerialNumber()
    {
        System.debug('#### validateSerialNumber()');
        //isValidSerialNumber = false;
        isValidSerialNumber = 'check';

        String serialNumber = currentOrderItem.orderItem.InstalledBase__r.SerialNo__c;

        if (serialNumber != null &&
            SCutilSerialNumber.isValidSerialNumber(serialNumber) && (serialNumber.length() > 7))
        {
            //isValidSerialNumber = true;
            isValidSerialNumber = 'ok';
            extractSerialNumberDetails();
        }
        else
        {
            //isValidSerialNumber = true;
            isValidSerialNumber = 'not_ok';
        }
        System.debug('#### isValidSerialNumber: '+isValidSerialNumber);
        return null;
    }

    /**
     * This method is called for the extraction of the
     * details for the serial number.
     *
     * @return    PageReference of the follow-up page
     *
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference extractSerialNumberDetails()
    {
        System.debug('#### extractSerialNumberDetails()');

        String serialNumber = currentOrderItem.orderItem.InstalledBase__r.SerialNo__c;

        if (serialNumber != null)
        {
            //isValidSerialNumber = true;
            SCutilSerialNumber tempSerialNumber = new SCutilSerialNumber (serialNumber);
            Map <String, String> serialNumberDetails = new Map <String, String> ();
            serialNumberDetails = tempSerialNumber.getSerialNumberDetails();
            System.debug('#### serialNumberDetails: '+serialNumberDetails);

            //currentOrderItem.orderItem.InstalledBase__r.Article__c = serialNumberDetails.get('articleNumber');
            // search for article, if article is founded, fill the article fields
            try
            {
                String searchArticleNumber = '%'+serialNumberDetails.get('articleNumber');
                
                articleFounded = [SELECT Id, ArticleNameCalc__c, Name, EANCode__c, ProductModel__c 
                                                    FROM SCArticle__c
                                                    where Name LIKE :searchArticleNumber];
                
                System.debug('#### articleFounded: '+articleFounded);
                currentOrderItem.orderItem.InstalledBase__r.Article__c = articleFounded.Id;
                currentOrderItem.orderItem.InstalledBase__r.ArticleName__c = articleFounded.ArticleNameCalc__c;
                currentOrderItem.orderItem.InstalledBase__r.ArticleEAN__c = articleFounded.EANCode__c;
                
                String articleProductId = articleFounded.ProductModel__c;
                
                if (articleProductId.length() > 0)
                {
                    productIdFounded = true;
                    //currentOrderItem.orderItem.InstalledBase__r.ProductModel__c = articleFounded.ProductModel__c;
                    productModelId = articleFounded.ProductModel__c;
                    System.debug('#### set productModelId : '+productModelId);
                    System.debug('#### set productIdFounded : '+productIdFounded);
                    chgProductModel(articleFounded.ProductModel__c);
                }    
            }
            catch (Exception e)
            {
                productIdFounded = false;
                currentOrderItem.orderItem.InstalledBase__r.Article__c = null;
            }

            // try to fill the contruction year field            
            try
            {
                currentOrderItem.orderItem.InstalledBase__r.ConstructionYear__c =
                    Decimal.ValueOf(serialNumberDetails.get('constructionYear'));
            }        
            catch (Exception e)
            {
                currentOrderItem.orderItem.InstalledBase__r.ConstructionYear__c = null;
            }

            // try to fill the contruction week field
            try
            {
                currentOrderItem.orderItem.InstalledBase__r.ConstructionWeek__c =
                    Decimal.ValueOf(serialNumberDetails.get('constructionWeek'));
            }        
            catch (Exception e)
            {
                currentOrderItem.orderItem.InstalledBase__r.ConstructionWeek__c = null;
            }
        }
        else
        {
            // something here
        }

        return null;
    }

    /**
     * This method is called for the reset of the serial
     * number and of the serial number details.
     *
     * @return    PageReference of the follow-up page
     *
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference resetSerialNumberDetails()
    {
        System.debug('#### resetSerialNumberDetails()');
        isValidSerialNumber = 'check';

        currentOrderItem.orderItem.InstalledBase__r.SerialNo__c = null;
        currentOrderItem.orderItem.InstalledBase__r.ConstructionYear__c = null;
        currentOrderItem.orderItem.InstalledBase__r.ConstructionWeek__c = null;
        currentOrderItem.orderItem.InstalledBase__r.Article__c = null;
        currentOrderItem.orderItem.InstalledBase__r.ArticleName__c = null;
        currentOrderItem.orderItem.InstalledBase__r.ArticleEAN__c = null;
        return null;
    }


    /**
     * Getter and setter for a comma separated list of all
     * installed base ids, which are assigned to the order.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public String assignedInstBases
    {
        get
        {
            return getAssignedInstBases(boOrder);
        }

        set;
    } // assignedInstBases


    /**
     * Getter and setter for the id of the product model.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public String productModelId
    {
        get
        {
            System.debug('### productModelId (getter) -> ' + this.productModelId);
            return this.productModelId;
        }

        set
        {
            System.debug('### productModelId (setter) -> ' + value);
            this.productModelId = value;
        }
    } // productModelId

    /**
     * Creates a new order item with the selected installed base
     * and adds this to the order item list in the order.
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference addNewOrderItem()
    {
        PageReference page = addNewOrderItem(boOrder, orderDuration);
        currentOrderItem = curOrderItem;
        return page;
    } // addNewOrderItem

    /**
     * Changes the installed base in the current selected
     * order item.
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference chgOrderItem()
    {
        PageReference page = chgOrderItem(boOrder, orderDuration);
        currentOrderItem = curOrderItem;
        return page;
    } // chgOrderItem

    /**
     * Gets the selected oreder item ID2 from the page and
     * sets the intern order item object for displaying the
     * history and error description.
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference selectItem()
    {
        PageReference page = selectItem(boOrder);
        System.debug('#### SCItemsTabComponentController.selectItem(): ID2 -> ' + curOrderItem.orderItem.ID2__c);
        currentOrderItem = curOrderItem;
        return page;
    } // selectItem

    /**
     * Deletes an order item.
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference delItem()
    {
        PageReference page = delItem(boOrder);
        // check if there is an existing order item deleted
        // (new added have no Id)
        // if yes, add it to the list, so that it can be
        // deleted during saving the order
        if (null != deletedOrderItemId)
        {
            deletedOrderItemIds.add(deletedOrderItemId);
        } // if (null != deletedOrderItemId)
        return page;
    } // delItem

    /**
     * Gets a list of unit types depending on the product class
     * of the current selected order item.
     *
     * @return    list of select options containing the unit types
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public List<SelectOption> getUnitType()
    {
        System.debug('#### getUnitType(): unit class -> ' +
                     curOrderItem.orderItem.InstalledBase__r.ProductUnitClass__c);
        return domUnitType.getDomainValuesByMaster(curOrderItem.orderItem.InstalledBase__r.ProductUnitClass__c);
    } // getUnitType

    /**
     * Gets a list of product groups depending on the unit type
     * of the current selected order item.
     *
     * @return    list of select options containing the product groups
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public List<SelectOption> getProductGroup()
    {
        System.debug('#### getProductGroup(): unit class -> ' +
                     curOrderItem.orderItem.InstalledBase__r.ProductUnitClass__c);
        System.debug('#### getProductGroup(): unit type -> ' +
                     curOrderItem.orderItem.InstalledBase__r.ProductUnitType__c);
        return domProdGroup.getDomainValuesByMasterEx(curOrderItem.orderItem.InstalledBase__r.ProductUnitClass__c,
                                                      curOrderItem.orderItem.InstalledBase__r.ProductUnitType__c);
    } // getProductGroup

    /**
     * Gets a list of product power depending on the product group
     * of the product model of the current selected order item.
     *
     * @return    list of select options containing the product powers
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public List<SelectOption> getProductPower()
    {
        System.debug('#### getProductPower(): product group -> ' +
                     curOrderItem.orderItem.InstalledBase__r.ProductGroup__c);
        return domProdPower.getDomainValuesByMaster(curOrderItem.orderItem.InstalledBase__r.ProductGroup__c);
    } // getProductPower

    /**
     * Gets a list of product energy depending on the product group
     * of the product model of the current selected order item. If the list
     * contains the product energy "2" (L / aardgas), the list item is selected.
     *
     * @return    list of select options containing the product energys
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public List<SelectOption> getProductEnergy()
    {
        System.debug('#### getProductEnergy(): product group -> ' +
                     curOrderItem.orderItem.InstalledBase__r.ProductGroup__c);
        
        // save the current energy list
        List <SelectOption> currentProductEnergyList =
            domProdEnergy.getDomainValuesByMaster(curOrderItem.orderItem.InstalledBase__r.ProductGroup__c);
            
        System.debug('#### getProductEnergy() currentProductEnergyList: '+currentProductEnergyList);
        
        for (Integer i = 0; i < currentProductEnergyList.size(); i++)
        {
            System.debug ('currentProductEnergyList[i]: '+currentProductEnergyList[i]);
            
            // search the domain value for the default selection
            if (currentProductEnergyList[i].getValue() == '2')
            {
                curOrderItem.orderItem.InstalledBase__r.ProductEnergy__c = currentProductEnergyList[i].getValue();
                //energyValue = currentProductEnergyList[i].getValue();
                //System.debug ('getProductEnergy energyValue is: '+energyValue);
            }
            else
            {
                System.debug ('getProductEnergy energyValue is null');
                //energyValue = null;
            }   
        }    
        return domProdEnergy.getDomainValuesByMaster(curOrderItem.orderItem.InstalledBase__r.ProductGroup__c);
    } // getProductEnergy

    /**
     * Is called when a product model was selected, it initialize the fields
     * depended on the product model.
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference chgProductModel()
    {
        System.debug('#### chgProductModel(): productModelId -> ' + productModelId);

        try
        {
            SCProductModel__c prodModel = [select Id, Name, Brand__c, Brand__r.Name, UnitClass__c,
                                                  UnitType__c, Group__c, Power__c, ProductNameCalc__c
                                           from SCProductModel__c
                                           where Id = :productModelId];

            curOrderItem.orderItem.InstalledBase__r.ProductModel__c = productModelId;
            curOrderItem.orderItem.InstalledBase__r.ProductModel__r = prodModel;
            curOrderItem.orderItem.InstalledBase__r.Brand__c = prodModel.Brand__c;
            curOrderItem.orderItem.InstalledBase__r.Brand__r = prodModel.Brand__r;
            curOrderItem.orderItem.InstalledBase__r.ProductUnitClass__c = prodModel.UnitClass__c;
            curOrderItem.orderItem.InstalledBase__r.ProductUnitType__c = prodModel.UnitType__c;
            curOrderItem.orderItem.InstalledBase__r.ProductGroup__c = prodModel.Group__c;
            curOrderItem.orderItem.InstalledBase__r.ProductPower__c = prodModel.Power__c;
            
            if (null != curOrderItem)
            {
                String selItem = curOrderItem.orderItem.ID2__c;
                if (SCBase.isSet(selItem) && selItem.equals('1'))
                {
                    boOrder.order.Brand__c = prodModel.Brand__c;
                    boOrder.order.BrandName__c = prodModel.Brand__r.Name;
                }
            }
            
            System.debug('#### chgProductModel(): prodModel -> ' + prodModel);
        }
        catch (Exception e)
        {
            System.debug('#### chgProductModel(): prodModel -> nothing founded');
        }

        return null;
    } // chgProductModel

    /**
     * Is called when a product model was selected, it initialize the fields
     * depended on the product model.
     *
     * @return    the page reference
     *
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference chgProductModel(String productModelIdChanged)
    {
        System.debug('#### chgProductModel(): productModelIdChanged -> ' + productModelIdChanged);

        try
        {
            SCProductModel__c prodModel = [select Id, Name, Brand__c, Brand__r.Name, UnitClass__c,
                                                  UnitType__c, Group__c, Power__c, ProductNameCalc__c
                                           from SCProductModel__c
                                           where Id = :productModelIdChanged];

            curOrderItem.orderItem.InstalledBase__r.ProductModel__c = productModelId;
            curOrderItem.orderItem.InstalledBase__r.ProductModel__r = prodModel;
            curOrderItem.orderItem.InstalledBase__r.Brand__c = prodModel.Brand__c;
            curOrderItem.orderItem.InstalledBase__r.Brand__r = prodModel.Brand__r;
            curOrderItem.orderItem.InstalledBase__r.ProductUnitClass__c = prodModel.UnitClass__c;
            curOrderItem.orderItem.InstalledBase__r.ProductUnitType__c = prodModel.UnitType__c;
            curOrderItem.orderItem.InstalledBase__r.ProductGroup__c = prodModel.Group__c;
            curOrderItem.orderItem.InstalledBase__r.ProductPower__c = prodModel.Power__c;
            
            if (null != curOrderItem)
            {
                String selItem = curOrderItem.orderItem.ID2__c;
                if (SCBase.isSet(selItem) && selItem.equals('1'))
                {
                    boOrder.order.Brand__c = prodModel.Brand__c;
                    boOrder.order.BrandName__c = prodModel.Brand__r.Name;
                }
            }
            
            System.debug('#### chgProductModel(): prodModel -> ' + prodModel);
        }
        catch (Exception e)
        {
            System.debug('#### chgProductModel(): prodModel -> nothing founded');
        }

        return null;
    } // chgProductModel
    
    /**
     * Removes the product model id from the current installed
     * base inside the order item.
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference removeProductModel()
    {
        System.debug('#### removeProductModel()');
        curOrderItem.orderItem.InstalledBase__r.ProductModel__r.Name = '';
        curOrderItem.orderItem.InstalledBase__r.ProductModel__c = null;
        curOrderItem.orderItem.InstalledBase__r.ProductModel__r = null;
        curOrderItem.orderItem.InstalledBase__r.Brand__r.Name = '';
        curOrderItem.orderItem.InstalledBase__r.Brand__c = null;
        curOrderItem.orderItem.InstalledBase__r.Brand__r = null;
        curOrderItem.orderItem.InstalledBase__r.ProductUnitClass__c = null;
        curOrderItem.orderItem.InstalledBase__r.ProductUnitType__c = null;
        curOrderItem.orderItem.InstalledBase__r.ProductGroup__c = null;
        curOrderItem.orderItem.InstalledBase__r.ProductPower__c = null;
        curOrderItem.orderItem.InstalledBase__r.ProductEnergy__c = null;

        // DH: remove the field for the serial number extraction
        isValidSerialNumber = 'check';

        currentOrderItem.orderItem.InstalledBase__r.SerialNo__c = null;
        currentOrderItem.orderItem.InstalledBase__r.ConstructionYear__c = null;
        currentOrderItem.orderItem.InstalledBase__r.ConstructionWeek__c = null;
        currentOrderItem.orderItem.InstalledBase__r.Article__c = null;
        currentOrderItem.orderItem.InstalledBase__r.ArticleName__c = null;
        currentOrderItem.orderItem.InstalledBase__r.ArticleEAN__c = null;       
        return null;
    } // removeProductModel

    /**
     * Retrieves the number of order items.
     *
     * @return    number of order items
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public Integer getItemCount()
    {
        System.debug('#### getItemCount()');
        return boOrder.boOrderItems.size();
    } // getItemCount
} // SCItemsTabComponentController
