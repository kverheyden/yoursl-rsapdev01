/*
 * @(#)SCMaintenancePlanningResultTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Test class checks the controler functions.
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCMaintenancePlanningResultTest
{
    private static SCMaintenancePlanningResultController ocac;

    static testMethod void planningResultControllerPositiv1() 
    {   
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.createInstalledBaseLocation(true);
        SCHelperTestClass.createInstalledBase(true);
        SCHelperTestClass.createProductModel(true);
        SCHelperTestClass.createTestUsers(true);
        
        SCHelperTestClass2.createContractTemplates(true);
        SCHelperTestClass2.createContract('Contract', SCHelperTestClass2.insuranceTemplate.Id, SCHelperTestClass.account.Id, SCHelperTestClass.installedBaseSingle.Id, true);
        
        SCContractSchedule__c cs = new SCContractSchedule__c();
        cs.CurrencyIsoCode = 'EUR';
        insert cs;

        SCHelperTestClass.order.Contract__c = SCHelperTestClass2.contracts.get(0).Id;
        
        Id appId = [ Select Id From SCAppointment__c Where Order__c = : SCHelperTestClass.order.Id Limit 1 ].Id;
        
        SCAppointment__c a = new SCAppointment__c(id = appId);
        a.id2__c = '1234';
        
        update a;
        
        SCContractScheduleItem__c csi = new SCContractScheduleItem__c();
        csi.ContractSchedule__c = cs.id;
        csi.Order__c = SCHelperTestClass.order.Id;
        csi.Contract__c = SCHelperTestClass2.contracts.get(0).Id;
        csi.AppointmentID2__c = '1234';
        
        insert csi;
        
        ApexPages.currentPage().getParameters().put('id', cs.id);
        
        /*
        List<SCAppointment__c> appointmentsList = SCHelperTestClass.appointments;
        
        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(appointmentsList);
        controller.setSelected(appointmentsList);
        
        ApexPages.currentPage().getParameters().put('id', SCHelperTestClass.order.Id);
        */
        
        
        ocac = new SCMaintenancePlanningResultController();
        
        ocac.getCon();
        ocac.getContractScheduleItem();
        
        
        ocac.selectedProcess = String.valueOf(appId);
        ocac.reloadPage();
        ocac.getLastProcessOfUser();
        ocac.getSelectionFilter();
        ocac.reloadPage();
        ocac.cancelAppointments();
    }

}
