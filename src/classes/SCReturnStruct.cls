/*
 * @(#)SCResultInfo.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCReturnStruct
{
    /**
     * Constants for the 'type' field.
     */
    public static final String TYPE_SUCCESS = 'S';
    
    public static final String TYPE_ERROR = 'E';
    
    public static final String TYPE_WARNING = 'W';
    
    public static final String TYPE_INFO = 'I';
    
    public static final String TYPE_ABORT = 'A';
    
    public static final String TYPE_EMPTY = ' ';


    /**
     * Describes the return structure type. The remaining
     * fields of the structure may be used in different ways depending on this type.
     */
    private String type;

    /**
     * An internally used field for performance counters, timer and similar
     */
    private Long counter;
    
    /**
     * Multi-purpose field which typically holds a short identifier.
     */
    public String id;

    /**
     * Multi-purpose field which typically holds a numerical identifier, 
     * like an error number.
     */
    public Integer numIdentifier;

    /**
     * Multi-purpose field which typically holds descriptive message, like 
     * an error message.
     */
    public String message;

    /**
     * Multi-purpose field which typically holds an additional descriptive message.
     */
    public String message_v1;

    /**
     * Multi-purpose field which typically holds an additional descriptive message.
     */
    public String message_v2;

    /**
     * Multi-purpose field which typically holds an additional descriptive message.
     */
    public String message_v3;
    
    /**
     * Multi-purpose field which typically holds an additional descriptive message.
     */
    public String message_v4;
    
    /**
     * Flags an empty structure
     */
    protected boolean bEmpty;
    
    /**
     * Constructs an empty MfxReturnStruct object
     */
    public SCReturnStruct()
    {
        empty();
    }
    
    /**
     * Initializes the MfxReturnStruct object
     */
    public void empty()
    {
        type = '';
        setCounter(0);
        id = '';
        setnumIdentifier(0);
        message = '';
        message_v1 = '';
        message_v2 = '';
        message_v3 = '';
        message_v4 = '';
        bEmpty = true;
    }


    /**
     * Returns true if structure is empty.
     * 
     */
    public boolean isEmpty()
    {
        return bEmpty;
    }

    
    /**
     * Returns the type.
     * 
     */
    public String getType()
    {
        return type;
    }
    
    /**
     * Sets the type.
     * 
     */
    public void setType(String type)
    {
        this.type = type;
        bEmpty = false;
    }
    
    /**
     * Returns the counter.
     * 
     */
    public Long getCounter()
    {
        return counter;
    }
    
    /**
     * Sets the counter.
     * 
     */
    public void setCounter(long counter)
    {
        this.counter = counter;
        bEmpty = false;
    }

    /**
     * Returns the numIdentifier.
     * 
     */
    public Integer getNumIdentifier()
    {
        return numIdentifier;
    }
    
    /**
     * Sets the numIdentifier.
     * 
     */
    public void setNumIdentifier(Integer numIdentifier)
    {
        this.numIdentifier = numIdentifier;
        bEmpty = false;
    }
    
}
