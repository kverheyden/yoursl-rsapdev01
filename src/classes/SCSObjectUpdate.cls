/*
 * @(#)SCSObjectUpdate.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Class for running a batch job, which does only an upate on a object.
 *
 * Example for a call:
 * Description: Updates all contract items of british contract
 *              The update executes a trigger on contract item, which does the proper work.
 *
 * Code:
 * SCSObjectUpdate u = new SCSObjectUpdate();
 * u.query = 'select id, InstalledBase__c from SCContractItem__c where Contract__r.Country__c = \'GB\'';
 * u.email = 'awagner@gms-online.de';
 *
 * Database.executeBatch(u, 95);
 *
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
global class SCSObjectUpdate implements Database.Batchable<sObject>
{
    public String query;
    public String email;


    global Database.querylocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        update scope;
    }
    
    global void finish(Database.BatchableContext BC)
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    
        mail.setToAddresses(new String[] {email});
        mail.setReplyTo('donotreply@gms-online.de');
        mail.setSenderDisplayName('Batch Processing');
        mail.setSubject('Batch Process Completed');
        mail.setPlainTextBody('Batch Process has completed');
    
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}
