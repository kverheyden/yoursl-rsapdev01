/** 
	* @author Jan Mensfeld
	* @date 30.10.2014
	* @description Class to hold Trigger parameters
	*/
public with sharing class TriggerParameters{
	
	 /** 
	* @author Jan Mensfeld
	* @date 30.10.2014
	* @description Enum to represent trigger events
	*/
	public Enum TriggerEvent { 
		beforeInsert, 
		beforeUpdate, 
		beforeDelete, 
		afterInsert, 
		afterUpdate, 
		afterDelete, 
		afterUndelete 
	}
	
	public TriggerEvent event { get; private set; }
	public List<SObject> oldList { get; private set; }
	public List<SObject> newList {get; private set; }
	public Map<Id, SObject> oldMap { get; private set; }
	public Map<Id, SObject> newMap { get; private set; }
	public String triggerObject { get; private set; }
	public Boolean isRunning { get; private set; }
	
	/** 
	* @author Jan Mensfeld
	* @date 30.10.2014
	* @description Instanziates the TriggerParameter object.
	* @param List<SObject> List of trigger records with state of 'before' event (Represents Trigger.old)
	* @param List<SObject> List of trigger records with state of 'after' event (Represents Trigger.new)
	* @param Map<Id, SObject> Map of trigger records with the state of 'before' event (Represents Trigger.oldMap)
	* @param Map<Id, SObject> Map of trigger records with the state of 'after' event (Represents Trigger.newMap)
	* @param Boolean Flag for isBefore event
	* @param Boolean Flag for isAfter event
	* @param Boolean Flag for isDelete event
	* @param Boolean Flag for isInsert event
	* @param Boolean Flag for isUpdate event
	* @param Boolean Flag for isUnDelete event
	* @param Boolean Flag to indicate if trigger is running
	*/
	public TriggerParameters(List<SObject> oldList, List<SObject> newList, Map<Id, SObject> oldMap, Map<Id, SObject> newMap,
								Boolean isBefore, Boolean isAfter, Boolean isDelete, Boolean isInsert, Boolean isUpdate, Boolean isUndelete, Boolean isRunning) {
		this.oldList = oldList;
		this.newList = newList;
		this.oldMap = oldMap;
		this.newMap = newMap;
									
		this.triggerObject = Utils.getSObjectTypeName((this.oldList != null && this.oldList.size() > 0) ? this.oldList[0] : this.newList[0]);
		
		if (isBefore && isInsert) 
			event = TriggerEvent.beforeInsert;
		else if (isBefore && isUpdate) 
			event = TriggerEvent.beforeUpdate;
		else if (isBefore && isDelete) 
			event = TriggerEvent.beforeDelete;
		else if (isAfter && isInsert) 
			event = TriggerEvent.afterInsert;
		else if (isAfter && isUpdate) 
			event = TriggerEvent.afterUpdate;
		else if (isAfter && isDelete) 
				event = TriggerEvent.afterDelete;
		else if (isAfter && isUndelete) 
			event = TriggerEvent.afterUndelete;
		
		isRunning = isRunning;
	}
}
